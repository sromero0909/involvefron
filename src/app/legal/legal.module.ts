import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LegalRoutingModule } from './legal-routing.module';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { NoticePrivacyComponent } from './notice-privacy/notice-privacy.component';


@NgModule({
  declarations: [
    TermsAndConditionsComponent,
    NoticePrivacyComponent
  ],
  imports: [
    CommonModule,
    LegalRoutingModule
  ]
})
export class LegalModule { }
