import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NoticePrivacyComponent } from './notice-privacy/notice-privacy.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';

const routes: Routes = [
  {path: 'terminos-y-condiciones', component: TermsAndConditionsComponent},
  {path: 'notice-privacy', component: NoticePrivacyComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LegalRoutingModule { }
