import { Directive, OnInit, forwardRef, ElementRef, HostListener, Renderer2 } from '@angular/core';
import { CurrencyPipe, DecimalPipe } from '@angular/common';

@Directive({
  selector: '[currency-only]'
})
export class CurrencyOnlyDirective implements OnInit {
  input: HTMLInputElement;
  constructor(private el: ElementRef, private currencyPipe: CurrencyPipe) {
    this.input = el.nativeElement;
  }

  ngOnInit() {
    this.input.value = this.format(this.input.value);
  }

  @HostListener('blur', ['$event.target.value']) onBlur(e: string) {
    this.input.value = this.format(e.replace(/[^\d.)]/gm, ''));
  }

  @HostListener('focus', ['$event.target.value']) onFocus(e: string) {
    try{
      e = Number(e.replace(/[^\d.)]/gm, '')).toString();
      if (Number(e) > 0){
        this.input.value = Number(e).toString();
      }
      else{
        this.input.value = '';
      }
    }
    catch {
      this.input.value = '';
    }
  }

  format(value: string): string {
    try{
      const ammount = Number(value);
      return new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'MXN',
        currencyDisplay: 'narrowSymbol',
        minimumFractionDigits: 2,
      }).format(Number(ammount))
    }
    catch {
      return '';
    }
  }
}
