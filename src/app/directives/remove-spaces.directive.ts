import { Directive, ElementRef, HostListener, Output, EventEmitter } from '@angular/core';
import { NgModel } from "@angular/forms";

@Directive({
    selector: '[text-trim]',
    providers: [NgModel]
})

export class TextInputTrimDirective {

@Output() ngModelChange: EventEmitter<any> = new EventEmitter();
input: HTMLInputElement;
constructor(private el: ElementRef,private model:NgModel) {
    this.input = el.nativeElement;
}


@HostListener('blur', ['$event.target.value']) onBlur(e: string) {
    const value = e.trim();
    let value2 = value.replace(/^\s+|\s+$|\s+(?=\s)/g, "");
     value2 = String(value2).replace(/\s+/g,' ' ).replace(/^\s/,'').replace(/\s$/,'');
     this.input.value = value2
     this.input.innerHTML = value2

  }

  @HostListener('focusout', ['$event.target.value']) onFocus(e: string) {
    let e1 = e.match(/\S+/g);
    this.input.value = e1 ? e1.join(' ') : '';
    this.ngModelChange.emit(this.input.value)

  }
@HostListener('change') onInputChange() {
    const value = this.el.nativeElement.value.trim();
    let value2 = value.replace(/^\s+|\s+$|\s+(?=\s)/g, "");
     value2 = String(value2).replace(/\s+/g,' ' ).replace(/^\s/,'').replace(/\s$/,'');
     this.input.value = value2
     this.model.valueAccessor?.writeValue(value2);
     this.input.innerHTML = value2
}
}