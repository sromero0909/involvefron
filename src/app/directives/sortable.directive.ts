import {Directive, EventEmitter, Input, Output} from '@angular/core';
import { Plantilla } from '../services/models/plantillas.model';

export type SortColumn = keyof Plantilla | '';
export type SortDirection = 'asc' | 'desc' | 'sort';
const rotate: {[key: string]: SortDirection} = { 'asc': 'desc', 'desc': 'sort', 'sort': 'asc' };

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '[class.sort]': 'direction === "sort"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {

  @Input() sortable!: string;
  @Input() direction: SortDirection = 'sort';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({column: this.sortable, direction: this.direction});
  }
}