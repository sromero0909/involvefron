import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appTextAreaWithBullets]'
})
export class TextAreaWithBulletsDirective {
  @Input() bulletCarachter = '•';
  constructor(private el: ElementRef) {
  }

  /**
   * Allows to alter the value of a <textArea> adding a default character
   * at the begging of each line, except if it's an empty line.
   *
   * @param bulletCarachter Defines the caracter used as bullet. '•' Is the default option.
   */
  @HostListener('input') appTextAreaWithBullets(): void {
    const currentValue = (this.el.nativeElement as HTMLTextAreaElement).value;
    (this.el.nativeElement as HTMLTextAreaElement).value = currentValue
    .replace(new RegExp(`${this.bulletCarachter} {0,}`, 'gm'), '') // Removes all the bullets at any position
    .replace(new RegExp(`^`, 'gm'), `${this.bulletCarachter} `) // Set the bullet at the begginng of the line
    .replace(new RegExp(`^${this.bulletCarachter} *\n`, 'gm'), '\n') // Removes bullet from empty lines
    .replace(new RegExp(`^${this.bulletCarachter} $`, 'gm'), ''); // Removes bullets from last line
  }
}


