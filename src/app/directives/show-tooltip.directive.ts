import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appShowTooltip]'
})
export class ShowTooltipDirective {
  /**
   * Set two listeners used to show and hide the "tooltip" in the sidebar
   * of the dashboard
   *
   * Must be located in the previous sibling of the tooltip information
   * @example
   * <div class="triggerer" appShowTooltip>
   * ...
   * </div>
   * <div tooltip class="source">
   *  <span>Tooltip information!</span>
   * </div>
   * @param el The element where the directive is being used
   */
  constructor(private el: ElementRef) {}

  @HostListener('mouseenter') mouseenter(): void {
    /**
     * The element to be assigned to the listener
     */
    const triggererElement = (this.el.nativeElement as HTMLElement);
    if (triggererElement.parentElement && triggererElement.parentElement.querySelector('[tooltip].source')){
      const tooltipElement = (triggererElement.parentElement.querySelector('[tooltip].source') as HTMLElement);
      const tooltipInfo = tooltipElement.innerHTML;

      let currentElement: HTMLElement | null = triggererElement;
      let offsetTop = currentElement.offsetTop;
      do{
        /**
         * Calculate the needed top offset to be aligned with the triggerer element
         */
        currentElement = currentElement.offsetParent as HTMLElement;
        if (currentElement && currentElement.offsetParent?.tagName !== 'BODY'){
          offsetTop += currentElement.offsetTop;
        }
      } while (currentElement !== null);

      const target = (document.querySelector('[tooltip].involve-tooltip .tooltip-info') as HTMLElement);
      if (target){
        /**
         * Overwrite the tooltip information
         */
        target.innerHTML = tooltipInfo;
        target.style.marginTop = `${offsetTop}px`;
        target.classList.remove('empty');
      }
    }
  }

  @HostListener('mouseleave') mouseleave(): void {
    const target = (document.querySelector('[tooltip].involve-tooltip .tooltip-info') as HTMLElement);
    if (target){
      /**
       * Erase any value on the tooltip target
       */
      target.innerHTML = '';
      target.style.marginTop = '0px';
      target.classList.add('empty');
    }
  }
}
