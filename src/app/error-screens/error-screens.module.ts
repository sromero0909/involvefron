import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ErrorScreensRoutingModule } from './error-screens-routing.module';
import { NotFoundComponent } from './not-found/not-found.component';


@NgModule({
  declarations: [
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    ErrorScreensRoutingModule
  ]
})
export class ErrorScreensModule { }
