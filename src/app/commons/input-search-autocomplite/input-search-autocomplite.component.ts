import { Component, EventEmitter, Output, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-input-search-autocomplite',
  templateUrl: './input-search-autocomplite.component.html',
  styleUrls: ['./input-search-autocomplite.component.scss']
})
export class InputSearchAutocompliteComponent implements OnInit {

  @Output() onEnter   : EventEmitter<string>  = new EventEmitter();
  @Output() onDebounce: EventEmitter<string>  = new EventEmitter();
  @Output() onBlur:     EventEmitter<boolean> = new EventEmitter();
  @Output() onFocus:     EventEmitter<boolean> = new EventEmitter();

  @Input() placeholder: string = '';
  @Input() selectListItem: string = '';
  @Input() valueInitial = '';
  debouncer: Subject<string> = new Subject();

  termino: string = '';

  valueInput = '';



  ngOnInit() {
    this.debouncer
      .pipe(debounceTime(600))
      .subscribe( valor => {
        this.onDebounce.emit( valor );
      });

      this.termino = this.selectListItem;
  }

  buscar() {
    this.onEnter.emit( this.termino );
  }

  teclaPresionada() {
    this.debouncer.next( this.termino );
  }

  onBlurDebounce = () => {
    this.onBlur.emit(false);
  }

  onFocusInput = (text:any) => {
    const texto = (text=="") ? this.selectListItem : text
    this.onFocus.emit(texto);
  }


  
}
