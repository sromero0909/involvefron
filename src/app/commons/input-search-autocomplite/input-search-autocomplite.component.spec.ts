import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputSearchAutocompliteComponent } from './input-search-autocomplite.component';

describe('InputSearchAutocompliteComponent', () => {
  let component: InputSearchAutocompliteComponent;
  let fixture: ComponentFixture<InputSearchAutocompliteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputSearchAutocompliteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputSearchAutocompliteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
