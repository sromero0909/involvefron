import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidatorsService {
  allCharacters: string = '[(a-z)(A-Z)(ñ)(Ñ)(.)( )(áéíóúÁÉÍÓÚ)(\')]*';
  allCharactersAndNumbersAndSpecial: string = '[(a-z)(A-Z)(ñ)(Ñ)(.)( )(áéíóúÁÉÍÓÚ)(0-9)(\')(\!)(\")(\#)(\$)(\%)(\&)(\')(\()(\))(\*)(\+)(\,)(\\)(\-)(\/)(\:)(\;)(\<)(\=)(\>)(\?)(\@)(\^)(\_)(\`)]*';
  noWhiteSpaces(control: FormControl) {
    const isOnlyWhiteSpaces = (control.value || '').trim() === '';
    return isOnlyWhiteSpaces ? { whiteSpace: true } : null
  }

  constructor() { }
}
