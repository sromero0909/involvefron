import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree,Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from 'src/app/auth/services/sessionService/session.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private sessionService: SessionService, private router: Router){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (this.sessionService.isTokenActive()){
        return true;
      }
      else{
        localStorage.removeItem('e');
        sessionStorage.removeItem('t');
        sessionStorage.removeItem('rId');
        return this.router.createUrlTree(['/login'], { queryParams: { returnUrl: state.url }});
      }
  }
  
}
