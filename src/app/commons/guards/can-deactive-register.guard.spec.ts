import { TestBed } from '@angular/core/testing';

import { CanDeactiveRegisterGuard } from './can-deactive-register.guard';

describe('CanDeactiveRegisterGuard', () => {
  let guard: CanDeactiveRegisterGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CanDeactiveRegisterGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
