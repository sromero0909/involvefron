import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

export interface PuedeDesactivar {
  permitirSalirRuta: () => Observable<boolean> | Promise <boolean> | boolean
}

@Injectable({
  providedIn: 'root'
})
export class CanDeactiveRegisterGuard implements CanDeactivate<PuedeDesactivar> {
  canDeactivate(component: PuedeDesactivar) {
    console.log("entra el guard");
    return component.permitirSalirRuta ? component.permitirSalirRuta(): true;
  }
  
}
