import { Component, TemplateRef } from '@angular/core';

import { ToastService } from '../../services/commons/toast.setvice';


@Component({
  selector: 'app-toasts',
  template: `
    <ngb-toast
      
      *ngFor="let toast of toastService.toasts"
      [class]="toast.classname"
      [autohide]="true"
      [delay]="toast.delay || 1000"
      (hidden)="toastService.remove(toast)"
      [header]="toast.textHeader"
    >
      <ng-template [ngIf]="isTemplate(toast)" [ngIfElse]="text" >
        <ng-template [ngTemplateOutlet]="toast.textOrTpl"></ng-template>
      </ng-template>
      <ng-template #text>
      <img async *ngIf="toast.flagIcono=='true'" class="cursor-pointer mr-3" src="{{toast.urlIcono}}"> {{ toast.textOrTpl }}</ng-template>
    </ngb-toast>
  `,
  host: { '[class.ngb-toasts]': 'true' }
})
export class ToastsContainer {
  constructor(public toastService: ToastService) { }

  isTemplate(toast: any) { return toast.textOrTpl instanceof TemplateRef; }
}
