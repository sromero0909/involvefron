import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ConfirmCombineComponent } from 'src/app/modulos/dashboardReclutadores/talent/candidate-finder/favorites/modal-favorite/confirm-combine/confirm-combine.component';
import { ConfirmDeleteComponent } from 'src/app/modulos/dashboardReclutadores/talent/candidate-finder/favorites/modal-favorite/confirm-delete/confirm-delete.component';

import { ModalFavoriteComponent } from 'src/app/modulos/dashboardReclutadores/talent/candidate-finder/favorites/modal-favorite/modal-favorite.component';
import { NoPermissionCombineComponent } from 'src/app/modulos/dashboardReclutadores/talent/candidate-finder/favorites/modal-favorite/no-permission-combine/no-permission-combine.component';
import { ApiService } from 'src/app/services/api.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';


@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {

  @Input() itemsFavorites: any;
  @Input() permission = false;

  catalogImages: any;
  recruiter: any;
  sortDirection = 'DESC';
  sortBy = 'creationDate';
  pageNumber = 0;
  pageSize = 15;
  query: string;

  @Output() propagarFiltroReclutador = new EventEmitter<string>();

  constructor(
    private dialog: MatDialog,
    private readonly apiService: ApiService,
    private readonly toastService: ToastService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  getListImagesFavorite = () => {
    this.apiService.get('/catalog/image').subscribe((resp) => {
      this.catalogImages = resp;

    }, (error) => {
      console.log(error);
    });
  }

  openEditDialog = (item: any) => {
    this.dialog.open(ModalFavoriteComponent, { data: { create: false, itemFavorite: item } }).afterClosed().subscribe((returnData) => {
      if (returnData.delete) {
        this.dialog.open(ConfirmDeleteComponent, { disableClose: true, data: { id: item?.listFavoriteId } }).afterClosed()
          .subscribe(() => {
            this.propagarFiltroReclutador.emit();
          });
      } else {
        const images: any[] = returnData.images;
        this.apiService.patch([
          { "op": "replace", "path": "/name", "value": returnData.value.name },
          { "op": "replace", "path": "/image", "value": { "imageId": returnData.value.imageId } }
        ], `/list-favorite/${item.listFavoriteId}`).subscribe((resp: any) => {
          item.name = resp.body.name;
          item.image = images.filter((image) => image.imageId === resp.body.image.imageId)[0];
        });
      }
    });
  }

  onItemDrop = (e: any, dropedOn: any, permission: boolean): void => {
    const dragged = e.dragData;
    if (permission && dragged.permission) {
      this.dialog.open(ConfirmCombineComponent, { disableClose: true, data: { id: e?.listFavoriteId } }).afterClosed().subscribe((returnData) => {
        if (returnData) {
          if (dragged.listFavorite.listFavoriteId != dropedOn.listFavoriteId) {
            this.apiService.put(`/list-favorite/combine?idListCombine=${dragged.listFavorite.listFavoriteId}&idListDestination=${dropedOn.listFavoriteId}`).subscribe(() => {
              this.propagarFiltroReclutador.emit();
            });
          }
        }
      });
    } else {
      this.dialog.open(NoPermissionCombineComponent, { disableClose: true, data: { id: e?.listFavoriteId } });
    }
  }



  beEditor = (id: any) => {
    this.apiService.postId(`/list-favorite/recruiter?listFavoriteId=${id}`).subscribe((resp) => {
      this.recruiter = resp;
      this.toastService.show('Se ha agregado como auxiliar al grupo de favoritos: ', { classname: 'bg-success text-light ngb-toastsBottom', delay: 6000 });
    }, (error) => {
      console.log(error);
    });
  }

  openItem(item: any, permission: boolean) {
    this.router.navigate(['/dashboard/talent/candidatos', item.listFavorite.listFavoriteId, permission]);
  }
}
