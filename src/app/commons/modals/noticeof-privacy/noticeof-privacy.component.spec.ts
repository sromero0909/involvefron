import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoticeofPrivacyComponent } from './noticeof-privacy.component';

describe('NoticeofPrivacyComponent', () => {
  let component: NoticeofPrivacyComponent;
  let fixture: ComponentFixture<NoticeofPrivacyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoticeofPrivacyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticeofPrivacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
