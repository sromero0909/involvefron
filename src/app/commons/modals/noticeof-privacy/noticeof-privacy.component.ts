import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-noticeof-privacy',
  templateUrl: './noticeof-privacy.component.html',
  styleUrls: ['./noticeof-privacy.component.scss']
})
export class NoticeofPrivacyComponent implements OnInit {

  @Input() data: any;
  @Output() btnAcept = new EventEmitter<boolean>();


  title: string = ""
  subtitle:string = ""
  text: string = ""
  constructor(private modalService: NgbModal,) { }

  ngOnInit(): void {
    this.subtitle = this.data.title
    this.text = this.data.text
  }


  closeModal = (response:boolean) => {
    this.btnAcept.emit(response)
    this.modalService.dismissAll();
  }

}
