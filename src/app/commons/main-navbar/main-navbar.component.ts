import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';



@Component({
  selector: 'app-main-navbar',
  templateUrl: './main-navbar.component.html',
  styleUrls: ['./main-navbar.component.scss']
})
export class MainNavbarComponent implements OnInit {


  @Input() mainNavData: any;

  @Input() iconBack: boolean;

  @Input() logo: boolean;

  @Input() image: string;

  @Input() title: string;

  @Input() subtitle: string;

  @Input() search: boolean;

  @Input() badgeAlert: boolean;

  @Input() extra: boolean;

  @Output() eventClick: EventEmitter<any> = new EventEmitter;

  arrayReclutadores: Array<any> = [];

  constructor(
    private readonly router: Router,
    public _suscription: SuscriptionService,
  ) { }

  ngOnInit(): void {
  }

  goTo = () => {
    this.router.navigateByUrl(this.mainNavData.linkUrl);
  }

  goBack = () => {
    window.history.go(-1);
    return false;
  }

  onClick = (event: any) => {
    this.eventClick.emit(event);
  }
}
