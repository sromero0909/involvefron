import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//importaciones para intenalizacion
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { InternalizacionComponent } from './internalizacion/internalizacion.component';
import {TranslateService} from '@ngx-translate/core';


export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [InternalizacionComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http1:HttpClient) =>{
          return new TranslateHttpLoader(http1,'./assets/i18n/','.json')
        },
        deps: [HttpClient]
      }
    })
  ],
  exports:[TranslateModule,InternalizacionComponent]
})
export class InternalizacionModule {
  public lenguajeActivo:string ='es'
  constructor(public translate: TranslateService) {
    translate.addLangs(['en', 'es']);
    translate.setDefaultLang(this.lenguajeActivo);
    translate.use(this.lenguajeActivo);
   // const browserLang = translate.getBrowserLang();
   // translate.use(browserLang.match(/en|fr|es/) ? browserLang : this.lenguajeActivo);

  }
 }
