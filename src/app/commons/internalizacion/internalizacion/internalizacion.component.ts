import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-internalizacion',
  templateUrl: './internalizacion.component.html',
  styleUrls: ['./internalizacion.component.scss']
})
export class InternalizacionComponent implements OnInit {

  constructor(public translate: TranslateService) {
    //translate.addLangs(['en', 'fr', 'es']);
   //translate.setDefaultLang('e');

    //const browserLang = translate.getBrowserLang();
    //translate.use(browserLang.match(/en|fr|es/) ? browserLang : 'es');
  }

  ngOnInit(): void {
  }

}
