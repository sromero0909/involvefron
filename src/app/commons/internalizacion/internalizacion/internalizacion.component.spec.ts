import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InternalizacionComponent } from './internalizacion.component';

describe('InternalizacionComponent', () => {
  let component: InternalizacionComponent;
  let fixture: ComponentFixture<InternalizacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InternalizacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
