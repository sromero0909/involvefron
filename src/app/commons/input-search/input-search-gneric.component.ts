import { Component, OnInit, Output, ViewChild, ElementRef, EventEmitter, Input } from '@angular/core';

import { debounceTime, map, distinctUntilChanged, filter } from 'rxjs/operators';
import { fromEvent } from 'rxjs';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';

@Component({
  selector: 'app-input-search-generic',
  templateUrl: './input-search-generic.component.html',
  styleUrls: ['./input-search-generic.component.scss'],
})
export class InputSearchComponent implements OnInit {

  @ViewChild('searchInputGeneric') searchInputGeneric!: ElementRef;
  @Input() hide = false;
  @Output() spreadsearchInputGeneric = new EventEmitter<string>();

  constructor(public _suscription: SuscriptionService, ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.inicializarBuscador();
  }


  inicializarBuscador() {
    if ((this.searchInputGeneric !== null && this.searchInputGeneric !== undefined)) {
      fromEvent(this.searchInputGeneric.nativeElement, 'keyup').pipe(
        map((event: any) => {
          return event.target.value;
        })
        , debounceTime(600)
        , distinctUntilChanged()
      ).subscribe((text: string) => {
        this.spreadsearchInputGeneric.emit(text);
      });
    }
  }
}
