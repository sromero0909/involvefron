import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsAndPhotoComponent } from './notifications-and-photo.component';

describe('NotificationsAndPhotoComponent', () => {
  let component: NotificationsAndPhotoComponent;
  let fixture: ComponentFixture<NotificationsAndPhotoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotificationsAndPhotoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsAndPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
