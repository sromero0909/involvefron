import { Component, OnInit, Input, Output, ViewChild, EventEmitter, TemplateRef, ElementRef } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

import { ActivatedRoute, Router } from '@angular/router';
import { PushNotificationsService } from 'src/app/services/commons/push-notifications.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { debounceTime, map, distinctUntilChanged, filter } from 'rxjs/operators';
import { fromEvent } from 'rxjs';

import * as moment from 'moment';
import 'moment/locale/es';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalExpirationComponent } from 'src/app/modulos/dashboardReclutadores/vacantes/modal-expiration/modal-expiration.component';
import { SuscriptionCanceledService } from 'src/app/services/reclutador/login/subscription-canceled.service';
import { CookieService } from 'ngx-cookie-service';
import { version } from '../../../../package.json';
import { SessionService } from 'src/app/auth/services/sessionService/session.service';

@Component({
  selector: 'app-notifications-and-photo',
  templateUrl: './notifications-and-photo.component.html',
  styleUrls: ['./notifications-and-photo.component.scss']
})
export class NotificationsAndPhotoComponent implements OnInit {
  currentVersion: string = version;
  @Output() spreadVacantSearchInput = new EventEmitter<string>();
  @Output() eventClick: EventEmitter<any> = new EventEmitter;

  @Input() hideElements = false;
  @Input() showOld = false;
  @Input() Show_menu_user = false;
  @Input() Show_menu_notification = false;
  @Input() ocultar = '';
  @Input() showProfile = true;
  @Input() showMessages = false;
  @Input() showNotification = false;
  @Input() showContentSearch = true;
  @Input() showTimmer = false;
  @Input() showExpirationbutton = false;

  @ViewChild('vacantSearchInput') vacantSearchInput!: ElementRef;

  // Datos de la sesion

  public nameUserSesion: string = this.authService.getName() + ' ' + this.authService.getLastName() + ' ' + this.authService.getSecondLastName();
  public userEmailSesion: string = this.authService.getEmail();
  public photoProfile: string = this.authService.getPhotoProfile();
  imagenPerfil = '../../../../assets/img/dashboard/vacantes/sinFotoPerfilSmall.svg';

  arrayNotifications: Array<any> = [];

  contNotifications = 0;
  newNotification = false;
  executeModal = false;
  idSuscription = '';
  userId = '';
  fechaVencimiento = this._suscriptionCanceled.getDateExpiration();
  fechaActual = this._suscriptionCanceled.getCurrentDate();

  MILLISECONDS_OF_A_SECOND = 1000;
  MILLISECONDS_OF_A_MINUTE = this.MILLISECONDS_OF_A_SECOND * 60;
  MILLISECONDS_OF_A_HOUR = this.MILLISECONDS_OF_A_MINUTE * 60;
  MILLISECONDS_OF_A_DAY = this.MILLISECONDS_OF_A_HOUR * 24;
  SPAN_DAYS: any = 0;
  SPAN_HOURS: any = 0;
  SPAN_MINUTES: any = 0;
  SPAN_SECONDS: any = 0;
  DATE_TARGET: any = new Date();


  expirationDate: string;
  isInResuscriptionProcess = false;
  isInSuscriptionProcess = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private notification: PushNotificationsService,
    private toastService: ToastService,
    public _suscription: SuscriptionService,
    private route: Router,
    private modalServise: NgbModal,
    private _suscriptionCanceled: SuscriptionCanceledService,
    private cookieService: CookieService,
    private sessionService: SessionService) {
    notification.requestPermission().then(token => {
      this.notification.setTokenNotification(token);
    }).catch((error) => {
      console.warn(error);
    });
    this.isInResuscriptionProcess = this.activatedRoute.snapshot.paramMap.has('isRenovation') &&
                                    this.activatedRoute.snapshot.paramMap.get('isRenovation')?.valueOf() === 'true';
    if (this.notification.getNumNotifications() != undefined) {
      this.newNotification = this.notification.getNumNotifications().newNotification;
      this.contNotifications = this.notification.getNumNotifications().contNotifications;
      this.arrayNotifications = this.notification.getNumNotifications().rows;
    }


  }

  ngOnInit(): void {
    this.notifications();
    if (this.authService.getUser() != undefined && this.authService.getUser() != '') {
      const user = JSON.parse(this.authService.getUser());
      this.userId = user.user?.userOpenPay;
      // if (this.userId) {
      //   this.hideElements = false
      // }
      this.expirationDate = user.user.suscriptionExpires.substring(0, 10);
      this.nameUserSesion = this.authService.getName() + ' ' + this.authService.getLastName() + ' ' + this.authService.getSecondLastName();
      this.userEmailSesion = this.authService.getEmail();    
      this.photoProfile = (String(this.photoProfile) == 'null' || String(this.photoProfile) == '' || String(this.photoProfile) == 'undefined') ? '../../../../assets/img/dashboard/vacantes/sinFotoPerfil.svg' + '?' + (new Date()).getTime() : this.photoProfile + '?' + (new Date()).getTime();
      this.isInSuscriptionProcess = user.stepsStatus <= 7;
    }
    this.validateDate();
    // this.getSuscription();
    // Se remueve como parte del hotfix para evitar el pago de usuarios hasta enero 31
  }

  ngAfterViewInit() {
    this.inicializarBuscador();
  }

  ngOnDestroy() {
    this.notification.setNumNotifications({ newNotification: this.newNotification, contNotifications: this.contNotifications, rows: this.arrayNotifications });
  }

  validateDate = () => {
    const localExpirationDate = moment(this.expirationDate);
    const todayDate = moment();
    const currentUser = JSON.parse(this.authService.getUser());
    if (localExpirationDate.diff(todayDate, 'd') < 0 &&
      localExpirationDate.diff(todayDate, 'd') >= -2 &&
      currentUser.user.userRol === 'RECRUITER_ADMIN' &&
      currentUser.stepsStatus !== null &&
      currentUser.stepsStatus >= 7 &&
      !this.isInResuscriptionProcess) {
      this.DATE_TARGET = moment(localExpirationDate);
      this.updateCountdown();
      this.showTimmer = true;
    }

    if (localExpirationDate.diff(todayDate, 'd') <= 5 &&
      localExpirationDate.diff(todayDate, 'd') >= 0 &&
      currentUser.user.userRol === 'RECRUITER_ADMIN' &&
      currentUser.stepsStatus !== null &&
      currentUser.stepsStatus >= 7 &&
      !this.isInResuscriptionProcess) {
      this.showExpirationbutton = true;
    }
  }

  mouseOverImagenPerfil() {
    this.imagenPerfil = '../../../../assets/img/dashboard/vacantes/Sinfoto.svg';
  }

  mouseLeaveImagenPerfil() {
    this.imagenPerfil = '../../../../assets/img/dashboard/vacantes/sinFotoPerfilSmall.svg';
  }

  logout(): void {
    this.authService.logOutRecuitre(this.notification.getTokenNotification()).subscribe(
      (response: any) => {
        localStorage.clear();
        this._suscription.setlastCompletedStep(0, true);
        document.cookie.split(';').map((el) => el.split('=')[0]).forEach((el) => {
          this.cookieService.delete(el.trim(), '/');
        });
        this.sessionService.setToken(null);
        this.sessionService.setTokenExpiration(null);
        this.sessionService.closeSessionChecker();
        this.router.navigateByUrl('/login');
      },
      (error) => {
        console.log('ERORR', error);
      }
    );
  }

  redirectToConfiguration() {
    this.router.navigateByUrl('/dashboard/configuration');
  }

  notifications = () => {
    this.notification.receiveMessage().subscribe(
      (response: any) => {
        if (response.hasOwnProperty('notification') && response.notification.body != null) {
          this.newNotification = true;
          this.contNotifications++;
          this.notification.setNumNotifications({ newNotification: this.newNotification, contNotifications: this.contNotifications, rows: this.arrayNotifications });
          const notification = {
            notification: true,
            textHeader: response.notification.title
          };
          // this.addNotification(response.notification.title, response.notification.body)
          this.toastService.showSuccessNotification(notification, response.notification.body);
        }
      },
      (error) => {

      }
    );
  }

  addNotification = (title: string, body: string) => {
    this.arrayNotifications.push({
      title,
      body
    });
  }

  resetNotofications = () => {
    this.newNotification = false;
    this.contNotifications = 0;
    this.notification.setNumNotifications({ newNotification: this.newNotification, contNotifications: this.contNotifications, rows: this.arrayNotifications });
  }

  reproducir() {
    const audio = new Audio('assets/Hola.mp3');
    audio.play();
  }

  updateCountdown = () => {
    const NOW: any = new Date();
    const DURATION = this.DATE_TARGET - NOW;
    const REMAINING_DAYS = Math.floor(DURATION / this.MILLISECONDS_OF_A_DAY);
    const REMAINING_HOURS = Math.floor((DURATION % this.MILLISECONDS_OF_A_DAY) / this.MILLISECONDS_OF_A_HOUR);
    const REMAINING_MINUTES = Math.floor((DURATION % this.MILLISECONDS_OF_A_HOUR) / this.MILLISECONDS_OF_A_MINUTE);
    const REMAINING_SECONDS = Math.floor((DURATION % this.MILLISECONDS_OF_A_MINUTE) / this.MILLISECONDS_OF_A_SECOND);

    this.SPAN_DAYS = (REMAINING_DAYS > 0) ?
      (REMAINING_DAYS > 10) ? REMAINING_DAYS : '0' + REMAINING_DAYS :
      '00';
    this.SPAN_HOURS = (REMAINING_HOURS > 0) ?
      (REMAINING_HOURS > 10) ? REMAINING_HOURS : '0' + REMAINING_HOURS :
      '00';
    this.SPAN_MINUTES = (REMAINING_MINUTES > 0) ?
      (REMAINING_MINUTES > 10) ? REMAINING_MINUTES : '0' + REMAINING_MINUTES :
      '00';
    this.SPAN_SECONDS = (REMAINING_SECONDS > 0) ?
      (REMAINING_SECONDS > 10) ? REMAINING_SECONDS : '0' + REMAINING_SECONDS :
      '00';

    // console.log('dias', this.SPAN_DAYS + '-', 'horas', REMAINING_HOURS + '-', 'minutos', this.SPAN_MINUTES + '-', 'segundos', this.SPAN_SECONDS);
    if (REMAINING_MINUTES <= 0) {
      this.SPAN_DAYS = '00';
      this.SPAN_HOURS = '00';
      this.SPAN_MINUTES = '00';
      return;
    }
    setTimeout(() => {
      this.updateCountdown();
    }, this.MILLISECONDS_OF_A_SECOND);
  }

  /**
   * Se obtiene la lista de suscripciones para el usuario.
   * De toda la lista hay que validar si hay una suscripción activa (¿o trial?)
   * Si no hay, se tienen que considerar los días de gracia (pay/configuration/FREE_DAYS)
   * y se activa el contador rojo
   * Si sí hay
   */
  getSuscription = () => {

    this._suscription.getSuscription(this.getParams()).subscribe(
      (response: any) => {
        if (response.status == 200 && response.body != null && response.body.length > 0) {
          let cont = 0;
          for (const i of response.body) {
            if (i.status == 'active' || i.status == 'trial') {
              this.lockScreen(false);
              this.DATE_TARGET = moment(this.fechaVencimiento).add(2, 'day');
              this.idSuscription = i.id;
              this._suscription.setIdSuscripcion(this.idSuscription);
              this._suscription.setCurrentCard(i.card.id);
              localStorage.setItem('date', i.chargeDate);
            } else {
              cont++;
              if (cont == response.body.length) {
                this.lockScreen(true);
                this.DATE_TARGET = moment(this.fechaVencimiento);
                this.updateCountdown();
                const day = (true) ? 0 : 2;
                if (this.fechaVencimiento != undefined && String(this.fechaVencimiento) != 'null' && this.fechaVencimiento != '' && moment(this.fechaActual).format('YYYY-MM-DD') > moment(this.fechaVencimiento).add(day, 'day').format('YYYY-MM-DD')) {
                  this.showTimmer = true;
                }
              }
            }
          }

        } else {
          if (0 == response.body.length) {
            this.lockScreen(true);
            this.DATE_TARGET = moment(this.fechaVencimiento);
            this.updateCountdown();
            if (this.fechaVencimiento != undefined && String(this.fechaVencimiento) != 'null' && this.fechaVencimiento != '') {
              this.showTimmer = true;
            }

          }
        }

      },
      (error) => {
        this.toastService.showMessageError(error.message);
      }
    );
  }

  getParams = () => {
    const params = {
      dateFin: moment(this.fechaActual).format('YYYY/MM/DD'),
      dateIni: moment(this.fechaActual).subtract(5, 'y').format('YYYY/MM/DD'),
      idClient: this.userId
    };
    return params;
  }

  deleteSuscription = () => {
    const params = {
      idSuscription: this.idSuscription,
      idClient: this.userId
    };
    this._suscription.deleteSuscription(params, '').subscribe(
      (response) => {
        if (response.status == 200 && (String(response.body).includes('elimino') || response.body == '')) {
          this._suscription.setReactivation(true);
          this.modalServise.dismissAll();
          this.route.navigateByUrl('/start-plan/false');
        } else {
          this.toastService.showMessageError('Error');
          //
        }
      },
      (error) => {
        this.toastService.showMessageError('Error:' + error.error.message);
      }
    );
  }

  inicializarBuscador() {
    if ((this.vacantSearchInput !== null && this.vacantSearchInput !== undefined)) {
      fromEvent(this.vacantSearchInput.nativeElement, 'keyup').pipe(
        map((event: any) => {
          return event.target.value;
        })
        , debounceTime(600)
        , distinctUntilChanged()
      ).subscribe((text: string) => {
        this.spreadVacantSearchInput.emit(text);
      });
    }
  }


  lockScreen = (cancel: boolean) => {
    const day = (cancel) ? 0 : 2;
    if (moment(this.fechaActual).format('YYYY-MM-DD') > moment(this.fechaVencimiento).add(day, 'day').format('YYYY-MM-DD')) {
      if (!this._suscription.getExecutemodal()) {
        this._suscription.setExecutemodal(true);
        this._suscription.isSubcriptionActive = false;
        const modalRef = this.modalServise.open(ModalExpirationComponent, { centered: true, size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.dataTem.subscribe((data: any) => {
          this.deleteSuscription();
        });
      }
    }
    else {
      this._suscription.isSubcriptionActive = true;
    }
  }

  eventEmit = (event: any) => {
    this.eventClick.emit(event);
  }

  Show_Menu_User() {
    this.Show_menu_user = !this.Show_menu_user;
  }
  Show_Menu_notification() {
    this.Show_menu_notification = !this.Show_menu_notification;
  }

  redirectoToPlan() {
    this.router.navigateByUrl('/start-plan/false/true');
  }
}
