import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BUTTONS_USER_CARD } from 'src/app/enums/keys-buttons.enum';
import { SidenavVacantesComponent } from 'src/app/modulos/dashboardReclutadores/sidenav-vacantes/sidenav-vacantes.component';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { CandidatesService } from 'src/app/services/reclutador/vacantes/selectionProcess/candidatos.service';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {

  BUTTONS = BUTTONS_USER_CARD;
  @Input() userCardData: any;
  @Input() showIcons = true;
  @Input() showPercent = false;
  @Input() basicIcons = true;
  @Input() flagMatch: boolean;
  @Input() setProcessSelection: any;



  @Output() eventModal: EventEmitter<any> = new EventEmitter;
  @Output() eventClick: EventEmitter<any> = new EventEmitter;
  @ViewChild('rechazar', { static: true }) modalRechazar!: TemplateRef<any>;
  @ViewChild('confirmacionRechazo', { static: true }) confirmacionRechazo!: TemplateRef<any>;
  @ViewChild('aprobar', { static: true }) aprobar!: TemplateRef<any>;
  @ViewChild('confirmacionAprobacion', { static: true }) confirmacionAprobacion!: TemplateRef<any>;
  @ViewChild("selector1") updateCounters!: SidenavVacantesComponent;
  @ViewChild('iconoChekBlanco', { static: true }) iconoChekBlanco!: TemplateRef<any>;
  flagPagination: boolean = false;
  selectionProcessId: string;
  banderaCerrarVacante: boolean;
  rejectFlag: boolean;
  approveSelected: boolean;
  nextFlag: boolean;
  banderaAprobar: boolean;
  selecAllinvitation: boolean;
  rejectSelected: boolean;
  shortListActive: boolean = false;
  rows: Array<any> = [];
  contadorChecks = 0;
  flagService: boolean = false;
  page_number: number = 0;
  page_size: number = 9;
  sortBy: string = "&sortBy=lastUpdate";
  mensajeToast: string = "";

  constructor(
    private modalService: NgbModal,
    private _candidates: CandidatesService,
    private toastService: ToastService,
    private router: Router
    ) { }



  ngOnInit(): void {
  }

  onClick = (event: any) => {
    this.eventClick.emit(event);
  }

  openModal = (event: any) => {
    this.eventModal.emit(event);
  }
  openModalRechazar(id: string, rejectSelected: boolean) {
    this.selectionProcessId = id;
    this.rejectSelected = rejectSelected;
    this.modalService.open(this.modalRechazar, { centered: true });
  }

  openModalconfirmacionRechazoCandidato(bandera: boolean, rejectFlag: boolean) {
    this.banderaCerrarVacante = bandera;
    this.rejectFlag = rejectFlag;
    this.modalService.open(this.confirmacionRechazo, { centered: true });
  }

  openModalAprobar(id: string, approveSelected: boolean) {
    this.selectionProcessId = id;
    this.approveSelected = approveSelected;
    this.modalService.open(this.aprobar, { centered: true });
  }

  openModalconfirmacionAprobacion(bandera: boolean, nextFlag: boolean) {
    this.nextFlag = nextFlag;
    this.banderaAprobar = bandera;
    this.modalService.open(this.confirmacionAprobacion, { centered: true });
  }
  generateArrayNextStage(flag: boolean, template: any) {
    let arrayProcessId = []
    if (this.approveSelected) {
      for (let card of this.rows) {
        if (card.check) {
          arrayProcessId.push(card.id)
        }
      }
    } else {
      arrayProcessId.push(this.selectionProcessId)
    }
    if (arrayProcessId.length == 1) {
      this.approveSelected = false
    }
    this.nextStage(template, arrayProcessId, flag, this.approveSelected)
  }
  nextStage = (template: any, arrayCandidates: any, flagNotification: boolean, flagPlural: boolean) => {
    this._candidates.nextStage(flagNotification, arrayCandidates).subscribe(
      (response: any) => {
        if (response != null && response.status == 200) {
          // redirreccionar al siguiente proceso 
          this.contadorChecks = 0
          if (flagNotification) {
            this.setMensajeNotificacionFelicitacion(template, flagPlural)
          } else {
            this.setMensajeNotificacion(template, flagPlural)
          }
        } else {
          this.showMessageError("Ocurrio un error");
        }

      },
      (error) => {
        this.showMessageError("Ocurrio un error");
      }
    );
  }
  
  setMensajeNotificacionFelicitacion(template: any, flagPlural: boolean) {
    this.mensajeToast = (flagPlural) ? "Enviamos un mensaje de felicitación a los candidatos" : "Enviamos un mensaje de felicitación al candidato"
    this.showSuccess(template);
  }
  setMensajeNotificacion(template: any, flagPlural: boolean) {
    this.mensajeToast = (flagPlural) ? "Por ahora los candidatos no serán notificados de tu decisión" : "Por ahora el candidato no será notificado de tu decisión"
    this.showSuccess(template);
  }
  showMessageError(dangerTpl: any) {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-light ngb-toastsBottom', delay: 8000 });
  }
  generateArrayRejectCandidate(flag: boolean, template: any) {
    let arrayProcessId = []
    if (flag || this.rejectSelected) {
      flag = this.rejectSelected
      for (let card of this.rows) {
        if (card.check) {
          arrayProcessId.push(card.id)
        }
      }
    } else {
      arrayProcessId.push(this.selectionProcessId)
    }
    if (arrayProcessId.length == 1) {
      flag = false
    }
    this.rejectCandidate(template, arrayProcessId, flag)
  }

  rejectCandidate = (template: any, arrayCandidates: any, flagNotification: boolean) => {
    this._candidates.rejectCandidate(this.rejectFlag, arrayCandidates).subscribe(
      (response: any) => {
        if (response != null && response.status == 200) {
          // redirrecionamiento 
          this.setMensajeToast(template, flagNotification)
        } else {
          this.showMessageError("Ocurrio un error");
        }

      },
      (error) => {
        this.showMessageError("Ocurrio un error");
      }
    );
  }

  setMensajeToastAprobar(template: any) {
    if (this.banderaAprobar) {
      this.mensajeToast = "Enviamos un mensaje de felicitación al candidato"
    } else {
      this.mensajeToast = "Por ahora el candidato no será notificado de tu decisión"
    }
    this.showSuccess(template);
  }
  
  setMensajeToast(template: any, flagPlural: boolean) {
    if (this.banderaCerrarVacante) {
      this.mensajeToast = (flagPlural) ? "Por ahora los candidatos no serán notificados de tu decisión" : "Por ahora el candidato no será notificado de tu decisión"
    } else {
      this.mensajeToast = (flagPlural) ? "Enviamos un mensaje  de agredecimiento a los candidatos" : "Enviamos un mensaje  de agredecimiento al candidato"
    }
    this.showSuccess(template);
  }
  showSuccess(template: any) {
    this.toastService.remove(1)
    this.toastService.show(this.mensajeToast, { classname: 'bg-success text-light ngb-toastsBottom', delay: 5000 });
    if(this.userCardData.data.status=="NUEVO"){
      this.router.navigateByUrl('/dashboard/proceso/seleccion/candidatos/false');

    }else if(this.userCardData.data.status=="PRUEBAS_Y_VIDEO"){
      this.router.navigateByUrl('/dashboard/proceso/seleccion/pruebas-video/false');

    }else if(this.userCardData.data.status=="FINALISTA"){
      this.router.navigateByUrl('/dashboard/proceso/seleccion/finalistas/false');

    }
  }
}
