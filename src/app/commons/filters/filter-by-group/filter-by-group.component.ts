import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-filter-by-group',
  templateUrl: './filter-by-group.component.html',
  styleUrls: ['./filter-by-group.component.scss']
})
export class FilterByGroupComponent implements OnInit {
  @Input() data: any;

  recruitersData: any;
  groupOfFavorites: boolean;
  recruiterSelected: string;
  sortDirection = 'ASC';
  pageNumber = 0;
  pageSize = 15;

  constructor(
    private readonly apiService: ApiService
  ) { }

  ngOnInit(): void {
  }



  findGroupByRecruiter = () => {
    this.data = [];
    if (this.recruiterSelected) {
      this.apiService.get(`/list-favorite/page?recruiterId=${this.recruiterSelected}`).subscribe((resp: any) => {
        this.data = resp.content;
      }, (error) => {
        console.log(error);
      });
    }
  }

  findGroupByDate = () => {
    this.data = [];
    if (this.sortDirection) {
      this.apiService.get(`/list-favorite/page?sortDirection=${this.sortDirection}&sortBy=creationDate`).subscribe((resp: any) => {
        this.data = resp.content;
      }, (error) => {
        console.log(error);
      });
    }
  }


  showGroupByItems = () => {
    this.data = [];
    if (this.pageSize) {
      this.apiService.get(`/list-favorite/page?pageSize=${this.pageSize}`).subscribe((resp: any) => {
        this.data = resp.content;
      }, (error) => {
        console.log(error);
      });
    }
  }




}
