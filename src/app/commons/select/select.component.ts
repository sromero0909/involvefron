import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {
  @Input() arrayObjetos:Array<any>;
  respuesta: String ="";
  @Input() mb="mb-1"
  @Input() textoParrafo="";
  @Input() placeHolder ="Seleccionar"

  constructor() {
    this.arrayObjetos = []

   }

  ngOnInit(): void {
  }

}
