import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CurrencyPipe, DecimalPipe } from '@angular/common';
import { NgDragDropModule } from 'ng-drag-drop';

import { SelectComponent } from '../commons/select/select.component';
import { UnknownErrorComponent } from './unknown-error/unknown-error.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { ModalComponent } from './modal/modal.component';
import { ToastsContainer } from './toast/toast.component';
import { NgbdSortableHeader } from '../directives/sortable.directive';
import { InterceptorLoaderService } from '../services/commons/interceptor-loader.service';
import { NotificationsAndPhotoComponent } from './notifications-and-photo/notifications-and-photo.component';
import { InputSearchAutocompliteComponent } from './input-search-autocomplite/input-search-autocomplite.component';
import { InputSearchComponent } from './input-search/input-search-gneric.component';
import { LoaderImageComponent } from './loader-image/loader-image.component';
import { NoticeofPrivacyComponent } from './modals/noticeof-privacy/noticeof-privacy.component';
import { PushNotificationsService } from '../services/commons/push-notifications.service';
import { CurrencyOnlyDirective } from '../directives/currency.directive';
import { CardsComponent } from './cards/cards.component';
import { MaterialModule } from '../modulos/material/material.module';
import { FilterByGroupComponent } from './filters/filter-by-group/filter-by-group.component';
import { TruncatePipe } from '../pipes/truncate/truncate.pipe';
import { InternalizacionModule } from 'src/app/commons/internalizacion/internalizacion.module';
import { TextInputTrimDirective } from '../directives/remove-spaces.directive';
import { LoadingComponent } from './loading/loading.component';
import { CustomCurrencyPipe } from './custom-currency/custom-currency.pipe';
import { MainNavbarComponent } from './main-navbar/main-navbar.component';
import { UserCardComponent } from './user-card/user-card.component';
import { ChangelogComponent } from './changelog/changelog.component';
import { DateAgoPipe } from '../pipes/date-ago/date-ago.pipe';
import { NoPermissionCombineComponent } from '../modulos/dashboardReclutadores/talent/candidate-finder/favorites/modal-favorite/no-permission-combine/no-permission-combine.component';
import { ModalPrivacyComponent } from '../auth/components/modal/modal-privacy/modal-privacy.component';
import { ModaltermsComponent } from '../auth/components/modal/modalterms/modalterms.component';
import { LoginComponent } from '../auth/pages/login/login.component';



const DECLARATIONS_EXPORTS = [
  NoPermissionCombineComponent,
  SelectComponent,
  ProgressBarComponent,
  UnknownErrorComponent,
  ModalComponent,
  ToastsContainer,
  NgbdSortableHeader,
  NotificationsAndPhotoComponent,
  InputSearchAutocompliteComponent,
  InputSearchComponent,
  LoaderImageComponent,
  NoticeofPrivacyComponent,
  CurrencyOnlyDirective,
  CardsComponent,
  FilterByGroupComponent,
  TextInputTrimDirective,
  LoadingComponent,
  MainNavbarComponent,
  UserCardComponent,
  CustomCurrencyPipe,
  TruncatePipe,
  DateAgoPipe,
  ModalPrivacyComponent,
  ModaltermsComponent,
  LoginComponent
]
@NgModule({
  declarations: [
    DECLARATIONS_EXPORTS,
    ChangelogComponent,
  ],
  exports: [
    DECLARATIONS_EXPORTS
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    InternalizacionModule,
    NgDragDropModule.forRoot()
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorLoaderService, multi: true },
    PushNotificationsService,
    CurrencyPipe,
    DecimalPipe,
  ],
})
export class moduleSharedModule { }
