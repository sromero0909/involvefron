import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { animate, keyframes, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-loader-image',
  templateUrl: './loader-image.component.html',
  styleUrls: ['./loader-image.component.scss'],
  animations: [
    trigger('imageAnimation', [
      state('show-image', style({
        display: '',
      })),
      state('hide-image', style({
        display: 'none'
      })),
      transition('show-image <=> hide-image', animate('1000ms ease-in')),
    ])
  ]
})
export class LoaderImageComponent implements OnInit {

  imageCtrl: string = 'hide-image';
  contentCtrl: string = 'show-image';

  @Input() height:string = "150";
  @Input() width:string = "150" 
  @Input() class:string = "rounded-circle"
  @Input() sinClases:boolean = false 
  @Input('url') set url(url: string) {
    if (url) {
      setTimeout(() => {
        this.loadImage(url);
      }, 100);
      
    }
  }
  @ViewChild('lImage') lImage!: ElementRef;

  constructor() { }

  ngOnInit() {
    
  }

  ngAfterViewInit() {
    this.lImage.nativeElement.onload = () => {
      this.imageCtrl = 'show-image';
      this.contentCtrl = 'hide-image';
    }
  }

  loadImage = (urlImage: any) => {
    this.imageCtrl = 'hide-image';
    this.contentCtrl = 'show-image';
    this.lImage.nativeElement.src = urlImage;
  }
}