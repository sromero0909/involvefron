import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private currentToken: string | null = null;
  private currentTokenExpiration: number | null = null;
  private currentUser: any | null = null;
  private currentRecruiterId: string | null = null;

  isActiveSessionChecker = true;
  sessionCheckerTimer: number | null = null;
  constructor(private http: HttpClient) { }

  /**
   * Setter
   * @param recruiterId based on service response
   * @returns `true` if property was set
   */
   serRecruiterId(recruiterId: string): boolean{
    try{
      this.currentRecruiterId = recruiterId;
      localStorage.setItem('rId', recruiterId);
      return true;
    }
    catch {
      return false;
    }
  }

  /**
   * Setter
   * @param token based on service response
   * @returns `true` if property was set
   */
   setToken(token: string | null): boolean{
    try{
      this.currentToken = token;
      if (token === null){
        localStorage.removeItem('t');
      }
      else{
        localStorage.setItem('t', token);
      }
      return true;
    }
    catch {
      return false;
    }
  }

  /**
   * Setter
   * @param tokenExpiration based on service response
   * @returns `true` if property was set
   */
   setTokenExpiration(tokenExpiration: number | null): boolean{
    try{
      if (tokenExpiration === null){
        localStorage.removeItem('e');
      }
      else{
        this.currentTokenExpiration = tokenExpiration;
        // this.currentTokenExpiration = new Date().getTime() + 20 * 1000;
        localStorage.setItem('e', this.currentTokenExpiration.toString());
      }
      return true;
    }
    catch {
      return false;
    }
  }

  /**
   * Setter
   * @param user based on service response
   * @returns `true` if property was set
   */
   setUser(user: any): boolean{
    try{
      this.currentUser = user;
      return true;
    }
    catch {
      return false;
    }
  }

  /**
   * Getter
   * @returns current token
   */
   getToken(): string | null{
    return this.currentToken;
  }

  /**
   * Getter
   * @returns current token expiration
   */
  getTokenExpiration(): number | null{
    return this.currentTokenExpiration;
  }

  /**
   * Getter
   * @returns current user
   */
  getUser(): any{
    return this.currentUser;
  }

  /**
   * Determines if the current session is valid
   * @returns `true` if it exists a token and it is valid
   * `false` otherwise
   */
   isTokenActive(): boolean{
    if (this.currentTokenExpiration === null &&
        localStorage.getItem('e') &&
        this.currentToken === null &&
        localStorage.getItem('t')){
      this.currentTokenExpiration = Number(localStorage.getItem('e'));
      this.currentToken = localStorage.getItem('t');
      this.currentRecruiterId = localStorage.getItem('rId');
      this.restoreSessionData();
    }
    const currentTime = new Date().getTime();
    return this.currentToken !== null&& (this.currentTokenExpiration || 0) >= currentTime;
  }

  /**
   * Checks every 10 seconds if the current token is still valid. If not, displays the login page
   * in order to refresh the data.
   *
   * This method is triggered when recovering the data session from `authService.restoreSessionData()` or
   * when the user logs in.
   */
   startSessionChecker(): void{
    this.isActiveSessionChecker = true;
    if (this.sessionCheckerTimer !== null){
      window.clearInterval(this.sessionCheckerTimer);
      this.sessionCheckerTimer = null;
    }
    this.sessionCheckerTimer = window.setInterval(
      () => {
        const remainingTime = ((this.currentTokenExpiration || 0) - new Date().getTime()) / 1000;
        if (remainingTime <= 0){
          this.closeSessionChecker();
        }
      },
      // TODO: Definir el tiempo de refresco
      5 * 60 * 1_000 // 5 minutes
    );
  }

  /**
   * Recover `currentCompany` and `currentUser` properties from a service
   * based on the saved `token` and `recruiterId`
   */
   restoreSessionData(): void{
    if (this.currentUser === null){
      this.http.get(`${environment.restPrefix}/recruiter/${this.currentRecruiterId}`, { observe: 'response'}).subscribe(
        (successResponse: any) => {
          this.currentUser = successResponse.body.user;
          if (this.sessionCheckerTimer === null){
            this.startSessionChecker();
          }
        },
        (errorResponse: any) => {
          console.error('restoreSessionData()', errorResponse);
        }
      );
    }
  }

  closeSessionChecker(): void{
    if (this.sessionCheckerTimer !== null){
      window.clearInterval(this.sessionCheckerTimer);
    }
    this.isActiveSessionChecker = false;
  }
}
