import { formatDate } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';


import * as moment from 'moment';
import 'moment/locale/es';
import { CookieService } from 'ngx-cookie-service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { ProfileService } from 'src/app/services/reclutador/configuration/profile.service';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { PaymentGatewayService } from 'src/app/services/reclutador/login/payment-gateway.service';
import { SuscriptionCanceledService } from 'src/app/services/reclutador/login/subscription-canceled.service';

const CURRENT_PAGE_STEP = 5;
@Component({
  selector: 'app-standby-screen',
  templateUrl: './standby-screen.component.html',
  styleUrls: ['./standby-screen.component.scss']
})
export class StandbyScreenComponent implements OnInit {

  loading = true;
  selectedPlan = '';
  dateBilling: any;
  statusStep = 5;

  @Input() isAtPayment = false;

  constructor(private route: Router,
              private activatedRoute: ActivatedRoute,
              private _payment: PaymentGatewayService,
              private traslateService: TranslateService,
              private _suscription: SuscriptionService,
              private _serviceGlobal: SuscriptionCanceledService,
              private cookieService: CookieService,
              private profileService: ProfileService,
              private toast: ToastService, ) { }

  ngOnInit(): void {
    const currentRecruiterId = JSON.parse(this.cookieService.get('user')).recruiterId;
    const previousExpirationDate = JSON.parse(this.cookieService.get('user')).user.suscriptionExpires;
    this.profileService.getRecruiter(currentRecruiterId).subscribe(
      (successResponse: any) => {
        const updatedExpirationDate = successResponse.user.suscriptionExpires;
        this._serviceGlobal.setDateExpiration(updatedExpirationDate);
        this.getDate();
      },
      (errorResponse: any) => {
        console.error(errorResponse);
      }
    );
    const flagRoute = this.activatedRoute.snapshot.paramMap.get('flag');
    this.loading = (flagRoute === 'false') ? false : true;
    if (this.loading && !this.isAtPayment){
      this._suscription.validateCurrentStatus(CURRENT_PAGE_STEP);
    }

    this.selectedPlan = this._serviceGlobal.getSummary()?.name;

    this.getDate();
  }

  getDate = async () => {
    moment.locale(this.traslateService.getDefaultLang());
    let tranlation1 = '';
    await this.traslateService.get(['configuration.subscription.of']).toPromise().then(
      (tranlation) => {
        tranlation1 = tranlation['configuration.subscription.of'];
      }
    );
    const date = this._serviceGlobal.getDateExpiration();
    let fecha;
    if ( date != 'null' && date != ''){
      fecha = moment(formatDate(String(date), 'yyyy-MM-dd', 'en_US'));
    }
    else{
      fecha = moment(formatDate(String('2022-01-31'), 'yyyy-MM-dd', 'en_US'));
    }
    this.dateBilling = fecha.format('DD-MMMM-YYYY');
    const dateTemp = String(this.dateBilling).split('-');
    this.dateBilling = dateTemp[0] + ' ' + tranlation1 + ' ' + dateTemp[1] + ' ' + tranlation1 + ' ' + dateTemp[2];
  }

  redirectToCompany = () => {
    const params = [{
      op: 'replace',
      path: '/stepsStatus',
      value: CURRENT_PAGE_STEP
    }];
    this.profileService.updateRecruiterPatch(params).subscribe(
      (successResponse: any) => {
        if (successResponse.status === 200 && successResponse.body !== null && !successResponse.body.hasOwnProperty('error')){
          this._suscription.setlastCompletedStep(successResponse.body.stepsStatus);
          this._suscription.redirectToCorrectScreen(successResponse.body.stepsStatus);
        }
      },
      (errorResponse: any) => {
        this.toast.showMessageError(errorResponse.error);
      }
    );
  }

}
