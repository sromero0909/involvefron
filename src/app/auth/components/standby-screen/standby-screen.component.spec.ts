import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StandbyScreenComponent } from './standby-screen.component';

describe('StandbyScreenComponent', () => {
  let component: StandbyScreenComponent;
  let fixture: ComponentFixture<StandbyScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StandbyScreenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StandbyScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
