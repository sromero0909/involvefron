import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/services/auth.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { PaymentGatewayService } from 'src/app/services/reclutador/login/payment-gateway.service';
import { environment } from 'src/environments/environment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { SuscriptionCanceledService } from 'src/app/services/reclutador/login/subscription-canceled.service';


declare var $: any;
declare var OpenPay: any;

@Component({
  selector: 'app-set-up-payment',
  templateUrl: './set-up-payment.component.html',
  styleUrls: ['./set-up-payment.component.scss'],
})
export class SetUpPaymentComponent implements OnInit {

  @Input() cols = 12;
  PK_OPENPAY = '';
  arrayErrors: any;
  url = `${environment.restPrefixPay}/card`;
  cardFailedAttempts = 0;
  displaysErrorNotification = false;
  glowsErrorNotification = false;
  constructor(public toastService: ToastService,
              private authService: AuthService,
              private _payment: PaymentGatewayService,
              private translate: TranslateService,
              private _suscription: SuscriptionService,
              private modalService: NgbModal,
              private _serviceGlobal: SuscriptionCanceledService) {
    translate.get('login.errors').subscribe(res => {
      localStorage.setItem('errorsCode', JSON.stringify(res));
    });

  }

  ngOnInit(): void {
    if (this.cardFailedAttempts >= 3){
      this.displaysErrorNotification = true;
    }
    $('#payment-form').on('submit', function(evt: any){
      evt.preventDefault();
   });
    $('#points-yes-button').on('click', function(evt: any) {
    evt.preventDefault();
   });
    $('#points-no-button').on('click', function(evt: any) {
    evt.preventDefault();
  });

    if (localStorage.getItem('execute') == 'yes'){
      $('#modalPromociones').modal('show');
    }else{
      $('#modalPromociones').modal('hide');
      $('#numColumns').val(this.cols);
      this.getKeysOpenPay('PK_OPENPAY', true);
      const planId = (this._serviceGlobal.getSummary() != undefined && this._serviceGlobal.getSummary().descuento == 0) ? this._serviceGlobal.getSummary().id : this._serviceGlobal.getIdPlan();
      $('#planId').val(planId);
      $('#clientId').val(JSON.parse(this.authService.getUser()).user.userOpenPay);
      const token = this.authService.getToken();
      // $('#numColumns').val(this.cols)
      localStorage.setItem('planId', planId);
      localStorage.setItem('clientId', (JSON.parse(this.authService.getUser()).user.userOpenPay));
    }

  }

  ngOnDestroy() {
    localStorage.removeItem('PK_OPENPAY');
    localStorage.removeItem('ID_OPENPAY');
    localStorage.removeItem('errorsCode');
    localStorage.removeItem('planId');
    localStorage.removeItem('clientId');
    localStorage.removeItem('statusTemp');
    localStorage.removeItem('execute');
    $('#modalPromociones').modal('hide');
  }


  initialiceForm = () => {
    const _this = this;
    $(document).ready(function() {
      let ejecuto = false;
      const cols = $('#numColumns').val();
      (cols == 12) ? $('#btnAdd').css('display', 'none') : $('#btnPay').css('display', 'none');
      const service = (cols == 12) ? 'pay/cardNew' : 'card';
      const step = (cols == 12) ? 4 : 8;
      OpenPay.setId(localStorage.getItem('ID_OPENPAY'));
      OpenPay.setApiKey(localStorage.getItem('PK_OPENPAY'));
      // ! Se estable el valor a false para producción. Este valor no debe ser modificado en este ambiente
      OpenPay.setSandboxMode(false);
      const deviceSessionId = OpenPay.deviceData.setup('payment-form', 'deviceIdHiddenFieldName');
      $('#pay-button').on('click', function(event: any) {
        if (_this.cardFailedAttempts < 3){
          if ($('#nameT').val().trim() !== '' &&
          $('#numTarget').val().trim() !== '' &&
          $('#month').val().trim().length === 2 &&
          $('#anio').val().trim().length === 2){
            ejecuto = false;
            localStorage.setItem('execute', 'yes');
            $('#modalPromociones').modal('show');
            event.preventDefault();
            $('#pay-button').prop('disabled', true);
            OpenPay.token.extractFormAndCreate('payment-form', sucess_callbak, error_callbak);
          }
          else{
            $('#textToast').text('Completa el formulario antes de continuar');
            _this.displayErrorNotification();
          }
        }
        else{
          _this.displayErrorNotification();
        }
      });

      let sucess_callbak = function(response: any) {
        let token_id = response.data.id;
        $('#token_id').val(token_id);
        $('#payment-form').submit();
      };

      $('form').on('submit', function(event: any) {
        if (!ejecuto && _this.cardFailedAttempts < 3) {
          ejecuto = true;
          localStorage.setItem('execute', 'yes');
          const form = $('form');
          event.preventDefault();
          $.ajax({
            method: 'POST', url: `${environment.restPrefixPay}/${service}`,
            data: form.serialize(),
            contentType: 'application/json',
            dataType: 'text',
            success(response: any, textStatus: any, request: any) {
              if (request.status == 200) {
                $('#modalPromociones').modal('hide');
                const currentUser = JSON.parse(_this.authService.getUser());
                currentUser.user.attemptsCard = 0;
                _this.authService.setUser(JSON.stringify(currentUser));
                if (cols == 12) {
                  $.ajax({
                    method: 'PATCH',
                    url: `${environment.restPrefix}/recruiter`,
                    headers: {
                      Authorization: 'Bearer ' + localStorage.getItem('token')?.replace('Bearer ', ''),
                      Accept: 'application/json'
                    },
                    data: params(),
                    contentType: 'application/json',
                    success(patchResponse: any, patchTextStatus: any, patchRequest: any){
                      _this._suscription.setlastCompletedStep(patchResponse.stepsStatus);
                    },
                    complete(){
                      $('#btn_next').trigger('click');
                    }
                  });
                } else {
                  $('#btn_closeModal').trigger('click');
                }
              } else {
                $('#modalPromociones').modal('hide');
                $('#textToast').text('Se declinó la tarjeta ¿Probamos con otra?');
                _this.displayErrorNotification();
              }
              localStorage.setItem('execute', 'no');
            },
            error(error: any) {
              if (error.status === 200 && error.statusText === 'parsererror'){
                // TODO: Validar por qué no entra en el success. Debe ser por contentType
                localStorage.setItem('execute', 'no');
                localStorage.removeItem('aboutToExpireSeen');
                window.location = error.responseText;
              }
              else{
                localStorage.setItem('execute', 'no');
                $('#modalPromociones').modal('hide');
                if (error.responseText === 'Se alcanzo un límite de tres tarjetas a registrar'){
                  _this.modalService.dismissAll({
                    carsLimitExceeded: true
                  });
                }
                else{
                  _this.handleWrongCardError();
                  $('#textToast').text('Se declinó la tarjeta ¿Probamos con otra?');
                  _this.displayErrorNotification();
                }
              }
            }
          });
        }
      });

      const params = function() {
        const params = [
          {
            op: 'replace',
            path: '/stepsStatus',
            value: 4
          }
        ];

        return JSON.stringify(params);
      };

      const showErrors = (code: number) => {
        // Default message required by OpenPay certification
        return 'Se declinó la tarjeta ¿Probamos con otra?';
      };

      let error_callbak = function(response: any) {
        localStorage.setItem('execute', 'no');
        $('#modalPromociones').modal('hide');
        const desc = response.data.description != undefined ? response.data.error_code : response.message;
        $('#textToast').text(showErrors(desc));
        $('#pay-button').prop('disabled', false);
        _this.handleWrongCardError();
      };

      $('#points-yes-button').on('click', function() {
        $('#use_card_points').val('true');
        $('#payment-form').submit();
      });

      $('#points-no-button').on('click', function() {
        $('#use_card_points').val('false');
        $('#payment-form').submit();
      });


      const myModal = document.getElementById('exampleModal');
      const myInput = document.getElementById('myInput');

      myModal?.addEventListener('shown.bs.modal', function() {
        myInput?.focus();
      });

      $('.nameT').on('keypress', function(event: any) {
        const regex = /^[a-zA-Z ]+$/;
        return regex.test(event.key);
      });

      $('.numTarjeta').on('keypress', function(event: any) {
        const regex = /^\d*$/;
        return regex.test(event.key);
      });
      const regexNUmber = /^\d*$/;

      $('.nameT').on('focusout', function(event: any) {
        const regex = /^[a-zA-Z ]+$/;
        (regex.test($('#nameT').val())) ? $('#nameT').val() : $('#nameT').val('');
      });

      $('#numTarget').on('focusout', function(event: any) {
        (regexNUmber.test($('#numTarget').val())) ? $('#numTarget').val() : $('#numTarget').val('');
      });

      $('#anio').on('focusout', function(event: any) {
        (regexNUmber.test($('#anio').val())) ? $('#anio').val() : $('#anio').val('');
      });

      $('#month').on('focusout', function(event: any) {
        (regexNUmber.test($('#month').val())) ? $('#month').val() : $('#month').val('');
      });

      $('#ccv').on('focusout', function(event: any) {
        (regexNUmber.test($('#ccv').val())) ? $('#ccv').val() : $('#ccv').val('');
      });

      $('[data-toggle="tooltip"]').tooltip();


    });
  }

  getKeysOpenPay = (code: string, execute: boolean) => {
    this._payment.getKeysOpenPay(code).subscribe(
      (response: any) => {
        if (response.status == 200 && response.body != null) {
          if (code == 'PK_OPENPAY') {
            localStorage.setItem('PK_OPENPAY', response.body.value);
          } else {
            localStorage.setItem('ID_OPENPAY', response.body.value);
          }
          if (execute) {
            this.getKeysOpenPay('ID_OPENPAY', false);
          } else {
            this.initialiceForm();
          }
        }
      },
      (error) => {
        this.toastService.showMessageError(error.error);
      }
    );
  }

  closeModal = () => {
    this.modalService.dismissAll({
      success: true
    });
  }

  handleWrongCardError(){
    if (this.cardFailedAttempts < 3){
      this._suscription.increaseFailedCardAttempts(JSON.parse(this.authService.getUser()).user.userId).subscribe(
        (successResponse: any) => {
          if (successResponse === 'Se agrego un intento'){
            this.cardFailedAttempts += 1;
            const currentUser = JSON.parse(this.authService.getUser());
            currentUser.user.attemptsCard = this.cardFailedAttempts;
            this.authService.setUser(JSON.stringify(currentUser));
            this.displayErrorNotification();
          }
        },
        (errorResponse: any) => {
          console.error(errorResponse);
        }
      );
    }
    else{
      this.displayErrorNotification();
    }
  }

  displayErrorNotification(){
    this.displaysErrorNotification = true;
    if (this.cardFailedAttempts < 3){
      setTimeout(() => {
        this.displaysErrorNotification = false;
      }, 4000);
    }
    else{
      this.glowsErrorNotification = true;
      setTimeout(() => {
        this.glowsErrorNotification = false;
      }, 4000);
    }
  }

  onBtnNextclick(): void{
    this._suscription.redirectToCorrectScreen(4);
  }
}

