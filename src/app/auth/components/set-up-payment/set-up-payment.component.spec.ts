import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetUpPaymentComponent } from './set-up-payment.component';

describe('SetUpPaymentComponent', () => {
  let component: SetUpPaymentComponent;
  let fixture: ComponentFixture<SetUpPaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetUpPaymentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetUpPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
