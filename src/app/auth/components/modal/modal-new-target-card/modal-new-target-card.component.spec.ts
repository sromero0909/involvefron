import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalNewTargetCardComponent } from './modal-new-target-card.component';

describe('ModalNewTargetCardComponent', () => {
  let component: ModalNewTargetCardComponent;
  let fixture: ComponentFixture<ModalNewTargetCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalNewTargetCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalNewTargetCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
