import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-new-target-card',
  templateUrl: './modal-new-target-card.component.html',
  styleUrls: ['./modal-new-target-card.component.scss']
})
export class ModalNewTargetCardComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  closeModal = () => {
    this.modalService.dismissAll();
  }

}
