import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-cookies',
  templateUrl: './modal-cookies.component.html',
  styleUrls: ['./modal-cookies.component.scss']
})
export class ModalCookiesComponent implements OnInit {

  closeResult!: string;
  @Output() btnAcept = new EventEmitter<boolean>();
  @Input() isModal = false;

  constructor(private modalService: NgbModal) {}

  ngOnInit(): void {
  }

  openScrollableContent(longContent: any) {
    this.modalService.open(longContent, { scrollable: true, size: 'lg' });
  }

  closeModal = (response: boolean) => {
    this.btnAcept.emit(response);
    this.modalService.dismissAll();
  }
}
