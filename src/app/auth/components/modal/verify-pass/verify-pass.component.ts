import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ProfileService } from 'src/app/services/reclutador/configuration/profile.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from 'src/app/services/commons/toast.setvice';


@Component({
  selector: 'app-verify-pass',
  templateUrl: './verify-pass.component.html',
  styleUrls: ['./verify-pass.component.scss']
})
export class ModalVerifyPassComponent implements OnInit {


  formEmail!: FormGroup;
  showPassword: boolean = false;

  constructor(private formBuilder: FormBuilder,
    private profileService: ProfileService,
    private toastService: ToastService,
    private modalService: NgbModal,

    ) { }

  ngOnInit(): void {
    this.initializeFormPassWord();
  }

  initializeFormPassWord = () => {
    this.formEmail = this.formBuilder.group({
      passwordEmail: [{ value: '', disabled: false }, [Validators.required]],
    });
  }

  verifyPassWord = () => {
    this.profileService.verifyPassWord(this.formEmail.get('passwordEmail')?.value).subscribe(
      (response: any) => {
        if (response != null) {
          if (response.code == 471) {
            this.toastService.showMessageError('La contraseña es incorrecta')
          } else {
            this.modalService.dismissAll('ok')
          }
        }
      },
      (error) => {
  
      })
  }
}
