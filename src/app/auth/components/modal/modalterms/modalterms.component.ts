import { Component, OnInit,Output,EventEmitter, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modalterms',
  templateUrl: './modalterms.component.html',
  styleUrls: ['./modalterms.component.scss']
})
export class ModaltermsComponent {

  closeResult!: string;
  @Output() btnAcept = new EventEmitter<boolean>();
  @Input() isModal = false;
  @Input() isHeader = false;
  

  constructor(private modalService: NgbModal) {}

  openBackDropCustomClass(content: any) {
    this.modalService.open(content, {backdropClass: 'light-blue-backdrop'});
  }

  openWindowCustomClass(content: any) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }

  openSm(content: any) {
    this.modalService.open(content, { size: 'sm' });
  }

  openLg(content: any) {
    this.modalService.open(content, { size: 'lg' });
  }

  openXl(content:any ) {
    this.modalService.open(content, { size: 'xl' });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  openScrollableContent(longContent:any) {
    this.modalService.open(longContent, { scrollable: true, size: 'lg' });
  }

  openModalDialogCustomClass(content:any) {
    this.modalService.open(content, { modalDialogClass: 'dark-modal' });
  }

  closeModal = (response:boolean) => {
    this.btnAcept.emit(response)
    this.modalService.dismissAll();
  }
  closeModalHeader = (response:boolean) => {
    if(response == true){
      this.modalService.dismissAll();
    }
    
  }
}
