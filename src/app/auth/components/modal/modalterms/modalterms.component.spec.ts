import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModaltermsComponent } from './modalterms.component';

describe('ModaltermsComponent', () => {
  let component: ModaltermsComponent;
  let fixture: ComponentFixture<ModaltermsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModaltermsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModaltermsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
