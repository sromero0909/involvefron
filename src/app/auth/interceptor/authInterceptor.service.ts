import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../services/auth.service'
import { SessionService } from '../services/sessionService/session.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor(
    private router: Router,
    private authService: AuthService,
    private state: ActivatedRoute,
    private sessionService: SessionService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  
    let token: string = this.authService.getToken();
    const tokenTem : any = (localStorage.getItem('token') != null) ?localStorage.getItem('token') : ""
    token = (token == "") ? tokenTem : token
    let request = req;
    //console.log('TOKEN: ', token);
    if (token != null && token !="") {
      if(token.includes("Bearer")){
        request = req.clone({
          setHeaders: {
            Authorization: `${ token }`
          }
        });
        //console.log('Token:', token);
      }else{
        request = req.clone({
          setHeaders: {
            Authorization: `Bearer ${ token }`
          }
        });
        //console.log('Bearer token:', token);
      }
    }

    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 403){
          const snapshot: any = this.state.snapshot;
          if (!snapshot._routerState.url.includes('login')){
            this.sessionService.closeSessionChecker();
            this.sessionService.setToken(null);
            this.sessionService.setTokenExpiration(null);
            this.router.navigateByUrl(`/login?returnUrl=${encodeURIComponent(snapshot._routerState.url)}`);
          }
        }
        return throwError( err );
      })
    );
  }

}