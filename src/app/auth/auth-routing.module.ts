import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';

import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { RestorePasswordComponent } from './pages/restore-password/restore-password.component';
import { GeneroComponent } from './pagesCandidato/register/genero/genero.component';
import { PerfilProfesionalComponent } from './pagesCandidato/register/perfil-profesional/perfil-profesional.component';
import { PuestoComponent } from './pagesCandidato/register/puesto/puesto.component';
import { ActivateRecuitreAccountComponent } from './pages/activate-recuitre-account/activate-recuitre-account.component';
import { RegisterCandidatoComponent} from './pagesCandidato/register/register.component'
import { SueldoComponent } from './pagesCandidato/register/sueldo/sueldo.component';
import { CreateCandidateAccountComponent } from './pages/create-candidate-account/create-candidate-account.component';
import { RestorePasswordCandidateComponent } from './pagesCandidato/restore-password-candidate/restore-password-candidate.component';
import { ViewTestComponent } from './pages/view-test/view-test.component';
import { RestorePasswordAttemptsComponent } from './pages/restore-password-attempts/restore-password-attempts.component';
import { ActivateCandidateAccountComponent } from './pagesCandidato/activate-candidate-account/activate-candidate-account.component';
import { PrivacyComponent } from './pages/privacy/privacy.component';
import { TermsComponent } from './pages/terms/terms.component';
import { CreateGuestPasswordComponent } from './pages-invited-recruiter/create-guest-password/create-guest-password.component';
import { RegisterGuestRecruiterComponent } from './pages-invited-recruiter/register-guest-recruiter/register-guest-recruiter.component';
import { UploadGuestPhotoComponent } from './pages-invited-recruiter/upload-guest-photo/upload-guest-photo.component';
import { TermsAndConditionsComponent } from './pages-invited-recruiter/terms-and-conditions/terms-and-conditions.component';
import { SelectYourPlanComponent } from './pages/select-your-plan/select-your-plan.component';
import { StartPlanComponent } from './pages/start-plan/start-plan.component';
import { StandbyScreenComponent } from './components/standby-screen/standby-screen.component';
import { CompanyDataComponent } from './pages/company-data/company-data.component';
import { InvitePeopleComponent } from './pages/invite-people/invite-people.component';
import { IntoCellphoneComponent } from './pagesCandidato/register/into-cellphone/into-cellphone.component';
import { ValidateSmsComponent } from './pagesCandidato/register/validate-sms/validate-sms.component';
import { RegisterSucessComponent } from './pagesCandidato/register/register-sucess/register-sucess.component';
import { BirthdayComponent } from './pagesCandidato/register/birthday/birthday.component';
import { MaritalStatusComponent } from './pagesCandidato/register/marital-status/marital-status.component';
import { NationalityComponent } from './pagesCandidato/register/nationality/nationality.component';
import { FirstJobComponent } from './pagesCandidato/register/first-job/first-job.component';
import { JobLocationComponent } from './pagesCandidato/register/job-location/job-location.component';
import { LiveThereComponent } from './pagesCandidato/register/live-there/live-there.component';
import { AvatarComponent } from './pagesCandidato/register/avatar/avatar.component';
import { CandidateLocationComponent } from './pagesCandidato/register/candidate-location/candidate-location.component';
import { OcrComponent } from './pagesCandidato/ocr/ocr.component';
import { UploadCvComponent } from './pagesCandidato/ocr/upload-cv/upload-cv.component';
import { ValidationOcrComponent } from './pagesCandidato/ocr/validation-ocr/validation-ocr.component';
import { WorkExperienceComponent } from './pagesCandidato/ocr/work-experience/work-experience.component';
import { EducationComponent } from './pagesCandidato/ocr/education/education.component';
import { CvComponent } from './pagesCandidato/ocr/cv/cv.component';
import { RegistroCvComponent } from './pagesCandidato/ocr/registro-cv/registro-cv.component';
import { RegistroCvCardComponent } from './pagesCandidato/ocr/registro-cv-card/registro-cv-card.component';
import { IntoPasswordComponent } from './pagesCandidato/register/into-password/into-password.component';
import { CanDeactiveRegisterGuard } from '../commons/guards/can-deactive-register.guard';
import { AuthGuard } from '../commons/guards/auth.guard';


const routes: Routes = [
  {path: '', component: LoginComponent },
  {path: 'login', component: LoginComponent },
  {path: 'login/candidate', component: LoginComponent },
  {path: 'register', component: RegisterComponent },
  {path: 'forgot-password', component: ForgotPasswordComponent },
  {path: 'restore-password/:keyToken', component: RestorePasswordComponent },
  {path: 'restore-password-attempts/:keyToken', component: RestorePasswordAttemptsComponent },
  {path: 'register-candidato', component: RegisterCandidatoComponent },
  {path: 'register-candidato/genero', component: GeneroComponent },
  {path: 'register-candidato/perfil-profesional', component: PerfilProfesionalComponent },
  {path: 'register-candidato/puesto', component: PuestoComponent },
  {path: 'activate-recuitre-account/:keyToken', component: ActivateRecuitreAccountComponent},
  {path: 'register-candidato/puesto', component: PuestoComponent },
  {path: 'register-candidato/sueldo', component: SueldoComponent },
  {path: 'create-candidate-account', component: CreateCandidateAccountComponent },
  {path: 'restore-passowrd-candidate/:keytoken', component: RestorePasswordCandidateComponent },
  {path: 'activate-candidate-account/:keyToken', component: ActivateCandidateAccountComponent},
  {path: 'termsAndConditions', component: TermsComponent, canActivate: [AuthGuard]},
  {path: 'noticeOfPrivacy', component: PrivacyComponent},

  {path: 'select-your-plan/:flag',component:SelectYourPlanComponent, canActivate: [AuthGuard]},
  {path: 'start-plan/:flag', component:StartPlanComponent, canDeactivate:[CanDeactiveRegisterGuard], canActivate: [AuthGuard]},
  {path: 'start-plan/:flag/:isRenovation', component:StartPlanComponent, canDeactivate:[CanDeactiveRegisterGuard], canActivate: [AuthGuard]},
  {path: 'invite-your-team',component: InvitePeopleComponent, canActivate: [AuthGuard]},
  {path: 'standby-screen/:flag',component:StandbyScreenComponent, canActivate: [AuthGuard]},
  {path: 'company-data',component:CompanyDataComponent, canActivate: [AuthGuard]},
  

  {path: 'create-guest-password/:keyToken', component:CreateGuestPasswordComponent},
  {path: 'register-guest-recruiter/:flag', component:RegisterGuestRecruiterComponent},
  {path: 'upload-guest-photo', component:UploadGuestPhotoComponent},
  {path: 'terms-and-conditions/:flag', component:TermsAndConditionsComponent},

  {path: 'into-cellphone/:keyToken', component:IntoCellphoneComponent},
  {path: 'validate-sms/:phone', component:ValidateSmsComponent},
  {path: 'candidate-count', component:IntoPasswordComponent},
  {path: 'register-success', component:RegisterSucessComponent},
  {path: 'birthday', component:BirthdayComponent},
  {path: 'marital-status', component:MaritalStatusComponent},
  {path: 'nationality', component:NationalityComponent},
  {path: 'first-job', component:FirstJobComponent},
  {path: 'job-location', component:JobLocationComponent},
  {path: 'live-there', component:LiveThereComponent},
  {path: 'avatar', component:AvatarComponent},
  {path: 'candidate-location', component:CandidateLocationComponent},

  {path: 'ocr', component:OcrComponent},
  {path: 'upload-cv', component:UploadCvComponent},
  {path: 'validation-ocr', component:ValidationOcrComponent},
  {path: 'work-experience-ocr', component:WorkExperienceComponent},
  {path: 'education-ocr', component:EducationComponent},
  {path: 'cv-ocr', component:CvComponent},
  {path: 'registro-cv', component:RegistroCvComponent},
  {path: 'registro-cv-card', component:RegistroCvCardComponent}


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
