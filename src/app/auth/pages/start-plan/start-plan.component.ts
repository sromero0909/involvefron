import { Component, OnInit, HostListener, ViewChild, TemplateRef } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';


import * as moment from 'moment';
import 'moment/locale/es';
import { AuthService } from 'src/app/services/auth.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { PaymentGatewayService } from 'src/app/services/reclutador/login/payment-gateway.service';
import { ModalNewTargetCardComponent } from '../../components/modal/modal-new-target-card/modal-new-target-card.component';
import { ProfileService } from 'src/app/services/reclutador/configuration/profile.service';
import { TranslateService } from '@ngx-translate/core';
import { SuscriptionCanceledService } from 'src/app/services/reclutador/login/subscription-canceled.service';
import { PuedeDesactivar } from 'src/app/commons/guards/can-deactive-register.guard';
import { Observable } from 'rxjs-compat/Observable';
import { map } from 'rxjs/operators';

declare var $: any;
declare var OpenPay: any;

const CURRENT_PAGE_STEP = 4;

@Component({
  selector: 'app-start-plan',
  templateUrl: './start-plan.component.html',
  styleUrls: ['./start-plan.component.scss']
})
export class StartPlanComponent implements OnInit, PuedeDesactivar {

  constructor(private activatedRoute: ActivatedRoute,
              private _payment: PaymentGatewayService,
              private modalService: NgbModal,
              private authService: AuthService,
              private _suscription: SuscriptionService,
              private toastService: ToastService,
              private formBuilder: FormBuilder,
              private profileService: ProfileService,
              private traslateService: TranslateService,
              private _suscriptionCanceled: SuscriptionCanceledService,
              private route: Router, ) { }
  @ViewChild('deleteCardConfirmation') deleteCardConfirmation!: TemplateRef<any>;
  @ViewChild('confirmPassWord') confirmPassWord!: TemplateRef<any>;
  @ViewChild('newCardRejected') newCardRejected!: TemplateRef<any>;
  @ViewChild('confirmCvv') confirmCvv: TemplateRef<any>;

  userId = '';
  cardId = '';
  now: any;
  dateBilling: any;
  modalParams: any;
  sumaryTem: any;
  initialView = true;
  loading = false;
  showPassword = false;
  reactivation = false;
  confirmedCvv: '';
  updatedCvv: FormGroup = new FormGroup({
    cvv: new FormControl('', [Validators.required,
                              Validators.pattern(/^[0-9]{3,4}$/)])
  });

  userCreditCardsArray: any[] = [];

  formEmail!: FormGroup;
  summary: any;
  @HostListener('window:beforeunload', ['$event'])


  doSomething($event: any) {
   // this.permitirSalirRuta();
    console.log('.........................');
  }

  permitirSalirRuta = (): boolean | Observable<boolean> | Promise<boolean> => {
    // const confirm = window.confirm('¿Quieres avandonar la pantalla?')
    return true;
  }


  ngOnInit(): void {
    this.getKeysOpenPay('PK_OPENPAY', true);
    const flagRoute = this.activatedRoute.snapshot.paramMap.get('flag');
    this.initialView = (flagRoute === 'false') ? false : true;

    if (localStorage.getItem('execute') !== 'yes') {
      if (this.initialView) {
        this.summary = this._suscriptionCanceled.getSummary();
      }
    }

    this.reactivation = (this.activatedRoute.snapshot.paramMap.get('isRenovation') && this.activatedRoute.snapshot.paramMap.get('isRenovation') === 'true') || false;
    this.sumaryTem = this._suscriptionCanceled.getSummary();
    this.getDate();
    if (this.authService.getUser() !== undefined && this.authService.getUser() !== '') {
      const user = JSON.parse(this.authService.getUser());
      this._suscription.setlastCompletedStep(user.stepsStatus);
      if (user.stepsStatus < 7){
        this._suscription.validateCurrentStatus(CURRENT_PAGE_STEP);
      }
      this.userId = user.user?.userOpenPay;
    }
    if (!this.initialView) {
      this.getCards();
      this.cardId = this._suscription.getCurrentCard();
    }
    this.initializeFormPassWord();
  }

  ngOnDestroy() {
    this._suscriptionCanceled.setSummary(JSON.stringify(this.sumaryTem));
    this.permitirSalirRuta();
  }

  getDate = async () => {
    moment.locale(this.traslateService.getDefaultLang());
    let tranlation1 = '';
    await this.traslateService.get(['configuration.subscription.of']).toPromise().then(
      (tranlation) => {
        tranlation1 = tranlation['configuration.subscription.of'];
      }
    );
    const date = moment();
    this.now = date.format('DD-MMMM-YYYY');
    const dateNowTem = this.now.split('-');
    this.now = dateNowTem[0] + ' ' + tranlation1 + ' ' + dateNowTem[1] + ', ' + dateNowTem[2];

    const fecha = moment().add(1, 'months');
    this.dateBilling = fecha.format('DD-MMM-YYYY');
    const dateBillingTem = this.dateBilling.split('-');
    this.dateBilling = dateBillingTem[0] + ' ' + dateBillingTem[1] + ' ' + dateBillingTem[2];

  }

  openModal(name: string, params?: any): void {
    this.modalService.dismissAll();
    this.modalParams = params;
    let modalRef;

    switch (name) {
      case 'addNewCard':
        const modalRef1 = this.modalService.open(ModalNewTargetCardComponent, { centered: true, size: 'lg', scrollable: true });
        modalRef1.dismissed.subscribe(
          (response) => {
            if (response){
              if (response.success){
                this.toastService.showSuccess('Se agregó una nueva tarjeta');
              }
              if (response.carsLimitExceeded){
                this.modalService.dismissAll();
                this.modalService.open(this.newCardRejected, {centered: true, size: 'md'});
              }
              this.getCards();
            }
          }
        );
        break;
      case 'confirmPassWord':
        modalRef = this.modalService.open(this.confirmPassWord, { centered: true, size: '' });
        break;
      case 'deleteCardConfirmation':
        modalRef = this.modalService.open(this.deleteCardConfirmation, { centered: true, size: '' });
        this.modalParams = params;
        break;
      case 'confirmCvv':
        modalRef = this.modalService.open(this.confirmCvv, { centered: true, size: 'md' });
        break;
    }
  }

  getCards = () => {
    this._suscription.getCards(this.getParams()).subscribe(
      (response: any) => {
        if (response.status === 200 && response.body !== null) {
          this.userCreditCardsArray = [];
          for (const i of response.body) {
            this.userCreditCardsArray.push(
              {
                id: i.id,
                type: i.brand,
                ends: String(i.cardNumber).substring(12, 16),
                isCurrent: false
              },
            );
          }
          if (this.userCreditCardsArray.length === 1 || (this._suscription.getCurrentCard() === '' && this.userCreditCardsArray.length > 1)) {
            this.userCreditCardsArray[0].isCurrent = !0;
            this.cardId = this.userCreditCardsArray[0].id;
          }
        }
      },
      (error) => {
        this.toastService.showMessageError(error.error);
      }
    );
  }

  getParams = () => {
    const params = {
      dateFin: moment().format('YYYY/MM/DD'),
      dateIni: moment().subtract(5, 'y').format('YYYY/MM/DD'),
      idClient: this.userId
    };
    return params;
  }

  onDeleteCardConfirmation(): void {
    this.deleteCard(this.modalParams.cardId);
    this.onCloseModal();
  }

  onCloseModal(): void {
    this.modalParams = null;
    this.modalService.dismissAll();
  }

  createSuscriptionByCard = () => {
    this.getPlanId().subscribe(
      (successResponse: any) => {
        const planId = successResponse;
        const deviceSessionId = OpenPay.deviceData.setup('formEmail');
        const params = {
          cardId: this.cardId,
          clientId: this.userId,
          planId,
          cvv: this.updatedCvv.value.cvv.toString(),
          url: 'dashboard/vacantes/activas',
          deviceSessionId
        };
        this._suscription.createSuscriptionByCard(params).subscribe(
          (response: any) => {
            if (response.hasOwnProperty('error')) {
              if (response.status !== 200) {
                this.toastService.showMessageError(response.error);
                return;
              }
            }
            localStorage.removeItem('aboutToExpireSeen');
            window.location = response.body;
          },
          (error) => {
            console.error(error);
          }
        );
      }
    );
  }

  /**
   * Evaluates the condition to select the correct planId to be sent on resubscription
   * It can be either the planId set when entering a coupon, the planOpenPay property
   * in the user model, or the Tama1 plan id
   * @returns A planId
   */
  getPlanId(): Observable<any> {
    const planId = (this._suscriptionCanceled.getSummary() !== null && this._suscriptionCanceled.getSummary().descuento === 0) ?
                    this._suscriptionCanceled.getSummary().id :
                    this._suscriptionCanceled.getIdPlan();
    if (planId === ''){
      if (JSON.parse(this.authService.getUser()).user.planOpenPay === null){
        return this.getPlans();
      }
      else{
        return new Observable<any>((observer) => {
          observer.next(JSON.parse(this.authService.getUser()).user.planOpenPay);
        });
      }
    }
    else{
      return new Observable<any>((observer) => {
        observer.next(planId);
      });
    }
  }

  /**
   * Asks the server for a list of plans and gets the plan called Tama1
   * @returns Tama1 plan id. null if the plan does not existe
   */
  getPlans(): Observable<any> {
    const params = {
      dateFin: moment().format('YYYY/MM/DD'),
      dateIni: moment().subtract(5, 'y').format('YYYY/MM/DD')
    };
    return this._payment.getPlan(params).pipe(map(
      (response: any) => {
        if (response != null && response.status === 200 && response.body !== undefined && response.body.length > 0) {
          const Tama1 = response.body.filter((plan: any) => {
            return plan.name.trim() === 'Tama1';
          });
          if (Tama1.length === 1){
            return Tama1[0].id;
          }
          else{
            return null;
          }
        }
        else{
          return null;
        }
      }
    ));
  }

  initializeFormPassWord = () => {
    this.formEmail = this.formBuilder.group({
      passwordEmail: [{ value: '', disabled: false }, [Validators.required]],
    });
  }

  deleteCard = (idcard: string) => {
    this._suscription.deleteCard(idcard, this.userId).subscribe(
      (response: any) => {
        if (response.status === 200 && response.body !== null && !response.body.hasOwnProperty('error_code')) {
          this.toastService.showSuccess('El método de pago ha sido eliminado');
          this.getCards();
        } else {
          this.toastService.showSuccess(response.body.description);
        }
      },
      (error) => {
        this.toastService.showSuccess(error.error.message);
      }
    );
  }

  changeCard = () => {
    this.onCloseModal();
    this.toastService.showSuccess('El método de pago ha sido cambiado');
    this._suscription.setCurrentCard(this.cardId);
    this.getCards();
  }

  changePlan = () => {
    this.profileService.verifyPassWord(this.formEmail.get('passwordEmail')?.value).subscribe(
      (response: any) => {
        if (response !== null) {
          if (response.code === 471) {
            this.toastService.showMessageError('La contraseña es incorrecta');
          } else {
            this.createSuscriptionByCard();
          }
        }
      },
      (error) => {

      });
  }

  getKeysOpenPay = (code: string, execute: boolean) => {
    this._payment.getKeysOpenPay(code).subscribe(
      (response: any) => {
        if (response.status === 200 && response.body !== null) {
          if (code === 'PK_OPENPAY') {
            localStorage.setItem('PK_OPENPAY', response.body.value);

            OpenPay.setId(localStorage.getItem('ID_OPENPAY'));
            OpenPay.setApiKey(localStorage.getItem('PK_OPENPAY'));
            // ! Se estable el valor a false para producción. Este valor no debe ser modificado en este ambiente
            OpenPay.setSandboxMode(false);

          } else {
            localStorage.setItem('ID_OPENPAY', response.body.value);
          }
          if (execute) {
            this.getKeysOpenPay('ID_OPENPAY', false);
          }
        }
      },
      (error) => {
        this.toastService.showMessageError(error.error);
      }
    );
  }

  setCurrentCard(card: any): void{
    this.userCreditCardsArray.forEach(el => {
      el.isCurrent = false;
    });
    this.cardId = card.id;
    card.isCurrent = true;
  }

  onConfirmCvvCode(): void{
    this.updatedCvv.markAllAsTouched();
    if (this.updatedCvv.valid){
      this.modalService.dismissAll();
      this.createSuscriptionByCard();
    }
  }
}
