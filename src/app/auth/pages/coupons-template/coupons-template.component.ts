import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-coupons-template',
  templateUrl: './coupons-template.component.html',
  styleUrls: ['./coupons-template.component.scss']
})
export class CouponsTemplateComponent implements OnInit {

  couponsList: FormGroup;
  HtmlGeneratedTemplate: string | null = null;
  isCopied = false;

  constructor(private fb: FormBuilder) {
    this.couponsList = this.fb.group({
      coupons: this.fb.array([])
    });
    const newControl = this.fb.group({
      couponName: this.fb.control('', Validators.required),
      couponDescription: this.fb.control('', Validators.required),
      couponDiscount: this.fb.control('', Validators.required)
    });
    this.couponsArray.push(newControl);
  }

  ngOnInit(): void {
  }

  addCoupon(): void{
    const newControl = this.fb.group({
      couponName: this.fb.control('', Validators.required),
      couponDescription: this.fb.control('', Validators.required),
      couponDiscount: this.fb.control('', Validators.required)
    });
    this.couponsArray.push(newControl);
    this.HtmlGeneratedTemplate = null;
  }

  removeCoupon(position: number): void{
    this.couponsArray.removeAt(position);
    this.HtmlGeneratedTemplate = null;
  }

  generateHtmlCouponsTemplate(): void{
    this.couponsList.markAllAsTouched();
    if (this.couponsList.valid){
      const couponsTemplate =
`<p class="font-weight-bold">1.- Condiciones generales:</p>
<br>
<p>Los cupones de descuento son una iniciativa utilizada por el Departamento de Marketing para campañas específicas. Usted podría ser elegido en futuras campañas y recibiría automáticamente un cupón a través de su correo electrónico. </p>
<p>Al entrar a la Plataforma Involve, obtendrá un cupón el cual es válido por una sola suscripción y únicamente en la una sola mensualidad. No es canjeable por dinero en efectivo. Los términos y condiciones relativos a la validez del cupón se incluyen con cada cupón.</p>
<p>Los cupones no son reembolsables, pueden utilizarse en una sola suscripción a plataforma www.involverh.com/home.</p>
<p>No es canjeable por dinero en efectivo. Los términos y condiciones relativos a la validez del cupón se incluyen con cada cupón. Existen diferentes tipos de porcentajes de descuento son válidos por días de uso. Todas las restricciones y detalles relativos a las ofertas pueden ser encontrados en las condiciones específicas de cada cupón, incluidas en el e-mail que contiene el cupón de descuento. No está permitido vender, comerciar o adquirir cupones de descuento de ninguna manera sin el permiso de Involverh.</p>
<p>Se aplican los Términos y Condiciones del Servicio disponibles en www.involverh.com/home</p>
<br>
<p class="font-weight-bold">2.- Tabla de características:</p>
<br>
<table class="table">
  <thead>
    <tr>
      <td>Nombre</td>
      <td>Característica</td>
      <td>Descuento</td>
    </tr>
  </thead>
  <tbody>ROWS_TO_REPLACE
  </tbody>
</table>`;
      let couponsRows = '';
      this.couponsList.value.coupons.forEach((el: any) => {
        couponsRows =
`${couponsRows}
    <tr><td>${el.couponName}</td><td>${el.couponDescription}</td><td>${el.couponDiscount}</td></tr>`;
      });
      this.HtmlGeneratedTemplate = couponsTemplate.replace('ROWS_TO_REPLACE', couponsRows);
    }
  }

  hasFormFieldError(position: number, fieldName: string){
    return this.couponsArray.at(position).get(fieldName)?.touched && this.couponsArray.at(position).get(fieldName)?.errors;
  }

  copyHtmlGeneratedTemplate(): void{
    if ( this.HtmlGeneratedTemplate ){
      navigator.clipboard.writeText(this.HtmlGeneratedTemplate);
      this.isCopied = true;
      setTimeout(() => {
        this.isCopied = false;
      }, 3_000);
    }
  }

  get couponsArray(): FormArray{
    return this.couponsList.get('coupons') as FormArray;
  }

  get isFormValid(): boolean{
    return this.couponsArray.valid && this.couponsArray.length > 0;
  }


}
