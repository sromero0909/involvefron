import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CouponsTemplateComponent } from './coupons-template.component';

describe('CouponsTemplateComponent', () => {
  let component: CouponsTemplateComponent;
  let fixture: ComponentFixture<CouponsTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CouponsTemplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CouponsTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
