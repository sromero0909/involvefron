import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { RecruiterTeamService } from 'src/app/services/reclutador/configuration/recruitment-team.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { ProfileService } from 'src/app/services/reclutador/configuration/profile.service';
import { AuthService } from '../../../services/auth.service';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { ModalReactivationComponent } from 'src/app/modulos/dashboardReclutadores/vacantes/modal-reactivation/modal-reactivation.component';
import { ModalExpirationComponent } from 'src/app/modulos/dashboardReclutadores/vacantes/modal-expiration/modal-expiration.component';

const CURRENT_PAGE_STEP = 7;
@Component({
  selector: 'app-invite-people',
  templateUrl: './invite-people.component.html',
  styleUrls: ['./invite-people.component.scss']
})
export class InvitePeopleComponent implements OnInit {

  recruitersForm: FormGroup;
  recruitersArray: any[] = [];
  statusStep = 7;

  constructor(private fb: FormBuilder,
              private router: Router,
              private recruiterTeamService: RecruiterTeamService,
              public toastService: ToastService,
              private profileService: ProfileService,
              private authService: AuthService,
              private _suscription: SuscriptionService,
              private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this._suscription.validateCurrentStatus(CURRENT_PAGE_STEP);
    this.recruitersForm = this.fb.group({
      email: this.fb.control('', [Validators.required, Validators.email]),
      rol: this.fb.control('RECRUITER')
    });
  }

  recruitersFormSubmit(): void {
    this.recruitersForm.markAllAsTouched();
    if (this.recruitersForm.valid){
      if (!this.existEmailInRecruitersArray(this.recruitersForm.value.email)){
        this.recruitersArray.push(this.recruitersForm.value);
      }
      this.recruitersForm.get('email')?.setValue('');
      this.recruitersForm.get('email')?.markAsUntouched();
    }
  }

  existEmailInRecruitersArray(email: string){
    return this.recruitersArray.filter((el: any) => el.email === email).length > 0;
  }

  removeRecruiter(index: number): void {
    this.recruitersArray.splice(index, 1);
  }

  /**
   *
   * Validates if a particular field has a specif error if provided, or any error if not
   *
   * @param fieldName The name of the field to validate
   * @param formToValidate The reference to the form where fieldName belongs
   * @param validationName A particular error name to look for it.
   *
   * @returns `true` if formToValidate.get(fieldName) has the `validationName` error or any error, in case `validationName` is not provided.
   */
  hasFormFieldError(fieldName: string, formToValidate: FormGroup | AbstractControl, validationName?: string): boolean {
    if (validationName !== undefined) {
      return (formToValidate.get(fieldName)?.touched && formToValidate.get(fieldName)?.hasError(validationName) && formToValidate.get(fieldName)?.value !== '') || false;
    }
    else {
      return (formToValidate.get(fieldName)?.touched && formToValidate.get(fieldName)?.errors !== null && formToValidate.get(fieldName)?.value !== '') || false;
    }
  }

  sendInvitation = () => {
    this.recruiterTeamService.sentInvitation(this.recruitersArray).subscribe(
      (response: any) => {
        if (response.status == 200) {
          this._suscription.setlastCompletedStep(response.body.stepsStatus);
          this.updateStep();
        }
      },
      (error) => {
        this.toastService.showMessageError(String(error.error).replace(',', ''));
      }
    );
  }

  updateStep = () => {
    this.modalService.dismissAll();
    const params = [
      {
        op: 'replace',
        path: '/stepsStatus',
        value: CURRENT_PAGE_STEP + 1
      }
    ];

    this.profileService.updateRecruiterPatch(params).subscribe(
      (response: any) => {
        if (response.status == 200 && response.body != null && !response.body.hasOwnProperty('error')) {
          this.showModalWelcome();
          this.authService.setUser(JSON.stringify(response.body));
          this._suscription.setlastCompletedStep(response.body.stepsStatus);
          this._suscription.redirectToCorrectScreen(response.body.stepsStatus);
        }else{
          this.toastService.showMessageError(response.body.error);
        }
      },
      (error) => {
        this.toastService.showMessageError(error.error);
      }
    );
  }

  openModal(content: any) {
    this.modalService.open(content, { centered: true });
  }

  showModalWelcome = () => {
    const modalRef = this.modalService.open(ModalExpirationComponent, { centered: true, size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.newUser = true;
  }

  get isEmailFieldValid(): boolean{
    return this.recruitersForm.valid;
  }
}
