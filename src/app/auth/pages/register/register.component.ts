import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MESSAGES } from 'src/app/services/models/messages';
import { RegisterService } from 'src/app/services/reclutador/login/register.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  /**Variables para pintar los botones de tamaño */
  tamanios: any[] = [{ idTamano: "DE1A10", tamano: "1 a 10" }, { idTamano: "DE11A50", tamano: "11 a 50" }, { idTamano: "DE51A250", tamano: "51 a 250" }, { idTamano: "MASDE250", tamano: "+ 250" }]
  id = 0;
  //expresion regular para validar el telefono
  regExpANumber = /^-?(0|[1-9]\d*)?$/;
  regExpMail = /\S+@\S+\.\S+/;

  isCorrect: boolean = false;
  notCorrect: boolean = false;
  public userEmail = '';
  public userPassword = '';
  public emailCorrect = true;
  public messageError = '';

  /**Variables del formulario */
  public valTamano: string = "";
  public name: string = "";
  public lastName: string = "";
  public monLastName: string = "";
  public contactPhone: string = "";
  public email: string = "";
  public nameBusiness: string = "";
  public termsConditions: boolean = false;
  public selectTamanio: boolean = false;
  public isSelectTamanio: boolean = true;
  public inputPassword = false;

  public nameError: boolean = false;
  public lastNameError: boolean = false;
  public monLastNameError: boolean = false;
  public contactPhoneError: boolean = false;
  public emailError: boolean = false;
  public nameBusinessError: boolean = false;

  //Respuesta
  respuestaName: String = "";
  respuestaLastName: String = "";
  respuestaMonLastName: String = "";
  respuestaContactPhone: String = "";
  respuestaEmail: String = "";
  respuestaNameBusiness: String = "";
  respuestaTamano: String = "";

  formRegister: FormGroup = this.fb.group({
    email: [, [Validators.required, Validators.minLength(5), Validators.pattern(MESSAGES.expEmail)]],
  });

  constructor(private authService: AuthService,
    private fb: FormBuilder,
    private _register: RegisterService) {
    this.notCorrect = true;
  }

  ngOnInit(): void {

  }

  validateMailRegister = () => {
    this.emailError = false;
    if (this.formRegister.invalid) {
      this.formRegister.get('email')?.markAsTouched();
      this.emailCorrect = false;
    } else {
      // this.isCorrect = true;
      this.registryMail();
    }
  }

  registryMail = () => {
    this.emailError = false;
    this.respuestaEmail = ''
    const params = {
      email: this.formRegister.get('email')?.value,
      userRol: "RECRUITER_ADMIN"
    }
    this._register.registryMail(params).subscribe(
      (response: any) => {
        if (response.status == 200 && response.body != null) {
          console.log("es el token del registro",response.headers.get('token'));
          this.authService.setToken(response.headers.get('token'));
          this.authService.setUser(JSON.stringify(response.body));
          localStorage.setItem('token', response.headers.get('token'));
          this.authService.setUser(JSON.stringify(response.body));
          this.isCorrect = true;
        } else {
          this.emailError = true;
          this.respuestaEmail = "Error al registrar el correo";
        }
      },
      (error) => {
        if (error != undefined && error.status == 412) {
          this.emailError = true;
          this.respuestaEmail = "Ya existe una cuenta con ese email";
        }
      }
    );
  }

  changeInput = () => {
    this.emailError = false;
  }

}