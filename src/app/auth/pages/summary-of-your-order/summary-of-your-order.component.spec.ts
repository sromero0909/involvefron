import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryOfYourOrderComponent } from './summary-of-your-order.component';

describe('SummaryOfYourOrderComponent', () => {
  let component: SummaryOfYourOrderComponent;
  let fixture: ComponentFixture<SummaryOfYourOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummaryOfYourOrderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryOfYourOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
