import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs';

import { PaymentGatewayService } from 'src/app/services/reclutador/login/payment-gateway.service';
import { SuscriptionCanceledService } from 'src/app/services/reclutador/login/subscription-canceled.service';

@Component({
  selector: 'app-summary-of-your-order',
  templateUrl: './summary-of-your-order.component.html',
  styleUrls: ['./summary-of-your-order.component.scss']
})
export class SummaryOfYourOrderComponent implements OnInit {

  @Input() isRenovation = false;

  summary:any;
  sumary$!: Observable<any[]>;
  private subscriptions!: Subscription;
  constructor(private _serviceGlobal:SuscriptionCanceledService,private _payment: PaymentGatewayService) { }

  ngOnInit(): void {
   this.summary = this._serviceGlobal.getSummary();
   this.changeSummary()
  }

  ngOnDestroy() {
    if (this.subscriptions != undefined) {
      this.subscriptions.unsubscribe();
    }
  }

  changeSummary = () => {
    this.sumary$ = this._payment.getSummary$();
    this.subscriptions = this.sumary$.subscribe(
      (candidate) => {
        this.summary = candidate;
      }
    )
  }

}
