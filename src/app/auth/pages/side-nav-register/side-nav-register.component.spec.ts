import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SideNavRegisterComponent } from './side-nav-register.component';

describe('SideNavRegisterComponent', () => {
  let component: SideNavRegisterComponent;
  let fixture: ComponentFixture<SideNavRegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SideNavRegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
