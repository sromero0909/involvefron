import { Component, OnInit } from '@angular/core';
import { MainNavModel } from 'src/app/modulos/dashboardReclutadores/talent/models/main-nav.model';

@Component({
  selector: 'app-side-nav-register',
  templateUrl: './side-nav-register.component.html',
  styleUrls: ['./side-nav-register.component.scss']
})
export class SideNavRegisterComponent implements OnInit {

  mainNavData: MainNavModel;

  constructor() { }

  ngOnInit(): void {
    this.mainNavData = {
      search: true,
      logo: true,
      iconBack: false,
    }
  }

}
