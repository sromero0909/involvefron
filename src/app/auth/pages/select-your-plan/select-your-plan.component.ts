import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import * as moment from 'moment';
import 'moment/locale/es';

import { ActivatedRoute, Router } from '@angular/router';
import { PaymentGatewayService } from 'src/app/services/reclutador/login/payment-gateway.service';
import { ProfileService } from 'src/app/services/reclutador/configuration/profile.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { TranslateService } from '@ngx-translate/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { RegisterService } from 'src/app/services/reclutador/login/register.service';
import { SuscriptionCanceledService } from 'src/app/services/reclutador/login/subscription-canceled.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

const CURRENT_PAGE_STEP = 3;
@Component({
  selector: 'app-select-your-plan',
  templateUrl: './select-your-plan.component.html',
  styleUrls: ['./select-your-plan.component.scss']
})
export class SelectYourPlanComponent implements OnInit {

  date = '';
  dateShort = '';
  title = '';
  planM = '';
  errorCode = '';
  idPlan = '';
  initialView = true;
  existPlan = false;
  reactivation = false;
  disableBtn = false;
  formPlan: FormGroup;

  arrayPlans: Array<any> = [];

  descuento = 0;
  statusStep = 3;

  @ViewChild('couponConditions', { static: true }) couponConditions!: TemplateRef<any>;
  couponConditionsHTML = '';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private _payment: PaymentGatewayService,
    private profileService: ProfileService,
    private toast: ToastService,
    private translate: TranslateService,
    private _suscription: SuscriptionService,
    private _serviceGlobal: SuscriptionCanceledService,
    private modalService: NgbModal
    ) {
    this.translate.get('login.step5.codeError').subscribe((res: string) => {
      this.errorCode = res;
    });
    this.initializeForm();
  }

  ngOnInit(): void {
    const flagRoute = this.activatedRoute.snapshot.paramMap.get('flag');
    this.initialView = (flagRoute == 'false') ? false : true;
    if (localStorage.getItem('execute') == 'yes'){
      localStorage.removeItem('execute');
      this.router.navigateByUrl('/login');
    }else{
      if (this.initialView){
        if (Number(localStorage.getItem('step')) == 4){
          localStorage.setItem('step', '');
          // history.forward();
        }
        this._suscription.validateCurrentStatus(CURRENT_PAGE_STEP);
      }
      this.reactivation = this._suscription.getReactivation();
      this._serviceGlobal.setIdPlan('');
      this.getPlans();

      this.getDate(false);
    }
  }

  getDate = (isComplite: boolean) => {
    moment.locale('es');
    const fecha = moment();
    this.date = fecha.format('DD-MMMM-YYYY');
    const datetem = this.date.split('-');
    this.date = datetem[0] + ' de ' + datetem[1] + ', ' + datetem[2];

    this.dateShort = fecha.format('DD-MMM-YYYY');
    const dateShortTem = this.dateShort.split('-');
    this.dateShort = dateShortTem[0] + ' ' + dateShortTem[1] + '.' + ' ' + dateShortTem[2];
    return this.date;
  }

  initializeForm = () => {
    this.formPlan = this.formBuilder.group({
      plan: [{ value: '', disabled: false }, []],
      acceptsConditions: [{value: false, disabled: false}, []],
      code: [{ value: '', disabled: false }, []],
    });
  }

  getPlans = async () => {
    const params = {
      dateFin: moment().format('YYYY/MM/DD'),
      dateIni: moment().subtract(5, 'y').format('YYYY/MM/DD')
    };
    let year = '';
    let month = '';
    await  this.translate.get(['login.plans.yearplan', 'login.plans.monthplan', 'login.plans.days']).toPromise().then(
      (tranlation) => {
        year = tranlation['login.plans.yearplan'];
        month = (this.initialView) ? tranlation['login.plans.days'] : tranlation['login.plans.monthplan'];
      }
    );
    this.title = month;
    this._payment.getPlan(params).subscribe(
      (response: any) => {
        if (response != null && response.status === 200 && response.body !== undefined && response.body.length > 0) {
          for (const i of response.body) {
            if (String(i.name).includes('Tama1')) {
              this.idPlan = i.id;
              this.arrayPlans.push({
                id: i.id,
                amount: i.amount,
                name: (String(i.name).includes('Tama1')) ? month : year,
                isYear: (String(i.name).includes('Tama1')) ? false : true,
                currency: i.currency,
              });
            }
          }
        }
      },
      (error) => {
        this.toast.showMessageError(error.error);
      }
    );
  }

  changeRadioPlan = (value: string) => {
    this.existPlan = false;
    this.title = value;
    const idPlan = this.formPlan.get('plan')?.value;
  }


  getSummary = () => {
    // const idPlan = this.formPlan.get('plan')?.value
    for (const i of this.arrayPlans) {
      if (i.id == this.idPlan) {
        const summary = {
          ...i,
          descuento: (this.descuento == 0) ? 0 :  i.amount - this.descuento,
          total : (this.descuento == 0) ? i.amount : this.descuento,
        };
        this._payment.setSummary$(summary);
      }
    }
  }

  changePlan = () => {
    this.getSummary();
    if (this.initialView) {
      this.updateStep();
    } else {
      this.router.navigateByUrl('/start-plan/false');
    }
  }

  validateForm = (searchCode: boolean = false) => {
    if (searchCode) {
      this.formPlan.get('plan')?.markAsTouched();
      if (this.formPlan.valid && !this.isCodeFieldEmpty) {
        this.getCouponConditionsHTML();
        this.getDiscountCode();
      }
    }
    else {
      this.changePlan();
    }
  }

  getDiscountCode = () => {
    const code = this.formPlan.get('code')?.value;
    if (code !== '') {
      this._payment.getDiscountCode(code).subscribe(
        (response: any) => {
          if (response.status == 200 && response.body != null) {
            const time1 = moment().format('YYYY-MM-DD');
            const time2 = moment(response.body.dateLimit).format('YYYY-MM-DD');
            if (time1 < time2 && response.body.numberOfRedeem < response.body.numberOfLimit) {
              this.existPlan = true;
              this.descuento = response.body.amount;
              if (String(this.title).includes('anual')) {
                this._serviceGlobal.setIdPlan(response.body.planYear.plan);
              } else {
                this._serviceGlobal.setIdPlan(response.body.planMonth.plan);
              }
              this.formPlan.get('code')?.setValue('');
              if (this.formPlan.get('code')?.value === '') {
                this.formPlan.get('code')?.disable();
                this.disableBtn = true;
              }
              setTimeout(() => {
                this.getSummary();
              }, 200);
            } else {
              this.toast.showSuccess(this.errorCode);
            }
          } else {
            this.existPlan = false;
            this.toast.showSuccess(this.errorCode);
          }
        },
        (error: any) => {
          if (error.error.text.includes('HTTP Status 400 – Bad Request')){
            this.toast.showMessageError(this.errorCode);
          }
          else{
            this.toast.showMessageError(error.error);
          }
        }
      );
    }
  }

  updateStep = () => {
    // const params = [
    //   {
    //     op: 'replace',
    //     path: '/stepsStatus',
    //     value: CURRENT_PAGE_STEP
    //   }
    // ];

    // this.profileService.updateRecruiterPatch(params).subscribe(
    //   (response: any) => {
    //     if (response.status === 200 && response.body != null && !response.body.hasOwnProperty('error')) {
    //       this._suscription.setlastCompletedStep(response.body.stepsStatus);
    //       this._suscription.redirectToCorrectScreen(response.body.stepsStatus);
    //     }
    //   },
    //   (error) => {
    //     this.toast.showMessageError(error.error);
    //   }
    // );
    this._suscription.redirectToCorrectScreen(CURRENT_PAGE_STEP);
  }

  /**
   * Gets the coupon conditions from a service. There are displays in a modal
   */
  getCouponConditionsHTML(): void{
    this._payment.getKeysOpenPay('TERMSCOOKIES').subscribe(
      (successResponse: any) => {
        this.couponConditionsHTML = successResponse.body.value.replace(/\\"/gi, '"');
      },
      (errorResponse: any) => {
        console.error(errorResponse);
      }
    );
  }

  /**
   * Triigered when the user clicked the link below the summary
   */
  onOpenCouponConditions(): void{
    this.modalService.open(this.couponConditions, { centered: true, size: 'lg', scrollable: false});

  }

  /**
   * Close the modal and sets the user decision
   * @param accepts user's decision
   */
  onCloseModal(accepts: boolean): void{
    this.modalService.dismissAll();
    this.formPlan.get('acceptsConditions')?.setValue(accepts);
  }

  avoidSpaces(event: KeyboardEvent){
    if (event.code.toLowerCase() === 'space'){
      event.preventDefault();
      return false;
    }
    return true;
  }

  get isCodeFieldEmpty(): boolean{
    return this.formPlan.get('code')?.value === '';
  }

  get isConditionsAccepted(): boolean{
    return this.formPlan.get('acceptsConditions')?.value === true;
  }

  get isCodeFieldDisabled(): boolean{
    return this.formPlan.get('code')?.disabled || false;
  }

  get canUserContinue(): boolean{
    return (!this.existPlan && this.isCodeFieldEmpty) ||
           (this.existPlan && this.formPlan.get('acceptsConditions')?.value === true);
  }
}
