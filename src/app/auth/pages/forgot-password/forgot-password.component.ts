import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MESSAGES } from 'src/app/services/models/messages';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  public userEmail: string = "";
  private passWord: string = "";
  // Variable que controla si el correo ya pasó validación
  isEmail: boolean = false;
  // Variable que controla si el correo existe en el sistema
  emailError: boolean = false;
  //Respuesta
  respuesta: String ="";
  //variable que controla los intentos erroneos al ingresar el correo
  attempst: boolean = false;

  formMail!: FormGroup;
  constructor(private authService: AuthService,private formBuilder: FormBuilder,) { }

  ngOnInit(): void {
    this.initializeFormMail();
     this.attempst = this.authService.getAttempts();
    console.log("son los intenos",this.attempst)
  }


  validaCorreo(): void {
    if(this.userEmail == null || this.userEmail == ""){
      this.emailError = true;
      this.isEmail = false;
      this.respuesta = "Debes ingresar una cuenta de correo electrónico";
    } else {
      this.authService.recoverPassword(this.userEmail).subscribe(
        (response) => {
          console.log("response de enviar correo",response);          
          if(String(response.body).includes('404')) {
            this.respuesta = "No encontramos tu cuenta, verifica tu correo"
            this.emailError = true;
            this.isEmail = false;
          }else if(String(response.body).includes('enviado')){
            this.emailError = false;
            this.isEmail = true;
          }else{
            this.respuesta = "Error al enviar el correo, intenta de nuevo"
            this.emailError = true;
          } 
        
        },
        (error) => {
          console.log('Ocurrio un error');
          return;
        });
    }
  }

  resetAttempts(tex:string){
    console.log("se ejecuto",tex);
    this.authService.setAttempts(false)
  }

  initializeFormMail = () => {
    this.formMail = this.formBuilder.group({
      email: [{ value: '', disabled: false }, [Validators.required,Validators.pattern(MESSAGES.expEmail)]],
    });
  }

  onClickSubmit() {
    if (this.formMail.invalid) {
      this.formMail.get('email')?.markAsTouched();
    } else {
      this.validaCorreo()
    }
  }
}
