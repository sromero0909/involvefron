import { Component, OnInit, Input,  Output, EventEmitter,  } from '@angular/core';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { RegisterService } from 'src/app/services/reclutador/login/register.service';

@Component({
  selector: 'app-screen-confirm',
  templateUrl: './screen-confirm.component.html',
  styleUrls: ['./screen-confirm.component.scss']
})
export class ScreenConfirmComponent implements OnInit {
  @Input() titulo:any = {};
  @Input() img:any = '';  
  @Input() msg:any = {};
  @Input() btn:any = '';
  @Input() sento:string = ''
  @Input() barra:boolean= false;
  @Input() isMsgHTML: boolean = false;
  @Output() attempts = new EventEmitter<string>();
  text:boolean = false;
  message:string = "";
  constructor(private registerService:RegisterService, private toastService: ToastService) { }

  ngOnInit(): void {
    this.message = this.msg
    this.text = (this.message.includes("login")) ? true: false;
    this.message = (this.text) ? '' : this.message
  }

  changeAttempts(){
    this.attempts.emit("true");
  }

  sendMail = () => {
    this.registerService.resendEmail().subscribe(
      (response:any) => {
        if(response.status == 200 && response.body != null && response.body.code == 200){
          this.toastService.showSuccess(response.body.message+",true")
        }
      }
    )
  }


}
