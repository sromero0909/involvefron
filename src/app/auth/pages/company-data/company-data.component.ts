import { Component, OnInit, ViewChild, TemplateRef, ElementRef, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';

import { AuthService } from 'src/app/services/auth.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { CompanyService } from 'src/app/services/reclutador/configuration/company.service';
import { ProfileService } from 'src/app/services/reclutador/configuration/profile.service';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs';
import { RegisterService } from 'src/app/services/reclutador/login/register.service';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { ValidatorsService } from 'src/app/commons/validatorsService/validators.service';

const CURRENT_PAGE_STEP = 6;
@Component({
  selector: 'app-company-data',
  templateUrl: './company-data.component.html',
  styleUrls: ['./company-data.component.scss']
})
export class CompanyDataComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              public toastService: ToastService,
              private companyService: CompanyService,
              private profileService: ProfileService,
              private router: Router,
              private _registry: RegisterService,
              private _suscription: SuscriptionService,
              private validatorsService: ValidatorsService) {
    this.initializeFormCompany();
  }
  @ViewChild('iconoChekBlanco', { static: true }) iconoChekBlanco!: TemplateRef<any>;
  @ViewChild('fileUploadCompany', { static: false })


  InputVar!: ElementRef;

  formCompany!: FormGroup;

  sectores: any;
  giros: any;

  idGiro = '';
  photo: any = '';
  imgTrash = '';
  tamEmprea = '';
  companyId = '';
  mensajeToast = '';
  statusStep = 6;
  bordeTamEmpres = { uno: false, dos: false, tres: false, cuatro: false };
  tamanios = [
    {
      idTamano: 'OPC1',
      tamano: '1 a 10'
    },
    {
      idTamano: 'OPC2',
      tamano: '11 a 50'
    },
    {
      idTamano: 'OPC3',
      tamano: '51 a 250'
    },
    {
      idTamano: 'OPC4',
      tamano: '+ 250'
    }
  ];

  country: Array<any> = [];

  uptateGiro = false;
  private subscriptions!: Subscription;
  recruiters$!: Observable<any>;

  @HostListener('window:beforeunload', ['$event'])
  WindowBeforeUnoad($event: any) {
    history.pushState(null, 'null', document.URL);
    window.addEventListener('popstate', function() {
        history.pushState(null, 'null', document.URL);
    });
  }



  ngOnInit(): void {
    this.initializeSelect();
    this._suscription.validateCurrentStatus(CURRENT_PAGE_STEP);

   // this.changeRecruites();
    setTimeout(() => {
      this._registry.setStatusRegistry$(2);
    }, 5000);
  }

  initializeSelect = () => {
    this.onchangeSector();
    this.getPaises();
    this.getSectores();
  }

  initializeFormCompany = () => {
    this.formCompany = this.formBuilder.group({
      nameCompany: [{ value: '', disabled: false }, [Validators.required, this.validatorsService.noWhiteSpaces,Validators.pattern(this.validatorsService.allCharactersAndNumbersAndSpecial)]],
      companySector: [{ value: '', disabled: false }, [Validators.required]],
      companygiro: [{ value: '', disabled: false }, [Validators.required]],
      typeCompany: [{ value: '', disabled: false }, [Validators.required]],
      numEmpleadosEmpresa: [{ value: '', disabled: false }, [Validators.required]],
      pais: [{ value: '', disabled: false }, [Validators.required]],
    });
  }

  getCompany = (update: boolean) => {
    this.companyService.getCompany(this.companyId).subscribe(
      (response: any) => {
        if (response != null) {
          this.loadInfCompany(response, update);
        }
      },
      (error) => {
        this.showMessageError(error.error);
      });
  }

  onclickTamEmpresa(op: string, tam: any) {

    this.resetBordeTamEmpresa();
    switch (op) {
      case 'OPC1':
        this.tamEmprea = tam;
        this.bordeTamEmpres.uno = true;
        break;
      case 'OPC2':
        this.tamEmprea = tam;
        this.bordeTamEmpres.dos = true;
        break;
      case 'OPC3':
        this.tamEmprea = tam;
        this.bordeTamEmpres.tres = true;
        break;
      case 'OPC4':
        this.tamEmprea = tam;
        this.bordeTamEmpres.cuatro = true;
        break;
      default:
        this.tamEmprea = tam;
        break;
    }
    this.formCompany.controls.numEmpleadosEmpresa.setValue(this.tamEmprea);
  }

  resetBordeTamEmpresa() {
    this.bordeTamEmpres.uno = false;
    this.bordeTamEmpres.dos = false;
    this.bordeTamEmpres.tres = false;
    this.bordeTamEmpres.cuatro = false;
  }

  getPaises = () => {
    this.authService.consultCountryu().subscribe(
      (response: any) => {
        if (response != null) {
          for (const item of response) {
            if (item.iso2 == 'MX' || item.iso2 == 'ES') {
              this.country.push(item);
            }
          }
        }
      },
      (error: any) => {
        this.showMessageError(error.error.message);
      });
  }

  getSectores(): void {
    this.authService.consultSector().subscribe(
      (response: any) => {
        this.sectores = response.body;
      },
      (error: any) => {
        this.showMessageError(error.message);
      });
  }

  searhGiro = (idSector: any): void => {
    this.authService.consultGiro(idSector).subscribe(
      (response: any) => {
        this.giros = response;
        if (this.uptateGiro) {
          setTimeout(() => {
            this.formCompany.controls.companygiro.setValue(this.idGiro);
          }, 5);
          this.uptateGiro = false;
        }
      },
      (error: any) => {
        this.showMessageError(error.message);
      });
  }

  onchangeSector(): void {
    this.formCompany.get('companySector')?.valueChanges.subscribe((changes: any) => {
      if (changes != '') {
        this.searhGiro(changes);
      }
    });
  }

  getSector = (idSector: string) => {
    let sectorTem;
    for (const sector of this.sectores) {
      if (sector.sectorId == idSector) {
        sectorTem = sector;
        break;
      }
    }
    return sectorTem;
  }

  getGiro = (idGiro: string) => {
    let giroTem;
    for (const giro of this.giros) {
      if (giro.industryId == idGiro) {
        giroTem = giro;
        break;
      }
    }
    return giroTem;
  }

  setTamEmpresa = (tam: string) => {
    switch (tam) {
      case 'DE1A10':
        this.tamEmprea = '1 a 10';
        break;
      case 'DE11A50':
        this.tamEmprea = '11 a 50';
        break;
      case 'DE51A250':
        this.tamEmprea = '51 a 250';
        break;
      case 'MASDE250':
        this.tamEmprea = '+ 250';
        break;
    }
    this.formCompany.controls.numEmpleadosEmpresa.setValue(this.tamEmprea);
  }

  loadInfCompany = (company: any, update: boolean) => {

    const client = company;
    this.companyId = client.companyId;
    this.uptateGiro = true;
    this.photo = (client.pathLogo == null) ? null : client.pathLogo + '?' + (new Date()).getTime();
    this.imgTrash = (this.photo == null) ? 'basuraGris.svg' : 'basura.svg';
    if (update) {
      if (client.company != null) {
        this.formCompany.controls.nameCompany.setValue(client.company);
      }

      if (client.sector != null) {
        this.formCompany.controls.companySector.setValue(client.sector.sectorId);
      }

      if (client.industry != null) {
        this.idGiro = client.industry.industryId;
        this.formCompany.controls.companygiro.setValue(client.industry.industryId);
      }
      if (client.typeCompany != null) {
        this.formCompany.controls.typeCompany.setValue(client.typeCompany);
      }

      if (client.companySize != null) {
        this.setTamEmpresa(client.companySize);
      }

      if (client.country != null) {
        this.formCompany.controls.pais.setValue(client.country);
      }
    }
  }

  getParamsCompany = () => {
    const numEmpleadosEmpresa = this.formCompany.get('numEmpleadosEmpresa')?.value;
    const cadena = (String(numEmpleadosEmpresa).includes('+ 250')) ? 'MASDE250' : 'DE';
    const params = {
      company: this.formCompany.get('nameCompany')?.value,
      companySize: (cadena == 'MASDE250') ? cadena : cadena + String(numEmpleadosEmpresa).replace(/ /g, '').toUpperCase(),
      country: this.formCompany.get('pais')?.value,
      industry: this.getGiro(this.formCompany.get('companygiro')?.value),
      pathLogo: this.photo,
      sector: this.getSector(this.formCompany.get('companySector')?.value),
      typeCompany: String(this.formCompany.get('typeCompany')?.value).toUpperCase(),
    };
    return params;
  }

  updateCompany = () => {

    this.companyService.createCompany(this.getParamsCompany()).subscribe(
      (response: any) => {
        if (response.status == 200 && response != null && response.body != null) {
          this.photo = response.pathLogo;
          //  this.mensajeToast = "Los cambios han sido actualizados"
          this.updateStep();
        }
      },
      (error) => {
        this.showMessageError(error.error);
      });
  }

  updateStep = () => {
    const params = [
      {
        op: 'replace',
        path: '/stepsStatus',
        value: CURRENT_PAGE_STEP
      }
    ];

    this.profileService.updateRecruiterPatch(params).subscribe(
      (response: any) => {
        if (response.status === 200 && response.body != null && !response.body.hasOwnProperty('error')) {
          this._suscription.setlastCompletedStep(response.body.stepsStatus);
          this._suscription.redirectToCorrectScreen(response.body.stepsStatus);
        } else {
          this.showMessageError('Ocurrió un error');
        }
      },
      (error) => {
        this.showMessageError(error.error);
      }
    );
  }

  onClickSubmitCompany() {
    if (this.formCompany.invalid) {
      this.formCompany.get('nameCompany')?.markAsTouched();
      this.formCompany.get('companySector')?.markAsTouched();
      this.formCompany.get('companygiro')?.markAsTouched();
      this.formCompany.get('typeCompany')?.markAsTouched();
      this.formCompany.get('numEmpleadosEmpresa')?.markAsTouched();
      this.formCompany.get('pais')?.markAsTouched();
    } else {
      this.updateCompany();
    }
  }


  showMessageError(dangerTpl: any) {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-light ngb-toastsBottom', delay: 6000 });
  }

  changeRecruites() {
    this.recruiters$ = this._registry.getStatusRegistry$();
    this.subscriptions = this.recruiters$.subscribe(login => {
      console.log('suscripciones de login', login);

    });
  }

}
