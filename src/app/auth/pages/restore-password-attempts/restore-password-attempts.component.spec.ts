import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RestorePasswordAttemptsComponent } from './restore-password-attempts.component';

describe('RestorePasswordAttemptsComponent', () => {
  let component: RestorePasswordAttemptsComponent;
  let fixture: ComponentFixture<RestorePasswordAttemptsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RestorePasswordAttemptsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RestorePasswordAttemptsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
