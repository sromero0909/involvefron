import { Component, ElementRef, OnInit, Renderer2, ViewChild, AfterContentInit} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service'
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterService } from 'src/app/services/reclutador/login/register.service';

@Component({
  selector: 'app-restore-password',
  templateUrl: './restore-password.component.html',
  styleUrls: ['./restore-password.component.scss']
})
export class RestorePasswordComponent implements OnInit {
  
   // Variables para requisitos de contraseña
   isLower: boolean = false;
   isUpper: boolean = false;
   isMinLenght: boolean = false;
   isANumber: boolean = false;
   isCorrect: boolean = false;
   notCorrect: boolean = false;

   //Expresiones Regulares
   regExpLower = /[a-z]/;
   regExpUpper = /[A-Z]/;
   regExpMinLenght = /.{8,}/;
   regExpANumber = /[0-9]/;

  public rulePassword: boolean = false;
  public userNewPassword: string = "";
  public userPasswordConfirm: string = "";

  // Variable para controlar si se muestra la contraseña
  showPassword: boolean = false;
  showPasswordConfirm: boolean = false;
  equalPassword: boolean = false;
  emptyPassword: boolean = false;
  emptyConfirmPassword: boolean = false;

  txtEye: string = "Mostrar";

  password: FormControl = new FormControl('', Validators.required);
  confirmPassword: FormControl = new FormControl('', Validators.required);

  // Token generado por el backend
  keyToken: string = "";
  passwordChanged: boolean = false;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private register: RegisterService
    ) { }

  ngOnInit(): void {
    this.keyToken = this.route.snapshot.params['keyToken'];
    console.log(this.keyToken)
  }

  validateNewPassword(): void{
    this.equalPassword = false;
    this.emptyPassword = false;
    this.emptyConfirmPassword = false;

    this.isLower = this.regExpLower.test(this.userNewPassword);
    this.isUpper = this.regExpUpper.test(this.userNewPassword);
    this.isMinLenght = this.regExpMinLenght.test(this.userNewPassword);
    this.isANumber = this.regExpANumber.test(this.userNewPassword);

    if (this.isLower && this.isANumber && this.isUpper && this.isMinLenght) {
      this.rulePassword = true;
    } else {
      this.rulePassword = false;
    }
  }

  sendPassword(): void{
    this.equalPassword = false;
    if((this.userNewPassword != null && this.userNewPassword != "") && (this.userPasswordConfirm != null && this.userPasswordConfirm != "")){
      if(this.userNewPassword == this.userPasswordConfirm){
        //lamamos al servicio para reestablecer contraseña
        this.authService.setToken(this.keyToken);
        this.register.createPassWord(this.userNewPassword).subscribe(
          (data) => {
            console.log(data)
            if ((data.status == 204 || data.status == 200) && data.body != null) {
              this.isCorrect = true;
            } else {
              this.isCorrect = false;
            }
          },
          (error) => {
            console.log(error.status);
            this.isCorrect = false;
          });
      } else{
        this.equalPassword = true;
      }
    } else {
      if(this.userNewPassword == ""){
        this.emptyPassword = true;
      } else {
        if(this.userPasswordConfirm == ""){
          this.emptyConfirmPassword = true;
        }
      }
    }
  }

}
