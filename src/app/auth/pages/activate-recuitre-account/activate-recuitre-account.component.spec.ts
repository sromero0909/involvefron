import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivateRecuitreAccountComponent } from './activate-recuitre-account.component';

describe('ActivateRecuitreAccountComponent', () => {
  let component: ActivateRecuitreAccountComponent;
  let fixture: ComponentFixture<ActivateRecuitreAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivateRecuitreAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivateRecuitreAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
