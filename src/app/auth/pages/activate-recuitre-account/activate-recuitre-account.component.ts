import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { AuthService } from '../../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterService } from 'src/app/services/reclutador/login/register.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { ValidatorsService } from 'src/app/commons/validatorsService/validators.service';
import { SessionService } from '../../services/sessionService/session.service';
import { ProfileService } from 'src/app/services/reclutador/configuration/profile.service';

import { CookieService } from 'ngx-cookie-service';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';

@Component({
  selector: 'app-activate-recuitre-account',
  templateUrl: './activate-recuitre-account.component.html',
  styleUrls: ['./activate-recuitre-account.component.scss']
})
export class ActivateRecuitreAccountComponent implements OnInit {

  passwordForm: FormGroup;
  isCorrect = false;
  notCorrect = false;
  activateAccount = false;

  public avisoPrivacidad = false;
  public termsConditions = false;
  public notifiInvitacion = false;

  // Variable para controlar si se muestra la contraseña
  showPassword = false;
  showPasswordConfirm = false;

  // Token generado por el backend
  keyToken = '';

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private _register: RegisterService,
    public toastService: ToastService,
    private fb: FormBuilder,
    private validatorsService: ValidatorsService,
    private sessionService: SessionService,
    private profileService: ProfileService,
    private _suscription: SuscriptionService,
  ) {
    this.passwordForm = this.fb.group({
        password: ['', [Validators.required,
                        Validators.maxLength(25),
                        Validators.minLength(8),
                        this.validatePasswordMissingLowerCharacter(),
                        this.validatePasswordMissingUpperCharacter(),
                        this.validatePasswordMissingNumer(),
                        this.validatorsService.noWhiteSpaces]],
        passwordConfirm: ['', [Validators.required, this.validateDifferentPassword]]
      },
      {
        validators: this.validateDifferentPassword()
      }
    );
    this.keyToken = this.route.snapshot.params.keyToken;
    if (this.keyToken){
      const tokenUser = JSON.parse(atob(this.keyToken.split('.')[1]));
      this.authService.setToken(this.keyToken);
      this.profileService.getRecruiter(tokenUser.id).subscribe(
        (successResponse: any) => {
          if (successResponse.stepsStatus !== null && successResponse.stepsStatus >= 0) {
            this.router.navigateByUrl('/', {replaceUrl: true});
          }
        }
      );

    }
  }

  ngOnInit(): void {}

  createPassword = () => {
    this.passwordForm.markAllAsTouched();
    if (this.passwordForm.valid){
      this.authService.setToken(this.keyToken);
      this._register.createPassWord(this.passwordForm.value.password).subscribe(
        (response: any) => {
          if (response.status == 200 && response.body != null) {
            if (response.body.hasOwnProperty('error')){
              this.toastService.showMessageError('Error al crear la contaseña');
            }else{
              this.authService.setToken(response.headers.get('token'));
              this.authService.setUser(JSON.stringify(response.body));
              localStorage.setItem('token', response.headers.get('token'));
              this.authService.setRecruiterId(response.body.recruiterId);
              this.authService.setEmail(response.body.user.email);
              const isTokenActive = this.sessionService.isTokenActive();
              if (!isTokenActive){
                const token = response.headers.get('token');
                const tokenPayload = JSON.parse(atob(token.split('.')[1]));
                const tokenExpiresAt = tokenPayload.exp * 1_000;
                const user = response.body.user;
                const recruiterId = response.body.recruiterId;
                this.sessionService.setToken(token);
                this.sessionService.setTokenExpiration(tokenExpiresAt);
                this.sessionService.serRecruiterId(recruiterId);
                this.sessionService.setUser(user);
                this.sessionService.startSessionChecker();
              }

              this.redirectToCompliteProfil();
            }
          }else{
            this.toastService.showMessageError('Error al crear la contaseña');
          }
        },
        (error) => {
          this.toastService.showMessageError(error.error.message);
        }
      );
    }
  }

  redirectToCompliteProfil = () => {
    const patchBody: any = [{
      op: 'replace',
      path: '/stepsStatus',
      value: 0
    }];
    this.profileService.updateRecruiterPatch(patchBody).subscribe((successResponse: any) => {
      this.router.navigateByUrl('terms-and-conditions/true', { replaceUrl: true, state: {k: 'login'}});
    });
  }

  validateDifferentPassword(){
    return (formGroup: FormGroup) => {
      if (formGroup.get('password')?.value !== formGroup.get('passwordConfirm')?.value){
          formGroup.get('passwordConfirm')?.setErrors({ differentPassword: true });
      }
      else{
        formGroup.get('passwordConfirm')?.setErrors(null);
      }
    };
  }

  validatePasswordMissingLowerCharacter(){
    return (formControl: AbstractControl) => {
      const includesLower = new RegExp(/[a-z]/).test(formControl.value);
      return includesLower ? null : { missingLower: true };
    };
  }

  validatePasswordMissingUpperCharacter(){
    return (formControl: AbstractControl) => {
      const includesUpper = new RegExp(/[A-Z]/).test(formControl.value);
      return includesUpper ? null : { missingUpper: true };
    };
  }

  validatePasswordMissingNumer(){
    return (formControl: AbstractControl) => {
      const includesNumber = new RegExp(/[0-9]/).test(formControl.value);
      return includesNumber ? null : { missingNumber: true };
    };
  }

  hasFormFieldError(fieldName: string, errorName?: string){
    if (errorName !== undefined){
      return this.passwordForm.get(fieldName)?.hasError(errorName);
    }
    else{
      return this.passwordForm.get(fieldName)?.errors;
    }
  }
}
