import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service'

@Component({
  selector: 'app-create-candidate-account',
  templateUrl: './create-candidate-account.component.html',
  styleUrls: ['./create-candidate-account.component.scss']
})
export class CreateCandidateAccountComponent implements OnInit {

  public userEmail: string = "";
  public userPassword: string = "";
  public emailCorrect: boolean = true;
  public emailError: boolean = false;
  public messageError: string = '';

  public inputPassword: boolean = false;

  // Variable para controlar si se muestra la contraseña
  showPassword: boolean = false;
  showPasswordConfirm: boolean = false;
  equalPassword: boolean = false;
  emptyPassword: boolean = false;
  emptyConfirmPassword: boolean = false;

  // Variables para requisitos de contraseña
  isLower: boolean = false;
  isUpper: boolean = false;
  isMinLenght: boolean = false;
  isANumber: boolean = false;
  isCorrect: boolean = false;
  notCorrect: boolean = false;
  activateAccount: boolean = false;
  isSendEmail: boolean = false;

  public avisoPrivacidad: boolean = false;
  public termsConditions: boolean = false;
  public notifiInvitacion: boolean = false;

  //Expresiones Regulares
  regExpLower = /[a-z]/;
  regExpUpper = /[A-Z]/;
  regExpMinLenght = /.{8,}/;
  regExpANumber = /[0-9]/;

 public rulePassword: boolean = false;
 public userNewPassword: string = "";
 public userPasswordConfirm: string = "";
  
  constructor(private authService: AuthService, private router: Router,private activatedRoute: ActivatedRoute,){ }

  ngOnInit(): void {
  }

  validateMail(): void {
    if(this.userEmail == ""){
      this.emailCorrect = false;
      this.messageError = 'Debes ingresar una cuenta de correo electrónico';
    } else {
      //monstramos contraseña
      this.isCorrect = true;
    }
  }

  validateMailCandidate(): void {
    this.authService.registerCandidate(this.userEmail).subscribe(
      (response) => {
        console.log(response)
        if (response.status == 201 || response.status == 200) {
          this.isCorrect = true;
          this.emailError = false;
          this.activateAccount = true;
          this.inputPassword = true;
          this.isSendEmail = true;
        }
      },
      (error) => {
        console.log(error);
        if(error.status === 412){
          this.emailError = true;
          this.messageError = "Ya existe una cuenta con ese email";
        }
      });
  }

  validateNewPassword(): void{
    this.equalPassword = false;
    this.emptyPassword = false;
    this.emptyConfirmPassword = false;

    this.isLower = this.regExpLower.test(this.userNewPassword);
    this.isUpper = this.regExpUpper.test(this.userNewPassword);
    this.isMinLenght = this.regExpMinLenght.test(this.userNewPassword);
    this.isANumber = this.regExpANumber.test(this.userNewPassword);

    if (this.isLower && this.isANumber && this.isUpper && this.isMinLenght) {
      this.rulePassword = true;
    } else {
      this.rulePassword = false;
    }
  }

  validatePasswords(): void{
    /**Validar paasword y mostrar la siguiente seccion */
    //this.inputPassword = false;
    //this.activateAccount = true;
    //this.isSendEmail = true;
  }

  redirectToVacantes() {
    this.router.navigate(['../register-candidato'], {
    relativeTo: this.activatedRoute,
  });
}

}



