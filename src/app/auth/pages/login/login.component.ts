import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { version } from '../../../../../package.json';

import { AuthService } from '../../../services/auth.service';

import { User } from 'src/app/services/models/user.model';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { SuscriptionCanceledService } from 'src/app/services/reclutador/login/subscription-canceled.service';
import { SessionService } from '../../services/sessionService/session.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Input() isOverlay = false;

  currentVersion: string = version;
  public userEmail = '';
  public userPassword = '';
  public emailCorrect = true;
  public emailError = false;
  public messageError = '';
  public passwordError = false;
  public showPassword = false;
  public inputPassword = false;
  public isCandidate = false;
  user!: User;
  constructor(private authService: AuthService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private _suscription: SuscriptionService,
              private _suscriptionCanceled: SuscriptionCanceledService,
              private sessionService: SessionService) {
    if (this.sessionService.isTokenActive()){
      if (!this.isOverlay){
        this._suscription.redirectToCorrectScreen(8);
      }
    }
    if (this.authService.getUser() && this.authService.getUser() !== ''){
      const user = JSON.parse(this.authService.getUser());
      if (user.user.userRol !== 'CANDIDATE'){
        this._suscription.validateCurrentStatus(user.stepsStatus);
      }
    }
  }

  ngOnInit(): void {
    this.getCurrentDate();
    if (this.router.url === '/login/candidate') {
      this.isCandidate = true;
    }
    this._suscription.setExecutemodal(false);
  }

  validateMail(): void {
    if (this.userEmail == '') {
      this.emailCorrect = false;
      this.messageError = 'Debes ingresar una cuenta de correo electrónico';
    } else { // validamos correo
      this.authService.validateMail(this.userEmail).subscribe(
        (data) => {
          if (data.body.body.code == 200) {
            this.emailCorrect = true;
            this.emailError = true;
            this.inputPassword = true;
          } else {
            this.messageError = 'No encontramos tu cuenta, verifica tu correo';
            this.emailCorrect = false;
            this.emailError = false;
          }
        },
        (error) => {
          console.error(error.status);
          if (error.status == 200) {
            this.emailCorrect = true;
            this.emailError = true;
            this.inputPassword = true;
          } else {
            this.messageError = 'No encontramos tu cuenta, verifica tu correo';
            this.emailCorrect = false;
            this.emailError = false;
          }
        });
    }
  }

  redirectToVacantes() {
    if (this.emailError == true){
      if (this.userPassword != null && this.userPassword != '' && !this.isCandidate ){
        // servicio de login
        this.authService.loginRecuitre(this.userEmail, this.userPassword).subscribe(
          (response) => {
            if (response.body.user && response.body.user.userRol !== 'CANDIDATE'){
              if (response.body.statusCodeValue && response.body.statusCodeValue  === 401){
                this.messageError = response.body.body.message;
                this.passwordError = true;
              }
              else if (response.body.status != 412 && response.body.status != 401 && response.body.status != 500) {
                const isTokenActive = this.sessionService.isTokenActive();
                if (!isTokenActive){
                  const token = response.headers.get('token');
                  const tokenPayload = JSON.parse(atob(token.split('.')[1]));
                  const tokenExpiresAt = tokenPayload.exp * 1_000;
                  const user = response.body.user;
                  const recruiterId = response.body.recruiterId;
                  this.sessionService.setToken(token);
                  this.sessionService.setTokenExpiration(tokenExpiresAt);
                  this.sessionService.serRecruiterId(recruiterId);
                  this.sessionService.setUser(user);
                  this.sessionService.startSessionChecker();

                  this.authService.setToken(response.headers.get('token'));
                  this.authService.setUser(JSON.stringify(response.body));
                  localStorage.setItem('token', response.headers.get('token'));
                  this.authService.setRecruiterId(response.body.recruiterId);
                  this.authService.setEmail(response.body.user.email);
                  this.authService.setName(response.body.user.name);
                  this.authService.setPhotoProfile(response.body.user.photo);
                  this.authService.setLastName(response.body.user.lastName);
                  this.authService.setSecondLastName(response.body.user.secondLastName);
                  this._suscriptionCanceled.setDateExpiration(response.body.user.suscriptionExpires);

                  if (!this.isOverlay){
                    this.activatedRoute.queryParamMap.subscribe(
                      (params) => {
                        if (params.get('returnUrl')){
                          const returnUrl = params.get('returnUrl');
                          this.router.navigateByUrl(returnUrl || '');
                        }
                        else{
                          this._suscription.setlastCompletedStep(response.body.stepsStatus || 0);
                          this._suscription.redirectToCorrectScreen(response.body.stepsStatus || 0);
                        }
                      }
                    );
                  }
                }
                else{
                  this._suscription.redirectToCorrectScreen(response.body.stepsStatus || 0);
                }
              } else {
                if (response.body.status == 412) {
                  this.passwordError = true;
                  this.messageError = 'Ha superado 3 intentos para ingresar a tu cuenta, el usuario se ha bloquedado.';
                  this.authService.setAttempts(true);
                  this.router.navigate(['../forgot-password'], {
                    relativeTo: this.activatedRoute,
                  });
                } else {
                  this.passwordError = true;
                  this.messageError = (response.body.hasOwnProperty('error')) ? response.body.error : 'No pudimos autenticarte, verifica tus datos';
                  this.authService.setAttempts(false);
                }
              }
            }
            else{
              this.passwordError = true;
              this.messageError = 'No pudimos autentificarte, el correo pertenece a una cuenta con un perfil distinto';
            }
          },
          (error) => {
            if (error.status === 412){
              this.messageError = 'Ha superado 3 intentos para ingresar a tu cuenta, el usuario se ha bloquedado.';
              this.authService.setAttempts(true);
            }
            else if (error.status === 401 && error?.error?.body?.message === 'Expiro suscripcion del usuario'){
              this.messageError = 'La suscripción ha expirado. Contacta al equipo de soporte';
            }
            else{
              this.messageError = 'No pudimos autenticarte, verifica tus datos';
            }
            this.passwordError = true;
          });

      } else if (this.isCandidate) {
        this.authService.loginCandidate(this.userEmail, this.userPassword).subscribe(
          (response) => {
            if (response.body.user && response.body.user.userRol === 'CANDIDATE'){
              if (response.body.status != 412 && response.body.status != 401){
                this.authService.setToken(response.headers.get('token'));
                this.authService.setUser(JSON.stringify(response.body));
                localStorage.setItem('token', response.headers.get('token'));
                this.authService.setRecruiterId(response.body.recruiterId);
                this.authService.setEmail(response.body.user.email);
                this.authService.setName(response.body.user.name);
                this.authService.setPhotoProfile(response.body.user.photo);
                this.authService.setLastName(response.body.user.lastName);
                this.authService.setSecondLastName(response.body.user.secondLastName);
              }else{
                if (response.body.status == 412){
                  this.passwordError = true;
                  this.messageError = 'Ha superado 3 intentos para ingresar a tu cuenta, el usuario se ha bloquedado.';
                  this.authService.setAttempts(true);
                  this.router.navigate(['../forgot-password'], {
                    relativeTo: this.activatedRoute,
                  });

                }else{
                  this.passwordError = true;
                  this.messageError = 'No pudimos autenticarte, verifica tus datos';
                  this.authService.setAttempts(false);
                }

              }
            }
            else{
              this.passwordError = true;
              this.messageError = 'No pudimos autentificarte, el correo pertenece a una cuenta con un perfil distinto';
            }


          },
          (error) => {
            console.error(error.status);
            this.passwordError = true;
            this.messageError = 'No pudimos autenticarte, verifica tus datos';
          });
      } else {
        this.passwordError = true;
        this.messageError = 'Debes ingresar una contraseña';
      }
    } else {
      this.messageError = 'Debes ingresar una cuenta de correo electrónico';
    }
  }

  redirectoToStep = (step: number) => {

    switch (step) {
      case 1:
        this.router.navigateByUrl('/select-your-plan/true');
        break;
      case 2:
        this.router.navigateByUrl('/select-your-plan/true');
        break;
      case 3:
        this.router.navigateByUrl('/company-data');
        break;
      case 4:
        this.router.navigateByUrl('/invite-your-team');
        break;
      case 5:
      case 6:
        this.router.navigate(['../dashboard'], {
          relativeTo: this.activatedRoute,
        });
        break;
      default:
        this.router.navigateByUrl('/register-guest-recruiter/true');
        break;

    }

  }

  getCurrentDate = () => {
    this._suscriptionCanceled.getCurrentDateService().subscribe(
      (response) => {
        if (response.status == 200 && response.body != null){
          const date = (response.body.includes('/')) ? response.body : new Date().toISOString().slice(0, 10).replace(/-/g, '/');
          this._suscriptionCanceled.setCurrentDate(date);
        }
      }
    );
  }

  navigate(url: string){
    this.router.navigateByUrl(url);
  }
}



