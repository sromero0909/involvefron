import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RestorePasswordCandidateComponent } from './restore-password-candidate.component';

describe('RestorePasswordCandidateComponent', () => {
  let component: RestorePasswordCandidateComponent;
  let fixture: ComponentFixture<RestorePasswordCandidateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RestorePasswordCandidateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RestorePasswordCandidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
