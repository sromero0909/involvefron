import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-birthday',
  templateUrl: './birthday.component.html',
  styleUrls: ['./birthday.component.scss']
})
export class BirthdayComponent implements OnInit {
  dataBirth = '';

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onChagesBirth() {
    console.log("Birth date: " + this.dataBirth);
    this.router.navigateByUrl('register-candidato/genero');

  }

}
