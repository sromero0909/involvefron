import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-job-location',
  templateUrl: './job-location.component.html',
  styleUrls: ['./job-location.component.scss']
})
export class JobLocationComponent implements OnInit {
  
  arrayPais:Array<any>;
  arrayCiudad:Array<any>;
  //Respuesta
  respuesta: String ="";
  constructor(private router: Router,private activatedRoute: ActivatedRoute) {
    this.arrayPais = ["Argentina","México",'Brasil']
    this.arrayCiudad = ["Ciudad de México","Guadalajara",'Puebla']
   }

  ngOnInit(): void {
  }

  onChangeLocation() {
    this.router.navigateByUrl('live-there');
  }

}
