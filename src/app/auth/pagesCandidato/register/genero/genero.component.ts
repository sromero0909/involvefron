import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-genero',
  templateUrl: './genero.component.html',
  styleUrls: ['./genero.component.scss']
})
export class GeneroComponent implements OnInit {
  botonMujer: string = "botonesGrises";
  botonHombre: string = "botonesGrises";
  botonNobinario: string = "botonesGrises";
  respuesta: String = "";
  selectedGenere = "";
  botonContinuar="botonContinuarGris";
  constructor(private router: Router,private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
  }

  botonSeleccionado(op: string): void {
    this.resetBotones();
    this.respuesta = op;
    console.log("es op",op);
    switch (op) {
      case "mujer":
        this.botonMujer ="botonesVerdes"
        break;
      case "hombre":
        this.botonHombre ="botonesVerdes"
        break;
      case "nobinario":
        this.botonNobinario ="botonesVerdes"
        break;
      default:
        this.resetBotones();
    }
    this.botonContinuar ="botonContinuarVerde"

  }

  resetBotones():void {
    this.botonMujer  = "botonesGrises";
    this.botonHombre = "botonesGrises";
    this.botonNobinario = "botonesGrises";
  }

  botonSiguientePantalla():void {

    if(this.respuesta != ""){
      this.redirectToPerfilProfesional()
      console.log("entra",this.respuesta);
    }
  }

  onChangeSeleted = (event: any) => {
    // console.log(event.value);
    this.router.navigateByUrl("marital-status");
  }

  redirectToPerfilProfesional(){
    this.router.navigate(['../perfil-profesional'], {
      relativeTo: this.activatedRoute,
    });
  }

}
