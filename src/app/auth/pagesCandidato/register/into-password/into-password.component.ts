import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { RegisterCandidateService } from '../services/register-candidate/register-candidate.service';

@Component({
  selector: 'app-into-password',
  templateUrl: './into-password.component.html',
  styleUrls: ['./into-password.component.scss']
})
export class IntoPasswordComponent implements OnInit {

  // Variables para requisitos para activar la cuenta
  isLower: boolean = false;
  isUpper: boolean = false;
  isMinLenght: boolean = false;
  isANumber: boolean = false;
  isCorrect: boolean = false;
  notCorrect: boolean = false;
  activateAccount: boolean = false;

  public avisoPrivacidad: boolean = false;
  public termsConditions: boolean = false;
  public notifiInvitacion: boolean = false;
  

  //Expresiones Regulares
  regExpLower = /[a-z]/;
  regExpUpper = /[A-Z]/;
  regExpMinLenght = /.{8,}/;
  regExpANumber = /[0-9]/;

 public rulePassword: boolean = false;
 public userNewPassword: string = "";
 public userPasswordConfirm: string = "";

 // Variable para controlar si se muestra la contraseña
 showPassword: boolean = false;
 showPasswordConfirm: boolean = false;
 equalPassword: boolean = false;
 emptyPassword: boolean = false;
 emptyConfirmPassword: boolean = false;

 txtEye: string = "Mostrar";

 password: FormControl = new FormControl('', Validators.required);
 confirmPassword: FormControl = new FormControl('', Validators.required);

 // Token generado por el backend
 keyToken: string = "";
 passwordChanged: boolean = false;

 constructor(
   private authService: AuthService,
   private route: ActivatedRoute,
   private router: Router,
   private candidateService: RegisterCandidateService
   ) { }

 ngOnInit(): void {
  this.keyToken = this.route.snapshot.params['keyToken'];
 }

 validateNewPassword(): void{
   this.equalPassword = false;
   this.emptyPassword = false;
   this.emptyConfirmPassword = false;

   this.isLower = this.regExpLower.test(this.userNewPassword);
   this.isUpper = this.regExpUpper.test(this.userNewPassword);
   this.isMinLenght = this.regExpMinLenght.test(this.userNewPassword);
   this.isANumber = this.regExpANumber.test(this.userNewPassword);

   if (this.isLower && this.isANumber && this.isUpper && this.isMinLenght) {
     this.rulePassword = true;
   } else {
     this.rulePassword = false;
   }
 }

 sendPassword(): void{
   console.log('Send password');
   this.equalPassword = false;
   if((this.userNewPassword != null && this.userNewPassword != "") && (this.userPasswordConfirm != null && this.userPasswordConfirm != "")){
     if(this.userNewPassword == this.userPasswordConfirm){
       this.activateAccount = true;
       this.createPassword();
     } else{
       this.equalPassword = true;
     }
   } else {
     if(this.userNewPassword == ""){
       this.emptyPassword = true;
     } else {
       if(this.userPasswordConfirm == ""){
         this.emptyConfirmPassword = true;
       }
     }
   }
 }

 createPassword = () => {
  this.candidateService.createPassword(this.userNewPassword).subscribe( (response: any) => {
    console.log(response);
  } );
 }

//**Metodo para registrar la cuenta */
 createAccount(): void{
   /**Validamos los checks de terminos y condiciones */
   console.log(this.avisoPrivacidad);
   console.log(this.termsConditions);
   console.log(this.notifiInvitacion);

  
   //lamamos al servicio para reestablecer contraseña
   this.authService.setToken(this.keyToken);
   this.authService.activateAccount(this.notifiInvitacion, this.userNewPassword,this.keyToken).subscribe(
     (response) => {
        //alert("Ahora, puedes intentar iniciar sesión");//confirmar que pantalla va a ir
       this.isCorrect = true;
       this.activateAccount = false;
     },
     (error) => {
       console.log(error.error.code, error.error.message);
       console.log("El correo es incorrecto");
       this.notCorrect = true;
       this.activateAccount = false;
     });
 }


 finishAccount() {
  console.log(this.avisoPrivacidad);
   console.log(this.termsConditions);
   console.log(this.notifiInvitacion);

   this.candidateService.acceptConditions(this.notifiInvitacion, this.avisoPrivacidad, this.termsConditions).subscribe( (response: any) => {
    console.log(response);
  } );

  this.router.navigate(['/register-success']);
 }

}
