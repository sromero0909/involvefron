import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntoPasswordComponent } from './into-password.component';

describe('IntoPasswordComponent', () => {
  let component: IntoPasswordComponent;
  let fixture: ComponentFixture<IntoPasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntoPasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntoPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
