import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-perfil-profesional',
  templateUrl: './perfil-profesional.component.html',
  styleUrls: ['./perfil-profesional.component.scss']
})
export class PerfilProfesionalComponent implements OnInit {
  public userEmail: string = "";
  botonContinuar:string = 'botonContinuarGris'
  //Respuesta
  respuesta: String ="";
  constructor(private router: Router,private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
  }

  botonSiguienteVentana():void {
    if(this.respuesta != "" && this.respuesta.length > 1) {
      this.redirectToPerfilProfesional()
    }
  }

  validaTexto():void {
    console.log("respuesta",this.respuesta.length);

    if(this.respuesta != "" && this.respuesta.length > 1) {
      this.botonContinuar ="botonContinuarVerde Continuar"
    }else{
      this.botonContinuar ="botonContinuarGris"
    }
  }

  redirectToPerfilProfesional(){
    this.router.navigateByUrl("job-location");
    //job-location
    // this.router.navigate(['../puesto'], {
    //   relativeTo: this.activatedRoute,
    // });
  }

}
