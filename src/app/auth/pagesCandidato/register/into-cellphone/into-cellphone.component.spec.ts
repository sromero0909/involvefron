import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntoCellphoneComponent } from './into-cellphone.component';

describe('IntoCellphoneComponent', () => {
  let component: IntoCellphoneComponent;
  let fixture: ComponentFixture<IntoCellphoneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntoCellphoneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntoCellphoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
