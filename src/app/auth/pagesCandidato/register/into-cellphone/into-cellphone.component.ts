import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../../services/auth.service';
import { NgForm } from '@angular/forms';
import { RegisterCandidateService } from '../services/register-candidate/register-candidate.service';

@Component({
  selector: 'app-into-cellphone',
  templateUrl: './into-cellphone.component.html',
  styleUrls: ['./into-cellphone.component.scss']
})
export class IntoCellphoneComponent implements OnInit {

  url = "https://flagpedia.net/data/flags/normal/mx.png";
  keyToken: string = "";
  phoneNumber = '';
  codeNumberPhone = '';
  errorMsg = false;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private candidateService: RegisterCandidateService) {
  }

  ngOnInit(): void {
    this.keyToken = this.route.snapshot.params['keyToken'];
    console.log(this.keyToken);
    this.authService.setToken(this.keyToken)
  }

  onSave = (formCell: NgForm) => {
    console.log('tam num:' + this.codeNumberPhone.length);
    if(this.codeNumberPhone.length<13) {
      this.errorMsg = true;
    } else {
      this.errorMsg = false;
      //navigate validate-sms
      this.candidateService.sendSms(this.codeNumberPhone).subscribe( (response: any) => {
        console.log(response);
      } );
      //this.router.navigateByUrl("/validate-sms",{'algo'});
      this.router.navigate(['/validate-sms/',this.codeNumberPhone]);
    }
    console.log(this.codeNumberPhone);

  }

  telInputObject(obj: any) {
    console.log(obj);
    obj.setCountry('mx');
  }

  getNumber(obj: any) {
    console.log('getNumber:');
    this.codeNumberPhone = obj;
    console.log(obj);
  }

  hasError(obj: any) {
    console.log('hasError');
    console.log(obj);
  }

  onCountryChange(obj: any) {
    console.log('onCountryChange');
    console.log(obj);
  }

}
