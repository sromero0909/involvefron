import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router } from '@angular/router';
@Component({
  selector: 'app-puesto',
  templateUrl: './puesto.component.html',
  styleUrls: ['./puesto.component.scss']
})
export class PuestoComponent implements OnInit {
  
  arrayPais:Array<any>;
  arrayCiudad:Array<any>;
  //Respuesta
  respuesta: String ="";
  constructor(private router: Router,private activatedRoute: ActivatedRoute) {
    this.arrayPais = ["Argentina","México",'Brasil']
    this.arrayCiudad = ["Ciudad de México","Guadalajara",'Puebla']
   }

  ngOnInit(): void {
  }

  redirectToSueldo(){
    this.router.navigate(['../sueldo'], {
      relativeTo: this.activatedRoute,
    });
  }

}
