import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-marital-status',
  templateUrl: './marital-status.component.html',
  styleUrls: ['./marital-status.component.scss']
})
export class MaritalStatusComponent implements OnInit {

  constructor( private router: Router ) { }

  ngOnInit(): void {
  }

  onChangeMaritalStatus = () => {
    // nationality
    this.router.navigateByUrl("nationality");
  }

}
