import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-candidate-location',
  templateUrl: './candidate-location.component.html',
  styleUrls: ['./candidate-location.component.scss']
})
export class CandidateLocationComponent implements OnInit {

  arrayPais:Array<any>;
  arrayCiudad:Array<any>;
  //Respuesta
  respuesta: String ="";
  constructor(private router: Router,private activatedRoute: ActivatedRoute) {
    this.arrayPais = ["Argentina","México",'Brasil']
    this.arrayCiudad = ["Ciudad de México","Guadalajara",'Puebla']
   }

  ngOnInit(): void {
  }

  onChangeCandidateLocation() {
    this.router.navigateByUrl("register-candidato/sueldo");
  }

}
