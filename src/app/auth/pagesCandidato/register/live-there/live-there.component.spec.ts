import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveThereComponent } from './live-there.component';

describe('LiveThereComponent', () => {
  let component: LiveThereComponent;
  let fixture: ComponentFixture<LiveThereComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LiveThereComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveThereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
