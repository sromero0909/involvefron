import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-live-there',
  templateUrl: './live-there.component.html',
  styleUrls: ['./live-there.component.scss']
})
export class LiveThereComponent implements OnInit {

  arrayLivethere : Array<any>;

  constructor(private router: Router,private activatedRoute: ActivatedRoute) {
    this.arrayLivethere = ["Si", "No"]
  }

  ngOnInit(): void {
  }

  onChangelive() {
    this.router.navigateByUrl("candidate-location");
  }

}
