import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-first-job',
  templateUrl: './first-job.component.html',
  styleUrls: ['./first-job.component.scss']
})
export class FirstJobComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onChangeFirstJob() {
    this.router.navigateByUrl("register-candidato/perfil-profesional");
  }

}
