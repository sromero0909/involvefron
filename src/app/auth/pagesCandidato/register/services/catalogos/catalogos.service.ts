import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CatalogosService {

  constructor(private http: HttpClient) { }

  nationalitySearch(search:string): Observable<any>{
    return this.http.post(`${environment.restPrefix}/catalog/nationality/search?search=${search}`, null, {observe: 'response'});
  }

  nationalityAll(): Observable<any>{
    return this.http.get(`${environment.restPrefix}/catalog/nationality/`);
  }
}
