import { Injectable } from '@angular/core';
import { Observable } from 'rxjs-compat/Observable';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegisterCandidateService {

  constructor(
    private http: HttpClient
  ) { }

  sendSms(phone:string): Observable<any>{
    return this.http.post(`${environment.restPrefix}/verify/send-sms?phone=${phone}`, null, {observe: 'response'});
  }

  checkCodeSms(code: string) {
    return this.http.post(`${environment.restPrefix}/verify/check-code-sms?code=${code}`, null, {observe: 'response'});
  }

  checkEmail() {
    return this.http.put(`${environment.restPrefix}/verify/check-email`, null, {observe: 'response'});
  }

  createPassword( password: string ) {
    return this.http.put(`${environment.restPrefix}/registry/create-pass?password=${password}`, null, {observe: 'response'});
  }

  acceptConditions( newsletter: boolean, privacity: boolean, terms: boolean ){
    return this.http.put(`${environment.restPrefix}/registry/accept-conditions?newsletter=${newsletter}&privacity=${privacity}&terms=${terms}`, null, {observe: 'response'});
  }
}
