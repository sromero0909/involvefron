import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterCandidatoComponent implements OnInit {
  //nombre:string = '';
  respuesta: string = '';
  //apellidoPaterno: string = '';
  //apellidoMaterno: string ='';
  formularioDatosPersonales: FormGroup;
  constructor(private formBuilder: FormBuilder, private router: Router,) {
    this.formularioDatosPersonales = this.formBuilder.group({
      nombre: [{ value: '', disabled: false }, [Validators.required]],
      apellidoPaterno: [{ value: '', disabled: false }, [Validators.required]],
      apellidoMaterno: [{ value: '', disabled: false }, [Validators.required]],
    });
   }

  ngOnInit(): void {
  }

  pageContinue = () => {
    // routerLink="./genero"
    console.log('Name: ' + this.nombre);
    console.log('Paterno: ' + this.apellidoPaterno);
    console.log('Materno: ' + this.apellidoMaterno);
    this.router.navigateByUrl('birthday');
  }

  get nombre() {
    return this.formularioDatosPersonales.controls.nombre.value;
  }

  get apellidoPaterno() {
    return this.formularioDatosPersonales.controls.apellidoPaterno.value;
  }

  get apellidoMaterno() {
    return this.formularioDatosPersonales.controls.apellidoMaterno.value;
  }

}
