import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CatalogosService } from '../services/catalogos/catalogos.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nationality',
  templateUrl: './nationality.component.html',
  styleUrls: ['./nationality.component.scss']
})
export class NationalityComponent implements OnInit {
  nationalities: string[] = [];

  constructor( private catalogService: CatalogosService, private router: Router ) {
      this.getNacionality();
   }

  ngOnInit(): void {
  }

  getNacionality() {
    // nationalityAll
    this.catalogService.nationalityAll().subscribe( ( response ) => {
      console.log('getNacionality');
      console.log(response.spanishName);
      this.nationalities = response;
    });
  }

  onChangeNationality() {
    this.router.navigateByUrl("first-job");
  }

  

}
