import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterCandidateService } from '../services/register-candidate/register-candidate.service';

@Component({
  selector: 'app-validate-sms',
  templateUrl: './validate-sms.component.html',
  styleUrls: ['./validate-sms.component.scss']
})
export class ValidateSmsComponent implements OnInit {
  phone = '';
  oneCode = '';
  twoCode = '';
  threeCode = '';
  fourCode = '';

  constructor( private route: ActivatedRoute, private router: Router, private candidateService: RegisterCandidateService) {
    this.phone = this.route.snapshot.params['phone'];
   }

  ngOnInit(): void {
  }

  validateSms = () => {
    console.log('codigo sms');
    console.log(this.oneCode + this.twoCode + this.threeCode + this.fourCode );

    this.candidateService.checkCodeSms(this.oneCode + this.twoCode + this.threeCode + this.fourCode).subscribe( (response: any) => {
      console.log(response);
    } );

    this.candidateService.checkEmail().subscribe( (response: any) => {
      console.log('check email...');
      console.log(response);
    } );

    this.router.navigateByUrl("candidate-count");
    //candidate-count
  }

}
