import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidateSmsComponent } from './validate-sms.component';

describe('ValidateSmsComponent', () => {
  let component: ValidateSmsComponent;
  let fixture: ComponentFixture<ValidateSmsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidateSmsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidateSmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
