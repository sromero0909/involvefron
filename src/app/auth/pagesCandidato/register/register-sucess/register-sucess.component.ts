import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-sucess',
  templateUrl: './register-sucess.component.html',
  styleUrls: ['./register-sucess.component.scss']
})
export class RegisterSucessComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onClickOk() {
    //register-candidato
    this.router.navigate(['/register-candidato']);
  }

}
