import { compileDeclareComponentFromMetadata } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../services/auth.service';


@Component({
  selector: 'app-activate-candidate-account',
  templateUrl: './activate-candidate-account.component.html',
  styleUrls: ['./activate-candidate-account.component.scss']
})
export class ActivateCandidateAccountComponent implements OnInit {
  showView = -1;
  keyToken = '';

  constructor(
    private router: ActivatedRoute,
    private authService: AuthService,) { }

  ngOnInit(): void {
    this.accountActivate();
  }


  accountActivate() {
    this.keyToken = this.router.snapshot.params['keyToken'];
    this.authService.setToken(this.keyToken);
    this.authService.setToken(this.keyToken);
    this.authService.verifyEmailCandidate (this.keyToken).subscribe(
      (response: any) => {
        const { code, data} = response;
        if( data !== '' && code == 200) {
          this.showView = 0;
        } else {
          this.showView = 1;
        }
      },
      (error) => {
        this.showView = 3;
        console.log(error.error.code, error.error.message);
      });
  }
}
