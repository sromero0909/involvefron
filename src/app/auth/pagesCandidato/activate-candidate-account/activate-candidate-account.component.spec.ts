import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivateCandidateAccountComponent } from './activate-candidate-account.component';

describe('ActivateCandidateAccountComponent', () => {
  let component: ActivateCandidateAccountComponent;
  let fixture: ComponentFixture<ActivateCandidateAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivateCandidateAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivateCandidateAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
