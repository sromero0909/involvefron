import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ocr',
  templateUrl: './ocr.component.html',
  styleUrls: ['./ocr.component.scss']
})
export class OcrComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onClickGo() {
    //upload-cv
    this.router.navigateByUrl("upload-cv");
  }

}
