import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroCvCardComponent } from './registro-cv-card.component';

describe('RegistroCvCardComponent', () => {
  let component: RegistroCvCardComponent;
  let fixture: ComponentFixture<RegistroCvCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroCvCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroCvCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
