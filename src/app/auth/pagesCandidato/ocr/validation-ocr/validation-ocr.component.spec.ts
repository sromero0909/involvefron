import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationOcrComponent } from './validation-ocr.component';

describe('ValidationOcrComponent', () => {
  let component: ValidationOcrComponent;
  let fixture: ComponentFixture<ValidationOcrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidationOcrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationOcrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
