import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroCvComponent } from './registro-cv.component';

describe('RegistroCvComponent', () => {
  let component: RegistroCvComponent;
  let fixture: ComponentFixture<RegistroCvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroCvComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroCvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
