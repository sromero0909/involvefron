import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterGuestRecruiterComponent } from './register-guest-recruiter.component';

describe('RegisterGuestRecruiterComponent', () => {
  let component: RegisterGuestRecruiterComponent;
  let fixture: ComponentFixture<RegisterGuestRecruiterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterGuestRecruiterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterGuestRecruiterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
