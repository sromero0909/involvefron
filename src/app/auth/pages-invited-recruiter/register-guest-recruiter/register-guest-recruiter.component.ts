import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { ValidatorsService } from 'src/app/commons/validatorsService/validators.service';
import { MainNavModel } from 'src/app/modulos/dashboardReclutadores/talent/models/main-nav.model';
import { AuthService } from 'src/app/services/auth.service';

import { ToastService } from 'src/app/services/commons/toast.setvice';
import { ProfileService } from 'src/app/services/reclutador/configuration/profile.service';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { RegisterService } from 'src/app/services/reclutador/login/register.service';

const CURRENT_PAGE_STEP = 2;

@Component({
  selector: 'app-register-guest-recruiter',
  templateUrl: './register-guest-recruiter.component.html',
  styleUrls: ['./register-guest-recruiter.component.scss']
})
export class RegisterGuestRecruiterComponent implements OnInit {
  mainNavData: MainNavModel;
  nombre = '';
  respuesta = '';
  apellidoPaterno = '';
  apellidoMaterno = '';
  widthValor2 = '0%';
  idOpenPay = '';
  formularioDatosPersonales: FormGroup;
  statusStep = 2;


  recruiter = false;
  constructor(private formBuilder: FormBuilder,
    private profileService: ProfileService,
    public toastService: ToastService,
    private activatedRoute: ActivatedRoute,
    private route: Router,
    private authService: AuthService,
    private _register: RegisterService,
    private _suscription: SuscriptionService,
    private validatorsService: ValidatorsService) {
    this.formularioDatosPersonales = this.formBuilder.group({
      nombre: [{ value: '', disabled: false }, [Validators.required, Validators.minLength(1), Validators.pattern(this.validatorsService.allCharacters),this.validatorsService.noWhiteSpaces]],
      apellidoPaterno: [{ value: '', disabled: false }, [Validators.required,Validators.pattern(this.validatorsService.allCharacters),this.validatorsService.noWhiteSpaces]],
      apellidoMaterno: [{ value: '', disabled: false }, [Validators.required,Validators.pattern(this.validatorsService.allCharacters),this.validatorsService.noWhiteSpaces]],
    });
  }

  ngOnInit(): void {
    const flagRoute = this.activatedRoute.snapshot.paramMap.get('flag');
    this.recruiter = (flagRoute == 'false') ? false : true;
    console.log('re', this.recruiter)
    localStorage.setItem('execute', 'no');
    if (this.recruiter) {
      this._suscription.validateCurrentStatus(CURRENT_PAGE_STEP);
    }
    this.mainNavData = {
      search: true,
      logo: true,
      iconBack: false,
    }
  }

  onClickSubmit = () => {
    if (this.formularioDatosPersonales.invalid) {
      this.formularioDatosPersonales.markAllAsTouched();
    } else {
      if (this.recruiter) {
        this.createUserOpenPay();
      } else {
        this.udateRecruiter();
      }
    }
  }

  udateRecruiter = () => {
    this.profileService.updateRecruiterPatch(this.getParams()).subscribe(
      (response: any) => {
        if (response.status === 200 && response.body !== null && !response.body.hasOwnProperty('error')) {
          this.authService.setUser(JSON.stringify(response.body));
          this.profileService.setIdRecruiterInvited(response.body.recruiterId);
          this.profileService.setUrlPhotoInvited(response.body.user.photo);
          this.profileService.setNameCompayInvited((response.body.company != null) ? response.body.company.company : null);

          this.widthValor2 = '25%';
          if (this.recruiter) {
            this._suscription.setlastCompletedStep(response.body.stepsStatus);
            this._suscription.redirectToCorrectScreen(response.body.stepsStatus);
          } else {
            this.route.navigateByUrl('/upload-guest-photo', { replaceUrl: true });
          }
        } else {
          this.toastService.showMessageError(response.body.error);
        }
      },
      (error) => {
        this.toastService.showMessageError(error.error);
      }
    );
  }

  getParams = () => {
    const body =
      [
        {
          op: 'replace',
          path: '/user/name',
          value: this.formularioDatosPersonales.get('nombre')?.value
        },
        {
          op: 'replace',
          path: '/user/lastName',
          value: this.formularioDatosPersonales.get('apellidoPaterno')?.value
        },
        {
          op: 'replace',
          path: '/user/secondLastName',
          value: this.formularioDatosPersonales.get('apellidoMaterno')?.value
        }
      ];
    if (this.recruiter) {
      body.push(
        {
          op: 'replace',
          path: '/stepsStatus',
          value: CURRENT_PAGE_STEP
        },
        {
          op: 'replace',
          path: '/user/userOpenPay',
          value: this.idOpenPay
        }
      );
    }
    return body;
  }

  createUserOpenPay = () => {
    const user = JSON.parse(this.authService.getUser());
    // if (user.user.email === undefined){
    //   user = JSON.parse(user);
    // }
    const params = {
      email: user.user.email,
      externalId: user.recruiterId,
      lastName: `${this.formularioDatosPersonales.value.apellidoPaterno} ${this.formularioDatosPersonales.value.apellidoMaterno}`,
      name: this.formularioDatosPersonales.value.nombre
    };
    this._register.createUserOpenPay(params).subscribe(
      (response: any) => {
        if (response.status === 200 && response.body !== null) {
          this.idOpenPay = response.body.id;
          this.authService.setName(this.formularioDatosPersonales.value.nombre);
          this.authService.setLastName(this.formularioDatosPersonales.value.apellidoPaterno);
          this.authService.setSecondLastName(this.formularioDatosPersonales.value.apellidoMaterno);
          this.udateRecruiter();
        } else {
          this.toastService.showMessageError('Error al actualizar los datos');
        }
      },
      (error) => {
        this.toastService.showMessageError(error.error);
      }
    );
  }

  validateName() {
    const text = this.formularioDatosPersonales.get('nombre')?.value;
    if (text.trim().length == 0) {
      this.formularioDatosPersonales.get('nombre')?.setValue('');
    }
  }

  validateLasName() {
    const text = this.formularioDatosPersonales.get('apellidoPaterno')?.value;
    if (text.trim().length == 0) {
      this.formularioDatosPersonales.get('apellidoPaterno')?.setValue('');
    }
  }

  validateSecondLastName() {
    const text = this.formularioDatosPersonales.get('apellidoMaterno')?.value;
    if (text.trim().length == 0) {
      this.formularioDatosPersonales.get('apellidoMaterno')?.setValue('');
    }
  }
}
