import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateGuestPasswordComponent } from './create-guest-password.component';

describe('CreateGuestPasswordComponent', () => {
  let component: CreateGuestPasswordComponent;
  let fixture: ComponentFixture<CreateGuestPasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateGuestPasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGuestPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
