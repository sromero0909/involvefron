import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastService } from 'src/app/services/commons/toast.setvice';
import { ProfileService } from 'src/app/services/reclutador/configuration/profile.service';
import { AuthService } from '../../../services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NoticeofPrivacyComponent } from 'src/app/commons/modals/noticeof-privacy/noticeof-privacy.component';
import { MESSAGES } from 'src/app/services/models/messages';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { RegisterService } from 'src/app/services/reclutador/login/register.service';

const CURRENT_PAGE_STEP = 1;

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.scss']
})
export class TermsAndConditionsComponent implements OnInit {

  // Token generado por el backend
  statusStep = 1;
  keyToken = '';
  nameCompany = '';
  isCorrect = false;
  notCorrect = false;
  activateAccount = true;
  avisoPrivacidad = false;
  termsConditions = false;
  notifiInvitacion = false;
  recruiter = false;
  @Output() btnAcept = new EventEmitter<boolean>();
  formTerms!: FormGroup;

  constructor(private authService: AuthService,
              private route: Router,
              private formBuilder: FormBuilder,
              private profileService: ProfileService,
              public toastService: ToastService,
              private modalService: NgbModal,
              private activatedRoute: ActivatedRoute,
              private _suscription: SuscriptionService,) { 
      this.initializeForm();
  }

  ngOnInit(): void {
    localStorage.setItem('execute', 'no');
    const flagRoute = this.activatedRoute.snapshot.paramMap.get('flag');
    this.recruiter = (flagRoute === 'false') ? false : true;
    if (this.recruiter){
      this._suscription.validateCurrentStatus(CURRENT_PAGE_STEP);
    }
  }


  initializeForm = () => {
    this.formTerms = this.formBuilder.group({
      avisoPrivacidad: [{ value: false, disabled: false }, [Validators.required, Validators.requiredTrue]],
      termsConditions: [{ value: false, disabled: false }, [Validators.requiredTrue]],
      cookiesPolicy: [{value: false, disabled: false}, [Validators.required, Validators.requiredTrue]],
      notifiInvitacion: [{ value: false, disabled: false }, []],
    });
  }

  onClickSubmit = () => {
    if (this.formTerms.invalid) {
      this.formTerms.get('avisoPrivacidad')?.markAsTouched();
      this.formTerms.get('termsConditions')?.markAsTouched();
      this.formTerms.get('cookiesPolicy')?.markAsTouched();
      this.formTerms.get('notifiInvitacion')?.markAsTouched();
    } else {
      this.udateRecruiter();
    }
  }

  udateRecruiter = () => {
    this.profileService.updateRecruiterPatch(this.getParams()).subscribe(
      (response: any) => {
        if (response.status == 200 && response.body != null && !response.body.hasOwnProperty('error')) {
          if (this.recruiter) {
            this._suscription.setlastCompletedStep(response.body.stepsStatus);
            this._suscription.redirectToCorrectScreen(response.body.stepsStatus);
          } else {
            this.isCorrect = true;
            this.activateAccount = false;
            this.nameCompany = this.profileService.getNameCompayInvited();
          }
        }else{
          this.toastService.showMessageError(response.body.error);
        }
      },
      (error) => {
        this.toastService.showMessageError(error.error);
      }
    );
  }

  getParams = () => {
    const body = [
      {
        op: 'replace',
        path: '/user/acceptPrivacy',
        value: this.formTerms.get('avisoPrivacidad')?.value
      },
      {
        op: 'replace',
        path: '/user/acceptTerms',
        value: this.formTerms.get('termsConditions')?.value
      },
      {
        op: 'replace',
        path: '/user/acceptCookies',
        value: this.formTerms.get('cookiesPolicy')?.value
      },
      {
        op: 'replace',
        path: '/user/acceptNewsletter',
        value: this.formTerms.get('notifiInvitacion')?.value
      },
      {
        op: 'replace',
        path: '/stepsStatus',
        value: (this.recruiter ? CURRENT_PAGE_STEP : 8)
      }
    ];
    return body;
  }

  openModalAviso = (event: boolean) => {
    this.formTerms.controls.avisoPrivacidad.setValue(event);
  }

  openModalTerminos = (event: boolean) => {
    this.formTerms.controls.termsConditions.setValue(event);
  }

  openModalCookies = (event: boolean) => {
    this.formTerms.controls.cookiesPolicy.setValue(event);
  }

  get isformValid(): boolean{
    return this.formTerms.valid;
  }

  // redirectTo = () => {
  //   if (this.recruiter) {
  //     this.route.navigateByUrl("/select-your-plan/true",{ replaceUrl: true,state:{k:'/select-your-plan/true'} });
  //   } else {
  //     this.route.navigateByUrl("/upload-guest-photo");
  //   }
  // }

}
