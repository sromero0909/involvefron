import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { ToastService } from 'src/app/services/commons/toast.setvice';
import { CompanyService } from 'src/app/services/reclutador/configuration/company.service';
import { ProfileService } from 'src/app/services/reclutador/configuration/profile.service';

@Component({
  selector: 'app-upload-guest-photo',
  templateUrl: './upload-guest-photo.component.html',
  styleUrls: ['./upload-guest-photo.component.scss']
})
export class UploadGuestPhotoComponent implements OnInit {
  existPhoto = this.profileService.getUrlPhotoyInvited()
  photo: any = (this.existPhoto == null || this.existPhoto == undefined || this.existPhoto == "") ? "assets/img/dashboard/vacantes/sinFotoPerfil.svg" : this.profileService.getUrlPhotoyInvited();
  imgTrash: string = ""
  widthValor3: string = '0'
  btnDisabled: boolean = false;

  @ViewChild('fileUploadCompany', { static: false })
  InputVar!: ElementRef;

  constructor(public toastService: ToastService,
    private companyService: CompanyService,
    private profileService: ProfileService) {
    this.imgTrash = (this.photo == null || String(this.photo).includes('assets')) ? 'basuraGris.svg' : 'basura.svg';
    this.btnDisabled = (this.imgTrash == 'basuraGris.svg') ? false : true;
  }

  ngOnInit(): void {
  }

  onFileSelected = (event: any) => {
    const file: File = event.target.files[0];
    if (file) {
      if (String(file.type).includes('image')) {
        if (file.size <= 2000000) {
          let formData = new FormData();
          formData.append("file", file, file.name);
          this.profileService.upLoadPhoto('URL_PHOTO', formData).subscribe(
            (response: any) => {
              this.InputVar.nativeElement.value = "";
              if (response != null && response.code == 201) {
                this.getCompany()
                this.widthValor3 = '25%'
              } else {
                this.toastService.showMessageError("Ocurrió in error al actualizar la foto");
              }
            },
            (error) => {
              this.toastService.showMessageError(error.error)
            });
        } else {
          this.InputVar.nativeElement.value = "";
          this.toastService.showMessageError("Elige una foto de hasta 2 MB");
        }
      } else {
        this.InputVar.nativeElement.value = "";
        this.toastService.showMessageError("El archivo seleccionado no es una imagen");
      }
    }

  }

  deletePhoto = () => {
    if (this.photo != null && !String(this.photo).includes('assets')) {
      this.profileService.deletePhoto().subscribe(
        (response: any) => {
          if (response != null && response.code == 200) {
            this.getCompany()
            this.widthValor3 = '0%'
          } else {
            this.toastService.showMessageError("Ocurrió in error al actualizar la foto");
          }
        },
        (error) => {
          this.toastService.showMessageError(error.error);
        })
    }
  }

  getCompany = () => {
    this.profileService.getRecruiter(this.profileService.getIdReruiterInvited()).subscribe(
      (response: any) => {
        if (response != null) {
          let client = response
          this.photo = (client.user.photo == null) ? 'assets/img/dashboard/vacantes/sinFotoPerfil.svg' : client.user.photo + "?" + (new Date()).getTime()
          this.imgTrash = (this.photo == null || this.photo == "assets/img/dashboard/vacantes/sinFotoPerfil.svg") ? 'basuraGris.svg' : 'basura.svg';
          this.btnDisabled = (this.imgTrash == 'basuraGris.svg') ? false : true;
        }
      },
      (error) => {
        this.toastService.showMessageError(error.error);
      }
    )
  }

}
