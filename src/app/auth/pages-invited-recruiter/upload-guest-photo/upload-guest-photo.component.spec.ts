import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadGuestPhotoComponent } from './upload-guest-photo.component';

describe('UploadGuestPhotoComponent', () => {
  let component: UploadGuestPhotoComponent;
  let fixture: ComponentFixture<UploadGuestPhotoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadGuestPhotoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadGuestPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
