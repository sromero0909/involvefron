import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RestorePasswordComponent } from './pages/restore-password/restore-password.component';
import { ScreenConfirmComponent } from './pages/screen-confirm/screen-confirm.component';
import { HttpClientModule } from '@angular/common/http';
import { NavBarComponent } from './pagesCandidato/nav-bar/nav-bar.component';
import { RegisterCandidatoComponent } from './pagesCandidato/register/register.component';
import { GeneroComponent } from './pagesCandidato/register/genero/genero.component';
import { PerfilProfesionalComponent } from './pagesCandidato/register/perfil-profesional/perfil-profesional.component';
import { PuestoComponent } from './pagesCandidato/register/puesto/puesto.component';
import { ActivateRecuitreAccountComponent } from './pages/activate-recuitre-account/activate-recuitre-account.component';
import { moduleSharedModule } from '../commons/commons.module';
import { SueldoComponent } from './pagesCandidato/register/sueldo/sueldo.component';
import { CreateCandidateAccountComponent } from './pages/create-candidate-account/create-candidate-account.component';
import { RestorePasswordCandidateComponent } from './pagesCandidato/restore-password-candidate/restore-password-candidate.component';
import { ViewTestComponent } from './pages/view-test/view-test.component';
import { RestorePasswordAttemptsComponent } from './pages/restore-password-attempts/restore-password-attempts.component';
import { ActivateCandidateAccountComponent } from './pagesCandidato/activate-candidate-account/activate-candidate-account.component';
import { TermsComponent } from './pages/terms/terms.component';
import { PrivacyComponent } from './pages/privacy/privacy.component';
import { CreateGuestPasswordComponent } from './pages-invited-recruiter/create-guest-password/create-guest-password.component';
import { RegisterGuestRecruiterComponent } from './pages-invited-recruiter/register-guest-recruiter/register-guest-recruiter.component';
import { UploadGuestPhotoComponent } from './pages-invited-recruiter/upload-guest-photo/upload-guest-photo.component';
import { InternalizacionModule } from '../commons/internalizacion/internalizacion.module';
import { TermsAndConditionsComponent } from './pages-invited-recruiter/terms-and-conditions/terms-and-conditions.component';
import { SideNavRegisterComponent } from './pages/side-nav-register/side-nav-register.component';

import { SummaryOfYourOrderComponent } from './pages/summary-of-your-order/summary-of-your-order.component';
import { StartPlanComponent } from './pages/start-plan/start-plan.component';

import { SetUpPaymentComponent } from './components/set-up-payment/set-up-payment.component';
import { StandbyScreenComponent } from './components/standby-screen/standby-screen.component';
import { SelectYourPlanComponent } from './pages/select-your-plan/select-your-plan.component';
import { CompanyDataComponent } from './pages/company-data/company-data.component';
import { InvitePeopleComponent } from './pages/invite-people/invite-people.component';
import { ModalNewTargetCardComponent } from './components/modal/modal-new-target-card/modal-new-target-card.component';


import { IntoCellphoneComponent } from './pagesCandidato/register/into-cellphone/into-cellphone.component';
import { ValidateSmsComponent } from './pagesCandidato/register/validate-sms/validate-sms.component';
import { RegisterSucessComponent } from './pagesCandidato/register/register-sucess/register-sucess.component';
import { BirthdayComponent } from './pagesCandidato/register/birthday/birthday.component';
import { MaritalStatusComponent } from './pagesCandidato/register/marital-status/marital-status.component';
import { NationalityComponent } from './pagesCandidato/register/nationality/nationality.component';
import { FirstJobComponent } from './pagesCandidato/register/first-job/first-job.component';
import { CandidateLocationComponent } from './pagesCandidato/register/candidate-location/candidate-location.component';
import { JobLocationComponent } from './pagesCandidato/register/job-location/job-location.component';
import { LiveThereComponent } from './pagesCandidato/register/live-there/live-there.component';
import { SidebarComponent } from './pagesCandidato/register/components/sidebar/sidebar.component';
import { AvatarComponent } from './pagesCandidato/register/avatar/avatar.component';
import { OcrComponent } from './pagesCandidato/ocr/ocr.component';
import { UploadCvComponent } from './pagesCandidato/ocr/upload-cv/upload-cv.component';
import { ValidationOcrComponent } from './pagesCandidato/ocr/validation-ocr/validation-ocr.component';
import { WorkExperienceComponent } from './pagesCandidato/ocr/work-experience/work-experience.component';
import { EducationComponent } from './pagesCandidato/ocr/education/education.component';
import { CvComponent } from './pagesCandidato/ocr/cv/cv.component';
import { RegistroCvComponent } from './pagesCandidato/ocr/registro-cv/registro-cv.component';
import { CvSkillsComponent } from './pagesCandidato/ocr/cv-skills/cv-skills.component';
import { RegistroCvCardComponent } from './pagesCandidato/ocr/registro-cv-card/registro-cv-card.component';
import {Ng2TelInputModule} from 'ng2-tel-input';
import { IntoPasswordComponent } from './pagesCandidato/register/into-password/into-password.component';
import { ModalVerifyPassComponent } from './components/modal/verify-pass/verify-pass.component';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalCookiesComponent } from './components/modal/modal-cookies/modal-cookies.component';
import { CouponsTemplateComponent } from './pages/coupons-template/coupons-template.component';

@NgModule({
  declarations: [
    RegisterComponent,
    ForgotPasswordComponent,
    RestorePasswordComponent,
    ScreenConfirmComponent,
    NavBarComponent,
    RegisterCandidatoComponent,
    GeneroComponent,
    PerfilProfesionalComponent,
    PuestoComponent,
    ActivateRecuitreAccountComponent,
    PuestoComponent,
    SueldoComponent,
    CreateCandidateAccountComponent,
    RestorePasswordCandidateComponent,
    ViewTestComponent,
    RestorePasswordAttemptsComponent,
    ActivateCandidateAccountComponent,
    TermsComponent,
    PrivacyComponent,
    CreateGuestPasswordComponent,
    RegisterGuestRecruiterComponent,
    UploadGuestPhotoComponent,
    TermsAndConditionsComponent,
    SideNavRegisterComponent,
    SelectYourPlanComponent,
    SummaryOfYourOrderComponent,
    SetUpPaymentComponent,
    StartPlanComponent,
    StandbyScreenComponent,
    CompanyDataComponent,
    InvitePeopleComponent,
    ModalNewTargetCardComponent,
    IntoCellphoneComponent,
    ValidateSmsComponent,
    RegisterSucessComponent,
    BirthdayComponent,
    MaritalStatusComponent,
    NationalityComponent,
    FirstJobComponent,
    CandidateLocationComponent,
    JobLocationComponent,
    LiveThereComponent,
    SidebarComponent,
    AvatarComponent,
    OcrComponent,
    UploadCvComponent,
    ValidationOcrComponent,
    WorkExperienceComponent,
    EducationComponent,
    CvComponent,
    RegistroCvComponent,
    CvSkillsComponent,
    RegistroCvCardComponent,
    IntoPasswordComponent,
    ModalVerifyPassComponent,
    ModalCookiesComponent,
    CouponsTemplateComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    moduleSharedModule,
    InternalizacionModule,
    Ng2TelInputModule,
    NgbTooltipModule
  ]
})
export class AuthModule { }
