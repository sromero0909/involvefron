import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PruebasPsicometricasComponent } from './pruebas-psicometricas.component';

describe('PruebasPsicometricasComponent', () => {
  let component: PruebasPsicometricasComponent;
  let fixture: ComponentFixture<PruebasPsicometricasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PruebasPsicometricasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PruebasPsicometricasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
