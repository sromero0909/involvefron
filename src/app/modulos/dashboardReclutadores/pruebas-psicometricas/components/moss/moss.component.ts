import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { PsicometricService } from '../psicometric.service';
import {Observable} from 'rxjs';
import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';
import { timeout } from 'rxjs/operators';
import { borderLeftStyle } from 'html2canvas/dist/types/css/property-descriptors/border-style';




@Component({
  selector: 'app-moss',
  templateUrl: './moss.component.html',
  styleUrls: ['./moss.component.scss']
})
export class MossComponent implements OnInit {

  processId: string;
  show = true;
  reporte = true;
  moss:any ;
  data: any;
  point: any = [];
  Data:Label[] =[];
  color : string[] =[];
  interpretacion: string;
  @ViewChild('myCanvas') canvas: ElementRef;
    
  // DATA
  public lineChartData: ChartDataSets[] = [
    {},
    
    //{ data: [180, 480, 770, 90, 1000, 270, 400], label: 'Series C', yAxisID: 'y-axis-1' }
  ];

  // TINME LINE 
  public lineChartLabels: Label[] = ['Habilidad de supervisión', 'Capacidad de decisión en las relaciones humanas', 'Capacidad de evaluación de problemas interpersonales', 'Habilidad para establecer relaciones interpersonales', 'Sentido común y tacto en las relaciones interpersonales'];
  public lineChartInterpretation: Label[] = [];
  // RESPONSIVE
  public lineChartOptions: any = {

    responsive: true,
    scales: {
      xAxes: [{
        ticks: {  

          fontFamily:'GilroyRegular',
          fontStyle: 'normal',
          fontColor: "#2E2E2E", 
          fontSize: 12 ,      
          min: 0,
          max: 100,
          
          callback: function (value: any) {
            return (value / this.max * 100).toFixed(0) + '%'; 
          },
        }
      }],
      yAxes: [{
        categoryPercentage: 0.5,
        ticks: {   
          fontFamily:'GilroyRegular',
          fontStyle: 'normal',
          fontColor: "#2E2E2E", 
          fontSize: 12 ,    
          position: 'left'
        }
      }]

    }    
   
  };

  // COLOR
  public lineChartColors: Color[] = [
    { 
      backgroundColor: '#E29A1D'
    }
  ];

  // TXT
  public lineChartLegend = false;

  // TIPO DE GRAFICA
  public lineChartType: ChartType = 'horizontalBar';
  public lineChartPlugins = [];

  constructor( private psicometricService:PsicometricService,private profileService: ProfileService ) {
    this.getDataMoss(); 
  }
 
  ngOnInit(): void {
  }
  getDataMoss(){
    this.profileService.getCandidate$();
    this.processId = this.profileService.candiate?.idProcess;
    if (this.processId){
      this.psicometricService.getMossInterpretation(this.processId).subscribe((response:any)=>{
        if(response.code === 200){
          this.data= response.data[0].moss[0].records;
          const Percentage = [];
          const ability = [];
          const Interpretation =[];
          const features =[]
          for(let i = 0 ; i < this.data.length ; i++ ){
            Percentage.push(this.data[i].percentage);
            ability.push(this.data[i].name);
            Interpretation.push(this.data[i].interpretation);
            features.push(this.data[i].features);
          }
          this.Data = Percentage
          Percentage.push(0);
          this.lineChartData[0].data = Percentage;
          this.lineChartLabels = ability;
          this.lineChartInterpretation = Interpretation;
          this.createCanvas();
        }else{
          this.lineChartData[0].data = [0,0,0,0,0,0];
          this.createCanvas();
        }
      });
    }
  
  }

  createCanvas(){
      var colores: any = []; 
      let gradient_green = this.canvas.nativeElement.getContext('2d').createLinearGradient(290,0,500,0);
      gradient_green.addColorStop(0, '#FFFFFF');
      gradient_green.addColorStop(1, '#24AA3D');
      let gradient_red = this.canvas.nativeElement.getContext('2d').createLinearGradient(290,0,500,0);
      gradient_red.addColorStop(0, '#FFFFFF');
      gradient_red.addColorStop(1, '#E21D1D');
      let gradient_orange = this.canvas.nativeElement.getContext('2d').createLinearGradient(290,0,500,0);
      gradient_orange.addColorStop(0, '#FFFFFF');
      gradient_orange.addColorStop(1, '#E29B1F');
      for(let i = 0 ; i < this.Data.length - 1 ; i++ ){
        if(this.Data[i] < "50"){
          this.color[i]= "Red"
          colores[i] = gradient_red;
        }else if(this.Data[i] > "50" && this.Data[i] < "80"){
          colores[i] = gradient_orange
          this.color[i]= "Orange"
        }else{
          colores[i] = gradient_green
          this.color[i]= "Green"
        }
      }
      this.lineChartColors = [
          {
            backgroundColor: colores
          }
      ];

  }
  

}
