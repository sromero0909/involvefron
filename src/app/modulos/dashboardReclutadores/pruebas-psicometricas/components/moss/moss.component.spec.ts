import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MossComponent } from './moss.component';

describe('MossComponent', () => {
  let component: MossComponent;
  let fixture: ComponentFixture<MossComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MossComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MossComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
