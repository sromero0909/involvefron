import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IpvComponent } from './ipv.component';

describe('IpvComponent', () => {
  let component: IpvComponent;
  let fixture: ComponentFixture<IpvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IpvComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IpvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
