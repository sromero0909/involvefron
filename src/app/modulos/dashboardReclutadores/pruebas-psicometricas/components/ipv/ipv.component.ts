import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { PsicometricService } from '../psicometric.service';
import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';



@Component({
  selector: 'app-ipv',
  templateUrl: './ipv.component.html',
  styleUrls: ['./ipv.component.scss']
})
export class IpvComponent implements OnInit {

  show = true;
  reporte = true;
  processId: string;
  ipv:any [] = [];
  Data:Label[] =[];
  color : string[] =[];

  @ViewChild('myCanvas') canvas: ElementRef;
    
  // DATA
  public lineChartData: ChartDataSets[] = [
    {}
    //{ data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
    //{ data: [180, 480, 770, 90, 1000, 270, 400], label: 'Series C', yAxisID: 'y-axis-1' }
  ];

  // TINME LINE 
  public lineChartLabels: Label[] = ['DGV', 'Receptividad', 'Agresividad', 'Comprensión', 'Adaptabilidad', 'Control de si mismo', 'Tolerancia a la frustración', 'Combatividad', 'Dominancia', 'Seguridad', 'Actividad', 'Sociabilidad'];
  public lineChartInterpretation: Label[] = [];
  // RESPONSIVE
  public lineChartOptions: any = {

    responsive: true,
    scales: {
      xAxes: [{
        ticks: {   
          fontFamily:'GilroyRegular',
          fontStyle: 'normal',
          fontColor: "#2E2E2E", 
          fontSize: 12 ,     
          min: 0,
          max: 10
        }
      }],
      yAxes: [{
        ticks: {   
          fontFamily:'GilroyRegular',
          fontStyle: 'normal',
          fontColor:  "#2E2E2E",
          fontSize: 12 ,  
          position: 'left'
        }
      }]

    }
  };

  // COLOR
  public lineChartColors: Color[] = [
    {
      backgroundColor: '#E29A1D',
      pointBackgroundColor: '#5DD6D6',
      pointHoverBackgroundColor: '#fff',
    }
  ];

  // TXT
  public lineChartLegend = false;

  // TIPO DE GRAFICA
  public lineChartType: ChartType = 'horizontalBar';
  public lineChartPlugins = [];

  constructor( private psicometricService:PsicometricService,private profileService: ProfileService  ) { 
  }

  ngOnInit(): void {
    this.getDataIpv();
  }
  getDataIpv(){
    this.profileService.getCandidate$();
    this.processId = this.profileService.candiate?.idProcess;
    if (this.processId){
      this.psicometricService.getIpvInterpretation(this.processId).subscribe((response:any)=>{
        if(response.code === 200){
          this.ipv= response.data[0].IPV[0].records;
          const percentage = [];
          const name = [];
          const interpretation =[];
          for(let i = 0 ; i < this.ipv.length ; i++ ){
             percentage.push(this.ipv[i].points);
             name.push(this.ipv[i].name);
             interpretation.push(this.ipv[i].interpretation);
          }
          this.Data = percentage
          percentage.push(0);
          this.lineChartData[0].data = percentage;
          this.lineChartLabels = name;
          this.lineChartInterpretation = interpretation;
          this.createCanvas()
        }else{
          this.createCanvas();
        }
      });
    }
  }
  createCanvas(){    
    var colores: any = []; 
      let gradient_green = this.canvas.nativeElement.getContext('2d').createLinearGradient(100,0,400,0);
      gradient_green.addColorStop(0, '#FFFFFF');
      gradient_green.addColorStop(1, '#24AA3D');
      let gradient_red = this.canvas.nativeElement.getContext('2d').createLinearGradient(100,0,400,0);
      gradient_red.addColorStop(0, '#FFFFFF');
      gradient_red.addColorStop(1, '#E21D1D');
      let gradient_orange = this.canvas.nativeElement.getContext('2d').createLinearGradient(100,0,400,0);
      gradient_orange.addColorStop(0, '#FFFFFF');
      gradient_orange.addColorStop(1, '#E29B1F');
      for(let i = 0 ; i < this.Data.length - 1 ; i++ ){
        if(this.Data[i] < "5"){
          this.color[i]= "Red"
          colores[i] = gradient_red;
        }else if(this.Data[i] > "5" && this.Data[i] < "8"){
          colores[i] = gradient_orange
          this.color[i]= "Orange"
        }else{
          colores[i] = gradient_green
          this.color[i]= "Green"
        }
      }
      this.canvas
      this.lineChartColors = [
          {
            backgroundColor: colores
          }
      ];
  }


}