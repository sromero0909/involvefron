import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';

import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';
import { PsicometricService } from '../psicometric.service';

@Component({
  selector: 'app-luscher',
  templateUrl: './luscher.component.html',
  styleUrls: ['./luscher.component.scss']
})
export class LuscherComponent implements OnInit {
  luscher:any ;
  data: any;
  processId: string;
  
  
  // DATA
  //public lineChartData: ChartDataSets[] = [
    //{ data: [100, 40, 20, 40],  }
    //{ data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
    //{ data: [180, 480, 770, 90, 1000, 270, 400], label: 'Series C', yAxisID: 'y-axis-1' }
  //];

  // TINME LINE 
  public lineChartLabels: Label[] = ['D', 'I', 'C', 'S'];

  // RESPONSIVE
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          gridLines: {
            color: '#EFEFEF',
          },
          ticks: {
            
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'horizontal',
          scaleID: 'x-axis-0',
          value: 'I',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
          }
        },
      ],
    },
  };
  
  // COLOR
  public lineChartColors: Color[] = [
    {
      backgroundColor: 'rgba(255, 255, 255, 0)',
      borderColor: '#0BB9B9',
      pointBackgroundColor: '#5DD6D6',
      pointHoverBackgroundColor: '#fff',
    }
  ];

  // TXT
  public lineChartLegend = false;

  // TIPO DE GRAFICA
  public lineChartType: ChartType = 'line';

  constructor(private psicometricService: PsicometricService,private profileService: ProfileService) { }

  ngOnInit(): void {
    this.getluscher();
  }
  getluscher(){
    this.profileService.getCandidate$();
    this.processId = this.profileService.candiate?.idProcess
    if (this.processId){
      this.psicometricService.getLuscherInterpretation(this.processId).subscribe((response:any)=>{
        if(response.code === 200){
          this.data= response.data;
         }else{
          this.data =  {
             data :{
              current:[
                "Es una persona que busca siempre un ambiente sereno y ordenado en su vida, sin conflictos ni problemas y sigue siempre modos de comportamiento convencionales. Tiene una necesidad de tranquilidad emocional dekjfmlkefm"
              ],
              desired:[
                "Es una persona que refleja dependencia e inseguridad, sus metas y motivaciones se han debilitado, esto lleva a tenciones y angustias al querer cumplir sus objetivos, dependiendo de la aprobación de alguien."
              ]
            },
             description: ["No se pudo acceder a la informacion del usuario"]
           }
         }
       });
    }

  }
}