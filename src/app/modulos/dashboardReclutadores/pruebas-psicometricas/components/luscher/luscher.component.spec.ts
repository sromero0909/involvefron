import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LuscherComponent } from './luscher.component';

describe('LuscherComponent', () => {
  let component: LuscherComponent;
  let fixture: ComponentFixture<LuscherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LuscherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LuscherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
