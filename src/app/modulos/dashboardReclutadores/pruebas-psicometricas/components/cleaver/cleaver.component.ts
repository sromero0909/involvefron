import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { PsicometricService } from '../psicometric.service';
import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';

@Component({
  selector: 'app-cleaver',
  templateUrl: './cleaver.component.html',
  styleUrls: ['./cleaver.component.scss']
})
export class CleaverComponent implements OnInit {
  processId: string;
  description: string;
  show = true;
  interpretaciones : any ;
  
  @ViewChild('myCanvas') canvas: ElementRef;
// DATA
  public lineChartDataNormal: ChartDataSets[] = [
    { data: [10, 20, 30, 40],  }
  ];

  public lineChartDataInitiator: ChartDataSets[] = [
    { data: [10, 20, 30, 40],}
    ];
  public lineChartDataMotivation: ChartDataSets[] = [
    { data: [10, 20, 30, 40],}
  ];
  public lineChartDataUnderPressure: ChartDataSets[] = [
    { data: [10, 20, 30, 40],}
  ];
  

  // TINME LINE 
  public lineChartLabels: Label[] = ['Dominancia', 'Influencia', 'Estabilidad', 'Cumplimiento'];

  // RESPONSIVE
  public lineChartOptions: (any) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{
        ticks: {
          fontFamily:'Gilroy',
          fontStyle: 'normal',
          callback: function (value: any) {
            let letra : any = [];
            if(value== "Dominancia"){
              letra= "D";

            }else if(value== "Influencia"){
              letra= "I";
            }else if(value== "Estabilidad"){
              letra= "S";
            }else if(value== "Cumplimiento"){
              letra= "C";
            }
            return (letra); 
          },
          
        }
          
      }],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          gridLines: {
            color: '#EFEFEF',
          },
          ticks: {
            
          }
        }
      ]
    }
    
  };
  
  // COLOR
  public lineChartColors: Color[] = [
    {
      backgroundColor: 'rgba(255, 255, 255, 0)',
      borderColor: '#0BB9B9',
      pointBackgroundColor: '#0BB9B9',
      pointHoverBackgroundColor: '#fff',
      pointRadius: 1,
    }
  ];
  public lineChartColorsNorm: Color[] = [
    {
      backgroundColor: 'rgba(255, 255, 255, 0)',
      borderColor: '#EAD637',
      pointBackgroundColor: '#EAD637',
      pointHoverBackgroundColor: '#fff',
      pointRadius: 1,
    }
  ];
  public lineChartColorsMotivation: Color[] = [
    {
      backgroundColor: 'rgba(255, 255, 255, 0)',
      borderColor: '#4E148C',
      pointBackgroundColor: '#4E148C',
      pointHoverBackgroundColor: '#fff',
      pointRadius: 1,
    }
  ];
  public lineChartColorsPressure: Color[] = [
    {
      backgroundColor: 'rgba(255, 255, 255, 0)',
      borderColor: '#1090C7',
      pointBackgroundColor: '#1090C7',
      pointHoverBackgroundColor: '#fff',
      pointRadius: 1,
    }
  ];
  // TXT
  public lineChartLegend = false;

  // TIPO DE GRAFICA
  public lineChartType: ChartType = 'line';

  constructor(private psicometricService:PsicometricService,private profileService: ProfileService ) {
    this.getDatacleaver();
   }

  ngOnInit(): void {
  }

  getDatacleaver(){
    this.profileService.getCandidate$();
    this.processId = this.profileService.candiate?.idProcess
    if (this.processId){
      this.psicometricService.getCleaverInterpretation(this.processId).subscribe((response:any)=>{
        if(response.code === 200){
          this.lineChartDataNormal[0].data  =response.data[0].data[0].data[0].Cleaver[0].records[0]["normal"];
          this.lineChartDataInitiator[0].data = response.data[0].data[0].data[0].Cleaver[0].records[0]["iniciador"];
          this.lineChartDataMotivation[0].data = response.data[0].data[0].data[0].Cleaver[0].records[0]["motivacion"];
          this.lineChartDataUnderPressure[0].data = response.data[0].data[0].data[0].Cleaver[0].records[0]["bajo presion"];
          this.description = response.data[0].data[0].data[0].Cleaver[0].records[0].description;
          this.interpretaciones = response.data[0].data[0].data[0].interpretacion[0].Listado;
        }else{
          this.description = "No se pudieron consultar los resultados de esta prueba."
        }
      });
    }
  }

}
