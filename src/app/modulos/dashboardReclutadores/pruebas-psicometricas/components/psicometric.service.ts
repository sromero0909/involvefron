import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PsicometricService {

  constructor(private http: HttpClient) { }
  
  getCleaverInterpretation  (process : string ): Observable<any>{
    return this.http.get(`${environment.restPsychometric}/tests/interpretations/cleaver?processId=${process}`);
    
  }
  getIpvInterpretation(process: string ): Observable<any>{
  return this.http.get(`${environment.restPsychometric}/tests/interpretations/ipv?processId=${process}`);
  
  }

  getLuscherInterpretation  (process : string ): Observable<any>{
      return this.http.get(`${environment.restPsychometric}/tests/interpretations/luscher?processId=${process}`);
      
  }
  getMossInterpretation(process: string ): Observable<any>{
    return this.http.get(`${environment.restPsychometric}/tests/interpretations/moss?processId=${process}`);
    
  }
  getTest(process : string): Observable<any>{
    return this.http.get(`${environment.restPsychometric}/tests/process?externalProcessId=${process}`);
  }
  getNametest(process: string ):  Observable<any>{
    return this.http.get(`${environment.restPsychometric}/tests/psicometrics?id=${process}`);
  }


}


