import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { moduleSharedModule } from 'src/app/commons/commons.module';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { InternalizacionModule } from 'src/app/commons/internalizacion/internalizacion.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfigurationRoutingModule } from './configuration-routing.module';
import { ConfigurationDashboardComponent } from './configuration-dashboard/configuration-dashboard.component';
import { ConfigurationProfileComponent } from './configuration-profile/configuration-profile.component';
import { ConfigurationCompanyComponent } from './configuration-company/configuration-company.component';
import { ConfigurationRecruitmentTeamComponent } from './configuration-recruitment-team/configuration-recruitment-team.component';
import { ConfigurationIntegrationsComponent } from './configuration-integrations/configuration-integrations.component';
import { ConfigurationTemplatesComponent } from './configuration-templates/configuration-templates.component';
import { ConfigurationNotificationsComponent } from './configuration-notifications/configuration-notifications.component';
import { ConfigurationSubscriptionComponent } from './configuration-subscription/configuration-subscription.component';



@NgModule({
  declarations: [
    ConfigurationDashboardComponent,
    ConfigurationProfileComponent,
    ConfigurationCompanyComponent,
    ConfigurationRecruitmentTeamComponent,
    ConfigurationIntegrationsComponent,
    ConfigurationTemplatesComponent,
    ConfigurationNotificationsComponent,
    ConfigurationSubscriptionComponent
  ],
  imports: [
    CommonModule,
    InternalizacionModule,
    ConfigurationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    moduleSharedModule,
    NgbModule,
  ]
})
export class ConfigurationModule { }
