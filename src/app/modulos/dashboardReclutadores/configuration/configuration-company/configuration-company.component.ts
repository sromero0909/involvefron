import { Component, OnInit, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthService } from '../../../../services/auth.service'
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { CompanyService } from 'src/app/services/reclutador/configuration/company.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidatorsService } from 'src/app/commons/validatorsService/validators.service';


@Component({
  selector: 'app-configuration-company',
  templateUrl: './configuration-company.component.html',
  styleUrls: ['./configuration-company.component.scss']
})
export class ConfigurationCompanyComponent implements OnInit {

  @ViewChild('iconoChekBlanco', { static: true }) iconoChekBlanco!: TemplateRef<any>;
  @ViewChild('deletePhotoConfirmation', { static: true }) deletePhotoConfirmation!: TemplateRef<any>;
  @ViewChild('fileUploadCompany', { static: false })
  InputVar!: ElementRef;

  formCompany!: FormGroup;

  sectores: any;
  giros: any;

  idGiro: string = "";
  photo: any = "";
  imgTrash: string = ""
  tamEmprea: string = '';
  companyId: string = '';
  mensajeToast: string = '';

  bordeTamEmpres = { "uno": false, "dos": false, "tres": false, "cuatro": false }
  tamanios: any[] = [{ idTamano: "OPC1", tamano: "1 a 10" }, { idTamano: "OPC2", tamano: "11 a 50" }, { idTamano: "OPC3", tamano: "51 a 250" }, { idTamano: "OPC4", tamano: "+ 250" }]

  country: Array<any> = [];

  uptateGiro = false;


  constructor(private formBuilder: FormBuilder,
    private authService: AuthService,
    public toastService: ToastService,
    private companyService: CompanyService,
    private modalService: NgbModal,
    private validatorsService: ValidatorsService) {
    this.initializeFormCompany();
  }



  ngOnInit(): void {
    this.initializeSelect();
  }

  initializeSelect = () => {
    this.companyId = JSON.parse(this.authService.getUser()).company.companyId
    this.photo = null;
    this.imgTrash = (this.photo == null) ? 'basuraGris.svg' : 'basura.svg';
    this.onchangeSector()
    this.getCompany(true)
    this.getPaises();
    this.getSectores();
  }

  initializeFormCompany = () => {
    this.formCompany = this.formBuilder.group({
      nameCompany: [{ value: '', disabled: false }, [Validators.required, this.validatorsService.noWhiteSpaces,Validators.pattern(this.validatorsService.allCharactersAndNumbersAndSpecial)]],
      companySector: [{ value: '', disabled: false }, [Validators.required]],
      companygiro: [{ value: '', disabled: false }, [Validators.required]],
      typeCompany: [{ value: '', disabled: false }, [Validators.required]],
      numEmpleadosEmpresa: [{ value: '', disabled: false }, [Validators.required]],
      pais: [{ value: '', disabled: false }, [Validators.required]],
    });
  }

  getCompany = (update: boolean) => {
    this.companyService.getCompany(this.companyId).subscribe(
      (response: any) => {
        if (response != null) {
          this.loadInfCompany(response, update);
        }
      },
      (error) => {
        this.showMessageError(error.error);
      })
  }

  onclickTamEmpresa(op: string, tam: any) {

    this.resetBordeTamEmpresa()
    switch (op) {
      case "OPC1":
        this.tamEmprea = tam;
        this.bordeTamEmpres.uno = true;
        break
      case "OPC2":
        this.tamEmprea = tam;
        this.bordeTamEmpres.dos = true;
        break
      case "OPC3":
        this.tamEmprea = tam;
        this.bordeTamEmpres.tres = true;
        break
      case "OPC4":
        this.tamEmprea = tam;
        this.bordeTamEmpres.cuatro = true
        break
      default:
        this.tamEmprea = tam
        break
    }
    this.formCompany.controls['numEmpleadosEmpresa'].setValue(this.tamEmprea);
  }

  resetBordeTamEmpresa() {
    this.bordeTamEmpres.uno = false;
    this.bordeTamEmpres.dos = false;
    this.bordeTamEmpres.tres = false;
    this.bordeTamEmpres.cuatro = false
  }

  getPaises = () => {
    this.authService.consultCountryu().subscribe(
      (response: any) => {
        if (response != null) {
          for (const item of response) {
            if (item.iso2 == 'MX' || item.iso2 == 'ES') {
              this.country.push(item);
            }
          }
        }
      },
      (error: any) => {
        this.showMessageError(error.error.message)
      });
  }

  getSectores(): void {
    this.authService.consultSector().subscribe(
      (response: any) => {
        this.sectores = response.body;
        //this.onchangeSector();
      },
      (error: any) => {
        this.showMessageError(error.message)
      });
  }

  searhGiro = (idSector: any): void => {
    this.authService.consultGiro(idSector).subscribe(
      (response: any) => {
        this.giros = response;
        if (this.uptateGiro) {
          setTimeout(() => {
            this.formCompany.controls['companygiro'].setValue(this.idGiro);
          }, 5);
          this.uptateGiro = false
        }
      },
      (error: any) => {
        this.showMessageError(error.message)
      });
  }

  onchangeSector(): void {
    this.formCompany.get("companySector")?.valueChanges.subscribe((changes: any) => {
      if (changes != '') {
        this.searhGiro(changes);
      }
    });
  }

  getSector = (idSector: string) => {
    let sectorTem;
    for (let sector of this.sectores) {
      if (sector.sectorId == idSector) {
        sectorTem = sector;
        break;
      }
    }
    return sectorTem;
  }

  getGiro = (idGiro: string) => {
    let giroTem;
    for (let giro of this.giros) {
      if (giro.industryId == idGiro) {
        giroTem = giro;
        break;
      }
    }
    return giroTem;
  }

  setTamEmpresa = (tam: string) => {
    switch (tam) {
      case "DE1A10":
        this.tamEmprea = "1 a 10";
        break;
      case "DE11A50":
        this.tamEmprea = "11 a 50";
        break;
      case "DE51A250":
        this.tamEmprea = "51 a 250";
        break;
      case "MASDE250":
        this.tamEmprea = "+ 250";
        break;
    }
    this.formCompany.controls['numEmpleadosEmpresa'].setValue(this.tamEmprea);
  }

  loadInfCompany = (company: any, update: boolean) => {

    const client = company
    this.companyId = client.companyId
    this.uptateGiro = true
    this.photo = (client.pathLogo == null) ? null : client.pathLogo + "?" + (new Date()).getTime()
    this.imgTrash = (this.photo == null) ? 'basuraGris.svg' : 'basura.svg';
    if (update) {
      if (client.company != null) {
        this.formCompany.controls['nameCompany'].setValue(client.company);
      }

      if (client.sector != null) {
        this.formCompany.controls['companySector'].setValue(client.sector.sectorId);
      }

      if (client.industry != null) {
        this.idGiro = client.industry.industryId
        this.formCompany.controls['companygiro'].setValue(client.industry.industryId);
      }
      if (client.typeCompany != null) {
        this.formCompany.controls['typeCompany'].setValue(client.typeCompany)
      }

      if (client.companySize != null) {
        this.setTamEmpresa(client.companySize);
      }

      if (client.country != null) {
        this.formCompany.controls['pais'].setValue(client.country)
      }
    }
  }

  getParamsCompany = () => {
    const numEmpleadosEmpresa = this.formCompany.get('numEmpleadosEmpresa')?.value;
    const cadena = (String(numEmpleadosEmpresa).includes("+ 250")) ? 'MASDE250' : "DE"
    let params = {
      company: this.formCompany.get('nameCompany')?.value,
      companyId: this.companyId,
      companySize: (cadena == "MASDE250") ? cadena : cadena + String(numEmpleadosEmpresa).replace(/ /g, "").toUpperCase(),
      country: this.formCompany.get('pais')?.value,
      industry: this.getGiro(this.formCompany.get('companygiro')?.value),
      pathLogo: this.photo,
      sector: this.getSector(this.formCompany.get('companySector')?.value),
      typeCompany: String(this.formCompany.get('typeCompany')?.value).toUpperCase(),
    }
    return params
  }

  updateCompany = () => {

    this.companyService.updataCompany(this.companyId, this.getParamsCompany()).subscribe(
      (response: any) => {
        if (response != null) {
          this.photo = response.pathLogo
          this.mensajeToast = "Los cambios han sido actualizados"
          this.toastService.showSuccess(this.iconoChekBlanco)
        }
      },
      (error) => {
        this.showMessageError(error.error)
      });
  }

  onClickSubmitCompany() {
    if (this.formCompany.invalid) {
      this.formCompany.get('nameCompany')?.markAsTouched();
      this.formCompany.get('companySector')?.markAsTouched();
      this.formCompany.get('companygiro')?.markAsTouched();
      this.formCompany.get('typeCompany')?.markAsTouched();
      this.formCompany.get('numEmpleadosEmpresa')?.markAsTouched();
      this.formCompany.get('pais')?.markAsTouched();
    } else {
      this.updateCompany()
    }
  }

  onFileSelected = (event: any) => {
    const file: File = event.target.files[0];
    if (file) {
      if (String(file.type).includes('image')) {
        if (file.size <= 2000000) {
          let formData = new FormData();
          formData.append("file", file, file.name);
          this.companyService.upLoadPhotoCompany(formData, "BUSSINES_LOGO").subscribe(
            (response: any) => {
              this.InputVar.nativeElement.value = "";
              if (response != null && response.code == 200) {
                this.getCompany(false)
                this.mensajeToast = response.message
                this.toastService.showSuccess(this.iconoChekBlanco)
              } else {
                this.toastService.showMessageError("Ocurrió in error al actualizar la foto");
              }
            },
            (error) => {
              this.showMessageError(error.error)
            });
        } else {
          this.InputVar.nativeElement.value = "";
          this.toastService.showMessageError("Elige una foto de hasta 2 MB");
        }
      } else {
        this.InputVar.nativeElement.value = "";
        this.toastService.showMessageError("El archivo seleccionado no es una imagen");
      }
    }

  }

  deletePhoto = () => {
    if (this.photo != null && !String(this.photo).includes('assets')) {
      this.companyService.deleteCompany("BUSSINES_LOGO").subscribe(
        (response: any) => {
          if (response != null && response.code == 200) {
            this.getCompany(false)
            this.mensajeToast = response.message
            this.toastService.showSuccess(this.iconoChekBlanco)
          } else {
            this.toastService.showMessageError("Ocurrió in error al actualizar la foto");
          }
          this.modalService.dismissAll();
        },
        (error) => {
          this.toastService.showMessageError(error.error);
        })
    }
  }
  showMessageError(dangerTpl: any) {
    this.toastService.show("Ocurrió un error: " + dangerTpl, { classname: 'bg-danger text-light ngb-toastsBottom', delay: 6000 });
  }
  get isCustomPhoto(): boolean{
    return this.photo != null && !this.photo.includes('assets');
  }

  onDeletePhotoAttempt(){
    this.modalService.open(this.deletePhotoConfirmation, { centered: true, size: 'md' });
  }

  onCloseModal(){
    this.modalService.dismissAll();
  }
}
