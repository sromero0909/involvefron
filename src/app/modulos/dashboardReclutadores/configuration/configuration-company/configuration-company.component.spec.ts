import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationCompanyComponent } from './configuration-company.component';

describe('ConfigurationCompanyComponent', () => {
  let component: ConfigurationCompanyComponent;
  let fixture: ComponentFixture<ConfigurationCompanyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigurationCompanyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
