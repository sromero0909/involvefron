import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MainNavModel } from '../../talent/models/main-nav.model';

@Component({
  selector: 'app-configuration-dashboard',
  templateUrl: './configuration-dashboard.component.html',
  styleUrls: ['./configuration-dashboard.component.scss']
})
export class ConfigurationDashboardComponent implements OnInit {
  mainNavData: MainNavModel;
  menu = { profile: true, company: false, notifications: false, template: false, team: false, suscription: false, integrations: false }
  mensajeToast: string = ""
  userRol: boolean = false
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.mainNavConfig();
    if (this.authService.getUser() != undefined && this.authService.getUser() != '') {
      const user = JSON.parse(this.authService.getUser()).user.userRol
      this.userRol = (user == "RECRUITER_ADMIN") ? true : false
    }
    this.redirecto('profile');
  }

  mainNavConfig = () => {
    this.mainNavData = {
      title: 'Configuración',
      search: true,
      image: 'assets/img/general/configuraciones.svg',
      iconBack: false,
    }
  }

  menuSelected = (menu: string) => {

    this.resetMenu();
    switch (menu) {
      case "profile":
        this.menu.profile = true
        this.redirecto('profile')
        break;
      case "company":
        this.menu.company = true
        this.redirecto('company');
        break;
      case "notifications":
        this.redirecto('notifications');
        this.menu.notifications = true
        break;
      case "template":
        this.redirecto('templates')
        this.menu.template = true
        break;
      case "team":
        this.redirecto('recruitment-team')
        this.menu.team = true
        break;
      case "suscription":
        this.menu.suscription = true
        this.redirecto('subscription')
        break;
      case "integrations":
        this.menu.integrations = true
        this.redirecto('integrations');
        break;

      default:
        break;
    }
  }

  resetMenu = () => {
    this.menu = { profile: false, company: false, notifications: false, template: false, team: false, suscription: false, integrations: false }
  }

  redirecto(rute: string) {
    this.router.navigateByUrl(`dashboard/configuration/(configuration:${rute})`)
  }
}
