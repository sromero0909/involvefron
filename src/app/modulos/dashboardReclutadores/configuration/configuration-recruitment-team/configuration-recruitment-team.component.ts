import { Component, OnInit, ViewChild, ViewChildren, QueryList, TemplateRef } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DecimalPipe } from '@angular/common';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdSortableHeader } from '../../../../directives/sortable.directive';

import { ToastService } from 'src/app/services/commons/toast.setvice';
import { PaginadorService } from 'src/app/services/reclutador/vacantes/paginador.service';
import { RecruiterTeamService } from 'src/app/services/reclutador/configuration/recruitment-team.service';
import { SideNavService } from 'src/app/services/reclutador/vacantes/sideNav.service';

@Component({
  selector: 'app-configuration-recruitment-team',
  templateUrl: './configuration-recruitment-team.component.html',
  styleUrls: ['./configuration-recruitment-team.component.scss'],
  providers: [PaginadorService, DecimalPipe]
})
export class ConfigurationRecruitmentTeamComponent implements OnInit {

  show = true;

  @ViewChild('iconoChekBlanco', { static: true }) iconoChekBlanco!: TemplateRef<any>;
  @ViewChildren(NgbdSortableHeader) headers!: QueryList<NgbdSortableHeader>;
  @ViewChild('selectRecruiter', { static: true }) selectRecruiter!: TemplateRef<any>;
  @ViewChild('deleteRecruiterModal', { static: true }) deleteRecruiterModal!: TemplateRef<any>;
  @ViewChild('changeRolModal', { static: true }) changeRolModal!: TemplateRef<any>;
  @ViewChild('invitedRecruiter', { static: true }) invitedRecruiter!: TemplateRef<any>;

  formInvit!: FormGroup;

  rows: Array<any> = [];
  arrayRecruiters: Array<any> = [];
  page_size: number = 10;
  page_number: number = 0;
  pageActual: number = 0;
  totalElements: number = 0;
  totalPages: number = 0;
  rowsMostrar: number = 0;
  pageTemporal: number = 0;
  restarRows: number = 0
  rowsPorPagina = 0
  page = 1

  searchFlag: boolean = false;
  flagService: boolean = false;
  resetTable: boolean = false;
  changeValueSelect: boolean = true

  sortDirection: string = "&sortDirection=ASC"
  orderBy: string = "&sortBy=us.name"
  columnActive: string = '';
  textFilter: string = '';
  mensajeToast: string = '';
  idNewRecruiter: string = '';
  oldRecruiter: string = '';
  recruiterId: string = '';
  FILTER_PAG_REGEX = /[^0-9]/g;

  constructor(
    private formBuilder: FormBuilder,
    public toastService: ToastService,
    private recruiterTeamService: RecruiterTeamService,
    private modalService: NgbModal,
    private sideNavService: SideNavService
  ) { this.initializeFormInvitedRecruiter(); }

  ngOnInit(): void {
    this.inicializaRows();
  }

  inicializaRows = () => {
    this.flagService = false;
    let params = {
      pageNumber: this.page_number,
      pageSize: this.page_size,
      other: `${this.orderBy}${this.sortDirection}${this.textFilter}`
    };

    this.recruiterTeamService.getRecruiters(params).subscribe(
      (response: any) => {
        setTimeout(() => {
          this.flagService = true;
        }, 100)
        if (response != null && response.status == 500) {
          this.totalElements = 0;
          this.totalPages = 0;
          this.toastService.showMessageError(response.error)
        } else {
          let data = (response != null && response.hasOwnProperty('content')) ? response.content : [];
          if (data.length > 0) {
            if (this.resetTable) {
              this.resetPaginadores();
            }
            for (let recruiter of data) {
              this.rows.push({
                id: recruiter.recruiterId,
                recruiter: recruiter.user.name + " " + recruiter.user.lastName,
                photo: (String(recruiter.user.photo) == "null") ? 'assets/img/dashboard/vacantes/sinFotoPerfil.svg' : recruiter.user.photo,
                email: recruiter.user.email,
                rol: (recruiter.user.userRol == "RECRUITER") ? 'Reclutador' : 'Coordinador',
                vacancies: recruiter.vacantsActived,
                date: recruiter.user.lastActivity,
                status: recruiter.status,
              });
            }
            this.totalElements = response.totalElements;
            this.totalPages = response.totalPages;
            if (this.pageActual == 0) {
              this.updateRowsMOstradas(this.pageActual + 1)
            } else {
              this.updateRowsMOstradas(this.pageActual)
            }
          }
        }

      },
      (error) => {
        this.toastService.showMessageError("Ocurrio un error");
      }
    );
  }

  rowsMostradas(page: any) {
    this.page_number = page - 1
    if (this.pageTemporal < parseInt(page) - 1) {
      this.resetTable = false;
      this.inicializaRows();
      this.pageTemporal = page - 1;
    }
    this.updateRowsMOstradas(page)
  }

  updateRowsMOstradas(page: any) {
    setTimeout(() => {
      let a = this.rows.slice((this.page - 1) * this.page_size, page * this.page_size)
      this.rowsPorPagina = a.length;
      if (this.pageActual <= page) {
        this.rowsMostrar = this.rowsMostrar + a.length;
        this.restarRows = a.length;
        this.pageActual = page;
      } else {
        this.rowsMostrar = this.rowsMostrar - this.restarRows;
        this.pageActual = page;
      }
    }, 100)
  }

  selecRow = () => {
    this.resetTable = true;
    this.inicializaRows()
  }

  resetPaginadores = () => {
    this.rowsMostrar = 0;
    this.pageActual = 0;
    this.page_number = 0;
    this.restarRows = 0;
    this.pageTemporal = 0;
    this.rows = [];
    this.page = 1
  }

  selectPage(page: string) {
    this.page = parseInt(page, 10) || 1;
  }

  formatInput(input: HTMLInputElement) {
    input.value = input.value.replace(this.FILTER_PAG_REGEX, '');
  }


  onSort({ column, direction }: any) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = 'sort';
      }
    });

    if (direction == 'sort') {
      this.sortDirection = "&sortDirection=ASC"
      this.orderBy = "&sortBy=us.name"

    } else {
      this.sortDirection = "&sortDirection=" + direction.toUpperCase()
      this.orderBy = "&sortBy=" + column
    }
    this.resetTable = true
    this.inicializaRows();
  }

  searchRecruiter = (text: string) => {
    this.textFilter = (text == "") ? "" : "&queryName=" + text

    this.resetTable = true;
    this.inicializaRows();
  }

  openModalChangeRol = (recruiterId: string) => {
    this.recruiterId = recruiterId
    this.modalService.open(this.changeRolModal, { centered: true });
  }

  changeRol = () => {
    this.recruiterTeamService.changeRol(this.recruiterId).subscribe(
      (response: any) => {
        if (response != null && response.status == 200) {
          this.resetTable = true;
          this.inicializaRows();
          this.mensajeToast = "El rol del reclutador ha cambiado"
          this.toastService.showSuccess(this.iconoChekBlanco)
        } else {
          this.toastService.showMessageError("Algo salio mal al cambiar el rol del reclutador");
        }
      },
      (error) => {
        this.toastService.showMessageError(error.error);
      }
    );
  }

  validateVacancies = (vacancies: number, idRecruiter: string) => {
    this.oldRecruiter = idRecruiter
    if (vacancies > 0) {
      this.getRecruiters();
      const modalRef = this.modalService.open(this.selectRecruiter, { size: 'lg', centered: true });
      modalRef.dismissed.subscribe(
        () => {
          this.changeValueSelect = true
          this.idNewRecruiter = "";
          this.oldRecruiter = "";
        }
      )
    } else {
      this.modalService.open(this.deleteRecruiterModal, { centered: true });
    }
  }

  closeModalAssingRecruiter = () => {
    this.modalService.dismissAll();
  }

  deleteRecruiter = () => {
    const idNewRecruiter = (this.idNewRecruiter == "") ? this.idNewRecruiter : "&newRecruiterId=" + this.idNewRecruiter
    this.recruiterTeamService.deleteRecruiter(this.oldRecruiter, idNewRecruiter).subscribe(
      (response: any) => {
        this.closeModalAssingRecruiter();
        if (response != null && response.code == 200) {
          this.resetTable = true;
          this.inicializaRows();
          this.mensajeToast = response.message
          this.toastService.showSuccess(this.iconoChekBlanco);
        } else {
          this.toastService.showMessageError((response != null) ? response.message : "Ocurrió un error");;
        }
      },
      (error) => {
        this.toastService.showMessageError(error.error);
      }
    );
  }


  getRecruiters = () => {
    this.sideNavService.getReclutadores().subscribe(
      (response) => {
        if (response != null && response.code == 200) {
          this.arrayRecruiters = response.data
        }
      },
      (error) => {
        this.toastService.showMessageError(error.error);
      }
    )
  }

  getRecruiterById = (idcruiter: string) => {
    this.changeValueSelect = false
    this.idNewRecruiter = idcruiter;
  }

  openModalInvitedRecruiter = () => {
    this.modalService.open(this.invitedRecruiter, { size: 'lg', centered: true });
  }

  initializeFormInvitedRecruiter = () => {
    this.formInvit = this.formBuilder.group({
      email: [{ value: '', disabled: false }, [Validators.required]],
      rol: [{ value: '', disabled: false }, [Validators.required]],
    });
  }

  onClickSubmit = () => {
    if (this.formInvit.invalid) {
      this.formInvit.get('email')?.markAsTouched();
      this.formInvit.get('rol')?.markAsTouched();
    } else {
      this.sendInvitation()
    }
  }

  resendInvitation = (recruiterId:string)=>{
    this.recruiterTeamService.resendInvitation(recruiterId).subscribe(
      (response: any) => {
        if (response.status == 200) {
          this.toastService.showSuccess("Se ha reenviado la invitación");
        } else {
          this.toastService.showMessageError("Ocurrió un error");
        }
      }
    );
  }

  sendInvitation = () => {
    let arraryEmail = []
    arraryEmail.push({
      email: this.formInvit.get('email')?.value,
      rol:this.formInvit.get('rol')?.value
    }) ;
    
    this.modalService.dismissAll();
    this.formInvit.reset();
    this.recruiterTeamService.sentInvitation(arraryEmail).subscribe(
      (response: any) => {
        if (response.status == 200) {
          this.toastService.showSuccess(response.body);
        } else {
          this.toastService.showMessageError("Ocurrió un error");
        }
      },
      (error) => {
        this.toastService.showMessageError(error.error.split(',')[1].trim() || error.error.split(',')[0].trim() );
      }
    );
  }
}
