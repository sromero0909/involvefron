import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationRecruitmentTeamComponent } from './configuration-recruitment-team.component';

describe('ConfigurationRecruitmentTeamComponent', () => {
  let component: ConfigurationRecruitmentTeamComponent;
  let fixture: ComponentFixture<ConfigurationRecruitmentTeamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigurationRecruitmentTeamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationRecruitmentTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
