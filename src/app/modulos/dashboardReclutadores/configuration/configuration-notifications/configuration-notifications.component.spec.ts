import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationNotificationsComponent } from './configuration-notifications.component';

describe('ConfigurationNotificationsComponent', () => {
  let component: ConfigurationNotificationsComponent;
  let fixture: ComponentFixture<ConfigurationNotificationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigurationNotificationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
