import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ToastService } from 'src/app/services/commons/toast.setvice';
import { NotificationsService } from 'src/app/services/reclutador/configuration/notifications.service';

@Component({
  selector: 'app-configuration-notifications',
  templateUrl: './configuration-notifications.component.html',
  styleUrls: ['./configuration-notifications.component.scss']
})
export class ConfigurationNotificationsComponent implements OnInit {

  @ViewChild('modalConfirmation', { static: true }) modalConfirmation!: TemplateRef<any>;

  rows: Array<any> = []
  dailyReport: any;
  hora: any;
  hours: Array<any> = ["01:00","01:30","02:00","02:30","03:00","03:30","04:00","04:30","05:00","05:30","06:00","06:30","07:00","07:30","08:00","08:30","09:00","09:30","10:00","10:30","11:00","11:30","12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00","19:30","20:00","20:30","21:00","21:30","22:00","22:30","23:00","23:30"]
  constructor(private notificationsService: NotificationsService,
    private toastService: ToastService,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.getListNotifications();
  }

  getListNotifications = () => {

    this.notificationsService.getListNotifications().subscribe(
      (response: any) => {
        if (response != null && response.length > 0) {
          this.rows = [];
          for (let notification of response) {
            notification['program'] = false
            if (notification.notification.typeNotification != "DAILY_REPORT") {
              this.rows.push(notification)
            } else {
              this.dailyReport = notification
              this.hora = (this.dailyReport.hour == null) ? '' : this.dailyReport.hour
            }
          }
        } else {
          this.toastService.showMessageError("Algo salió mal")
        }
      },
      (error) => {
        this.toastService.showMessageError(error.message);
      }
    );
  }

  updateRow = (id: string, property: string) => {
    for (let row of this.rows) {
      if (row.notificationId == id) {
        row[property] = !row[property]
      }
    }
    if (this.dailyReport.notificationId == id) {
      this.dailyReport[property] = !this.dailyReport[property]
    }
  }

  updateDateRow(id: string, property: string, data: string) {
    for (let row of this.rows) {
      if (row.notificationId == id) {
        row[property] = data
      }
    }
  }

  updateNotifications = () => {
    let arraytem = [];
    for (let array of Object.freeze(this.rows)) {
      arraytem.push(array)
    }
    arraytem.push(this.dailyReport)
    this.notificationsService.updateNotifications(arraytem).subscribe(
      (response: any) => {
        if (response != null) {
          this.toastService.showSuccess("Las notificaciones han sido actualizadas,true");
          this.getListNotifications();
        } else {
          this.toastService.showMessageError("Algo salió mal")
        }
      },
      (error) => {
        this.toastService.showMessageError(error.message);
      }
    );
  }

  changeHour = (text: string) => {
    this.dailyReport.hour = text
  }

  openModalDiscard = () => {
    this.modalService.open(this.modalConfirmation, { centered: true });
  }
}
