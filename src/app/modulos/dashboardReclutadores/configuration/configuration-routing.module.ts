import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigurationCompanyComponent } from './configuration-company/configuration-company.component';
import { ConfigurationDashboardComponent } from './configuration-dashboard/configuration-dashboard.component';
import { ConfigurationIntegrationsComponent } from './configuration-integrations/configuration-integrations.component';
import { ConfigurationNotificationsComponent } from './configuration-notifications/configuration-notifications.component';
import { ConfigurationProfileComponent } from './configuration-profile/configuration-profile.component';
import { ConfigurationRecruitmentTeamComponent } from './configuration-recruitment-team/configuration-recruitment-team.component';
import { ConfigurationSubscriptionComponent } from './configuration-subscription/configuration-subscription.component';
import { ConfigurationTemplatesComponent } from './configuration-templates/configuration-templates.component';

const routes: Routes = [
    {
        path: '',
        component: ConfigurationDashboardComponent,
        children: [
            {
                path: 'profile',
                component: ConfigurationProfileComponent,
                pathMatch: 'full',
                outlet: "configuration"
            },
            {
                path: 'company',
                component: ConfigurationCompanyComponent,
                pathMatch: 'full',
                outlet: "configuration"
            },
            {
                path: 'recruitment-team',
                component: ConfigurationRecruitmentTeamComponent,
                pathMatch: 'full',
                outlet: "configuration"
            },
            {
                path: 'integrations',
                component: ConfigurationIntegrationsComponent,
                pathMatch: 'full',
                outlet: "configuration"
            },
            {
                path: 'templates',
                component: ConfigurationTemplatesComponent,
                pathMatch: 'full',
                outlet: "configuration"
            },
            {
                path: 'notifications',
                component: ConfigurationNotificationsComponent,
                pathMatch: 'full',
                outlet: "configuration"
            },
            {
                path: 'subscription',
                component: ConfigurationSubscriptionComponent,
                pathMatch: 'full',
                outlet: "configuration"
            },
        ]
    }

];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})

export class ConfigurationRoutingModule { }