import { Component, OnInit,HostListener,ViewChild,ElementRef } from '@angular/core';

import { DomSanitizer } from '@angular/platform-browser';
import { FormBuilder } from '@angular/forms';

import { ProfileService } from 'src/app/services/reclutador/configuration/profile.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { AuthService } from '../../../../services/auth.service'
import { CompanyService } from 'src/app/services/reclutador/configuration/company.service';



@Component({
  selector: 'app-configuration-templates',
  templateUrl: './configuration-templates.component.html',
  styleUrls: ['./configuration-templates.component.scss']
})
export class ConfigurationTemplatesComponent implements OnInit {

  formColors: any;
  files:any;
  error!: string;
  dragAreaClass!: string;
  draggedFiles: any;
  url:any
  urlPreview!:  string
  bgtext:string = ""
  bgButton:string = '';
  bgButtonText:string = '';
  defaultHeaderImage:string = ''

  @ViewChild('fileTemplate', { static: false })
  InputVar!: ElementRef;

  @HostListener("dragover", ["$event"]) onDragOver(event: any) {
    this.dragAreaClass = "droparea";
    event.preventDefault();
  }
  @HostListener("dragenter", ["$event"]) onDragEnter(event: any) {
    this.dragAreaClass = "droparea";
    event.preventDefault();
  }
  @HostListener("dragend", ["$event"]) onDragEnd(event: any) {
    this.dragAreaClass = "dragarea";
    event.preventDefault();
  }
  @HostListener("dragleave", ["$event"]) onDragLeave(event: any) {
    this.dragAreaClass = "dragarea";
    event.preventDefault();
  }
  @HostListener("drop", ["$event"]) onDrop(event: any) {
    this.dragAreaClass = "dragarea";
    event.preventDefault();
    event.stopPropagation();
    if (event.dataTransfer.files) {
      let files: FileList = event.dataTransfer.files;
      this.saveFiles(files);
    }
  }

  constructor(
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder,
    private profileService: ProfileService,
    private toastService:ToastService,
    private authService: AuthService,
    private companyService: CompanyService, ) {
      this.initializeForColors();
     }

  ngOnInit(): void {
    this.dragAreaClass = "dragarea";
    this.updateInformation(JSON.parse(this.authService.getUser()).recruiterId)
  }

  initializeForColors = () => {
    this.formColors = this.formBuilder.group({
      inputText: [{ value: '#000000', disabled: false }],
      inputButton: [{ value: '#35E6E9', disabled: false }],
      inputTextButton: [{ value: '#35E6E9', disabled: false }],
    });
  }

  onFileChange(event: any) {
    this.files = event.target.files;
    if (this.files.length > 0 && String(this.files[0].type).includes('png')) {
    this.saveFiles(this.files);
  } else {
    //this.InputVar.nativeElement.value = "";
    this.toastService.showMessageError("El archivo seleccionado no es una imagen png");
  }
  
  }

  saveFiles(files: FileList) {
      this.error = "";
      this.draggedFiles = files;
      const file = files[0];
       this.url = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(file));
  }

  preview = () => {
    if (this.url){
      this.urlPreview = (this.url || this.defaultHeaderImage);
    }
    this.bgtext = this.formColors.get('inputText').value
    this.bgButton = this.formColors.get('inputButton').value
    this.bgButtonText = this.formColors.get('inputTextButton').value
  }

  updateInformation(id: string) {
    this.profileService.getRecruiter(id).subscribe(
      (response: any) => {
        if (response != null) {
          this.loadInfCompany(response);
        } else {
          this.toastService.showMessageError("Ocurrió un error al actualizar la foto de perfil");
        }
      },
      (error) => {
        this.toastService.showMessageError(error.message);
      });
  }
  
  loadInfCompany = (response:any) => {
    this.bgtext = response.company.colorText;
    this.bgButton = response.company.colorButton;
    this.bgButtonText = response.company.colorTextButton;
    this.url = (response.company.pathLogoEmail.includes('/res-involve/Involve_header.png')) ? null : response.company.pathLogoEmail;
    this.urlPreview = response.company.pathLogoEmail;
    this.defaultHeaderImage = (response.company.pathLogoEmail.includes('/res-involve/Involve_header.png') ? response.company.pathLogoEmail : '');
    this.formColors.controls['inputText'].setValue(this.bgtext );
    this.formColors.controls['inputButton'].setValue(this.bgButton );
    this.formColors.controls['inputTextButton'].setValue(this.bgButtonText );
  }

  onFileSelected = () => {
    
    if (this.files) {
      const file: File = this.files[0];
        if (file.size <= 2000000) {
          let formData = new FormData();
          formData.append("file", file, file.name);
          this.companyService.upLoadPhotoCompany(formData,"TEMPLATE_EMAIL").subscribe(
            (response: any) => {
              if (response != null && response.code == 200) {
               this.updateRecruiter();
                //this.toastService.showSuccess(this.iconoChekBlanco)
              } else {
                this.toastService.showMessageError("Ocurrió in error al actualizar la foto");
              }
            },
            (error) => {
              this.toastService.showMessageError(error.message)
            });
        } else {
         // this.InputVar.nativeElement.value = "";
          this.toastService.showMessageError("Elige una foto de hasta 2 MB");
        }
    }else{
      this.updateRecruiter();
    }

  }

  updateRecruiter = () => {
    this.profileService.updateRecruiterPatch(this.getParams()).subscribe(
      (response: any) => {
        if (response.status == 200 && response.body != null && !response.body.hasOwnProperty('error')) {
          this.toastService.showSuccess("Los cambios han sido guardados,true");
          this.loadInfCompany(response.body);
        }else{
          this.toastService.showMessageError(response.body.error)
        }
      },
      (error) => {
        this.toastService.showMessageError(error.message)
      }
    );
  }

  getParams = () => {
    const body: any[] =
      [
        {
          "op": "replace",
          "path": "/company/colorTextButton",
          "value": this.formColors.get('inputTextButton').value
        },
        {
          "op": "replace",
          "path": "/company/colorText",
          "value": this.formColors.get('inputText').value
        },
        {
          "op": "replace",
          "path": "/company/colorButton",
          "value": this.formColors.get('inputButton').value,
        }
      ];
    if (!this.url){
      body.push(
        {
          "op": "replace",
          "path": "/company/pathLogoEmail",
          "value": null,
        }
      )
    }
    return body;
  }


  deleteImagen = () => {
     this.url = null;
  }

  cancelChages = () => {
    this.updateInformation(JSON.parse(this.authService.getUser()).recruiterId)
  }
}


