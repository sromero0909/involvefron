import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationTemplatesComponent } from './configuration-templates.component';

describe('ConfigurationTemplatesComponent', () => {
  let component: ConfigurationTemplatesComponent;
  let fixture: ComponentFixture<ConfigurationTemplatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigurationTemplatesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
