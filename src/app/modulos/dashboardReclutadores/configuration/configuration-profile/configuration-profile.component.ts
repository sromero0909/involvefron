import { Component, OnInit, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthService } from '../../../../services/auth.service'
import { ProfileService } from 'src/app/services/reclutador/configuration/profile.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';

import { User } from 'src/app/services/models/user.model';
import { MESSAGES } from 'src/app/services/models/messages';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidatorsService } from 'src/app/commons/validatorsService/validators.service';



@Component({
  selector: 'app-configuration-profile',
  templateUrl: './configuration-profile.component.html',
  styleUrls: ['./configuration-profile.component.scss']
})
export class ConfigurationProfileComponent implements OnInit {

  @ViewChild('iconoChekBlanco', { static: true }) iconoChekBlanco!: TemplateRef<any>;
  @ViewChild('deletePhotoConfirmation', { static: true }) deletePhotoConfirmation!: TemplateRef<any>;
  @ViewChild('fileUpload', { static: false })
  InputVar!: ElementRef;
  formProfile!: FormGroup;
  formEmail!: FormGroup;
  formPassWord!: FormGroup;

  user!: User

  passwordError: boolean = false;
  showPassword: boolean = false;
  showOldPassword: boolean = false;
  showNewPassword: boolean = false;
  showConfirmPassWord: boolean = false;
  messageMail: boolean = false;
  messagePassWord: boolean = false;
  showList: boolean = false;
  messageError: string = '';
  rol: string = '';
  photo: string = 'assets/img/dashboard/vacantes/sinFotoPerfil.svg';
  mensajeToast: string = '';
  emailUser: string = '';
  imgTrash: string = "basuraGris.svg"


  // Variables para requisitos de contraseña
  isLower: boolean = false;
  isUpper: boolean = false;
  isMinLenght: boolean = false;
  isANumber: boolean = false;
  isCorrect: boolean = false;
  notCorrect: boolean = false;

  //Expresiones Regulares
  regExpLower = /[a-z]/;
  regExpUpper = /[A-Z]/;
  regExpMinLenght = /.{8,}/;
  regExpANumber = /[0-9]/;

  public rulePassword: boolean = false;
  public userNewPassword: string = "";
  public userPasswordConfirm: string = "";

  err401Mail: boolean = false;
  errConflicMail: boolean = false;
  errPassWord: boolean = false;
  view = { principal: true, email: false, password: false }
  prueba: boolean = true
  constructor(private authService: AuthService,
    private formBuilder: FormBuilder,
    private profileService: ProfileService,
    public toastService: ToastService,
    private router: Router,
    private _suscription : SuscriptionService,
    private modalService: NgbModal,
    private validatorsService: ValidatorsService) {
    this.initializeFormProfile();
    this.initializeFormEmail();
    this.initializeFormPassWord();
  }

  ngOnInit(): void {
    if(this._suscription.getEditPlan()){
      this._suscription.setEditPlan(false);
      this.router.navigateByUrl("dashboard/configuration/(configuration:subscription)")
    }else{
      this.updateInformation(JSON.parse(this.authService.getUser()).recruiterId)
    }
  }

  initializeFormProfile = () => {
    this.formProfile = this.formBuilder.group({
      name: [{ value: '', disabled: false }, [Validators.required, Validators.pattern(this.validatorsService.allCharacters),this.validatorsService.noWhiteSpaces]],
      firsLastName: [{ value: '', disabled: false }, [Validators.required,Validators.pattern(this.validatorsService.allCharacters),this.validatorsService.noWhiteSpaces]],
      secondLastName: [{ value: '', disabled: false }, [Validators.required,Validators.pattern(this.validatorsService.allCharacters),this.validatorsService.noWhiteSpaces]],
    });
  }

  initializeFormEmail = () => {
    this.formEmail = this.formBuilder.group({
      emailNew: [{ value: '', disabled: false }, [Validators.required,Validators.pattern(MESSAGES.expEmail)]],
      confirmEmail: [{ value: '', disabled: false }, [Validators.required,Validators.pattern(MESSAGES.expEmail)]],
      passwordEmail: [{ value: '', disabled: false }, [Validators.required]],
    });
  }

  initializeFormPassWord = () => {
    this.formPassWord = this.formBuilder.group({
      oldPassword: [{ value: '', disabled: false }, [Validators.required]],
      newPassword: [{ value: '', disabled: false }, [Validators.required]],
      confirmPassWord: [{ value: '', disabled: false }, [Validators.required]],
    });
  }

  changeView = (view: string) => {
    this.resetViews();
    switch (view) {
      case "principal":
        this.view.principal = true
        break;
      case "email":
        this.resetFormEmail();
        this.showPassword = false;
        this.view.email = true
        break;
      case "pass":
        this.resetFormPassWord();
        this.showOldPassword = false;
        this.showNewPassword = false;
        this.showConfirmPassWord = false;
        this.isLower = false;
        this.isUpper = false;
        this.isMinLenght = false;
        this.isANumber = false;
        this.isCorrect = false;
        this.notCorrect = false;
        this.view.password = true,
          this.showList = false;
        break;
    }
  }

  resetViews = () => {
    this.view = { principal: false, email: false, password: false }
  }

  loadInfUser = (data: User) => {
    this.formProfile.controls['name'].setValue(data.user.name);
    this.formProfile.controls['firsLastName'].setValue(data.user.lastName);
    this.formProfile.controls['secondLastName'].setValue(data.user.secondLastName);
    this.rol = (data.user.userRol == 'RECRUITER') ? 'Reclutador' : (data.user.userRol == 'COORDINATOR') ? 'Coordinador' : 'Reclutador Administrador';
    this.emailUser = data.user.email;
    this.photo = (String(data.user.photo) == 'null') ? 'assets/img/dashboard/vacantes/sinFotoPerfil.svg' + "?" + (new Date()).getTime() : data.user.photo + "?v=" + new Date().getTime()
    this.imgTrash = (this.photo == null || String(this.photo).includes('assets')) ? 'basuraGris.svg' : 'basura.svg';
    this.authService.setName(data.user.name)
    this.authService.setLastName(data.user.lastName)
    this.authService.setSecondLastName(data.user.secondLastName);
    this.authService.setEmail(data.user.email);
    this.authService.setPhotoProfile(this.photo)
  }

  updateRecruiter = (update: boolean) => {
    if(this.validateFormProfile()){
      let obj = JSON.parse(this.authService.getUser());
      let flag = (obj.user.name == this.formProfile.get('name')?.value) ? false : true
      let flaF = (obj.user.lastName == this.formProfile.get('firsLastName')?.value) ? false : true
      let flagS = (obj.user.secondLastName == this.formProfile.get('secondLastName')?.value) ? false : true
      if (flag || flaF || flagS || update) {
        obj.user.name = this.formProfile.get('name')?.value
        obj.user.lastName = this.formProfile.get('firsLastName')?.value
        obj.user.secondLastName = this.formProfile.get('secondLastName')?.value
        this.profileService.updataRecruiter(obj).subscribe(
          (response: any) => {
            if (response != null) {
              this.authService.setUser(JSON.stringify(response.body));
              this.loadInfUser(JSON.parse(this.authService.getUser()));
            }
          },
          (error) => {
            this.showMessageError(error.error);
          }
        );
      }  
    }
  }

  showMessageError = (dangerTpl: any) => {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-light', delay: 3000 });
  }

  showSuccess = (mensaje: any) => {
    this.toastService.remove(1)
    this.toastService.show(mensaje, { classname: 'bg-success text-light ngb-toastsBottom', delay: 5000 });
  }

  onDeletePhotoAttempt(){
    this.modalService.open(this.deletePhotoConfirmation, { centered: true, size: 'md' });
  }

  onCloseModal(){
    this.modalService.dismissAll();
  }

  deletePhto = () => {
    if (this.photo != null && !String(this.photo).includes('assets')) {
      this.profileService.deletePhoto().subscribe(
        (response: any) => {
          if (response != null && response != undefined && response.code == 200) {
            this.updateInformation(JSON.parse(this.authService.getUser()).recruiterId);
            this.showSuccess("Tu foto ha sido eliminada");
          } else {
            this.toastService.showMessageError("Ocurrió un error");
          }
          this.modalService.dismissAll();
        },
        (error) => {
          this.toastService.showMessageError(error.error)
        }
      );
    }
  }

  get isCustomPhoto(): boolean{
    return this.photo != null && !this.photo.includes('assets');
  }

  onFileSelected = (event: any) => {
    const file: File = event.target.files[0];
    if (file) {
      if (String(file.type).includes('image')) {
        if (file.size <= 2000000) {
          let formData = new FormData();
          formData.append("file", file, file.name);
          this.profileService.upLoadPhoto('URL_PHOTO', formData).subscribe(
            (response: any) => {
              this.InputVar.nativeElement.value = "";
              if (response != null && (response.code == 201 )) {
                this.mensajeToast = response.message;
                this.toastService.showSuccess(this.iconoChekBlanco)
                this.updateInformation(JSON.parse(this.authService.getUser()).recruiterId)
              } else {
                this.toastService.showMessageError("Error al cambiar la foto de perfil");
              }
            },
            (error) => {
              this.showMessageError(error.error)
            });
        } else {
          this.InputVar.nativeElement.value = "";
          this.toastService.showMessageError("Elige una foto de hasta 2 MB");
        }
      } else {
        this.InputVar.nativeElement.value = "";
        this.toastService.showMessageError("El archivo seleccionado no es una imagen");
      }
    }
  }

  validateFormProfile = () => {
    if (this.formProfile.invalid){
      this.formProfile.get('name')?.markAsTouched();
      this.formProfile.get('firsLastName')?.markAsTouched();
      this.formProfile.get('secondLastName')?.markAsTouched();
    }
    return this.formProfile.valid;
  }

  onClickSubmit = () => {
    if (this.formEmail.invalid) {
      this.formEmail.get('passwordEmail')?.markAsTouched();
      this.formEmail.get('emailNew')?.markAsTouched();
      this.formEmail.get('confirmEmail')?.markAsTouched();
    } else {
      this.updateEmail()
    }
  }

  updateEmail = () => {
    const pass = this.formEmail.get('passwordEmail')?.value
    const newEmail = this.formEmail.get('emailNew')?.value
    const confirmEamil = this.formEmail.get('confirmEmail')?.value
    if (newEmail == confirmEamil) {
      this.messageMail = false;
      if (pass != '') {
        this.profileService.verifyPassWord(pass).subscribe(
          (response: any) => {
            if (response != null) {
              if (response.code == 471) {
                this.err401Mail = true;
              } else {
                this.err401Mail = false;
                this.profileService.changeEmail(this.formEmail.get('emailNew')?.value).subscribe(
                  (response) => {
                    if (String(response).includes('Conflict')) {
                      this.errConflicMail = true;
                    } else {
                      this.errConflicMail = false;
                      if (String(response).includes('cambio con exito')) {
                        this.updateInformation(JSON.parse(this.authService.getUser()).recruiterId)
                        this.changeView('principal')
                        this.mensajeToast = "Los cambios han sido guardados"
                        this.showSuccess(this.iconoChekBlanco);
                      } else {
                        this.toastService.showMessageError("Ocurrio un error al cambiar la contraseña");
                      }
                    }
                  },
                  (error) => {
                    this.toastService.showMessageError(error.error);
                  });
              }
            }
          },
          (error) => {
            this.toastService.showMessageError(error.error);
          });
      }
    } else {
      this.messageMail = true
    }
  }

  onClickSubmitPass() {
    if (this.formPassWord.invalid) {
      this.formPassWord.get('oldPassword')?.markAsTouched();
      this.formPassWord.get('newPassword')?.markAsTouched();
      this.formPassWord.get('confirmPassWord')?.markAsTouched();
    } else {
      this.updatePassWord()
    }
  }

  validateNewPassword(): void {
    const userNewPassword = this.formPassWord.get('newPassword')?.value
    this.showList = (userNewPassword != "") ? true : false;
    this.isLower = this.regExpLower.test(userNewPassword);
    this.isUpper = this.regExpUpper.test(userNewPassword);
    this.isMinLenght = this.regExpMinLenght.test(userNewPassword);
    this.isANumber = this.regExpANumber.test(userNewPassword);

    if (this.isLower && this.isANumber && this.isUpper && this.isMinLenght) {
      this.rulePassword = true;
    } else {
      this.rulePassword = false;
    }
  }

  updatePassWord = () => {
    const oldPassword = this.formPassWord.get('oldPassword')?.value
    const newPassword = this.formPassWord.get('newPassword')?.value
    const confirmPassWord = this.formPassWord.get('confirmPassWord')?.value
    if (newPassword == confirmPassWord) {
      this.messagePassWord = false;
      if (oldPassword != '') {
        this.profileService.verifyPassWord(oldPassword).subscribe(
          (response: any) => {
            if (response != null) {
              if (response.code == 471) {
                this.errPassWord = true;
              } else {
                this.errPassWord = false;
                this.profileService.changePassWord(newPassword).subscribe(
                  (response: any) => {
                    if (response != null && response.code == 200) {
                      this.changeView('principal')
                      this.mensajeToast = response.message
                      this.toastService.showSuccess(this.iconoChekBlanco)
                    } else {
                      this.showMessageError("Ocurrió un error");
                    }
                  },
                  (error) => {
                    this.toastService.showMessageError(error.error);
                  });
              }
            }
          },
          (error) => {
            this.toastService.showMessageError(error.error);
          });
      }
    } else {
      this.messagePassWord = true
    }
  }

  resetFormPassWord = () => {
    this.formPassWord.reset();
    this.initializeFormPassWord()
  }

  resetFormEmail = () => {
    this.formEmail.reset();
    this.initializeFormEmail();
  }

  redirecToForgotPassWord = () => {
    this.router.navigateByUrl('/forgot-password');
  }

  updateInformation(id: string) {
    this.profileService.getRecruiter(id).subscribe(
      (response: any) => {
        if (response != null) {
          this.authService.setUser(JSON.stringify(response));
          this.loadInfUser(JSON.parse(this.authService.getUser()));
        } else {
          this.toastService.showMessageError("Ocurrió un error al actualizar la foto de perfil");
        }
      },
      (error) => {
        this.showMessageError(error.error);
      });
  }
}
