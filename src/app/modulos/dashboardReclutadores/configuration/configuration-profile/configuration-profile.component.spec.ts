import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationProfileComponent } from './configuration-profile.component';

describe('ConfigurationProfileComponent', () => {
  let component: ConfigurationProfileComponent;
  let fixture: ComponentFixture<ConfigurationProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigurationProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
