import { Component, NgModule, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/services/auth.service';

import { ToastService } from 'src/app/services/commons/toast.setvice';
import { IntegrationsService } from 'src/app/services/reclutador/configuration/integrations.service';


@Component({
  selector: 'app-configuration-integrations',
  templateUrl: './configuration-integrations.component.html',
  styleUrls: ['./configuration-integrations.component.scss']
})
export class ConfigurationIntegrationsComponent implements OnInit {

  formIntegrations: any;
  message:boolean = false;
  arrayJobBoards: Array<any> = []
  bagOcc:boolean;
  bagEmpleosTI:boolean;
  user:any;
  avisoPrivacidad: boolean = false;
  avisoTerminos: boolean = false;
  indice:number;
  bag:string;

  constructor(private formBuilder: FormBuilder,
    public toastService: ToastService,
    public integrationService: IntegrationsService,
    public modalService: NgbModal,
    private authService: AuthService) {

    this.formIntegrations = this.formBuilder.group({
      jobBoards:this.formBuilder.array([]),
    });
  }

  ngOnInit(): void {
    this.initializeRows();
    this.user = JSON.parse(this.authService.getUser());
    this.validateLoginBags();
  }

  initializeRows = () => {
   this.arrayJobBoards.push(/*{
     active:true,
     name:"involve",
     urlLogo: "/assets/img/dashboard/logoInvolveFooter.svg",
     hide:true,
     connection:true,
     email:"involverh@grupotama.com.mx",
     passWord:"12345678",
     confirmPassword:"12345678"
   },*/
   {
    active:false,
    name:"OCC",
    urlLogo: "/assets/img/dashboard/vacantes/logoOcc.svg",
    hide:true,
    connection:false,
    email:"",
    password:"",
    confirmPassword:"",
    message:false
  },
  {
    active:false,
    name:"HIRELINE",
    urlLogo: "/assets/img/dashboard/vacantes/logoEmpleosTI.svg",
    hide:true,
    connection:false,
    email:"",
    password:"",
    confirmPassword:"",
    message:false
  }
  /*{
    active:false,
    name:"linkedIn",
    urlLogo: "/assets/img/dashboard/vacantes/logoLinkedIn.svg",
    hide:true,
    connection:false,
    email:"",
    passWord:"",
    confirmPassword:""
  },
  {
    active:false,
    name:"indeed",
    urlLogo: "/assets/img/dashboard/vacantes/logoIndeed.svg",
    hide:true,
    connection:false,
    email:"",
    passWord:"",
    confirmPassword:""
  }*/)

   this.createForm();
  }

  createForm = () => {
    this.formIntegrations.reset()
    for(let data of this.arrayJobBoards){
      const areaFormGroup = this.formBuilder.group({
        active: [{ value: data.active, disabled: false }, []],
        name: [{ value: data.name, disabled: false }, []],
        urlLogo: [{ value: data.urlLogo, disabled: false }, []],
        hide: [{ value: data.hide, disabled: false }, []],
        connection: [{ value: data.connection, disabled: false }, []],
        email: [{ value: data.email, disabled: false }, []],
        password: [{ value: data.password, disabled: false }, []],
        confirmPassword: [{ value: data.confirmPassword, disabled: false }, []],
        message: [{ value: data.message, disabled: false }, []],
      });
      this.jobBoard.push(areaFormGroup)
    }
  }

  get jobBoard(): FormArray{
    return this.formIntegrations.get('jobBoards') as FormArray;
  }

  changeStatus = (indice:number) => {
    let value = this.formIntegrations.get('jobBoards').value[indice]
    const hide = this.formIntegrations.get('jobBoards').value[indice].hide
    value.hide = (hide) ? false : true
    this.formIntegrations.get("jobBoards").at(indice).setValue(value);
    this.indice = indice;
  }

  activeConnection = ()=> {
    let value = this.formIntegrations.get('jobBoards').value[this.indice];
    this.message = false;
    let sindex = String(this.indice);
    let response = this.getAuth(sindex,value.email,value.pass);
    if(response == 200){
      value = this.formIntegrations.get('jobBoards').value[this.indice];
      const connection = this.formIntegrations.get('jobBoards').value[this.indice].connection;
      value.connection = (connection) ? false : true;
      this.formIntegrations.get("jobBoards").at(this.indice).setValue(value);
    }

    let mensaje = (value.connection) ? "La conexión ha sido validada" : "Conexión inválida";
    this.toastService.showSuccess(mensaje+",true");
    const active = this.formIntegrations.get('jobBoards').value[this.indice].active
    value.active = (active) ? false : true
  }

  getAuth = (bag:string, email:string,pass:string) => {
    this.integrationService.getAuth(this.user.recruiterId,email,pass,bag).subscribe((response:any) => {
      return response.code;
    }, (error:any) => {
        this.toastService.showMessageError(error);
        return 500;
    })
    return 500;
  }

  preventModal = (indice:number,content:any) => {
    let value = this.formIntegrations.get('jobBoards').value[indice];
    if(value.email != "" && value.password != ""){
        this.message = false;
        this.modalService.open(content, { size: 'md' });
    } else {
      value.message = true;
      this.formIntegrations.get("jobBoards").at(indice).setValue(value);
    }
  }

  closeModal = (content:any) => {
    this.modalService.dismissAll();
  }

  validateLoginBags = () => {
    this.integrationService.getActiveBags(this.user.recruiterId).subscribe((response:any) => {
      if(response.code == 200){
        if(response.data.conexion){
          for(let i = 0; i < this.jobBoard.controls.length; i++){
            if(this.jobBoard.controls[i].value.name == response.data.nombre){
                let values = this.jobBoard.controls[i].value;
                values.connection = response.data.conexion;
                values.active = response.data.conexion;
                values.email = response.data.username;
                this.jobBoard.controls[i].setValue(values);
            }
          }
        }
      }
    }, (error:any) => {
      this.toastService.showMessageError(error);
    })
  }

  activatePriv = (event:any) => {
    this.avisoPrivacidad = event.srcElement.checked;
  }

  activateTerm = (event:any) => {
    this.avisoTerminos = event.srcElement.checked;
  }

  activeModal = (content:any) => {
    this.modalService.open(content,{size:'md'});
    this.bag = this.jobBoard.controls[this.indice].value.name == 'HIRELINE' ? 'HIRELINE' : 'OCC';
  }

  updatePassword = (indice: number) => {
    let values;
        for(let i = 0; i < this.jobBoard.controls.length; i++){
          if(indice == i){
            if(this.jobBoard.controls[i].value.password == this.jobBoard.controls[i].value.confirmPassword){
              this.message = false;
              values = this.jobBoard.controls[i].value;
              values.message = false;
              this.jobBoard.controls[i].setValue(values);

              if(this.jobBoard.controls[i].value.name == 'HIRELINE'){
                let userBag = {
                  recruiterId:this.user.recruiterId,
                  username: this.jobBoard.controls[i].value.email,
                  pas:this.jobBoard.controls[i].value.password,
                  nombre:this.jobBoard.controls[i].value.name,
                  conexion:this.jobBoard.controls[i].value.connection,
                  status:this.jobBoard.controls[i].value.active,
                  logo:this.jobBoard.controls[i].value.urlLogo
                }
                this.modalService.dismissAll();
                this.integrationService.updatePassBag(userBag).subscribe((res:any) => {
                  values = this.jobBoard.controls[i].value;
                  values.hide = true;
                  this.jobBoard.controls[i].setValue(values);
                  let mensaje = "Se ha actualizado la contraseña";
                  this.toastService.showSuccess(mensaje+",true");
                }, (error:any) => {
                  this.toastService.showMessageError("No se actualizó la contraseña: "+ error);
                });
              }
            } else {
              this.message = true;
              values = this.jobBoard.controls[i].value;
              values.message = true;
              this.jobBoard.controls[i].setValue(values);
              return;
            }
          }
        }
  }

}
