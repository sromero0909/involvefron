import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationIntegrationsComponent } from './configuration-integrations.component';

describe('ConfigurationIntegrationsComponent', () => {
  let component: ConfigurationIntegrationsComponent;
  let fixture: ComponentFixture<ConfigurationIntegrationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigurationIntegrationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationIntegrationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
