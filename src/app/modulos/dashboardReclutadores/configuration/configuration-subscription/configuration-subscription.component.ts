import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import * as moment from 'moment';
import 'moment/locale/es';

import { ToastService } from 'src/app/services/commons/toast.setvice';
import { AuthService } from 'src/app/services/auth.service';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { ModalNewTargetCardComponent } from 'src/app/auth/components/modal/modal-new-target-card/modal-new-target-card.component';
import { ModalVerifyPassComponent } from 'src/app/auth/components/modal/verify-pass/verify-pass.component';
import { SuscriptionCanceledService } from 'src/app/services/reclutador/login/subscription-canceled.service';

@Component({
  selector: 'app-configuration-subscription',
  templateUrl: './configuration-subscription.component.html',
  styleUrls: ['./configuration-subscription.component.scss']
})
export class ConfigurationSubscriptionComponent implements OnInit {

  userCreditCardsArray: any[] = [];
  currentUser: any;
  isSubscriptionAboutToExpire = false;
  subscriptionRemainingDays: number;
  expirationDate: string;

  currentView = 'principal';
  modalParams: any;

  billingDataForm: FormGroup = new FormGroup({
    accountType: new FormControl('person'),
    rfc: new FormControl('', [
      Validators.required
        // ,Validators.pattern('[A-ZÑ&]{3,4}\d{6}[A-V1-9][A-Z1-9][0-9A]')
        // folvera TODO: Agregar expresión regular correcta
    ]),
    name: new FormControl('', Validators.required),
    street: new FormControl(''),
    number: new FormControl(''),
    internalNumber: new FormControl(''),
    neighborhood: new FormControl(''),
    municipality: new FormControl(''),
    state: new FormControl(''),
    country: new FormControl(''),
    zipCode: new FormControl(''),
    phoneNumberCountryCode: new FormControl(''),
    phoneNumber: new FormControl('')
  });

  @ViewChild('addNewCard') addNewCard!: TemplateRef<any>;
  @ViewChild('deleteCardConfirmation') deleteCardConfirmation!: TemplateRef<any>;
  @ViewChild('newCardRejected') newCardRejected!: TemplateRef<any>;

  countryList: any[] = [
    {
      name: 'México',
      countryCode: 'MX',
      phoneCountryCode: '+52'
    },
    {
      name: 'Estados Unidos',
      countryCode: 'USA',
      phoneCountryCode: '+1'
    }
  ];
  constructor(private modalService: NgbModal,
              private toastService: ToastService,
              private route: Router,
              private authService: AuthService,
              private _suscription: SuscriptionService,
              private _suscriptionCanceled: SuscriptionCanceledService) { }

  ngOnInit(): void {
    if (this.authService.getUser() != undefined && this.authService.getUser() != '') {
      this.currentUser = JSON.parse(this.authService.getUser());
      this.expirationDate = moment(this.currentUser.user.suscriptionExpires.split(' ')[0]).locale('es').format('DD [de] MMMM [de] YYYY');
      const expirationDate = moment(this.currentUser.user.suscriptionExpires.split(' ')[0]);
      const todayDate = moment();

      this.subscriptionRemainingDays = expirationDate.diff(todayDate, 'd');
      this.isSubscriptionAboutToExpire = this.subscriptionRemainingDays <= 5;

      this.getCards();
    }
  }

  /**
   * Decides wich modal will be opened
   * @param name name of the frame to be opened
   * @param params set references to be passed through the popup and the main component
   */
  openModal(name: string, params?: any): void {
    this.modalService.dismissAll();
    this.modalParams = params;
    let modalRef;
    switch (name) {
      case 'addNewCard':
        const modalRef2 = this.modalService.open(ModalNewTargetCardComponent, { centered: true, size: 'md' });
        modalRef2.dismissed.subscribe(
          (response) => {
            if (response){
              if (response.success){
                this.toastService.showSuccess('Se agregó una nueva tarjeta');
              }
              if (response.carsLimitExceeded){
                this.modalService.open(this.newCardRejected, {centered: true, size: 'md'});
              }
              this.getCards();
            }
          }
        );
        break;
      case 'deleteCardConfirmation':
        modalRef = this.modalService.open(this.deleteCardConfirmation, { centered: true, size: '' });
        this.modalParams = params;
        break;
    }
  }

  /**
   * Close the current opened modal
   */
  onCloseModal(): void {
    this.modalParams = null;
    this.modalService.dismissAll();
  }

  /**
   * Request to delete the selected card.
   */
  onDeleteCardConfirmation(): void {
    const cardPosition = this.userCreditCardsArray.findIndex((el) => el.id === this.modalParams.cardId);
    if (!this.userCreditCardsArray[cardPosition].isCurrent) {
      if (cardPosition > -1) {
        this.deleteCard(this.modalParams.cardId)
        this.onCloseModal();
      }
    }
    else {
      this.toastService.showMessageError('La tarjeta se encuentra en uso');
    }
  }

  onBillingDataFormSubmit(): void {
  }

  onChangeBillingData(targetScreen?: string): void {
    if (targetScreen) {
      this.currentView = targetScreen;
    }
    else {
      this.currentView = 'billingData';
    }
    document.querySelector('.container.scroll')?.scroll(0, 0);
  }

  /**
   * Returns the local credit cards array setting in the first place the current card
   */
  get creditCardsArray(): any[] {
    if (this.userCreditCardsArray != undefined) {
      return this.userCreditCardsArray.sort((a, b) => (a.isCurrent ? -1 : 1));
    }
    else{
      return [];
    }
  }

  get isAccountTypePerson(): boolean {
    return this.billingDataForm.value.accountType === 'person';
  }

  get isAccountTypeBusiness(): boolean {
    return this.billingDataForm.value.accountType === 'business';
  }

  /**
   * @returns An object to be passed to getCards() and getSubscription()
   */
  getParams(): any {
    return {
      dateFin: moment(this._suscriptionCanceled.getCurrentDate()).format('YYYY/MM/DD'),
      dateIni: moment(this._suscriptionCanceled.getCurrentDate()).subtract(5, 'y').format('YYYY/MM/DD'),
      idClient: this.currentUser.user.userOpenPay
    };
  }

  /**
   * Ask the server for the user cards based on its userOpenPay property
   */
  getCards(): void {
    this._suscription.getCards(this.getParams()).subscribe(
      (response: any) => {
        if (response.status == 200 && response.body != null) {
          this.userCreditCardsArray = response.body.map(
            (el: any) => { 
              return {
                id: el.id,
                type: el.brand,
                ends: String(el.cardNumber).substring(12, 16),
                isCurrent: false
              };
            }
          );
        }
      },
      (error) => {
        this.toastService.showMessageError(error.error);
      }
    );
  }

  /**
   * Unlinks a credit card from the user account
   * @param idcard 
   */
  deleteCard = (idcard: string) => {
    this._suscription.deleteCard(idcard, this.currentUser.user.userOpenPay).subscribe(
      (response: any) => {
        if (response.status == 200 && response.body != null && !response.body.hasOwnProperty('error_code')) {
          this.toastService.showSuccess('La tarjeta ha sido eliminada');
          this.getCards();
        } else {
          this.toastService.showSuccess(response.body.description);
        }
      },
      (error) => {
        this.toastService.showSuccess(error.error.message);
      }
    );
  }


  redirectToPlan = (changePlan: boolean) => {
    if (!changePlan) {
      this.onCloseModal();
    }
    this._suscription.setReactivation(true);
    this.route.navigateByUrl('/start-plan/false/true');

  }
}
