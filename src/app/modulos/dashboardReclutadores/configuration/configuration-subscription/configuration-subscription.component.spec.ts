import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationSubscriptionComponent } from './configuration-subscription.component';

describe('ConfigurationSubscriptionComponent', () => {
  let component: ConfigurationSubscriptionComponent;
  let fixture: ComponentFixture<ConfigurationSubscriptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigurationSubscriptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationSubscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
