import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClietsRoutingModule } from './clientes-routing.module';
import { ClientsComponent } from './clients/clients.component';
import { moduleSharedModule } from 'src/app/commons/commons.module';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { InternalizacionModule } from 'src/app/commons/internalizacion/internalizacion.module';
import { ModalNewClientComponent } from './modal-new-client/modal-new-client.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalErrorComponent } from './modal-error/modal-error.component';



@NgModule({
  declarations: [
    ClientsComponent,
    ModalNewClientComponent,
    ModalErrorComponent
  ],
  imports: [
    CommonModule,
    InternalizacionModule,
    ClietsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    moduleSharedModule,
    NgbModule,
  ]
})
export class ClientsModule { }
