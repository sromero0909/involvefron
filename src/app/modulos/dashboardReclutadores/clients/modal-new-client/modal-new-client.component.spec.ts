import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalNewClientComponent } from './modal-new-client.component';

describe('ModalNewClientComponent', () => {
  let component: ModalNewClientComponent;
  let fixture: ComponentFixture<ModalNewClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalNewClientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalNewClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
