import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';

import { FormBuilder, Validators } from '@angular/forms';

import { AuthService } from 'src/app/services/auth.service';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { ClientsService } from 'src/app/services/reclutador/clients/clients.service';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InputSearchAutocompliteComponent } from 'src/app/commons/input-search-autocomplite/input-search-autocomplite.component';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-modal-new-client',
  templateUrl: './modal-new-client.component.html',
  styleUrls: ['./modal-new-client.component.scss']
})
export class ModalNewClientComponent implements OnInit {

  @Input() data: any;
  @Input() createsClientInside: boolean = false;
  @Output() dataTem: EventEmitter<any> = new EventEmitter();
  @ViewChild(InputSearchAutocompliteComponent) inputSearch!: InputSearchAutocompliteComponent;
  @Input() autoSave: boolean;

  tamanios: any[] = [{ idTamano: 'OPC1', tamano: '1 a 10' }, { idTamano: 'OPC2', tamano: '11 a 50' }, { idTamano: 'OPC3', tamano: '51 a 250' }, { idTamano: 'OPC4', tamano: '+ 250' }];
  id = 0;

  sectores: any;
  idSector = 0;

  giros: any;
  idGiro = 0;

  public valTamano = 0;
  public selectTamanio = false;

  cargos: any;
  idCargo = 0;

  pasos: any;
  formularioNewClient: any;
  paramsEditar: any;
  tamEmprea = '';
  clickEmpresa = false;
  isShowNumberPerson = true;

  bordeTamEmpres = { uno: false, dos: false, tres: false, cuatro: false };
  showCustomerSuggestions = false;
  isErrorClients = false;

  clientsSuggestion: any[] = [];
  clients: any[] = [];
  terminoCliente = '';
  txtTermino = '';
  valueInitialInputSearch = '';
  inputVacio = false;

  termino = '';
  hayError = false;
  puestos: any[] = [];

  flagloadInf = false;
  flagloadInfSector = false;

  constructor(
    private authService: AuthService,
    public toastService: ToastService,
    private formBuilder: FormBuilder,
    private catalogSevice: CatalogService,
    private _client: ClientsService,
    private modalService: NgbModal,
    private cookieService: CookieService
  ) {
  }

  ngOnInit(): void {
    this.initialiceForm();
    this.initializaSelect();
  }

  initializaSelect() {

    switch (this.data.status) {
      case 'edit':
        if (this._client.getClientTem().arrSector != undefined && this._client.getClientTem().arrSector.length > 0) {
          this.giros = this._client.getClientTem().arrGiro;
          this.sectores = this._client.getClientTem().arrSector;
          this.formularioNewClient.controls.sector.setValue(this._client.getClientTem().sectorId);
          this.formularioNewClient.controls.giro.setValue(this._client.getClientTem().industryId);
          this.onchangeSector();
          this.loadInf();
        } else {
          this.getSectores();
          this.searhGiro(this._client.getClientTem().sectorId);
        }
        break;
      case 'returnNewClient':
        this.loadInfReturnNewClient();
        break;
      default:
        this.getSectores();
        break;
    }
  }

  initialiceForm = (): void => {
    this.formularioNewClient = this.formBuilder.group({
      nameCliente: [{ value: this.data.newClientName, disabled: false }, [Validators.required]],
      sector: [{ value: '', disabled: false }, [Validators.required, Validators.minLength(2)]],
      giro: [{ value: '', disabled: false }, [Validators.required, Validators.minLength(2)]],
      tipoEmpresa: [{ value: '', disabled: false }, [Validators.required]],
      numEmpleadosEmpresa: [{ value: '', disabled: false }, [Validators.required]],
    });
  }

  getSectores(): void {
    this.authService.consultSector().subscribe(
      (response: any) => {
        this.sectores = response.body;
        if (this.data.status == 'edit' && !this.flagloadInfSector) {
          this.flagloadInfSector = true;
          this.formularioNewClient.controls.sector.setValue(this._client.getClientTem().sectorId);
        }
        this.onchangeSector();
      },
      (error: any) => {
        this.showMessageError(error.message);
      });
  }

  searhGiro = (idSector: any): void => {
    this.authService.consultGiro(idSector).subscribe(
      (response: any) => {
        this.giros = response;
        if (this.data.status == 'edit' && !this.flagloadInf) {
          this.flagloadInf = true;
          this.formularioNewClient.controls.giro.setValue(this._client.getClientTem().industryId);
          this.loadInf();
        }
      },
      (error: any) => {
        this.showMessageError(error.message);
      });
  }

  onchangeSector(): void {
    this.formularioNewClient.get('sector')?.valueChanges.subscribe((changes: any) => {
      if (changes != '') {
        this.searhGiro(changes);
      }
    });
  }

  onclickTamEmpresa(op: string, tam: any) {
    this.clickEmpresa = false;
    this.resetBordeTamEmpresa();
    switch (op) {
      case 'OPC1':
        this.tamEmprea = tam;
        this.bordeTamEmpres.uno = true;
        break;
      case 'OPC2':
        this.tamEmprea = tam;
        this.bordeTamEmpres.dos = true;
        break;
      case 'OPC3':
        this.tamEmprea = tam;
        this.bordeTamEmpres.tres = true;
        break;
      case 'OPC4':
        this.tamEmprea = tam;
        this.bordeTamEmpres.cuatro = true;
        break;
      default:
        this.tamEmprea = tam;
        break;
    }
    this.formularioNewClient.controls.numEmpleadosEmpresa.setValue(this.tamEmprea);
  }

  resetBordeTamEmpresa() {
    this.bordeTamEmpres.uno = false;
    this.bordeTamEmpres.dos = false;
    this.bordeTamEmpres.tres = false;
    this.bordeTamEmpres.cuatro = false;
  }

  suggestions(termino: string) {
    this.hayError = false;
    this.terminoCliente = termino;
    this.inputVacio = (this.terminoCliente == '') ? true : false;
    this.showCustomerSuggestions = true;
    this.catalogSevice.getClientesOcurrences(this.terminoCliente)
      .subscribe(
        clients => {
          if (this.terminoCliente === '' || this.terminoCliente == null) {
            this.clientsSuggestion = [];
            this.showCustomerSuggestions = false;
          }
          else {
            this.clientsSuggestion = clients.data.splice(0, 5);
          }
        },
        (err) => this.clientsSuggestion = []
      );
  }

  searhSuggestion(event: any) {
    this.showCustomerSuggestions = false;
    this.isErrorClients = false;
    this.txtTermino = String(event.target.innerText).replace(' * Nuevo', '');
    this.inputVacio = (this.txtTermino == '') ? true : false;
  }

  search(event: any) {
    this.showCustomerSuggestions = false;
    this.isErrorClients = false;
  }

  showMessageError(dangerTpl: any) {
    this.toastService.show('Ocurrió un error: ' + dangerTpl, { classname: 'bg-danger text-light ngb-toastsBottom', delay: 6000 });
  }

  onFocus = (text: any) => {
    if (text == '') {
      this.inputVacio = true;
    } else {
      this.inputVacio = false;
    }
  }

  onClickSubmit() {
    if (this.formularioNewClient.invalid) {
      this.formularioNewClient.get('nameCliente').markAsTouched();
      this.formularioNewClient.get('sector').markAsTouched();
      this.formularioNewClient.get('giro').markAsTouched();
      this.formularioNewClient.get('tipoEmpresa').markAsTouched();
      this.formularioNewClient.get('numEmpleadosEmpresa').markAsTouched();
      this.clickEmpresa = true;
    } else {
      if (this.data.status == 'edit') {
        this.editClient();
      } else {
        this.createNewClient();
      }
    }
  }

  createNewClient = () => {
    const params = this.getParamsNewClient();
    delete params.sector;
    delete params.giro;
    let texto: any;
    texto = { status: 'ok', message: 'Se creó un nuevo cliente' };
    this.cookieService.set('newClientObject', JSON.stringify(params));
    if (this.createsClientInside){
      this._client.createNewClient(params).subscribe((response: any) => {
        this.modalService.dismissAll(texto);
      });
    }
    else{
      this.modalService.dismissAll(texto);
    }
  }


  editClient = () => {
    const params = this.getParamseditClient();
    delete params.arrSector;
    delete params.arrGiro;
    this._client.editClient(params).subscribe(
      (response: any) => {
        let texto: any;
        if (response != undefined && response != null && (response.status == 400 || response.status == 500)) {
          texto = { status: 'nok', message: response.error };
        } else {
          texto = { status: 'ok', message: 'Los cambios han sido guardados' };
        }
        this.modalService.dismissAll(texto);
      },
      (err) => {
        const texto = { status: 'nok', message: err.error };
        this.modalService.dismissAll(texto);
      }
    );
  }

  loadInf = () => {
    this.valueInitialInputSearch = this._client.getClientTem().name;
    this.formularioNewClient.controls.nameCliente.setValue(this._client.getClientTem().name);
    this.txtTermino = this._client.getClientTem().name;
    this.inputVacio = (this.txtTermino == '') ? true : false;
    this.formularioNewClient.controls.tipoEmpresa.setValue(this._client.getClientTem().typeCompany);
    this.setTamEmpresa(this._client.getClientTem().employees);
  }

  getParamsNewClient = () => {
    const numEmpleadosEmpresa = this.formularioNewClient.get('numEmpleadosEmpresa').value;
    const cadena = (String(numEmpleadosEmpresa).includes('+ 250')) ? 'MASDE250' : 'DE';
    const params = {
      name: this.formularioNewClient.get('nameCliente').value,
      sectorId: this.formularioNewClient.get('sector').value,
      industryId: this.formularioNewClient.get('giro').value,
      typeCompany: String(this.formularioNewClient.get('tipoEmpresa').value).toUpperCase(),
      employees: (cadena == 'MASDE250') ? cadena : cadena + String(numEmpleadosEmpresa).replace(/ /g, '').toUpperCase(),
      giro: this.giros,
      sector: this.sectores
    };
    return params;
  }

  getParamseditClient = () => {
    const params = Object.freeze(this._client.getClientTem());
    const numEmpleadosEmpresa = this.formularioNewClient.get('numEmpleadosEmpresa').value;
    const cadena = (String(numEmpleadosEmpresa).includes('+ 250')) ? 'MASDE250' : 'DE';
    const ojbTem = {
      clientId: params.clientId,
      creationDate: params.creationDate,
      company: {
        companyId: params.company.companyId,
        company: params.company.company,
        companySize: params.company.companySize
      },
      companyId: params.companyId,
      isMine: params.isMine,
      isActive: params.isActive,
      name: this.formularioNewClient.get('nameCliente').value,
      sectorId: this.formularioNewClient.get('sector').value,
      sector: this.getSector(this.formularioNewClient.get('sector').value),
      industryId: this.formularioNewClient.get('giro').value,
      industry: this.getGiro(this.formularioNewClient.get('giro').value),
      typeCompany: String(this.formularioNewClient.get('tipoEmpresa').value).toUpperCase(),
      employees: (cadena == 'MASDE250') ? cadena : cadena + String(numEmpleadosEmpresa).replace(/ /g, '').toUpperCase(),
      arrSector: this.sectores,
      arrGiro: this.giros
    };
    return ojbTem;
  }

  getSector = (idSector: string) => {
    let sectorTem;
    for (const sector of this.sectores) {
      if (sector.sectorId == idSector) {
        sectorTem = sector;
        break;
      }
    }
    return sectorTem;
  }

  getGiro = (idGiro: string) => {
    let giroTem;
    for (const giro of this.giros) {
      if (giro.industryId == idGiro) {
        giroTem = giro;
        break;
      }
    }
    return giroTem;
  }

  setTamEmpresa = (tam: string) => {
    switch (tam) {
      case 'DE1A10':
        this.tamEmprea = '1 a 10';
        break;
      case 'DE11A50':
        this.tamEmprea = '11 a 50';
        break;
      case 'DE51A250':
        this.tamEmprea = '51 a 250';
        break;
      case 'MASDE250':
        this.tamEmprea = '+ 250';
        break;
    }
    this.formularioNewClient.controls.numEmpleadosEmpresa.setValue(this.tamEmprea);
  }

  closeModal = () => {
    const texto = { status: 'cerrar' };
    this.modalService.dismissAll(texto);
  }

  getDataTem = () => {
    let data;
    if (this.data.status == 'edit') {
      data = this.getParamseditClient();
    } else {
      data = this.getParamsNewClient();
    }

    this.dataTem.emit(data);
    this._client.setClietTem(data);
  }

  loadInfReturnNewClient = () => {
    const client = this._client.getClientTem();
    if (client.name != '') {
      this.formularioNewClient.controls.nameCliente.setValue(this._client.getClientTem().name);
    }
    if (client.sector != undefined) {
      this.sectores = client.sector;
      this.formularioNewClient.controls.sector.setValue(client.sectorId);
      this.onchangeSector();
    }
    if (client.giro != undefined) {
      this.giros = client.giro;
      this.formularioNewClient.controls.giro.setValue(client.industryId);
    }
    if (client.typeCompany != '') {
      this.formularioNewClient.controls.tipoEmpresa.setValue(client.typeCompany);
    }

    if (client.employees) {
      this.setTamEmpresa(client.employees);
    }
  }
}




