import { Component, Input, OnInit } from '@angular/core';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-error',
  templateUrl: './modal-error.component.html',
  styleUrls: ['./modal-error.component.scss']
})
export class ModalErrorComponent implements OnInit {

  @Input() data:any

  constructor(private modalService: NgbModal,) { }

  ngOnInit(): void {
  }


  closeModal = () => {
    this.modalService.dismissAll();
  }
}
