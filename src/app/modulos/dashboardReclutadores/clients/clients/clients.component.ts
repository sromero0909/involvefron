import { Component, OnInit, QueryList, ViewChild, ViewChildren, TemplateRef } from '@angular/core';

import { DecimalPipe } from '@angular/common';

import { NgbdSortableHeader } from '../../../../directives/sortable.directive';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ModalNewClientComponent } from '../modal-new-client/modal-new-client.component';
import { ModalErrorComponent } from '../modal-error/modal-error.component';

import { ToastService } from 'src/app/services/commons/toast.setvice';
import { PaginadorService } from 'src/app/services/reclutador/vacantes/paginador.service';
import { ClientsService } from 'src/app/services/reclutador/clients/clients.service';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { MainNavModel } from '../../talent/models/main-nav.model';




@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss'],
  providers: [PaginadorService, DecimalPipe]
})
export class ClientsComponent implements OnInit {

  page = 1
  rows: Array<any> = [];
  arregloOrderBy: Array<any> = []
  clientTem: any = null;
  dataModalTem: any;
  page_size: number = 10;
  page_number: number = 0;
  rowsMostrar: number = 0;
  pageActual: number = 0;
  pageTemporal: number = 0;
  restarRows: number = 0
  contadorChecks = 0;
  rowsPorPagina = 0
  totalElements: number = 0;
  totalPages: number = 0;
  contadorFiltroColumn = 0;

  checkAll: boolean = false;
  orderAsc: boolean = true;
  orderDesc: boolean = true;
  order: boolean = false;
  searchFlag: boolean = false;
  flagService: boolean = false;
  resetTable: boolean = false;
  flagReturnModalEdit: boolean = false;

  mensajeToast: string = "";
  idVacante: string = "";
  reasonId: string = "";
  filtroCliente: string = "";
  filtroReclutador: string = "";
  claseCheck: string = ""
  dataPlantilla: any;
  sortDirection: string = "&sortDirection=DESC"
  orderBy: string = "&sortBy=creationDate"
  columnActive: string = "";
  textFilter: string = "";
  idCliente: string = '';

  mainNavData: MainNavModel;

  @ViewChild('deleteClient', { static: true }) deleteClient!: TemplateRef<any>;
  @ViewChild('modalExit', { static: true }) modalExit!: TemplateRef<any>;
  @ViewChild('newClient', { static: true }) newClient!: TemplateRef<any>;
  @ViewChild('iconoChekBlanco', { static: true }) iconoChekBlanco!: TemplateRef<any>;
  @ViewChildren(NgbdSortableHeader) headers!: QueryList<NgbdSortableHeader>;
  @ViewChild(ModalNewClientComponent) newModal!: ModalNewClientComponent

  constructor(
    private modalService: NgbModal,
    public service: PaginadorService,
    public toastService: ToastService,
    private _client: ClientsService,
    public _suscription: SuscriptionService,
  ) {

  }

  ngOnInit(): void {
    this.inicializaRows();
    this.mainNavConfig();
  }

  mainNavConfig = () => {
    this.mainNavData = {
      title: 'Clientes',
      extra: false,
      badgeAlert: true,
      search: true,
      image: 'assets/img/dashboard/Clientes/logoClients.svg',
      imageClass: 'filter-green',
      iconBack: false,
    }
  }

  inicializaRows = () => {
    this.flagService = false;
    let params = {
      pageNumber: this.page_number,
      pageSize: this.page_size,
      other: `${this.orderBy}${this.sortDirection}${this.textFilter}`
    };

    this._client.getClients(params).subscribe(
      (response: any) => {
        setTimeout(() => {
          this.flagService = true;
        }, 100)
        if (response != null && response.status == 500) {
          this.totalElements = 0;
          this.totalPages = 0;
          this.showMessageError(response.error)
        } else {
          let data = (response != null && response.hasOwnProperty('content')) ? response.content : [];
          if (data.length > 0) {
            if (this.resetTable) {
              this.resetPaginadores();
            }
            for (let client of data) {
              this.rows.push({
                check: false,
                id: client.clientId,
                name: client.name,
                sector: (client.sector != null) ? client.sector.spanishName : '',
                activities: (client.industry != null) ? client.industry.spanishName : '',
                typeOfCompany: client.typeCompany,
                fecha: client.creationDate,
                numEmployeesTemp: (client.employees == null) ? '' : (client.employees == "MASDE250") ? "+ 250" : String(client.employees).replace("DE", "DE ").replace("A", " A "),
                numEmployees: client.employees,
                client2: Object.seal(client)
              });
            }
            this.totalElements = response.totalElements;
            this.totalPages = response.totalPages;
            if (this.pageActual == 0) {
              this.updateRowsMOstradas(this.pageActual + 1)
            } else {
              this.updateRowsMOstradas(this.pageActual)
            }
          }
        }

      },
      (error) => {
        this.showMessageError("Ocurrio un error");
      }
    );
  }

  onchangeCheck(id: number) {
    for (let i = 0; i < this.rows.length; i++) {
      if (this.rows[i].id == id) {
        (this.rows[i].check) ? this.contadorChecks-- : this.contadorChecks++
        this.rows[i].check = !this.rows[i].check
      }
    }
    if (this.contadorChecks > 0 && this.contadorChecks < this.totalElements) {
      this.claseCheck = "minus"; this.checkAll = true
    } else {
      this.claseCheck = ""; this.checkAll = false
      if (this.contadorChecks == this.totalElements) {
        this.checkAll = true
      }
    }
  }

  selectAll() {
    this.checkAll = !this.checkAll;
    for (let i = 0; i < this.rows.length; i++) {
      this.contadorChecks = (this.checkAll) ? this.rows.length : 0
      this.rows[i].check = this.checkAll;
    }
    if (this.contadorChecks > 0 && this.contadorChecks < this.totalElements) { this.claseCheck = "minus"; this.checkAll = true } else { this.claseCheck = ""; }

  }

  limpiarSeleccion() {
    for (let i = 0; i < this.rows.length; i++) {
      this.contadorChecks = 0
      this.rows[i].check = false
    }
    this.checkAll = false
  }

  eliminarClients(idClient: string) {
    const clientes: any = [];

    let unCliente = false
    if (idClient != "") {
      unCliente = true;
      clientes.push(this.idCliente)
    } else {
      for (let row of this.rows) {
        if (row.check) {
          clientes.push(row.id)
        }
      }
    }
    this._client.deleteClient(clientes).subscribe(
      (response: any) => {
        if (response != null && (response.status == 204 || response.status == 200)) {
          let reload = false;
          const message = response.headers.get('message') || (clientes.length === 0 ? 'El cliente ha sido eliminado' : 'Los clientes han sido eliminado');
          this.contadorChecks = this.contadorChecks - response.body;
          if (response.body == null) {
            this.showModalError("No es posible eliminar", message);
            unCliente = false
            reload = false
          }
          if (response.body > 0 && response.body < clientes.length) {
            this.showModalError(response.body + " " + "de " + clientes.length + " clientes han sido eliminados", message);
            reload = true
          }

          if (response.body == clientes.length) {
            reload = true
          }

          if (reload || unCliente) {
            if (response.body == clientes.length || unCliente) {
              this.mensajeToast = message
              this.showSuccess(this.iconoChekBlanco);
            }
            this.resetTable = true;
            this.contadorChecks = 0;
            this.checkAll = false
            this.inicializaRows();
          }
        } else {
          const message = response.headers.get('message');
          this.showMessageError(message);
        }
      },
      (error) => {
        const responseMessage = error.headers.get('message');
        const deleted = error.headers.get('vacantes-eliminadas');
        // alert(`No se pudieron borrar todos. Enviados: ${clientes.length}, borrados: ${deleted}. Message: ${message}`);
        // this.showMessageError(message);
        if (Number(deleted) > 0) {
          this.showModalError(`Se han eliminado ${deleted} de ${clientes.length} clientes`, this.deleteClientsErrorHandler(responseMessage));
          this.rows = [];
          this.inicializaRows();
        }
        else {
          if (clientes.length === 1) {
            // Se intentó eliminar UN cliente con vacantes activas o pausadas
            this.showModalError('No podemos eliminar al cliente', 'Tiene vacantes activas, pruebe reasignando las vacantes e intente más tarde');
          }
          else {
            // Se intentaron eliminar múltiples clientes, todos con vacantes activas o pausadas
            this.showModalError('No podemos eliminar a los clientes', this.deleteClientsErrorHandler(responseMessage));
          }
        }
      }
    );
  }

  deleteClientsErrorHandler(message: string): string {
    if (message.includes('activas, ')) {
      return 'El resto aún tienen vacantes activas.';
    }
    else if (message.includes('pausadas, ')) {
      return 'El resto aún tienen vacantes pausadas.';
    }
    else if (message.includes('activas y/o pausadas, ')) {
      return 'El resto aún tiene vacantes activas o detenidas.';
    }
    else {
      return message;
    }
  }

  selectPage(page: string) {
    this.page = parseInt(page, 10) || 1;
  }

  formatInput(input: HTMLInputElement) {
    input.value = input.value.replace(FILTER_PAG_REGEX, '');
  }


  onSort({ column, direction }: any) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = 'sort';
      }
    });

    if (direction == 'sort') {
      this.sortDirection = "&sortDirection=DESC"
      this.orderBy = "&sortBy=creationDate"

    } else {
      this.sortDirection = "&sortDirection=" + direction.toUpperCase()
      this.orderBy = "&sortBy=" + column
    }
    this.resetTable = true
    this.inicializaRows();
  }

  showSuccess(mensaje: any) {
    this.toastService.remove(1)
    this.toastService.show(mensaje, { classname: 'bg-success text-light ngb-toastsBottom', delay: 5000 });
  }

  setMensajeNotificacion(mensaje: string) {
    this.mensajeToast = mensaje;
  }

  rowsMostradas(page: any) {
    this.page_number = page - 1
    if (this.pageTemporal < parseInt(page) - 1) {
      this.resetTable = false;
      this.inicializaRows();
      this.pageTemporal = page - 1;
    }
    this.updateRowsMOstradas(page)
  }

  updateRowsMOstradas(page: any) {
    setTimeout(() => {
      let a = this.rows.slice((this.page - 1) * this.page_size, page * this.page_size)
      this.rowsPorPagina = a.length;
      if (this.pageActual <= page) {
        this.rowsMostrar = this.rowsMostrar + a.length;
        this.restarRows = a.length;
        this.pageActual = page;
      } else {
        this.rowsMostrar = this.rowsMostrar - this.restarRows;
        this.pageActual = page;
      }
    }, 100)
  }

  selecRow = () => {
    this.resetTable = true;
    this.inicializaRows()
  }

  resetPaginadores = () => {
    this.rowsMostrar = 0;
    this.pageActual = 0;
    this.page_number = 0;
    this.restarRows = 0;
    this.pageTemporal = 0;
    this.rows = [];
    this.page = 1
  }

  showMessageError = (dangerTpl: any) => {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-light ngb-toastsBottom', delay: 3000 });
  }

  openModalDeleteClient = (idClient: string) => {
    this.idCliente = idClient
    const modalRef = this.modalService.open(this.deleteClient, { centered: true });
  }

  openModalMessageDiscard = () => {
    const modalRef = this.modalService.open(this.modalExit, { centered: true });
  }

  openModalNewClient = (client: any, status: string) => {
    if (client != null) {
      this._client.setClietTem(client)
    }
    const modalRef = this.modalService.open(ModalNewClientComponent, { size: 'lg', centered: true });
    const data = { status: status }
    modalRef.componentInstance.data = data
    modalRef.componentInstance.createsClientInside = true;
    modalRef.dismissed.subscribe(
      (data) => {
        switch (data.status) {
          case 0:
            break;
          case "ok":
            this.mensajeToast = data.message
            this.showSuccess(this.iconoChekBlanco);
            this.resetTable = true;
            this.inicializaRows();
            break;
          case "nok":
            this.showMessageError(data.message)
            break;
          default:
            if (data == 0) {
              this.flagReturnModalEdit = false;
              this.openModalMessageDiscard();
              modalRef.componentInstance.getDataTem()
            }
            break;
        }
      }
    );
    modalRef.componentInstance.dataTem.subscribe((data: any) => {
      this.dataModalTem = data
    })
  }

  openModalEditClient = (client: any) => {
    this.clientTem = client;
    if (client != null) {
      this._client.setClietTem(client)
    }
    const modalRef = this.modalService.open(ModalNewClientComponent, { size: 'lg', centered: true });
    const data = { status: "edit" }

    modalRef.componentInstance.data = data
    modalRef.dismissed.subscribe(
      (data) => {
        switch (data.status) {
          case "ok":
            this.mensajeToast = data.message
            this.showSuccess(this.iconoChekBlanco);
            this.resetTable = true;
            this.inicializaRows();
            break;
          case "nok":
            this.showMessageError(data.message)
            break;
          default:
            if (data == 0) {
              this.flagReturnModalEdit = true;
              this.openModalMessageDiscard();
              modalRef.componentInstance.getDataTem()
            }
            break;
        }
      }
    );
    modalRef.componentInstance.dataTem.subscribe((data: any) => {
      this.dataModalTem = data
    })
  }

  showModalError = (title: string, message: string) => {
    const modalRef = this.modalService.open(ModalErrorComponent, { centered: true });
    const data = { title, message }
    modalRef.componentInstance.data = data
    modalRef.dismissed.subscribe(
      (data) => {
      }
    );
  }

  searchClient = (text: string) => {
    this.textFilter = "&query=" + text
    this.resetTable = true;
    this.inicializaRows();
  }

  returEditModal = () => {
    this.openModalEditClient(this.dataModalTem)
  }

  returnModalNewClient = () => {
    this.openModalNewClient(this.dataModalTem, "returnNewClient")
  }

  returnModal = () => {
    if (this.flagReturnModalEdit) {
      this.returEditModal();
    } else {
      this.returnModalNewClient()
    }

  }
}

const FILTER_PAG_REGEX = /[^0-9]/g;
