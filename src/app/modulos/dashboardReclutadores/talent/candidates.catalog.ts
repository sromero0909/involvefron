export const candidates = {
    "candidates": {
        "content": [
            {
                "candidateId": "2c9f906e7c56caf3017c5839071d0004",
                "postalCodePosition": null,
                "maritalStatus": null,
                "nacionality": {
                    "nationalityId": "40288086796ba27001796bbcf9770000",
                    "spanishName": "Mexicana",
                    "englishName": "Mexican"
                },
                "residenceCountry": null,
                "stateResidence": null,
                "townshipResidence": null,
                "desiredSalary": 26000.00,
                "dateBirth": null,
                "changeResidence": null,
                "statusEmail": true,
                "statusPhone": true,
                "age": 53,
                "updateCV": "2021-07-15",
                "porcentageProfilel": 100,
                "steepsOnboarding": null,
                "notWorkExperience": null,
                "residentJob": null,
                "user": {
                    "userId": "2c9f906e7c56caf3017c5839071d0005",
                    "name": "Mariano",
                    "lastName": "Romero",
                    "checkCode": null,
                    "secondLastName": "Pichardo",
                    "email": "rafaelgarciamarquez1234@gmail.com",
                    "password": "$2a$10$Pv7K/qHQwNicjogNsBMj2u4XBIt7T2gf6b9A1.tU8U4Sc5oVQCpmq",
                    "gender": "NO_BINARY",
                    "photo": null,
                    "phone": null,
                    "attempts": 0,
                    "userRol": "CANDIDATE",
                    "userSendbird": "candidate_2c9f906e7c56caf3017c5839071d0005",
                    "acceptPrivacy": false,
                    "acceptTerms": true,
                    "acceptNewsletter": true,
                    "createDate": "2021-10-07",
                    "lastActivity": "2021-10-11",
                    "historyDevices": []
                },
                "positionProfile": {
                    "positionId": "402880de7bcc192f017bcc66c4be0007",
                    "position": "Desarrollador IOs"
                },
                "aboutMeComplete": false,
                "professionalDataComplete": false,
                "laboralExperienceComplete": false,
                "skillsComplete": false,
                "educationComplete": false,
                "porcentageColor": "#F2B732",
                "pathPresentationVideo": null,
                "uploadPresentationVideo": null,
                "expirationPresentationVideo": null,
                "questionPresentation1": null,
                "questionPresentation2": null,
                "urlFiles": [],
                "academicHistory": [],
                "languages": [
                    {
                        "candidateLanguage": "86908",
                        "level": "BASICO",
                        "language": {
                            "languageId": "402880de79733a900179734054b70003",
                            "spanishName": "Japonés",
                            "englishName": "Japanese"
                        },
                        "candidateId": "2c9f906e7c56caf3017c5839071d0004"
                    },
                    {
                        "candidateLanguage": "86925",
                        "level": "INTERMEDIO",
                        "language": {
                            "languageId": "402880de79733a9001797340583f0006",
                            "spanishName": "Chino",
                            "englishName": "Chinese"
                        },
                        "candidateId": "2c9f906e7c56caf3017c5839071d0004"
                    }
                ],
                "courses": [],
                "certifications": [],
                "hardSkills": [
                    {
                        "skillCandidateId": "8687ajdd13",
                        "level": "AVANZADO",
                        "skill": {
                            "skillId": "2c9f906e7c5d0486017c6067f37a0010",
                            "name": "Pruebas unitarias"
                        },
                        "candidateId": "2c9f906e7c56caf3017c5839071d0004"
                    },
                    {
                        "skillCandidateId": "8687ajdd33",
                        "level": "INTERMEDIO",
                        "skill": {
                            "skillId": "2c9f906e7c5d0486017c6067f352000d",
                            "name": "Angular"
                        },
                        "candidateId": "2c9f906e7c56caf3017c5839071d0004"
                    },
                    {
                        "skillCandidateId": "8687ajdd53",
                        "level": "EXPERTO",
                        "skill": {
                            "skillId": "2c9f906e7c5bd0c6017c5c8d279d002a",
                            "name": "Expresión oral"
                        },
                        "candidateId": "2c9f906e7c56caf3017c5839071d0004"
                    }
                ],
                "softSkills": [],
                "areas": [],
                "specialties": [],
                "workExperiencie": [],
                "positionRequired": null,
                "idealJob": null,
                "countryPosition": null,
                "statePosition": "México",
                "townshipPosition": null,
                "minSalary": null,
                "maxSalary": null,
                "workingDay": null,
                "modality": null,
                "contract": null,
                "typeCompany": null,
                "timeVacant": null,
                "firstSearch": null,
                "notificationsPush": null
            },
            {
                "candidateId": "2c9f906e7c58bb47017c6186826b000c",
                "postalCodePosition": null,
                "maritalStatus": null,
                "nacionality": null,
                "residenceCountry": null,
                "stateResidence": null,
                "townshipResidence": null,
                "desiredSalary": 18000.00,
                "dateBirth": null,
                "changeResidence": null,
                "statusEmail": true,
                "statusPhone": true,
                "age": 20,
                "updateCV": "2021-09-25",
                "porcentageProfilel": 100,
                "steepsOnboarding": null,
                "notWorkExperience": null,
                "residentJob": null,
                "user": {
                    "userId": "2c9f906e7c58bb47017c6186826b000d",
                    "name": "Jonathan",
                    "lastName": "Sanchez",
                    "checkCode": null,
                    "secondLastName": "Flores",
                    "email": "yalarcon@grupotama.mx",
                    "password": null,
                    "gender": "WOMAN",
                    "photo": null,
                    "phone": null,
                    "attempts": 0,
                    "userRol": "CANDIDATE",
                    "userSendbird": "candidate_2c9f906e7c58bb47017c6186826b000d",
                    "acceptPrivacy": false,
                    "acceptTerms": false,
                    "acceptNewsletter": false,
                    "createDate": "2021-10-08",
                    "lastActivity": null,
                    "historyDevices": []
                },
                "positionProfile": {
                    "positionId": "402880de7bcc192f017bcc66c4be0007",
                    "position": "Desarrollador IOs"
                },
                "aboutMeComplete": false,
                "professionalDataComplete": false,
                "laboralExperienceComplete": false,
                "skillsComplete": false,
                "educationComplete": false,
                "porcentageColor": "#F2B732",
                "pathPresentationVideo": null,
                "uploadPresentationVideo": null,
                "expirationPresentationVideo": null,
                "questionPresentation1": null,
                "questionPresentation2": null,
                "urlFiles": [],
                "academicHistory": [],
                "languages": [],
                "courses": [],
                "certifications": [],
                "hardSkills": [],
                "softSkills": [],
                "areas": [],
                "specialties": [],
                "workExperiencie": [],
                "positionRequired": null,
                "idealJob": null,
                "countryPosition": null,
                "statePosition": "México",
                "townshipPosition": null,
                "minSalary": null,
                "maxSalary": null,
                "workingDay": null,
                "modality": null,
                "contract": null,
                "typeCompany": null,
                "timeVacant": null,
                "firstSearch": null,
                "notificationsPush": null
            },
            {
                "candidateId": "402880857bd103b0017bd1050a790000",
                "postalCodePosition": null,
                "maritalStatus": null,
                "nacionality": null,
                "residenceCountry": null,
                "stateResidence": null,
                "townshipResidence": null,
                "desiredSalary": 21000.00,
                "dateBirth": null,
                "changeResidence": null,
                "statusEmail": true,
                "statusPhone": true,
                "age": 35,
                "updateCV": "2021-11-10",
                "porcentageProfilel": 100,
                "steepsOnboarding": null,
                "notWorkExperience": null,
                "residentJob": null,
                "user": {
                    "userId": "402880857bd103b0017bd1050a830001",
                    "name": "Araceli",
                    "lastName": "Mora",
                    "checkCode": null,
                    "secondLastName": "Hernández",
                    "email": "araceli.morahernandez06q@gmail.com",
                    "password": "$2a$10$ENQwZetLCk9UYuhQoP1srOe/698RoGIq1uKWjzZHAD/TyN4VNrozO",
                    "gender": "WOMAN",
                    "photo": null,
                    "phone": null,
                    "attempts": 0,
                    "userRol": "CANDIDATE",
                    "userSendbird": null,
                    "acceptPrivacy": true,
                    "acceptTerms": true,
                    "acceptNewsletter": true,
                    "createDate": "2021-09-10",
                    "lastActivity": "2021-09-13",
                    "historyDevices": []
                },
                "positionProfile": {
                    "positionId": "402880de7bcc192f017bcc66c4be0007",
                    "position": "Desarrollador IOs"
                },
                "aboutMeComplete": false,
                "professionalDataComplete": false,
                "laboralExperienceComplete": false,
                "skillsComplete": false,
                "educationComplete": false,
                "porcentageColor": "#F2B732",
                "pathPresentationVideo": null,
                "uploadPresentationVideo": null,
                "expirationPresentationVideo": null,
                "questionPresentation1": null,
                "questionPresentation2": null,
                "urlFiles": [],
                "academicHistory": [],
                "languages": [
                    {
                        "candidateLanguage": "86900",
                        "level": "AVANZADO",
                        "language": {
                            "languageId": "402880de79733a90017973405bd70009",
                            "spanishName": "Portugués",
                            "englishName": "Portuguese"
                        },
                        "candidateId": "402880857bd103b0017bd1050a790000"
                    },
                    {
                        "candidateLanguage": "86917",
                        "level": "NATIVO",
                        "language": {
                            "languageId": "402880de79733a90017973405f53000c",
                            "spanishName": "Neerlandés",
                            "englishName": "Dutch"
                        },
                        "candidateId": "402880857bd103b0017bd1050a790000"
                    },
                    {
                        "candidateLanguage": "86934",
                        "level": "AVANZADO",
                        "language": {
                            "languageId": "402880de79733a9001797340525a0001",
                            "spanishName": "Francés",
                            "englishName": "French"
                        },
                        "candidateId": "402880857bd103b0017bd1050a790000"
                    }
                ],
                "courses": [],
                "certifications": [],
                "hardSkills": [
                    {
                        "skillCandidateId": "8687ajdd23",
                        "level": "EXPERTO",
                        "skill": {
                            "skillId": "ff8081817c04fda3017c04fe3ffd001ek",
                            "name": "Java"
                        },
                        "candidateId": "402880857bd103b0017bd1050a790000"
                    },
                    {
                        "skillCandidateId": "8687ajdd43",
                        "level": "AVANZADO",
                        "skill": {
                            "skillId": "ff8081817c04fda3017c04fe3ffd001e6",
                            "name": "Angular JS"
                        },
                        "candidateId": "402880857bd103b0017bd1050a790000"
                    },
                    {
                        "skillCandidateId": "8687ajdd63",
                        "level": "INTERMEDIO",
                        "skill": {
                            "skillId": "ff8081817c04fda3017c04fe3ffd001e1",
                            "name": "React"
                        },
                        "candidateId": "402880857bd103b0017bd1050a790000"
                    }
                ],
                "softSkills": [],
                "areas": [],
                "specialties": [],
                "workExperiencie": [],
                "positionRequired": null,
                "idealJob": null,
                "countryPosition": null,
                "statePosition": "México",
                "townshipPosition": null,
                "minSalary": null,
                "maxSalary": null,
                "workingDay": null,
                "modality": null,
                "contract": null,
                "typeCompany": null,
                "timeVacant": null,
                "firstSearch": null,
                "notificationsPush": null
            },
            {
                "candidateId": "2c9f906e7c56caf3017c58215bec0002",
                "postalCodePosition": null,
                "maritalStatus": null,
                "nacionality": null,
                "residenceCountry": null,
                "stateResidence": null,
                "townshipResidence": null,
                "desiredSalary": 28000.00,
                "dateBirth": null,
                "changeResidence": null,
                "statusEmail": true,
                "statusPhone": true,
                "age": 38,
                "updateCV": "2021-08-30",
                "porcentageProfilel": 100,
                "steepsOnboarding": null,
                "notWorkExperience": null,
                "residentJob": null,
                "user": {
                    "userId": "2c9f906e7c56caf3017c58215bec0003",
                    "name": "Marcos",
                    "lastName": "Mandujano",
                    "checkCode": null,
                    "secondLastName": "Rivero",
                    "email": "gallardomarquezricardo",
                    "password": null,
                    "gender": "MAN",
                    "photo": null,
                    "phone": null,
                    "attempts": 0,
                    "userRol": "CANDIDATE",
                    "userSendbird": "candidate_2c9f906e7c56caf3017c58215bec0003",
                    "acceptPrivacy": false,
                    "acceptTerms": true,
                    "acceptNewsletter": true,
                    "createDate": "2021-10-07",
                    "lastActivity": null,
                    "historyDevices": []
                },
                "positionProfile": {
                    "positionId": "402880de7bcc192f017bcc6da89e0009",
                    "position": "Desarrollador Android"
                },
                "aboutMeComplete": false,
                "professionalDataComplete": false,
                "laboralExperienceComplete": false,
                "skillsComplete": false,
                "educationComplete": false,
                "porcentageColor": "#F2B732",
                "pathPresentationVideo": null,
                "uploadPresentationVideo": null,
                "expirationPresentationVideo": null,
                "questionPresentation1": null,
                "questionPresentation2": null,
                "urlFiles": [],
                "academicHistory": [],
                "languages": [
                    {
                        "candidateLanguage": "86890",
                        "level": "AVANZADO",
                        "language": {
                            "languageId": "402880de79733a9001797340607d000d",
                            "spanishName": "Coreano",
                            "englishName": "Korean"
                        },
                        "candidateId": "2c9f906e7c56caf3017c58215bec0002"
                    },
                    {
                        "candidateLanguage": "86907",
                        "level": "INTERMEDIO",
                        "language": {
                            "languageId": "402880de79733a900179734053890002",
                            "spanishName": "Alemán",
                            "englishName": "German"
                        },
                        "candidateId": "2c9f906e7c56caf3017c58215bec0002"
                    },
                    {
                        "candidateLanguage": "86924",
                        "level": "BASICO",
                        "language": {
                            "languageId": "402880de79733a900179734057120005",
                            "spanishName": "Ruso",
                            "englishName": "Russian"
                        },
                        "candidateId": "2c9f906e7c56caf3017c58215bec0002"
                    }
                ],
                "courses": [],
                "certifications": [],
                "hardSkills": [
                    {
                        "skillCandidateId": "8687ajdd12",
                        "level": "BASICO",
                        "skill": {
                            "skillId": "2c9f906e7c5d0486017c6067f352000d",
                            "name": "Angular"
                        },
                        "candidateId": "2c9f906e7c56caf3017c58215bec0002"
                    },
                    {
                        "skillCandidateId": "8687ajdd32",
                        "level": "BASICO",
                        "skill": {
                            "skillId": "2c9f906e7c5bd0c6017c5c8d279d002a",
                            "name": "Expresión oral"
                        },
                        "candidateId": "2c9f906e7c56caf3017c58215bec0002"
                    },
                    {
                        "skillCandidateId": "8687ajdd52",
                        "level": "AVANZADO",
                        "skill": {
                            "skillId": "2c9f906e7c5bd0c6017c5c86463c001e",
                            "name": "Habilidades lingüísticas."
                        },
                        "candidateId": "2c9f906e7c56caf3017c58215bec0002"
                    }
                ],
                "softSkills": [],
                "areas": [],
                "specialties": [],
                "workExperiencie": [],
                "positionRequired": null,
                "idealJob": null,
                "countryPosition": null,
                "statePosition": "México",
                "townshipPosition": null,
                "minSalary": 2000.00,
                "maxSalary": 25000.00,
                "workingDay": null,
                "modality": null,
                "contract": null,
                "typeCompany": null,
                "timeVacant": null,
                "firstSearch": null,
                "notificationsPush": null
            },
            {
                "candidateId": "2c9f906e7c58bb47017c6189c1e4000e",
                "postalCodePosition": null,
                "maritalStatus": null,
                "nacionality": {
                    "nationalityId": "40288086796ba27001796bbcff430005",
                    "spanishName": "Española",
                    "englishName": "Spanish"
                },
                "residenceCountry": null,
                "stateResidence": null,
                "townshipResidence": null,
                "desiredSalary": 21000.00,
                "dateBirth": null,
                "changeResidence": null,
                "statusEmail": true,
                "statusPhone": true,
                "age": 30,
                "updateCV": "2021-07-15",
                "porcentageProfilel": 100,
                "steepsOnboarding": null,
                "notWorkExperience": null,
                "residentJob": null,
                "user": {
                    "userId": "2c9f906e7c58bb47017c6189c1e4000f",
                    "name": "Abraham",
                    "lastName": "Morales",
                    "checkCode": null,
                    "secondLastName": "Rosales",
                    "email": "gatitorigama86123abc@gmail.com",
                    "password": "$2a$10$Pv7K/qHQwNicjogNsBMj2u4XBIt7T2gf6b9A1.tU8U4Sc5oVQCpmq",
                    "gender": "MAN",
                    "photo": null,
                    "phone": null,
                    "attempts": 0,
                    "userRol": "CANDIDATE",
                    "userSendbird": "candidate_2c9f906e7c58bb47017c6189c1e4000f",
                    "acceptPrivacy": false,
                    "acceptTerms": false,
                    "acceptNewsletter": false,
                    "createDate": "2021-10-08",
                    "lastActivity": "2021-10-12",
                    "historyDevices": []
                },
                "positionProfile": {
                    "positionId": "402880de7bcc192f017bcc6da89e0009",
                    "position": "Desarrollador Android"
                },
                "aboutMeComplete": false,
                "professionalDataComplete": false,
                "laboralExperienceComplete": false,
                "skillsComplete": false,
                "educationComplete": false,
                "porcentageColor": "#F2B732",
                "pathPresentationVideo": null,
                "uploadPresentationVideo": null,
                "expirationPresentationVideo": null,
                "questionPresentation1": null,
                "questionPresentation2": null,
                "urlFiles": [],
                "academicHistory": [],
                "languages": [],
                "courses": [],
                "certifications": [],
                "hardSkills": [],
                "softSkills": [],
                "areas": [],
                "specialties": [],
                "workExperiencie": [],
                "positionRequired": null,
                "idealJob": null,
                "countryPosition": null,
                "statePosition": "México",
                "townshipPosition": null,
                "minSalary": null,
                "maxSalary": null,
                "workingDay": null,
                "modality": null,
                "contract": null,
                "typeCompany": null,
                "timeVacant": null,
                "firstSearch": null,
                "notificationsPush": null
            },
            {
                "candidateId": "402881cb7be4bda4017be4bf1e2d0000",
                "postalCodePosition": null,
                "maritalStatus": null,
                "nacionality": null,
                "residenceCountry": null,
                "stateResidence": null,
                "townshipResidence": null,
                "desiredSalary": 27000.00,
                "dateBirth": null,
                "changeResidence": null,
                "statusEmail": true,
                "statusPhone": true,
                "age": 32,
                "updateCV": "2021-04-19",
                "porcentageProfilel": 100,
                "steepsOnboarding": null,
                "notWorkExperience": null,
                "residentJob": null,
                "user": {
                    "userId": "402881cb7be4bda4017be4bf1e380001",
                    "name": "Jose",
                    "lastName": "Azaña",
                    "checkCode": null,
                    "secondLastName": "Contreras",
                    "email": "jose@gmail.com",
                    "password": "$2a$10$LRyXw41NO/qIqEmjEVHfmO/5IwQf/zwB7Yt72d3es0Gupy6Qnkq66",
                    "gender": "MAN",
                    "photo": null,
                    "phone": null,
                    "attempts": 0,
                    "userRol": "CANDIDATE",
                    "userSendbird": null,
                    "acceptPrivacy": true,
                    "acceptTerms": true,
                    "acceptNewsletter": true,
                    "createDate": "2021-09-14",
                    "lastActivity": null,
                    "historyDevices": []
                },
                "positionProfile": {
                    "positionId": "402880de7bcc192f017bcc6da89e0009",
                    "position": "Desarrollador Android"
                },
                "aboutMeComplete": false,
                "professionalDataComplete": false,
                "laboralExperienceComplete": false,
                "skillsComplete": false,
                "educationComplete": false,
                "porcentageColor": "#F2B732",
                "pathPresentationVideo": null,
                "uploadPresentationVideo": null,
                "expirationPresentationVideo": null,
                "questionPresentation1": null,
                "questionPresentation2": null,
                "urlFiles": [],
                "academicHistory": [],
                "languages": [
                    {
                        "candidateLanguage": "86902",
                        "level": "BASICO",
                        "language": {
                            "languageId": "402880de79733a90017973405e2d000b",
                            "spanishName": "Indonesio",
                            "englishName": "Indonesian"
                        },
                        "candidateId": "402881cb7be4bda4017be4bf1e2d0000"
                    },
                    {
                        "candidateLanguage": "86936",
                        "level": "AVANZADO",
                        "language": {
                            "languageId": "402880de79733a900179734054b70003",
                            "spanishName": "Japonés",
                            "englishName": "Japanese"
                        },
                        "candidateId": "402881cb7be4bda4017be4bf1e2d0000"
                    }
                ],
                "courses": [],
                "certifications": [],
                "hardSkills": [
                    {
                        "skillCandidateId": "8687ajdd25",
                        "level": "INTERMEDIO",
                        "skill": {
                            "skillId": "ff8081817c04fda3017c04fe3ffd001es",
                            "name": "Python"
                        },
                        "candidateId": "402881cb7be4bda4017be4bf1e2d0000"
                    },
                    {
                        "skillCandidateId": "8687ajdd45",
                        "level": "BASICO",
                        "skill": {
                            "skillId": "ff8081817c04fda3017c04fe3ffd001ep",
                            "name": "PHP"
                        },
                        "candidateId": "402881cb7be4bda4017be4bf1e2d0000"
                    },
                    {
                        "skillCandidateId": "8687ajdd65",
                        "level": "EXPERTO",
                        "skill": {
                            "skillId": "ff8081817c04fda3017c04fe3ffd001ek",
                            "name": "Java"
                        },
                        "candidateId": "402881cb7be4bda4017be4bf1e2d0000"
                    }
                ],
                "softSkills": [],
                "areas": [],
                "specialties": [],
                "workExperiencie": [],
                "positionRequired": null,
                "idealJob": null,
                "countryPosition": null,
                "statePosition": "México",
                "townshipPosition": null,
                "minSalary": null,
                "maxSalary": null,
                "workingDay": null,
                "modality": null,
                "contract": null,
                "typeCompany": null,
                "timeVacant": null,
                "firstSearch": null,
                "notificationsPush": null
            },
            {
                "candidateId": "2c9eb0817c101089017c101161b30000",
                "postalCodePosition": null,
                "maritalStatus": null,
                "nacionality": {
                    "nationalityId": "40288086796ba27001796bbcf9770000",
                    "spanishName": "Mexicana",
                    "englishName": "Mexican"
                },
                "residenceCountry": null,
                "stateResidence": null,
                "townshipResidence": null,
                "desiredSalary": 19000.00,
                "dateBirth": null,
                "changeResidence": null,
                "statusEmail": true,
                "statusPhone": true,
                "age": 46,
                "updateCV": "2021-09-25",
                "porcentageProfilel": 100,
                "steepsOnboarding": null,
                "notWorkExperience": null,
                "residentJob": null,
                "user": {
                    "userId": "2c9eb0817c101089017c101161c90001",
                    "name": "Yolanda",
                    "lastName": "Prat",
                    "checkCode": null,
                    "secondLastName": "Gámez",
                    "email": "jlima6@grupotama.mx",
                    "password": "$2a$10$Pv7K/qHQwNicjogNsBMj2u4XBIt7T2gf6b9A1.tU8U4Sc5oVQCpmq",
                    "gender": "MAN",
                    "photo": null,
                    "phone": null,
                    "attempts": 0,
                    "userRol": "CANDIDATE",
                    "userSendbird": null,
                    "acceptPrivacy": false,
                    "acceptTerms": false,
                    "acceptNewsletter": false,
                    "createDate": "2021-09-22",
                    "lastActivity": "2021-10-09",
                    "historyDevices": [
                        {
                            "historyDevicesId": "2c9eb0817c101089017c101557640002",
                            "creationDeviceDate": "2021-09-23 08:25:01",
                            "nameDevice": "Crome",
                            "versionDevice": "9.2",
                            "statusDevice": true,
                            "userFirebase": null,
                            "userId": "2c9eb0817c101089017c101161c90001"
                        }
                    ]
                },
                "positionProfile": {
                    "positionId": "402881cb7c32c2cf017c338c4d59000a",
                    "position": "Desarrollador backend"
                },
                "aboutMeComplete": false,
                "professionalDataComplete": false,
                "laboralExperienceComplete": false,
                "skillsComplete": false,
                "educationComplete": false,
                "porcentageColor": "#F2B732",
                "pathPresentationVideo": null,
                "uploadPresentationVideo": null,
                "expirationPresentationVideo": null,
                "questionPresentation1": null,
                "questionPresentation2": null,
                "urlFiles": [],
                "academicHistory": [],
                "languages": [
                    {
                        "candidateLanguage": "86888",
                        "level": "AVANZADO",
                        "language": {
                            "languageId": "402880de79733a90017973405e2d000b",
                            "spanishName": "Indonesio",
                            "englishName": "Indonesian"
                        },
                        "candidateId": "2c9eb0817c101089017c101161b30000"
                    },
                    {
                        "candidateLanguage": "86922",
                        "level": "AVANZADO",
                        "language": {
                            "languageId": "402880de79733a900179734054b70003",
                            "spanishName": "Japonés",
                            "englishName": "Japanese"
                        },
                        "candidateId": "2c9eb0817c101089017c101161b30000"
                    }
                ],
                "courses": [],
                "certifications": [],
                "hardSkills": [
                    {
                        "skillCandidateId": "8687ajdd11",
                        "level": "EXPERTO",
                        "skill": {
                            "skillId": "2c9f906e7c5bd0c6017c5c8d279d002a",
                            "name": "Expresión oral"
                        },
                        "candidateId": "2c9eb0817c101089017c101161b30000"
                    },
                    {
                        "skillCandidateId": "8687ajdd31",
                        "level": "INTERMEDIO",
                        "skill": {
                            "skillId": "2c9f906e7c5bd0c6017c5c86463c001e",
                            "name": "Habilidades lingüísticas."
                        },
                        "candidateId": "2c9eb0817c101089017c101161b30000"
                    },
                    {
                        "skillCandidateId": "8687ajdd51",
                        "level": "INTERMEDIO",
                        "skill": {
                            "skillId": "2c9eb0817c3256f7017c3275b2c70004",
                            "name": "Conocimientos de marketing y negocios"
                        },
                        "candidateId": "2c9eb0817c101089017c101161b30000"
                    },
                    {
                        "skillCandidateId": "86888",
                        "level": "AVANZADO",
                        "skill": {
                            "skillId": "402881cb7c32c2cf017c336613790006",
                            "name": "Herramientas de análisis de datos"
                        },
                        "candidateId": "2c9eb0817c101089017c101161b30000"
                    }
                ],
                "softSkills": [],
                "areas": [],
                "specialties": [],
                "workExperiencie": [],
                "positionRequired": null,
                "idealJob": null,
                "countryPosition": null,
                "statePosition": "México",
                "townshipPosition": null,
                "minSalary": null,
                "maxSalary": null,
                "workingDay": null,
                "modality": null,
                "contract": null,
                "typeCompany": null,
                "timeVacant": null,
                "firstSearch": null,
                "notificationsPush": null
            },
            {
                "candidateId": "2c9fd42d7c0a9815017c0e77a1960008",
                "postalCodePosition": null,
                "maritalStatus": null,
                "nacionality": null,
                "residenceCountry": null,
                "stateResidence": null,
                "townshipResidence": null,
                "desiredSalary": 10000.00,
                "dateBirth": null,
                "changeResidence": null,
                "statusEmail": true,
                "statusPhone": true,
                "age": 46,
                "updateCV": "2021-02-13",
                "porcentageProfilel": 100,
                "steepsOnboarding": null,
                "notWorkExperience": null,
                "residentJob": null,
                "user": {
                    "userId": "2c9fd42d7c0a9815017c0e77a1960009",
                    "name": "Pedro Antonio",
                    "lastName": "Flores",
                    "checkCode": null,
                    "secondLastName": "Bustamante",
                    "email": "rgm486@gmail.com",
                    "password": "password",
                    "gender": "NO_BINARY",
                    "photo": null,
                    "phone": null,
                    "attempts": 3,
                    "userRol": "CANDIDATE",
                    "userSendbird": null,
                    "acceptPrivacy": false,
                    "acceptTerms": true,
                    "acceptNewsletter": true,
                    "createDate": "2021-09-22",
                    "lastActivity": null,
                    "historyDevices": []
                },
                "positionProfile": {
                    "positionId": "402881cb7c32c2cf017c338c4d59000a",
                    "position": "Desarrollador backend"
                },
                "aboutMeComplete": false,
                "professionalDataComplete": false,
                "laboralExperienceComplete": false,
                "skillsComplete": false,
                "educationComplete": false,
                "porcentageColor": "#F2B732",
                "pathPresentationVideo": null,
                "uploadPresentationVideo": null,
                "expirationPresentationVideo": null,
                "questionPresentation1": null,
                "questionPresentation2": null,
                "urlFiles": [],
                "academicHistory": [],
                "languages": [
                    {
                        "candidateLanguage": "86898",
                        "level": "NATIVO",
                        "language": {
                            "languageId": "402880de79733a9001797340596b0007",
                            "spanishName": "Árabe",
                            "englishName": "Arabic"
                        },
                        "candidateId": "2c9fd42d7c0a9815017c0e77a1960008"
                    },
                    {
                        "candidateLanguage": "86915",
                        "level": "BASICO",
                        "language": {
                            "languageId": "402880de79733a90017973405d00000a",
                            "spanishName": "Turco",
                            "englishName": "Turkish"
                        },
                        "candidateId": "2c9fd42d7c0a9815017c0e77a1960008"
                    },
                    {
                        "candidateLanguage": "86932",
                        "level": "BASICO",
                        "language": {
                            "languageId": "402880de79733a9001797340607d000d",
                            "spanishName": "Coreano",
                            "englishName": "Korean"
                        },
                        "candidateId": "2c9fd42d7c0a9815017c0e77a1960008"
                    }
                ],
                "courses": [],
                "certifications": [],
                "hardSkills": [
                    {
                        "skillCandidateId": "8687ajdd21",
                        "level": "INTERMEDIO",
                        "skill": {
                            "skillId": "ff8081817c04fda3017c04fe3ffd001e1",
                            "name": "React"
                        },
                        "candidateId": "2c9fd42d7c0a9815017c0e77a1960008"
                    },
                    {
                        "skillCandidateId": "8687ajdd41",
                        "level": "EXPERTO",
                        "skill": {
                            "skillId": "ff8081817c04fda3017c04fe3ffd001e",
                            "name": "Conocimientos en software administrativo"
                        },
                        "candidateId": "2c9fd42d7c0a9815017c0e77a1960008"
                    },
                    {
                        "skillCandidateId": "8687ajdd61",
                        "level": "INTERMEDIO",
                        "skill": {
                            "skillId": "ff8081817c04fda3017c04fe3ffd001",
                            "name": "Programación orientada a objetos"
                        },
                        "candidateId": "2c9fd42d7c0a9815017c0e77a1960008"
                    }
                ],
                "softSkills": [],
                "areas": [],
                "specialties": [],
                "workExperiencie": [],
                "positionRequired": null,
                "idealJob": null,
                "countryPosition": null,
                "statePosition": "México",
                "townshipPosition": null,
                "minSalary": null,
                "maxSalary": null,
                "workingDay": null,
                "modality": null,
                "contract": null,
                "typeCompany": null,
                "timeVacant": null,
                "firstSearch": null,
                "notificationsPush": null
            }
        ],
        "pageable": {
            "sort": {
                "sorted": false,
                "unsorted": true,
                "empty": true
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 8,
            "paged": true,
            "unpaged": false
        },
        "totalPages": 1,
        "totalElements": 8,
        "last": true,
        "size": 8,
        "number": 0,
        "sort": {
            "sorted": false,
            "unsorted": true,
            "empty": true
        },
        "numberOfElements": 8,
        "first": true,
        "empty": false
    },
    "filters": {
        "sueldoMinimo": 2000.00,
        "sueldoMaximo": 28000.00,
        "edadMinima": 20,
        "edadMaxima": 53,
        "idiomas": [
            {
                "languageId": "402880de79733a900179734054b70003",
                "spanishName": "Japonés",
                "englishName": "Japanese"
            },
            {
                "languageId": "402880de79733a9001797340583f0006",
                "spanishName": "Chino",
                "englishName": "Chinese"
            },
            {
                "languageId": "402880de79733a90017973405bd70009",
                "spanishName": "Portugués",
                "englishName": "Portuguese"
            },
            {
                "languageId": "402880de79733a90017973405f53000c",
                "spanishName": "Neerlandés",
                "englishName": "Dutch"
            },
            {
                "languageId": "402880de79733a9001797340525a0001",
                "spanishName": "Francés",
                "englishName": "French"
            },
            {
                "languageId": "402880de79733a9001797340607d000d",
                "spanishName": "Coreano",
                "englishName": "Korean"
            },
            {
                "languageId": "402880de79733a900179734053890002",
                "spanishName": "Alemán",
                "englishName": "German"
            },
            {
                "languageId": "402880de79733a900179734057120005",
                "spanishName": "Ruso",
                "englishName": "Russian"
            },
            {
                "languageId": "402880de79733a90017973405e2d000b",
                "spanishName": "Indonesio",
                "englishName": "Indonesian"
            },
            {
                "languageId": "402880de79733a9001797340596b0007",
                "spanishName": "Árabe",
                "englishName": "Arabic"
            },
            {
                "languageId": "402880de79733a90017973405d00000a",
                "spanishName": "Turco",
                "englishName": "Turkish"
            }
        ],
        "idiomasNiveles": null,
        "fechaActualizacion": null
    }
};