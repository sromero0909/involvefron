export const states = [
    {
        "id": 3447,
        "name": "Chihuahua",
        "countryCode": "MX",
        "fipsCode": "06",
        "iso2": "CHH",
        "latitude": 28.6329957,
        "longitude": -106.0691004
    },
    {
        "id": 3448,
        "name": "Oaxaca",
        "countryCode": "MX",
        "fipsCode": "20",
        "iso2": "OAX",
        "latitude": 17.0731842,
        "longitude": -96.7265889
    },
    {
        "id": 3449,
        "name": "Sinaloa",
        "countryCode": "MX",
        "fipsCode": "25",
        "iso2": "SIN",
        "latitude": 25.1721091,
        "longitude": -107.4795173
    },
    {
        "id": 3450,
        "name": "México",
        "countryCode": "MX",
        "fipsCode": "15",
        "iso2": "MEX",
        "latitude": 23.634501,
        "longitude": -102.552784
    },
    {
        "id": 3451,
        "name": "Chiapas",
        "countryCode": "MX",
        "fipsCode": "05",
        "iso2": "CHP",
        "latitude": 16.7569318,
        "longitude": -93.1292353
    },
    {
        "id": 3452,
        "name": "Nuevo León",
        "countryCode": "MX",
        "fipsCode": "19",
        "iso2": "NLE",
        "latitude": 25.592172,
        "longitude": -99.9961947
    },
    {
        "id": 3453,
        "name": "Durango",
        "countryCode": "MX",
        "fipsCode": "10",
        "iso2": "DUR",
        "latitude": 37.27528,
        "longitude": -107.8800667
    },
    {
        "id": 3454,
        "name": "Tabasco",
        "countryCode": "MX",
        "fipsCode": "27",
        "iso2": "TAB",
        "latitude": 17.8409173,
        "longitude": -92.6189273
    },
    {
        "id": 3455,
        "name": "Querétaro",
        "countryCode": "MX",
        "fipsCode": "22",
        "iso2": "QUE",
        "latitude": 20.5887932,
        "longitude": -100.3898881
    },
    {
        "id": 3456,
        "name": "Aguascalientes",
        "countryCode": "MX",
        "fipsCode": "01",
        "iso2": "AGU",
        "latitude": 21.8852562,
        "longitude": -102.2915677
    },
    {
        "id": 3457,
        "name": "Baja California",
        "countryCode": "MX",
        "fipsCode": "02",
        "iso2": "BCN",
        "latitude": 30.8406338,
        "longitude": -115.2837585
    },
    {
        "id": 3458,
        "name": "Tlaxcala",
        "countryCode": "MX",
        "fipsCode": "29",
        "iso2": "TLA",
        "latitude": 19.318154,
        "longitude": -98.2374954
    },
    {
        "id": 3459,
        "name": "Guerrero",
        "countryCode": "MX",
        "fipsCode": "12",
        "iso2": "GRO",
        "latitude": 17.4391926,
        "longitude": -99.5450974
    },
    {
        "id": 3460,
        "name": "Baja California Sur",
        "countryCode": "MX",
        "fipsCode": "03",
        "iso2": "BCS",
        "latitude": 26.0444446,
        "longitude": -111.6660725
    },
    {
        "id": 3461,
        "name": "San Luis Potosí",
        "countryCode": "MX",
        "fipsCode": "24",
        "iso2": "SLP",
        "latitude": 22.1564699,
        "longitude": -100.9855409
    },
    {
        "id": 3462,
        "name": "Zacatecas",
        "countryCode": "MX",
        "fipsCode": "32",
        "iso2": "ZAC",
        "latitude": 22.7708555,
        "longitude": -102.5832426
    },
    {
        "id": 3463,
        "name": "Tamaulipas",
        "countryCode": "MX",
        "fipsCode": "28",
        "iso2": "TAM",
        "latitude": 24.26694,
        "longitude": -98.8362755
    },
    {
        "id": 3464,
        "name": "Veracruz",
        "countryCode": "MX",
        "fipsCode": "30",
        "iso2": "VER",
        "latitude": 19.173773,
        "longitude": -96.1342241
    },
    {
        "id": 3465,
        "name": "Morelos",
        "countryCode": "MX",
        "fipsCode": "17",
        "iso2": "MOR",
        "latitude": 18.6813049,
        "longitude": -99.1013498
    },
    {
        "id": 3466,
        "name": "Yucatán",
        "countryCode": "MX",
        "fipsCode": "31",
        "iso2": "YUC",
        "latitude": 20.7098786,
        "longitude": -89.0943377
    },
    {
        "id": 3467,
        "name": "Quintana Roo",
        "countryCode": "MX",
        "fipsCode": "23",
        "iso2": "ROO",
        "latitude": 19.1817393,
        "longitude": -88.4791376
    },
    {
        "id": 3468,
        "name": "Sonora",
        "countryCode": "MX",
        "fipsCode": "26",
        "iso2": "SON",
        "latitude": 37.9829496,
        "longitude": -120.3821724
    },
    {
        "id": 3469,
        "name": "Guanajuato",
        "countryCode": "MX",
        "fipsCode": "11",
        "iso2": "GUA",
        "latitude": 21.0190145,
        "longitude": -101.2573586
    },
    {
        "id": 3470,
        "name": "Hidalgo",
        "countryCode": "MX",
        "fipsCode": "13",
        "iso2": "HID",
        "latitude": 26.1003547,
        "longitude": -98.2630684
    },
    {
        "id": 3471,
        "name": "Coahuila",
        "countryCode": "MX",
        "fipsCode": "07",
        "iso2": "COA",
        "latitude": 27.058676,
        "longitude": -101.7068294
    },
    {
        "id": 3472,
        "name": "Colima",
        "countryCode": "MX",
        "fipsCode": "08",
        "iso2": "COL",
        "latitude": 19.2452342,
        "longitude": -103.7240868
    },
    {
        "id": 3473,
        "name": "Mexico City",
        "countryCode": "MX",
        "fipsCode": "09",
        "iso2": "CMX",
        "latitude": 19.4326077,
        "longitude": -99.133208
    },
    {
        "id": 3474,
        "name": "Michoacán",
        "countryCode": "MX",
        "fipsCode": "16",
        "iso2": "MIC",
        "latitude": 19.5665192,
        "longitude": -101.7068294
    },
    {
        "id": 3475,
        "name": "Campeche",
        "countryCode": "MX",
        "fipsCode": "04",
        "iso2": "CAM",
        "latitude": 19.8301251,
        "longitude": -90.5349087
    },
    {
        "id": 3476,
        "name": "Puebla",
        "countryCode": "MX",
        "fipsCode": "21",
        "iso2": "PUE",
        "latitude": 19.0414398,
        "longitude": -98.2062727
    },
    {
        "id": 3477,
        "name": "Nayarit",
        "countryCode": "MX",
        "fipsCode": "18",
        "iso2": "NAY",
        "latitude": 21.7513844,
        "longitude": -104.8454619
    },
    {
        "id": 4857,
        "name": "Jalisco",
        "countryCode": "MX",
        "fipsCode": "14",
        "iso2": "JAL",
        "latitude": 20.6595382,
        "longitude": -103.3494376
    }
];