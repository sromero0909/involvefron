import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CandidateFinderComponent } from './candidate-finder/candidate-finder.component';
import { FavoritesComponent } from './candidate-finder/favorites/favorites.component';
import { CandidatesComponent } from './candidates/candidates.component';

const routes: Routes = [
    {
        path: '',
        component: CandidateFinderComponent
    },
    {
        path: 'candidatos',
        component: CandidatesComponent
    },
    {
        path: 'candidatos/:favoriteListId/:canEdit',
        component: CandidatesComponent
    },
    {
        path: 'favoritos',
        component: FavoritesComponent
    }
];
@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})

export class TalentRoutingModule { }