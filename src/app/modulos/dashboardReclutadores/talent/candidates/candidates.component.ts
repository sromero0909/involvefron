import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, OperatorFunction } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, startWith, switchMap } from 'rxjs/operators';
import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';
import { TalentNetoworkService } from 'src/app/services/reclutador/talent-network/talent-network.service';
import { CandidatesService } from 'src/app/services/reclutador/vacantes/selectionProcess/candidatos.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MainNavModel } from '../models/main-nav.model';
import { ApiService } from 'src/app/services/api.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { ModalFavoriteComponent } from '../candidate-finder/favorites/modal-favorite/modal-favorite.component';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDeleteComponent } from '../candidate-finder/favorites/modal-favorite/confirm-delete/confirm-delete.component';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.scss'],
  providers: []
})
export class CandidatesComponent implements OnInit {
  myControl = new FormControl();
  options: any;
  filteredOptions: Observable<any[]>;
  selectedDate: string;
  searchForm: FormGroup;
  filterForm: FormGroup;
  candidates: any[] = [];
  candidatesFinder: any;
  pageNumber: any = 1;
  pagesTotal = 1;
  pageSize: any = 8;
  bodyResponse: any;
  informationCandidate = { phone: "", email: "", git: "", linkedIn: "", file: "", behance: "" };
  minDesiredSalary: number;
  maxDesiredSalary: number;
  salarioMin:number = 0;
  salarioMax:number = 0;
  mainNavData: MainNavModel;
  mainNavDataFavoriteGroup: MainNavModel;
  hideSearchBar: boolean;
  favoriteCandidates: boolean = false;

  displaysAdvancedFilters = false;
  studyLevel: any[] = [
    {
      name: 'Licenciatura',
      ammount: 320,
      isChecked: false
    },
    {
      name: 'Especialidad',
      ammount: 16,
      ischecked: false
    }
  ];
  studyInstitution: any[] = [
    {
      name: 'IPN',
      ammount: 320,
      isChecked: false
    },
    {
      name: 'UNAM',
      ammount: 16,
      ischecked: false
    }
  ];
  nationalities: any[] = [
    {
      name: 'México',
      ammount: 320,
      isChecked: false
    },
    {
      name: 'Perú',
      ammount: 16,
      ischecked: false
    }
  ];
  genders: any[] = [
    {
      name: 'Masculino',
      ammount: 320,
      isChecked: false
    },
    {
      name: 'Femenino',
      ammount: 16,
      ischecked: false
    }
  ];
  mobilityAvailability: any[] = [
    {
      name: 'Indistinto',
      ammount: 320,
      isChecked: false
    },
    {
      name: 'Con disponibilidad',
      ammount: 16,
      ischecked: false
    },
    {
      name: 'Sin disponibilidad',
      ammount: 16,
      ischecked: false
    }
  ];

  availableBasicFilters: any = {
    languageLevels: [
      {
        name: 'Básico',
        value: 'BASICO',
      },
      {
        name: 'Intermedio',
        value: 'INTERMEDIO',
      },
      {
        name: 'Avanzado',
        value: 'AVANZADO',
      },
      {
        name: 'Nativo',
        value: 'NATIVO'
      }
    ],
    lastUpdate: [
      {
        name: 'Hoy',
        value: 'HOY',
      },
      {
        name: 'Hace 5 días',
        value: 'HACE_5_DIAS',
      },
      {
        name: 'Hace 15 días',
        value: 'HACE_15_DIAS',
      },
      {
        name: 'Hace 1 mes',
        value: 'HACE_1_MES',
      },
      {
        name: 'Hace 3 meses',
        value: 'HACE_3_MESES',
      },
      {
        name: 'Hace 6 meses',
        value: 'HACE_6_MESES',
      },
      {
        name: 'Todos',
        value: 'TODOS'
      }
    ],
    edadMaxima: '',
    edadMinima: '',
    sueldoMaximo: '',
    sueldoMinimo: '',
    languages: [],
  }

  @ViewChild('informacionContacto', { static: true }) informacionContacto!: TemplateRef<any>;
  @ViewChild('addToFavorites', { static: true }) addToFavorites!: TemplateRef<any>;
  @ViewChild('inviteToVacant', { static: true }) inviteToVacant!: TemplateRef<any>;
  @ViewChild('emptyFavoritesList', { static: true }) emptyFavoritesList!: TemplateRef<any>;
  @ViewChild('deleteCandidate', { static: true }) deleteCandidate!: TemplateRef<any>;

  multipleCandidatesSelected: any[] = [];
  favoritesList: any[] = [];
  foundVacants: any[] = [];
  isAnyCandidateSelected = false;
  formularioModalBuscar: FormGroup = new FormGroup({
    modelBuscarVacante: new FormControl('', [Validators.required, Validators.minLength(4)])
  });
  buscarVacante: any = [''];
  modelBuscarVacante: any = '';
  isCheckingFavoriteList = false;
  favoriteListId = '';
  favoriteListDetails: any;
  search: OperatorFunction<string, readonly { name: string }[]> = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term => term.length < 3 ? [] :
        this._candidates.searchVacancy(term).pipe(
          map((response: any) => {
            this.buscarVacante = [];
            if (response !== undefined && response !== null && response.content.length > 0) {
              const data = response.content;
              for (const i of data) {
                this.buscarVacante.push({
                  name: i.vacant.position.position + '-' + i.vacant.client.name,
                  idVacant: i.vacant.vacantId,
                });
              }
              this.modelBuscarVacante = this.buscarVacante;
            }
            if (this.buscarVacante === '') {
              this.buscarVacante = [];
              this.buscarVacante.push({ name: 'No se encontraron resultados' });
            }
            return this.buscarVacante;
          }),
        )
      )
    )
  formatter = (x: { name: string }) => x.name;

  constructor(
    public talentService: TalentNetoworkService,
    private fb: FormBuilder,
    private router: Router,
    private profileService: ProfileService,
    private _candidates: CandidatesService,
    private modalService: NgbModal,
    private apiService: ApiService,
    private readonly toastService: ToastService,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService) {
      this.searchForm = this.fb.group({
        finder: [this.candidatesFinder?.finder || '', [Validators.required]],
        location: [this.candidatesFinder?.location.name || '', [Validators.required]]
      });
      this.authService.consultState('MX').subscribe(
        (response: any[]) => {
          this.options = response.sort((a, b) => (a.name > b.name ? 1 : -1));
          this.filteredOptions = this.searchForm.controls.location.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
          );
          this.buildSearchForm();
          this.activatedRoute.params.subscribe((params: Params) => {
            if (params.favoriteListId) {
              this.isCheckingFavoriteList = true;
              this.favoriteListId = params.favoriteListId;
              this.searchForm.reset({
                finder: '',
                location: ''
              });
              this.searchFavoritesDetails();
            }else{
              this.favoriteCandidates = true;
              this.searchCandidates(true);
            }
          });
        });

    this.buildFilterForm(true);

    
  }

  buildSearchForm() {
    const candidatesFinderSession: any = sessionStorage.getItem('candidatesFinder');
    this.candidatesFinder = JSON.parse(candidatesFinderSession);
    this.candidatesFinder.location = this.options.filter((state: any) => {
      return state.id === this.candidatesFinder.location;
    })[0];
    this.searchForm.reset({
      finder: this.candidatesFinder?.finder || '',
      location: this.candidatesFinder?.location.name || ''
    });
    this.filteredOptions = this.searchForm.controls.location.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  buildFilterForm(isFirstLoad: boolean = true) {
    if (isFirstLoad) {
      this.filterForm = this.fb.group({
        minDesiredSalary: '',
        maxDesiredSalary: '',
        minAge: ['', Validators.pattern('[0-9]*')],
        maxAge: ['', Validators.pattern('[0-9]*')],
        updateDate: 'TODOS',
        languages: this.fb.array([]),
        languageLevel: ''
      });
      this.filterForm.get('updateDate')?.valueChanges.subscribe((change) => {
        this.searchCandidates(false);
      });
    }
    this.filterForm.get('minDesiredSalary')?.setValue(this.salarioMin);
    this.filterForm.get('maxDesiredSalary')?.setValue(this.salarioMax);
  }

  ngOnInit() {
    this.mainNavData = {
      title: 'Talent Network',
      search: false,
      image: 'assets/img/dashboard/TalentNetwork/Grupo 3038.svg',
      iconBack: false,
      link: true,
      linkTitle: 'Favoritos',
      linkUrl: '/dashboard/talent/favoritos'
    }
    
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter((option: any) => option.name.toLowerCase().includes(filterValue));
  }

  getFullName(candidate: any) {
    return candidate.user.name + ' ' + candidate.user.lastName + ' ' + candidate.user.secondLastName;
  }

  searchCandidates(isfirstLoad: boolean) {
    this.filterForm.markAllAsTouched();
    if (this.filterForm.valid) {
      this.candidatesFinder.finder = this.searchForm.value.finder;
      this.candidatesFinder.location = this.options.filter((el: any) => el.name === this.searchForm.value.location)[0]
      const params = {
        page: this.pageNumber - 1,
        query: this.candidatesFinder.finder,
        size: this.pageSize,
        stateId:  this.candidatesFinder.location.id
      };
       let body : any ;
        this.salarioMin=this.filterForm?.value.minDesiredSalary;
        this.salarioMax= this.filterForm?.value.maxDesiredSalary;
      if(this.filterForm?.value.minAge!="" && this.filterForm?.value.maxAge!=""){
         body = {
          sueldoMinimo: (this.filterForm?.value.minDesiredSalary) ? this.filterForm.value.minDesiredSalary : '',
          sueldoMaximo: (this.filterForm?.value.maxDesiredSalary) ? this.filterForm.value.maxDesiredSalary : '',
          edadMinima: (this.filterForm?.value.minAge) ? this.filterForm.value.minAge : '',
          edadMaxima: (this.filterForm?.value.maxAge) ? this.filterForm.value.maxAge : '',
          fechaActualizacion: this.filterForm.get('updateDate')?.value,
        };

      }else{
         body = {
          sueldoMinimo: (this.filterForm?.value.minDesiredSalary) ? this.filterForm.value.minDesiredSalary : '',
          sueldoMaximo: (this.filterForm?.value.maxDesiredSalary) ? this.filterForm.value.maxDesiredSalary : '',
          fechaActualizacion: this.filterForm.get('updateDate')?.value,
        };
      }
      
      const selectedLanguages = this.availableBasicFilters.languages.filter((el: any) => el.isChecked );
      if (selectedLanguages.length > 0) {
        body.idiomasNiveles = selectedLanguages.map((el: any) => {
          return {
              englishName:el.englishName,
              languageId: el.languageId,
              spanishName:el.spanishName
          };
        });
      }
      this.talentService.getCandidates(params, body).subscribe((candidatesResponse: any) => {
        if (candidatesResponse.candidates) {
          this.candidates = candidatesResponse?.candidates.content.map((el: any) => {
            return {
              ...el,
              isChecked: false
            };
          });
          this.availableBasicFilters.sueldoMaximo = candidatesResponse.filters?.sueldoMaximo || '';
          this.availableBasicFilters.sueldoMinimo = candidatesResponse.filters?.sueldoMinimo || '';
          this.pagesTotal = candidatesResponse.candidates.totalPages;
          this.pageNumber = candidatesResponse.candidates.number + 1;
          
          this.availableBasicFilters.languages = candidatesResponse.filters?.idiomas.map((language: any) => {
            return {
              ...language,
              isChecked: false,
              level: ''
            };
          }) || [];

          this.buildFilterForm(isfirstLoad);
        }
        else {
          console.error(candidatesResponse.error);

        }
      });
    }

  }

  searchFavoritesDetails() {
    const params = {
      pageNumber: this.pageNumber - 1,
      pageSize: this.pageSize,
    };
    this.talentService.getFavoriteListDetails(this.favoriteListId, params, null).subscribe((success: any) => {
      this.candidates = success.body.candidates?.content.map((el: any) => {
        return {
          ...el.candidate,
          isChecked: false,
          listFavoriteCandidateId: el.listFavoriteCandidateId,
        };
      }) || [];
      this.favoriteListDetails = success.body.listFavorite;
      console.log(this.favoriteListDetails);
      this.favoriteListDetails.permission = this.activatedRoute.snapshot.paramMap.get('canEdit') === 'true';
      this.availableBasicFilters.sueldoMaximo = success.body.filtros?.sueldoMaximo || '';
      this.availableBasicFilters.sueldoMinimo = success.body.filtros?.sueldoMinimo || '';
      this.pagesTotal = success.body.candidates?.totalPages || 0;
      this.pageNumber = success.body.candidates?.number + 1 || 0;
      // TODO: Falta todo lo de la respuesta normal
      this.mainNavDataFavoriteGroup = {
        title: `Favoritos: ${this.favoriteListDetails.name}`,
        search: false,
        image: 'assets/img/general/black-heart.svg',
        iconBack: true,
      }

    });

  }

  addPageNumber() {
    this.pageNumber = this.pageNumber + 1;
    if (this.favoriteListId) {
      this.searchFavoritesDetails();
    }
    else {
      this.searchCandidates(true);
    }
  }

  subPageNumber() {
    this.pageNumber = this.pageNumber - 1;
    if (this.pageNumber > 0) {
      if (this.favoriteListId) {
        this.searchFavoritesDetails();
      }
      else {
        this.searchCandidates(true);
      }
    }
    else {
      this.pageNumber = 1;
    }
  }

  redirectToProfile(id: any) {
    let contador = 0;
    const tempRows: any[] = [];
    this.candidates.forEach((candidate: any) => {
      tempRows.push({
        candidate: candidate,
        check: false,
        colorPorcentaje: "rojo",
        data: null,
        edad: candidate.age,
        favorito: true,
        flagMacth: false,
        id: "abs62e76b7s86",
        interviewPresentation: true,
        licenciatura: candidate.positionProfile?.position || '',
        nombre: candidate.user.name,
        photo: "../../../../assets/img/dashboard/vacantes/sinFotoPerfil.svg",
        porcentaje: "0%",
        presentationVideo: false,
        reject: false,
        statusTerna: "ACTIVO",
        sueldo: candidate.desiredSalary,
        ubicacion: "México",
        urls: {
          behance: "",
          email: candidate.user.email,
          file: "",
          git: "",
          linkedIn: "",
          phone: "",
        }
      });
    });

    for (let candidate of this.candidates) {
      contador++;
      if (candidate.candidateId == id) {
        let data = {
          id: candidate.candidateId,
          idShow: candidate.positionProfile?.position || '',
          name: candidate.user.name,
          client: candidate.client,
          pageActual: this.pageNumber,
          pageSize: this.pageSize,
          cardActual: contador,
          setProcessSelection: 'talent',
          status: '',
          totalElemt: 0,
          totalPages: 0,
          rowsMostrar: this.candidates.length,
          testsPsychometricComplete: false
        }
        this.profileService.setSelectedCard(data);
      }
    }

    this._candidates.setRowsTemporal(tempRows);
    this.router.navigateByUrl('dashboard/proceso/perfil/(profile:talent-CV)');
  }

  openModalInformacionContacto(data: any, OpenModal: boolean) {
    this.informationCandidate.phone = data.user.phone = (data.user.phone != null) ? data.user.phone : "";
    this.informationCandidate.email = data.user.email;
    this.informationCandidate.git = "";
    this.informationCandidate.linkedIn = "";
    this.informationCandidate.file = "";
    this.informationCandidate.behance = ""
    for (let user of data.urlFiles) {
      switch (user.type) {
        case "URL_GIT":
          this.informationCandidate.git = user.pathFile;
          break;
        case "URL_LINKED":
          this.informationCandidate.linkedIn = user.pathFile;
          break;
        case "URL_FOLDER":
          this.informationCandidate.file = user.pathFile;
          break;
      }
    }
    if (OpenModal) {
      this.modalService.open(this.informacionContacto, { size: "lg", centered: true });
    }
  }

  toggleAdvancedFilters(): void {
    this.displaysAdvancedFilters = !this.displaysAdvancedFilters;
  }

  toggleLanguageSelected(evemt:any,language: any): void {
    language.isChecked = !language.isChecked;
  }

  toggleCandidateSelected(candidate: any): void {
    candidate.isChecked = !candidate.isChecked;
    this.isAnyCandidateSelected = this.candidates.filter((el: any) => el.isChecked).length > 0;
  }


  changeCurrentPage(): void {
    const targetPage = Number.parseInt(this.pageNumber, 0);
    if (isNaN(targetPage) || targetPage < 1 || targetPage > this.pagesTotal) {
      this.pageNumber = 1;
    }
    else {
      if (this.favoriteListId) {
        this.searchFavoritesDetails();
      }
      else {
        this.searchCandidates(false);
      }
    }
  }


  openCreateDialog = () => {
    this.modalService.dismissAll();
    this.dialog.open(ModalFavoriteComponent, { data: { create: true } }).afterClosed().subscribe((returnData) => {
      this.apiService.post({
        name: returnData.name,
        image: {
          imageId: returnData.imageId,
          pathImageBackground: returnData.imageBackground,
          pathImageMiniature: returnData.imageBackground
        }
      }, '/list-favorite').subscribe((resp: any) => {
        if (resp.body.listFavoriteId) {
          const newFavoriteList = resp.body;
          console.log(newFavoriteList)
          this.multipleCandidatesSelected = this.candidates.filter((candidate: any) => candidate.isChecked);
          this.onAddUsersToFavoriteListOrVacantProcess(newFavoriteList, null, () => {
            this.toastService.show('Se creó el grupo', { classname: 'bg-success text-light ngb-toastsBottom', delay: 3000 });
            this.router.navigate(['/dashboard', 'talent', 'candidatos', newFavoriteList.listFavoriteId, 'true']);
          });
        }
        else {
          this.toastService.showMessageError('Ocurrió un error al crear la lista de favoritos');
        }
      });
    });
  }


  addUserToFavoritesToMultipleAction(modalType: string, user?: any): void {
    if (user !== undefined) {
      this.multipleCandidatesSelected = [user];
    } else {
      this.multipleCandidatesSelected = this.candidates.filter((candidate: any) => candidate.isChecked);
    }
    if (modalType === 'favorites') {
      this.apiService.get('/list-favorite/page').subscribe((resp: any) => {
        if (resp.content.length === 0) {
          this.modalService.open(this.emptyFavoritesList, { centered: true, size: 'md' });
          user.isChecked = true;
        }
        else {
          this.favoritesList = resp.content;
          this.modalService.open(this.addToFavorites, { size: 'md', centered: true });
        }
      }, (error) => {
        console.log(error);
      });
    } else if (modalType === 'delete') {
      this.deleteOfFavorites();
      // this.openDeleteCandidateModal();
    }
    else {
      this.openVacanciesListModal();
    }
  }

  onAddUsersToFavoriteListOrVacantProcess(favoriteList: any, vacantProcess: any, callback?: any): void {
    const users = this.multipleCandidatesSelected.map((el) => {
      return el.candidateId;
    });
    if (favoriteList !== null) {
      const data = {
        candidates: users,
        listFavoriteId: favoriteList.listFavoriteId
      };
      this.apiService.post(data, '/list-favorite/candidate').subscribe((success) => {
        if (callback !== undefined) {
          callback();
        }
        else {
          this.toastService.show('Tus favoritos se actualizaron',
            { classname: 'bg-success text-light ngb-toastsBottom ngb-toastsBottom', delay: 3000 });
          this.modalService.dismissAll();
          if (users.length > 0) {
            this.candidates = this.candidates.map((candidate: any) => {
              return {
                ...candidate,
                isChecked: false
              };
            });
            this.isAnyCandidateSelected = false;
          }
        }
      },
        (error) => {
          console.error(error);
        });
    }
    else if (vacantProcess !== null) {
      const currentVacant = this.formularioModalBuscar.get('modelBuscarVacante')?.value;
      this._candidates.sendInvitationByCandidatesId(currentVacant.idVacant, users).subscribe((success: any) => {
        this.toastService.show('Oferta enviada',
          { classname: 'bg-success text-light ngb-toastsBottom ngb-toastsBottom', delay: 3000 });
        this.formularioModalBuscar.get("modelBuscarVacante")?.setValue("");
        this.modalService.dismissAll();
        if (users.length > 0) {
          this.clearSelection();
          this.isAnyCandidateSelected = false;
        }
      },
        (error: any) => {
          console.error(error);
        });
    }
  }

  openVacanciesListModal(): void {
    this.modalService.open(this.inviteToVacant, { size: 'md', centered: true });
    this.modalService.hasOpenModals();
  }

  deleteOfFavorites() {
    this.modalService.open(this.deleteCandidate, { centered: true, size: 'md' });
  }

  removeCandidates(deletes: boolean) {
    if (deletes) {
      const users = this.multipleCandidatesSelected.map((el) => {
        return el.listFavoriteCandidateId;
      });
      this.talentService.removeCandidates(this.favoriteListId, users).subscribe((success) => {
        this.searchFavoritesDetails();
        this.modalService.dismissAll();
      },
        (error) => {
          console.error(error);
        });
    }
  }

  openEditDialog = (item: any) => {
    this.dialog.open(ModalFavoriteComponent, { data: { create: false, itemFavorite: item } }).afterClosed().subscribe((returnData) => {
      if (returnData.delete) {
        this.dialog.open(ConfirmDeleteComponent, { disableClose: true, data: { id: item?.listFavoriteId } }).afterClosed()
          .subscribe(() => {
            this.removeCandidates(false);

          });
      } else {
        const images: any[] = returnData.images;
        this.apiService.patch([
          { "op": "replace", "path": "/name", "value": returnData.value.name },
          { "op": "replace", "path": "/image", "value": { "imageId": returnData.value.imageId } }
        ], `/list-favorite/${item.listFavoriteId}`).subscribe((resp: any) => {
          this.favoriteListDetails.name = resp.body.name;
          this.favoriteListDetails.image = images.filter((image) => image.imageId === resp.body.image.imageId)[0];
          this.toastService.show('Se guardaron los cambios', { classname: 'bg-success text-light ngb-toastsBottom', delay: 3000 });
        });
      }
    });
  }

  get selectedcandidates(): number {
    return this.candidates.filter((el: any) => el.isChecked).length;
  }

  multipleSelectionAction(action: string): void {
    switch (action) {
      case 'vacant':
        this.addUserToFavoritesToMultipleAction('vacant');
        break;
      case 'favorites':
        this.addUserToFavoritesToMultipleAction('favorites');
        break;
      case 'newFavorites':
        this.openCreateDialog();
        break;
      case 'delete':
        this.addUserToFavoritesToMultipleAction('delete');
        break;
    }

  }

  clearSelection() {
    this.candidates = this.candidates.map((candidate: any) => {
      return {
        ...candidate,
        isChecked: false
      };
    });
  }

  closeModals() {
    this.modalService.dismissAll();
  }

  contactClicked(link: string, method: string = 'link'){
    window.open(`${method === 'email' ? 'mailto:' : ''}${link}`);
    // TOOD: Agregar ${method === 'phone' ? 'tel:' : ''} al método cuando la plataforma sea responsiva y se pueda abrir desde un celular
  }
}
