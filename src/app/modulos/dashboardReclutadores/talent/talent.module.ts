import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CandidateFinderComponent } from './candidate-finder/candidate-finder.component';
import { moduleSharedModule } from 'src/app/commons/commons.module';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InternalizacionModule } from 'src/app/commons/internalizacion/internalizacion.module';
// import { ModalNewClientComponent } from './modal-new-client/modal-new-client.component';
import { TalentRoutingModule } from './talent-routing.module';
import { MaterialModule } from '../../material/material.module';
import { CandidatesComponent } from './candidates/candidates.component';
import { FavoritesComponent } from './candidate-finder/favorites/favorites.component';
import { ModalFavoriteComponent } from './candidate-finder/favorites/modal-favorite/modal-favorite.component';


// import { ModalErrorComponent } from './modal-error/modal-error.component';

@NgModule({
  declarations: [
    CandidateFinderComponent,
    CandidatesComponent,
    FavoritesComponent,
    ModalFavoriteComponent,
    // ModalNewClientComponent,
    // ModalErrorComponent
  ],
  imports: [
    CommonModule,
    InternalizacionModule,
    TalentRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    moduleSharedModule,
    NgbModule,
    NgbTooltipModule,
    MaterialModule
  ]
})
export class TalentModule { }
