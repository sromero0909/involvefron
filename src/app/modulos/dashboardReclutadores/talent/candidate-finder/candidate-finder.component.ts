import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';

import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { TalentNetoworkService } from 'src/app/services/reclutador/talent-network/talent-network.service';
import { MainNavModel } from '../models/main-nav.model';
@Component({
  selector: 'app-candidate-finder',
  templateUrl: './candidate-finder.component.html',
  styleUrls: ['./candidate-finder.component.scss'],
  providers: []
})
export class CandidateFinderComponent implements OnInit {

  mainNavData: MainNavModel;

  myControl = new FormControl();
  filteredOptions: Observable<any[]>;
  form: FormGroup;
  options: any[];

  constructor(public router: Router, private fb: FormBuilder, public talentService: TalentNetoworkService,
    public _suscription: SuscriptionService, private authService: AuthService) {
    this.buildForm();
    this.authService.consultState('MX').subscribe(
      (response: any[]) => {
        this.options = response.sort((a, b) => (a.name > b.name ? 1 : -1));
        this.filteredOptions = this.form.controls.location.valueChanges.pipe(
          startWith(''),
          map(value => this._filter(value))
        );
      },
      (error: any) => {
        this.filteredOptions = this.form.controls.location.valueChanges.pipe(
          startWith(''),
          map(value => this._filter(value))
        );
    });
    // this.options= ['Ciudad de México', 'Ciudad Madero, Tamaulipas', 'Ciudad Valles, San Luis Potosí'];
  }

  buildForm() {
    this.form = this.fb.group({
      finder: ['', [Validators.required]],
      location: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.mainNavData = {
      title: 'Talent Network',
      search: false,
      image: 'assets/img/dashboard/TalentNetwork/Grupo 3038.svg',
      iconBack: false,
      link: true,
      linkTitle: 'Favoritos',
      linkUrl: '/dashboard/talent/favoritos'
    }

    this.filteredOptions = this.form.controls.location.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  redirectToCandidates() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      const state: any = this.options.filter(options => {
        return options.name === this.form.value.location;
      });
      sessionStorage.setItem('candidatesFinder', JSON.stringify({
        finder: this.form.value.finder,
        location: state[0].id
      }));
      this.router.navigateByUrl('/dashboard/talent/candidatos');
    }
  }

  private _filter(value: any): any[] {
    const filterValue = value.toLowerCase();

    return this.options?.filter(option => option.name.toLowerCase().includes(filterValue)) || '';
  }

  /**
   * Validates if a particular field has a specif error if provided, or any error if not
   * @param fieldName The name of the field to validate
   * @param formToValidate The reference to the form where fieldName belongs
   * @param validationName A particular error name to look for it.
   *
   * @returns `true` if formToValidate.get(fieldName) has the `validationName` error or any error, in case `validationName` is not provided.
   */
  hasFormFieldError(fieldName: string, formToValidate: FormGroup, validationName?: string): boolean {
    if (validationName !== undefined) {
      return (formToValidate.get(fieldName)?.touched && formToValidate.get(fieldName)?.hasError(validationName)) || false;
    }
    else {
      return (formToValidate.get(fieldName)?.touched && formToValidate.get(fieldName)?.errors !== null) || false;
    }
  }
}
