import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { ToastService } from 'src/app/services/commons/toast.setvice';
import { ApiService } from 'src/app/services/api.service';
import { ModalFavoriteComponent } from './modal-favorite/modal-favorite.component';
import { MainNavModel } from '../../models/main-nav.model';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';


@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss'],

})

export class FavoritesComponent {
  @Output() propagarFiltroReclutador = new EventEmitter<string>();
  mainNavData: MainNavModel;
  data: any;
  recruitersData: any;
  recruiterSelected = 'Reclutadores';
  optionsFilterOrder: any;
  sortDirection = 'DESC';
  sortBy = 'creationDate';
  pageNumber = 0;
  pageSize = 15;
  query: string;

  constructor(
    private dialog: MatDialog,
    private readonly apiService: ApiService,
    private readonly toastService: ToastService,
    public _suscription: SuscriptionService,
  ) { }

  ngOnInit() {
    this.getListFavorite();
    this.getRecruiters();
    this.mainNavData = {
      title: 'Favoritos',
      search: true,
      image: 'assets/img/general/black-heart.svg',
      iconBack: true,
    }
  }

  searchGroup = (query: string) => {
    this.data = [];
    this.apiService.get(`/list-favorite/page?query=${query}`).subscribe((resp: any) => {
      this.data = resp.content;

    }, (error) => {
      console.log(error);
    });
  }

  findGroupByRecruiter = () => {
    this.data = [];
    if (this.recruiterSelected === 'Reclutadores') {
      this.getListFavorite();
    } else {
      this.apiService.get(`/list-favorite/page?recruiterId=${this.recruiterSelected}`).subscribe((resp: any) => {
        this.data = resp.content;
      }, (error) => {
        console.log(error);
      });
    }
  }

  findGroupByFilters = (valor: any) => {
    this.data = [];
    this.filterOrderBy(valor);
    if (this.sortDirection) {
      this.apiService.get(`/list-favorite/page?pageSize=${this.pageSize}&sortBy=${this.sortBy}&sortDirection=${this.sortDirection}`).subscribe((resp: any) => {
        this.data = resp.content;
      }, (error) => {
        console.log(error);
      });
    };
  }


  filterOrderBy(valor: any) {
    switch (valor) {
      case 'RECIENTES':
        this.sortDirection = 'DESC'
        this.sortBy = 'creationDate'
        break;
      case 'MAS_ANTIGUOS':
        this.sortDirection = 'ASC'
        this.sortBy = 'creationDate'
        break;
      case 'A_Z':
        this.sortDirection = 'ASC'
        this.sortBy = 'name'
        break;
      case 'Z_A':
        this.sortDirection = 'DESC'
        this.sortBy = 'name'
        break;
    }
  }

  getRecruiters = () => {
    this.apiService.get('/recruiter').subscribe((resp: any) => {
      this.recruitersData = resp;
    }, (error) => {
      console.log(error);
    });
  }


  getListFavorite = () => {
    this.apiService.get(`/list-favorite/page?pageSize=${this.pageSize}&sortBy=${this.sortBy}&sortDirection=${this.sortDirection}`).subscribe((resp: any) => {
      this.data = resp.content;
      console.log(this.data)
    });
  }

  openCreateDialog = () => {
    this.dialog.open(ModalFavoriteComponent, { data: { create: true } }).afterClosed().subscribe((returnData) => {
      this.apiService.post({
        name: returnData.name,
        image: {
          imageId: returnData.imageId,
          pathImageBackground: returnData.imageBackground,
          pathImageMiniature: returnData.imageBackground
        }
      }, '/list-favorite').subscribe((resp) => {
        this.getListFavorite();
      });
    });
  }

}
