import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmCombineComponent } from './confirm-combine.component';

describe('ConfirmCombineComponent', () => {
  let component: ConfirmCombineComponent;
  let fixture: ComponentFixture<ConfirmCombineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConfirmCombineComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmCombineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
