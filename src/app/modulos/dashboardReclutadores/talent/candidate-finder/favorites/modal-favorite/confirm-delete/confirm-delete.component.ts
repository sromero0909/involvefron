import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';


@Component({
  selector: 'app-confirm-delete',
  templateUrl: './confirm-delete.component.html',
  styleUrls: ['./confirm-delete.component.scss']
})
export class ConfirmDeleteComponent implements OnInit {
  returnData: any;


  constructor(
    private readonly apiService: ApiService,
    public dialogRef: MatDialogRef<ConfirmDeleteComponent>,
    private readonly toastService: ToastService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
  }

  deleteGroup = () => {
    this.apiService.delete(`/list-favorite?listFavoriteId=${this.data.id}`).subscribe((resp) => {
      this.dialogRef.close({ deleted: true });
      this.toastService.show('El grupo fue eliminado', { classname: 'bg-success text-light ngb-toastsBottom', delay: 3000 });
    }, (error) => {
      console.log(error);
    }
    );
  }

  closeModal = () => this.dialogRef.close();

}
