import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ValidatorsService } from 'src/app/commons/validatorsService/validators.service';
import { ApiService } from 'src/app/services/api.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';

@Component({
  selector: 'app-modal-favorite',
  templateUrl: './modal-favorite.component.html',
  styleUrls: ['./modal-favorite.component.scss']
})

export class ModalFavoriteComponent implements OnInit {

  form: FormGroup;
  returnData: any;
  catalogImages: any;


  constructor(
    private dialog: MatDialog,
    private readonly fb: FormBuilder,
    private readonly toastService: ToastService,
    public dialogRef: MatDialogRef<ModalFavoriteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly apiService: ApiService,
    private readonly validatorsService: ValidatorsService
  ) { }

  ngOnInit() {
    this.initForm();
    this.getListImagesFavorite();
  }

  initForm = () => {
    this.form = this.fb.group(
      {
        name: new FormControl(this.data?.itemFavorite?.name, [Validators.required, this.validatorsService.noWhiteSpaces, Validators.pattern("^\\S{1}.{1,248}\\S{1}$")
        ]),
        imageBackground: new FormControl(this.data?.itemFavorite?.image?.pathImageBackground, Validators.required),
        imageId: new FormControl(this.data?.itemFavorite?.image?.imageId),
      })
  }


  sendIDimage = (id: string) => {
    this.form.controls.imageId.setValue(id);
  }

  get name() {
    return this.form.controls.name
  }

  get imageBackground() {
    return this.form.controls.imageBackground
  }


  getListImagesFavorite = () => {
    this.apiService.get('/catalog/image').subscribe((resp) => {
      this.catalogImages = resp;
    }, (error) => {
      console.log(error);
    });
  }

  createGroup = () => {
    const newGroup = {
      name: this.form.value.name,
      imageId: this.form.value.imageId || this.catalogImages[0].imageId
    };
    this.toastService.show('Se creó el grupo ', { classname: 'bg-success text-light ngb-toastsBottom', delay: 3000 });

    this.dialogRef.close(newGroup);
  }

  saveGroup = () => {
    this.dialogRef.close({
      value: this.form.value,
      images: this.catalogImages
    });
    this.toastService.show('Guardamos tus cambios', { classname: 'bg-success text-light ngb-toastsBottom', delay: 3000 });
  }

  deleteGroup = () => {
    this.dialogRef.close({ delete: true });
  };

  closeModal = () => this.dialogRef.close();
}
