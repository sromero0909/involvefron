import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';


@Component({
  selector: 'app-confirm-combine',
  templateUrl: './confirm-combine.component.html',
  styleUrls: ['./confirm-combine.component.scss']
})
export class ConfirmCombineComponent implements OnInit {
  returnData: any;


  constructor(
    private readonly apiService: ApiService,
    public dialogRef: MatDialogRef<ConfirmCombineComponent>,
    private readonly toastService: ToastService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
  }

  combineGroup = () => {
    this.apiService.put(`/list-favorite/combine?idListCombine=${this.data.id}&idListDestination=${this.data.id}`).subscribe(() => {
      this.dialogRef.close({ deleted: false });
      this.toastService.show('Guardamos tus cambios', { classname: 'bg-success text-light ngb-toastsBottom', delay: 3000 });
    }, (error) => {
      console.log(error);
    });
  }

  closeModal = () => this.dialogRef.close();

}
