import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoPermissionCombineComponent } from './no-permission-combine.component';

describe('NoPermissionCombineComponent', () => {
  let component: NoPermissionCombineComponent;
  let fixture: ComponentFixture<NoPermissionCombineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NoPermissionCombineComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoPermissionCombineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
