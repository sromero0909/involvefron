import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateFinderComponent } from './candidate-finder.component';

describe('CandidateFinderComponent', () => {
  let component: CandidateFinderComponent;
  let fixture: ComponentFixture<CandidateFinderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CandidateFinderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateFinderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
