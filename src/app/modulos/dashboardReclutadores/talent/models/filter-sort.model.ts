export interface FilterSortModel {
  sortDirection: string;
  sortBy: string;
}