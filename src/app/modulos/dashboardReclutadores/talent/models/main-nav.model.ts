export interface MainNavModel {
  iconBack?: boolean;
  title?: string;
  subtitle?: string;
  logo?: boolean;
  image?: string;
  imageClass?: string;
  search?: boolean;
  badgeAlert?: boolean;
  link?: boolean;
  linkTitle?: string;
  linkUrl?: string;
  extra?: boolean;
  filters?: boolean;
}