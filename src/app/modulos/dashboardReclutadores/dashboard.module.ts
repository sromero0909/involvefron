import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashBoardRoutingModule } from './dashboard-routing.module';
import { VacantesComponent } from './vacantes/vacantes.component';
import { SidenavVacantesComponent } from './sidenav-vacantes/sidenav-vacantes.component';
import { EnpausaComponent } from './vacantes/enpausa/enpausa.component';
import { BorradoresComponent } from './vacantes/borradores/borradores.component';
import { CerradasComponent } from './vacantes/cerradas/cerradas.component';
import { PlantillasComponent } from './vacantes/plantillas/plantillas.component';
import { CrearVacanteComponent } from './vacantes/crear-vacante/crear-vacante.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DescripcionComponent } from './vacantes/pasos-crearvacante/descripcion/descripcion.component';
import { CondicionesComponent } from './vacantes/pasos-crearvacante/condiciones/condiciones.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { ModalCerrarVacanteComponent } from './vacantes/modal-cerrar-vacante/modal-cerrar-vacante.component';
import { InternalizacionModule } from 'src/app/commons/internalizacion/internalizacion.module';
import { ModalGuardarComponent } from './vacantes/modal-guardar/modal-guardar.component';
import { RequisitosComponent } from './vacantes/pasos-crearvacante/requisitos/requisitos.component';
import { ModalPreguntasComponent } from './vacantes/modal-preguntas/modal-preguntas.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PruebasVideoComponent } from './vacantes/pasos-crearvacante/pruebas-video/pruebas-video.component';
import { ReclutamientoComponent } from './vacantes/pasos-crearvacante/reclutamiento/reclutamiento.component';
import { VistaPreviaComponent } from './vacantes/pasos-crearvacante/vista-previa/vista-previa.component';
import { PublicarVacanteComponent } from './vacantes/pasos-crearvacante/publicar-vacante/publicar-vacante.component';
import { ModalReclutadoresComponent } from './vacantes/modal-reclutadores/modal-reclutadores.component';
import { moduleSharedModule } from 'src/app/commons/commons.module';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { InputSearchComponent } from './components/input-search/input-search.component';
import { RequirementsComponent } from './vacantes/pasos-crearvacante/requirements/requirements.component';
import { QuestionsComponent } from './vacantes/pasos-crearvacante/components/questions/questions.component';
import { SoftSkillsComponent } from './vacantes/pasos-crearvacante/components/soft-skills/soft-skills.component';
import { Requeriments2Component } from './vacantes/pasos-crearvacante/requeriments2/requeriments2.component';
import { RequiredEducationComponent } from './vacantes/pasos-crearvacante/components/required-education/required-education.component';
import { StepBarComponent } from './vacantes/pasos-crearvacante/components/step-bar/step-bar.component';
import { PositionComponent } from './vacantes/pasos-crearvacante/position/position.component';
import { TextAreaWithBulletsDirective } from 'src/app/directives/text-area-with-bullets.directive';
import { ModalAreasComponent } from './vacantes/pasos-crearvacante/components/modal-areas/modal-areas.component';
import { ShowTooltipDirective } from 'src/app/directives/show-tooltip.directive';
import { FavoritesComponent } from './talent/candidate-finder/favorites/favorites.component';
import { ModalReactivationComponent } from './vacantes/modal-reactivation/modal-reactivation.component';
import { ModalExpirationComponent } from './vacantes/modal-expiration/modal-expiration.component';
import { FooterComponent } from './components/footer/footer.component';
import { ChartsModule } from 'ng2-charts';
import { MossComponent } from './pruebas-psicometricas/components/moss/moss.component';
import { LuscherComponent } from './pruebas-psicometricas/components/luscher/luscher.component';
import { IpvComponent } from './pruebas-psicometricas/components/ipv/ipv.component';
import { CleaverComponent } from './pruebas-psicometricas/components/cleaver/cleaver.component';
import { LoadingComponent } from 'src/app/commons/loading/loading.component';
import { EstadisticasWebComponent } from './estadisticas-web/estadisticas-web.component';
import { ResumenComponent } from './estadisticas-web/resumen/resumen.component';
import { ComparativaComponent } from './estadisticas-web/comparativa/comparativa.component';
import { GraficasComponent } from './estadisticas-web/graficas/graficas.component';
import { DetalleComponent } from './estadisticas-web/detalle/detalle.component';
@NgModule({
  declarations: [
    DashboardComponent,
    VacantesComponent,
    SidenavVacantesComponent,
    EnpausaComponent,
    BorradoresComponent,
    CerradasComponent,
    PlantillasComponent,
    CrearVacanteComponent,
    DescripcionComponent,
    CondicionesComponent,
    ModalCerrarVacanteComponent,
    ModalGuardarComponent,
    RequisitosComponent,
    ModalPreguntasComponent,
    PruebasVideoComponent,
    ReclutamientoComponent,
    VistaPreviaComponent,
    PublicarVacanteComponent,
    ModalReclutadoresComponent,
    InputSearchComponent,
    RequirementsComponent,
    QuestionsComponent,
    SoftSkillsComponent,
    Requeriments2Component,
    RequiredEducationComponent,
    StepBarComponent,
    PositionComponent,
    TextAreaWithBulletsDirective,
    ShowTooltipDirective,
    ModalAreasComponent,
    ModalReactivationComponent,
    ModalExpirationComponent,
    FooterComponent,
    MossComponent,
    LuscherComponent,
    CleaverComponent,
    IpvComponent,
    EstadisticasWebComponent,
    ResumenComponent,
    ComparativaComponent,
    GraficasComponent,
    DetalleComponent,
  ],
  imports: [
    CommonModule,
    DashBoardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbTooltipModule,
    AngularEditorModule,
    InternalizacionModule,
    NgbModule,
    NgxSpinnerModule,
    moduleSharedModule,
    NgxSliderModule,
    ChartsModule,
  ],
  exports: [
    SidenavVacantesComponent,
  ],
  providers: [CurrencyPipe]
})
export class DashboardModule { }
