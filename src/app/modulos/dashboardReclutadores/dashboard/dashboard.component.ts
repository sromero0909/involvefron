import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from 'src/app/services/commons/loader.service';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { SideNavService } from 'src/app/services/reclutador/vacantes/sideNav.service';
import { VacantesService } from 'src/app/services/reclutador/vacantes/vacantes.service';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  status = true;
  hideTextos = { vacantes: '', talent: '', estadisticas: '', clientes: '' };
  menuActivo = { vacantes: 'active', talent: '', estadisticas: '', clientes: '' };
  talent = '';
  elementoAvtivo = 'vacantes';
  showSideNav = 'oultaSineNav';

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private authService: AuthService,
              private _sidenav: SideNavService, 
              public _suscription: SuscriptionService, ) { }

  ngOnInit(): void {
    // if(!this.authService.getToken()){
    // this.router.navigate(['../login'], {
    // relativeTo: this.activatedRoute,
    // });
    // } else {
    this.authService.setVacantId('');
    this.getVacantAll();
    this.redirectToVacantes();
    //  }
    this._suscription.setExecutemodal(false);
  }

  // obtenemos el total de vancantes
  getVacantAll() {
    this._sidenav.totalVacancies().subscribe(
      (data) => {
        if (data != null) {
          const [activasTem, pausaTem, borradoresTem, cerradasTem, templatesTem] = data;
          const [activas] = activasTem;
          const [pausa] = pausaTem;
          const [borradores] = borradoresTem;
          const [, totalActivas] = activas;
          const [, totalPausa] = pausa;
          const [, totalBorradores] = borradores;
          this.authService.setVacantActive((totalActivas > 0) ? totalActivas : 0);
          this.authService.setVacantPause((totalPausa > 0) ? totalPausa : 0);
          this.authService.setVacantDrafts((totalBorradores > 0) ? totalBorradores : 0);
          this._sidenav.upDateCounters(data);
        }
      },
      (error) => {
        console.log(error.status);
      });
  }

  minimizaMaximizaSideNav() {
    this.status = !this.status;
    this.hideTextos.vacantes = (this.status == true) ? 'hideTextos' : 'showTextos';
    this.hideTextos.talent = (this.status == true) ? 'hideTextos' : 'showTextos';
    this.hideTextos.estadisticas = (this.status == true) ? 'hideTextos' : 'showTextos';
    this.hideTextos.clientes = (this.status == true) ? 'hideTextos' : 'showTextos';
    this.marcaMenuActivo(this.elementoAvtivo);
  }


  mouseOverLista(lista: string) {
    switch (lista) {
      case 'vacantes':
        if (this.status) {
          this.hideTextos.vacantes = '';
        }
        break;
      case 'talent':
        if (this.status) {
          this.hideTextos.talent = '';
        }
        break;
      case 'estadisticas':
        if (this.status) {
          this.hideTextos.estadisticas = '';
        }
        break;
      case 'clientes':
        if (this.status) {
          this.hideTextos.clientes = '';
        }
        break;
      default:


    }

  }

  mouseLeaveLista(lista: string) {
    const hide = 'hideTextos';
    switch (lista) {
      case 'vacantes':
        if (this.status && this.menuActivo.vacantes != 'active') {
          this.hideTextos.vacantes = hide;
        }
        break;
      case 'talent':
        if (this.status && this.menuActivo.talent != 'active') {
          this.hideTextos.talent = hide;
        }
        break;
      case 'estadisticas':
        if (this.status && this.menuActivo.estadisticas != 'active') {
          this.hideTextos.estadisticas = hide;
        }
        break;
      case 'clientes':
        if (this.status && this.menuActivo.clientes != 'active') {
          this.hideTextos.clientes = hide;
        }
        break;
      default:

    }
  }

  marcaMenuActivo(lista: string) {
    this.elementoAvtivo = lista;
    const hide = 'active';
    const show = 'showTextos';
    this.clearSelection();
    if (this.status) {
      this.hideAllTextos();
    }
    switch (lista) {
      case 'vacantes':
        this.menuActivo.vacantes = hide;
        this.hideTextos.vacantes = show;
        break;
      case 'talent':
        this.menuActivo.talent = hide;
        this.hideTextos.talent = show;
        break;
      case 'estadisticas':
        this.hideTextos.estadisticas = show;
        this.menuActivo.estadisticas = hide;
        break;
      case 'clientes':
        this.hideTextos.clientes = show;
        this.menuActivo.clientes = hide;
        break;
      default:
    }

  }

  clearSelection() {
    this.menuActivo.vacantes = '';
    this.menuActivo.talent = '';
    this.menuActivo.estadisticas = '';
    this.menuActivo.clientes = '';
  }

  hideAllTextos() {
    this.hideTextos.vacantes = 'hideTextos';
    this.hideTextos.talent = 'hideTextos';
    this.hideTextos.estadisticas = 'hideTextos';
    this.hideTextos.clientes = 'hideTextos';
  }

  mouseOverSideNav(): void {
    this.showSideNav = '';
  }

  mouseLeaveSideNav(): void {
    this.showSideNav = 'oultaSineNav';

  }

  redirectToVacantes() {
    this.router.navigate(['./vacantes/activas'], {
      relativeTo: this.activatedRoute,
    });
  }


}
