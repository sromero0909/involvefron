import { Component, OnInit,QueryList,ViewChild,ViewChildren,TemplateRef } from '@angular/core';
import { VacantesService } from 'src/app/services/reclutador/vacantes/vacantes.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {NgbdSortableHeader, SortEvent} from '../../../../directives/sortable.directive';
import { PaginadorService } from 'src/app/services/reclutador/vacantes/paginador.service';
import { Observable } from 'rxjs';
import { DecimalPipe } from '@angular/common';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { VacantesCerradasService } from '../../../../services/reclutador/vacantes/vacantes.cerradas.service'
import { VacantesActivasService } from 'src/app/services/reclutador/vacantes/vacantes.activas.service';
import { PlantillasService } from 'src/app/services/reclutador/vacantes/plantillas/plantillas.service';
import { CookiePasosVacanteService } from 'src/app/services/reclutador/commons/cookie-pasos-vacantes.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-plantillas',
  templateUrl: './plantillas.component.html',
  styleUrls: ['./plantillas.component.scss'],
  providers: [PaginadorService, DecimalPipe]
})
export class PlantillasComponent implements OnInit {
  page = 1
  rows1: Observable<any[]>;
  rows:Array<any> =[];
  arregloOrderBy: Array<any> = []
  page_size: number = 10;
  page_number:number = 0;
  rowsMostrar: number = 0;
  pageActual: number = 0;
  pageTemporal: number = 0;
  restarRows:number = 0
  contadorChecks = 0;
  rowsPorPagina = 0
  totalElements: number = 0;
  totalPages: number = 0;

  checkAll:boolean = false;
  formularioPlantilla!:FormGroup;
  mensajeToast:string = "";
  idVacante: string = "";
  reasonId: string = "";
  filtroCliente: string = "";
  filtroReclutador: string = "";
  claseCheck:string=""
  textFilter: string = "";
  dataPlantilla: any;
  sortDirection: string = "DESC"
  orderBy: string = "createDate"
  @ViewChild('modalPlantilla', { static: true }) cerrarVacante!:TemplateRef<any>;
  @ViewChildren(NgbdSortableHeader) headers!: QueryList<NgbdSortableHeader>;
  constructor(private _menuSeleccionado: VacantesService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public service: PaginadorService,
    public toastService: ToastService,
    private _vacantesService: VacantesCerradasService,
    private _pasosCrearVacante: CookiePasosVacanteService,
    private vacantesActivasService: VacantesActivasService,
    private plantillaService: PlantillasService ) {
      this.inizializarFormularioPlantilla();
      this.rows1 = service.countries$;
    }

  ngOnInit(): void {
    this._menuSeleccionado.setMenuSeleccionado("plantillas");
    this.inicializaRows();
    this.llenarCataloOrderBy();
  }


  inicializaRows(){
    
    //**Llamamos al servicio que trae las vacantes */
    //let params = `pageSize=${this.page_size}&pageNumber=${this.page_number}${this.orderBy}${this.sortDirection}&status=PLANILLA`
    // `/search-vacant/administration?pageSize=${this.page_size}&pageNumber=${this.page_number}${this.orderBy}${this.sortDirection}&status=CERRADA${this.filtroCliente}${this.filtroReclutador}`

    let params = {
      pageSize: this.page_size,
      pageNumber: this.page_number,
      sortDirection: this.sortDirection,
      orderBy: this.orderBy,
      status: "PLANTILLA",
      clientFilter: this.filtroCliente,
      recruiterFilter: this.filtroReclutador,
      textFilter: this.textFilter
    }

    //let params = `/search-vacant/administration?pageSize=${this.page_size}&pageNumber=${this.page_number}${this.orderBy}${this.sortDirection}&status=PLANTILLA${this.filtroCliente}${this.filtroReclutador}`;
   
    this.plantillaService.getListPlantillas(params).subscribe(
      (response: any) => {
        let data = (response.hasOwnProperty('content')) ? response.content : [];
        console.log(data);
        if (data.length > 0) {
          for (let i = 0; i < data.length; i++) {
            this.rows.push({
              check:false,
              id:data[i].vacant.vacantId,
              idshow:data[i].vacant.idShow,
              nameVacant: data[i].vacant.name,
              fechaCreacion: data[i].vacant.createDate ,
              boton: "Editar"
            });
          }
          this.totalElements = response.totalElements;
          this.totalPages = response.totalPages;
          if (this.pageActual == 0) {
            this.updateRowsMOstradas(this.pageActual + 1)
          } else {
            this.updateRowsMOstradas(this.pageActual)
          }
        }
      },
      (error) => {
        console.log("error en el servicio pausar vacante", error);
      }
    );
  }

  onchangeCheck(id:number){
    for(let i = 0; i < this.rows.length; i++){
      if(this.rows[i].id == id){
        (this.rows[i].check) ? this.contadorChecks-- : this.contadorChecks++
        this.rows[i].check = !this.rows[i].check
        
      }
    }
    if(this.contadorChecks > 0 && this.contadorChecks < this.totalElements) {this.claseCheck = "minus"; this.checkAll=true}else{this.claseCheck = ""; this.checkAll=false} 
  }

  selectAll(){
    this.checkAll  = !this.checkAll; 
    for(let i = 0; i < this.rows.length; i++){
      this.contadorChecks = (this.checkAll) ? this.rows.length :0
      this.rows[i].check = this.checkAll; 
    }
    if(this.contadorChecks > 0 && this.contadorChecks < this.totalElements) {this.claseCheck = "minus"; this.checkAll=true}else{this.claseCheck = "";} 

  }

  limpiarSeleccion(){
    for(let i = 0; i < this.rows.length; i++){
        this.contadorChecks = 0
        this.rows[i].check = false
    }
    this.checkAll = false
  }

  eliminarBorradores(){
    const vacantes:any = [];
    this.checkAll = false
    this.rows = this.rows.filter((i) =>{
      if(i.check){
        vacantes.push(i.id)
      }
      return !i.check
    });
    this._vacantesService.eliminarVacante(vacantes).subscribe(
      (response) => {
        if(response.status == 204){
          this.contadorChecks = 0;
          this.resetPaginadores();
          this.inicializaRows();
          this.showSuccess("Se eliminaron la(s) vacante(s)")
        }
      },
      (error)=>{
        this.showMessageError(error.error);
        console.log("error en eliminar vacante",error);
       }
     );
  }
  selectPage(page: string) {
    this.page = parseInt(page, 10) || 1;
  }

  formatInput(input: HTMLInputElement) {
    input.value = input.value.replace(FILTER_PAG_REGEX, '');
  }

  getInicialesNombre(nombre: string) {
    let iniciales = ""
    let separador = " ",
      arregloDeSubCadenas = nombre.split(separador);

    /*for (let i = 0; i < arregloDeSubCadenas.length; i++) {
      console.log(arregloDeSubCadenas[i] + "  ");
    }*/

    for (let j = 0; j < arregloDeSubCadenas.length; j++) {
      const subCadena = arregloDeSubCadenas[j].substring(0, 1)
      iniciales += subCadena;
    }
    return iniciales.slice(0, 2);
  }

  openModalCrearPlatilla(data: any) {
    this.dataPlantilla = data
    this.modalService.open(this.cerrarVacante, { size:'lg', centered: true });
  }
 
  getNombrePlantilla(): void{
    const pregunta = this.formularioPlantilla?.get("inputPregunta")?.value
    this.formularioPlantilla?.reset();
  }

  inizializarFormularioPlantilla(){
    this.formularioPlantilla = this.formBuilder.group({
      inputPregunta: [{ value: '', disabled: false }, [Validators.required, Validators.minLength(5)]],
    });
  }

  editarvacante(idVacante: any) {
    this.redirectTocCrear();
  }
  redirectTocCrear() {
    // poner en true los pasos que ya tengan informacion
    this._pasosCrearVacante.setStatusPasosCrearVacante({ paso1: true, paso2: true, paso3: true, paso4: true, paso5: false, paso6: false })
    this._pasosCrearVacante.setEditarPlantilla(true);
    this.router.navigate(['../crear'], {
      relativeTo: this.activatedRoute,
    });
    setTimeout(() => {
     // this.next('cuatro') //para direccionar a un paso especifico
    }, 100);
  }

  onSort({ column, direction }: any) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = 'sort';
      }
    });
    
     if(direction == 'sort'){
      this.sortDirection = "&sortDirection=DESC"
      this.orderBy = "&sortBy=publicationDate"
      
     }else{
      this.sortDirection = "&sortDirection="+direction.toUpperCase()
      this.orderBy = "&sortBy="+column
     }
     this.resetPaginadores();
     this.inicializaRows();
  }

  showSuccess(mensaje:any) {
    this.toastService.remove(1)
    this.toastService.show(mensaje, { classname: 'bg-primary text-light ngb-toastsBottom', delay: 5000});
  }
  setMensajeNotificacion(mensaje:string){
    this.mensajeToast = mensaje;
  }

  rowsMostradas(page: any) {
    this.page_number = page - 1
    if (this.pageTemporal < parseInt(page) - 1) {
      this.inicializaRows();
      this.pageTemporal = page - 1;
    }
    this.updateRowsMOstradas(page)
  }

  updateRowsMOstradas(page: any) {
    setTimeout(() => {
      let a = this.rows.slice((this.page - 1) * this.page_size, page * this.page_size)
      //console.log("page", page, "a", a);
      this.rowsPorPagina = a.length;
      if (this.pageActual <= page) {
        this.rowsMostrar = this.rowsMostrar + a.length;
        this.restarRows = a.length;
        this.pageActual = page;
      } else {
        this.rowsMostrar = this.rowsMostrar - this.restarRows;
        this.pageActual = page;
      }
    }, 100)
  }

  selecRow() {
    this.resetPaginadores();
    this.inicializaRows()
  }

  resetPaginadores() {
    this.rowsMostrar = 0;
    this.pageActual = 0;
    this.page_number = 0;
    this.restarRows = 0;
    this.pageTemporal = 0;
    this.rows = [];
    this.page = 1
  }

  generateArrayReclutadores(datos: any) {
    let reclutadores = [];
    for (let i = 0; i < datos.length; i++) {
      const nombre = datos[i].recruiter.user.name + " " + datos[i].recruiter.user.lastName + " " + datos[i].recruiter.user.secondLastName;
      reclutadores.push({
        url: (datos[i].recruiter.user.photo != null) ? datos[i].recruiter.user.photo : "",
        nombre: nombre,
        iniciales: this.getInicialesNombre(nombre),
        tipo: datos[i].type
      });
    }
    return reclutadores;
  }

  crearPlantilla(template: any) {
    const pregunta = this.formularioPlantilla?.get("inputPregunta")?.value
    this.formularioPlantilla?.reset();
    this.dataPlantilla.name = pregunta;
    this.vacantesActivasService.crearPlantilla(this.dataPlantilla).subscribe(
      (response) => {
        this.showSuccess(template);
      },
      (error) => {
        console.log("error en el servicio pausar vacante", error);
        this.showMessageError(error.error.error);
      }
    );
  }

  aplicarFiltroClientes(data: any) {
    this.filtroCliente = (data != "") ? '&clientId=' + data : '';
    console.log("es el filtro cliente", data);
    this.resetPaginadores();
    this.inicializaRows();
  }

  aplicarFiltroReclutador(data: any) {
    this.filtroReclutador = (data != "") ? '&recruiterId=' + data : '';
    //console.log("es el filtro cliente", data);
    this.resetPaginadores();
    this.inicializaRows();
  }

  llenarCataloOrderBy() {
    this.vacantesActivasService.catalogoOrderBy().subscribe(
      (response) => {
        //console.log("es el catalogo order", response);
        if (response.length > 0) {
          for (let i = 0; i < response.length; i++) {
            const arrTemp = response[i][0];
            //console.log("arreglo tempo", arrTemp);
            this.arregloOrderBy.push({
              nombre: arrTemp[arrTemp.length - 1],
              value: arrTemp[0]
            })
          }
        }
      },
      (error) => {
        this.showMessageError(error)
      }
    );
  }

  filterOrderBy(valor: any) {
    console.log("es el valor de filtro", valor);
    switch (valor) {
      case 'RECIENTES':
        this.sortDirection = "DESC"
        this.orderBy = "createDate"
        break;
      case 'MAS_ANTIGUAS':
        this.sortDirection = "ASC"
        this.orderBy = "createDate"
        break;
      case 'A_Z':
        this.sortDirection = "ASC"
        this.orderBy = "name"
        break;
      case 'Z_A':
        this.sortDirection = "DESC"
        this.orderBy = "name"
        break;
    }
    this.resetPaginadores();
    this.inicializaRows();
  }

  showMessageError(dangerTpl: any) {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-light ngb-toastsBottom', delay: 3000 });
  }

  searchVacant(text:string){
    if(text.length >2){
      this.textFilter = "&textFilter="+ text
    }else{
      this.textFilter = ""
    }
    this.resetPaginadores();
    this.inicializaRows();
  }
}

const FILTER_PAG_REGEX = /[^0-9]/g;