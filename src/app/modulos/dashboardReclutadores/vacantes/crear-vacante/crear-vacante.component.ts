import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { Router } from '@angular/router';
import { CookiePasosVacanteService } from 'src/app/services/reclutador/commons/cookie-pasos-vacantes.service';
import { MainNavModel } from '../../talent/models/main-nav.model';

@Component({
  selector: 'app-crear-vacante',
  templateUrl: './crear-vacante.component.html',
  styleUrls: ['./crear-vacante.component.scss']
})
export class CrearVacanteComponent implements OnInit {

  mainNavData: MainNavModel;

  closeResult: string;

  pasosStatusCrearVacante: any;
  constructor(
    private _pasosCrearVacante: CookiePasosVacanteService,
    private modalService: NgbModal,
    private readonly router: Router
  ) {
    this.pasosStatusCrearVacante = this._pasosCrearVacante.getStatusPasosCrearVacante();
  }

  ngOnInit(): void {
    this.pasosStatusCrearVacante = this._pasosCrearVacante.getStatusPasosCrearVacante();
    this.mainNavConfig();
  }

  mainNavConfig = () => {
    this.mainNavData = {
      title: 'Crear Vacante',
      search: false,
      filters: false,
      image: 'backVacancy',
      imageClass: 'cursor-pointer'
    }
  }

  openModal(content: any) {
    this.modalService.open(content, { centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  goBack = () => {
    this.modalService.dismissAll();
    this.router.navigateByUrl('/dashboard/vacantes/activas');
  }
  ngAfterViewInit() {
  }

  onSubmit() {
  }
  redirecto() {
  }

}
