import { Component, OnInit } from '@angular/core';
import { StepperService } from '../../../../../services/reclutador/commons/stepper.service'
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Observable, OperatorFunction } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, tap, switchMap, filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from '../../../../../services/auth.service'
import { CookiePasosVacanteService } from 'src/app/services/reclutador/commons/cookie-pasos-vacantes.service';
import { enableDebugTools } from '@angular/platform-browser';
import {Location} from '@angular/common';
import { RutasCrearVacanteService } from 'src/app/services/reclutador/commons/rutasCrearVacante.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { stringify } from '@angular/compiler/src/util';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { CatalogService } from '../../../../../services/catalog/catalog.service';

@Component({
  selector: 'app-requisitos',
  templateUrl: './requisitos.component.html',
  styleUrls: ['./requisitos.component.scss']
})
export class RequisitosComponent implements OnInit {
  nivelesEstudio: any;
  educationId: string = "";

  estatusAcademico: any;
  idStatus: string = "";

  areas: any;
  idArea: string = "";

  especialidades: any;
  idEspecialidad: string = "";

  idiomas: any;
  idIdioma: number = 0;


  formularioRequisitos: any;
  nivelEstudiosSeleccionado: FormControl = new FormControl(null);
  arrarNivelEstudios: any[] = [];
  arrayEstatusAcademico: any[] = [];
  arrayPais: any[] = [];
  arrayAreas: any[] = []
  arrayEspecialidades: any[] = []
  arrayNivel: any[] = []
  arrayIdioma: any[] = []
  arrayNivelIdioma: any[] = []

  arregloUniversidades: any[] = []
  arregloAreas: any[] = []
  arregloEspecialidades: any[] = []
  arrregloPreguntasCerradas: any[] = []
  arrregloPreguntasCerradasHabTec: any[] = []
  arrregloPreguntasCerradasHabSoc: any[] = []
  arregloPreguntasAbiertas: any[] = []
  arregloPreguntasAbiertasHabTec: any[] = []
  arregloPreguntasAbiertasHabSoc: any[] = []
  arregloPreguntasExperiencia: any[] = []
  arregloPreguntasExperienciaHabTec: any[] = []
  arregloHabilidadesTecnicas: any[] = []
  arregloHabilidadesSociales: any[] = []
  arregloIdiomas: any[] = []
  bamderaLicenciatura: boolean = false;
  banderaBachillerato: boolean = false;
  banceraEstatusAcademico: boolean = false;
  banderaNombreInstitucion: boolean = false;

  banderaPais: boolean = false;
  banderaTitulo: boolean = false;
  valorNivelEstudios: string = ''
  valorEstatusAcademico: string = ''
  valorPais: string = ''
  valorPregntaCerrada = ''
  public modelLicenciatura: any;
  public modelNombreinstitucion: any;
  questionsGlobal: any[] = [];
  language: any[] = [];
  searching = false;

  mostrarSugerencias: boolean = false;

  termino : string = '';
  hayError: boolean = false;
  txtTerminoPuesto = '';
  isPositionNew = false;
  showCustomerSuggestions: boolean = false;
  terminoCliente: string = '';
  isErrorClients = false;
  puestosSugeridos: any[] = [];
  clientsSuggestion: {name: string, skillId: string}[] = [];
  selectHardSkill: {name: string, skillId: string} = {name:'',skillId:''};

  softSugeridos: any[] = [];
  softSuggestion: {name: string, skillId: string}[] = [];
  selectSoftSkill: {name: string, skillId: string} = {name:'',skillId:''};
  txtTerminoSoft = '';
  softSkills: [] = [];
  terminoSoft : string = '';
  mostrarSugerenciasSoft: boolean = false;


  //clientsSuggestion: any[] = [];
  txtTermino = '';

  hardSkills: [] = [];
  public model: any;

  arrayInstituciones:{name: string, idInstitution: string}[] = [
    {
      idInstitution: "402880de796fcc8f01796fe660e50005",
      name: "Instituto de Estudios Superiores de Chiapas S.C.",
    },
    {
      idInstitution: "402880de796fcc8f01796fe662070006",
      name: "Instituto de Estudios Superiores de Diseño (Arte A.C.)",
    },
    {
      idInstitution: "402880de796fcc8f01796fe663300007",
      name: "Instituto de Estudios Superiores de Tamaulipas A.C.",
    },
    {
      idInstitution: "402880de796fcc8f01796fe6645a0008",
      name: "Instituto de Estudios Superiores del Bajío",
    },
    {
      idInstitution: "402880de796fcc8f01796fe6657d0009",
      name: "Otro Instituto de Estudios Superiores",
    },
    {
      idInstitution: "402880de796fcc8f01796fe667c2000b",
      name: "Instituto Tecnológico Autonómo de México",
    },
    {
      idInstitution: "402880de796fcc8f01796fe668e3000c",
      name: "Instituto Tecnológico de Acapulco",
    },
    {
      idInstitution: "402880de796fcc8f01796fe66a02000d",
      name: "Instituto Tecnológico de Agua Prieta",
    },
    {
      idInstitution: "402880de796fcc8f01796fe66b2c000e",
      name: "Instituto Tecnológico de Aguascalientes",
    },
    {
      idInstitution: "402880de796fcc8f01796fe66c5b000f",
      name: "Instituto Tecnológico de Altiplano de Tlaxcala",
    }
  ]

  search: OperatorFunction<string, readonly {name: string, idInstitution: string}[]> = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(600),
      distinctUntilChanged(),
      map(term => (term.length < 2 && licenciaturas != undefined && licenciaturas != null && licenciaturas != []) ? []
        : licenciaturas.filter((v: any) => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  searchNombreInstitucion: OperatorFunction<string, readonly string[]> = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(600),
      distinctUntilChanged(),
      map(term => (term.length < 2 && instituciones != undefined && instituciones != null && instituciones != []) ? []
        : instituciones.filter((v: any) => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
        // statesWithFlags.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  constructor(private formBuilder: FormBuilder, private stepperService: StepperService,
    private authService: AuthService,
    private router: Router,
    private _pasosCrearVacante: CookiePasosVacanteService,
    private _location:   Location,
    private _rutas:RutasCrearVacanteService,
    public toastService: ToastService,
    private catalogService: CatalogService
    ) {
    this.formularioRequisitos = this.formBuilder.group({
      nivelEstudios: [{ value: '', disabled: false }, [Validators.required]],
      checkNivelEstudios: [{ value: false, disabled: false }, []],
      estatusAcademico: [{ value: '', disabled: false }, [Validators.required]],
      checkEstatusAcademico: [{ value: false, disabled: false }, []],
      modelLicenciatura: [{ value: '', disabled: false }, [Validators.required]],
      pais: [{ value: '', disabled: false }, [Validators.required]],
      areas: [{ value: '', disabled: false }, [Validators.required]],
      area:this.formBuilder.array([]),
      especialidad: [{ value: '', disabled: true }, [Validators.required]],
      especialidades:this.formBuilder.array([]),
      modelNombreinstitucion: [{ value: '', disabled: false }, [Validators.required]],
      habilidadesTecnicas: [{ value: '', disabled: false }],
      nivel: [{ value: '', disabled: false }, [Validators.required]],
      habilidadesSociales: [{ value: '', disabled: false }],
      habilidadNivel:this.formBuilder.array([]),
      idioma: [{ value: '', disabled: false }, [Validators.required]],
      nivelIdioma: [{ value: '', disabled: false }, [Validators.required]],
      cerradasExpLab:this.formBuilder.array([]),
      aniosExpLab:this.formBuilder.array([]),
      cerradasHabTec:this.formBuilder.array([]),
      aniosExperienciaSegunda:this.formBuilder.array([]),
      habilidadSocial: this.formBuilder.array([]),
      cerradasHabSociales:this.formBuilder.array([]),
      arrIdiomas:this.formBuilder.array([]),
    });
  }

  ngOnInit(): void {
    this.validarCookie();
    this.arrarNivelEstudios.push({ data: "Licenciatura" }, { data: "Bachilleraro" });
    this.arrayEstatusAcademico.push({ data: "Titulado" }, { data: "Pasante" });
    this.arrayPais.push({ data: "México" }, { data: "España" });
    this.arrayAreas.push({ data: "Sistemas y tecnologia" }, { data: "Administración" });
    this.arrayEspecialidades.push({ data: "Desarrollo móvil" }, { data: "Desarrollador Java" });
    this.arrayNivel.push({ data: "Básico" }, { data: "Intermedio" }, { data: "Avanzado" }, { data: "Experto" });
    this.arrayIdioma.push({ data: "Inglés" }, { data: "Español" }, { data: "Chino" });
    this.arrayNivelIdioma.push({ data: "Básico" }, { data: "Intermedio" }, { data: "Avanzado" }, { data: "Nativo" });
    this.onchangeNivelEstudios();
    this.onchangeEstatusAcademico();
    this.onchangePais();
    this.onchangeAreas();
    this.onchangeEspecialidades();
    this.onchangeNivelHalidadesTecnicas();
    this.onchangeNivelIdiomas();
    this.onchangeTituloCarrera();
    this.onchangeInstitucion();
    
  }

  ngAfterViewInit() {
    document.getElementById('inputOcultore')?.focus()
  }

  get area(): FormArray{
    return this.formularioRequisitos.get('area') as FormArray;
  }

  getNameArea(idArea:string){
   let nombreArea = "";
    for(let i = 0; i < this.areas.length; i++){
      if(idArea == this.areas[i].areaId){
        nombreArea = this.areas[i].spanishName
        break;
      }
    }
    return nombreArea
  }

  agregarArea(){
    const areaFormGroup = this.formBuilder.group({
      yearExperience: [{ value: 0, disabled: false }, []],
      exclud: [{ value: false, disabled: false }, []],
      areaId: [{ value: this.formularioRequisitos.get("areas")?.value, disabled: false }, []],
      nombreArea: [{ value: this.getNameArea(this.formularioRequisitos.get("areas")?.value), disabled: false }, []],
    });
    this.area.push(areaFormGroup)
  }



  eliminarArea(indice:number){
    this.area.removeAt(indice)
  }

  get especialidad(): FormArray{
    return this.formularioRequisitos.get('especialidades') as FormArray;
  }

  getNameEsp(idEsp:string){
    let nombreEsp = "";
    for(let i = 0; i < this.especialidades.length; i++){
      if(idEsp == this.especialidades[i].specialtyId){
        nombreEsp = this.especialidades[i].spanishName
        break;
      }
    }
    return nombreEsp
  }

  agregarEspecialidad(){
    const areaFormGroup = this.formBuilder.group({
      aniosEspecialidad: [{ value: 0, disabled: false }, []],
      excluyenteEspecialidad: [{ value: false, disabled: false }, []],
      idEspecialidad: [{ value: this.formularioRequisitos.get("especialidad")?.value, disabled: false }, []],
      nombreEspecialidad: [{ value: this.getNameEsp(this.formularioRequisitos.get("especialidad")?.value), disabled: false }, []],
    });
    this.especialidad.push(areaFormGroup)
  }

  eliminarEspacialidad(indice:number){
    this.especialidad.removeAt(indice)
  }

  searhNivelEstudios():void{
    this.authService.consultNivelEstudios().subscribe(
      (response:any) => {
        this.nivelesEstudio = response;
      },
      (error:any) => {
        console.log(error.status, error.message);
        this.showMessageError(error.message)
      });
  }

  removeNivelEstudios():void {
    this.bamderaLicenciatura = false;
    this.banderaBachillerato = false;
    this.formularioRequisitos.get("nivelEstudios")?.setValue("");
    this.formularioRequisitos.get('nivelEstudios').setValidators([Validators.required]);
    this.formularioRequisitos.get('checkNivelEstudios').updateValueAndValidity()

  }

  searhEstatusAcademicos():void{
    this.authService.consultEstatusAcademicos().subscribe(
      (response:any) => {
        this.estatusAcademico = response;
      },
      (error:any) => {
        console.log(error.status, error.message);
        this.showMessageError(error.error.message)
      });
  }

  searhAreas():void{
    this.authService.consultAreasEspecialidades().subscribe(
      (response:any) => {
        this.areas = response;
      },
      (error:any) => {
        console.log(error.status, error.message);
        this.showMessageError(error.message)
      });
  }

  searhEspecialidades():void{
    this.authService.consultEspecialidadesPorArea(this.formularioRequisitos.get("areas")?.value).subscribe(
      (response:any) => {
        this.especialidades = response;
      },
      (error:any) => {
        console.log(error.status, error.message);
        this.showMessageError(error.message)
      });
  }

  searhIdiomas():void{
    this.authService.consultIdiomas().subscribe(
      (response:any) => {
        this.idiomas = response;
      },
      (error:any) => {
        console.log(error.status, error.message);
        this.showMessageError(error.message)
      });
  }

  validaNivelDeEstdios(valor: string): void {
    valor = this.getNombreLicenciatura(valor)
    this.valorNivelEstudios = valor;
    switch (valor) {
      case "Licenciatura":
        this.bamderaLicenciatura = true;
        this.banderaBachillerato = false;
        break;
      default:
        if(valor != ''){
          this.bamderaLicenciatura = false;
          this.banderaBachillerato = true;
        }
        break;
    }
  }

  getNombreLicenciatura(valor:any){
    for(let i=0; i < this.nivelesEstudio.length; i++){
            if(valor == this.nivelesEstudio[i].educationId){
              return this.nivelesEstudio[i].name
            }
    }
  }
  anadirExperienciaLaboral() {
    const trabajo = this.formBuilder.group({
      checkNivelEstios: new FormControl(''),
    });
    this.nivelEstudios.push(trabajo);
  }

  get nivelEstudios(): FormArray {
    return this.formularioRequisitos.get('nivelEstudios') as FormArray;
  }

  next(pantalla: string): void {
    let show = {
      action: 'next',
      pantalla
    }
    this.redirectoStep4();
    this.stepperService.nextStepper(show);
  }

  previous(pantalla: string): void {
    let show = {
      action: 'previous',
      pantalla
    }
    this.redirectoStep2();
    this.stepperService.nextStepper(show);
  }

  redirecTo(pantalla: string): void {
    this._rutas.pantalla(pantalla)
  }

  submit() {
    if (this.formularioRequisitos.valid) {
    }
    else {
      alert("FILL ALL FIELDS" + this.formularioRequisitos.getError)
    }
  }

  onchangeNivelEstudios(): void {
    this.formularioRequisitos.get("nivelEstudios")?.valueChanges.subscribe((changes: any) => {
      if(changes != ''){
        this.validaNivelDeEstdios(changes);
      }
    });
  }

  onchangeEstatusAcademico(): void {
    this.formularioRequisitos.get("estatusAcademico")?.valueChanges.subscribe((changes: any) => {
      if(changes != ''){
        this.valorEstatusAcademico = this.getNombreEstatusAcademico(changes);
        this.banceraEstatusAcademico = true;
      }
    });
  }

  removeStatusAcademio():void{
    this.banceraEstatusAcademico=false;
    this.formularioRequisitos.get("estatusAcademico").setValue("")
    this.formularioRequisitos.get('estatusAcademico').setValidators([Validators.required]);
    this.formularioRequisitos.get('estatusAcademico').updateValueAndValidity()
  }

  getNombreEstatusAcademico(valor:any) {
    for(let i = 0; i < this.estatusAcademico.length; i++){
        if(valor == this.estatusAcademico[i].idStatus){
          return this.estatusAcademico[i].name
        }
    }
  }

  onchangePais(): void {
    this.formularioRequisitos.get("pais")?.valueChanges.subscribe((changes: any) => {
      this.valorPais = changes;
      this.banderaPais = true;
      this.arregloUniversidades = []
    });
  }

  onchangeAreas(): void {
    this.formularioRequisitos.get("areas")?.valueChanges.subscribe((changes: any) => {
      let exiteArea = false
      for(let i = 0; i < this.area.value.length; i++){
        if(this.area.value[i].nombreArea == this.formularioRequisitos.get("areas")?.value ){
          exiteArea = true;
        }
      }
      if (!exiteArea && changes != '') {
        this.searhEspecialidades();
        this.agregarArea();
        this.formularioRequisitos.get("area")?.disable();
        this.formularioRequisitos.get("especialidad")?.enable()
      }
    });
  }

  onchangeNivelHalidadesTecnicas(): void {
    this.formularioRequisitos.get("nivel")?.valueChanges.subscribe((changes: any) => {
      let existeHabilidad = false;
      //let habilidad = this.formularioRequisitos.get("habilidadesTecnicas")?.value
      let habilidad = this.txtTerminoPuesto;
      for (let i = 0; i < this.habilidadNivel.value.length; i++) {
        if (String(this.habilidadNivel.value[i].nombreHabiliad).includes(habilidad)) {
          existeHabilidad = true;
        }
      }
      if (!existeHabilidad && changes != null && changes != '') {
        this.agregarHabilidadNivel();
        this.formularioRequisitos.get("nivel")?.setValue("");
        this.formularioRequisitos.get('nivel').setValidators([]);
        this.formularioRequisitos.get('nivel').updateValueAndValidity();
        this.txtTerminoPuesto = '';
        this.formularioRequisitos.get("habilidadesTecnicas")?.setValue("");
        //this.formularioRequisitos.get('habilidadesTecnicas').setValidators([]);
        //this.formularioRequisitos.get('habilidadesTecnicas').updateValueAndValidity();
      }
      if(this.habilidadNivel.value.length == 0){
      }
    });
  }

  get habilidadNivel(): FormArray{
    return this.formularioRequisitos.get('habilidadNivel') as FormArray;
  }

  agregarHabilidadNivel(){
    const areaFormGroupNivel = this.formBuilder.group({
      aniosHabilidad: [{ value: 0, disabled: false }, []],
      excluyenteHabilidad: [{ value: false, disabled: false }, []],
      //nombreHabiliad: [{ value: this.formularioRequisitos.get("habilidadesTecnicas")?.value +"-"+this.formularioRequisitos.get("nivel")?.value, disabled: false }, []],
      nombreHabiliad: [{ value: this.selectHardSkill.name +"-"+this.formularioRequisitos.get("nivel")?.value, disabled: false }, []],
      idSkill: this.selectHardSkill.skillId
    });
    this.habilidadNivel.push(areaFormGroupNivel)
  }

  eliminarHabilidadNivel(indice:number){
    this.habilidadNivel.removeAt(indice)
    if(this.habilidadNivel.value.length == 0){
      this.formularioRequisitos.get("nivel")?.setValue("");
      this.formularioRequisitos.get('nivel').setValidators([Validators.required]);
      this.formularioRequisitos.get('nivel').updateValueAndValidity();
      this.formularioRequisitos.get("habilidadesTecnicas")?.setValue("");
      this.formularioRequisitos.get('habilidadesTecnicas').setValidators([Validators.required]);
      this.formularioRequisitos.get('habilidadesTecnicas').updateValueAndValidity();
    }
  }

  onchangeEspecialidades(): void {
    this.formularioRequisitos.get("especialidad")?.valueChanges.subscribe((changes: any) => {
      let exiteEspecialidad = false
      if (changes.length > 0) {
        for (let i = 0; i < this.especialidad.value.length; i++) {
          if (this.especialidad.value[i].nombreEspecialidad == changes) {
            exiteEspecialidad = true;
          }
        }
        if (!exiteEspecialidad && changes != '') {
          this.agregarEspecialidad();
        }
      }
      this.formularioRequisitos.get("area")?.enable()
    });
  }

  get nivelIdioma(): FormArray{
    return this.formularioRequisitos.get('arrIdiomas') as FormArray;
  }

  agregarNivelIdioma(idioma:string,nivel:string,bandera:boolean){
    let nombreIdioma = (bandera) ? this.getNombreIdioma(this.idiomas,this.formularioRequisitos.get("idioma")?.value) :"-"
    const areaFormGroup = this.formBuilder.group({
      aniosIdioma: [{ value: 0, disabled: false }, []],
      excluyenteIfioma: [{ value: false, disabled: false }, []],
      nombreIdioma: [{ value: (bandera) ? nombreIdioma.spanish_name : idioma, disabled: false }, []],
      nivelIdioma:[{value: nombreIdioma.spanish_name+"-"+nivel, disabled: false }, []],
      idIdioma:[{ value: this.formularioRequisitos.get("idioma")?.value, disabled: false }, []],
      idNivel:[{ value: this.formularioRequisitos.get("arrIdiomas")?.value, disabled: false }, []],
    });
    this.nivelIdioma.push(areaFormGroup)
  }

  transformLanguage = () => {
    let obj: any = {};
    for(let idioma of this.nivelIdioma.value) {
      obj = {
        languageId: idioma.idIdioma,
        level: this.removeAccent(String(idioma.nivelIdioma).split('-')[1]).toUpperCase(),
        exclud: idioma.excluyenteIfioma
      }
      this.language.push(obj);
    }
    return this.language;
  }

  onchangeNivelIdiomas(): void {
    this.formularioRequisitos.get("nivelIdioma")?.valueChanges.subscribe((changes: any) => {
      let exiteEspecialidad = false
      let idioma = this.formularioRequisitos.get("idioma")?.value
      for (let i = 0; i < this.nivelIdioma.value.length; i++) {
        if (this.nivelIdioma.value[i].idIdioma == idioma) {
          exiteEspecialidad = true;
        }
      }
      if (!exiteEspecialidad && changes != '') {
        this.agregarNivelIdioma(idioma,changes,true)
        this.formularioRequisitos.get("nivelIdioma")?.setValue("");
        this.formularioRequisitos.get('nivelIdioma').setValidators([]);
        this.formularioRequisitos.get('nivelIdioma').updateValueAndValidity()
        this.formularioRequisitos.get("idioma")?.setValue("");
        this.formularioRequisitos.get('idioma').setValidators([]);
        this.formularioRequisitos.get('idioma').updateValueAndValidity()
      }
    });
  }

  removeAccent = ( text: string ) => {
    return text
      .normalize('NFD')
      .replace(/([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+/gi,"$1")
      .normalize();
  }

  getNombreIdioma(arreglo:any,id:any){
    return arreglo.find(function(item:any){
        return item.languageId == id;
    });
}
  onchangeTituloCarrera() {
    this.formularioRequisitos.get("modelLicenciatura")?.valueChanges.subscribe((changes1: any) => {
      this.onPercentChange(changes1);
    });
  }
  onPercentChange(percent: any) {
    if (percent != '')
      this.modelLicenciatura = percent;

    if (this.modelLicenciatura != undefined && this.modelLicenciatura.length > 2 && this.valorNivelEstudios == "Licenciatura") {
      licenciaturas = ["Ingeniería química", "Ingeniería en sistemas"];
    }
    if (this.modelLicenciatura != undefined && this.modelLicenciatura.length > 5 && this.valorNivelEstudios == "Licenciatura") {
      this.banderaTitulo = true;
    }
  }

  onchangeInstitucion() {
    this.formularioRequisitos.get("modelNombreinstitucion")?.valueChanges.subscribe((changes1: any) => {
      this.onPercentChangeInstitucion(changes1);
    });
  }

  onPercentChangeInstitucion(percent: number) {

    this.modelNombreinstitucion = percent;
    if (this.modelNombreinstitucion != undefined && this.modelNombreinstitucion.length > 2 && this.valorNivelEstudios == "Licenciatura") {
      instituciones = ["Benemerita Universidad Autonoma de Puebla","Universidad Anáhuac Puebla","Universidad de las Américas Puebla","Universidad de las Ciencias de la Comunicación de Puebla S.C.",
        "Universidad Autónoma de México", "Instituto Politécnico Nacional","Instituto de Estudios Superiores de Chiapas S.C.","Instituto de Estudios Superiores de Diseño (Arte A.C.)",
      "Instituto de Estudios Superiores de Tamaulipas A.C.","Instituto de Estudios Superiores del Bajío","ESCOM","Centro de Estudios Superiores de Diseño de Monterrey S.C.", "Centro de Estudios Superiores de San Ángel S.C.",
      "Centro de Estudios Superiores del Noroeste", "Centro de Estudios Universitarios","Centro de Estudios Superiores de Diseño de Monterrey S.C.","Tecnológico de Monterrey"]
    }
  }

  get habilidadSocial(): FormArray{
    return this.formularioRequisitos.get('habilidadSocial') as FormArray;
  }

  agregarHabilidadSocial(dato:string){
    const areaFormGroup = this.formBuilder.group({
      aniosHabilidadSocila: [{ value: 0, disabled: false }, []],
      excluyenteHabilidadSocila: [{ value: false, disabled: false }, []],
      nombreHabilidadSocial: [{ value: dato, disabled: false }, []],
      skill: this.selectSoftSkill
    });
    this.habilidadSocial.push(areaFormGroup)
  }

  eliminarHabilidadSocial(indice:number){
    this.area.removeAt(indice)
  }

  addHabilidadSocial(event: KeyboardEvent): void {
    event.preventDefault();
    if (event.code == "Enter") {
      let exiteEspecialidad = false;
      const changes = this.txtTerminoSoft//this.formularioRequisitos.get("habilidadesSociales")?.value
      for (let i = 0; i < this.habilidadSocial.value.length; i++) {
        if (this.habilidadSocial.value[i].nombreHabilidadSocial == changes) {
          exiteEspecialidad = true;
        }
      }
      if (!exiteEspecialidad && changes!= null && changes!= '') {
        this.agregarHabilidadSocial(changes);
        this.formularioRequisitos.get("habilidadesSociales")?.setValue("");
        this.formularioRequisitos.get('habilidadesSociales').setValidators([]);
        this.formularioRequisitos.get('habilidadesSociales').updateValueAndValidity()
      }
    }
  }

  addCarrera(event: KeyboardEvent): void {
    if (event.code == "Enter") {
      this.banderaTitulo=true
      this.formularioRequisitos.get("modelLicenciatura")?.setValue("");
      this.formularioRequisitos.get('modelLicenciatura').setValidators([]);
      this.formularioRequisitos.get('modelLicenciatura').updateValueAndValidity()
    }
  }

  addIstitucion(event: KeyboardEvent): void {
    if (event.code == "Enter") {
      if (this.modelNombreinstitucion != undefined && this.modelNombreinstitucion.length > 3) {
        this.banderaNombreInstitucion = true;
        this.arregloUniversidades.push({ nombre: this.modelNombreinstitucion, pais: this.valorPais })
      }
      this.formularioRequisitos.get("modelNombreinstitucion")?.setValue("");
      this.formularioRequisitos.get('modelNombreinstitucion').setValidators([]);
      this.formularioRequisitos.get('modelNombreinstitucion').updateValueAndValidity()
    }
  }

  removeCarrera() {
    this.banderaTitulo = false;
    this.formularioRequisitos.get("modelLicenciatura")?.reset();
    this.formularioRequisitos.get('modelLicenciatura').setValidators([Validators.required]);
    this.formularioRequisitos.get('modelLicenciatura').updateValueAndValidity()
  }

  removeInstituciones(index: number) {
    if (this.arregloUniversidades.length > 0) {
      let arregloTem = []
      for (let i = 0; i < this.arregloUniversidades.length; i++) {
        if (i != index) {
          arregloTem.push(this.arregloUniversidades[i])
        }
      }
      if (this.arregloUniversidades.length == 1) {
        this.banderaNombreInstitucion = false
        this.formularioRequisitos.get("modelNombreinstitucion")?.reset();
        this.formularioRequisitos.get('modelNombreinstitucion').setValidators([Validators.required, Validators.minLength(2)]);
        this.formularioRequisitos.get('modelNombreinstitucion').updateValueAndValidity()
      }
      this.arregloUniversidades = arregloTem;
    } else {
      this.banderaNombreInstitucion = false
      this.formularioRequisitos.get("modelNombreinstitucion")?.reset();
      this.formularioRequisitos.get('modelNombreinstitucion').setValidators([Validators.required, Validators.minLength(2)]);
      this.formularioRequisitos.get('modelNombreinstitucion').updateValueAndValidity()
    }
  }

  removeEspecialidades(index: number) {
    this.especialidad.removeAt(index);
      if (this.especialidad.value.length == 0) {
        this.formularioRequisitos.get("especialidad")?.setValue("");
        this.formularioRequisitos.get("especialidad")?.enable();
      }
  }

  removeAreas(index: number) {
      this.area.removeAt(index);
      if (this.area.controls.length == 0) {
        this.arregloAreas = []
        this.formularioRequisitos.get("areas")?.setValue("");
        this.formularioRequisitos.get("especialidad")?.setValue("");
      }
  }

  aniosExperienciaAreas(id: any, accion: string) {
    if (parseInt(this.area.value[id].yearExperience) > 0 || accion == "mas") {
      let valor = (accion == "menos") ? parseInt(this.area.value[id].yearExperience) - 1 : parseInt(this.area.value[id].yearExperience) + 1
      this.area.controls[id].patchValue({ yearExperience: valor }, { onlySelf: false })
    }
  }

  aniosExperienciaExperienciaLaboral(id: any, accion: string) {
    for (let i = 0; i < this.arregloAreas.length; i++) {
      if (this.arregloAreas[i].yearExperience.id == id) {
        let aniosTem = this.arregloAreas[i].yearExperience.numAmios
        if (aniosTem > 0 || accion == "mas") {
          this.arregloAreas[i].yearExperience.numAmios = (accion == "menos") ? parseInt(this.arregloAreas[i].yearExperience.numAmios) - 1 : parseInt(this.arregloAreas[i].yearExperience.numAmios) + 1
        }
      }
    }
  }

  aniosExperienciaEspecialidades(id: any, accion: string) {
    for (let i = 0; i < this.especialidad.value.length; i++) {
      if (parseInt(this.especialidad.value[id].aniosEspecialidad) > 0 || accion == "mas") {
        let valor = (accion == "menos") ? parseInt(this.especialidad.value[id].aniosEspecialidad) - 1 : parseInt(this.especialidad.value[id].aniosEspecialidad) + 1
        this.especialidad.controls[id].patchValue({ aniosEspecialidad: valor }, { onlySelf: false })
      }
    }
  }

  aniosExperienciaExpLaboral(id: any, accion: string) {
    for (let i = 0; i < this.aniosExpLab.value.length; i++) {
      if (parseInt(this.aniosExpLab.value[id].aniosExExpLab) > 0 || accion == "mas") {
        let valor = (accion == "menos") ? parseInt(this.aniosExpLab.value[id].aniosExExpLab) - 1 : parseInt(this.aniosExpLab.value[id].aniosExExpLab) + 1
        this.aniosExpLab.controls[id].patchValue({ aniosExExpLab: valor }, { onlySelf: false })
      }
    }
  }

/*Preguntas experiencia Laboral*/
  get cerradasExpLab(): FormArray{
    return this.formularioRequisitos.get('cerradasExpLab') as FormArray;
  }

  eliminarcerradasExpLab(indice:number){
    this.cerradasExpLab.removeAt(indice)
  }

  get aniosExpLab(): FormArray{
    return this.formularioRequisitos.get('aniosExpLab') as FormArray;
  }

  eliminaraniosExpLab(indice:number){
    this.aniosExpLab.removeAt(indice)
  }

  eliminaAbiertasExpLap(indice:number){
    this.arregloPreguntasAbiertas.splice(indice,1);
  }

  /*Preguntas Habilidades Tecnicas*/
  get cerradasHabTec(): FormArray{
    return this.formularioRequisitos.get('cerradasHabTec') as FormArray;
  }

  eliminarcerradasHabTec(indice:number){
    this.cerradasHabTec.removeAt(indice)
  }

  eliminaAbiertasHabTec(indice:number){
    this.arregloPreguntasAbiertasHabTec.splice(indice,1);
  }

  get aniosHabTec(): FormArray{
    return this.formularioRequisitos.get('aniosExperienciaSegunda') as FormArray;
  }

  eliminaraniosHabTec(indice:number){
    this.aniosHabTec.removeAt(indice)
  }

  /*Preguntas Habilidades Sociales*/
  get cerradasHabSociales(): FormArray{
    return this.formularioRequisitos.get('cerradasHabSociales') as FormArray;
  }

  eliminarcerradasHabSociales(indice:number){
    this.cerradasHabSociales.removeAt(indice)
  }

  eliminaAbiertasSociales(indice:number){
    this.arregloPreguntasAbiertasHabSoc.splice(indice,1);
  }

  addPreguntaCerrada(pregunta: string, tipoPregunta: string): void {
    this.valorPregntaCerrada = pregunta;
    switch (tipoPregunta) {
      case "cerradas":
        const areaFormGroup = this.formBuilder.group({
          aniosCeExpLab: [{ value: 0, disabled: false }, []],
          excluyenteCeExpLab: [{ value: false, disabled: false }, []],
          preguntaCeExpLab: [{ value: pregunta, disabled: false }, []],
        });
        this.cerradasExpLab.push(areaFormGroup)
        break;
      case "abierta":
        this.arregloPreguntasAbiertas.push({ pregunta });
        break;
      case "experiencia":
        const aniosExpLabFormGroup = this.formBuilder.group({
          aniosExExpLab: [{ value: 0, disabled: false }, []],
          excluyenteExExpLab: [{ value: false, disabled: false }, []],
          preguntaExExpLab: [{ value: pregunta, disabled: false }, []],
        });
        this.aniosExpLab.push(aniosExpLabFormGroup)
        break;
      case "cerradasHabilidadesTecnicas":
        const areaCeFormGroupTec = this.formBuilder.group({
          aniosCeExpTec: [{ value: 0, disabled: false }, []],
          excluyenteCeExpTec: [{ value: false, disabled: false }, []],
          preguntaCeExpTec: [{ value: pregunta, disabled: false }, []],
        });
        this.cerradasHabTec.push(areaCeFormGroupTec)
        break;
      case "abiertaHabilidadesTecnicas":
        this.arregloPreguntasAbiertasHabTec.push({ pregunta });
        break;
      case "experienciaHabilidadesTecnicas":
        const aniosHabTecFormGroup = this.formBuilder.group({
          aniosexHabTec: [{ value: 0, disabled: false }, []],
          excluyenteHabTec: [{ value: false, disabled: false }, []],
          preguntaHabTec: [{ value: pregunta, disabled: false }, []],
        });
        this.aniosHabTec.push(aniosHabTecFormGroup)
        break;
      case "cerradasHabilidadesSociales":
        const cerradasHabSocial = this.formBuilder.group({
          aniosCeExpSoci: [{ value: 0, disabled: false }, []],
          excluyenteCeExpSoci: [{ value: false, disabled: false }, []],
          preguntaCeExpSoci: [{ value: pregunta, disabled: false }, []],
        });
        this.cerradasHabSociales.push(cerradasHabSocial)
        break;
      case "abiertaHabilidadesSociales":
        this.arregloPreguntasAbiertasHabSoc.push({ pregunta });
        break;
      default:
        break;
    }
  }
  ///para habilidades tecnicas

  aniosExperienciaHT(id: any, accion: string) {
    for (let i = 0; i < this.aniosHabTec.value.length; i++) {
      if (parseInt(this.aniosHabTec.value[id].aniosexHabTec) > 0 || accion == "mas") {
        let valor = (accion == "menos") ? parseInt(this.aniosHabTec.value[id].aniosexHabTec) - 1 : parseInt(this.aniosHabTec.value[id].aniosexHabTec) + 1
        this.aniosHabTec.controls[id].patchValue({ aniosexHabTec: valor }, { onlySelf: false })
      }
    }
  }

  removeHabilidadSocial(index: number) {
    if (this.habilidadSocial.value.length > 0) {
      this.habilidadSocial.removeAt(index)
      if (this.habilidadSocial.value.length == 0) {
        this.formularioRequisitos.get("habilidadesSociales")?.setValue("");
        this.formularioRequisitos.get('habilidadesSociales').setValidators([Validators.required]);
        this.formularioRequisitos.get('habilidadesSociales').updateValueAndValidity();
      }
    }
  }

  removeIiomas(index: number) {
    if (this.nivelIdioma.value.length > 0) {
      this.nivelIdioma.removeAt(index)
      if (this.nivelIdioma.value.length == 0) {
        this.formularioRequisitos.get("nivelIdioma")?.setValue("");
        this.formularioRequisitos.get('nivelIdioma').setValidators([Validators.required, Validators.minLength(2)]);
        this.formularioRequisitos.get('nivelIdioma').updateValueAndValidity()
        this.formularioRequisitos.get("idioma")?.setValue("")
        this.formularioRequisitos.get('idioma').setValidators([Validators.required, Validators.minLength(2)]);
        this.formularioRequisitos.get('idioma').updateValueAndValidity()
      }
    }
  }
  //fin habilidades sociales
  onClickSubmit() {
    let pasos: any = {};

    if(this.formularioRequisitos.valid)
      pasos = this._pasosCrearVacante.getStatusPasosCrearVacante();

    if (this.formularioRequisitos.invalid) {
      this.formularioRequisitos.get('nivelEstudios').markAsTouched();
      this.formularioRequisitos.get('estatusAcademico').markAsTouched();
      this.formularioRequisitos.get('modelLicenciatura').markAsTouched();
      this.formularioRequisitos.get('pais').markAsTouched();
      this.formularioRequisitos.get('area').markAsTouched();
      this.formularioRequisitos.get('especialidad').markAsTouched();
      this.formularioRequisitos.get('habilidadesTecnicas').markAsTouched();
      this.formularioRequisitos.get('habilidadesSociales').markAsTouched();
      this.formularioRequisitos.get('idioma').markAsTouched();
      this.formularioRequisitos.get('nivelIdioma').markAsTouched();
      pasos.paso3 = false;
      this._pasosCrearVacante.setStatusPasosCrearVacante(pasos)
    } else {
      this.redirecTo('cuatro');
      pasos.paso3 = true;

      if(this.formularioRequisitos.valid)
      this._pasosCrearVacante.setStatusPasosCrearVacante(pasos)
    }
    if(this.formularioRequisitos.valid)
      this.savePaso3();
  }

  savePaso3(): void {
    let paso3 = this._pasosCrearVacante.getPasosCrearVacante();
    this.transformarQuestionsExperienciaLaboral();
    this.transformarQuestionsSkills();
    this.transformarQuestionsSociales();
    paso3.paso3 = {
      nivelEstudios: {
        nivel: this.addNivelEstudios(),
        excluyente: this.formularioRequisitos.get('checkNivelEstudios').value,
      },
      estatusAcademico: {
        estatus: this.formularioRequisitos.get('estatusAcademico').value,
        excluyente: this.formularioRequisitos.get('checkEstatusAcademico').value
      },
      pais: {
        id: this.formularioRequisitos.get('pais').value
      },
      academicTitle: this.modelLicenciatura,
      institucion: this.arregloUniversidades,
      experienciaLaboral: {
        area: this.genericObjectArea(),
        especialidades: this.genericObjectSpeciality(),
      },
      habilidadesTecnicas: {
        habilidad: this.genericObjectHardSkill()
      },
      habilidadesSociales: {
        habilidad: this.genericObjectSoftSkill()
      },
      questions: this.questionsGlobal,
      idiomas: this.transformLanguage(),
      arreglos: {
        nivelesEstudio: this.nivelesEstudio,
        estastusAcademico: this.estatusAcademico
      }
    }
    this._pasosCrearVacante.setPasosCrearVacante(paso3);
    this.authService.creatVacantP3(paso3, String(this.authService.getVacantId())).subscribe(
      (response: any) => {
    },
    (error: any) => {
      console.log(error.status, error.message);
    }
    );
  }

  genericObjectArea = () => {
    let obj: any;
    let arreglo: any[] = [];
    let indice: number = 0;
    for (let area of this.area.value){
      obj = {
        area:{
          areaId: area.areaId
        },
        exclud: area.exclud,
        yearExperience: area.yearExperience
      }
      arreglo[indice] = obj;
      indice++;
    }
    let areas = arreglo;
    return areas;
  }

  genericObjectSpeciality = () => {
    let obj: any;
    let arreglo: any[] = [];
    let indice: number = 0;
    for (let spec of this.especialidad.value){
      obj = {
          exclud: spec.excluyenteEspecialidad,
          speciality: {
          specialtyId : spec.idEspecialidad
          },
          yearExperience: spec.aniosEspecialidad
      }
      arreglo[indice] = obj;
      indice++;
    }
    let specialitys = arreglo;
    return specialitys;
  }

  genericObjectHardSkill = () => {
    let obj: any;
    let arrayHardSkill: any[] = [];

    for(let hardSkill of this.habilidadNivel.value) {
      obj = {
        level: this.removeAccent(String(hardSkill.nombreHabiliad).split('-')[1]).toUpperCase(),
        //skillId: "2c9f9e967a52f7cc017a5327c5c9001e",
        skillId: hardSkill.idSkill,
        exclud : hardSkill.excluyenteHabilidad
      }
      arrayHardSkill.push(obj);
    }
    return arrayHardSkill;
  }

  genericObjectSoftSkill = () => {
    let obj: any;
    let arraySoftSkill: any[] = [];
    for(let softSkill of this.habilidadSocial.value) {
      obj = {
        level: 'BASICO',
        //skillId: '2c9f9e967a5889be017a589a5d480008'
        skillId: softSkill.skill.skillId
      }
      arraySoftSkill.push(obj);
    }
    return arraySoftSkill;
  }

  transformarQuestionsExperienciaLaboral = () => {
    let obj: any = {};
    for(let close of this.cerradasExpLab.value) {
      obj = {
        exclud: close.excluyenteCeExpLab,
        question: close.preguntaCeExpLab,
        type: "AREA_SPECIALTY",
        typeQuestion : "CERRADA",
        typeAnswer : true
      }
      this.questionsGlobal.push(obj);
    }
    for(let open of this.arregloPreguntasAbiertas) {
      obj = {
        exclud: false,
        question: open.pregunta,
        type: "AREA_SPECIALTY",
        typeQuestion : "ABIERTA"
      }
      this.questionsGlobal.push(obj);
    }
    for(let experience of this.aniosExpLab.value ) {
      obj = {
        exlud: false,
        question: experience.preguntaExExpLab,
        type: "AREA_SPECIALTY",
        typeQuestion : "EXPERIENCIA",
        yearsExperience : 2
      }
      this.questionsGlobal.push(obj);
    }
  }

  transformarQuestionsSkills = () => {
    let obj: any = {};

    for(let close of this.cerradasHabTec.value) {
      obj = {
        exclud: close.excluyenteCeExpTec,
        question: close.preguntaCeExpTec,
        type: "HARD_SKILL",
        typeQuestion : "CERRADA",
        typeAnswer : true
      }
      this.questionsGlobal.push(obj);
    }
    for(let open of this.arregloPreguntasAbiertasHabTec) {
      obj = {
        exclud: false,
        question: open.pregunta,
        type: "HARD_SKILL",
        typeQuestion : "ABIERTA"
      }
      this.questionsGlobal.push(obj);
    }
    for(let experience of this.aniosHabTec.value ) {
      obj = {
        exlud: experience.excluyenteHabTec,
        question: experience.preguntaHabTec,
        type: "HARD_SKILL",
        typeQuestion : "EXPERIENCIA",
        yearsExperience : experience.aniosexHabTec
      }
      this.questionsGlobal.push(obj);
    }
  }

  transformarQuestionsSociales = () => {
    let obj: any = {};
    for(let close of this.cerradasHabSociales.value) {
      obj = {
        exclud: close.excluyenteCeExpSoci,
        question: close.preguntaCeExpSoci,
        type: "SOFT_SKILL",
        typeQuestion : "CERRADA",
        typeAnswer : true
      }
      this.questionsGlobal.push(obj);
    }
    for(let open of this.arregloPreguntasAbiertasHabSoc) {
      obj = {
        exclud: false,
        question: open.pregunta,
        type: "SOFT_SKILL",
        typeQuestion : "ABIERTA"
      }
      this.questionsGlobal.push(obj);
    }
  }


  redirectoStep4() {
    this.router.navigateByUrl("dashboard/vacantes/crear/(vacante:descripcion//condiciones:laborales//requisitos:requisitos//pruebas:video)")
  }

  redirectoStep2() {
    this._location.back();
  }

  validarCookie(): void {
    let pasos = this._pasosCrearVacante.getPasosCrearVacante();

    if (pasos.paso3 != '') {
      this.nivelesEstudio = pasos.paso3.arreglos.nivelesEstudio;
      this.estatusAcademico = pasos.paso3.arreglos.estastusAcademico;
      this.areas = pasos.paso3.arreglos.areas;
      this.idiomas = this.idiomas;
      this.bamderaLicenciatura = true ;
      this.banceraEstatusAcademico = true;
      this.banderaTitulo = true;
      this.banderaNombreInstitucion = true
      this.formularioRequisitos.controls['nivelEstudios'].setValue(pasos.paso3.nivelEstudios.nivel.idNivelEstudio);
      this.valorNivelEstudios = pasos.paso3.nivelEstudios.nivel.nombre;
      this.formularioRequisitos.controls['checkNivelEstudios'].setValue(pasos.paso3.nivelEstudios.excluyente);
      this.formularioRequisitos.controls['estatusAcademico'].setValue(pasos.paso3.estatusAcademico.estatus.idEstatusAcademico);
      this.valorEstatusAcademico = pasos.paso3.estatusAcademico.estatus.nombre
      this.formularioRequisitos.controls['checkEstatusAcademico'].setValue(pasos.paso3.estatusAcademico.excluyente);
      this.formularioRequisitos.controls['modelLicenciatura'].setValue(pasos.paso3.tituloOcarrera);
      this.modelLicenciatura = pasos.paso3.tituloOcarrera;
      this.formularioRequisitos.controls['pais'].setValue(pasos.paso3.pais.id);
      this.arregloUniversidades = pasos.paso3.institucion
      this.valorPais = pasos.paso3.pais.id,
      this.addAreasCookie(pasos.paso3.experienciaLaboral.areas);
      this.addEspecialidadCookie(pasos.paso3.experienciaLaboral.especialidades);
      this.addcerradasExpLabCookie(pasos.paso3.experienciaLaboral.preguntas.cerradas);
      this.addExperienciaExpLabCookie(pasos.paso3.experienciaLaboral.preguntas.aniosExperiencia);
      this.arregloPreguntasAbiertas = pasos.paso3.experienciaLaboral.preguntas.abiertas;
      this.addNivelHabilidad(pasos.paso3.habilidadesTecnicas.habilidad);
      this.addcerradasHabTecCookie(pasos.paso3.habilidadesTecnicas.preguntas.cerradas);
      this.addEsperienciaHabTecCookie(pasos.paso3.habilidadesTecnicas.preguntas.aniosExperiencia);
      this.addHabilidadCookie(pasos.paso3.habilidadesSociales.habilidad);
      this.addEsperienciaHabSociCookie(pasos.paso3.habilidadesSociales.preguntas.cerradas);
      this.arregloPreguntasAbiertasHabSoc = pasos.paso3.habilidadesSociales.preguntas.abiertas;
      this.addIdiomasCookieidiomas(pasos.paso3.idiomas);
      this.idiomas = pasos.paso3.habilidadesSociales.preguntas.idiomas
      this.deleteControls();
    }else{
      this.searhNivelEstudios();
      this.searhEstatusAcademicos();
      this.searhAreas();
      this.searhIdiomas();
    }
  }

  deleteControls() {
    this.formularioRequisitos.get('modelNombreinstitucion').setValidators([]);
    this.formularioRequisitos.get('modelNombreinstitucion').updateValueAndValidity();
    this.formularioRequisitos.get('areas').setValidators([]);
    this.formularioRequisitos.get('areas').updateValueAndValidity()
    this.formularioRequisitos.get('especialidad').setValidators([]);
    this.formularioRequisitos.get('especialidad').updateValueAndValidity()
    this.formularioRequisitos.get('habilidadSocial').setValidators([]);
    this.formularioRequisitos.get('habilidadSocial').updateValueAndValidity()
    this.formularioRequisitos.get('idioma').setValidators([]);
    this.formularioRequisitos.get('idioma').updateValueAndValidity();
    this.formularioRequisitos.get('nivelIdioma').setValidators([]);
    this.formularioRequisitos.get('nivelIdioma').updateValueAndValidity();
    this.formularioRequisitos.get('habilidadesSociales').setValidators([]);
    this.formularioRequisitos.get('habilidadesSociales').updateValueAndValidity();
    this.formularioRequisitos.get('habilidadesTecnicas').setValidators([]);
    this.formularioRequisitos.get('habilidadesTecnicas').updateValueAndValidity();
    this.formularioRequisitos.get('nivel').setValidators([]);
    this.formularioRequisitos.get('nivel').updateValueAndValidity();
  }

  addNivelEstudios(){
    let id = this.formularioRequisitos.get('nivelEstudios').value
      return this.nivelesEstudio.find(function(item:any){
          return item.idNivelEstudio == id;
      });
  }

  addEstatusAcademico(){
    let id = this.formularioRequisitos.get('estatusAcademico').value
      return this.estatusAcademico.find(function(item:any){
          return item.idEstatusAcademico == id;
      });
  }
  addAreasCookie(areas:any){
    for(let i = 0; i < areas.length; i++){
      this.agregarArea();
      this.area.controls[i].setValue(areas[i])
    }
  }

  addEspecialidadCookie(especialidades:any){
    for(let i = 0; i < especialidades.length; i++){
      this.agregarEspecialidad();
      this.especialidad.controls[i].setValue(especialidades[i])
    }
  }

  addcerradasExpLabCookie(especialidades:any){
    for(let i = 0; i < especialidades.length; i++){
      this.addPreguntaCerrada(especialidades[i].preguntaCeExpLab, "cerradas")
      this.cerradasExpLab.controls[i].setValue(especialidades[i])
    }
  }

  addExperienciaExpLabCookie(especialidades:any){
    for(let i = 0; i < especialidades.length; i++){
      this.addPreguntaCerrada(especialidades[i].preguntaExExpLab, "experiencia")
      this.aniosExpLab.controls[i].setValue(especialidades[i])
    }
  }

  addNivelHabilidad(especialidades:any){
    for(let i = 0; i < especialidades.length; i++){
      this.agregarHabilidadNivel();
      this.habilidadNivel.controls[i].setValue(especialidades[i])
    }
  }

  addcerradasHabTecCookie(especialidades:any){
    for(let i = 0; i < especialidades.length; i++){
      this.addPreguntaCerrada(especialidades[i].preguntaCeExpTec, "cerradasHabilidadesTecnicas")
      this.cerradasHabTec.controls[i].setValue(especialidades[i])
    }
  }

  addEsperienciaHabTecCookie(especialidades:any){
    for(let i = 0; i < especialidades.length; i++){
      this.addPreguntaCerrada(especialidades[i].preguntaCeExpTec, "experienciaHabilidadesTecnicas")
      this.aniosHabTec.controls[i].setValue(especialidades[i])
    }
  }

  addHabilidadCookie(habilidad:any){
    for(let i = 0; i < habilidad.length; i++){
      this.agregarHabilidadSocial(habilidad[i].nombreHabilidadSocial)
      this.habilidadSocial.controls[i].setValue(habilidad[i])
    }
  }

  addEsperienciaHabSociCookie(especialidades:any){
    for(let i = 0; i < especialidades.length; i++){
      this.addPreguntaCerrada(especialidades[i].preguntaCeExpSoci, "cerradasHabilidadesSociales")
      this.cerradasHabSociales.controls[i].setValue(especialidades[i])
    }
  }

  addIdiomasCookieidiomas(idiomas:any){
    for(let i = 0; i < idiomas.length; i++){
      this.agregarNivelIdioma(idiomas[i].nombreIdioma,idiomas[i].idNivel,false)
      this.nivelIdioma.controls[i].setValue(idiomas[i])
    }
  }

  showMessageError(dangerTpl:any) {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-light ngb-toastsBottom', delay: 3000 });
  }

  /**Configurar buscador de habilidades duras */
  //countries: {name: string, image: string}[] = []

  buscar( termino: string ) {
    this.mostrarSugerencias = false;
    this.hayError = false;
    this.termino  = termino;

    this.catalogService.getHardSkills( termino )
      .subscribe( (hardSkills) => {
        this.hardSkills = hardSkills;
      }, (err) => {
        this.hayError = true;
        this.hardSkills   = [];
      });
  }

  sugerencias( termino: string ) {
    this.hayError = false;
    this.termino = termino;
    this.mostrarSugerencias = true;
    this.catalogService.getHardSkills( termino )
      .subscribe(
        puestos => {
          if(termino === '') {
            this.puestosSugeridos = [];
            this.mostrarSugerencias = false;
          }
          else {
            this.puestosSugeridos = puestos.splice(0,5);
          }
        },
        (err) => this.puestosSugeridos = []
      );
  }

  buscarSugeridoNew( event: any, data: any ) {
    //this.buscar( termino );
    this.mostrarSugerencias = false;
    this.hayError = false;
    this.txtTerminoPuesto = String(event.target.innerText).replace(' * Nuevo','');
    this.selectHardSkill = data;
    if(String(event.target.innerText).includes('Nuevo')) {
      this.isPositionNew = true;
    } else {
      this.isPositionNew = false;
    }
  }

  /**Configurar buscador de habilidades suaves */

  buscarSoftKill( termino: string ) {
    this.mostrarSugerenciasSoft = false;
    this.hayError = false;
    this.terminoSoft  = termino;
    this.catalogService.getSoftSkills( termino )
      .subscribe( (softSkills) => {
        this.softSkills = softSkills;
      }, (err) => {
        this.hayError = true;
        this.softSkills   = [];
      });
  }

  sugerenciasSoftKill( termino: string ) {
    this.hayError = false;
    this.terminoSoft = termino;
    this.mostrarSugerenciasSoft = true;
    this.catalogService.getSoftSkills( termino )
      .subscribe(
        soft => {
          if(termino === '') {
            this.softSugeridos = [];
            this.mostrarSugerenciasSoft = false;
          }
          else {
            this.softSugeridos = soft.splice(0,5);
          }
        },
        (err) => this.softSugeridos = []
      );
  }

  buscarSugeridoSoftkill( event: any, data: any ) {
    this.mostrarSugerenciasSoft = false;
    this.hayError = false;
    this.txtTerminoSoft = String(event.target.innerText).replace(' * Nuevo','');
    this.selectSoftSkill = data;
    if(String(event.target.innerText).includes('Nuevo')) {
      this.isPositionNew = true;
    } else {
      this.isPositionNew = false;
    }
  }



}


let licenciaturas: any = [""]
let instituciones: any = [""]
const generateId = () => Math.random().toString(36).substr(2, 18)
