import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';
import { VacantesService } from 'src/app/services/reclutador/vacantes/vacantes.service';

@Component({
  selector: 'app-vista-previa',
  templateUrl: './vista-previa.component.html',
  styleUrls: ['./vista-previa.component.scss']
})
export class VistaPreviaComponent implements OnInit {
  currentVacancy: any;


  basicSkillsFormatted: any[];
  benefitsFormatted: any[];
  excludingHardSkills: string;
  excludingSpeciality: string;
  excludingLanguagesFormatted: string;
  functionsFormatted: string;
  languagesFormatted: string;
  presentationQuestions: any[];
  questionsFormatted: any[];
  stateFormatted: string;
  speciality: string;
  areasFormatted: string;
  videoQuestions: any = {
    firstRound: [],
    secondRound: []
  };
  workingDayCatalog: any = {
    TIEMPO_COMPLETO: 'Tiempo completo',
    MEDIO_TIEMPO: 'Medio tiempo',
    FLEXIBLE: 'Flexible',
    DIURNO: 'Diurno',
    MIXTO: 'Mixto',
    NOCTURNO: 'Nocturno'
  };
  catalog: any = {
    PRESENCIAL: 'Presencial',
    HOME_OFFICE: 'Medio tiempo',
    MIXTO: 'Mixto',
    HIBRIDO: 'Híbrido',
  };

  psychometricTests: any[];
  modality:any;



  constructor(
    private router: Router,
    private authService: AuthService,
    private vacancyService: VacancyService,
    private _vacanteService: VacantesService) {
    this.currentVacancy = this.vacancyService.getCurrentVacancy();
    console.log(this.currentVacancy);
    if(this.currentVacancy != null){
      this.vacancyService.getVacancy(this.currentVacancy.vacantId)?.subscribe((response) => {
        this.currentVacancy = response;
        this.currentVacancy.modality = this.catalog[this.currentVacancy.modality];

        this.authService.consultState(this.currentVacancy.country.iso2).subscribe((states: any[]) => {
          this.stateFormatted = states.filter((el: any) => el.iso2 === this.currentVacancy.state.iso2)[0].name;
        });
        this.vacancyService.getPresentationQuestions()?.subscribe((presentationsQuestions: any) => {
          if (presentationsQuestions !== null && !(presentationsQuestions.status >= 400)) {
            this.presentationQuestions = presentationsQuestions;
          }
        }, (error) => {
          console.error(error);
        });
        this.vacancyService.getPsychometricTests()?.subscribe((psychometricTestsResponse: any) => {
          this.psychometricTests = psychometricTestsResponse.body.data;
        });

        this.functionsFormatted = this.currentVacancy.functions.replace(/• /g, '').split('\n');
        this.benefitsFormatted = this.currentVacancy.benefitsInvolve.map((el: any) => {
          return (el.benefit ? el.benefit.description : el.benefitOther.description);
        }).concat(this.currentVacancy.benefits?.replace(/• /g, '').split('\n'));

        this.languagesFormatted = this.currentVacancy.languageVacant.map((el: any) => {
          return `${el.language.spanishName} - ${el.level.toLowerCase()}`;
        }).join(', ');

        this.questionsFormatted = this.currentVacancy.questions
          .filter((el: any) => ['AREA_SPECIALTY', 'HARD_SKILL', 'SOFT_SKILL'].includes(el.type) && el.isArmed === false)
          .map((el: any) => el.question);

        this.excludingHardSkills = this.currentVacancy.hardSkillVacant
          .filter((el: any) => el.exclud)
          .map((el: any) => el.skill.name)
          .join(', ');
        this.speciality = this.currentVacancy.specialityVacant
          .map((el: any) => el.speciality.spanishName)
          .join(', ');
        this.areasFormatted = this.currentVacancy.areasVacant
          .map((el: any) => el.area.spanishName)
          .join(', ');
        this.excludingSpeciality = this.currentVacancy.specialityVacant
          .filter((el: any) => el.exclud === true)
          .map((el: any) => el.speciality.spanishName)
          .join(', ');
        this.excludingLanguagesFormatted = this.currentVacancy.languageVacant
          .filter((el: any) => el.exclud)
          .map((el: any) => `${el.language.spanishName} - ${el.level.toLowerCase()}`)
          .join(', ');

        this.basicSkillsFormatted = this.currentVacancy.hardSkillVacant;
        this.basicSkillsFormatted = this.basicSkillsFormatted.concat(this.currentVacancy.softSkillVacant);

        this.videoQuestions.firstRound = this.currentVacancy.questions.filter((el: any) => el.type === 'VIDEO');
        this.videoQuestions.secondRound = this.currentVacancy.questions.filter((el: any) => el.type === 'VIDEO_S');
        document.querySelector('#content')?.scroll(0, 0);
      });
    }else{
      this.router.navigateByUrl('/dashboard/vacantes/activas');
    }
  }

  ngOnInit(): void {
  }

  redirectTo(pantalla: string) {}
  redirectoStep6() {}

  goToStep(step: number): void{
    const targetUrl = '/dashboard/vacantes/create/(vacancy:placeholder)?fromPreview=true';
    let targetPath = '';
    switch (step){
      case 1:
        targetPath = 'description';
        break;
      case 2:
        targetPath = 'position';
        break;
      case 3:
        targetPath = 'requeriments';
        break;
      case 4:
        targetPath = 'questionnaire';
        break;
      case 5:
        targetPath = 'tests';
        break;
      case 6:
        targetPath = 'recruiters';
        break;
      case 7:
        targetPath = 'vacante';
        break;        
    }
    this.router.navigateByUrl(targetUrl.replace('placeholder', targetPath));
  }

  nextStep(): void{
    this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:vacante)');
    /*this.vacancyService.publishVacancy(this.currentVacancy.vacantId)?.subscribe((response) => {
      if (response !== null && !(response.status >= 400)){
        this.vacancyService.resetCurrentVacancy();
        this._vacanteService.setNuevaVacante(true);
        this.router.navigateByUrl('/dashboard/vacantes/create/vacante');
      }
    },
    (error) => {
      console.error('error', error);
    });*/
  }

  getPsychometricTestName(testId: string): string{
    return this.psychometricTests?.filter((test: any) => test.psicometricosCatId === testId)[0].name || '';
  }

  get workingDayFormatted(): string{
    return this.workingDayCatalog[this.currentVacancy.workingDay] || '';
  }

  get missionFormatted(): string{
    return `${this.currentVacancy.mission.at(0).toLowerCase()}${this.currentVacancy.mission.substr(1)}` || '';
  }
}
