import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormArray, FormControl, Validators, FormGroup } from '@angular/forms';
import { Observable, of, OperatorFunction } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap, filter } from 'rxjs/operators';

import { AuthService } from 'src/app/services/auth.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { CreateVacantStep3 } from 'src/app/services/reclutador/vacantes/createVacant/CreateVacantStep3.service';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';



@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {

  formQuestions: FormGroup;
  skillsArray: any[] = [];
  areas: Array<any> = [];
  especialidades: Array<any> = [];
  resultSearch: Array<any> = []
  idiomas: Array<any> = [];
  numQuestions = []

  @Input() radiosArray: Array<{ displayText: string, value: any, id: string, margin: any }> = [];
  @Input() title: string = "";
  @Input() subTitle: string = "";
  @Input() marginLeft: string = ""
  @Input() template = "";
  @Input() placeholder = "";
  @Input() displayErrorMessage = false;

  @Output() radios = new EventEmitter<Array<any>>();

  existQuestion: boolean = false;
  searchFailed = false;
  currentVacancy: any;
  
  inputFormatter = (state: any) => (state.name ? state.name.replace('(Crear) ', '') : '');
  resultFormatter = (state: any) => state.name;
  search!: OperatorFunction<string, readonly { skillId: any, name: any }[]>;
  constructor(private formBuilder: FormBuilder,
    private authService: AuthService,
    public toastService: ToastService,
    private step3Service: CreateVacantStep3,
    private vacancyService: VacancyService
    ) {
}

  ngOnInit(): void {
    this.currentVacancy = this.vacancyService.getCurrentVacancy();
    this.initializeServices();
  }


  initializeServices = () => {
    switch (this.template) {
      case "areas":
        this.initializeFormAreas();
        this.searhAreas();
        this.onchangeAreas();
        break;
      case "hardSkill":
        this.initializeForm();
        this.initializeAutoComplitHardSkill();
        if (this.currentVacancy!= null && this.currentVacancy.hardSkillVacant != null) {
         // this.loadInfTempHardSkill();
        }
        break;
      case "idiomas":
        this.initializeForm();
        this.searhIdiomas();
        break;
      default:
        this.initializeForm();
        break;
    }

    this.numQuestions = this.formQuestions.get("questions")?.value;


  }

  initializeFormAreas = () => {
    this.formQuestions = this.formBuilder.group({
      areas: [{ value: '', disabled: false }, [Validators.required]],
      especialidad: [{ value: '', disabled: false }, [Validators.required]],
      questions: this.formBuilder.array([]),
    })
  }
  initializeForm = () => {
    this.formQuestions = this.formBuilder.group({
      question: [{ value: '', disabled: false }, [Validators.required]],
      questions: this.formBuilder.array([]),
    });
    this.formQuestions.get('question')?.valueChanges.subscribe((change) => {
      if (change !== null && typeof change === 'object'){
        /**
         * Only triggered when edited from an autocomplete object
         */
        if (this.skillsArray.filter((el) => el.name.trim() === change.name.replace('(Crear) ', '').trim()).length === 0){
          /**
           * Verify there's no coincidence in the current skills array
           */
          this.skillsArray.push({...change, name: change.name.replace('(Crear) ', '').trim()});
          this.formQuestions.get('question')?.setValue('');
          this.existQuestion = false;
        }
        else{
          this.existQuestion = true;
        }
      }
    });
  }

  initializeAutoComplitHardSkill = () => {
    this.search = (text$: Observable<string>) =>
      text$.pipe(
        debounceTime(600),
        distinctUntilChanged(),
        filter(term => term.length >= 2),
        switchMap(term =>
          this.step3Service.getHardSkill(term).pipe(
            map((response: any[]) => {
              /**
               * Add the Create option
               */
              if (response.filter((el) => el.name.toLowerCase().trim() === term.toLowerCase().trim()).length === 0){
                response.push({name: `(Crear) ${term}`, isNew: !0});
              }
              return response;
            }),
            tap((response) => {
              this.resultSearch = response;
            }
            ),
            catchError(() => {
              return of([]);
            }))
        )
      )
  }

  get questions(): FormArray {
    return this.formQuestions.get('questions') as FormArray;
  }

  addQuestions = (text: string, update: any) => {
    const areaFormGroupNivel = this.formBuilder.group({
      text: [{ value: (update != null) ? update.text : this.formQuestions.get(text)?.value.name, disabled: false }, []],
      habilidad: [{ value: (update != null) ? update.habilidad : this.formQuestions.get(text)?.value, disabled: false }, []],
      minimumRequired: new FormControl((update != null) ? update.minimumRequired : 'BASICO', Validators.required),
      excluyente: [{ value: (update != null) ? update.excluyente : false, disabled: false }, []],
    });
    this.questions.push(areaFormGroupNivel)
    this.numQuestions = this.formQuestions.get("questions")?.value;
    this.formQuestions.get("question")?.reset();
    //let infoTem = this.step3Service.getInfoStep3();
    //infoTem.hardSkill = this.formQuestions.get("questions")?.value
    this.radios.emit(this.formQuestions.get("questions")?.value)
  }

  removeSkillFromArray(index: number){
    this.skillsArray.splice(index, 1);
  }

  changeRadio(e: any) {
    this.radios.emit(this.formQuestions.get("questions")?.value)
  }


  onClickSubmit = (isAutoComplite: boolean) => {
    if (this.skillsArray.length === 0) {
      this.formQuestions.get('question')?.markAsTouched();
    } else {
      this.skillsArray.forEach((el) => {
        this.formQuestions.get('question')?.setValue(el);
        if (isAutoComplite) {
          this.findTextAutoComplite(this.formQuestions.get("question")?.value)
        } else {
          this.findQuestion(this.formQuestions.get("question")?.value)
        }
        if (this.existQuestion) {
          this.formQuestions.get("question")?.reset();
          setTimeout(() => {
            this.existQuestion = false
          }, 2000);
        } else {
          if (this.searchFailed) {
            this.formQuestions.get("question")?.reset();
            setTimeout(() => {
              this.searchFailed = false
            }, 2000);
          } else {
              this.addQuestions("question", null);
          }
        }
      });
      this.skillsArray = [];
    }
  }

  /**
   * Determina si ya existe una pregunta que cuyo name sea igual a `newQuestion`
   * @param newQuestion `name` de la nueva pregunta
   */
  findQuestion = (newQuestion: string) => {
    const questions = this.formQuestions.get("questions")?.value
    const existQuestion = questions.find((question: any) => question.text === newQuestion);
    this.existQuestion = (existQuestion == undefined) ? false : true
  }

  /**
   * Valida que `newText` sea un objeto y tenga la propiedad name. Si no,
   * establece en true la propiedad searchFailed
   * @param newText
   */
  findTextAutoComplite = (newText: any) => {
    if (newText.hasOwnProperty("name")) {
      this.searchFailed = false
      this.findQuestion(newText.name)
    } else {
      this.searchFailed = true
    }
  }

  findSpeciality = (newSpeciality: string) => {
    const questions = this.formQuestions.get("questions")?.value
    const existQuestion = questions.find((question: any) => question.especialidad.specialtyId === newSpeciality);
    this.existQuestion = (existQuestion == undefined) ? false : true
  }

  deleteQuetion(indice: number) {
    this.questions.removeAt(indice)
    this.radios.emit(this.formQuestions.get("questions")?.value)
  }

  searhAreas = (): void => {
    this.authService.consultAreasEspecialidades().subscribe(
      (response: any) => {
        if (response != null) {
          this.areas = response;
        } else {
          this.toastService.showMessageError("Ocurrió un error al obtener las áreas");
        }
      },
      (error: any) => {
        this.toastService.showMessageError(error.message)
      });
  }

  onchangeAreas = (): void => {
    this.formQuestions.get("areas")?.valueChanges.subscribe((changes: any) => {
      const exiteArea = false
      if (!exiteArea && changes != '') {
        this.searhEspecialidades();
      }
    });
  }

  searhEspecialidades(): void {
    this.authService.consultEspecialidadesPorArea(this.formQuestions.get("areas")?.value).subscribe(
      (response: any) => {
        this.especialidades = response;
        this.formQuestions.get("especialidad")?.reset();
        this.formQuestions.controls['especialidad'].setValue("");
      },
      (error: any) => {
        this.toastService.showMessageError(error.message)
      });
  }

  onClickSubmitAreas = () => {
    if (this.formQuestions.invalid) {
      this.formQuestions.get('areas')?.markAsTouched();
      this.formQuestions.get('especialidad')?.markAsTouched();
    } else {
      this.findSpeciality(this.formQuestions.get("especialidad")?.value)
      if (this.existQuestion) {
        setTimeout(() => {
          this.existQuestion = false
        }, 2000);
      } else {
        //this.addAreas(this.formQuestions.get('areas').value);
      }
    }
  }

  addAreas = (text: string,areas:any,especialidad:any) => {
    const areaFormGroupNivel = this.formBuilder.group({
      text: [{ value:(areas== null) ? especialidad.text : text, disabled: false }, []],
      areas: [{ value: (areas== null) ? especialidad.areas : areas, disabled: false }, []],
      especialidad: [{ value: (areas== null) ? especialidad.especialidad :especialidad, disabled: false }, []],
      minimumRequired: new FormControl((areas== null) ? String(especialidad.yearExperience) :'1', Validators.required),
      excluyente: [{ value: (areas== null) ? Boolean(especialidad.exclud) :false, disabled: false }, []],
    });
    this.questions.push(areaFormGroupNivel)
    this.numQuestions = this.formQuestions.get("questions")?.value;
    this.formQuestions.get("especialidad")?.reset();
    this.radios.emit(this.formQuestions.get("questions")?.value)
  }

  findFiels = (array: any, property: string, campo: string) => {
    let response;
    for (let obj of array) {
      if (obj[property] != undefined && obj[property] == campo) {
        response = obj
      }
    }
    return response
  }

  searhIdiomas(): void {
    this.authService.consultIdiomas().subscribe(
      (response: any) => {
        if (response != null && response.status == 200 && response.body != null && response.body.data.length > 0) {
          this.idiomas = response.body.data.sort((a: any, b: any) => a.spanishName > b.spanishName ? 1 : -1);
        } else {
          this.toastService.showMessageError("Ocurrió un error al obtener los idiomas");
        }
      },
      (error: any) => {
        this.toastService.showMessageError(error.message)
      });
  }

  onClickSubmitIdiomas = () => {
    if (this.formQuestions.invalid) {
      this.formQuestions.get('question')?.markAsTouched();
    } else {
      this.findIdiom(this.formQuestions.get("question")?.value)
      if (this.existQuestion) {
        setTimeout(() => {
          this.existQuestion = false
        }, 2000);
      } else {
        this.addIdiom(this.formQuestions.get('question')?.value,null);
      }
    }
  }

  findIdiom = (newSpeciality: string) => {
    const questions = this.formQuestions.get("questions")?.value
    const existQuestion = questions.find((question: any) => question.idiomas.languageId === newSpeciality);
    this.existQuestion = (existQuestion == undefined) ? false : true
  }

  addIdiom = (text: string,update:any) => {
    const areaFormGroupNivel = this.formBuilder.group({
      text: [{ value: (update != null) ? update.text : this.findFiels(this.idiomas, "languageId", text).spanish_name, disabled: false }, []],
      idiomas: [{ value: (update != null) ? update.idiomas : this.findFiels(this.idiomas, "languageId", text), disabled: false }, []],
      minimumRequired: new FormControl((update != null) ? update.minimumRequired : 'BASICO'),
      excluyente: [{ value: (update != null) ? update.excluyente : false, disabled: false }, []],
    });
    this.questions.push(areaFormGroupNivel)
    this.numQuestions = this.formQuestions.get("questions")?.value;
    //this.formQuestions.get("especialidad")?.reset();
    this.formQuestions.get("question")?.reset();
    this.radios.emit(this.formQuestions.get("questions")?.value)
  }

  loadInfTempHardSkill = () => {
    const infTem: any = this.currentVacancy.hardSkillVacant;
    this.formQuestions.value.questions = infTem
    for (let i of infTem)
      this.addQuestions("question", i)
  }

  loadInfTempIdiom = (array:any) => {
    for (let i of array){
      const update = {
        text: i.language.spanish_name,
        idiomas: this.findFiels(this.idiomas, "languageId", i.language.languageId),
        minimumRequired: i.level,
        excluyente: i.exclud
      };
      this.addIdiom(i.languageId, update);
      }
  }
  

}
