import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-required-education',
  templateUrl: './required-education.component.html',
  styleUrls: ['./required-education.component.scss']
})
export class RequiredEducationComponent implements OnInit {

  @Input() radiosArray: Array<{ displayText: string, id: string, value: any }> = [{ displayText: "Sí", id: "rs1", value: true }, { displayText: "No", id: "rn2", value: false }, { displayText: "Sin preferencia", id: "sp3", value: null }];
  @Input() title: string = '';
  @Input() hideQuestion: boolean = false;
  @Input() typeQuestion: string = '';
  @Output() questions = new EventEmitter<Array<any>>();

  deleteQuestion: string = 'questionsOpen';

  formrequeriments: any;
  currentVacancy: any;

  constructor(private formBuilder: FormBuilder) {
    this.initializeFormClosed();
  }

  ngOnInit(): void {

  }

  isObjectEmpty(obj: any): boolean {
    return obj === undefined || obj === null ? true : Object.entries(obj).length === 0;
  }

  initializeFormClosed = () => {
    this.formrequeriments = this.formBuilder.group({
      question: [{ value: '', disabled: false }, [Validators.required]],
      questions: this.formBuilder.array([]),
      questionsOpen: this.formBuilder.array([]),
      questionnExp: this.formBuilder.array([]),
    })
  }

  initializeFormOpen = () => {
    this.formrequeriments = this.formBuilder.group({
      question: [{ value: '', disabled: false }, [Validators.required]],
      questions: this.formBuilder.array([]),
    })
  }


  get questionsClosed(): FormArray {
    return this.formrequeriments.get('questions') as FormArray;
  }

  get questionsOpen(): FormArray {
    return this.formrequeriments.get('questionsOpen') as FormArray;
  }

  get questionsExp(): FormArray {
    return this.formrequeriments.get('questionnExp') as FormArray;
  }

  addQuestionsClosed = (text: string, value: string, update: any) => {
    const areaFormGroupNivel = this.formBuilder.group({
      text: new FormControl((update != null) ? update.text : text, Validators.required),//[{ value: text, disabled: false }, [Validators.required]],
      radios: [{ value: (update != null) ? update.radios : null, disabled: false }, []],
      excluyente: [{ value: (update != null) ? update.excluyente : false, disabled: false }, []],
      select: [{ value: (update != null) ? update.select : value, disabled: false }, []],
      typeQuestion: [{ value: (update != null) ? update.typeQuestion : value, disabled: false }, []],
      type: [{ value: (update != null) ? update.type : this.typeQuestion, disabled: false }, []]
    });
    this.questionsClosed.push(areaFormGroupNivel)
    this.questions.emit(this.formrequeriments.value)
  }

  addQuestionsOpen = (text: string, value: string, update: any) => {
    const areaFormGroupNivel = this.formBuilder.group({
      text: new FormControl((update != null) ? update.text : text, Validators.required),
      type: [{ value: (update != null) ? update.type : this.typeQuestion, disabled: false }, []],
      selectOpen: [{ value: (update != null) ? update.selectOpen : value, disabled: false }, []],
      typeQuestion: [{ value: (update != null) ? update.selectOpen : value, disabled: false }, []],
    });
    this.questionsOpen.push(areaFormGroupNivel)
    this.questions.emit(this.formrequeriments.value)
  }

  addQuestionsExp = (text: string, value: string, update: any) => {
    const areaFormGroupNivel = this.formBuilder.group({
      text: new FormControl((update != null) ? update.text : text, Validators.required),
      yearExperience: [{ value: (update != null) ? update.yearExperience : 0, disabled: false }, []],
      excluyenteanios: [{ value: (update != null) ? update.excluyenteanios : false, disabled: false }, []],
      selectExp: [{ value: (update != null) ? update.selectExp : value, disabled: false }, []],
      typeQuestion: [{ value: (update != null) ? update.selectExp : value, disabled: false }, []],
      type: [{ value: (update != null) ? update.type : this.typeQuestion, disabled: false }, []],
    });
    this.questionsExp.push(areaFormGroupNivel)
    this.questions.emit(this.formrequeriments.value)
  }

  deleteQuetion(indice: number, type: any) {
    switch (type) {
      case "questions":
        this.questionsClosed.removeAt(indice)
        break;
      case "questionsOpen":
        this.questionsOpen.removeAt(indice)
        break;
      case "questionnExp":
        this.questionsExp.removeAt(indice)
        break;
    }
    this.questions.emit(this.formrequeriments.value)
  }

  anios = (id: any, accion: string, num: any) => {
    if (parseInt(this.questionsExp.value[id].yearExperience) > 0 || accion == "mas") {
      let valor = (accion == "menos") ? parseInt(this.questionsExp.value[id].yearExperience) - 1 : parseInt(this.questionsExp.value[id].yearExperience) + 1
      this.questionsExp.controls[id].patchValue({ yearExperience: valor }, { onlySelf: false })
      this.questions.emit(this.formrequeriments.value)
    }
  }

  changeRadio() {
    this.questions.emit(this.formrequeriments.value)
  }

  clickTypeQuestion = (type: string) => {
    switch (type) {
      case "CERRADA":
        this.deleteQuestion = "questions"
        break;
      case "ABIERTA":
        this.deleteQuestion = "questionsOpen"
        break;
      case "EXPERIENCIA":
        this.deleteQuestion = "questionnExp"
        break;
      default:
        this.deleteQuestion = "questionsOpen"
        break;
    }
  }

  changeTypeQuestion = (type: string, indice: number, text: string) => {
    switch (type) {
      case "CERRADA":
        this.deleteQuetion(indice, this.deleteQuestion)
        this.addQuestionsClosed(text, type, null)
        break;
      case "ABIERTA":
        this.deleteQuetion(indice, this.deleteQuestion)
        this.addQuestionsOpen(text, type, null)
        break;
      case "EXPERIENCIA":
        this.deleteQuetion(indice, this.deleteQuestion)
        this.addQuestionsExp(text, type, null)

        break;
      default:
        this.deleteQuetion(indice, "questionsOpen")
        break;
    }

  }

  onClickSubmit() {
    let cerradas = false;
    let abiertas = false;
    let experiencia = false;

    let istrue = false;

    if (this.questionsClosed.invalid) {
      for (let i = 0; i < this.questionsClosed.controls.length; i++)
        this.questionsClosed.controls[i].markAsTouched()
    } else {
      cerradas = true
    }
    if (this.questionsOpen.invalid) {
      for (let i = 0; i < this.questionsOpen.controls.length; i++)
        this.questionsOpen.controls[i].markAsTouched()
    } else {
      abiertas = true
    }

    if (this.questionsExp.invalid) {
      for (let i = 0; i < this.questionsExp.controls.length; i++)
        this.questionsExp.controls[i].markAsTouched()
    } else {
      experiencia = true
    }

    istrue = (cerradas && abiertas && experiencia) ? true : false
    return istrue
  }
}
