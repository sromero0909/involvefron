import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequiredEducationComponent } from './required-education.component';

describe('RequiredEducationComponent', () => {
  let component: RequiredEducationComponent;
  let fixture: ComponentFixture<RequiredEducationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequiredEducationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequiredEducationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
