import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';

@Component({
  selector: 'app-step-bar',
  templateUrl: './step-bar.component.html',
  styleUrls: ['./step-bar.component.scss']
})
export class StepBarComponent implements OnInit {

  @Input() numButtons = 8;
  @Input() buttonsSelected = 1;

  arrayButtons: Array<any> = [];

  currentVacancy: any;
  constructor(private vacancyService: VacancyService,
              private router: Router) {
    this.currentVacancy = this.vacancyService.getCurrentVacancy();
  }

  ngOnInit(): void {
    this.initializeButtons();
  }


  initializeButtons = () => {
    let clas = '';
    for (let i = 1; i <= this.numButtons; i++){
      clas = (this.buttonsSelected >= i) ? 'redondeado' : 'redondeadoGreen';
      this.arrayButtons.push({
        class: clas
      });

    }
  }

  goToStep(step: number): void{
    if (!this.isObjectEmpty(this.currentVacancy) && step <= this.currentVacancy.steps + 1){
      const targetUrl = '/dashboard/vacantes/create/(vacancy:placeholder)';
      let targetPath = '';
      switch (step){
        case 1:
          targetPath = 'description';
          break;
        case 2:
          targetPath = 'position';
          break;
        case 3:
          targetPath = 'requeriments';
          break;
        case 4:
          targetPath = 'questionnaire';
          break;
        case 5:
          targetPath = 'tests';
          break;
        case 6:
          targetPath = 'recruiters';
          break;
        case 7:
          targetPath = 'vacante';
          break;
      }
      this.router.navigateByUrl(targetUrl.replace('placeholder', targetPath));
    }
  }

  isObjectEmpty(obj: any): boolean {
    return obj === undefined || obj === null? true : Object.entries(obj).length === 0;
  }
}
