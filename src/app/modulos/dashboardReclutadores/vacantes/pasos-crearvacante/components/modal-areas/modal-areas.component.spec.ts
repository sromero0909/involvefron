import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAreasComponent } from './modal-areas.component';

describe('ModalAreasComponent', () => {
  let component: ModalAreasComponent;
  let fixture: ComponentFixture<ModalAreasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalAreasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAreasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
