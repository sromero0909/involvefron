import { Component, OnInit,Output,EventEmitter } from '@angular/core';

import { FormBuilder, Validators } from '@angular/forms';

import { AuthService } from 'src/app/services/auth.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-areas',
  templateUrl: './modal-areas.component.html',
  styleUrls: ['./modal-areas.component.scss']
})
export class ModalAreasComponent implements OnInit {

  @Output() dataTem: EventEmitter<any> = new EventEmitter();

  formAreas: any;

  areas: Array<any> = [];
  especialidades: Array<any> = [];
  constructor(
    private authService: AuthService,
    public toastService: ToastService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
  ) {
    this.initialiceForm();
   }

  ngOnInit(): void {
    this.searhAreas();
    this.onchangeAreas();
  }


  initialiceForm = () => {
    this.formAreas = this.formBuilder.group({
      areas: [{ value: '', disabled: false }, [Validators.required]],
      especialidad: this.formBuilder.array([]),
    })
  }

  searhAreas = (): void => {
    this.authService.consultAreasEspecialidades().subscribe(
      (response: any) => {
        if (response != null) {
          this.areas = response;
        } else {
          this.toastService.showMessageError("Ocurrió un error al obtener las áreas");
        }
      },
      (error: any) => {
        this.toastService.showMessageError(error.message)
      });
  }

  onchangeAreas = (): void => {
    this.formAreas.get("areas")?.valueChanges.subscribe((changes: any) => {
      let exiteArea = false
      if (!exiteArea && changes != '') {
        this.searhEspecialidades();
      }
    });
  }

  searhEspecialidades(): void {
    this.authService.consultEspecialidadesPorArea(this.formAreas.get("areas")?.value).subscribe(
      (response: any) => {
        if(response != null) {
          this.especialidades = response;
          for(let i in this.especialidades){
            this.especialidades[i]['check'] = false;
          }
        }
      },
      (error: any) => {
        this.toastService.showMessageError(error.message)
      });
  }

  changeCheck = (id:string) => {
      for(let i in this.especialidades){
        if(this.especialidades[i].specialtyId == id){
          this.especialidades[i].check = !this.especialidades[i].check
          break;
        }
      }
  }

  setAreas = () => {
    let obj;
    let arr = [];
    let areas = [];
    let text = "";
    let areaToEspeciality = {};
    for(let i of this.especialidades){
      if(i.check){
        delete i.ckeck
        arr.push({...i,isArea:false})
      }
    }
    for(let i of this.areas){
      if(this.formAreas.get('areas').value == i.areaId){
        text = i.spanishName
        areas = i
        areaToEspeciality = {
          areaId: i.areaId,
          check: false,
          englishName: i.englishName,
          spanishName:i.spanishName,
        //  specialtyId: "40288087797b055a01797b37290c0042"
        }
      }
    }
    arr.unshift(areaToEspeciality)

    obj= {
      text,
      areas,
      especialidades:arr
    }
    this.closeModal();
    this.dataTem.emit(obj)
  }

  closeModal = () => {
    this.modalService.dismissAll()
  }
}
