import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { Observable, of, OperatorFunction } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap, filter } from 'rxjs/operators';

import { CreateVacantStep3 } from 'src/app/services/reclutador/vacantes/createVacant/CreateVacantStep3.service';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';
@Component({
  selector: 'app-soft-skills',
  templateUrl: './soft-skills.component.html',
  styleUrls: ['./soft-skills.component.scss']
})
export class SoftSkillsComponent implements OnInit {
  skillsArray: any[] = [];
  
  

  @Input() title: string = "";
  @Input() subTitle: string = "";
  @Output() radios = new EventEmitter<Array<any>>();
  @Input() placeholder = ""
  @Input() displayErrorMessage = false;
  formSoftSkills: any;
  existQuestion: boolean = false;
  searchFailed = false;

  arraySoftSkills: Array<any> = [];
  currentVacancy: any;
  inputFormatter = (state: any) => (state.name ? state.name.replace('(Crear) ', '') : '');
  resultFormatter = (state: any) => state.name;
  search!: OperatorFunction<string, readonly { skillId: any, name: any }[]>

  constructor(private formBuilder: FormBuilder,
    private step3Service: CreateVacantStep3,
    private vacancyService: VacancyService) {
    this.initializeForm();
    this.initializeAutoComplitHardSkill();
    this.loadSoftSkills();
  }

  ngOnInit(): void {
  }


  initializeForm = () => {
    this.formSoftSkills = this.formBuilder.group({
      question: [{ value: '', disabled: false }, [Validators.required]],
    });
    this.formSoftSkills.get('question')?.valueChanges.subscribe((change: any) => {
      if (change !== null && typeof change === 'object'){
        /**
         * Only triggered when edited from an autocomplete object
         */
        if (this.skillsArray.filter((el) => el.name.trim() === change.name.replace('(Crear) ', '').trim()).length === 0){
          /**
           * Verify there's no coincidence in the current skills array
           */
          this.skillsArray.push({...change, name: change.name.replace('(Crear) ', '').trim()});
          this.formSoftSkills.get('question')?.setValue('');
          this.existQuestion = false;
        }
        else{
          this.existQuestion = true;
        }
      }
    });
  }

  initializeAutoComplitHardSkill = () => {
    this.search = (text$: Observable<string>) =>
      text$.pipe(
        debounceTime(600),
        distinctUntilChanged(),
        filter(term => term.length >= 2),
        switchMap(term =>
          this.step3Service.getSoftSkill(term).pipe(
            map((response: any[]) => {
              if (response.filter((el) => el.name.toLowerCase().trim() === term.toLowerCase().trim()).length === 0){
                response.push({name: `(Crear) ${term}`, isNew: !0});
              }
              return response;
            }),
            catchError(() => {
              return of([]);
            }))
        )
      )
  }

  onClickSubmit = () => {
    if (this.skillsArray.length === 0) {
      this.formSoftSkills.get('question')?.markAsTouched();
    } else {
      this.skillsArray.forEach((el) => {
        this.formSoftSkills.get('question')?.setValue(el);
        this.findQuestion(this.formSoftSkills.get("question")?.value)
        if (this.existQuestion) {
          setTimeout(() => {
            this.existQuestion = false
          }, 2000);
        } else {
          if (this.searchFailed) {
            setTimeout(() => {
              this.searchFailed = false
            }, 2000);
          } else {
            this.arraySoftSkills.push({
              softSkill: this.formSoftSkills.get('question').value
            })
            this.radios.emit(this.arraySoftSkills);
          }
        }
        this.formSoftSkills.get('question').reset();
      });
      this.skillsArray = [];
    }
  }

  findQuestion = (newQuestion: any) => {
    if (newQuestion.hasOwnProperty("name")) {
      this.searchFailed = false;
      const existSoftSkill = this.arraySoftSkills.find((question: any) => question.softSkill.name === newQuestion.name);
      this.existQuestion = (existSoftSkill == undefined) ? false : true
    } else {
      this.searchFailed = true
    }
  }

  deleteItemArray(indice: number) {
    this.arraySoftSkills.splice(indice, 1);
    this.radios.emit(this.arraySoftSkills);
  }

  removeSkillFromArray(index: number){
    this.skillsArray.splice(index, 1);
  }

  loadSoftSkills(): void{
    this.currentVacancy = this.vacancyService.getCurrentVacancy();
    if (this.currentVacancy !== null && this.currentVacancy !== undefined && this.currentVacancy.softSkillVacant.length > 0){
      this.arraySoftSkills = this.currentVacancy.softSkillVacant.map((el: any) => {
        return {
          softSkill: {
            name: el.skill.name,
            skillId: el.skillId
          }
        };
      });
    }
  }
}
