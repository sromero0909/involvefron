import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Observable, of, OperatorFunction } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap, filter } from 'rxjs/operators';

import { ModalAreasComponent } from '../components/modal-areas/modal-areas.component';

import { CreateVacantStep3 } from 'src/app/services/reclutador/vacantes/createVacant/CreateVacantStep3.service';
import { AuthService } from 'src/app/services/auth.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { QuestionsComponent } from '../components/questions/questions.component';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';
import { SoftSkillsComponent } from '../components/soft-skills/soft-skills.component';


@Component({
  selector: 'app-requirements',
  templateUrl: './requirements.component.html',
  styleUrls: ['./requirements.component.scss']
})
export class RequirementsComponent implements OnInit {
  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              public toastService: ToastService,
              private modalService: NgbModal,
              private step3Service: CreateVacantStep3,
              private router: Router,
              private vacacyService: VacancyService,
              private activatedRoute: ActivatedRoute,) {

    this.initializeForm();
  }

  @ViewChild('areas') areas!: QuestionsComponent;
  @ViewChild('idioms') idioms!: QuestionsComponent;
  @ViewChild('hardSkill') hardSkill!: QuestionsComponent;
  @ViewChild('softSkill') softSkill!: SoftSkillsComponent;

  currentVacant: any;
  radiosArray: Array<{ displayText: string, value: string, id: string, margin: any }> = [{ displayText: '1 año', value: '1', id: 'r1', margin: false }, { displayText: '2 años', value: '2', id: 'r2', margin: false }, { displayText: '3 años', value: '3', id: 'r3', margin: false }, { displayText: '+5 años', value: '5', id: 'r5', margin: false }];
  radiosArrayAbility: Array<{ displayText: string, value: any, id: string, margin: any }> = [{ displayText: 'Básico', value: 'BASICO', id: 'a1', margin: '-1.4em' }, { displayText: 'Intermedio', value: 'INTERMEDIO', id: 'a2', margin: '-2.2em' }, { displayText: 'Avanzado', value: 'AVANZADO', id: 'a3', margin: '-1.6em' }, { displayText: 'Experto', value: 'EXPERTO', id: 'a4', margin: '-1.5em' }];
  radiosArrayIdiom: Array<{ displayText: string, value: string, id: string, margin: any }> = [{ displayText: 'Básico', value: 'BASICO', id: 'a1', margin: '-1.4em' }, { displayText: 'Intermedio', value: 'INTERMEDIO', id: 'a2', margin: '-2.2em' }, { displayText: 'Avanzado', value: 'AVANZADO', id: 'a3', margin: '-1.6em' }, { displayText: 'Nativo', value: 'NATIVO', id: 'a4', margin: '-1.5em' }];

  arrayInstitution: Array<any> = [];
  arrayAreas: Array<any> = [];
  arrayEspeciality: Array<any> = [];
  arrayHardSkill: Array<any> = [];
  arraySoftSkill: Array<any> = [];
  arrayIdiom: Array<any> = [];

  titleExpLa = 'Experiencia laboral';
  subtTitle = 'Áreas y especialidades';
  titleHabTec = 'Habilidades duras';
  subtHabTec = 'Añada los conocimientos técnicos o habilidades duras requeridas por la posición';
  titleSoftSkills = 'Habilidades blandas';
  subtitleSoftSkills = 'Añada las habilidades sociales o habilidades blandas requeridas por la posición';
  titleIdiomas = 'Añada los idiomas requeridos por la posición';
  subtitleIdiomas = 'Añada los idiomas requeridos por la posición';

  formularioRequisitos: FormGroup;
  nivelesEstudio: Array<any> = [];
  estatusAcademico: any;

  autocomplete = false;
  showImgExp = true;
  showLevel = false;
  search!: OperatorFunction<string, readonly { idInstitution: any, name: any }[]>;

  letter = 1000;
  isStepSubmitted = false;

  inputFormatter = (state: any) => (state.name ? state.name.replace('(Crear) ', '') : '');
  resultFormatter = (state: any) => state.name;

  ngOnInit(): void {
    this.initializeServices();
    setTimeout(() => {
      this.getVacancy();
      document.querySelector('#content')?.scroll(0, 0);
    }, 1000);
  }

  initializeForm = () => {
    this.formularioRequisitos = this.formBuilder.group({
      nivelEstudios: [{ value: '', disabled: false }, [Validators.required]],
      checkNivelEstudios: [{ value: false, disabled: false }, []],
      estatusAcademico: [{ value: '', disabled: false }, [Validators.required]],
      checkEstatusAcademico: [{ value: false, disabled: false }, []],
      carrera: [{ value: '', disabled: false }, []],
      institucion: [{ value: '', disabled: false }, []],
    });
    this.formularioRequisitos.get('institucion')?.valueChanges.subscribe((change) => {
      if (change !== null && change !== undefined && typeof change === 'object'){
        this.addIstitucion(new KeyboardEvent('keyup'), true);
      }
    });
  }

  initializeServices = () => {
    this.searhNivelEstudios();
    this.searhEstatusAcademicos();
    this.onchangeNivelEstudios();
    this.initializeAutoComplit();

  }

  initializeAutoComplit = () => {
    this.search = (text$: Observable<string>) =>
      text$.pipe(
        debounceTime(600),
        distinctUntilChanged(),
        filter(term => term.length >= this.letter),
        switchMap(term =>
          this.step3Service.getInstitution(term).pipe(
            map((response: any[]) => {
              /**
               * Add the Create option
               */
              if (response.filter((el) => el.name.toLowerCase().trim() === term.toLowerCase().trim()).length === 0){
                response.push({name: `(Crear) ${term}`, isNew: !0, idInstitution: ''});
              }
              return response;
            }),
            tap((response) => {
            }
            ),
            catchError(() => {
              return of([]);
            }))
        )
      );
  }

  searhNivelEstudios = (): void => {
    this.authService.consultNivelEstudios().subscribe(
      (response: any) => {
        if (response != null) {
          this.nivelesEstudio = response;
        }
      },
      (error: any) => {
        this.toastService.showMessageError(error.message);
      });
  }

  searhEstatusAcademicos = (): void => {
    this.authService.consultEstatusAcademicos().subscribe(
      (response: any) => {
        if (response != null) {
          this.estatusAcademico = response;
        }
      },
      (error: any) => {
        this.toastService.showMessageError(error.error.message);
      });
  }

  onchangeNivelEstudios(): void {
    this.formularioRequisitos.get('nivelEstudios')?.valueChanges.subscribe((changes: any) => {
      if (changes != '') {
        this.disableInstitution(changes);
        this.hideStatusAcademic(changes);
        for (const nivel of this.nivelesEstudio) {
          if (nivel.educationId == changes && (String(nivel.name).toUpperCase().includes('LICENCIATURA') || String(nivel.name).toLocaleLowerCase().includes('especialidad') || String(nivel.name).toLocaleLowerCase().includes('maes') || String(nivel.name).toLocaleLowerCase().includes('doctorado'))) {
            this.autocomplete = true;
            this.letter = 2;
            this.arrayInstitution = [];
            break;
          } else {
            this.autocomplete = false;
            this.arrayInstitution = [];
            // this.resetAutoComplit
            this.letter = 1000;
            this.formularioRequisitos.get('institucion')?.reset();
          }
        }
      }
    });
  }

  disableInstitution = (changes: string) => {
    for (const nivel of this.nivelesEstudio) {
      if (nivel.educationId == changes && (String(nivel.name).toUpperCase().includes('PRIMARIA') || String(nivel.name).toUpperCase().includes('GENERAL'))) {
        this.showLevel = false;
        this.formularioRequisitos.get('institucion')?.disable();
        break;
      } else {
        this.showLevel = true;
        this.formularioRequisitos.get('institucion')?.enable();
      }
    }
  }

  hideStatusAcademic = (id: string) => {
    for (const nivel of this.nivelesEstudio) {
      if (nivel.educationId == id) {
        for (const i in this.estatusAcademico) {
          if (String(nivel.name).toLocaleLowerCase().includes('bachillerato') || String(nivel.name).toLocaleLowerCase().includes('primaria') || String(nivel.name).toLocaleLowerCase().includes('superior')) {
            if (this.estatusAcademico[i].name.toLocaleLowerCase().includes('pasante') || this.estatusAcademico[i].name.toLocaleLowerCase().includes('titulado')) {
              this.estatusAcademico[i].show = false;
            } else {
              this.estatusAcademico[i].show = true;
            }
          } else {
            this.estatusAcademico[i].show = true;
          }
        }
      }
    }
  }

  addIstitucion(event: KeyboardEvent, isAutoTriggered?: boolean ): void {
    const institution = this.formularioRequisitos.get('institucion')?.value;
    if (((event.code == 'Enter' || event.code == 'NumpadEnter') && institution != '') || isAutoTriggered) {
      if (this.autocomplete == true) {
        if (institution.hasOwnProperty('idInstitution')) {
          if (!this.existIntitution(institution.name)) {
            this.arrayInstitution.push(this.formularioRequisitos.get('institucion')?.value);
          }
          this.deleteValidators('institucion');
        }
        else{
          if (!this.existIntitution(institution.name)) {
            this.arrayInstitution.push({
              name: this.formularioRequisitos.get('institucion')?.value.name.replace('(Crear) ', '')
            });
          }
          this.deleteValidators('institucion');
        }
      } else {
        if (!this.existIntitution(institution)) {
          this.arrayInstitution.push({
            acronym: '',
            codeCountry: '',
            idInstitution: '',
            name: institution
          });
          this.deleteValidators('institucion');
        }
      }
    }
  }

  existIntitution = (newIntitution: any) => {
    let exist = false;
    for (const institution of this.arrayInstitution) {
      if (institution.name == newIntitution.replace('(Crear) ', '')) {
        exist = true;
        break;
      }
    }
    return exist;
  }

  deleteValidators = (field: string) => {
    this.formularioRequisitos.get(field)?.setValue('');
    this.formularioRequisitos.get(field)?.setValidators([]);
    this.formularioRequisitos.get(field)?.updateValueAndValidity();
  }

  deleteItemArray = (array: any, indice: number) => {
    array.splice(indice, 1);
    if (array.length == 0){
      this.showImgExp = false;
    }
  }

  openModalAreas = () => {
    const modalRef = this.modalService.open(ModalAreasComponent, { size: 'lg', centered: true });
    modalRef.componentInstance.dataTem.subscribe((data: any) => {
      for (const i of data.especialidades) {
        this.areas?.addAreas(data.text, data.areas, i);
      }
    });
  }

  getExpLab = (array: any) => {
    this.arrayAreas = [];
    this.arrayEspeciality = [];
    for (const i of array) {
      if (i.especialidad.check) {
        this.arrayEspeciality.push(
          {
            exclud: i.excluyente,
            speciality: {
              specialtyId: i.especialidad.specialtyId,
            },
            yearExperience: Number(i.minimumRequired)
          }
        );
      } else {
        this.arrayAreas.push(
          {
            area: {
              areaId: i.areas.areaId,
            },
            exclud: i.excluyente,
            yearExperience: Number(i.minimumRequired)
          }
        );
      }
    }
  }

  getHardSkill = (array: any) => {
    const infotem = this.step3Service.getInfoStep3();
    infotem.hardSkill = array;
    this.step3Service.setInfoStep3(infotem);
    this.arrayHardSkill = [];
    for (const i of array) {
      if (i.habilidad.hasOwnProperty('isNew') && i.habilidad.isNew) {
        this.arrayHardSkill.push(
          {
            level: i.minimumRequired,
            skill: {
              name: i.habilidad.name
            },
            exclud: i.excluyente
          }
        );
      } else {
        this.arrayHardSkill.push(
          {
            level: i.minimumRequired,
            skillId: i.habilidad.skillId,
            exclud: i.excluyente
          }
        );
      }
    }
  }

  getSoftSkill = (array: any) => {
    this.arraySoftSkill = [];
    for (const i of array) {
      if (i.softSkill.isNew) {
        this.arraySoftSkill.push(
          {
            level: i.minimumRequired,
            skill: {
              name: i.softSkill.name
            }
          }
        );
      } else {
        this.arraySoftSkill.push(
          {
            level: i.minimumRequired,
            skillId: i.softSkill.skillId
          }
        );
      }
    }
  }

  getIdioms = (array: any) => {
    const infotem = this.step3Service.getInfoStep3();
    infotem.idiom = array;
    this.step3Service.setInfoStep3(infotem);
    this.arrayIdiom = [];
    for (const i of array) {
      this.arrayIdiom.push({
        languageId: i.idiomas.languageId,
        level: i.minimumRequired,
        exclud: i.excluyente
      });
    }

  }

  getInstitutions = () => {
    const tem = [];
    for (const i of this.arrayInstitution) {
      if (i.idInstitution == '') {
        tem.push({
          institute: {
            institutionOther: {
              name: i.name.replace('(Crear) ', '')
            }
          }
        });
      } else {
        tem.push({
          idInstitution: i.idInstitution
        });
      }
    }

    return tem;
  }

  getParams = () => {

    const params = {
      academicTitle: this.formularioRequisitos.get('carrera')?.value,
      area: this.arrayAreas,
      hardSkill: this.arrayHardSkill,
      idEducation: this.formularioRequisitos.get('nivelEstudios')?.value,
      levelEducationExclud: this.formularioRequisitos.get('checkNivelEstudios')?.value || false,
      idStatusEducation: this.formularioRequisitos.get('estatusAcademico')?.value,
      statusEducationExclud: this.formularioRequisitos.get('checkEstatusAcademico')?.value || false,
      institution: this.getInstitutions(),
      language: this.arrayIdiom,
      softSkill: this.arraySoftSkill,
      speciality: this.arrayEspeciality
    };

    return params;
  }

  valiateFiels = (isDraft: boolean = false) => {
    const education = this.validateFieldsDescription();
    if (education && this.arrayAreas.length > 0 && this.arraySoftSkill.length > 0 && this.arraySoftSkill.length > 0 ){
    this.saveStep3Tem(isDraft);
  }

  }

  validateFieldsDescription = () => {
    let valid = false;
    this.isStepSubmitted = true;

    if (this.formularioRequisitos.invalid) {
      this.formularioRequisitos.get('estatusAcademico')?.markAsTouched();
      this.formularioRequisitos.get('nivelEstudios')?.markAsTouched();
    } else {
      valid = true;
    }
    return valid;
  }
  saveStep3Tem = (isDraft: boolean = false) => {
    const previousStep = this.currentVacant.steps;
    this.step3Service.saveStep3(this.currentVacant.vacantId, this.getParams()).subscribe(
      (response) => {
        if (response !== null) {
          this.vacacyService.getVacancy(this.currentVacant.vacantId)?.subscribe((newestVacancy: any) => {
            this.vacacyService.setVacancyInfo(newestVacancy);
            if (previousStep > response.steps){
              this.vacacyService.uploadVacancyInfo({}, previousStep)?.subscribe((update) => {
                if (isDraft){
                  this.toastService.showSuccess('Se ha guardado el borrador');
                  this.router.navigateByUrl('/dashboard/vacantes/borradores');
                }
                else{
                  if (this.activatedRoute.snapshot.queryParamMap.has('fromPreview')){
                    this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:preview)');
                  }
                  else{
                    this.router.navigateByUrl('/dashboard/vacantes/create/(vacancy:questionnaire)');
                  }
                }
              });
            }
            else{
              if (isDraft){
                this.toastService.showSuccess('Datos guardados');
              }
              else{
                if (this.activatedRoute.snapshot.queryParamMap.has('fromPreview')){
                  this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:preview)');
                }
                else{
                  this.router.navigateByUrl('/dashboard/vacantes/create/(vacancy:questionnaire)');
                }
              }
            }
          })
        }
      },
      (error) => {

      }// this.vacacyService.getCurrentVacancy().vacantId
    );
  }

  prevStep(): void {
    this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:position)');
  }

  getVacancy = () => {
    this.currentVacant = this.vacacyService.getCurrentVacancy();
    if (this.currentVacant){
      this.loadInfo();
    }
  }


  loadInfo = () => {
    this.currentVacant = this.vacacyService.getCurrentVacancy();
    if (this.currentVacant != null) {
      this.loadInfTempAreas(this.currentVacant.areasVacant, this.currentVacant.specialityVacant);
      this.loadVacancyInfo(this.currentVacant);
      const recoveredHardSkills = this.currentVacant.hardSkillVacant.map((el: any) => {
        return {
          excluyente: el.exclud,
          habilidad: el.skill,
          minimumRequired: el.level,
          text: el.skill.name
        };
      });
      // this.getHardSkill(recoveredHardSkills);
      const recoveredSoftSkills = this.currentVacant.softSkillVacant.map((el: any) => {
        return {
          softSkill: {
            name: el.skill.name,
            skillId: el.skillId
          }
        };
      });
      this.getSoftSkill(recoveredSoftSkills);
      this.idioms.loadInfTempIdiom(this.currentVacant.languageVacant);
      this.loadHardSkill(this.currentVacant.hardSkillVacant);
      // this.loadSoftSkill(this.currentVacant.softSkillVacant)
      this.softSkill.loadSoftSkills();
    }
  }
  loadInfTempAreas = (areasVacant: any, specialityVacant: any) => {
    let obj;
    const arr = [];
    const areas = [];
    const text = '';
    for (const i of areasVacant) {
      this.showImgExp = false;
      const a = {
        text: i.area.spanishName,
        areas: i.area,
        exclud: i.exclud,
        especialidad: {
          areaId: i.area.areaId,
          check: false,
          englishName: i.area.englishName,
          spanishName: i.area.spanishName,
        },
        yearExperience: i.yearExperience
      };
      setTimeout(() => {
        this.areas?.addAreas('', null, a);
        for (const j of specialityVacant) {
          if (i.area.areaId == j.speciality.areaId) {
            const a = {
              text: j.speciality.spanishName,
              areas: i.area,
              exclud: j.exclud,
              especialidad: {
                areaId: j.speciality.areaId,
                check: true,
                englishName: j.speciality.englishName,
                spanishName: j.speciality.spanishName,
                specialtyId: j.speciality.specialtyId

              },
              yearExperience: j.yearExperience
            };
            this.areas?.addAreas('', null, a);
          }

        }
      }, 100);
    }
  }

  loadVacancyInfo(vacancy: any): void {
    this.formularioRequisitos.reset({
      nivelEstudios: vacancy.idEducation || '',
      checkNivelEstudios: vacancy.levelEducationExclud || '',
      estatusAcademico: vacancy.idStatusEducation || '',
      checkEstatusAcademico: vacancy.statusEducationExclud,
      carrera: vacancy.academicTitle || ''
    });

    for (const i of vacancy.institutionVacant) {
      this.arrayInstitution.push({
        idInstitution: i.idInstitution,
        name: (i.institute ? (i.institute.name || i.institute.institutionOther.name) : (i.name || i.institutionOther.name))
      });
      this.deleteValidators('institucion');
    }
  }

  loadHardSkill = (hardSkillVacant: any) => {
    for (const i of hardSkillVacant) {
      const obj = {
        text: i.skill.name,
        habilidad: i.skill,
        minimumRequired: i.level,
        excluyente: i.exclud
      };
      this.hardSkill.addQuestions('question', obj);
    }
  }

  loadSoftSkill = (softSkillVacant: any) => {
    for (const i of softSkillVacant) {
      const obj = {
        text: i.skill.name,
        habilidad: i.skill,
        minimumRequired: i.level,
        excluyente: i.exclud
      };
      this.hardSkill.addQuestions('question', obj);
    }
  }

  isObjectEmpty(obj: any): boolean {
    return obj === undefined || obj === null ? true : Object.entries(obj).length === 0;
  }
}



// TODO
// recuperar hardSkills
