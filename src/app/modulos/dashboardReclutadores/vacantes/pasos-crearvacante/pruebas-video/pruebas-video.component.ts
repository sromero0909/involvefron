import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';

@Component({
  selector: 'app-pruebas-video',
  templateUrl: './pruebas-video.component.html',
  styleUrls: ['./pruebas-video.component.scss']
})
export class PruebasVideoComponent implements OnInit {
  /**
   * Catalog variables
   */
  psychometricTests: any[];
  presentationQuestions: any[];
  technicalQuestions: any = {
    firstRound: [],
    secondRound: []
  };
  testsForm: FormGroup;
  currentVacancy: any;

  constructor(
    private vacancyService: VacancyService,
    private router: Router,
    private fb: FormBuilder,
    private toastService: ToastService,
    private activatedRoute: ActivatedRoute,
  ) {
    this.initializeForm();
  }

  ngOnInit(): void {
    this.vacancyService.getPsychometricTests()?.subscribe((response: any) => {
      if (response !== null && !(response.status >= 400) && response.body) {
        this.psychometricTests = response.body.data.map((el: any) => {
          return {
            enabled: false,
            ...el
          };
        });
        // TODO: Se remueve temporalmente prueba Terman. Cuando se libere por parte de back, eliminar la siguiente línea
        this.psychometricTests = this.psychometricTests.filter((test: any) => test.name !== 'Terman');
        this.loadVacancyInfo('psychometricTests');
      }
    }, (error) => {
      console.error(error);
    });
    this.vacancyService.getPresentationQuestions()?.subscribe((response: any) => {
      if (response !== null && !(response.status >= 400) && response.body) {
        this.presentationQuestions = response.body;
        this.loadVacancyInfo('questions');
      }
    }, (error) => {
      console.error(error);
    });
    this.currentVacancy = this.vacancyService.getCurrentVacancy();
    if (!this.currentVacancy || this.isObjectEmpty(this.currentVacancy)) {
      this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:description)');
    }
    document.querySelector('#content')?.scroll(0, 0);
  }

  /**
   *
   * Define the fields and behavior for the form in this step.
   * Also sets the listeners to save and recover the step information
   * from a service
   */
  initializeForm(): void {
    this.testsForm = this.fb.group({
      psychometricTests: this.fb.array([]),
      firstRoundQuestions: this.fb.array([], [Validators.required]),
      secondRoundQuestions: this.fb.array([], [Validators.required]),
    });
  }

  loadVacancyInfo(section: string): void {
    if (section === 'psychometricTests'){
      const psychometricSavedTests = this.currentVacancy.listVacantPsychometricTestInvolve;
      psychometricSavedTests.forEach((el: any) => {
        this.onTogglePsychometricTest(el.catPsycometricTestId);
      });
    }else if (section === 'questions'){
      const allQuestions = this.currentVacancy.questions;
      this.technicalQuestions.firstRound = allQuestions.filter((el: any) => {
          return el.type === 'VIDEO';
        }).map((el: any) => {
          return el.question;
        });
      this.technicalQuestions.secondRound = allQuestions.filter((el: any) => {
        return el.type === 'VIDEO_S';
      }).map((el: any) => {
        return el.question;
      });
    }
  }

  /**
   *
   * Toggles the value `enabled` inside the psychometricTests
   * catalog variable
   *
   * @param index the position of the element to be changed
   */
  onTogglePsychometricTest(testId: string): void {
    const enabled = this.getPsychometricTestById(testId).enabled;
    this.getPsychometricTestById(testId).enabled = !enabled;
  }

  /**
   * folvera. Pruebas
   */
  submitForm(): void { }

  addTechnicalQuestion(questionText: string, round: number): void {
    if (round === 1 && this.firstRoundQuestionsLength < 4) {
      this.technicalQuestions.firstRound.push(questionText);
    }
    else if (round === 2 && this.secondRoundQuestionsLength < 4) {
      this.technicalQuestions.secondRound.push(questionText);
    }
  }

  removeTechnicalQuestion(index: number, round: number): void {
    if (round === 1) {
      if (index <= this.technicalQuestions.firstRound.length - 1) {
        this.technicalQuestions.firstRound.splice(index, 1);
      }
    }
    else if (round === 2) {
      if (index <= this.technicalQuestions.secondRound.length - 1) {
        this.technicalQuestions.secondRound.splice(index, 1);
      }
    }
  }

  dataToUpload(): any {
    const data: any = {};
    const technicalQuestions: any[] = [];
    const psychometricQuestions: any[] = [];
    this.testsForm.get('psychometricTests')?.value.forEach((el: string) => {
      const test = {
        catPsycometricTestId: el,
      };
      psychometricQuestions.push(test);
    });
    data.listVacantPsychometricTestInvolve = psychometricQuestions;

    this.testsForm.get('firstRoundQuestions')?.value.forEach((el: string) => {
      const question: any = {
        exclud: false,
        type: 'VIDEO',
        question: el,
        typeQuestion: 'CERRADA',
        isArmed: false
      };
      technicalQuestions.push(question);
    });

    this.testsForm.get('secondRoundQuestions')?.value.forEach((el: string) => {
      const question: any = {
        exclud: false,
        type: 'VIDEO_S',
        question: el,
        typeQuestion: 'CERRADA',
        isArmed: false
      };
      technicalQuestions.push(question);
    });
    data.newQuestions = technicalQuestions;
    return data;
  }

  getPsychometricTestById(testId: string) {
    return this.psychometricTests.filter((el) => el.psicometricosCatId === testId)[0];
  }

  prevStep(): void {
    this.router.navigateByUrl('/dashboard/vacantes/create/(vacancy:questionnaire)');
  }

  nextStep(isDraft: boolean = false): void {
    this.testsForm.markAllAsTouched();
    if (this.technicalQuestions.firstRound.length > 0 &&
      this.technicalQuestions.firstRound.length <= 4 &&
      this.technicalQuestions.secondRound.length > 0 &&
      this.technicalQuestions.secondRound.length <= 4) {
      (this.testsForm.get('psychometricTests') as FormArray).clear();
      this.psychometricTests.filter((el) => {
        return el.enabled;
      }).forEach((el: any) => {
        const control = this.fb.control(el.psicometricosCatId);
        (this.testsForm.get('psychometricTests') as FormArray).push(control);
      });
      (this.testsForm.get('firstRoundQuestions') as FormArray).clear();
      this.technicalQuestions.firstRound.forEach((el: string) => {
        const control = this.fb.control(el);
        (this.testsForm.get('firstRoundQuestions') as FormArray).push(control);
      });
      (this.testsForm.get('secondRoundQuestions') as FormArray).clear();
      this.technicalQuestions.secondRound.forEach((el: string) => {
        const control = this.fb.control(el);
        (this.testsForm.get('secondRoundQuestions') as FormArray).push(control);
      });
      if (this.testsForm.valid) {
        const previousStep = this.currentVacancy.steps;
        this.vacancyService.saveStep5(this.currentVacancy.vacantId, this.dataToUpload())?.subscribe((response) => {
          if (response !== null && !(response.status >= 400)){
            if (previousStep > response.steps){
              this.vacancyService.uploadVacancyInfo({}, previousStep)?.subscribe((update) => {
                if (isDraft){
                  this.toastService.showSuccess('Se ha guardado el borrador');
                this.router.navigateByUrl('/dashboard/vacantes/borradores');
                }
                else{
                  if (this.activatedRoute.snapshot.queryParamMap.has('fromPreview')){
                    this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:preview)');
                  }
                  else{
                    this.router.navigateByUrl('/dashboard/vacantes/create/(vacancy:recruiters)');
                  }
                }
              });
            }
            else{
              if (isDraft){
                this.toastService.showSuccess('Se ha guardado el borrador');
                this.router.navigateByUrl('/dashboard/vacantes/borradores');
              }
              else{
                if (this.activatedRoute.snapshot.queryParamMap.has('fromPreview')){
                  this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:preview)');
                }
                else{
                  this.router.navigateByUrl('/dashboard/vacantes/create/(vacancy:recruiters)');
                }
              }
            }
          }
        });
      }
    }
  }

  get firstRoundQuestions(): string[] {
    return this.technicalQuestions.firstRound;
  }

  get firstRoundQuestionsLength(): number {
    return this.technicalQuestions.firstRound.length;
  }

  get secondRoundQuestions(): string[] {
    return this.technicalQuestions.secondRound;
  }

  get secondRoundQuestionsLength(): number {
    return this.technicalQuestions.secondRound.length;
  }

  get hasTechnicalInterviewFirstRoundError(): boolean | undefined {
    return this.testsForm.get('firstRoundQuestions')?.touched && this.testsForm.get('firstRoundQuestions')?.hasError('required');
  }

  get hasTechnicalInterviewSecondRoundError(): boolean | undefined {
    return this.testsForm.get('firstRoundQuestions')?.touched && this.testsForm.get('firstRoundQuestions')?.hasError('required');
  }

  isObjectEmpty(obj: any): boolean {
    return obj === undefined || obj === null ? true : Object.entries(obj).length === 0;
  }
}

