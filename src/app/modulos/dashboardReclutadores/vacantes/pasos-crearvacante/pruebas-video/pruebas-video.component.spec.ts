import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PruebasVideoComponent } from './pruebas-video.component';

describe('PruebasVideoComponent', () => {
  let component: PruebasVideoComponent;
  let fixture: ComponentFixture<PruebasVideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PruebasVideoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PruebasVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
