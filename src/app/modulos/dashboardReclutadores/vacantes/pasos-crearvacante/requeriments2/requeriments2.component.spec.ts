import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Requeriments2Component } from './requeriments2.component';

describe('Requeriments2Component', () => {
  let component: Requeriments2Component;
  let fixture: ComponentFixture<Requeriments2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Requeriments2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Requeriments2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
