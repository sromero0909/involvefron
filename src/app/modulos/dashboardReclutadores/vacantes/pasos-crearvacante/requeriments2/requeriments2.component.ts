import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastService } from 'src/app/services/commons/toast.setvice';
import { CreateVacantStep3 } from 'src/app/services/reclutador/vacantes/createVacant/CreateVacantStep3.service';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';

import { RequiredEducationComponent } from '../components/required-education/required-education.component';

@Component({
  selector: 'app-requeriments2',
  templateUrl: './requeriments2.component.html',
  styleUrls: ['./requeriments2.component.scss']
})
export class Requeriments2Component implements OnInit {

  @ViewChild('expLab') expLab!: RequiredEducationComponent;
  @ViewChild('hardSkill') hardSkill!: RequiredEducationComponent;
  @ViewChild('softSkill') softSkill!: RequiredEducationComponent;
  @ViewChild('prueba') myButton: ElementRef;
  arrayRadios: Array<any> = [];
  params: Array<any> = [];
  hayquestions = { hard: false, soft: false, exp: false };

  arrHard: any;
  arrSof: any;
  arrExp: any;
  currentVacancy: any;


  titleExpLab = 'Experiencia laboral';
  titleHardSkill = 'Habilidades duras';
  titleSoftSkill = 'Habilidades blandas';
  typeQuestion = '';

  initialView = true;
  radios = false;

  constructor(
    private vacancyService: VacancyService,
    private router: Router,
    private step3Service: CreateVacantStep3,
    private toasService: ToastService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.initialiceRadios();
    this.currentVacancy = this.vacancyService.getCurrentVacancy();
    if (this.currentVacancy && !this.isObjectEmpty(this.currentVacancy) && this.currentVacancy != null && this.currentVacancy.questions.length > 0) {
      this.initialView = false;
      setTimeout(() => {
        this.loadVacancyInfo();
        document.querySelector('#content')?.scroll(0, 0);

      }, 100);
    }
    document.querySelector('#content')?.scroll(0, 0);
  }

  isObjectEmpty(obj: any): boolean {
    return obj === undefined || obj === null ? true : Object.entries(obj).length === 0;
  }

  initialiceRadios = () => {
    this.arrayRadios = [];
    this.arrayRadios.push(
      {
        text: '',
        value: 'AREA_SPECIALTY'
      },
      {
        text: '',
        value: 'HARD_SKILL'
      },
      {
        text: '',
        value: 'SOFT_SKILL'
      });
  }


  changeRadioMenu = (event: any) => {
    this.initialView = false;

    setTimeout(() => {
      this.typeQuestion = event;
      this.initialiceRadios();
      switch (event) {
        case 'AREA_SPECIALTY':
          this.expLab.addQuestionsOpen('', 'ABIERTA', null);
          break;
        case 'HARD_SKILL':
          this.hardSkill.addQuestionsOpen('', 'ABIERTA', null);
          break;
        case 'SOFT_SKILL':
          this.softSkill.addQuestionsOpen('', 'ABIERTA', null);
          break;
        default:
          break;
      }
    }, 100);
  }

  getExpLab = (questions: any) => {
    this.arrExp = questions;
    this.hayquestions.exp = (this.arrExp.questionnExp.length > 0 || this.arrExp.questions.length > 0 || this.arrExp.questionsOpen.length > 0) ? true : false;
  }

  getHardSkill = (questions: any) => {
    this.arrHard = questions;
    this.hayquestions.hard = (this.arrHard.questionnExp.length > 0 || this.arrHard.questions.length > 0 || this.arrHard.questionsOpen.length > 0) ? true : false;
  }

  getSoftSkill = (questions: any) => {
    this.arrSof = questions;
    this.hayquestions.soft = (this.arrSof.questionnExp.length > 0 || this.arrSof.questions.length > 0 || this.arrSof.questionsOpen.length > 0) ? true : false;
  }

  getParams = (array: any) => {
    // 2c9f98747c0629d2017c0a3592650006

    for (const i of array) {
      switch (i.typeQuestion) {
        case 'CERRADA':
          this.params.push({
            exclud: i.excluyente,
            question: i.text,
            type: i.type,
            typeQuestion: i.typeQuestion,
            typeAnswer: i.radios,
            isArmed: false
          });
          break;
        case 'ABIERTA':
          this.params.push({
            question: i.text,
            type: i.type,
            typeQuestion: i.typeQuestion,
            isArmed: false
          });
          break;
        case 'EXPERIENCIA':
          this.params.push({
            exclud: i.excluyenteanios,
            question: i.text,
            type: i.type,
            typeQuestion: i.typeQuestion,
            yearsExperience: i.yearExperience,
            isArmed: false
          });
          break;
      }
    }
    return this.params;
  }


  validateFields = () => {
    const hardSkillistrue = this.hardSkill?.onClickSubmit();
    const softSkillistrue = this.softSkill?.onClickSubmit();
    const expLabistrue = this.expLab?.onClickSubmit();
    if (hardSkillistrue && softSkillistrue && expLabistrue) {
      this.params = [];
      if (this.arrExp != undefined) {
        this.getParams(this.arrExp.questionnExp);
        this.getParams(this.arrExp.questions);
        this.getParams(this.arrExp.questionsOpen);
      }

      if (this.arrHard != undefined) {
        this.getParams(this.arrHard.questionnExp);
        this.getParams(this.arrHard.questions);
        this.getParams(this.arrHard.questionsOpen);
      }


      if (this.arrSof != undefined) {
        this.getParams(this.arrSof.questionnExp);
        this.getParams(this.arrSof.questions);
        this.getParams(this.arrSof.questionsOpen);
      }
      setTimeout(() => {
        this.saveStep4(false);
      }, 50);
    }
  }

  saveStep4 = (borrador: boolean) => {
    const previousStep = this.currentVacancy.steps;
    const obj = {
      newQuestions: this.params
    };
    this.step3Service.saveStep4(this.vacancyService.getCurrentVacancy().vacantId, obj).subscribe(
      (response) => {
        if (response != null) {
          this.vacancyService.setVacancyInfo(response);
          if (previousStep > response.steps){
            this.vacancyService.uploadVacancyInfo({}, previousStep)?.subscribe((update) => {
              if (borrador){
                this.toasService.showSuccess('Se ha guardado el borrador');
                this.router.navigateByUrl('/dashboard/vacantes/borradores');
              }
              else{
                if (this.activatedRoute.snapshot.queryParamMap.has('fromPreview')){
                  this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:preview)');
                }
                else{
                  this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:tests)');
                }
              }
            });
          }
          else{
            if (borrador){
              this.toasService.showSuccess('Datos guardados');
            }
            else{
              if (this.activatedRoute.snapshot.queryParamMap.has('fromPreview')){
                this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:preview)');
              }
              else{
                this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:tests)');
              }
            }
          }
        } else {
          this.toasService.showMessageError('Error al guardar las preguntas');
        }
      },
      (error) => {
        this.toasService.showMessageError(error.error);
      }
    );
  }

  loadVacancyInfo = () => {
    for (const i of this.currentVacancy.questions) {
      if (!i.isArmed){
        switch (i.type) {
          case 'HARD_SKILL':
            this.loadTypeQuestions(this.hardSkill, i);
            break;
          case 'SOFT_SKILL':
            this.loadTypeQuestions(this.softSkill, i);
            break;
          case 'AREA_SPECIALTY':
            this.loadTypeQuestions(this.expLab, i);
            break;
        }
      }
    }
  }

  loadTypeQuestions = (type: RequiredEducationComponent, question: any) => {
    switch (question.typeQuestion) {
      case 'ABIERTA':
        const obj = {
          text: question.question,
          type: question.type,
          selectOpen: question.typeQuestion,
          typeQuestion: question.typeQuestion
        };
        type.addQuestionsOpen('', '', obj);
        break;
      case 'EXPERIENCIA':
        const objexp = {
          text: question.question,
          yearExperience: question.yearsExperience,
          excluyenteanios: question.exclud,
          selectExp: question.typeQuestion,
          typeQuestion: question.typeQuestion,
          type: question.type,
        };
        type.addQuestionsExp('', '', objexp);
        break;
      case 'CERRADA':
        const objClose = {
          text: question.question,
          radios: question.typeAnswer,
          excluyente: question.exclud,
          select: question.typeQuestion,
          typeQuestion: question.typeQuestion,
          type: question.type,
        };
        type.addQuestionsClosed('', '', objClose);
        break;
    }
  }

  toBack = () => {
    this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:requeriments)');
  }

  get isAnyQuestion(): boolean{
    return this.hayquestions.exp || this.hayquestions.hard || this.hayquestions.soft;
  }
}
