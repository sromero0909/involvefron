import { Component, OnInit } from '@angular/core';
import { StepperService } from '../../../../../services/reclutador/commons/stepper.service'
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, OperatorFunction } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, tap, switchMap } from 'rxjs/operators';
import { Location } from '@angular/common';
import { CookiePasosVacanteService } from 'src/app/services/reclutador/commons/cookie-pasos-vacantes.service';
import { RutasCrearVacanteService } from 'src/app/services/reclutador/commons/rutasCrearVacante.service';
import { AuthService } from '../../../../../services/auth.service';
import { VacantesActivasService } from 'src/app/services/reclutador/vacantes/vacantes.activas.service';
import { SideNavService } from 'src/app/services/reclutador/vacantes/sideNav.service';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';

@Component({
  selector: 'app-reclutamiento',
  templateUrl: './reclutamiento.component.html',
  styleUrls: ['./reclutamiento.component.scss']
})
export class ReclutamientoComponent implements OnInit {
  formularioReclutadores: FormGroup;
  arregloReclutadores: any[] = [];
  currentVacancy: any;
  viewDefault: boolean = false;
  view: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private stepperService: StepperService,
    private router: Router,
    private vacantesActivasService: VacantesActivasService,
    private _location: Location,
    private _pasosCrearVacante: CookiePasosVacanteService,
    private _rutas: RutasCrearVacanteService,
    private authService: AuthService,
    private sideNavService: SideNavService,
    private vacancyService: VacancyService
  ) {
    this.currentVacancy = this.vacancyService.getCurrentVacancy();
    this.formularioReclutadores = this.formBuilder.group({
      reclutadoresAuxiliares: [{ value: '', disabled: false }, []],
      arrReclutadores: this.formBuilder.array([]),
    });
  }

  ngOnInit(): void {
    this.getAllRecruters();
    document.querySelector('#content')?.scroll(0, 0);    
  }


  getAllRecruters = () => {
    this.sideNavService.getReclutadores().subscribe(
      (response) => {
        console.log("es el response",response);
        if (response.body != null && response.status == 200) {
          this.arregloReclutadores = response.body;
          let reclutadoresTem = []
          for (let i in this.arregloReclutadores) {
            const reclutadorActivo = this.authService.getRecruiterId()
            if (this.arregloReclutadores[i].recruiterId != reclutadorActivo) {
              this.arregloReclutadores[i]['selected'] = false;
              reclutadoresTem.push(this.arregloReclutadores[i])
            }
          }
          this.arregloReclutadores = reclutadoresTem
          this.setRecruiters();
          if (this.arregloReclutadores.length > 0) {
            this.view = true;
            this.viewDefault = false;
          } else {
            this.viewDefault = true;
            this.view = false;
          }

        } else {
          this.viewDefault = true;
          this.view = false;
        }
      },
      (error) => {

      }
    );
  }

  setRecruiters = () => {
    this.currentVacancy = this.vacancyService.getCurrentVacancy();
    if (this.currentVacancy && !this.isObjectEmpty(this.currentVacancy)) {
      for (let i of this.currentVacancy.recruiters) {
        for (let j in this.arregloReclutadores) {
          if (i.recruiterId == this.arregloReclutadores[j].recruiterId) {
            this.arregloReclutadores[j].selected = true
          }
        }
      }
    }
  }


  recriterSelected = (id: string) => {

    for (let i in this.arregloReclutadores) {
      if (this.arregloReclutadores[i].user.userId == id) {
        this.arregloReclutadores[i].selected = !this.arregloReclutadores[i].selected
      }
    }

  }

  isObjectEmpty(obj: any): boolean {
    return obj === undefined || obj === null ? true : Object.entries(obj).length === 0;
  }

  saveRecruiter = () => {
    const params: any = this.getParams();
    if (params.recruiters.length == 0) {
      this.saveStep();
    } else {
      this.vacancyService.saveStep6(this.vacancyService.getCurrentVacancy().vacantId, this.getParams())?.subscribe(
        (response) => {
          if (response != null) {
            const previousStep = this.currentVacancy.steps;
            if (previousStep > response.steps){
              this.vacancyService.uploadVacancyInfo({}, previousStep)?.subscribe((update) => {
                if (update != null){
                  this.redirectTo();
                }
               });
            }
            else{
              this.redirectTo();
            }
          }
        },
        (error) => {

        }
      )
    }

  }

saveStep = () => {
  const previousStep = this.currentVacancy.steps;
  this.vacancyService.uploadVacancyInfo({}, (previousStep > 6 ? previousStep : 6 ))?.subscribe((update) => {
   if(update != null){
     this.redirectTo();
   }
  })
}

  getParams = () => {
    let paramsTem = []
    for (let i in this.arregloReclutadores) {
      if (this.arregloReclutadores[i].selected) {
        paramsTem.push({
          "notifications": true,
          "recruiterId": this.arregloReclutadores[i].recruiterId,
          "type": "AUXILIAR"
        })
      }
    }
    paramsTem.push({
      Notifications: true,
      type: 'AUXILIAR',
      recruiterId: this.authService.getRecruiterId()
    });

    const params = {
      "recruiters": paramsTem
    }
    return params
  }




  toBack = () => {
    this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:tests)');
  }

  redirectTo = () => {
    this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:preview)');
  }

}
