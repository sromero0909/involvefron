import { Component, OnInit } from '@angular/core';
import { StepperService } from '../../../../../services/reclutador/commons/stepper.service';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { VacantesService } from 'src/app/services/reclutador/vacantes/vacantes.service';
import {Location} from '@angular/common';
import { CookiePasosVacanteService } from 'src/app/services/reclutador/commons/cookie-pasos-vacantes.service';
import { AuthService } from '../../../../../services/auth.service';
import { publish } from 'rxjs/operators';
import { IntegrationsService } from 'src/app/services/reclutador/configuration/integrations.service';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { CreateVacantStep2Service } from 'src/app/services/reclutador/vacantes/createVacant/createVacantStep2.service';

@Component({
  selector: 'app-publicar-vacante',
  templateUrl: './publicar-vacante.component.html',
  styleUrls: ['./publicar-vacante.component.scss']
})
export class PublicarVacanteComponent implements OnInit {
  arrayPruebasDomi: any[] = [{nombre: 'involve', url: '../../../../assets/img/dashboard/logoInvolveLetrasVerdes.svg'}, {nombre: 'linkedIn', url: ''}, {nombre: 'occ', url: ''}, {nombre: 'indeed', url: ''}, {nombre: 'facebook', url: ''}];
  arrayPruebas: any [] = [];
  arrayJobBoards: Array<any> = [];
  user: any;
  formIntegrations: FormGroup;
  avisoPrivacidad = false;
  avisoTerminos = false;
  message = false;
  accept = true;
  showMoreControls: any;
  indice: number;
  positionWorkingHours: any [] = [];
  currentVacancy: any;

  constructor(private formBuilder: FormBuilder,
              private stepperService: StepperService,
              private authService: AuthService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private _vacanteService: VacantesService,
              private _location: Location,
              private _pasosCrearVacante: CookiePasosVacanteService,
              private integrationService: IntegrationsService,
              private vacancyService: VacancyService,
              private modalService: NgbModal,
              private toastService: ToastService,
              private createVacancy: CreateVacantStep2Service, ) {
    this.currentVacancy = this.vacancyService.getCurrentVacancy();
    this.formIntegrations = this.formBuilder.group({
      jobBoards: this.formBuilder.array([]),
    });
  }

  ngOnInit(): void {
    this.initializeRows();
    this.user = JSON.parse(this.authService.getUser());
    this.validateLoginBags();
    this.createVacancy.getPositionWorkingHours()?.subscribe((positionWorkingHours: any) => {
      this.positionWorkingHours = positionWorkingHours;
    }, (error: any) => {
      console.log('error', error);
    });
  }

  closeModal = (content: any) => {
    this.modalService.dismissAll();
  }

  createForm = () => {
    this.formIntegrations.reset();
    for (const data of this.arrayJobBoards){
      const areaFormGroup = this.formBuilder.group({
        active: [{ value: data.active, disabled: false }, []],
        name: [{ value: data.name, disabled: false }, []],
        urlLogo: [{ value: data.urlLogo, disabled: false }, []],
        hide: [{ value: data.hide, disabled: false }, []],
        connection: [{ value: data.connection, disabled: false }, []],
        email: [{ value: data.email, disabled: false }, []],
        password: [{ value: data.password, disabled: false }, []],
        message: [{ value: data.message, disabled: false }, []],
      });
      this.jobBoard.push(areaFormGroup);
    }
  }


  get jobBoard(): FormArray{
    return this.formIntegrations.get('jobBoards') as FormArray;
  }

  ngAfterViewInit() {
    document.getElementById('inputOcultore')?.focus();
  }

  inicializaPruebas(): void{
    for (let i = 0; i < this.arrayPruebasDomi.length; i++){
      this.arrayPruebas.push({
        nombre: this.arrayPruebasDomi[i].nombre,
        id: generateId(),
        url: this.arrayPruebasDomi[i].url
      });
    }
  }



  next(pantalla: string): void{
    const show = {
     action: 'next',
     pantalla
    };
    this.stepperService.nextStepper(show);
   // this.redirectoStep5();
 }

 previous(pantalla: string): void{
   const show = {
    action: 'previous',
    pantalla
   };
  // this.redirectoStep5()
  // this.stepperService.nextStepper(show);
}

/*redirectoStep5(){
 // this.router.navigateByUrl("dashboard/vacantes/crear/(vacante:descripcion//condiciones:laborales//requisitos:requisitos//pruebas:video//equipo:reclutamiento)")
 this._location.back();

}*/
  redirecto(){
  this._pasosCrearVacante.setPasosCrearVacante({paso1: '', paso2: '', paso3: '', paso4: '', paso5: '', paso6: ''});
  this._pasosCrearVacante.setStatusPasosCrearVacante({paso1: false, paso2: false, paso3: false, paso4: false, paso5: false, paso6: false});
  this.authService.createVacantP6(String(this.authService.getVacantId())).subscribe(
    (response) => {
    },
    (error) => {
    }
  );
  this._vacanteService.setNuevaVacante(true);
  this.router.navigate(['dashboard/vacantes', 'activas']);
  }

  publishVacancy = () => {
    const activeExternalPlatforms = this.jobBoard.controls.filter((platform: any) => {
      return platform.value.active;
    });
    if (activeExternalPlatforms.length === 0){
      this.vacancyService.publishVacancy(this.currentVacancy.vacantId)?.subscribe((response) => {
        if (response !== null && !(response.status >= 400)){
          this.vacancyService.resetCurrentVacancy();
          this._vacanteService.setNuevaVacante(true);
          this.router.navigateByUrl('/dashboard/vacantes/activas');
        }
      },
      (error) => {
        console.error('error', error);
      });
    }else{
      // TODO: Se tiene que esperar a la publicación de todas las plataformas activas, antes de publicar en Involve. O bien, definir el comportamiento
      for (let i = 0; i < this.jobBoard.controls.length; i++){
        if (this.jobBoard.controls[i].value.active == true && this.jobBoard.controls[i].value.name == 'HIRELINE'){
            this.postulateHirleLineBag();
        }
      }
    }

  }

  validateLoginBags = () => {
    this.integrationService.getActiveBags(this.user.recruiterId).subscribe((response: any) => {
      if (response.code == 200){
        if (response.data.conexion){
          for (let i = 0; i < this.jobBoard.controls.length; i++){
            if (this.jobBoard.controls[i].value.name == response.data.nombre){
                const values = this.jobBoard.controls[i].value;
                values.connection = response.data.conexion;
                values.active = response.data.conexion;
                this.jobBoard.controls[i].setValue(values);
            }
          }
        }
      }
    }, (error: any) => {
        console.log(error);
    });
  }

  initializeRows = () => {
    this.arrayJobBoards.push(/*{
      active:true,
      name:"involve",
      urlLogo: "/assets/img/dashboard/logoInvolveFooter.svg",
      hide:true,
      connection:true,
      email:"involverh@grupotama.com.mx",
      passWord:"12345678",
      confirmPassword:"12345678"
    },*/
    /*{
     active:false,
     name:"OCC",
     urlLogo: "/assets/img/dashboard/vacantes/logoOcc.svg",
     hide:true,
     connection:false,
     email:"",
     password:"",
     confirmPassword:"",
     message:false
   },*/
   {
     active: false,
     name: 'HIRELINE',
     urlLogo: '/assets/img/dashboard/vacantes/logoEmpleosTI.svg',
     hide: true,
     connection: false,
     email: '',
     password: '',
     confirmPassword: '',
     message: false
   });

    this.createForm();
  }

  postulateHirleLineBag = () => {
    const currentVacancie = this.vacancyService.getCurrentVacancy();
    const salary = currentVacancie.salaryMaximum == null ? currentVacancie.salaryMinimum : currentVacancie.salaryMinimum + ' a ' + currentVacancie.salaryMaximum;
    const salaryShow = currentVacancie.salaryShow == false ? 0 : 1;
    const modality = currentVacancie.modality == 'PRESENCIAL' ? 0 : 1;
    const languages = currentVacancie.languageVacant;
    let languageLevel;
    const skillsVac: any = [];
    const exclud = 0;
    let idJobkind;
    let level = 0;
    let confidential = 0;

    languages.forEach((e: any) => {
    if (e.language.spanishName == 'Inglés'){
      switch (e.level){
        case 'BASICO':
          languageLevel = 2;
          break;
          case 'INTERMEDIO':
            languageLevel = 3;
            break;
          case 'AVANZADO':
            languageLevel = 4;
            break;
          case 'NATIVO':
            languageLevel = 1;
            break;
          default:
            console.log('Ningun lenguaje');
            break;
      }
    } else {
      languageLevel = 0;
    }
  });

    this.positionWorkingHours.forEach((e: any) => {
      switch (e.id){
        case 'TIEMPO_COMPLETO':
          idJobkind = 2;
          break;
        case 'MEDIO_TIEMPO':
          idJobkind = 4;
          break;
        default:
          idJobkind = 6;
          break;
      }
    });

    currentVacancie.hardSkillVacant.forEach((e: any) => {
      switch (e.level){
        case 'BASICO':
          level = 1;
          break;
          case 'INTERMEDIO':
            level = 2;
            break;
          case 'AVANZADO':
            level = 3;
            break;
          case 'EXPERTO':
            level = 3;
            break;
          default:
            console.log('Ningun nivel');
            break;
      }
      /*skillsVac.push(
        {
          id:87,
          isMandatory: exclud = e.exclud == false ? 0 : 1,
          level:level
        });*/
    });

    const vacancie: any = {
      jobkindId: idJobkind,
      regionId: currentVacancie.state.id,
      subregionId: currentVacancie.town.id,
      position: currentVacancie.position.position,
      description: currentVacancie.typePosition.spanishName,
      salaryRange: salary,
      badgeId: 1,
      isSalaryVisible: salaryShow,
      active: 1,
      confidential: confidential = currentVacancie.confidential == false ? 0 : 1,
      englishLevelId: languageLevel,
      isTemporalHomeOffice: modality,
      skills: {}
    };

    this.integrationService.saveVacancyToBag(vacancie, this.user.recruiterId).subscribe((response: any) => {
      if (response.status == 200){
        // this.router.navigateByUrl('/dashboard/vacantes/create/vacante');
        this.vacancyService.publishVacancy(this.currentVacancy.vacantId)?.subscribe((publishResponse) => {
          if (publishResponse !== null && !(publishResponse.status >= 400)){
            this.vacancyService.resetCurrentVacancy();
            this._vacanteService.setNuevaVacante(true);
            this.router.navigateByUrl('/dashboard/vacantes/activas');
          }
        },
        (error) => {
          console.error('error', error);
        });
      }
    }, (error: any) => {
        console.log(error);
    });
  }

  activeConnection = () => {
    const values = this.jobBoard.controls[this.indice].value;
    this.message = false;
    const sindex = String(this.indice);
    const response = this.getAuth(sindex, values.email, values.password);
    if (response == 200){
      const connection = values.connection;
      values.connection = (connection) ? false : true;
      this.jobBoard.controls[this.indice].setValue(values);
    }

    const mensaje = (values.connection) ? 'La conexión ha sido validada' : 'Conexión inválida';
    this.toastService.showSuccess(mensaje + ',true');
    this.modalService.dismissAll();
    const active = values.active;
    values.active = (active) ? false : true;
    this.jobBoard.controls[this.indice].setValue(values);
  }

  preventModal = (indice: number, content: any) => {
    const values = this.jobBoard.controls[indice].value;
    if (values.email != '' && values.password != ''){
        this.message = false;
        this.modalService.open(content, { size: 'md' });
    } else {
      this.message = true;
      values.message = this.message;
      this.jobBoard.controls[indice].setValue(values);
    }
  }

  getAuth = (bag: any, email: string, pass: string) => {
    bag = bag == 1 ? 'HIRELINE' : 'OCC';
    this.integrationService.getAuth(this.user.recruiterId, email, pass, bag).subscribe((response: any) => {
      return response.code;
    }, (error: any) => {
        console.log(error);
        return error.status;
    });
    return 500;
  }

  activate = (indice: number, event: any) => {
    const values = this.jobBoard.controls[indice].value;
    values.hide = event.srcElement.checked ? false : true;
    this.jobBoard.controls[indice].setValue(values);
    this.indice = indice;
  }

  activatePriv = (event: any) => {
    event.srcElement.checked ? true : false;
  }

  activateTerm = (event: any) => {
    event.srcElement.checked ? true : false;
  }
}

const generateId = () => Math.random().toString(36).substr(2, 18);
