import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ActivatedRoute,Router } from '@angular/router';
import { StepperService } from '../../../../../services/reclutador/commons/stepper.service'
import { AuthService } from '../../../../../services/auth.service'
import { CookiePasosVacanteService } from 'src/app/services/reclutador/commons/cookie-pasos-vacantes.service';
import {CurrencyPipe, Location} from '@angular/common';
import { RutasCrearVacanteService } from 'src/app/services/reclutador/commons/rutasCrearVacante.service';

@Component({
  selector: 'app-condiciones',
  templateUrl: './condiciones.component.html',
  styleUrls: ['./condiciones.component.scss']
})
export class CondicionesComponent implements OnInit {
  modalidades: any[] = [{idModalidad:"PRESENCIAL",modalidad:"Presencial"},{idModalidad:"HOME_OFFICE",modalidad:"Home Office"},{idModalidad:"MIXTO",modalidad:"Mixto"}]
  idModality: string = "";

  workDays: any[] = [{idJornadaLaboral:"TIEMPO_COMPLETO",jornadaLaboral:"Tiempo completo"},{idJornadaLaboral:"MEDIO_TIEMPO",jornadaLaboral:"Medio tiempo"}]
  idWorkDay: string = "";

  contracts: any;
  idContract: string = "";

  country: Array<any> = [];
  idCountry: string = "MX";

  states: any;
  idState: string = "";

  municipios: any;
  idMunicipio: number = 0;

  nacionalidades: any;
  idNacionalidad:string = "";

  numeroVacante: number = 0;
  numeroPersonas: number = 0;
  formdata: any;
  htmlContent = '';
  iconoInfo: boolean = true;

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Ingresa texto aquí...',
    translate: 'yes',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      [
        'undo',
        'redo',
        'italic',
        'underline',
        'strikeThrough',
        'subscript',
        'superscript',
        'justifyLeft',
        'justifyCenter',
        'justifyRight',
        'justifyFull',
        'indent',
        'outdent',
        'heading',
        'fontName',
        'insertOrderedList',
      ],
      [
        'fontSize',
        'textColor',
        'backgroundColor',
        'customClasses',
        'link',
        'unlink',
        'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode'
      ]
    ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };
  formularioCondiciones: any;
  isSalaryExactly = false;
  isShowNationNationalities: boolean = true;
  isContractDuration = false;

  constructor(private formBuilder: FormBuilder,private stepperService:StepperService,
    private currencyPipe : CurrencyPipe,
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _location:   Location,
    private _pasosCrearVacante: CookiePasosVacanteService,
    private _rutas:RutasCrearVacanteService) {
      this.formdata = this.formBuilder.group({
      description: ['', [Validators.required,
      Validators.maxLength(400), Validators.minLength(5)]]
    });
  }

  ngOnInit(): void {
    this.initForm();
    this.validarCookie();
    this.onChanges();
  }

  onChanges(){
    this.formularioCondiciones.get('sueldoNeto').valueChanges
      .subscribe( (val: any) => {
        this.isSalaryExactly = (val === 'Monto exacto');
        this.validationMinimumSalary();
    });

      this.formularioCondiciones.get('tipoContrato').valueChanges
      .subscribe( (val: any) => {
        this.isContractDuration = (val === '40288088798adb5701798b18e6b80001');
        this.validationContractDuration();
    });
  }

  initForm() {
    this.formularioCondiciones = this.formBuilder.group({
      sueldoNeto: [{ value: '', disabled: false }, [Validators.required]],
      sueldoMinimo: [{ value: '', disabled: false }, [Validators.required]],
      sueldoMaximo: [{ value: '', disabled: false }, [Validators.required]],
      noMostrarSueldo:  [{ value: '', disabled: false }, []],
      editorEnriquecido: [{ value: '', disabled: false }, [Validators.required]],
      pais: [{ value: '', disabled: false }, [Validators.required]],
      estado: [{ value: '', disabled: false }, [Validators.required]],
      municipio: [{ value: '', disabled: false }, [Validators.required]],
      modalidadEmpleo: [{ value: '', disabled: false }, [Validators.required]],
      jornadaLaboral: [{ value: '', disabled: false }, [Validators.required]],
      horarioLaboral: [{ value: '', disabled: false }, [Validators.required]],
      tipoContrato: [{ value: '', disabled: false }, [Validators.required]],
      contractDuration: [{ value: '', disabled: false }, [Validators.required]],
      tiponacionalidadCandidato: [{ value: '', disabled: false }, [Validators.required]],
      nacionalidad: [{ value: '', disabled: false }, [Validators.required]],
    });
  }

  ngAfterViewInit() {
    document.getElementById('inputOculto12')?.focus()
  }


  onClickSubmit() {
    let pasos = this._pasosCrearVacante.getStatusPasosCrearVacante();
    if (this.formularioCondiciones.invalid) {
      this.formularioCondiciones.get('sueldoNeto').markAsTouched();
      this.formularioCondiciones.get('sueldoMinimo').markAsTouched();
      this.formularioCondiciones.get('sueldoMaximo').markAsTouched();
      this.formularioCondiciones.get('noMostrarSueldo').markAsTouched();
      this.formularioCondiciones.get('editorEnriquecido').markAsTouched();
      this.formularioCondiciones.get('pais').markAsTouched();
      this.formularioCondiciones.get('estado').markAsTouched();
      this.formularioCondiciones.get('municipio').markAsTouched();
      this.formularioCondiciones.get('modalidadEmpleo').markAsTouched();
      this.formularioCondiciones.get('jornadaLaboral').markAsTouched();
      this.formularioCondiciones.get('horarioLaboral').markAsTouched();
      this.formularioCondiciones.get('tipoContrato').markAsTouched();
      this.formularioCondiciones.get('contractDuration').markAsTouched();
      this.formularioCondiciones.get('tiponacionalidadCandidato').markAsTouched();
      this.formularioCondiciones.get('nacionalidad').markAsTouched();
      pasos.paso2 = false;
      this._pasosCrearVacante.setStatusPasosCrearVacante(pasos)
    } else {
      this.redirecTo('tres');
      pasos.paso2 = true;
      this._pasosCrearVacante.setStatusPasosCrearVacante(pasos)
    }
    this.savePaso2();
  }

  menosVacantes(): void {
    (this.numeroVacante > 0) ? this.numeroVacante-- : this.numeroVacante
  }

  masVacantes(): void {
    (this.numeroVacante >= 0) ? this.numeroVacante++ : this.numeroVacante
  }

  menosPersonas(): void {
    (this.numeroPersonas > 0) ? this.numeroPersonas-- : this.numeroPersonas
  }

  masPersonas(): void {
    (this.numeroPersonas >= 0) ? this.numeroPersonas++ : this.numeroPersonas
  }

  radioChangeHandlerNacionalities = (event: any) => {
    this.isShowNationNationalities =  !(event.target.value === 'Todas las nacionalidades');
    this.validationNacionality();
  }

  mouseOverInfo(): void {
    this.iconoInfo = !this.iconoInfo;
  }

  mouseLeaveInfo(): void {
    this.iconoInfo = !this.iconoInfo;
  }


  next(pantalla:string):void{
    let show = {
      action:'next',
      pantalla
    }
    this.stepperService.nextStepper(show);
    this.redirectoStep3()
  }

  previous(pantalla:string):void{
    let show ={
    action:'previous',
    pantalla
    }
  this.stepperService.nextStepper(show);
  this.redirectoStep1();
}

redirecTo(pantalla: string): void {
  this._rutas.pantalla(pantalla)
}

//meotod que trae las jornadas laborales
getTiposContrato():void{
  this.authService.consultContract().subscribe(
    (response:any) => {
      this.contracts = response;
    },
    (error:any) => {
    });
  }

  //meotod que trae los paises
getPaises():void{
  this.authService.consultCountryu().subscribe(
    (response:any) => {
      for (const item of response) {
          if(item.iso2 == 'MX' || item.iso2 == 'ES'){
            this.country.push(item);
          }
        }
    },
    (error:any) => {
    });
  }

  searhState():void{
    let pasos = this._pasosCrearVacante.getPasosCrearVacante();
    this.authService.consultState(this.idCountry).subscribe(
      (response:any) => {
        this.states = response;
        if(pasos.paso2.hasOwnProperty("estado")){
        this.formularioCondiciones.controls['estado'].setValue(pasos.paso2.estado.id);
        }
      },
      (error:any) => {
      });
  }

  searhMunicipio():void{
    this.authService.consultMunicipio(this.idCountry,this.idState).subscribe(
      (response:any) => {
        this.municipios = response;
      },
      (error:any) => {
      });
  }

  searchNationality():void{
    this.authService.getNationality().subscribe(
      (response:any) => {
        this.nacionalidades = response
      },
      (error:any) => {
      });
  }

  redirectoStep3(){
    this.router.navigateByUrl("dashboard/vacantes/crear/(vacante:descripcion//condiciones:laborales//requisitos:requisitos)");
  }

  redirectoStep1(){
    //this.router.navigateByUrl("../dashboard/vacantes/crear/(vacante:descripcion");
    this._location.back();
  }

  savePaso2(): void {
    let paso2 = this._pasosCrearVacante.getPasosCrearVacante();
    const minimunSalary = (this.formularioCondiciones.get('sueldoMinimo').value).replace(/,/g, "");
    const maximunSalary = (this.formularioCondiciones.get('sueldoMaximo').value).replace(/,/g, "");
    paso2.paso2 = {
      sueldoNeto: this.formularioCondiciones.get('sueldoNeto').value,
      sueldoMinimo: (this.isSalaryExactly) ? '0' : String(minimunSalary).substring(1, minimunSalary.length-3),
      sueldoMaximo: String(maximunSalary).substring(1, maximunSalary.length-3),
      noMostrarSueldo: this.formularioCondiciones.get('noMostrarSueldo').value,
      editorEnriquecido: this.formularioCondiciones.get('editorEnriquecido').value,
      pais: this.getPais(this.formularioCondiciones.get('pais').value),
      estado: this.getStados(this.formularioCondiciones.get('estado').value),
      municipio: this.getMunicipios(this.formularioCondiciones.get('municipio').value),
      modalidadEmpleo: this.formularioCondiciones.get('modalidadEmpleo').value,
      jornadaLaboral: this.getJornadaLa(this.formularioCondiciones.get('jornadaLaboral').value),
      horarioLaboral: this.formularioCondiciones.get('horarioLaboral').value,
      tipoContrato: this.getTiposContratos(this.formularioCondiciones.get('tipoContrato').value),
      contractDuration: this.contractDuration.value,
      tiponacionalidadCandidato: this.formularioCondiciones.get('tiponacionalidadCandidato').value,
      nacionalidad: this.getNacionalidades(this.formularioCondiciones.get('nacionalidad').value),
      arreglos: {
        modalidades: this.modalidades,
        workDays: this.workDays,
        contracts: this.contracts,
        country: this.country
      }
    }

    this._pasosCrearVacante.setPasosCrearVacante(paso2);
      //enviamos el paso 1 al servicio para guardar
    this.authService.creatVacantP2(paso2.paso2, String(this.authService.getVacantId()), this.isSalaryExactly).subscribe(
      (response: any) => {
      },
      (error: any) => {
      });
  }

  transformAmount(element: any, name: string) {
      let controlValue: any;
      controlValue = (name === 'Sueldo Minimo')? this.sueldoMinimo : this.sueldoMaximo;
      var regexNumber = /^[0-9]+$/;
      if(String(controlValue.value).match(regexNumber)) {
        controlValue.value = this.currencyPipe.transform(controlValue.value, '$');
        element.target.value = controlValue.value;
      }
  }

  getJornadaLa(valor: any) {
    let jornadas ={
      nombre:"",
      id:0
    }
    for(let i = 0; i < this.workDays.length; i++){
      if(valor == this.workDays[i].idJornadaLaboral){
        jornadas.nombre = this.workDays[i].jornadaLaboral;
        jornadas.id = this.workDays[i].idJornadaLaboral;
        break;
      }
    }
    return jornadas
  }

  getTiposContratos(valor: any) {
    let contratos ={
      nombre:"",
      id:0
    }
    for(let i = 0; i < this.contracts.length; i++){
      if(valor == this.contracts[i].contractId){
        contratos.nombre = this.contracts[i].name;
        contratos.id = this.contracts[i].contractId;
        break;
      }
    }
    return contratos
  }

  getPais(valor: any) {
    let paises = [{ idPais: 'MX', nombrePais: "México" }, { idPais: 'ES', nombrePais: "España" }]
    let pais = {
      nameNative: "",
      iso2: ""
    }
    for (let i = 0; i < paises.length; i++) {
      if (valor == paises[i].idPais) {
        pais.nameNative = paises[i].nombrePais;
        pais.iso2 = paises[i].idPais;
        break;
      }
    }
    return pais
  }

  getStados(valor:any){
    let estados ={
      nombre:"",
      id:""
    }
    for(let i = 0; i < this.states.length; i++){
      if(valor == this.states[i].iso2){
        estados.nombre = this.states[i].name;
        estados.id = this.states[i].iso2;
        break;
      }
    }
    return estados
  }

  getMunicipios(valor:any){
    let municipio ={
      nombre:"",
      id:""
    }
    for(let i = 0 ; i < this.municipios.length; i++){
        if(valor == this.municipios[i].name){
          municipio.nombre = this.municipios[i].name
          municipio.id = this.municipios[i].name
          break;
        }
    }
    return municipio
  }

  getNacionalidades(valor:any){
    let nacionalidad ={
      nombre:"",
      id:""
    }
    for(let i = 0 ; i < this.nacionalidades.length; i++){
        if(valor == this.nacionalidades[i].nationalityId){
          nacionalidad.nombre = this.nacionalidades[i].spanishName
          nacionalidad.id = this.nacionalidades[i].nationalityId
          break;
        }
    }
    return nacionalidad
  }

  validarCookie(): void {
    let pasos = this._pasosCrearVacante.getPasosCrearVacante();
    if (pasos.paso2 != '') {

      this.modalidades = pasos.paso2.arreglos.modalidades;
      this.workDays = pasos.paso2.arreglos.workDays;
      this.contracts = pasos.paso2.arreglos.contracts;
      this.country = pasos.paso2.arreglos.country
      this.idCountry = pasos.paso2.pais.id
      this.formularioCondiciones.controls['sueldoNeto'].setValue(pasos.paso2.sueldoNeto);
      this.formularioCondiciones.controls['sueldoMinimo'].setValue(pasos.paso2.sueldoMinimo);
      this.formularioCondiciones.controls['sueldoMaximo'].setValue(pasos.paso2.sueldoMaximo);
      this.formularioCondiciones.controls['noMostrarSueldo'].setValue(pasos.paso2.noMostrarSueldo);
      this.formularioCondiciones.controls['editorEnriquecido'].setValue(pasos.paso2.editorEnriquecido);
      this.formularioCondiciones.controls['pais'].setValue(pasos.paso2.pais.id);
      this.formularioCondiciones.controls['estado'].setValue(pasos.paso2.estado.id);
      this.formularioCondiciones.controls['municipio'].setValue(pasos.paso2.municipio.id);
      this.formularioCondiciones.controls['modalidadEmpleo'].setValue(pasos.paso2.modalidadEmpleo);
      this.formularioCondiciones.controls['jornadaLaboral'].setValue(pasos.paso2.jornadaLaboral.id);
      this.formularioCondiciones.controls['horarioLaboral'].setValue(pasos.paso2.horarioLaboral);
      this.formularioCondiciones.controls['tipoContrato'].setValue(pasos.paso2.tipoContrato.id);
      this.formularioCondiciones.controls['contractDuration'].setValue(pasos.paso2.contractDuration);
      this.formularioCondiciones.controls['tiponacionalidadCandidato'].setValue(pasos.paso2.tiponacionalidadCandidato);
      this.formularioCondiciones.controls['nacionalidad'].setValue(pasos.paso2.nacionalidad);
    } else {
      this.getTiposContrato();
      this.getPaises();
      this.searchNationality();
    }
  }

  get sueldoMaximo() {
    return this.formularioCondiciones.get('sueldoMaximo');
  }

  get sueldoMinimo() {
    return this.formularioCondiciones.get('sueldoMinimo');
  }

  get nacionalidad() {
    return this.formularioCondiciones.get('nacionalidad');
  }

  get contractDuration() {
    return this.formularioCondiciones.get('contractDuration');
  }

  validationMinimumSalary = () => {
    this.sueldoMinimo.clearValidators();
    (this.isSalaryExactly) ? null: this.sueldoMinimo.setValidators([Validators.required]);
    this.sueldoMinimo.updateValueAndValidity();
  }

  validationNacionality = () => {
    this.nacionalidad.clearValidators();
    (this.isShowNationNationalities) ? this.nacionalidad.setValidators([Validators.required]) : null;
    this.nacionalidad.updateValueAndValidity();
  }

  validationContractDuration = () => {
    this.contractDuration.clearValidators();
    (this.isContractDuration) ? this.contractDuration.setValidators([Validators.required]) : null;
    this.contractDuration.updateValueAndValidity();
  }

  public inputValidatorNumber(event: any) {
    const pattern = /^[0-9]*$/;
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
  }

}

