import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';
import { CreateVacantStep2Service } from 'src/app/services/reclutador/vacantes/createVacant/createVacantStep2.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastService } from 'src/app/services/commons/toast.setvice';

@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.scss']
})
export class PositionComponent implements OnInit {
  /**
   * Two forms to enter the informations
   */
  formPosition: FormGroup;
  formWorkingConditions: FormGroup;
  /**
   * An object to save the length of some inputs. It's used
   * to display the limit on the bottom right corner of the field.
   */
  fieldsLength: any = {};

  /**
   * Catalog variables
   */
  benefitsInvolve: any[];
  contractTypes: any[];
  nationalities: any[];
  positionTypes: any[];
  positionWorkingHours: any[];

  currentVacancy: any;

  constructor(private formBuilder: FormBuilder,
              private vacancyService: VacancyService,
              private toastService: ToastService,
              private createVacancy: CreateVacantStep2Service,
              private router: Router,
              private activatedRoute: ActivatedRoute, ) {
                this.initializeForms();
  }

  /**
   * Fill up all the catalog variables
   */
  ngOnInit(): void {
    this.createVacancy.getContractTypes('mx')?.subscribe((contractTypes: any) => {
      this.contractTypes = contractTypes;
    });

    this.createVacancy.getPositionTypes()?.subscribe((positionTypes: any) => {
      this.positionTypes = positionTypes;
   });

    this.createVacancy.getNationalities()?.subscribe((nationalities: any[]) => {
      this.nationalities = nationalities;
    });

    this.createVacancy.getPositionWorkingHours()?.subscribe((positionWorkingHours: any) => {
      this.positionWorkingHours = positionWorkingHours;
    });
    this.currentVacancy = this.vacancyService.getCurrentVacancy();
    if (this.currentVacancy && !this.isObjectEmpty(this.currentVacancy)){
      this.loadVacancyInfo();
    }
    else{
     this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:description)');

    }
    document.querySelector('#content')?.scroll(0, 0);
  }

  /**
   *
   * @returns Defines all the properties to be send before moving the next step
   */
  dataToUpload(): any{
    const data: any = {};

    data.mission = this.formPosition.value.mission;
    data.functions = this.formPosition.value.duties;
    data.typePositionId = this.formPosition.value.positionType;
    data.contractId = this.formWorkingConditions.value.contractType;
    data.peopleCharge = String(this.formPosition.value.managedPeopleAmmount);
    data.schedule = this.formWorkingConditions.value.workingHours;
    data.allNationality = String(this.formWorkingConditions.value.specificNationality === 'false');
    data.workingDay = this.formWorkingConditions.value.workingHoursModality;
    if (data.allNationality === 'false'){
      data.nationalityId = this.formWorkingConditions.value.candidateNationality;
    }

    return data;
  }
  /**
   * Define the fields and behavior for both forms in this step.
   * Also sets the listeners to save and recover the step information
   * from a service
   */
  initializeForms(): void{
    this.formPosition = this.formBuilder.group({
      mission: [{value: '', disabled: false}, [Validators.required, Validators.maxLength(140)]],
      positionType: [{value: '', disabled: false}, [Validators.required]],
      managesPeople: [{ value: 'false', disabled: false }],
      managedPeopleAmmount: [{ value: 0, disabled: false}],
      duties: [{value: '', disabled: false}, [Validators.required, Validators.maxLength(2000)]],
    });
    this.formWorkingConditions = this.formBuilder.group({
      workingHoursModality: [{value: '', disabled: false}, [Validators.required]],
      workingHours: [{value: '', disabled: false}, [Validators.required]],
      contractType: [{value: '', disabled: false}, [Validators.required]],
      specificNationality: [{value: 'false', disabled: false}, []],
      candidateNationality: [{value: '', disabled: false}, []],
    });

  }

  isObjectEmpty(obj: any): boolean {
    return obj === undefined || obj === null? true : Object.entries(obj).length === 0;
  }

  /**
   *
   * Validates if a particular field has a specif error if provided, or any error if not
   *
   * @param fieldName The name of the field to validate
   * @param formToValidate The reference to the form where fieldName belongs
   * @param validationName A particular error name to look for it.
   *
   * @returns `true` if formToValidate.get(fieldName) has the `validationName` error or any error, in case `validationName` is not provided.
   */
  hasFormFieldError(fieldName: string, formToValidate: FormGroup, validationName?: string): boolean{
    if (validationName !== undefined){
      return (formToValidate.get(fieldName)?.touched && formToValidate.get(fieldName)?.hasError(validationName)) || false;
    }
    else{
      return (formToValidate.get(fieldName)?.touched && formToValidate.get(fieldName)?.errors !== null ) || false;
    }
  }

  /**
   *
   * Gets the length of the `fieldName` input
   *
   * @param fieldName The name of the interested field
   * @param formToValidate The reference to the form where fieldName belongs
   *
   * @returns The length of the field's value as a string
   */
  getFieldLength(fieldName: string, formToValidate: FormGroup): number{
    return this.fieldsLength[fieldName] || 0;
  }

  loadVacancyInfo(): void{
    this.formPosition.reset({
      mission: this.currentVacancy?.mission || '',
      positionType: this.currentVacancy?.typePositionId || '',
      managesPeople: String(this.currentVacancy?.peopleCharge > 0) || 'false',
      managedPeopleAmmount: Number(this.currentVacancy?.peopleCharge || 0),
      duties: this.currentVacancy?.functions || '',
      benefits: this.currentVacancy?.benefits || '',
    });
    this.formWorkingConditions.reset({
      workingHoursModality: this.currentVacancy?.workingDay || '',
      workingHours: this.currentVacancy?.schedule || '',
      contractType: this.currentVacancy?.contractId || '',
      specificNationality: String(this.currentVacancy?.allNationality !== null && this.currentVacancy?.allNationality === false) || 'false',
      candidateNationality: this.currentVacancy?.nationalityId || '',
    });
    this.updateFieldLength('mission', this.formPosition);
    this.updateFieldLength('duties', this.formPosition);
    this.updateFieldLength('benefits', this.formPosition);
    
    
    this.benefitsInvolve = this.currentVacancy?.benefitsInvolve.map((el: any) => (el.benefit? el.benefit.description : el.benefitOther.description));
  }

  nextStep(isDraft: boolean = false): void{
    if (this.formPosition.valid && this.formWorkingConditions.valid){
      const updatedStep = (this.currentVacancy.steps > 2 ? this.currentVacancy.steps : 2);
      this.vacancyService.uploadVacancyInfo(this.dataToUpload(), updatedStep)?.subscribe((response) => {
        if (response !== null){
          if(isDraft){
            this.toastService.showSuccess('Se ha guardado el borrador');
            this.router.navigateByUrl('/dashboard/vacantes/borradores');
          }
          else{
            this.redirecto();
          }
        }
      });
    }
    else{
      this.formPosition.markAllAsTouched();
      this.formWorkingConditions.markAllAsTouched();
    }
  }

  prevStep(): void{
    this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:description)');
  }
  /**
   *
   * Triggered by an `input event`, saves on a local property the length of field's value
   *
   * @param fieldName The name of the field to update
   * @param formToValidate The reference to the form where fieldName belongs
   */
  updateFieldLength(fieldName: string, formToValidate: FormGroup): void{
    const value: string = formToValidate.get(fieldName)?.value || '';
    this.fieldsLength[fieldName] = value.length;
  }

  /**
   *
   * Used to update the value of the `managedPeopleAmmount` property inside the `formPosition` form.
   * It can ether be a positive or negative value.
   *
   * @param increment A value to increment/decrement
   */
  updatePositionManagedPeople(increment: number): void{
    const currentValue = this.formPosition.get('managedPeopleAmmount')?.value;
    let newValue = currentValue;
    if ((increment === -1 && currentValue > 1) || 
        (increment === 1 && currentValue < 999)){
      newValue = currentValue + increment;
    }
    this.formPosition.get('managedPeopleAmmount')?.setValue(newValue);
  }

  /**
   * Used to show/hide the `managedPeopleAmmount` field based on the `managesPeople` property
   */
  get positionManagesPeople(): boolean {
    if (this.formPosition.get('managesPeople')?.value === 'false'){
      this.formPosition.get('managedPeopleAmmount')?.setValue(0);
    }
    return this.formPosition.get('managesPeople')?.value === 'true';
  }

  /**
   * Used to show/hide the `candidateNationality` field based on the `specificNationality` property
   */
  get positionsRequiresSpecificNationality(): boolean {
    return this.formWorkingConditions.get('specificNationality')?.value === 'true';
  }

  redirecto() {
    if (this.activatedRoute.snapshot.queryParamMap.has('fromPreview')){
      this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:preview)');
    }
    else{
      this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:requeriments)');
    }
  }

  validateManagedPeopleAmmount(): void{
    if (this.formPosition.get('managedPeopleAmmount')?.value === null){
      this.formPosition.get('managedPeopleAmmount')?.setValue(1);
    }
    else if (this.formPosition.get('managedPeopleAmmount')?.value > 999){
      this.formPosition.get('managedPeopleAmmount')?.setValue(999);
    }
  }
}
