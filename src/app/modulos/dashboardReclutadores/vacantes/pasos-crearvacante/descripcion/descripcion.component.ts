import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ToastService } from 'src/app/services/commons/toast.setvice';
import { CreateVacantStep1Service } from 'src/app/services/reclutador/vacantes/createVacant/createVacantStep1.service';
import { AuthService } from '../../../../../services/auth.service';

import { Observable, of, OperatorFunction } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap, filter } from 'rxjs/operators';

import { ActivatedRoute, Router } from '@angular/router';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';
import { ModalNewClientComponent } from '../../../clients/modal-new-client/modal-new-client.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ClientsService } from '../../../../../services/reclutador/clients/clients.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-descripcion',
  templateUrl: './descripcion.component.html',
  styleUrls: ['./descripcion.component.scss']
})
export class DescripcionComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
              private createVacanService: CreateVacantStep1Service,
              private vacancyService: VacancyService,
              private toasService: ToastService,
              private authService: AuthService,
              private router: Router,
              private modalService: NgbModal,
              private _client: ClientsService,
              private cookieService: CookieService,
              private activatedRoute: ActivatedRoute, ) {
    this.initializeFormDescription();
    this.initializeFormSalary();
    this.initializeAutoComplitClient();
    this.initializeAutoComplitHardSkill();
    this.initializeFormClients();
  }

  get minimumSalaryNumber(): number | null | string{
    return (this.formSalary.get('minimumSalary')?.value ?
      Number(this.formSalary.get('minimumSalary')?.value?.replace(/[^\d.)]/gm, '')) :
      null);
  }

  get maximumSalaryNumber(): number | null | string{
    return (this.formSalary.get('maximumSalary')?.value ?
      Number(this.formSalary.get('maximumSalary')?.value?.replace(/[^\d.)]/gm, '')) :
      null);
  }

  get exactlySalaryNumber(): number | null | string{
    return (this.formSalary.get('exactlySalary')?.value ?
      Number(this.formSalary.get('exactlySalary')?.value?.replace(/[^\d.)]/gm, '')) :
      null);
  }

  @ViewChild(ModalNewClientComponent) newModal!: ModalNewClientComponent;
  @ViewChild('modalExit', { static: true }) modalExit!: TemplateRef<any>;

  numberChars = new RegExp('[^0-9]', 'g');

  textPreviewPosition = '';
  textPreviewMouunt = '';
  minSalary = '';
  maxSalary = '';
  exactlySalary = '';
  textPreviewPrestation = '*Plan de carrera y crecimiento *Sueldo competitivo *Excelente ambiente de trabajo * Estabilidad Laboral';
  textTitle = '';
  typeSalary = '';
  modalityEmployed = '';
  nameCliente = '';
  logoClient: String = '';
  nameMunicipality = '';
  nameState = '';

  modalidades: any[] = [];
  arrayBenefits: Array<any> = [];
  arrayBenefitsInvolve: Array<any> = [];
  arrayBenefitsNames: Array<any> = [];
  states: Array<any> = [];
  country: Array<any> = [];
  municipios: Array<any> = [];
  formAboutVacancy: FormGroup;
  formSalary: FormGroup;
  formClients: FormGroup;

  countCharactersTitle = 0;
  countCharactersEspeciality = 0;
  countBenefits = 0;

  monthlySalary = false;
  messageSalary = false;
  aumentoOtro = false;
  aumentoOtro1 = false;
  isBenefitsValid = true;
  search!: OperatorFunction<string, readonly { id: any, name: any }[]>;
  searchClient!: OperatorFunction<string, readonly { positionId: any, position: any }[]>;

  arrayClientes: Array<any> = [];
  requiresToCreateANewClient = false;

  currentVacancy: any;

  dataModalTem: any;
  inputFormatter = (state: any) => (state.position ? state.position.replace('(Crear) ', '') : '');
  resultFormatter = (state: any) => state.position;

  formatter = (state: any) => {
    return state.name;
  }

  ngOnInit(): void {
    this.getBenefits();
    this.getModality();
    this.getPaises();
    this.currentVacancy = this.vacancyService.getCurrentVacancy();
    if (this.currentVacancy && !this.isObjectEmpty(this.currentVacancy)) {
        this.loadVacancyInfo();
    }
    document.querySelector('#content')?.scroll(0, 0);
  }

  initializeFormDescription = () => {
    this.formAboutVacancy = this.formBuilder.group({
      title: [{ value: '', disabled: false }, [Validators.required]],
      especiality: [{ value: '', disabled: false }, []],
      numVacancies: [{ value: 1, disabled: false }, [Validators.required]],
      confidential: [{ value: false, disabled: false }, []],
    });

    this.formAboutVacancy.get('title')?.valueChanges.subscribe((change) => {
      if (change === null || change === undefined || typeof change === 'object'){
        this.changeCarateres('title');
      }
    });
  }

  initializeFormSalary = () => {
    this.formSalary = this.formBuilder.group({
      monthlySalary: [{ value: 'exactly', disabled: false }, [Validators.required]],
      minimumSalary: [{ value: '', disabled: true }, [Validators.required, Validators.min(1)]],
      maximumSalary: [{ value: '', disabled: true }, [Validators.required, Validators.min(1)]],
      exactlySalary: [{ value: '', disabled: false }, [Validators.required, Validators.min(1)]],
      salaryType: [{ value: '', disabled: false }, [Validators.required]],
      commissions: [{ value: false, disabled: false }, []],
      hideSalary: [{ value: false, disabled: false }, []],
      benefit1: [{ value: '', disabled: false }, []],
      benefit2: [{ value: '', disabled: false }, []]
    });
  }

  initializeFormClients = () => {
    this.formClients = this.formBuilder.group({
      clients: [{ value: '', disabled: false }, [Validators.required]],
      country: [{ value: '', disabled: false }, [Validators.required]],
      state: [{ value: '', disabled: false }, [Validators.required]],
      municipality: [{ value: '', disabled: false }, [Validators.required]],
      modality: [{ value: '', disabled: false }, [Validators.required]],
    });
    this.formClients.get('clients')?.valueChanges.subscribe((change) => {
      if (change === null || change === undefined || typeof change === 'string'){
        this.requiresToCreateANewClient = true;
      }
      else{
        if (change?.name.includes('(Crear) ')){
          this.requiresToCreateANewClient = true;
          this.openModalNewClient(null, 'new');
        }
        else{
          this.requiresToCreateANewClient = false;
        }
      }
      this.getNameClient();
    });
  }

  isObjectEmpty(obj: any): boolean {
    return obj === undefined || obj === null ? true : Object.entries(obj).length === 0;
  }

  loadVacancyInfo(): void {
    this.formAboutVacancy.reset({
      title: {
        position: this.currentVacancy.position?.position || '',
        positionId: this.currentVacancy.position?.positionId || ''
      } ,
      especiality: this.currentVacancy.specialty || '',
      numVacancies: Number(this.currentVacancy.numbersVacants || 0),
      confidential: (this.currentVacancy.confidential !== null && this.currentVacancy.confidential === true || false),
    });
    this.formSalary.reset({
      monthlySalary: (String(this.currentVacancy.salaryExactly) == 'null') ? 'range' : 'exactly',
      salaryType: this.currentVacancy.typeSalary || '',
      commissions: this.currentVacancy.commissions === true || false,
      hideSalary: (this.currentVacancy.salaryShow !== true || false),
    });

    this.formClients.reset({
      clients: {
        clientId: this.currentVacancy.client.clientId,
        logo: (this.currentVacancy.client.company != null && this.currentVacancy.client.company.pathLogo != null) ? this.currentVacancy.client.company.pathLogo : null ,
        name: this.currentVacancy.client.name
      },
      country: this.currentVacancy.country.iso2 || '',
      state: this.currentVacancy.state.iso2 || '',
      modality: this.currentVacancy.modality || '',
    });

    this.changeState();
    /**
     * Longitud de los campos del primer formulario
     */
    this.textTitle = this.formAboutVacancy.value.title;
    this.textPreviewPosition = this.formAboutVacancy.value.especiality;
    this.changeCarateres('title');
    this.changeCarateres('especiality');

    /**
     * Cantidad y tipo de salario
     */
    this.changeSelectSalary();
    this.formSalary.get('exactlySalary')?.setValue(String(this.currentVacancy.salaryExactly || '0'));
    this.formSalary.get('minimumSalary')?.setValue(String(this.currentVacancy.salaryMinimum || '0'));
    this.formSalary.get('maximumSalary')?.setValue(String(this.currentVacancy.salaryMaximum || '0'));
    this.chnageMinSalary();
    this.chnageMaxSalary();
    this.changeHideSalary();
    this.chnageExactySalary();
    this.changeSalaryTipe();
    /**
     * Ubicación de la vacante
     */
    this.getNameClient();
    this.changeCountry();
    this.changeState();
    this.changeMunicipio();
  }


  changeCarateres = (type: string) => {
    switch (type) {
      case 'title':
        if (this.formAboutVacancy.get('title')?.value.position && this.formAboutVacancy.get('title')?.value.position !== ''){
          this.countCharactersTitle = this.formAboutVacancy.get('title')?.value.position.replace('(Crear) ', '').length;
        }
        else{
          this.countCharactersTitle = this.formAboutVacancy.get('title')?.value.length;
        }
        this.textTitle = (this.formAboutVacancy.get('title')?.value.position != undefined) ? this.formAboutVacancy.get('title')?.value.position.replace('(Crear) ', '') : '';
        this.textTitle = (this.textTitle.length > 35) ? String(this.textTitle).substring(0, 35) + '...' : this.textTitle;
        break;
      case 'especiality':
        this.countCharactersEspeciality = String(this.formAboutVacancy.get('especiality')?.value).length;
        this.textPreviewPosition = (this.formAboutVacancy.get('especiality')?.value == '') ? '' : '| ' + this.formAboutVacancy.get('especiality')?.value;
        this.textPreviewPosition = (this.textPreviewPosition.length > 37) ? String(this.textPreviewPosition).substring(0, 37) + '...' : this.textPreviewPosition;
        break;
      default:
        break;
    }
  }

  changeNumVacancies = (option: string) => {
    let numvacancies: number = this.formAboutVacancy.get('numVacancies')?.value;
    numvacancies = (option == 'mas') ? numvacancies + 1 : (numvacancies > 1) ? numvacancies - 1 : 1;
    this.formAboutVacancy.controls.numVacancies.setValue(numvacancies);
  }

  onFocusNumVacancies = () => {
    if (this.formAboutVacancy.get('numVacancies')?.value > 99){
      this.formAboutVacancy.get('numVacancies')?.setValue(99);
    }
    let numvacancies: number = this.formAboutVacancy.get('numVacancies')?.value;
    numvacancies = (numvacancies < 1) ? 1 : numvacancies;
    this.formAboutVacancy.controls.numVacancies.setValue(numvacancies);
  }


  changeSelectSalary = () => {
    const option = this.formSalary.get('monthlySalary')?.value;
    this.formSalary.get('minimumSalary')?.reset();
    this.formSalary.get('maximumSalary')?.reset();
    this.formSalary.get('exactlySalary')?.reset();
    if (option == 'range') {
      this.monthlySalary = true;
      this.formSalary.get('minimumSalary')?.enable();
      this.formSalary.get('maximumSalary')?.enable();
      this.formSalary.get('exactlySalary')?.disable();
      this.exactlySalary = '';
    } else {
      this.monthlySalary = false;
      this.formSalary.get('minimumSalary')?.disable();
      this.formSalary.get('maximumSalary')?.disable();
      this.formSalary.get('exactlySalary')?.enable();
      this.maxSalary = '';
      this.minSalary = '';

    }
  }

  chnageMinSalary = () => {
    this.minSalary = this.formSalary.get('minimumSalary')?.value;
  }

  chnageMaxSalary = () => {
    this.maxSalary = this.formSalary.get('maximumSalary')?.value;
  }

  chnageExactySalary = () => {
    this.exactlySalary = this.formSalary.get('exactlySalary')?.value;
  }

  changeHideSalary = () => {
    this.messageSalary = this.formSalary.get('hideSalary')?.value;
  }

  changeModality = () => {
    const temp = this.formClients.get('modality')?.value;
    for (const i of this.modalidades) {
      if (i.value == temp) {
        this.modalityEmployed = i.name;
      }
    }
  }

  changeSalaryTipe = () => {
    switch (this.formSalary.get('salaryType')?.value) {
      case 'GROSS_SALARY':
        this.typeSalary = 'brutos';
        break;
      case 'NET_INCOME':
        this.typeSalary = 'netos';
        break;
      default:
        this.typeSalary = '';
        break;
    }
  }

  getBenefits = () => {

    this.createVacanService.getBenefits().subscribe(
      (response: any) => {
        if (response != null && response.status == 200 && response.body != null  && response.body.length > 0) {
          for (const obj of response.body) {
            this.arrayBenefits.push({
              check: false,
              disabled: false,
              ...obj
            });
          }
          if (!this.isObjectEmpty(this.currentVacancy)) {
            let countOther = 0;
            this.currentVacancy.benefitsInvolve.forEach((el: any) => {
              if (el.benefitOther != null){
                if (countOther == 0){
                 this.formSalary.controls.benefit1.setValue(el.benefitOther.description);
                 this.changeOtherBenefit1();
                 countOther++;
                }else{
                  this.formSalary.controls.benefit2.setValue(el.benefitOther.description);
                  this.changeOtherBenefit2();
                }
              }
              this.changeBenefits(el.benefitId, true);
            });
          }
        } else {
          this.toasService.showMessageError((response == null) ? 'Ocurrio un error al consultar los beneficios' : response.message);
        }
      },
      (error) => {
        this.toasService.showMessageError(error.message);
      });
  }


  changeBenefits = (id: string, update: boolean) => {
    for (const obj in this.arrayBenefits) {
      if (id == this.arrayBenefits[obj].benefitsId) {
        this.arrayBenefits[obj].check = !this.arrayBenefits[obj].check;
        this.countBenefits = (this.arrayBenefits[obj].check) ? this.countBenefits + 1 : this.countBenefits - 1;
        break;
      }
    }
    this.getNameBenefits();
    this.isBenefitsValid = this.arrayBenefits.length > 0;
  }


  changeOtherBenefit1 = () => {

    if (this.formSalary.get('benefit1')?.value != '' && !this.aumentoOtro) {
      this.countBenefits = this.countBenefits + 1;
      this.aumentoOtro = true;
    } else {
      if (this.formSalary.get('benefit1')?.value == '' && this.aumentoOtro) {
        this.countBenefits = this.countBenefits - 1;
        this.aumentoOtro = false;
      }
    }
    this.getNameBenefits();
    this.isBenefitsValid = this.arrayBenefits.length > 0;
  }

  changeOtherBenefit2 = () => {

    if (this.formSalary.get('benefit2')?.value != '' && !this.aumentoOtro1) {
      this.countBenefits = this.countBenefits + 1;
      this.aumentoOtro1 = true;
    } else {
      if (this.formSalary.get('benefit2')?.value == '' && this.aumentoOtro1) {
        this.countBenefits = this.countBenefits - 1;
        this.aumentoOtro1 = false;
      }
    }
    this.getNameBenefits();
    this.isBenefitsValid = this.arrayBenefits.length > 0;
  }

  getIdBenefits = () => {
    this.arrayBenefitsInvolve = [];
    for (const obj of this.arrayBenefits) {
      if (obj.check) {
        this.arrayBenefitsInvolve.push({ benefitId: obj.benefitsId });
      }
    }
    if (this.aumentoOtro) {
      this.arrayBenefitsInvolve.push({
          benefitOther: {
            description: this.formSalary.get('benefit1')?.value
          }
        });
    }
    if (this.aumentoOtro1) {
      this.arrayBenefitsInvolve.push({
          benefitOther: {
            description: this.formSalary.get('benefit2')?.value
          }
        });
    }
  }

  getNameBenefits = () => {
    this.arrayBenefitsNames = [];
    for (const obj of this.arrayBenefits) {
      if (obj.check) {
        this.arrayBenefitsNames.push(obj.description);
      }
    }
    if (this.aumentoOtro) {
      this.arrayBenefitsNames.push(this.formSalary.get('benefit1')?.value);

    }
    if (this.aumentoOtro1) {
      this.arrayBenefitsNames.push(this.formSalary.get('benefit2')?.value);
    }
  }
  getModality = () => {
    this.createVacanService.getCatalogModality().subscribe(
      (response: any) => {
        if (response != null) {

          this.modalidades.push(
            {
              value: response[0][0][0],
              name: response[0][0][1]
            },
            {
              value: response[1][0][0],
              name: response[1][0][1]
            },
            {
              value: response[2][0][0],
              name: response[2][0][1]
            });
          if (this.currentVacancy && !this.isObjectEmpty(this.currentVacancy)) {
              this.changeModality();
              this.formClients.get('modality')?.setValue(this.currentVacancy.modality || '');
            }

        }
      },
      (error) => {

      });
  }

  getPaises = (): void => {
    this.authService.consultCountryu().subscribe(
      (response: any) => {
        if (response != null) {
          for (const item of response) {
            if (item.iso2 == 'MX' || item.iso2 == 'ES') {
              this.country.push(item);
            }
          }
        }
      },
      (error: any) => {
        this.toasService.showMessageError(error.error.message);
      });
  }

  getNameClient = () => {
    const client: any = this.formClients.get('clients')?.value;
    if (client && client.name?.includes('(Crear) ')){
      this.openModalNewClient(null, 'new');
    }
    else{
      this.nameCliente = client.name?.replace('(Crear) ', '') || '';
      this.logoClient = (client.hasOwnProperty('clientId')) ? client.logo : 'https://involve-resources.s3.amazonaws.com/res-involve/Involve_header.png';
      this.changeMunicipio();
    }
  }

  changeCountry = () => {
    this.searhState();
  }

  searhState = (): void => {
    this.authService.consultState(this.formClients.get('country')?.value).subscribe(
      (response: any[]) => {
        this.states = response.sort((a, b) => (a.name > b.name ? 1 : -1));
      },
      (error: any) => {
        this.toasService.showMessageError(error.error.message);
      });
  }

  changeState = () => {
    const iso2 = this.formClients.get('state')?.value;
    this.searhMunicipio();
    for (const obj of this.states) {
      if (obj.iso2 == iso2) {
        this.nameState = obj.name || '';
      }
    }
  }

  searhMunicipio = (): void => {
    this.authService.consultMunicipio(this.formClients.get('country')?.value, this.formClients.get('state')?.value).subscribe(
      (response: any[]) => {
        this.municipios = response.sort((a, b) => (a.name > b.name ? 1 : -1));
        if (this.currentVacancy && !this.isObjectEmpty(this.currentVacancy)) {
          setTimeout(() => {
            this.formClients.get('municipality')?.setValue(this.currentVacancy.town.name || '');
          }, 100);
        }
      },
      (error: any) => {
        this.toasService.showMessageError(error.error.message);
      });
  }


  changeMunicipio = (): void => {
    this.nameMunicipality = this.formClients.get('municipality')?.value || '';
  }

  validAllFields = (isDraft: boolean = false) => {
    const formdesc = this.validateFieldsDescription();
    const formSal = this.validateFieldsSalary();
    const formBene = this.validateBenefits();
    this.isBenefitsValid = formBene;
    const arrayClien = this.validateClient();
    const formClient = this.validateFormClients();

    if (formdesc && formSal && formBene && arrayClien && formClient && !this.requiresToCreateANewClient) {
      this.createVacancy(isDraft);
    }
  }

  validateFieldsDescription = () => {
    let valid = false;
    if (this.formAboutVacancy.invalid) {
      this.formAboutVacancy.get('title')?.markAsTouched();
      this.formAboutVacancy.get('numVacancies')?.markAsTouched();
    } else {
      valid = true;
    }
    return valid;
  }

  validateFieldsSalary = () => {
    let valid = false;
    if (this.formSalary.invalid) {
      this.formSalary.get('monthlySalary')?.markAsTouched();
      this.formSalary.get('minimumSalary')?.markAsTouched();
      this.formSalary.get('maximumSalary')?.markAsTouched();
      this.formSalary.get('exactlySalary')?.markAsTouched();
      this.formSalary.get('salaryType')?.markAsTouched();
    } else {
      valid = true;
    }
    return valid;
  }

  validateBenefits = () => {
    this.getIdBenefits();
    let valid = false;
    if (this.arrayBenefitsInvolve.length > 0) {
      valid = true;
    }
    return valid;
  }

  validateClient = () => {
    let valid = false;
    const client = this.formClients.get('clients')?.value;
    if (client.hasOwnProperty('clientId') || client.hasOwnProperty('employees')) {
      valid = true;
    } else {
     // this.formClients.controls.clients.setValue('');
    }
    return valid;
  }

  validateFormClients = () => {
    let valid = false;
    if (this.formClients.invalid) {
      this.formClients.get('clients')?.markAsTouched();
      this.formClients.get('country')?.markAsTouched();
      this.formClients.get('state')?.markAsTouched();
      this.formClients.get('municipality')?.markAsTouched();
      this.formClients.get('modality')?.markAsTouched();
    } else {
      valid = true;
    }
    return valid;
  }

  initializeAutoComplitClient = () => {
    this.search = (text$: Observable<string>) =>
      text$.pipe(
        debounceTime(600),
        distinctUntilChanged(),
        filter(term => term.length >= 2),
        switchMap(term =>
          this.createVacanService.getClientsByQuery(term).pipe(
            map((response: any) => {
              this.arrayClientes = [];
              let alreadyExists = false;
              if (response != null && response.status == 200 && response.body != null && response.body.length > 0){
                for (const obj of response.body) {
                  if (obj.name.toLowerCase().trim() === term.toLowerCase().trim()){
                    alreadyExists = true;
                  }
                  this.arrayClientes.push({
                    clientId: obj.clientId,
                    name: obj.name,
                    logo: obj?.company?.pathLogo || null
                  });
                }
              }
              if (!alreadyExists){
                this.arrayClientes.push({
                  name: `(Crear) ${term}`
                });
              }
              return term;
            }),
            map(term => (this.arrayClientes.length == 0) ? []
              : this.arrayClientes.filter((v: any) => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
          )
        )
      );
  }

  initializeAutoComplitHardSkill = () => {
    this.searchClient = (text$: Observable<string>) =>
      text$.pipe(
        debounceTime(600),
        distinctUntilChanged(),
        filter(term => term.length >= 2),
        switchMap(term =>
          this.createVacanService.getCatalogPosition(term).pipe(
            map((response: any[]) => {
              /**
               * Add the Create option
               */
              if (response.filter((el) => el.position.toLowerCase().trim() === term.toLowerCase().trim()).length === 0){
                response.push({position: `(Crear) ${term}`, isNew: !0});
              }
              return response;
            }),
            tap((response: any) => {
             // this.resultSearch = response;
            }
            ),
            catchError(() => {
              return of([]);
            }))
        )
      );
  }

  getParamsCreateVacancy = (): any => {
    const params: any =
    {
      specialty: this.formAboutVacancy.get('especiality')?.value,
      client: this.formClients.get('clients')?.value,
      numbersVacants: String(this.formAboutVacancy.get('numVacancies')?.value),
      typeSalary: this.formSalary.get('salaryType')?.value,
      commissions: this.formSalary.get('commissions')?.value,
      confidential: this.formAboutVacancy.get('confidential')?.value,
      salaryShow: this.formSalary.get('hideSalary')?.value !== true,
      benefitsInvolve: this.arrayBenefitsInvolve,
      country: this.getCountryByCode(this.formClients.get('country')?.value),
      state: this.getStateByCode(this.formClients.get('state')?.value),
      town: this.getMunicipalityByCode(this.formClients.get('municipality')?.value),
      modality: this.formClients.get('modality')?.value,
    };
    if (this.exactlySalaryNumber == '' || this.exactlySalaryNumber == null ) {
      params.salaryMaximum = this.maximumSalaryNumber;
      params.salaryMinimum = this.minimumSalaryNumber;
      params.salaryExactly = null;
    } else {
      params.salaryExactly = this.exactlySalaryNumber;
      params.salaryMinimum = this.exactlySalaryNumber;
    }
    if (this.formAboutVacancy.get('title')?.value.hasOwnProperty('positionId')){
      params.positionId = this.formAboutVacancy.get('title')?.value.positionId;
    }else{
      if (typeof this.formAboutVacancy.get('title')?.value === 'object'){
        params.position = {
          position: this.formAboutVacancy.get('title')?.value.position.replace('(Crear) ', '')
        };
      }
      else{
        params.position = {
          position: this.formAboutVacancy.get('title')?.value
        };
      }
    }

    return params;
  }

  createVacancy = (isDraft: boolean = false) => {
    this.setVacancyClient(this.formClients.value.clients).subscribe((clientResponse) => {
      if (clientResponse !== null && !(clientResponse.status >= 400)){
        this.formClients.get('clients')?.setValue(clientResponse);
        if (!this.currentVacancy || this.isObjectEmpty(this.currentVacancy)) {
          this.vacancyService.createVacancy(this.getParamsCreateVacancy())?.subscribe((response: any) => {
            if (response != null) {
              if (isDraft){
                this.toasService.showSuccess('Datos guardados');
              }
              else{
                if (this.activatedRoute.snapshot.queryParamMap.has('fromPreview')){
                  this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:preview)');
                }
                else{
                  this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:position)');
                }
              }
            } else {
              const message = 'Ocurrió un error al crear la vacante';
              this.toasService.showMessageError(message);
            }
          });
        }
        else {
          const fullUpdateObject = this.getParamsCreateVacancy();
          const { client, ...operationsToUpdate } = fullUpdateObject;
          operationsToUpdate.clientId = client.clientId;
          if (this.formAboutVacancy.get('title')?.value.hasOwnProperty('positionId')){
            operationsToUpdate.positionId = this.formAboutVacancy.get('title')?.value.positionId;
          }else{
            if (typeof this.formAboutVacancy.get('title')?.value === 'object'){
              operationsToUpdate.position = {
                position: this.formAboutVacancy.get('title')?.value.position.replace('(Crear) ', '')
              };
            }
            else{
              operationsToUpdate.position = {
                position: this.formAboutVacancy.get('title')?.value
              };
            }
          }
          this.vacancyService.uploadVacancyInfo(operationsToUpdate, this.currentVacancy.steps)?.subscribe((response) => {
            if (response != null) {
              Object.assign(this.currentVacancy, response);
              this.vacancyService.setVacancyInfo(this.currentVacancy);
              if (this.activatedRoute.snapshot.queryParamMap.has('fromPreview')){
                this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:preview)');
              }
              else{
                this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:position)');
              }
            } else {
              const message = 'Ocurrió un error al crear la vacante';
              this.toasService.showMessageError(message);
            }
          });
        }
      }
      else{
        this.requiresToCreateANewClient = true;
      }
    });
  }

  openModalNewClient = (client: any, status: string) => {
    if (client != null) {
      this._client.setClietTem(client);
    }
    const modalRef = this.modalService.open(ModalNewClientComponent, { size: 'lg', centered: true });
    let newClientName = '';
    if (typeof this.formClients.value.clients === 'string'){
      newClientName = this.formClients.value.clients.replace('(Crear) ', '');
    }
    else if (typeof this.formClients.value.clients === 'object'){
      newClientName = this.formClients.value.clients.name.replace('(Crear) ', '');
    }
    const data = { status, newClientName };
    modalRef.componentInstance.data = data;
    modalRef.dismissed.subscribe(
      (data) => {
        if (data){
          switch (data.status) {
            case 0:
              break;
            case 'ok':
              this.openModalMessageDiscard();
              modalRef.componentInstance.getDataTem();
              this.formClients.get('clients')?.setValue(JSON.parse(this.cookieService.get('newClientObject')));
              this.modalService.dismissAll();
              break;
            case 'nok':
              // this.showMessageError(data.message
              break;
            default:
              if (data == 0) {
                // this.flagReturnModalEdit = false;
                this.openModalMessageDiscard();
                modalRef.componentInstance.getDataTem();
              }
              break;
          }
        }
      }
    );
    modalRef.componentInstance.dataTem.subscribe((data: any) => {
      this.dataModalTem = data;
    });
  }

  openModalMessageDiscard = () => {
    const modalRef = this.modalService.open(this.modalExit, { centered: true });
  }

  /**
   * If necessary, creates a new client from the data set in the modal.
   * If not, just returns the same object
   * @param clientData Client object. Either an existing client or a new one
   * @returns An observable with an existing client
   */
  setVacancyClient(clientData: any): Observable<any> {
    if (!clientData.clientId){
      return this._client.createNewClient(clientData).pipe(map((response: any) => {
        return {
          clientId: response.clientId,
          name: response.name,
        };
      }));
    }
    else{
      return new Observable<any>((observer) => {
        observer.next(clientData);
      });
    }
  }

  getCountryByCode(countryCode: string){
    return this.country.filter((el: any) => el.iso2 === countryCode)[0];
  }

  getStateByCode(stateCode: string){
    return this.states.filter((el: any) => el.iso2 === stateCode)[0];
  }

  getMunicipalityByCode(municipalityCode: string){
    return this.municipios.filter((el: any) => el.name === municipalityCode)[0];
  }
}
