import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-modal-cerrar-vacante',
  templateUrl: './modal-cerrar-vacante.component.html',
  styleUrls: ['./modal-cerrar-vacante.component.scss']
})
export class ModalCerrarVacanteComponent implements OnInit {

  constructor(private modalService: NgbModal,
    private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
  }

  openVerticallyCentered(content:any) {
    this.modalService.open(content, { centered: true });
  }

  redirecTo(){
    this.router.navigate(['../../vacantes/activas'], {
      relativeTo: this.activatedRoute,
    });
  }
}
