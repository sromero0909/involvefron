import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCerrarVacanteComponent } from './modal-cerrar-vacante.component';

describe('ModalCerrarVacanteComponent', () => {
  let component: ModalCerrarVacanteComponent;
  let fixture: ComponentFixture<ModalCerrarVacanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalCerrarVacanteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCerrarVacanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
