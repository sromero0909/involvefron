import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CerradasComponent } from './cerradas.component';

describe('CerradasComponent', () => {
  let component: CerradasComponent;
  let fixture: ComponentFixture<CerradasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CerradasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CerradasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
