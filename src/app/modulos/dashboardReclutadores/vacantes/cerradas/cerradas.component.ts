import { Component, OnInit,QueryList,ViewChild,ViewChildren,TemplateRef } from '@angular/core';
import { VacantesService } from 'src/app/services/reclutador/vacantes/vacantes.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {NgbdSortableHeader, SortEvent} from '../../../../directives/sortable.directive';
import { PaginadorService } from 'src/app/services/reclutador/vacantes/paginador.service';
import { Observable } from 'rxjs';
import { DecimalPipe } from '@angular/common';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { VacantesCerradasService } from '../../../../services/reclutador/vacantes/vacantes.cerradas.service'
import { VacantesActivasService } from 'src/app/services/reclutador/vacantes/vacantes.activas.service';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
@Component({
  selector: 'app-cerradas',
  templateUrl: './cerradas.component.html',
  styleUrls: ['./cerradas.component.scss'],
  providers: [PaginadorService, DecimalPipe]
})
export class CerradasComponent implements OnInit {
  page = 1
  rows1: Observable<any[]>;
  rows:Array<any> =[];
  arregloOrderBy: Array<any> = []
  page_size: number = 10;
  page_number:number = 0;
  rowsMostrar: number = 0;
  pageActual: number = 0;
  pageTemporal: number = 0;
  restarRows:number = 0
  contadorChecks = 0;
  rowsPorPagina = 0
  totalElements: number = 0;
  totalPages: number = 0;
  contadorFiltroColumn = 0;

  checkAll:boolean = false;
  orderAsc:boolean = true;
  orderDesc:boolean = true;
  order:boolean = false;
  searchFlag:boolean = false;

  formularioPlantilla!:FormGroup;
  mensajeToast:string = "";
  idVacante: string = "";
  reasonId: string = "";
  filtroCliente: string = "";
  filtroReclutador: string = "";
  claseCheck:string=""
  dataPlantilla: any;
  sortDirection: string = "&sortDirection=DESC"
  orderBy: string = "&sortBy=updateDate"
  columnActive:string = "";
  textFilter: string = "";

  @ViewChild('modalPlantilla', { static: true }) cerrarVacante!:TemplateRef<any>;
  @ViewChildren(NgbdSortableHeader) headers!: QueryList<NgbdSortableHeader>;
  constructor(private _menuSeleccionado: VacantesService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    public service: PaginadorService,
    public toastService: ToastService,
    private _vacantesService: VacantesCerradasService,
    private vacantesActivasService: VacantesActivasService,
    private dasboardComponent: DashboardComponent,
    public _suscription: SuscriptionService,) { 
      this.inizializarFormularioPlantilla();
      this.rows1 = service.countries$;
    }

  ngOnInit(): void {
    this._menuSeleccionado.setMenuSeleccionado("cerradas");
    this.inicializaRows();
    this.llenarCataloOrderBy();
  }

 
  inicializaRows(){
    //**Llamamos al servicio que trae las vacantes */
    let params =''; 
    params = (!this.searchFlag) 
    ? `/vacant/admin?pageSize=${this.page_size}&pageNumber=${this.page_number}${this.orderBy}${this.sortDirection}&status=CERRADA${this.filtroCliente}${this.filtroReclutador}`
    : `/vacant/admin?pageSize=${this.page_size}&pageNumber=${this.page_number}${this.orderBy}${this.sortDirection}&status=CERRADA${this.filtroCliente}${this.filtroReclutador}${this.textFilter}`
    this.rows = [];
    this.vacantesActivasService.listarVacantesActivas(params).subscribe(
      (response: any) => {
        let data = (response != null && response.hasOwnProperty('content')) ? response.content : [];
        if (data.length > 0) {
          for (let i = 0; i < data.length; i++) {
            this.rows.push({
              check:false,
              id:data[i].vacant.vacantId,
              idshow:data[i].vacant.idShow,
              vacante: data[i].vacant.name,
              motivoCierre: data[i].vacant.reasonClose.spanishReason,
              fechaCierre: data[i].vacant.updateDate,
              reclutadores: this.generateArrayReclutadores(data[i].vacant.recruiters),
              contratados: data[i].contratado,
              plnatilla:"Convertir a plantilla",
              vacant: data[i].vacant
            });
          }
          this.totalElements = response.totalElements;
          this.totalPages = response.totalPages;
          if (this.pageActual == 0) {
            this.updateRowsMOstradas(this.pageActual + 1)
          } else {
            this.updateRowsMOstradas(this.pageActual)
          }
        }
      },
      (error) => {
        console.log("error en el servicio pausar vacante", error);
      }
    );
  }

  onchangeCheck(id:number){
    for(let i = 0; i < this.rows.length; i++){
      if(this.rows[i].id == id){
        (this.rows[i].check) ? this.contadorChecks-- : this.contadorChecks++
        this.rows[i].check = !this.rows[i].check
        
      }
    }
    if(this.contadorChecks > 0 && this.contadorChecks < this.totalElements) {this.claseCheck = "minus"; this.checkAll=true}else{this.claseCheck = ""; this.checkAll=false} 
  }

  selectAll(){
    this.checkAll  = !this.checkAll; 
    for(let i = 0; i < this.rows.length; i++){
      this.contadorChecks = (this.checkAll) ? this.rows.length :0
      this.rows[i].check = this.checkAll; 
    }
    if(this.contadorChecks > 0 && this.contadorChecks < this.totalElements) {this.claseCheck = "minus"; this.checkAll=true}else{this.claseCheck = "";} 

  }

  limpiarSeleccion(){
    for(let i = 0; i < this.rows.length; i++){
        this.contadorChecks = 0
        this.rows[i].check = false
    }
    this.checkAll = false
  }

  eliminarBorradores(){
    const vacantes:any = [];
     this.checkAll = false
     this.rows = this.rows.filter((i) =>{ 
       if(i.check){
        vacantes.push(i.id)
       }
      return !i.check
     });
    
     this._vacantesService.eliminarVacante(vacantes).subscribe(
      (response) => {
        if((response.status == 204 || response.status == 200) && response.body == null){
          this.contadorChecks = this.contadorChecks-vacantes.length;
          this.dasboardComponent.getVacantAll();
          this.resetPaginadores();
          this.inicializaRows();
          this.showSuccess("Se eliminaron la(s) vacante(s)")
        }       
       },
       (error)=>{
         this.showMessageError(error.error);
       }
     );
  }
  selectPage(page: string) {
    if(parseInt(page, 10) > this.totalPages){
      let Myelement : any
      Myelement = document.getElementById("paginationInput");
      Myelement.value = this.totalPages;
      this.page = this.totalPages
      this.rowsMostradas(this.totalPages);
    }else{
      this.page = parseInt(page, 10) || 1;
      this.rowsMostradas(parseInt(page));
    }
  }

  formatInput(input: HTMLInputElement) {
    input.value = input.value.replace(FILTER_PAG_REGEX, '');
  }

  getInicialesNombre(nombre: string) {
    let iniciales = ""
    let separador = " ",
      arregloDeSubCadenas = nombre.split(separador);

    for (let j = 0; j < arregloDeSubCadenas.length; j++) {
      const subCadena = arregloDeSubCadenas[j].substring(0, 1)
      iniciales += subCadena;
    }
    return iniciales.slice(0, 2);
  }

  openModalCrearPlatilla(data: any) {
    this.dataPlantilla = data
    this.modalService.open(this.cerrarVacante, { size:'lg', centered: true });
  }
 
  getNombrePlantilla(): void{
    const pregunta = this.formularioPlantilla?.get("inputPregunta")?.value
    this.formularioPlantilla?.reset();
  }

  inizializarFormularioPlantilla(){
    this.formularioPlantilla = this.formBuilder.group({
      inputPregunta: [{ value: '', disabled: false }, [Validators.required, Validators.minLength(5)]],
    });
  }

  onSort({ column, direction }: any) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = 'sort';
      }
    });
    
     if(direction == 'sort'){
      this.sortDirection = "&sortDirection=DESC"
      this.orderBy = "&sortBy=updateDate"
      
     }else{
      this.sortDirection = "&sortDirection="+direction.toUpperCase()
      this.orderBy = "&sortBy="+column
     }
     this.resetPaginadores();
     this.inicializaRows();
  }

  showSuccess(mensaje:any) {
    this.toastService.remove(1)
    this.toastService.show(mensaje, { classname: 'bg-primary text-light ngb-toastsBottom', delay: 5000});
  }
  setMensajeNotificacion(mensaje:string){
    this.mensajeToast = mensaje;
  }

  rowsMostradas(page: any) {
    this.page_number = parseInt(page, 10) - 1;
    if (this.pageTemporal < parseInt(page) - 1) {
      this.pageTemporal = page - 1;
    }
    this.inicializaRows();
    this.updateRowsMOstradas(page);
  }

  updateRowsMOstradas(page: any) {
    setTimeout(() => {
      let a = this.rows.slice((this.page - 1) * this.page_size, page * this.page_size)
      this.rowsPorPagina = a.length;
      if (this.pageActual <= page) {
        this.rowsMostrar = this.rowsMostrar + a.length;
        this.restarRows = a.length;
        this.pageActual = page;
      } else {
        this.rowsMostrar = this.rowsMostrar - this.restarRows;
        this.pageActual = page;
      }
    }, 100)
  }

  selecRow() {
    this.resetPaginadores();
    this.inicializaRows()
  }

  resetPaginadores() {
    this.rowsMostrar = 0;
    this.pageActual = 0;
    this.page_number = 0;
    this.restarRows = 0;
    this.pageTemporal = 0;
    this.rows = [];
    this.page = 1
  }

  generateArrayReclutadores(datos: any) {
    let reclutadores = [];
    for (let i = 0; i < datos.length; i++) {
      const nombre = datos[i].recruiter.user.name + " " + datos[i].recruiter.user.lastName + " " + datos[i].recruiter.user.secondLastName;
      reclutadores.push({
        url: (datos[i].recruiter.user.photo != null) ? datos[i].recruiter.user.photo : "",
        nombre: nombre,
        iniciales: this.getInicialesNombre(nombre),
        tipo: datos[i].type
      });
    }
    return reclutadores;
  }

  crearPlantilla(template: any) {
    const pregunta = this.formularioPlantilla?.get("inputPregunta")?.value
    this.formularioPlantilla?.reset();
    this.dataPlantilla.name = pregunta;
    this.vacantesActivasService.crearPlantilla(this.dataPlantilla).subscribe(
      (response) => {
        this.showSuccess(template);
      },
      (error) => {
        this.showMessageError(error.error.error);
      }
    );
  }

  aplicarFiltroClientes(data: any) {
    this.filtroCliente = (data != "") ? '&clientId=' + data : '';
    this.resetPaginadores();
    this.inicializaRows();
  }

  aplicarFiltroReclutador(data: any) {
    this.filtroReclutador = (data != "") ? '&recruiterId=' + data : '';
    this.resetPaginadores();
    this.inicializaRows();
  }

  llenarCataloOrderBy() {
    this.vacantesActivasService.catalogoOrderBy().subscribe(
      (response) => {
        if (response != null && response.length > 0) {
          for (let i = 0; i < response.length; i++) {
            const arrTemp = response[i][0];
            this.arregloOrderBy.push({
              nombre: arrTemp[arrTemp.length - 1],
              value: arrTemp[0]
            })
          }
        }
      },
      (error) => {
        this.showMessageError(error)
      }
    );
  }

  filterOrderBy(valor: any) {
    switch (valor) {
      case 'RECIENTES':
        this.sortDirection = "&sortDirection=DESC"
        this.orderBy = "&sortBy=updateDate"
        break;
      case 'MAS_ANTIGUAS':
        this.sortDirection = "&sortDirection=ASC"
        this.orderBy = "&sortBy=updateDate"
        break;
      case 'A_Z':
        this.sortDirection = "&sortDirection=ASC"
        this.orderBy = "&sortBy=name"
        break;
      case 'Z_A':
        this.sortDirection = "&sortDirection=DESC"
        this.orderBy = "&sortBy=name"
        break;
    }
    this.resetPaginadores();
    this.inicializaRows();
  }

  orderByColumn(column:string){
    
    this.ocultarOrderColumn();
    this.mostrarIconoOrder(column)
    switch (column) {
      case 'RECIENTES':
        this.sortDirection = "&sortDirection=DESC"
        this.orderBy = "&sortBy=updateDate"
        break;
      case 'MAS_ANTIGUAS':
        this.sortDirection = "&sortDirection=ASC"
        this.orderBy = "&sortBy=updateDate"
        break;
      case 'A_Z':
        this.sortDirection = "&sortDirection=ASC"
        this.orderBy = "&sortBy=name"
        break;
      case 'Z_A':
        this.sortDirection = "&sortDirection=DESC"
        this.orderBy = "&sortBy=name"
        break;
    }
   // this.resetPaginadores();
    //this.inicializaRows();
  }

  mostrarIconoOrder(column:string){
    if(this.columnActive == column){
      this.contadorFiltroColumn++
    }else{
      this.contadorFiltroColumn = 1;
      this.columnActive = column 
    }
    if(this.contadorFiltroColumn == 1){
      this.orderDesc = false;
      this.sortDirection = "&sortDirection=DESC"
      this.orderBy = "&sortBy="+column
    } 
    if(this.contadorFiltroColumn == 2){
      this.orderAsc = false;
      this.sortDirection = "&sortDirection=ASC"
      this.orderBy = "&sortBy="+column
    } 
    if(this.contadorFiltroColumn == 3){
      this.order = false;
      this.contadorFiltroColumn = 0;
      this.sortDirection = "&sortDirection=DESC"
      this.orderBy = "&sortBy=updateDate"
    } 
  }
  ocultarOrderColumn(){
    this.orderAsc = true;
    this.orderDesc = true;
    this.order = true;
  }
  showMessageError(dangerTpl: any) {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-light ngb-toastsBottom', delay: 3000 });
  }

  searchVacant(text:string){
    if(text.length >2){
      this.searchFlag = true;
      this.textFilter = "&textFilter="+ text
    }else{
      this.searchFlag = false;
    }
    this.resetPaginadores();
    this.inicializaRows();
  }
}

const FILTER_PAG_REGEX = /[^0-9]/g;