import { Component, Input, OnInit, ViewChild, Output, EventEmitter, TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap } from 'rxjs/operators';
import { Observable, OperatorFunction } from 'rxjs';

import { VacantesActivasService } from 'src/app/services/reclutador/vacantes/vacantes.activas.service';

@Component({
  selector: 'app-modal-reclutadores',
  templateUrl: './modal-reclutadores.component.html',
  styleUrls: ['./modal-reclutadores.component.scss']
})
export class ModalReclutadoresComponent implements OnInit {
  formularioMasReclutadores: FormGroup;
  public modelReclutadores: any;
  @Input() arregloReclutadores: Array<any> = []
  @Input() ocultar: boolean = false;
  @Input() numReclutadores: number = 0;
  @Input() idVacante!: string;
  @Input() permission!: boolean
  mostrarBtn: boolean = false;
  @Output() propagar = new EventEmitter<string>();
  @Output() propagaRecruiter = new EventEmitter<string>();
  newArregloReclutadores: Array<any> = []
  bandera: boolean = false;
  banderaMostrar: boolean = false;
  banderaAgregado: boolean = false;
  mensaje1 = false;
  mensaje2 = false;
  reclutadoresAuxiliarres: number = 0
  searching = false;
  searchFailed = false;

 

  @ViewChild('longContent', { static: true }) longContent!: TemplateRef<any>;
  search: OperatorFunction<string, readonly { name: string, flag: string }[]> = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(600),
      distinctUntilChanged(),
      tap(() => this.searching = true),
      switchMap(term => term.length < 3 ? [] :
        this.vacantesActivasService.atuCompletadoReclutador(term).pipe(
          map((response: any) => {
            if (response != undefined && response != null && response.status == 200 && response.body != null && response.body.length > 0) {
              reclutadores = [];
              for (let i = 0; i < response.body.length; i++) {
                reclutadores.push({
                  name: response.body[i].user.name + ' ' + response.body[i].user.lastName + ' ' + response.body[i].user.secondLastName,
                  flag: (response.body[i].user.photo != null) ? response.body[i].user.photo : '../../../../assets/img/dashboard/vacantes/sinFotoPerfil.svg',
                  id: response.body[i].recruiterId
                }
                )
              }
            }
            return term;
          }),
          map(term => (term.length < 2 && reclutadores != undefined && reclutadores != null && reclutadores != []) ? []
            : reclutadores.filter((v: any) => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
        )
      ),
      tap(() => this.searching = false)
    )

  formatter = (x: { name: string }) => x.name;
  constructor(private modalService: NgbModal, private formBuilder: FormBuilder,
    private vacantesActivasService: VacantesActivasService,
    //public activasComponent : VacantesComponent
    ) {
    this.formularioMasReclutadores = this.formBuilder.group({
      modelReclutador: [{ value: '', disabled: false }, [Validators.minLength(5)]],
    });
  }

  ngOnInit(): void {
    this.mostrarBtn = this.ocultar
    this.reclutadoresAuxiliarres = (this.arregloReclutadores.length > 2) ? this.arregloReclutadores.length - 2 : 0
    this.newArregloReclutadores = this.arregloReclutadores;
  }
  

  addReclutadorAuxiliar() {
    const texto = this.formularioMasReclutadores.get('modelReclutador')?.value
    const existeReclutador = this.validarReclutador(texto);
    if (texto != null && texto != '' && existeReclutador) {
      this.bandera = false;
      this.mensaje1 = false;
      this.reclutadorAgregado(texto);
      if (!this.banderaAgregado) {
        const newAssistants = this.newArregloReclutadores
          .filter((recruiter) => recruiter.tipo !== 'ENCARGADO')
          .map((recruiter) => {
            return {
              assistant: true,
              notifications: true,
              recruiterId: recruiter.recruiterId,
              type: 'AUXILIAR'
            };
          }).concat([{
            assistant: true,
            notifications: true,
            recruiterId: texto.id,
            type: 'AUXILIAR'
          }]);
        this.vacantesActivasService.agregarReclutador(this.idVacante, {recruiters: newAssistants}).subscribe(
          (response) => {
            if (response != undefined && response != null) {
              this.propagaRecruiter.emit('true');
              this.changeRecruites();
              this.permission = true;
              this.newArregloReclutadores.push({
                iniciales: this.getInicialesNombre(texto.name),
                nombre: texto.name,
                tipo: 'AUXILIAR',
                url: (texto.flag.includes('sinFotoPerfil')) ? '' : texto.flag,
                vacantId:this.idVacante,
                recruiterId:texto.id
              })
              this.formularioMasReclutadores.get('modelReclutador')?.reset();

              this.banderaAgregado = false;
              this.mensaje1 = false
            }
          }, (error) => {

          }
        );
      }

    }
  }

   changeRecruites() {
     this.vacantesActivasService.setRecruiter$('true');
   }

  validarReclutador(nombreReclutador: any) {
    if (nombreReclutador != null && nombreReclutador != '') {
      for (let i = 0; i < reclutadores.length; i++) {
        if (reclutadores[i].name == nombreReclutador.name) {
          this.bandera = true;
          this.mensaje1 = true
        }
      }
      if (reclutadores.length == 0) {
        this.bandera = false;
        this.banderaMostrar = true
        this.mensaje1 = true
        this, this.mensaje2 = false
      }
    } else {
      this.bandera = true;
    }
    return this.bandera;
  }

  reclutadorAgregado(nombreReclutador: any) {
    if (nombreReclutador != null && nombreReclutador != '') {
      for (let i = 0; i < this.newArregloReclutadores.length; i++) {
        if (this.newArregloReclutadores[i].nombre == nombreReclutador.name) {
          this.banderaAgregado = true;
          setTimeout(() => {
            this.banderaAgregado = false;
          }, 8000);
          this.mensaje2 = true
        }
      }
    }
    return this.bandera;
  }
  removeReclutador(indice: number) {
    this.newArregloReclutadores.splice(indice, 1);
  }
  openScrollableContent(longContent: any) {
    this.modalService.open(longContent, { centered: true });
  }

  getInicialesNombre(nombre: string) {
    let iniciales = ''
    let separador = ' ',
      arregloDeSubCadenas = nombre.split(separador);

    for (let j = 0; j < arregloDeSubCadenas.length; j++) {
      const subCadena = arregloDeSubCadenas[j].substring(0, 1)
      iniciales += subCadena;
    }
    return iniciales.slice(0, 2).toUpperCase();
  }

  searchReclutador(query: string) {
    reclutadores = []
    this.vacantesActivasService.atuCompletadoReclutador(query).subscribe(
      (response) => {
        if (response.data.length > 0) {
          for (let i = 0; i < response.data.length; i++) {
            reclutadores.push({
              name: response.data[i].user.name + ' ' + response.data[i].user.lastName + ' ' + response.data[i].user.secondLastName,
              flag: (response.data[i].user.photo != null) ? response.data[i].user.photo : '../../../../assets/img/dashboard/vacantes/sinFotoPerfil.svg',
              id: response.data[i].recruiterId
            }
            )
          }
        }
      },
      (error) => {
        console.log('error en el servicio', error);
      }
    );
  }

  removeRecruiter(idVacant: string, idRecruiter: string, indice: number) {

    this.vacantesActivasService.removeRecruiter(idVacant, idRecruiter).subscribe(
      (response) => {
        if (response) {
          this.propagaRecruiter.emit('true');
          this.changeRecruites();
          this.removeReclutador(indice);
        }
      },
      (error) => {
        console.log('error en el servicio', error);
      }
    );
  }

}

let reclutadores: { name: string, flag: string, id: string }[] = []