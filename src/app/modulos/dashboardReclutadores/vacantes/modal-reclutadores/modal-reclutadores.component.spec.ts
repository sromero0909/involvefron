import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalReclutadoresComponent } from './modal-reclutadores.component';

describe('ModalReclutadoresComponent', () => {
  let component: ModalReclutadoresComponent;
  let fixture: ComponentFixture<ModalReclutadoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalReclutadoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalReclutadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
