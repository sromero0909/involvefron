import { Component, OnInit,ViewChild,TemplateRef,ViewContainerRef } from '@angular/core';
import { VacantesService } from 'src/app/services/reclutador/vacantes/vacantes.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VacantesActivasService } from 'src/app/services/reclutador/vacantes/vacantes.activas.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import {formatDate} from '@angular/common';

import { DashboardComponent } from '../../dashboard/dashboard.component'

import * as moment from 'moment';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-enpausa',
  templateUrl: './enpausa.component.html',
  styleUrls: ['./enpausa.component.scss']
})
export class EnpausaComponent implements OnInit {
  formularioCerrarVacante!: FormGroup;
  formularioPublicar!: FormGroup;
  nuevaVacante:boolean = false;
  datosPausarVacanteValid = false;

  @ViewChild('longContent', { static: true }) sayHelloTemplate!:TemplateRef<any>;
  @ViewChild('publicarVacante', { static: true }) publicarVacante!:TemplateRef<any>;
  @ViewChild('confirmacionCerrar', { static: true }) confirmacionCerrar!:TemplateRef<any>;
  @ViewChild('cerrarVacante', { static: true }) cerrarVacante!:TemplateRef<any>;
  @ViewChild('iconoChekBlanco', { static: true }) iconoChekBlanco!: TemplateRef<any>;

  page = 1
  rows:Array<any> =[]
  motivosCerrarVacante: Array<any> = []
  motivosPausarVacante: Array<any> = []
  arregloOrderBy: Array<any> = []
  page_size: number = 5;
  page_number:number = 0;
  totalElements: number = 0;
  totalPages: number = 0;
  rowsMostrar: number = 0;
  pageActual: number = 0;
  pageTemporal: number = 0;
  pageUpdate: number = 0;
  restarRows: number = 0;
  contadorPaginas: number = 0;
  rowsPorPagina = 0;
  mensajeToast: string = "";
  filtroCliente: string = "";
  filtroReclutador: string = "";
  idVacante: string = "";
  reasonId: string = "";
  textFilter: string = "";
  indiceRow: number = 0;
  sortDirection: string = "&sortDirection=DESC"
  orderBy: string = "&sortBy=createDate"
  searchFlag:boolean = false;
  otroMotivo = '';
  constructor(private _menuSeleccionado: VacantesService,private modalService: NgbModal,
    private vacantesActivasService: VacantesActivasService,
    public toastService: ToastService,
    private formBuilder: FormBuilder,
    private dasboardComponent: DashboardComponent,
    public _suscription: SuscriptionService,
    private vacancyService: VacancyService,
    private router: Router,) { 
    this.inicializarFormularioCerrarVacante();
    this.inicializarPublicarVacante();
  }

  ngOnInit(): void {
    this._menuSeleccionado.setMenuSeleccionado("enpausa");
    this.inicializaRows();
    this.llenarCataloOrderBy();
  }

  
  openScrollableContent() {
    this.modalService.open(this.sayHelloTemplate, { centered: true });
  }
  openConfirmacionPausa(idVacant:string,indice:number){
    console.log("es id",idVacant);
    this.idVacante = idVacant;
    this.indiceRow = indice
    this.modalService.open(this.publicarVacante, { size:'lg',centered: true });
  }

  inicializaRows() {

    //**Llamamos al servicio que trae las vacantes */
    let params =''; 
   
    params = (!this.searchFlag) 
    ? `/vacant/admin?pageSize=${this.page_size}&pageNumber=${this.page_number}${this.orderBy}${this.sortDirection}&status=PAUSADA${this.filtroCliente}${this.filtroReclutador}`
    : `/vacant/admin?pageSize=${this.page_size}&pageNumber=${this.page_number}${this.orderBy}${this.sortDirection}&status=PAUSADA${this.filtroCliente}${this.filtroReclutador}${this.textFilter}`
    this.rows = [];
    this.vacantesActivasService.listarVacantesActivas(params).subscribe(
      (response: any) => {
        let data = (response != null && response.hasOwnProperty('content')) ? response.content : [];
        if (data.length > 0) {
            for (let pausa of data ) {
              let name =  ( typeof pausa.vacant.recruiters[0]?.recruiter.user?.name === 'undefined') ? 'Sin asignacion': pausa.vacant.recruiters[0]?.recruiter.user?.name;
              let lastName =  ( typeof pausa.vacant.recruiters[0]?.recruiter.user?.lastName === 'undefined') ? '': pausa.vacant.recruiters[0]?.recruiter.user?.lastName;
        
              const fecha1 = moment(pausa.vacant.updateDate);
              const fecha2 = moment();
              const dias = fecha2.diff(fecha1, 'days');
              const tiempo = (dias === 0)? 'Pausado hoy':  (dias === 1)? `Pausado hace ${dias} día` :`Pausado hace ${dias} días`  ;
             
            this.rows.push({
              idVacante: pausa.vacant.vacantId,
              nombreProyecto: pausa.vacant.name,
              status: "En procesos de publicación",
              cliente: pausa.vacant.client.name,
              tiempo: (pausa.vacant.publicationDate == null) ? `${tiempo}` : moment(pausa.vacant.updateDate).format('LL'),
              confidencialidad: pausa.vacant.confidential,
              motivoPausa: (pausa.vacant.reasonPause != null) ? pausa.vacant.reasonPause.spanishReason : '',
              colorMotivo: (pausa.vacant.reasonPause != null ) ? this.getColorMotivo(pausa.vacant.reasonPause.spanishReason) :'defaultColor',
              candidatos: {
                nuevos: pausa.nuevo,
                videos: pausa.video,
                entrevista: pausa.entrevista,
                finalistas: pausa.finalista,
                contradatos: pausa.contratado
              },
              reclutadores: this.generateArrayReclutadores(pausa.vacant.recruiters),
              vacant: pausa.vacant,
              permission: pausa.permission
            });
          }
          this.totalElements = response.totalElements;
          this.totalPages = response.totalPages;
          this.pageUpdate = response.pageable.pageNumber;
          if (this.pageActual == 0) {
            this.updateRowsMOstradas(this.pageActual + 1)
          } else {
            this.updateRowsMOstradas(this.pageActual)
          }
        }
      },
      (error) => {
        console.log("error en el servicio pausar vacante", error.error);
      }

    );

  }

  generateArrayReclutadores(datos: any) {
    let reclutadores = [];
    for (let i = 0; i < datos.length; i++) {
      const nombre = datos[i].recruiter.user.name + " " + datos[i].recruiter.user.lastName + " " + datos[i].recruiter.user.secondLastName;
      reclutadores.push({
        url: (datos[i].recruiter.user.photo != null) ? datos[i].recruiter.user.photo : "",
        nombre: nombre,
        iniciales: this.getInicialesNombre(nombre),
        tipo: datos[i].type,
        recruiterId:datos[i].recruiterId,
        vacantId:datos[i].vacantId
      });
    }
    return reclutadores;
  }

  getColorMotivo(reason:string): string{
    let color = 'bluecolor';
    
    if(String(reason).toUpperCase().includes('PRESUPUESTO') || String(reason).toUpperCase().includes('SOLICITUD')){
      color = 'redcolor'
    }

    if(String(reason).toUpperCase().includes('PERIODO') || String(reason).toUpperCase().includes('PERFIL')){
      color = 'bluecolor'
    }

    return color;
  }
  cerrarPopUpNuevaVentana(){
    this.nuevaVacante = false;
  }

  getInicialesNombre(nombre: string) {
    let iniciales = ""
    let separador = " ",
      arregloDeSubCadenas = nombre.split(separador);

    for (let j = 0; j < arregloDeSubCadenas.length; j++) {
      const subCadena = arregloDeSubCadenas[j].substring(0, 1)
      iniciales += subCadena;
    }
    return iniciales.slice(0, 2);
  }

  getPageSymbol(current: number) {
    return ['A', 'B', 'C', 'D', 'E', 'F', 'G'][current - 1];
  }

  selectPage(page: string) {
    if(parseInt(page, 10) > this.totalPages){
      let Myelement : any
      Myelement = document.getElementById("paginationInput");
      Myelement.value = this.totalPages;
      this.page = this.totalPages
      this.rowsMostradas(this.totalPages);
    }else{
      this.page = parseInt(page, 10) || 1;
      this.rowsMostradas(parseInt(page));
    }
  }

  formatInput(input: HTMLInputElement) {
    input.value = input.value.replace(FILTER_PAG_REGEX, '');
  }


  inicializarPublicarVacante(){
    this.formularioPublicar = this.formBuilder.group({
      nombreVacante: [{ value: '', disabled: false }, [Validators.required]], 
    });
  }

  inicializarFormularioCerrarVacante(){
    this.formularioCerrarVacante = this.formBuilder.group({
      presupuesto: [{ value: false, disabled: false }, []],
      solicitud: [{ value: false, disabled: false }, []],
      candidato:  [{ value: false, disabled: false }, []],
      otroCheck: [{ value: false, disabled: false }, []],
      otroText:  [{ value: "", disabled: false }, []],
      noMostrar:  [{ value: "", disabled: false }, []],
    });
    
   // this.formularioCerrarVacante.controls['presupuesto'].setValue(true);
  }

  validarChecksPausarVacante(check:string) {
    const presupuesto = this.formularioCerrarVacante.get('presupuesto')?.value
    const solicitud = this.formularioCerrarVacante.get('solicitud')?.value
    const candidato = this.formularioCerrarVacante.get('candidato')?.value
    const otroCheck = this.formularioCerrarVacante.get('otroCheck')?.value
    
        this.resetChecksPausarVacante();
        switch (check) {
          case 'presupuesto':
            this.formularioCerrarVacante.controls['presupuesto'].setValue(presupuesto);
            break;
          case 'solicitud':
            this.formularioCerrarVacante.controls['solicitud'].setValue(solicitud);
            break;
          case 'candidato':
            this.formularioCerrarVacante.controls['candidato'].setValue(candidato);
            break;
          case 'otroCheck':
            this.formularioCerrarVacante.controls['otroCheck'].setValue(otroCheck);
            break;
    }
    this.datosPausarVacanteValid = (presupuesto || solicitud || candidato || otroCheck) ? true : false
    if (check != "otroCheck") { this.getReasonId(check) } else { this.reasonId = "" }
  }

  resetChecksPausarVacante(){
    this.formularioCerrarVacante.controls['presupuesto'].setValue(false);
    this.formularioCerrarVacante.controls['solicitud'].setValue(false);
    this.formularioCerrarVacante.controls['candidato'].setValue(false);
    this.formularioCerrarVacante.controls['otroCheck'].setValue(false);
    this.formularioCerrarVacante.controls['otroText'].setValue('');
  }

  getReasonId(reason: string) {
      for (let i = 0; i < this.motivosCerrarVacante.length; i++) {
        if (reason == this.motivosCerrarVacante[i].nombreFormulario) {
          this.reasonId = this.motivosCerrarVacante[i].reasonId
        }
      }
  }

  siguientePausarVacante(){
    this.otroMotivo = this.formularioCerrarVacante.get('otroText')?.value;
    if(this.datosPausarVacanteValid){
      this.modalService.dismissAll();
      // this.openConfirmacionCerrar()
      this.cerrarVacanteService(this.iconoChekBlanco);
      this.resetChecksPausarVacante();
    }
  }

  openConfirmacionCerrar(){
    this.modalService.open(this.confirmacionCerrar, { centered: true });
  }
  openScrollableCerrar(bandera:boolean,idVacante: string, indice: number) {
    this.idVacante = idVacante
    this.indiceRow = indice;
    this.catalogReasonsClose();
    this.modalService.open(this.cerrarVacante, { centered: true });
  }

  showSuccess(mensaje: any) {
    this.toastService.remove(1)
    this.toastService.show(mensaje, { classname: 'bg-success text-light ngb-toastsBottom', delay: 5000 });
  }

  showMessageError(dangerTpl: any) {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-light ngb-toastsBottom', delay: 5000 });
  }

  setMensajeNotificacion(mensaje: string) {
    this.mensajeToast = mensaje;
  }

  rowsMostradas(page: any) {
    this.page_number = parseInt(page, 10) - 1;
    if (this.pageTemporal < parseInt(page) - 1) {
      this.pageTemporal = page - 1;
    }
    this.inicializaRows();
    this.updateRowsMOstradas(page);
  }

  updateRowsMOstradas(page: any) {
    setTimeout(() => {
      let a = this.rows.slice((this.page - 1) * this.page_size, page * this.page_size)
      this.rowsPorPagina = a.length;
      if (this.pageActual <= page) {
        this.rowsMostrar = this.rowsMostrar + a.length;
        this.restarRows = a.length;
        this.pageActual = page;
      } else {
        this.rowsMostrar = this.rowsMostrar - this.restarRows;
        this.pageActual = page;
      }
    }, 100)
  }

  selecRow() {
    this.resetPaginadores();
    this.inicializaRows()
  }

  resetPaginadores() {
    this.rowsMostrar = 0;
    this.pageActual = 0;
    this.page_number = 0;
    this.restarRows = 0;
    this.pageTemporal = 0;
    this.rows = [];
    this.page = 1
  }

  llenarCataloOrderBy() {
    this.vacantesActivasService.catalogoOrderBy().subscribe(
      (response) => {
        if (response != null && response.length > 0) {
          for (let i = 0; i < response.length; i++) {
            const arrTemp = response[i][0];
            this.arregloOrderBy.push({
              nombre: arrTemp[arrTemp.length - 1],
              value: arrTemp[0]
            })
          }
        }
      },
      (error) => {
        this.showMessageError(error.error)
      }
    );
  }

  filterOrderBy(valor: any) {
    switch (valor) {
      case 'RECIENTES':
        this.sortDirection = "&sortDirection=DESC"
        this.orderBy = "&sortBy=createDate"
        break;
      case 'MAS_ANTIGUAS':
        this.sortDirection = "&sortDirection=ASC"
        this.orderBy = "&sortBy=createDate"
        break;
      case 'A_Z':
        this.sortDirection = "&sortDirection=ASC"
        this.orderBy = "&sortBy=name"
        break;
      case 'Z_A':
        this.sortDirection = "&sortDirection=DESC"
        this.orderBy = "&sortBy=name"
        break;
    }
    this.resetPaginadores();
    this.inicializaRows();
  }

  catalogReasonsClose() {
    let nombresFormularios = ['presupuesto', 'solicitud', 'candidato']
    let data = []
    this.vacantesActivasService.getMotivosCerrar().subscribe(
      (response) => {

        for (let i = 0; i < response.length; i++) {
          response[i]['nombreFormulario'] = nombresFormularios[i]
        }
        this.motivosCerrarVacante = response
      },
      (error) => {
        this.showMessageError(error.error)
      }
    );
  }

  cerrarVacanteService(template: any) {
    let body = {}

    body = (this.reasonId != "") ? { reasonId: this.reasonId } : { "spanishReason": this.otroMotivo }
    this.vacantesActivasService.cerrarVacante(this.idVacante, body).subscribe(
      (response) => {
        this.removeRow();
        this.dasboardComponent.getVacantAll();
        this.resetPaginadores();
        this.inicializaRows();
        this.setMensajeNotificacion('Se cerró la vacante');
        this.showSuccess(template)
      },
      (error) => {
        this.showMessageError(error.error)
      }
    );
  }

  removeRow() {
    this.rows.splice(this.indiceRow, 1);
  }

  aplicarFiltroClientes(data: any) {
    this.filtroCliente = (data != "") ? '&clientId=' + data : '';
    this.resetPaginadores();
    this.inicializaRows();
  }

  aplicarFiltroReclutador(data: any) {
    this.filtroReclutador = (data != "") ? '&recruiterId=' + data : '';
    this.resetPaginadores();
    this.inicializaRows();
  }

  searchVacant(text:string){
    if(text.length >2){
      this.searchFlag = true;
      this.textFilter = "&textFilter="+ text
    }else{
      this.searchFlag = false;
    }
    this.resetPaginadores();
    this.inicializaRows();
  }

  postVacancy(idVacante: string){
    let fechaHoy = formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss', 'en_US')
    const body = [
      {
        "op":"replace",
        "path":"/updateDate",
        "value":fechaHoy
     },
     {
      "op":"replace",
      "path":"/publicationDate",
      "value":fechaHoy
      },

      {
        "op":"replace",
        "path":"/status",
        "value":"ACTIVA"
     }
    ];
    this.vacantesActivasService.activateVacancy(idVacante,body).subscribe(
      async (response) => {
        if(response.status == 200){
          this.dasboardComponent.getVacantAll();
          this.resetPaginadores();
          this.inicializaRows();
          this.mensajeToast = "Se publicó la vacante";
          this.showSuccess(this.iconoChekBlanco);
          this.nuevaVacante = true;
          this.router.navigateByUrl('/dashboard/vacantes');
        }
      },
      (error) => {

      }
    );
  }
}

const FILTER_PAG_REGEX = /[^0-9]/g;