import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnpausaComponent } from './enpausa.component';

describe('EnpausaComponent', () => {
  let component: EnpausaComponent;
  let fixture: ComponentFixture<EnpausaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnpausaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnpausaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
