import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalExpirationComponent } from './modal-expiration.component';

describe('ModalExpirationComponent', () => {
  let component: ModalExpirationComponent;
  let fixture: ComponentFixture<ModalExpirationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalExpirationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalExpirationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
