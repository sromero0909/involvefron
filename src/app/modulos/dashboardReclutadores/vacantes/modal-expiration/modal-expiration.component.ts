import { Component, OnInit, Output,EventEmitter, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  
  selector: 'app-modal-expiration',
  templateUrl: './modal-expiration.component.html',
  styleUrls: ['./modal-expiration.component.scss']
})
export class ModalExpirationComponent implements OnInit {

  @Output() dataTem: EventEmitter<any> = new EventEmitter();
  @Input() newUser:boolean = false;
  slider1: boolean = true;
  slider2: boolean = false;
  slider3: boolean = false;
  constructor(private modalService: NgbModal
    ) { }

  ngOnInit(): void {
  }

  cambio = (e: any) => {
    console.log('cambio algo', e)
    this.resetSlider();
    switch (e.current) {
      case 'ngb-slide-0':
        this.slider1 = true;
        break;
      case 'ngb-slide-1':
        this.slider2 = true;
        break;
      case 'ngb-slide-2':
        this.slider3 = true;
        break;
    }
  }

  resetSlider = () => {
    this.slider1 = false;
    this.slider2 = false;
    this.slider3 = false;
  }

  deleteSuscription = () => {
    this.dataTem.emit('execute');
  }

  close = () => {
    this.modalService.dismissAll();
  }
}
