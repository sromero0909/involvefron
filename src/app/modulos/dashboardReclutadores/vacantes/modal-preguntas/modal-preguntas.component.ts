import { Component, Input, OnInit, ViewChild, Output, EventEmitter, Version } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { RequisitosComponent } from '../pasos-crearvacante/requisitos/requisitos.component';
import { StepperService } from 'src/app/services/reclutador/commons/stepper.service';
import { isInteger } from '@ng-bootstrap/ng-bootstrap/util/util';
import { PruebasVideoService } from 'src/app/services/reclutador/vacantes/pruebas-video.service';

@Component({
  selector: 'app-modal-preguntas',
  templateUrl: './modal-preguntas.component.html',
  styleUrls: ['./modal-preguntas.component.scss']
})
export class ModalPreguntasComponent implements OnInit {

  @Input() tipoPregunta!: string;
  @Input() version = 1;
  @Input() numPreguntas!: number;
  @Input() isDisabled = false;
  pregunta = '';
  numPregunta = 0;
  formularioGuardarPreguntas: FormGroup;
  @Output() propagar = new EventEmitter<any>();

  constructor(private formBuilder: FormBuilder, private modalService: NgbModal, stepperService: StepperService,
              private _pruebasVideoService: PruebasVideoService) {
    this.formularioGuardarPreguntas = this.formBuilder.group({
      inputPregunta: [{ value: '', disabled: false }, [Validators.required, Validators.minLength(5)]],
    });
  }

  ngOnInit(): void {
    this.pregunta = this.tipoPregunta;
    this.numPregunta = this.numPreguntas;
    console.log('es el tipo de pregunta', this.pregunta, this.numPregunta);
  }

  openLg(content: any) {
    if (this.pregunta == 'video') {
      if (this._pruebasVideoService.getContadorPreuntas() < 4) {
        this.modalService.open(content, { size: 'lg', centered: true });
      }
    } else {
      this.modalService.open(content, { size: 'lg', centered: true });
    }
  }

  getPregunta(): void {
    let pregunta;
    if (this.version == 2) {
      pregunta = {
        pregunta: this.formularioGuardarPreguntas?.get('inputPregunta')?.value.replace('  ', ' ').replace('.', ''),
        typeQuestion: this.tipoPregunta
      };
    } else {
      pregunta = this.formularioGuardarPreguntas?.get('inputPregunta')?.value.replace('  ', ' ').replace('.', '');
    }
    this.propagar.emit(pregunta);
    this.formularioGuardarPreguntas?.get('inputPregunta')?.reset();
  }

}
