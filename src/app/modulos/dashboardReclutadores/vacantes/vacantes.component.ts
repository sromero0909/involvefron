import { Component, OnInit, ViewChild, TemplateRef, ViewContainerRef } from '@angular/core';
import { VacantesService } from 'src/app/services/reclutador/vacantes/vacantes.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { ActivatedRoute, Router } from '@angular/router';
import { VacantesActivasService } from 'src/app/services/reclutador/vacantes/vacantes.activas.service';
import { AuthService } from 'src/app/services/auth.service';
import { DashboardComponent } from '../dashboard/dashboard.component';

import * as moment from 'moment';
import { CandidatesService } from 'src/app/services/reclutador/vacantes/selectionProcess/candidatos.service';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { ModalReactivationComponent } from './modal-reactivation/modal-reactivation.component';
import { ModalExpirationComponent } from './modal-expiration/modal-expiration.component';

import { Observable, OperatorFunction } from 'rxjs';
import { Subscription } from 'rxjs';
import { SuscriptionCanceledService } from 'src/app/services/reclutador/login/subscription-canceled.service';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';
import { ProfileService } from 'src/app/services/reclutador/configuration/profile.service';
import { SideNavService } from 'src/app/services/reclutador/vacantes/sideNav.service';

declare var gtag: any;
@Component({
  selector: 'app-vacantes',
  templateUrl: './vacantes.component.html',
  styleUrls: ['./vacantes.component.scss']
})
export class VacantesComponent implements OnInit {
  vacantActiva: any[] = [];
  formularioCerrarVacante!: FormGroup;
  formularioPlantilla!: FormGroup;
  nuevaVacante = true;
  page = 1;
  rows: Array<any> = [];
  motivosCerrarVacante: Array<any> = [];
  motivosPausarVacante: Array<any> = [];
  arregloOrderBy: Array<any> = [];
  reclutadoresModal: Array<any> = [];
  page_size = 5;
  page_number = 0;
  totalElements = 0;
  totalPages = 0;
  rowsMostrar = 0;
  pageActual = 0;
  pageTemporal = 0;
  restarRows = 0;
  contadorPaginas = 0;
  rowsPorPagina = 0;
  notificacionActiva = true;
  datosPausarVacanteValid = false;
  banderaModales = false;
  banderaNotificacion = true;
  banderaModalPausa = false;
  searchFlag = false;
  indiceRow = 0;
  mensajeToast = '';
  idVacante = '';
  reasonId = '';
  filtroCliente = '';
  filtroReclutador = '';
  otroMotivo = '';
  dataPlantilla: any;
  sortDirection = '&sortDirection=DESC';
  orderBy = '&sortBy=publicationDate';
  textFilter = '';
  dateExpiration = this._suscriptionCanceled.getDateExpiration(); // '2021-11-11';//
  nextExpirationDate: string;

  private subscriptions!: Subscription;
  recruiters$!: Observable<any[]>;

  @ViewChild('longContent', { static: true }) sayHelloTemplate!: TemplateRef<any>;
  @ViewChild('confirmacionPausa', { static: true }) confirmacionPausa!: TemplateRef<any>;
  @ViewChild('cerrarVacante', { static: true }) cerrarVacante!: TemplateRef<any>;
  @ViewChild('confirmacionCerrar', { static: true }) confirmacionCerrar!: TemplateRef<any>;
  @ViewChild('modalNombrePlantilla', { static: true }) modalNombrePlantilla!: TemplateRef<any>;
  @ViewChild('modalPaymentRenovation', { static: true }) modalPaymentRenovation!: TemplateRef<any>;
  @ViewChild('modalAboutToExpire', { static: true }) modalAboutToExpire!: TemplateRef<any>;
  @ViewChild('iconoChekBlanco', { static: true }) iconoChekBlanco!: TemplateRef<any>;
  constructor(private _menuSeleccionado: VacantesService, private modalService: NgbModal,
              private formBuilder: FormBuilder,
              public toastService: ToastService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private vacantesActivasService: VacantesActivasService,
              private authService: AuthService,
              private candidateService: CandidatesService,
              private dasboardComponent: DashboardComponent,
              public _suscription: SuscriptionService,
              private _suscriptionCanceled: SuscriptionCanceledService,
              private vacancyService: VacancyService,
              private profileService: ProfileService,
              private sideNavService: SideNavService) {
    this.getVacantAll();
    this.validateExpiration();
    this.inicializarFormularioCerrarVacante();
    this.inizializarFormularioPlantilla();
  }

  ngOnInit(): void {
    this.getCurrentDate();
    if (this._suscription.getEditPlan()){
      this.modalService.open(ModalReactivationComponent, { centered: true });
      this._suscription.setEditPlan(false);
    }
    this._menuSeleccionado.setMenuSeleccionado('activas');
    this.nuevaVacante = this._menuSeleccionado.getNuevaVacante();
    this._menuSeleccionado.setNuevaVacante(false);
    this.inicializaRows();
    this.llenarCataloOrderBy();
    const reclutadorActivo = this.authService.getRecruiterId();
    this.changeRecruites();
  }

  ngOnDestroy() {
    this.subscriptions?.unsubscribe();
  }

  openScrollableContent(bandera: boolean, idVacante: string, indice: number, reclutadores: []) {
    this.banderaModales = bandera;
    this.idVacante = idVacante;
    this.indiceRow = indice;
    this.getCatalogoMotivosPausar();
    this.modalService.open(this.sayHelloTemplate, { centered: true });
    this.idVacante = idVacante;
    this.validarModalPausa(reclutadores);
  }
  openScrollableCerrar(bandera: boolean, idVacante: string, indice: number) {
    this.idVacante = idVacante;
    this.indiceRow = indice;
    this.banderaModales = bandera;
    this.modalService.open(this.cerrarVacante, { centered: true });
    if (bandera) {
      this.getCatalogoMotivosCerrar();
    }
  }

  openConfirmacionCerrar() {
    this.modalService.open(this.confirmacionCerrar, { centered: true });
  }

  openNombrePlantilla(data: any) {
    this.dataPlantilla = data;
    this.modalService.open(this.modalNombrePlantilla, { size: 'lg', centered: true });
  }

  showSuccess(mensaje: any) {
    this.toastService.remove(1);
    this.toastService.show(mensaje, { classname: 'bg-success text-light ngb-toastsBottom', delay: 5000 });
  }
  inicializaRows() {

    // **Llamamos al servicio que trae las vacantes */
    let params = '';

    params = (!this.searchFlag)
      ? `/vacant/admin?pageSize=${this.page_size}&pageNumber=${this.page_number}${this.orderBy}${this.sortDirection}&status=ACTIVA${this.filtroCliente}${this.filtroReclutador}`
      : `/vacant/admin?pageSize=${this.page_size}&pageNumber=${this.page_number}${this.orderBy}${this.sortDirection}&status=ACTIVA${this.filtroCliente}${this.filtroReclutador}${this.textFilter}`;
    this.rows = [];
    this.vacantesActivasService.listarVacantesActivas(params).subscribe(
      (response: any) => {
        const data = (response != null && response.hasOwnProperty('content')) ? response.content : [];
        if (data.length > 0) {
          for (const activa of data) {
            const name = (typeof activa.vacant.recruiters[0]?.recruiter.user?.name === 'undefined') ? 'Sin asignacion' : activa.vacant.recruiters[0]?.recruiter.user?.name;
            const lastName = (typeof activa.vacant.recruiters[0]?.recruiter.user?.lastName === 'undefined') ? '' : activa.vacant.recruiters[0]?.recruiter.user?.lastName;

            const fecha1 = moment(activa.vacant.createDate);
            const fecha2 = moment();
            const dias = fecha2.diff(fecha1, 'days');
            const tiempo = (dias === 0) ? 'Guardado hoy' : (dias === 1) ? `Guardado hace ${dias} día` : `Guardado hace ${dias} días`;
            this.rows.push({
              idVacante: activa.vacant.vacantId,
              nombreProyecto: activa.vacant.name,
              status: 'Anuncio publicado',
              cliente: activa.vacant.client.name,
              tiempo: (activa.vacant.publicationDate == null) ? `${tiempo}` : moment(activa.vacant.publicationDate).format('LL'),
              confidencialidad: activa.vacant.confidential,
              candidatos: {
                nuevos: activa.nuevo,
                videos: activa.video,
                entrevista: activa.entrevista,
                finalistas: activa.finalista,
                contradatos: activa.contratado
              },
              reclutadores: this.generateArrayReclutadores(activa.vacant.recruiters),
              vacant: activa.vacant,
              permission: activa.permission
            });

            if (!this.validarVacantesConfidenciales(activa.vacant.recruiters) && activa.vacant.confidential) {
              this.rows.pop();
            }
          }
          this.totalElements = response.totalElements;
          this.totalPages = response.totalPages;
          if (this.pageActual == 0) {
            this.updateRowsMOstradas(this.pageActual + 1);
          } else {
            this.updateRowsMOstradas(this.pageActual);
          }
        }
      },
      (error) => {
        console.log('error en el servicio pausar vacante', error);
      }
    );

  }

  generateArrayReclutadores(datos: any) {
    const reclutadores = [];
    for (let i = 0; i < datos.length; i++) {
      const nombre = datos[i].recruiter.user.name + ' ' + datos[i].recruiter.user.lastName + ' ' + datos[i].recruiter.user.secondLastName;
      reclutadores.push({
        url: (datos[i].recruiter.user.photo != null) ? datos[i].recruiter.user.photo : '',
        nombre,
        iniciales: this.getInicialesNombre(nombre),
        tipo: datos[i].type,
        recruiterId: datos[i].recruiterId,
        vacantId: datos[i].vacantId
      });
    }
    return reclutadores;
  }

  cerrarPopUpNuevaVentana() {
    this.nuevaVacante = false;
  }

  getInicialesNombre(nombre: string) {
    let iniciales = '';
    const separador = ' ',
      arregloDeSubCadenas = nombre.split(separador);

    for (let j = 0; j < arregloDeSubCadenas.length; j++) {
      const subCadena = arregloDeSubCadenas[j].substring(0, 1);
      iniciales += subCadena;
    }
    return iniciales.slice(0, 2);
  }

  getPageSymbol(current: number) {
    return ['A', 'B', 'C', 'D', 'E', 'F', 'G'][current - 1];
  }

  selectPage(page: string) {
    if (parseInt(page, 10) > this.totalPages){
      let Myelement: any;
      Myelement = document.getElementById('paginationInput');
      Myelement.value = this.totalPages;
      this.page = this.totalPages;
      this.rowsMostradas(this.totalPages);
    }else{
      this.page = parseInt(page, 10) || 1;
      this.rowsMostradas(parseInt(page));
    }
  }

  formatInput(input: HTMLInputElement) {
    input.value = input.value.replace(FILTER_PAG_REGEX, '');
  }

  inicializarFormularioCerrarVacante() {
    this.formularioCerrarVacante = this.formBuilder.group({
      presupuesto: [{ value: false, disabled: false }, []],
      solicitud: [{ value: false, disabled: false }, []],
      candidato: [{ value: false, disabled: false }, []],
      otroCheck: [{ value: false, disabled: false }, []],
      otroText: [{ value: '', disabled: false }, []],
      noMostrar: [{ value: '', disabled: false }, []],
    });

    // this.formularioCerrarVacante.controls['presupuesto'].setValue(true);
  }

  validarChecksPausarVacante(check: string) {
    const presupuesto = this.formularioCerrarVacante.get('presupuesto')?.value;
    const solicitud = this.formularioCerrarVacante.get('solicitud')?.value;
    const candidato = this.formularioCerrarVacante.get('candidato')?.value;
    const otroCheck = this.formularioCerrarVacante.get('otroCheck')?.value;

    this.resetChecksPausarVacante();
    switch (check) {
      case 'presupuesto':
        this.formularioCerrarVacante.controls.presupuesto.setValue(presupuesto);
        break;
      case 'solicitud':
        this.formularioCerrarVacante.controls.solicitud.setValue(solicitud);
        break;
      case 'candidato':
        this.formularioCerrarVacante.controls.candidato.setValue(candidato);
        break;
      case 'otroCheck':
        this.formularioCerrarVacante.controls.otroCheck.setValue(otroCheck);
        break;
    }
    this.datosPausarVacanteValid = (presupuesto || solicitud || candidato || otroCheck) ? true : false;
    if (check != 'otroCheck') { this.getReasonId(check); } else { this.reasonId = ''; }
  }

  resetChecksPausarVacante() {
    this.formularioCerrarVacante.controls.presupuesto.setValue(false);
    this.formularioCerrarVacante.controls.solicitud.setValue(false);
    this.formularioCerrarVacante.controls.candidato.setValue(false);
    this.formularioCerrarVacante.controls.otroCheck.setValue(false);
    this.formularioCerrarVacante.controls.otroText.setValue('');
  }

  getReasonId(reason: string) {
    if (this.banderaModales) {
      for (let i = 0; i < this.motivosCerrarVacante.length; i++) {
        if (reason == this.motivosCerrarVacante[i].nombreFormulario) {
          this.reasonId = this.motivosCerrarVacante[i].reasonId;
        }
      }
    } else {
      for (let i = 0; i < this.motivosPausarVacante.length; i++) {
        if (reason == this.motivosPausarVacante[i].nombreFormulario) {
          this.reasonId = this.motivosPausarVacante[i].reasonId;
        }
      }
    }

  }

  siguientePausarVacante() {
    this.otroMotivo = this.formularioCerrarVacante.get('otroText')?.value;
    if (this.datosPausarVacanteValid) {
      this.modalService.dismissAll();
      if (this.banderaModales){
        // this.openConfirmacionCerrar();
        this.cerrarVacanteService(this.iconoChekBlanco);
      }
      else{
        // if (this.banderaModalPausa) {
          this.pausarVacante(this.iconoChekBlanco);
        // } else {
        //   this.modalService.open(this.confirmacionPausa, { centered: true });
        // }
      }
      this.resetChecksPausarVacante();
    }
  }

  inizializarFormularioPlantilla() {
    this.formularioPlantilla = this.formBuilder.group({
      inputPregunta: [{ value: '', disabled: false }, [Validators.required, Validators.minLength(5)]],
    });
  }


  mensajeNotificacion(mensaje: any) {
    if (this.banderaNotificacion) {
      this.mensajeToast = 'Se silenciaron las notificaciones';
      this.showSuccess(mensaje);
    } else {
      this.mensajeToast = 'Se activaron las notificaciones';
      this.showSuccess(mensaje);

    }
    this.banderaNotificacion = !this.banderaNotificacion;
  }
  setMensajeNotificacion(mensaje: string) {
    this.mensajeToast = mensaje;
  }

  reDirecto(proceso: string, vacant: any) {
    const card = {
      id: vacant.vacantId,
      idShow: vacant.idShow,
      name: vacant.name,
      client: vacant.client.name,
      pageActual: 0,
      pageSize: 0,
      cardActual: 0,
      setProcessSelection: '',
      status: '',
      totalElemt: 0,
      totalPages: 0,
      rowsMostrar: 0,
      testsPsychometricComplete: false
    };
    this.candidateService.setSelectedCard(card);
    this._menuSeleccionado.setBarraActiva(false);
    this._menuSeleccionado.setBarraActiva(false);
    let url = './vacantes/activas';
    switch (proceso) {
      case 'candidatos':
        url = '../../proceso/seleccion/candidatos/false';
        break;
      case 'pruebas':
        url = '../../proceso/seleccion/pruebas-video/false';
        break;
      case 'entrevistas':
        url = '../../proceso/seleccion/entrevistas/false';
        break;
      case 'finalistas':
        url = '../../proceso/seleccion/finalistas/false';
        break;
      case 'contratados':
        url = '../../proceso/seleccion/contratados/false';
        break;
    }
    this.router.navigate([url], {
      relativeTo: this.activatedRoute,
    });
  }

  pausarVacante(template: any) {
    let body = {};
    body = (this.reasonId != '') ? { reasonId: this.reasonId } : { spanishReason: this.otroMotivo };
    let check = this.formularioCerrarVacante.get('noMostrar')?.value;
    check = (check != '') ? check : this.banderaModalPausa;
    check = (check == null) ? false : check;
    this.formularioCerrarVacante.get('noMostrar')?.reset();

    this.vacantesActivasService.pasusarVacante(this.idVacante, body, check).subscribe(
      (response) => {
        this.removeRow();
        this.totalElements--;
        this.dasboardComponent.getVacantAll();
        this.resetPaginadores();
        this.inicializaRows();
        this.setMensajeNotificacion('Se pausó la vacante');
        this.showSuccess(template);
      },
      (error) => {
        this.showMessageError(error.error.error);
      }
    );
  }

  cerrarVacanteService(template: any) {
    let body = {};

    body = (this.reasonId != '') ? { reasonId: this.reasonId } : { spanishReason: this.otroMotivo };
    this.vacantesActivasService.cerrarVacante(this.idVacante, body).subscribe(
      (response) => {
        this.removeRow();
        this.totalElements--;
        this.dasboardComponent.getVacantAll();
        this.resetPaginadores();
        this.inicializaRows();
        this.setMensajeNotificacion('Se cerró la vacante');
        this.showSuccess(template);
        this.otroMotivo = '';
      },
      (error) => {
        this.showMessageError(error.error.error);
      }
    );
  }

  removeRow() {
    this.rows.splice(this.indiceRow, 1);
  }

  getCatalogoMotivosCerrar() {
    const nombresFormularios = ['presupuesto', 'solicitud', 'candidato'];
    const data = [];
    this.vacantesActivasService.getMotivosCerrar().subscribe(
      (response) => {

        for (let i = 0; i < response.length; i++) {
          response[i].nombreFormulario = nombresFormularios[i];
        }
        this.motivosCerrarVacante = response;
      },
      (error) => {
        this.showMessageError(error.error.error);
      }
    );
  }

  cambiarStatusNotificaciones(indice: number) {
    const reclutadorActivo = this.authService.getRecruiterId();
    for (let i = 0; i < this.rows[indice].vacant.recruiters.length; i++) {
      if (this.rows[indice].vacant.recruiters[i].recruiterId == reclutadorActivo) {
        this.rows[indice].vacant.recruiters[i].notifications = this.banderaNotificacion;
      }
    }
  }

  crearPlantilla(template: any) {
    const pregunta = this.formularioPlantilla?.get('inputPregunta')?.value;
    this.formularioPlantilla?.reset();
    this.dataPlantilla.name = pregunta;
    delete this.dataPlantilla.status;
    this.vacantesActivasService.crearPlantilla(this.dataPlantilla).subscribe(
      (response) => {
        this.dasboardComponent.getVacantAll();
        this.showSuccess(template);
      },
      (error) => {
        this.showMessageError(error.error.error);
      }
    );
  }

  getCatalogoMotivosPausar() {
    const nombresFormularios = ['presupuesto', 'solicitud', 'candidato'];
    const data = [];
    this.vacantesActivasService.getMotivosPausar().subscribe(
      (response) => {

        for (let i = 0; i < response.length; i++) {
          response[i].nombreFormulario = nombresFormularios[i];
        }
        this.motivosPausarVacante = response;
      },
      (error) => {
        console.log('error en el servicio pausar vacante', error);
      }
    );
  }

  rowsMostradas(page: any) {

    this.page_number = parseInt(page, 10) - 1;
    if (this.pageTemporal < parseInt(page) - 1) {
      this.pageTemporal = page - 1;
    }
    this.inicializaRows();
    this.updateRowsMOstradas(page);

  }

  updateRowsMOstradas(page: any) {
    setTimeout(() => {
      const a = this.rows.slice((this.page - 1) * this.page_size, page * this.page_size);
      this.rowsPorPagina = a.length;
      if (this.pageActual <= page) {
        this.rowsMostrar = this.rowsMostrar + a.length;
        this.restarRows = a.length;
        this.pageActual = page;
      } else {
        this.rowsMostrar = this.rowsMostrar - this.restarRows;
        this.pageActual = page;
      }
    }, 100);
  }

  selecRow() {
    this.resetPaginadores();
    this.inicializaRows();
  }

  resetPaginadores() {
    this.rowsMostrar = 0;
    this.pageActual = 0;
    this.page_number = 0;
    this.restarRows = 0;
    this.pageTemporal = 0;
    this.rows = [];
    this.page = 1;
  }
  validarModalPausa(reclutadores: any) {
    const reclutadorActivo = this.authService.getRecruiterId();
    for (let i = 0; i < reclutadores.length; i++) {
      if (reclutadores[i].recruiter.recruiterId == reclutadorActivo) {
        this.banderaModalPausa = (reclutadores[i].recruiter.showModalPause == null) ? false : reclutadores[i].recruiter.showModalPause;
      }
    }
  }

  validarNotificaciones(reclutadores: any) {
    const reclutadorActivo = this.authService.getRecruiterId();
    for (let i = 0; i < reclutadores.length; i++) {
      if (reclutadores[i].recruiter.recruiterId == reclutadorActivo) {
        this.banderaNotificacion = reclutadores[i].notifications;
      }
    }
  }

  validarVacantesConfidenciales(reclutadores: any) {
    const reclutadorActivo = this.authService.getRecruiterId();
    let mostrar = false;
    for (let i = 0; i < reclutadores.length; i++) {
      if (reclutadores[i].recruiter.recruiterId == reclutadorActivo) {
        mostrar = true;
      }
    }
    return mostrar;
  }

  // cambiarStatusNotificaciones(reclutadores: any) {
  //   const reclutadorActivo = this.authService.getRecruiterId();
  //   for (let i = 0; i < reclutadores.length; i++) {
  //     if (reclutadores[i].recruiter.recruiterId == reclutadorActivo) {
  //       this.banderaNotificacion = reclutadores[i].notifications
  //     }
  //   }
  // }

  aplicarFiltroClientes(data: any) {
    this.filtroCliente = (data != '') ? '&clientId=' + data : '';
    this.resetPaginadores();
    this.inicializaRows();
  }

  aplicarFiltroReclutador(data: any) {
    this.filtroReclutador = (data != '') ? '&recruiterId=' + data : '';
    this.resetPaginadores();
    this.inicializaRows();
  }

  llenarCataloOrderBy() {
    if (this.nuevaVacante){
      this.dasboardComponent.getVacantAll();
    }
    this.vacantesActivasService.catalogoOrderBy().subscribe(
      (response) => {
        if (response != null && response.length > 0) {
          for (let i = 0; i < response.length; i++) {
            const arrTemp = response[i][0];
            this.arregloOrderBy.push({
              nombre: arrTemp[arrTemp.length - 1],
              value: arrTemp[0]
            });
          }
        }
      },
      (error) => {
        this.showMessageError(error);
      }
    );
  }


  showMessageError(dangerTpl: any) {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-light ngb-toastsBottom', delay: 3000 });
  }


  filterOrderBy(valor: any) {
    switch (valor) {
      case 'RECIENTES':
        this.sortDirection = '&sortDirection=DESC';
        this.orderBy = '&sortBy=createDate';
        break;
      case 'MAS_ANTIGUAS':
        this.sortDirection = '&sortDirection=ASC';
        this.orderBy = '&sortBy=createDate';
        break;
      case 'A_Z':
        this.sortDirection = '&sortDirection=ASC';
        this.orderBy = '&sortBy=name';
        break;
      case 'Z_A':
        this.sortDirection = '&sortDirection=DESC';
        this.orderBy = '&sortBy=name';
        break;
    }
    this.resetPaginadores();
    this.inicializaRows();
  }

  searchVacant(text: string) {
    if (text.length > 2) {
      this.searchFlag = true;
      this.textFilter = '&textFilter=' + text;
    } else {
      this.searchFlag = false;
    }
    this.resetPaginadores();
    this.inicializaRows();
  }

  // updateFlagPermision() {
  //   this.resetPaginadores();
  //   this.inicializaRows();
  // }

  changeRecruites() {
    this.recruiters$ = this.vacantesActivasService.getRecruiter$();
    this.subscriptions = this.recruiters$.subscribe(recruiters => {
      this.resetPaginadores();
      this.inicializaRows();
    });
  }

  get reasonValue(): string{
    return this.formularioCerrarVacante.get('otroText')?.value;
  }

  getCurrentDate = () => {
    this._suscriptionCanceled.getCurrentDateService().subscribe(
      (response) => {
        if (response.status == 200 && response.body != null){
          const date = (response.body.includes('/')) ? response.body : new Date().toISOString().slice(0, 10).replace(/-/g, '/');
          this._suscriptionCanceled.setCurrentDate(date);
        }
      }
    );
  }

  redirectToEdit(vacantId: any) {
    this.vacancyService.getVacancy(vacantId)?.subscribe( (response: any) => {
      if (response !== null && !(response.status >= 400)){
        this.router.navigateByUrl('/dashboard/vacantes/create/(vacancy:preview)');
      }
    });
  }

  canEditVacancy(vacancy: any): boolean{
    return vacancy.candidatos.contradatos === 0 &&
           vacancy.candidatos.entrevista === 0 &&
           vacancy.candidatos.finalistas === 0 &&
           vacancy.candidatos.nuevos === 0 &&
           vacancy.candidatos.videos === 0;
  }

  redirectoToPlan(){
    this.modalService.dismissAll();
    this.router.navigateByUrl('/start-plan/false/true');
  }

  validateExpiration(){
    if (this.activatedRoute.snapshot.queryParamMap.has('id')){
      const previousExpirationDate = this._suscriptionCanceled.getDateExpiration().substring(0, 10);
      const currentUser = JSON.parse(this.authService.getUser());
      this.profileService.getRecruiter(currentUser.recruiterId).subscribe(
        (successResponse: any) => {
          if (moment(successResponse.user.suscriptionExpires.substring(0, 10)).isAfter(previousExpirationDate) ){
            this.authService.setUser(JSON.stringify(successResponse));
            this._suscriptionCanceled.setDateExpiration(successResponse.user.suscriptionExpires);
            this.nextExpirationDate = moment(this._suscriptionCanceled.getDateExpiration()).locale('es').format('DD [de] MMMM, YYYY');
            this.modalService.open(this.modalPaymentRenovation, { centered: true });
            // TODO: Este evento debe detonarse SÓLO cuando se acredita el pago. Remover de la llamada del método
            gtag('event', 'conversion', {send_to: 'AW-641111593/_3N3CMKek6gDEKms2rEC'});
          }
        }
      );
    }
    else{
      const expirationDate = moment(this._suscriptionCanceled.getDateExpiration().substring(0, 10));
      const todayDate = moment();
      const isUserAdmin = JSON.parse(this.authService.getUser()).user.userRol === 'RECRUITER_ADMIN';
      if (isUserAdmin &&
          (!localStorage.getItem('aboutToExpireSeen') ||
          localStorage.getItem('aboutToExpireSeen') !== 'true') &&
          expirationDate.diff(todayDate, 'd') <= 5){
        setTimeout(() => {
          localStorage.setItem('aboutToExpireSeen', 'true');
          this.modalService.open(this.modalAboutToExpire, { centered: true, size: 'lg' });
        }, 1000);
      }
    }
  }

  getVacantAll() {
    this.sideNavService.totalVacancies().subscribe(
      (data) => {
        if (data != null) {
          const [activasTem, pausaTem, borradoresTem, [[, totalClosed]], templatesTem] = data;
          const [activas] = activasTem;
          const [pausa] = pausaTem;
          const [borradores] = borradoresTem;
          const [, totalActivas] = activas;
          const [, totalPausa] = pausa;
          const [, totalBorradores] = borradores;
          this.authService.setVacantActive((totalActivas > 0) ? totalActivas : 0);
          this.authService.setVacantPause((totalPausa > 0) ? totalPausa : 0);
          this.authService.setVacantDrafts((totalBorradores > 0) ? totalBorradores : 0);
          this.authService.setVacantClosed(totalClosed);
          this.sideNavService.upDateCounters(data);
        }
      },
      (error) => {
        console.log(error.status);
      });
  }
}


const FILTER_PAG_REGEX = /[^0-9]/g;
