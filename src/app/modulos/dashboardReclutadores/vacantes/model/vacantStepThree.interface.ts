import { TypeQuestion } from '../enum/vantant.enum';

export interface VacanteStepThree {
  idEducation: string;
  idStatusEducation: string;
  levelEducationExclud: boolean;
	statusEducationExclud: boolean;
	academicTitle: string;
	institution: Institution[];
	area: Area[];
	speciality: Speciality[];
	hardSkill: HardSkill[];
	softSkill: SoftSkill[];
	language: Language[];
	question: Question[];
}

interface Institution {
	idInstitucion: string;
}

interface Area {
	area:  {
		areaId: string;
	}
	exclud: boolean;
	yearExperience: number;
}

interface Speciality {
	speciality:  {
		specialtyId: string;
	}
	exclud: boolean;
	yearExperience: number;
}

interface HardSkill {
	level: string;
	skillId: string;
	exclud: false;
}

interface SoftSkill {
	level: string;
	skillId: string;
}

interface Language {
	languajeId: string;
	level: string;
	exclud: true;
}

interface Question {
	exclud: string;
	question: string;
	type: string;
	typeQuestion: TypeQuestion;
	yearsExperience: number;
}



