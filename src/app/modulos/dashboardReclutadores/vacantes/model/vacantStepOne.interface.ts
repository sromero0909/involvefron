import { CompanyType, Employees, StatusVacant } from '../enum/vantant.enum';

export interface VacantStepOne {
  name: string;
  titlePosition: string;
  numbersVacants: string;
  confidential: boolean;
  typePositionId: string;
  peopleCharge: string;
  description: string;
  createDate: Date;
  draftName: string;
  status: StatusVacant;
  steps: number;
  client: Client;
}

interface Client {
  companyId: string;
  employees: Employees;
  industryId: string;
  name: string;
  sectorId: string;
  typeCompany: CompanyType;
}

