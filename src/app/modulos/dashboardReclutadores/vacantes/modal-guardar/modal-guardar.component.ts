import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from 'src/app/services/commons/toast.setvice';

@Component({
  selector: 'app-modal-guardar',
  templateUrl: './modal-guardar.component.html',
  styleUrls: ['./modal-guardar.component.scss']
})
export class ModalGuardarComponent implements OnInit {

  constructor(private modalService: NgbModal, 
              private toastService: ToastService,
              private router: Router) { }

  ngOnInit(): void {
  }
  
  close() {
    this.modalService.dismissAll();
    this.toastService.showSuccess('Se ha guardado el borrador');
    this.router.navigateByUrl('/dashboard/vacantes/borradores');
  }

}
