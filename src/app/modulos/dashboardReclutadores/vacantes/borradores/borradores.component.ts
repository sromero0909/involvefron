import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core'; import { VacantesService } from 'src/app/services/reclutador/vacantes/vacantes.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StepperService } from 'src/app/services/reclutador/commons/stepper.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BorradoresService } from 'src/app/services/reclutador/vacantes/borradores/borradores.service';
import { ClientesService } from 'src/app/services/clientes/clientes.service';
import * as moment from 'moment';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { VacantesActivasService } from 'src/app/services/reclutador/vacantes/vacantes.activas.service';
import { CookiePasosVacanteService } from '../../../../services/reclutador/commons/cookie-pasos-vacantes.service';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';

@Component({
  selector: 'app-borradores',
  templateUrl: './borradores.component.html',
  styleUrls: ['./borradores.component.scss']
})
export class BorradoresComponent implements OnInit {
  formularioCerrarVacante!: FormGroup;
  formularioVacantes!: FormGroup;
  page = 1;
  rows: Array<any> = [];
  motivosCerrarVacante: Array<any> = [];
  arregloOrderBy: Array<any> = [];
  page_size = 5;
  page_number = 0;
  totalElements = 0;
  totalPages = 0;
  rowsMostrar = 0;
  pageActual = 0;
  pageTemporal = 0;
  restarRows = 0;
  contadorPaginas = 0;
  contadorChecks = 0;
  rowsPorPagina = 0;
  datosPausarVacanteValid = false;
  checkAll = false;
  searchFlag = false;
  borradores: any;

  filtroCliente = '';
  filtroReclutador = '';
  textFilter = '';
  sortDirection = '&sortDirection=DESC';
  orderBy = '&sortBy=createDate';
  reasonId = '';
  otroMotivo = '';
  idVacante = '';
  mensajeToast = '';

  @ViewChild('confirmacionCerrar', { static: true }) confirmacionCerrar!: TemplateRef<any>;
  @ViewChild('deleteVacant', { static: true }) deleteVacant!: TemplateRef<any>;
  @ViewChild('iconoChekBlanco', { static: true }) iconoChekBlanco!: TemplateRef<any>;

  constructor(
    private _menuSeleccionado: VacantesService,
    private borradoresService: BorradoresService,
    public toastService: ToastService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private stepperService: StepperService,
    private clientService: ClientesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private vacantesActivasService: VacantesActivasService,
    private dasboardComponent: DashboardComponent,
    private _pasosCrearVacante: CookiePasosVacanteService,
    private vacancyService: VacancyService,
    public _suscription: SuscriptionService,
    ) {
    this.inicializarFormularioCerrarVacante();
    this.inicializarFormularioVacates();
  }

  ngOnInit(): void {
    this._menuSeleccionado.setMenuSeleccionado('borradores');
    this.llenarCataloOrderBy();
    this.inicializaRows();
  }

  inicializaRows() {
    const params = {
      pageSize: this.page_size,
      pageNumber: this.page_number,
      sortDirection: this.sortDirection,
      orderBy: this.orderBy,
      status: 'BORRADOR',
      clientFilter: this.filtroCliente,
      recruiterFilter: this.filtroReclutador,
      textFilter: this.textFilter
    };
    this.rows = [];
    this.borradoresService.getListBorradores(params).subscribe((response: any) => {
      if (response != null) {
        const data = response.content;
        this.borradores = data;
        if (data != null && data.length > 0) {
          for (const borrador of data) {
            // this.clientService.getClient(borrador.vacant.client.clientId).subscribe( (response: any) => {
            const name = (typeof borrador.vacant.recruiters[0]?.recruiter.user?.name === 'undefined') ? 'Sin asignacion' : borrador.vacant.recruiters[0]?.recruiter.user?.name;
            const lastName = (typeof borrador.vacant.recruiters[0]?.recruiter.user?.lastName === 'undefined') ? '' : borrador.vacant.recruiters[0]?.recruiter.user?.lastName;

            const fecha1 = moment(borrador.vacant.createDate);
            const fecha2 = moment();
            const dias = fecha2.diff(fecha1, 'days');
            const tiempo = (dias === 0) ? 'Guardado hoy' : (dias === 1) ? `Guardado hace ${dias} día` : `Guardado hace ${dias} días`;
            this.rows.push({
              id: borrador.vacant.vacantId,
              check: false,
              nombreProyecto: borrador.vacant.name,
              status: '',
              cliente: borrador.vacant.client.name,
              tiempo: (borrador.vacant.publicationDate == null) ? `${tiempo}` : '',
              confidencialidad: borrador.vacant.confidential,
              cadidatos: {
                nuevos: 0,
                videos: 0,
                entrevista: 0,
                finalistas: 0,
                contradatos: 0 + ' de ' + 0
              },
              reclutadores: [{
                nombre: `${name} ${lastName}`,
                iniciales: this.getInicialesNombre(`${name} ${lastName}`),
                tipo: 'Encargado',
              }],
              permission: borrador.permission,
              colorStep: borrador.vacant.steps,
              createdBy: `${borrador.vacant.recruiters[0].recruiter.user.name} ${borrador.vacant.recruiters[0].recruiter.user.lastName}`
            });
            // } );
          }
          this.totalElements = response.totalElements;
          this.totalPages = response.totalPages;
          if (this.pageActual == 0) {
            this.updateRowsMostradas(this.pageActual + 1);
          } else {
            this.updateRowsMostradas(this.pageActual);
          }
        } else {
          this.totalElements = response.totalElements;
        }
      }
    });
  }

  getInicialesNombre(nombre: string) {
    let iniciales = '';
    const separador = ' ',
      arregloDeSubCadenas = nombre.split(separador);

    for (let j = 0; j < arregloDeSubCadenas.length; j++) {
      const subCadena = arregloDeSubCadenas[j].substring(0, 1);
      iniciales += subCadena;
    }
    return iniciales.slice(0, 2);
  }

  selectPage(page: string) {
    if(parseInt(page, 10) > this.totalPages){
      let Myelement : any
      Myelement = document.getElementById("paginationInput");
      Myelement.value = this.totalPages;
      this.page = this.totalPages
      this.rowsMostradas(this.totalPages);
    }else{
      this.page = parseInt(page, 10) || 1;
      this.rowsMostradas(parseInt(page));
    }
  }

  formatInput(input: HTMLInputElement) {
    input.value = input.value.replace(FILTER_PAG_REGEX, '');
  }

  inicializarFormularioCerrarVacante() {
    this.formularioCerrarVacante = this.formBuilder.group({
      presupuesto: [{ value: false, disabled: false }, []],
      solicitud: [{ value: false, disabled: false }, []],
      candidato: [{ value: false, disabled: false }, []],
      otroCheck: [{ value: false, disabled: false }, []],
      otroText: [{ value: '', disabled: false }, []],
      noMostrar: [{ value: '', disabled: false }, []],
    });
    // this.formularioCerrarVacante.controls['presupuesto'].setValue(true);
  }

  validarChecksPausarVacante(check: string) {
    const presupuesto = this.formularioCerrarVacante.get('presupuesto')?.value;
    const solicitud = this.formularioCerrarVacante.get('solicitud')?.value;
    const candidato = this.formularioCerrarVacante.get('candidato')?.value;
    const otroCheck = this.formularioCerrarVacante.get('otroCheck')?.value;

    this.resetChecksPausarVacante();
    switch (check) {
      case 'presupuesto':
        this.formularioCerrarVacante.controls.presupuesto.setValue(presupuesto);
        break;
      case 'solicitud':
        this.formularioCerrarVacante.controls.solicitud.setValue(solicitud);
        break;
      case 'candidato':
        this.formularioCerrarVacante.controls.candidato.setValue(candidato);
        break;
      case 'otroCheck':
        this.formularioCerrarVacante.controls.otroCheck.setValue(otroCheck);
        break;
    }
    this.datosPausarVacanteValid = (presupuesto || solicitud || candidato || otroCheck) ? true : false;
    if (check != 'otroCheck') { this.getReasonId(check); } else { this.reasonId = '' }
  }

  siguientePausarVacante() {
    this.otroMotivo = this.formularioCerrarVacante.get('otroText')?.value;
    if (this.datosPausarVacanteValid) {
      this.modalService.dismissAll();
      this.openConfirmacionCerrar();
      this.resetChecksPausarVacante();
    }
  }
  resetChecksPausarVacante() {
    this.formularioCerrarVacante.controls.presupuesto.setValue(false);
    this.formularioCerrarVacante.controls.solicitud.setValue(false);
    this.formularioCerrarVacante.controls.candidato.setValue(false);
    this.formularioCerrarVacante.controls.otroCheck.setValue(false);
    this.formularioCerrarVacante.controls.otroText.setValue('');
  }

  openConfirmacionCerrar() {
    this.modalService.open(this.confirmacionCerrar, { centered: true });
  }

  openScrollableCerrar(bandera: boolean, idVacante: string, indice: number) {
    this.idVacante = idVacante;
    //this.indiceRow = indice;
    //this.banderaModales = bandera;
    this.modalService.open(this.deleteVacant, { centered: true });
    if (bandera) {
      this.getCatalogoMotivosCerrar();
    }
  }

  next(pantalla: string): void {
    const show = {
      action: 'next',
      go: true,
      pantalla
    };
    this.redirectoStep4();
    this.stepperService.nextStepper(show);
  }

  redirectTocCrear(step: string, vacantId: any) {
    this.vacancyService.getVacancy(vacantId)?.subscribe( (response: any) => {
      if (response !== null && !(response.status >= 400)){
        const targetUrl = 'dashboard/vacantes/create/(vacancy:placeholder)';
        let targetPath = '';
        switch (this.vacancyService.getCurrentVacancy().steps){
          case 1:
            targetPath = 'position';
            break;
          case 2:
            targetPath = 'requeriments';
            break;
          case 3:
            targetPath = 'questionnaire';
            break;
          case 4:
            targetPath = 'tests';
            break;
          case 5:
            targetPath = 'recruiters';
            break;
          case 6:
          case 7:
            targetPath = 'preview';
            break;
            default:
              targetPath = 'description';
              break;
        }
      this.router.navigateByUrl(targetUrl.replace('placeholder', targetPath));
      }
    });

    // console.log(this.borradores);
    // let pasos: string[] = ['uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis'];
    // this.createStepOne(indice);
    // this.router.navigate(['../crear'], {
    //   relativeTo: this.activatedRoute,
    // });
    // setTimeout(() => {
    //   this.next(pasos[Number(step) - 1])
    // }, 100);
  }
  redirectoStep4() {
    this.router.navigateByUrl('dashboard/vacantes/crear/(vacante:descripcion//condiciones:laborales//requisitos:requisitos//pruebas:video)');
  }

  createStepOne = (indice: any) => {
    const paso1 = this._pasosCrearVacante.getPasosCrearVacante();
    paso1.paso1 = {
      nombreVacante: this.borradores[indice].vacant.name,
      // tituloPuesto: this.formularioDescripcion.get('tituloPuesto').value,
      tituloPuesto: this.borradores[indice].vacant.titlePosition,
      numeroVacantesDisponibles: this.borradores[indice].vacant.numbersVacants,
      vacanteConfidecnial: this.borradores[indice].vacant.confidential,
      // nombreCliente: this.formularioDescripcion.get('nombreCliente').value,
      nombreCliente: this.borradores[indice].vacant.client.name,
      sector: this.borradores[indice].vacant.client.sectorId,
      giro: this.borradores[indice].vacant.client.industryId,
      tipoEmpresa: this.borradores[indice].vacant.client.typeCompany,
      numeroEmpleados: this.borradores[indice].vacant.numbersVacants,
      tipoPuesto: this.borradores[indice].vacant.typePositionId,
      personasAcargo: true,
      numPersonasAcargo: this.borradores[indice].vacant.peopleCharge,
      editorEnriquecido: this.borradores[indice].vacant.description
    };
    this._pasosCrearVacante.setPasosCrearVacante(paso1);
    console.log('STEP_1 cookie');
    console.log(paso1);

    const pasos = this._pasosCrearVacante.getPasosCrearVacante();
    console.log('Obtener cookie');
    console.log(pasos);
  }

  inicializarFormularioVacates() {
    this.formularioVacantes = this.formBuilder.group({
      arrVacantes: this.formBuilder.array([])
    });
  }

  onchangeCheck(id: number) {
    for (let i = 0; i < this.rows.length; i++) {
      if (this.rows[i].id == id) {
        (this.rows[i].check) ? this.contadorChecks-- : this.contadorChecks++;
        this.rows[i].check = !this.rows[i].check;
      }
    }
  }

  limpiarSeleccion() {
    for (let i = 0; i < this.rows.length; i++) {
      this.contadorChecks = 0;
      this.rows[i].check = false;
    }
  }


  eliminarBorradores() {
    const vacantes: any = [];
    this.checkAll = false;
    this.rows = this.rows.filter((i) => {
      if (i.check) {
        vacantes.push(i.id);
      }
      return !i.check;
    });
    console.log('son las vacantes a eliminar', vacantes);
    // 
    this.modalService.open(this.deleteVacant, { centered: true });
    /*
    this.borradoresService.eliminarVacante(vacantes).subscribe(
      (response) => {
        if ((response.status == 204 || response.status == 200) && response.body == null) {
          this.contadorChecks = 0;
          this.dasboardComponent.getVacantAll();
          this.resetPaginadores();
          this.inicializaRows();
          this.showSuccess('Se eliminaron la(s) vacante(s)');
        }
        console.log('es larepose de eliminar', response);
      },
      (error) => {
        this.showMessageError(error.error);
        console.log('error en eliminar vacante', error);
      }
    );
    */
  }

  rowsMostradas(page: any) {
    this.page_number = parseInt(page, 10) - 1;
    if (this.pageTemporal < parseInt(page) - 1) {
      this.pageTemporal = page - 1;
    }
    this.inicializaRows();
    this.updateRowsMostradas(page);
  }

  updateRowsMostradas(page: any) {
    setTimeout(() => {
      const a = this.rows.slice((this.page - 1) * this.page_size, page * this.page_size);
      this.rowsPorPagina = a.length;
      if (this.pageActual <= page) {
        this.rowsMostrar = this.rowsMostrar + a.length;
        this.restarRows = a.length;
        this.pageActual = page;
      } else {
        this.rowsMostrar = this.rowsMostrar - this.restarRows;
        this.pageActual = page;
      }
    }, 100);
  }

  selecRow() {
    this.resetPaginadores();
    this.inicializaRows();
  }

  resetPaginadores() {
    this.rowsMostrar = 0;
    this.pageActual = 0;
    this.page_number = 0;
    this.restarRows = 0;
    this.pageTemporal = 0;
    this.rows = [];
    this.page = 1;
  }

  showSuccess(mensaje: any) {
    this.toastService.remove(1);
    this.mensajeToast = mensaje;
    this.toastService.show(this.iconoChekBlanco, { classname: 'bg-success text-light ngb-toastsBottom', delay: 5000 });
  }

  aplicarFiltroClientes(data: any) {
    this.filtroCliente = (data != '') ? '&clientId=' + data : '';
    this.resetPaginadores();
    this.inicializaRows();
  }

  aplicarFiltroReclutador(data: any) {
    this.filtroReclutador = (data != '') ? '&recruiterId=' + data : '';
    this.resetPaginadores();
    this.inicializaRows();
  }

  llenarCataloOrderBy() {
    this.vacantesActivasService.catalogoOrderBy().subscribe(
      (response) => {
        if (response != null && response.length > 0) {
          for (let i = 0; i < response.length; i++) {
            const arrTemp = response[i][0];
            this.arregloOrderBy.push({
              nombre: arrTemp[arrTemp.length - 1],
              value: arrTemp[0]
            });
          }
        }
      },
      (error) => {
        this.showMessageError(error);
      }
    );
  }


  showMessageError(dangerTpl: any) {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-light ngb-toastsBottom', delay: 3000 });
  }


  filterOrderBy(valor: any) {
    switch (valor) {
      case 'RECIENTES':
        this.sortDirection = '&sortDirection=DESC';
        this.orderBy = '&sortBy=createDate';
        break;
      case 'MAS_ANTIGUAS':
        this.sortDirection = '&sortDirection=ASC';
        this.orderBy = '&sortBy=createDate';
        break;
      case 'A_Z':
        this.sortDirection = '&sortDirection=ASC';
        this.orderBy = '&sortBy=name';
        break;
      case 'Z_A':
        this.sortDirection = '&sortDirection=DESC';
        this.orderBy = '&sortBy=name';
        break;
    }
    this.resetPaginadores();
    this.inicializaRows();
  }

  searchVacant(text: string) {
    if (text.length > 2) {
      this.searchFlag = true;
      this.textFilter = '&textFilter=' + text;
    } else {
      this.searchFlag = false;
      this.textFilter = '';
    }
    this.resetPaginadores();
    this.inicializaRows();
  }
  getReasonId(reason: string) {
    for (let i = 0; i < this.motivosCerrarVacante.length; i++) {
      if (reason == this.motivosCerrarVacante[i].nombreFormulario) {
        this.reasonId = this.motivosCerrarVacante[i].reasonId;
      }
    }
  }

  getCatalogoMotivosCerrar() {
    const nombresFormularios = ['presupuesto', 'solicitud', 'candidato'];
    const data = [];
    this.vacantesActivasService.getMotivosCerrar().subscribe(
      (response) => {

        for (let i = 0; i < response.length; i++) {
          response[i].nombreFormulario = nombresFormularios[i];
        }
        this.motivosCerrarVacante = response;
      },
      (error) => {
        this.showMessageError(error.error.error);
      }
    );
  }

  cerrarVacanteService(template: any) {
    let body = {};

    body = (this.reasonId != '') ? { reasonId: this.reasonId } : { 'spanishReason': this.otroMotivo };
    this.vacantesActivasService.cerrarVacante(this.idVacante, body).subscribe(
      (response) => {
       // this.removeRow();
        this.totalElements--;
        this.dasboardComponent.getVacantAll();
        this.resetPaginadores();
        this.inicializaRows();
        this.showSuccess(template);
        this.otroMotivo = '';
      },
      (error) => {
        this.showMessageError(error.error.error);
      }
    );
  }

  setMensajeNotificacion(mensaje: string) {
    this.mensajeToast = mensaje;
  }

  onCloseModal(){
    this.modalService.dismissAll();
  }

  deleteVacancy(){
    this.onCloseModal();
    const body = [{
        op: 'replace',
        path: '/status',
        value: 'ELIMINADA'
    }];

    this.vacantesActivasService.deleteVacancy(this.idVacante, body).subscribe(
      (response) => {
        this.totalElements--;
        this.dasboardComponent.getVacantAll();
        this.resetPaginadores();
        this.inicializaRows();
        this.showSuccess('Se eliminó el borrador');
        this.otroMotivo = '';
      },
      (error) => {
        this.showMessageError(error.error.error);
      }
    );
  }

}
const FILTER_PAG_REGEX = /[^0-9]/g;
