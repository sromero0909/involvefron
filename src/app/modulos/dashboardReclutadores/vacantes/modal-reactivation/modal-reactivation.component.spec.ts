import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalReactivationComponent } from './modal-reactivation.component';

describe('ModalReactivationComponent', () => {
  let component: ModalReactivationComponent;
  let fixture: ComponentFixture<ModalReactivationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalReactivationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalReactivationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
