import { Component, OnInit } from '@angular/core';
import { PaymentGatewayService } from 'src/app/services/reclutador/login/payment-gateway.service';
import { formatDate } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import * as moment from 'moment';
import 'moment/locale/es'
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { SuscriptionCanceledService } from 'src/app/services/reclutador/login/subscription-canceled.service';


@Component({
  selector: 'app-modal-reactivation',
  templateUrl: './modal-reactivation.component.html',
  styleUrls: ['./modal-reactivation.component.scss']
})
export class ModalReactivationComponent implements OnInit {

  selectedPlan = '';
  dateBilling: any;
  target:any = {type:'',number:'',chargeDate: new Date()}
  numTarget:string = '';
  typeTarget:string = ''
  constructor(private _serviceGlobal:SuscriptionCanceledService, private _payment: PaymentGatewayService, private traslateService: TranslateService,private _suscription: SuscriptionService,
    private modalServise: NgbModal) { }

  ngOnInit(): void {
    this.target = this._suscription.getTarget();
    this.selectedPlan = this._serviceGlobal.getSummary()?.name
    this.getDate();
    this.getTarget();
  }

  getDate = async () => {
    moment.locale(this.traslateService.getDefaultLang())
    let tranlation1= '';
    await  this.traslateService.get(['configuration.subscription.of']).toPromise().then(
      (tranlation) => {
        tranlation1 = tranlation['configuration.subscription.of']
      }
    );
    const fecha = moment(this.target.chargeDate)
    this.dateBilling = fecha.format('DD-MMMM-YYYY');
    const dateTemp = String(this.dateBilling).split('-')
    this.dateBilling = dateTemp[0] + " " + tranlation1 + " " + dateTemp[1] + " " + tranlation1 + " " + dateTemp[2]
  }

  getTarget = () => {
    this.numTarget = String(this.target.number).substring(12, 16);
    this.typeTarget = this.target.type;
  }

  close = () => {
    this.modalServise.dismissAll();
  }

}
