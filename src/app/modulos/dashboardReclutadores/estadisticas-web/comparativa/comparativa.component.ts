import { Component, OnInit } from '@angular/core';
import { EstadisticasServicesService } from '../services/estadisticas-services.service';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-comparativa',
  templateUrl: './comparativa.component.html',
  styleUrls: ['./comparativa.component.scss']
})
export class ComparativaComponent implements OnInit {
  public userEmailSesion: string = this.authService.getEmail();

  constructor( 
    private _estadisticas:EstadisticasServicesService, 
    private authService: AuthService
    ) { }

  showSpan = true;
  month:any;
  vacantes:any;
  vacantesValor:any;
  vacantesCubiertas:any;
  vacantesValorCubiertas:any;
  contrataciones:any;
  cardContrataciones:any;
  eficiencia:any;
  cardEficiencia:any;
  datosInput:any;
  Error:any
  option:any;
  item:any;
  date:any;
  ano:any;
  mes:any;

  monthFin:any;
  vacantesFin:any;
  vacantesValorCubiertasFin:any;
  cardContratacionesFin:any;
  cardEficienciaFin:any;

  codMes:any;
  optineComboMes:any;

  ngOnInit(): void {
    this.item = '';
    this.consultaMes();
    this.consultaEstadisticas();
  }

  consultaMes(){
    this._estadisticas.getCamparativeDates().subscribe(
      (response:any)=>{
        this.datosInput = response.data;
      }
    );
  }

  consultaEstadisticas(){
    this.option = this.item; 
    this._estadisticas.getMonth(this.option).subscribe(
      (response:any)=>{
        if( response.code == 204){
          this.Error = response.code;
        }else{
          let info = response.data;
          // this.datosInput = info.mesesCombo;
          const original = info.consultaMes;
          const copyOriginal = [...original];
          copyOriginal.pop();
          this.month = original;
          this.monthFin = original[original.length - 1];
          this.vacantes = info.vacantesTotales;
          this.vacantesValor = info.cardValores;
          this.vacantesFin = this.vacantesValor[this.vacantesValor.length - 1];
          this.vacantesCubiertas = info.vacantesCubiertas;
          this.vacantesValorCubiertas = info.cardValoresCubiertas;
          this.vacantesValorCubiertasFin = this.vacantesValorCubiertas[this.vacantesValorCubiertas.length - 1];
          this.contrataciones = info.contrataciones;
          this.cardContrataciones = info.cardValoresContrataciones;
          this.cardContratacionesFin = this.cardContrataciones[this.cardContrataciones.length - 1];
          this.eficiencia = info.eficiencia;
          this.cardEficiencia = info.cardValoresEficiencia;
          this.cardEficienciaFin = this.cardEficiencia[this.cardEficiencia.length - 1];
        }
    });
  }

  clickAddTodo(){
    this.codMes = document.getElementById("mes");
    this.optineComboMes = document.getElementById("mes");
    this.item = this.optineComboMes.options[this.optineComboMes.selectedIndex].value;
    this.consultaEstadisticas();
  }
}
