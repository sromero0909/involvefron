import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { NgbdSortableHeader } from 'src/app/directives/sortable.directive';
@Injectable({
  providedIn: 'root'
})

export class EstadisticasServicesService {
  
  constructor( private http: HttpClient ) { }

  getMes(){
    return this.http.get(`${environment.restStatistics}/stadistics/chart/fechas`);
  }

  getCamparativeDates(){
    return this.http.get(`${environment.restStatistics}/stadistics/stadistics/datescomparative`);
  }

  getResumen (initDate?:any, finalDate?:any ): Observable<any> {
    return this.http.get(`${environment.restStatistics}/stadistics/stadistics/resume?finalDate=${finalDate}&initDate=${initDate}`);
  }

  getMonth(option?:any): Observable<any>  {
    return this.http.get(`${environment.restStatistics}/stadistics/stadistics/comparative?option=${option}`);
  }

  getGraficas(date?:any){
    return this.http.get(`${environment.restStatistics}/stadistics/chart/line?date=${date}`);
  }

  // getSector(){
  //   return this.http.get(`${environment.restPrefix}/catalog/sector`);
  // }

  getSector(): Observable<any> {
    return this.http.get(`${environment.restPrefix}/catalog/sector`);
  }

  getArea(): Observable<any> {
    return this.http.get(`${environment.restPrefix}/catalog/area`);
  }
  
  getGraficasArea (idArea?:any): Observable<any>{
    return this.http.get(`${environment.restStatistics}/stadistics/chart/column?idArea=${idArea}`);
  }

  getDetallesTabla(date:any): Observable<any>{
    return this.http.get(`${environment.restStatistics}/stadistics/details/recruiter?date=${date}`);
  }

  getDetallesTablaCliente(date:any): Observable<any>{
    return this.http.get(`${environment.restStatistics}/stadistics/details/client?date=${date}`);
  }

}