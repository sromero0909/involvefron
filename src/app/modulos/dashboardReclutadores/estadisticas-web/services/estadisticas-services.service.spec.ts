import { TestBed } from '@angular/core/testing';

import { EstadisticasServicesService } from './estadisticas-services.service';

describe('EstadisticasServicesService', () => {
  let service: EstadisticasServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstadisticasServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
