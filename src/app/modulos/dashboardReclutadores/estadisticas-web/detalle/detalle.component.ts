import { EstadisticasServicesService } from '../services/estadisticas-services.service';
import {DecimalPipe} from '@angular/common';
import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {Observable} from 'rxjs';
import { NgbdSortableHeader, SortEvent } from 'src/app/directives/sortable.directive';
import { PaginadorService } from 'src/app/services/reclutador/vacantes/paginador.service';
import { Sort } from '@angular/material/sort';


const FILTER_PAG_REGEX = /[^0-9]/g;

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss'],
  providers: [PaginadorService, DecimalPipe]
})


export class DetalleComponent implements OnInit {
  
  page = 0;
  pageReclu:any;
  pageCliente:any;
  pageSize:any;
  collectionSizeReclu:any;
  collectionSizeCliente:any;
  countriesResultReclu: any[];
  countriesResultCliente: any[];
  countriesReclu:any[];
  dataReclu:any[];
  dataCliente:any[];
  rowsMuestraReclu:any;
  rowsMuestraCliente:any;
  rowsMuestraTotalReclu:any;
  rowsMuestraTotalCliente:any;
  clientes:any;
  codMes:any
  optineComboMes:any;
  valorMes:any;
  datosInput:any;
  dateGraf:any;
  sortedData: any[];
  resetTableName: boolean = false;
  resetVacanteTotal: boolean = false;
  resetvacanteCerrada: boolean = false;
  resetvacanteCubierta: boolean = false;
  reseteficiencia: boolean = false;
  resetdiasCobertura: boolean = false;
  mustrabotton: boolean = false;

  constructor( private _estadisticas:EstadisticasServicesService ) { 
  }
  

  
  ngOnInit(): void {
    this.pageReclu = 1;
    this.pageCliente = 1;
    this.pageSize = 10;
    this.valorMes = '';
    this.consultaMes();
    this.consultaDetalle();
  }

  selectPage(page: string) {
    this.page = parseInt(page, 10) || 1;
  }

  formatInput(input: HTMLInputElement) {
    input.value = input.value.replace(FILTER_PAG_REGEX, '');
  }

  consultaMes(){
    this._estadisticas.getMes().subscribe(
      (response:any)=>{
        this.datosInput = response.data;
      }
    );
  };

  clickAddTodoMesReclu(){
    this.codMes = document.getElementById("mesConsultaDetalle");
    this.optineComboMes = document.getElementById("mesConsultaDetalle");
    this.valorMes = this.optineComboMes.options[this.optineComboMes.selectedIndex].value;
    this.consultaDetalle();
  }

  clickAddTodoMesCliente(){
    this.codMes = document.getElementById("mesConsultaDetalle");
    this.optineComboMes = document.getElementById("mesConsultaDetalle");
    this.valorMes = this.optineComboMes.options[this.optineComboMes.selectedIndex].value;
    this.consultaDetalleCliente();
  }

  consultaDetalle() {
    this.mustrabotton = false;
    this.dateGraf = this.valorMes;
    this._estadisticas.getDetallesTabla(this.dateGraf).subscribe(
      (response:any)=>{
          this.dataReclu = response.data;
          this.rowsMuestraTotalReclu = this.dataReclu.length;
          this.collectionSizeReclu = this.rowsMuestraTotalReclu;
          this.refreshCountries();
      }
    );
  }

  consultaDetalleCliente() {
    this.mustrabotton = true;
    this.dateGraf = this.valorMes;
    this._estadisticas.getDetallesTablaCliente(this.dateGraf).subscribe(
      (response:any)=>{
          this.dataCliente = response.data;
          this.rowsMuestraTotalCliente = this.dataCliente.length;
          this.collectionSizeCliente = this.rowsMuestraTotalCliente;
          this.refreshCountriesCliente();
      }
    );
  }

  refreshCountries() {
    this.countriesResultReclu = this.dataReclu.map((country, i) => (
      {id: i + 1, ...country})).slice((this.pageReclu - 1) * this.pageSize, (this.pageReclu - 1) * this.pageSize + this.pageSize);
    this.rowsMuestraReclu = this.countriesResultReclu.length;
    // this.rowsMuestra2 = this.countriesResult.length;
  }
  
  refreshCountriesCliente() {
    this.countriesResultCliente = this.dataCliente.map((country, i) => (
      {id: i + 1, ...country})).slice((this.pageCliente - 1) * this.pageSize, (this.pageCliente - 1) * this.pageSize + this.pageSize);
    this.rowsMuestraCliente = this.countriesResultCliente.length;
  }
  
  
  onSort({ column }: any) {
    !this.resetTableName;
    if(this.resetTableName == true){
      let byName = this.countriesResultReclu.slice(0);
      byName.sort(function(a:any,b:any) {
        let x = a.name.toLowerCase();
        let y = b.name.toLowerCase();
        return x < y ? -1 : x > y ? 1 : 0;
      });
      this.countriesResultReclu = byName;
      this.resetTableName = false;
    }else if( this.resetTableName == false ){
      let byName = this.countriesResultReclu.slice(0);
      byName.sort(function(a:any,b:any) {
        let x = a.name.toLowerCase();
        let y = b.name.toLowerCase();
        return x > y ? -1 : x < y ? 1 : 0;
      });
      this.countriesResultReclu = byName;
      this.resetTableName = true;
    }
  }
  onSortCliente({ column }: any) {
    !this.resetTableName;
    if(this.resetTableName == true){
      let byName = this.countriesResultCliente.slice(0);
      byName.sort(function(a:any,b:any) {
        let x = a.name.toLowerCase();
        let y = b.name.toLowerCase();
        return x < y ? -1 : x > y ? 1 : 0;
      });
      this.countriesResultCliente = byName;
      this.resetTableName = false;
    }else if( this.resetTableName == false ){
      let byName = this.countriesResultCliente.slice(0);
      byName.sort(function(a:any,b:any) {
        let x = a.name.toLowerCase();
        let y = b.name.toLowerCase();
        return x > y ? -1 : x < y ? 1 : 0;
      });
      this.countriesResultCliente = byName;
      this.resetTableName = true;
    }
  }
  onSort1({ column }: any) {
    !this.resetVacanteTotal;
    if(this.resetVacanteTotal == true){
      var byDate = this.countriesResultReclu.slice(0);
      byDate.sort(function(a:any,b:any) {
        return a.totales - b.totales;
      });
      this.countriesResultReclu = byDate;
      this.resetVacanteTotal = false;
    }else if( this.resetVacanteTotal == false ){
       var byDate = this.countriesResultReclu.slice(0);
       byDate.sort(function(a:any,b:any) {
         return b.totales - a.totales;
       });
       this.countriesResultReclu = byDate;
       this.resetVacanteTotal = true;
     }
  }
  onSort1Cliente({ column }: any) {
    !this.resetVacanteTotal;
    if(this.resetVacanteTotal == true){
      var byDate = this.countriesResultCliente.slice(0);
      byDate.sort(function(a:any,b:any) {
        return a.totales - b.totales;
      });
      this.countriesResultCliente = byDate;
      this.resetVacanteTotal = false;
    }else if( this.resetVacanteTotal == false ){
       var byDate = this.countriesResultCliente.slice(0);
       byDate.sort(function(a:any,b:any) {
         return b.totales - a.totales;
       });
       this.countriesResultCliente = byDate;
       this.resetVacanteTotal = true;
     }
  }
  onSort2({ column }: any) {
    !this.resetvacanteCerrada;
    if(this.resetvacanteCerrada === true){
      var byDate = this.countriesResultReclu.slice(0);
      byDate.sort(function(a:any,b:any) {
        return a.cerradas - b.cerradas;
      });
      this.countriesResultReclu = byDate;
      this.resetvacanteCerrada = false;
    }else if( this.resetvacanteCerrada === false ){
      var byDate = this.countriesResultReclu.slice(0);
      byDate.sort(function(a:any,b:any) {
        return b.cerradas - a.cerradas;
      });
      this.countriesResultReclu = byDate;
      this.resetvacanteCerrada = true;
    }    
  }
  onSort2Cliente({ column }: any) {
    !this.resetvacanteCerrada;
    if(this.resetvacanteCerrada === true){
      var byDate = this.countriesResultCliente.slice(0);
      byDate.sort(function(a:any,b:any) {
        return a.cerradas - b.cerradas;
      });
      this.countriesResultCliente = byDate;
      this.resetvacanteCerrada = false;
    }else if( this.resetvacanteCerrada === false ){
      var byDate = this.countriesResultCliente.slice(0);
      byDate.sort(function(a:any,b:any) {
        return b.cerradas - a.cerradas;
      });
      this.countriesResultCliente = byDate;
      this.resetvacanteCerrada = true;
    }    
  }
  onSort3({ column }: any) {
    !this.resetvacanteCubierta;
    if(this.resetvacanteCubierta === true){
      var byDate = this.countriesResultReclu.slice(0);
      byDate.sort(function(a:any,b:any) {
        return a.cubiertas - b.cubiertas;
      });
      this.countriesResultReclu = byDate;
      this.resetvacanteCubierta = false;
    }else if( this.resetvacanteCubierta === false ){
      var byDate = this.countriesResultReclu.slice(0);
      byDate.sort(function(a:any,b:any) {
        return b.cubiertas - a.cubiertas;
      });
      this.countriesResultReclu = byDate;
      this.resetvacanteCubierta = true;
    }
    
  }
  onSort3Cliente({ column }: any) {
    !this.resetvacanteCubierta;
    if(this.resetvacanteCubierta === true){
      var byDate = this.countriesResultCliente.slice(0);
      byDate.sort(function(a:any,b:any) {
        return a.cubiertas - b.cubiertas;
      });
      this.countriesResultCliente = byDate;
      this.resetvacanteCubierta = false;
    }else if( this.resetvacanteCubierta === false ){
      var byDate = this.countriesResultCliente.slice(0);
      byDate.sort(function(a:any,b:any) {
        return b.cubiertas - a.cubiertas;
      });
      this.countriesResultCliente = byDate;
      this.resetvacanteCubierta = true;
    }
    
  }
  onSort4({ column }: any) {
    !this.reseteficiencia;
    if(this.reseteficiencia === true){
      var byDate = this.countriesResultReclu.slice(0);
      byDate.sort(function(a:any,b:any) {
        return a.eficiencia - b.eficiencia;
      });
      this.countriesResultReclu = byDate;
      this.reseteficiencia = false;
    }else if( this.reseteficiencia === false ){
      var byDate = this.countriesResultReclu.slice(0);
      byDate.sort(function(a:any,b:any) {
        return b.eficiencia - a.eficiencia;
      });
      this.countriesResultReclu = byDate;
      this.reseteficiencia = true;
    }
  }
  onSort4Cliente({ column }: any) {
    !this.reseteficiencia;
    if(this.reseteficiencia === true){
      var byDate = this.countriesResultCliente.slice(0);
      byDate.sort(function(a:any,b:any) {
        return a.eficiencia - b.eficiencia;
      });
      this.countriesResultCliente = byDate;
      this.reseteficiencia = false;
    }else if( this.reseteficiencia === false ){
      var byDate = this.countriesResultCliente.slice(0);
      byDate.sort(function(a:any,b:any) {
        return b.eficiencia - a.eficiencia;
      });
      this.countriesResultCliente = byDate;
      this.reseteficiencia = true;
    }
  }
  onSort5({ column }: any) {
    !this.resetdiasCobertura;
    if(this.resetdiasCobertura === true){
      var byDate = this.countriesResultReclu.slice(0);
      byDate.sort(function(a:any,b:any) {
        return a.diasCobertura - b.diasCobertura;
      });
      this.countriesResultReclu = byDate;
      this.resetdiasCobertura = false;
    }else if( this.resetdiasCobertura === false ){
      var byDate = this.countriesResultReclu.slice(0);
      byDate.sort(function(a:any,b:any) {
        return b.diasCobertura - a.diasCobertura;
      });
      this.countriesResultReclu = byDate;
      this.resetdiasCobertura = true;
    }
    
  }
  onSort5Cliente({ column }: any) {
    !this.resetdiasCobertura;
    if(this.resetdiasCobertura === true){
      var byDate = this.countriesResultCliente.slice(0);
      byDate.sort(function(a:any,b:any) {
        return a.diasCobertura - b.diasCobertura;
      });
      this.countriesResultCliente = byDate;
      this.resetdiasCobertura = false;
    }else if( this.resetdiasCobertura === false ){
      var byDate = this.countriesResultCliente.slice(0);
      byDate.sort(function(a:any,b:any) {
        return b.diasCobertura - a.diasCobertura;
      });
      this.countriesResultCliente = byDate;
      this.resetdiasCobertura = true;
    }
    
  }

  

  
}
