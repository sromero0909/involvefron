import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadisticasWebComponent } from './estadisticas-web.component';

describe('EstadisticasWebComponent', () => {
  let component: EstadisticasWebComponent;
  let fixture: ComponentFixture<EstadisticasWebComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EstadisticasWebComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadisticasWebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
