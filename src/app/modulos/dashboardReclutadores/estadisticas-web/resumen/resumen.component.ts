import { Component, OnInit } from '@angular/core';
import { EstadisticasServicesService } from '../services/estadisticas-services.service';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label, PluginServiceGlobalRegistrationAndOptions } from 'ng2-charts';
import {NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import * as Chart from 'chart.js';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-resumen',
  templateUrl: './resumen.component.html',
  styleUrls: ['./resumen.component.scss']
})
export class ResumenComponent implements OnInit {
  public userEmailSesion: string = this.authService.getEmail();
  model: NgbDateStruct;
  resumen:any [] = [];
  cardBody:any [] = [];
  valorGrafica:any;
  valorDefaultGrafica:any;
  initDate:any;
  finalDate:any;
  hoveredDate: NgbDate | null = null;
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  year:any;  
  month:any;
  day:any;
  ctx:any;
  date:any;
  chart: any;
  resumenPrueba: any;
  optieneValor:any;
  Error:any

  
  constructor( 
    private _estadisticas:EstadisticasServicesService,
    private authService: AuthService,
    private calendar: NgbCalendar, 
    public formatter: NgbDateParserFormatter ) { 
      this.fromDate = calendar.getNext(calendar.getToday(), 'd', -30);
      this.toDate = calendar.getToday();
      this.year = this.toDate.year;
      this.month = this.toDate.month;
      this.day = this.toDate.day;
    }
  ngOnInit(): void {
    // console.log(this.userEmailSesion);
    this.consultaResumen();
    // this.cardBody = this._estadisticas.getCardBody();
  }
  
  consultaResumen = () => {
    this.initDate = this.fromDate?.year+"-"+this.fromDate?.month+"-"+this.fromDate?.day;
    this.finalDate = this.toDate?.year+"-"+this.toDate?.month+"-"+this.toDate?.day;
    this._estadisticas.getResumen(this.initDate,this.finalDate).subscribe(
      (response:any)=>{
        if( response.code == 204){
          this.Error = response.code;
        }else{
          this.resumen = response.data.data;
          this.resumenPrueba = [this.resumen[this.resumen.length - 1]];
          this.optieneValor = this.resumen[this.resumen.length - 1];
          this.valorGrafica = this.optieneValor.valorGrafica;
          this.valorDefaultGrafica = ( 100 - this.valorGrafica);
          this.chart = new Chart('canvas', {
            type: 'doughnut',
            data: {
              labels: ['Vacantes cerradas con contratación','Vacantes publicadas'],
              datasets: [
                { 
                  data: [this.valorGrafica, this.valorDefaultGrafica],
                  backgroundColor: ["#5DD6D6", "#0000000F"],
                  borderColor: ['#0000000F'],
                  fill: true
                },
              ]
            },
            options: {
              legend: {
                display: true,
                position: 'bottom',
              },
              tooltips:{
                enabled:true
              }
            }
          });
        }
      }
    );
  }

  limpiaMeses(){
    this.fromDate = this.calendar.getNext(this.calendar.getToday(), 'd', -1);
    this.model = this.calendar.getToday();
  }
  
  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && (date.equals(this.fromDate)) || date.after(this.fromDate)) {
      this.toDate = date;
      
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }
  
  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }
}
