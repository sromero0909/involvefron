import { Component, OnInit } from '@angular/core';
import { MainNavModel } from '../talent/models/main-nav.model';

@Component({
  selector: 'app-estadisticas-web',
  templateUrl: './estadisticas-web.component.html',
  styleUrls: ['./estadisticas-web.component.scss']
})
export class EstadisticasWebComponent implements OnInit {

  mainNavData: MainNavModel;
  constructor() { }

  ngOnInit(): void {
    this.mainNavConfig();
  }

  mainNavConfig = () => {
    this.mainNavData = {
      title: 'Estadísticas',
      extra: false,
      search: false,
      image: 'assets/img/dashboard/estadisticas/Grupo 2816.svg',
      iconBack: false,
    }
  }

}
