import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import * as Chart from 'chart.js';
import { ChartDataSets, ChartType, ChartOptions } from 'chart.js';
import { Color, Label, BaseChartDirective } from 'ng2-charts';
import { EstadisticasServicesService } from '../services/estadisticas-services.service';

@Component({
  selector: 'app-graficas',
  templateUrl: './graficas.component.html',
  styleUrls: ['./graficas.component.scss']
})
export class GraficasComponent implements OnInit {

  constructor( private _estadisticas:EstadisticasServicesService) { }

  @ViewChild('myCanvas') canvas: ElementRef;
  @ViewChild('myCanvas2') canvas2: ElementRef;

  
  option:any;
  item:any;
  date:any;
  ano:any;
  mes:any;
  resumen:any [] = [];
  resumen2:any [] = [];
  cardBody:any [] = [];
  dateGraf:any;
  dateGrafArea:any;
  idArea:any;
  valorGraficaArea:any;
  chart: any;
  valorLabelGrafica:any;
  sectores:any;
  areas:any;
  codMes:any
  codSector:any;
  codArea:any;
  optineComboMes:any;
  optineComboSector:any;
  optineComboArea:any;
  valorMes:any;
  valorSector:any;
  valorArea:any

  datosInput:any;
  Error:any
  mostrarGraficaSector:boolean = false;
  mostrarGraficaArea:boolean = false;
  sinDataGrap:boolean = false;
  dataGraf:boolean = true;
  loadingGrafica:boolean = false;
  titleGrapOne:any;
  titleGrapTwo:any;

  public lineChartDataNormal: ChartDataSets[] = [];
  public lineChartDataNormal2: ChartDataSets[] = [];
  public lineChartDataNormal3: ChartDataSets[] = [];
  public lineChartDataNormalArea: ChartDataSets[] = [];
  public lineChartLabels:Label[] = [];
  public lineChartLabels2: Label[] = [];
  public lineChartLabels3: Label[] = [];
  public lineChartLabelsArea: Label[] = [];
  public lineChartDataMotivation: Label[] = [];
  
  public lineChartOptions = { 
    responsive: true,
    scales: {
      xAxes: [{
        ticks: {   
          fontfamily: 'GilroyRegular',
          fontColor: "#2E2E2E", 
          fontSize: 12 ,     
          min: 0
        },
        scaleLabel: {
          display: true,
          labelString: "Mes",
          fontColor: "#979797"
        },
        gridLines: {
          display: false
        },
        position: 'bootom'
      }],
      yAxes: [{
        ticks: {   
          fontFamily:'GilroyRegular',
          fontColor:  "#2E2E2E",
          fontSize: 12,  
          position: 'left',
          min: 0,
          max:100,
          stepSize: 20,
          display: true
        },
        gridLines: {
          display: false
        },
        scaleLabel: {
          display: true,
          labelString: "Porcentaje",
          fontColor: "#979797"
        }
      }]
      
    } 
  };
  public lineChartOptions2 = { 
    responsive: true,
    scales: {
      xAxes: [{
        ticks: {   
          fontFamily:'GilroyRegular',
          fontColor: "#2E2E2E", 
          fontSize: 12 ,     
          min: 0
        },
        scaleLabel: {
          display: true,
          labelString: "Mes",
          fontColor: "#979797"
        },
        gridLines: {
          display: false
        },
        position: 'bootom'
      }],
      yAxes: [{
        ticks: {   
          fontFamily:'GilroyRegular',
          fontStyle: 'normal',
          fontColor:  "#2E2E2E",
          fontSize: 12,  
          position: 'left',
          // min: 0,
          // max:100,
          // stepSize: 5,
          display: true,
        },
        gridLines: {
          display: false
        },
        scaleLabel: {
          display: true,
          labelString: "Días",
          fontColor: "#979797"
        }
      }]
    } 
  };
  public lineChartOptionsArea: any = {
    responsive: true,
    scales: {
      xAxes: [{
        ticks: {   
          fontFamily:'GilroyRegular',
          fontStyle: 'normal',
          fontColor: "#2E2E2E", 
          fontSize: 12 ,     
          min: 0
        },
        scaleLabel: {
          display: true,
          labelString: "Candidatos",
          fontColor: "#979797"
        },
        gridLines: {
          display: false
        },
        position: 'top'
      }],
      yAxes: [{
        ticks: {   
          fontFamily:'GilroyRegular',
          fontStyle: 'normal',
          fontColor:  "#2E2E2E",
          fontSize: 12,  
          position: 'left'
        },
        gridLines: {
          display: true
        },
        scaleLabel: {
          display: true,
          labelString: "Especialidad",
          fontColor: "#979797"
        }
      }]
    }
  };

  public lineChartColorsNorm: Color[] = [];
  public lineChartColorsNorm2: Color[] = [];
  public lineChartColorsNormArea: Color[] = [];

  public lineChartLegend = true;
  public lineChartLegendArea = false;

  public lineChartType: ChartType = 'line';
  public lineChartTypeSelectArea: ChartType = 'horizontalBar';


  ngOnInit(): void {
    this.date = new Date().toISOString().slice(0, 10);
    this.ano = this.date.substring(0,4);
    this.mes = this.date.substring(5,7);
    this.option = this.mes+"-"+this.ano;
    this.valorMes = '';
    this.valorArea = '';
    this.consultaMes();
    this.consultaArea();
    this.consultaSector();
    this.consultaEstadisticas();
    // this.consultaEstadisticas2();
  }

  consultaMes(){
    this._estadisticas.getMes().subscribe(
      (response:any)=>{
        this.datosInput = response.data;
      }
    );
  };

  consultaSector(){
    this._estadisticas.getSector().subscribe(
      (response:any)=>{
        for (const key in response) {
          if (Object.prototype.hasOwnProperty.call(response, key)) {
            this.sectores = response;
          }
        }
      }
    )
  };

  consultaArea(){
    this._estadisticas.getArea().subscribe(
      (response:any)=>{
        for (const key in response) {
          if (Object.prototype.hasOwnProperty.call(response, key)) {
            this.areas = response;
          }
        }
      }
    )
  };

  
  consultaEstadisticas(){
    this.dateGraf = this.valorMes;
    this._estadisticas.getGraficas(this.dateGraf).subscribe(
      (response:any)=>{
        if( response.code == 204 ){
          this.Error = response.code;
        }else{
          this.resumen = response.data;
          this.titleGrapOne = this.resumen[0].textGrafica[0].titletext;
          this.titleGrapTwo = this.resumen[1].textGrafica[0].titletext;
          this.lineChartLabels = this.resumen[0].valorLabelGrafica[0].meses;
          this.lineChartDataNormal =  this.resumen[0].datosGrafica;
          this.lineChartLabels2 = this.resumen[1].valorLabelGrafica[0].meses;
          this.lineChartDataNormal2 =  this.resumen[1].datosGrafica;

          this.createCanvas2();
        }
      }
    );
  }

  createCanvas2(){    
    let gradient_green = this.canvas.nativeElement.getContext('2d').createLinearGradient(200,700,700,100);
    gradient_green.addColorStop(1, '#CFF4F4');
    gradient_green.addColorStop(0, '#CFF4F400');

    let gradient_purple = this.canvas.nativeElement.getContext('2d').createLinearGradient(200,700,700,100);
    gradient_purple.addColorStop(1, '#FDEDFF');
    gradient_purple.addColorStop(0, '#FDEDFF');

    let gradient_yellow = this.canvas.nativeElement.getContext('2d').createLinearGradient(200,700,700,100);
    gradient_yellow.addColorStop(1, '#FEF8CD');
    gradient_yellow.addColorStop(0, '#FEF8CD00');
    this.lineChartColorsNorm = [
        {
          backgroundColor: gradient_green,
          borderColor: '#0BB9B9',
        },
        {
          backgroundColor: gradient_purple,
          borderColor: '#4E148C',
        }
    ];
    this.lineChartColorsNorm2 = [
      {
        backgroundColor: gradient_yellow,
        borderColor: '#EAD637',
      }
  ];
  }
  

  consultaEstadisticas2(){
    this.idArea = this.valorArea;
    this._estadisticas.getGraficasArea(this.idArea).subscribe(
      (response:any)=>{
        this.loadingGrafica = true;
        this.resumen2 = response.data;
        this.lineChartLabelsArea = this.resumen2[0].valorLabelGrafica[0].meses;
        this.lineChartDataNormalArea =  this.resumen2[0].datosGrafica;
        if( this.lineChartLabelsArea.length > 0 ){
          this.dataGraf = true;
        }else{
          this.dataGraf = false;
          this.loadingGrafica = false;
        }
        this.loadingGrafica = false;
        this.createCanvas();
      }
    )
  };

  createCanvas(){    
    let gradient_green = this.canvas.nativeElement.getContext('2d').createLinearGradient(200,50,400,0);
    gradient_green.addColorStop(1, '#B4F3D6');
    gradient_green.addColorStop(0, '#B4F3D64D');
    this.canvas
    this.lineChartColorsNormArea = [
        {
          backgroundColor: gradient_green
        }
    ];
    
  }

  clickAddTodoMes(){
    this.codMes = document.getElementById("mesConsulta");
    this.optineComboMes = document.getElementById("mesConsulta");
    this.valorMes = this.optineComboMes.options[this.optineComboMes.selectedIndex].value;
    this.consultaEstadisticas();
  }
    
  clickAddTodoSector(){
    this.codSector = document.getElementById("sector");
    this.optineComboSector = document.getElementById("sector");
    this.valorSector = this.optineComboSector.options[this.optineComboSector.selectedIndex].value;
    this.mostrarGraficaSector = true;
  }

  clickAddTodoArea(){
    this.codArea = document.getElementById("area");
    this.optineComboArea = document.getElementById("area");
    this.valorArea = this.optineComboArea.options[this.optineComboArea.selectedIndex].value;
    this.mostrarGraficaArea = true;
    this.consultaEstadisticas2();
  }
}
