import { Component, EventEmitter, Output, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-input-search',
  templateUrl: './input-search.component.html',
  styleUrls: ['./input-search.component.scss']
})
export class InputSearchComponent implements OnInit {

  @Output() onEnter   : EventEmitter<string>  = new EventEmitter();
  @Output() onDebounce: EventEmitter<string>  = new EventEmitter();
  @Output() onBlur:     EventEmitter<boolean> = new EventEmitter();

  @Input() placeholder: string = '';
  @Input() selectListItem: string = '';

  debouncer: Subject<string> = new Subject();

  termino: string = '';

  valueInput = '';

  ngOnInit() {
    this.debouncer
      .pipe(debounceTime(600))
      .subscribe( valor => {
        this.onDebounce.emit( valor );
      });

      this.termino = this.selectListItem;
  }

  buscar() {
    this.onEnter.emit( this.termino );
  }

  teclaPresionada() {
    this.debouncer.next( this.termino );
  }

  onBlurDebounce = () => {
    this.onBlur.emit(false);
  }

}
