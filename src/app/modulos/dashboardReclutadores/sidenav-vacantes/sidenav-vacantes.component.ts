import { Component, OnInit, Input, Output, ViewChild, EventEmitter, TemplateRef, ElementRef } from '@angular/core';
import { VacantesService } from 'src/app/services/reclutador/vacantes/vacantes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Options } from '@angular-slider/ngx-slider';
import { CookieService } from 'ngx-cookie-service';

import { AuthService } from '../../../services/auth.service';
import { SideNavService } from 'src/app/services/reclutador/vacantes/sideNav.service';
import { CandidatesService } from '../../../services/reclutador/vacantes/selectionProcess/candidatos.service';
import { VacantCard } from 'src/app/services/models/cardVacants.model';
import { VacancyService } from 'src/app/services/reclutador/vacantes/createVacant/vacancy.service';
import { SuscriptionService } from 'src/app/services/reclutador/configuration/suscription.servise';
import { PushNotificationsService } from 'src/app/services/commons/push-notifications.service';
import { MainNavModel } from '../talent/models/main-nav.model';
import { SessionService } from 'src/app/auth/services/sessionService/session.service';

@Component({
  selector: 'app-sidenav-vacantes',
  templateUrl: './sidenav-vacantes.component.html',
  styleUrls: ['./sidenav-vacantes.component.scss']
})
export class SidenavVacantesComponent implements OnInit {

  mainNavData: MainNavModel;
  // Datos de la sesion
  public nameUserSesion: string = this.authService.getName() + ' ' + this.authService.getLastName() + ' ' + this.authService.getSecondLastName();
  public userEmailSesion: string = this.authService.getEmail();
  public photoProfile: string = this.authService.getPhotoProfile();
  contadorActivas: number;
  contadorEnPausa: number;
  contadorBorradores: number;
  contadorCerradas: number;
  contadorMasFiltros = 0;
  contadorCandidatos = 0;
  contadorPruebas = 0;
  contadorEntrevistas = 0;
  contadorFinalistas = 0;
  contadorContratados = 0;
  averageAge = 0;
  averageSalary = 0;
  barraVacantesActiva$!: Observable<boolean>;
  formularioFiltros!: FormGroup;
  isCreatingANewVacancy = false;
  @Input() showMainNavVacancy = true;
  @Input() showStatus = true;
  @Input() showTitleCreateVacancy = false;
  @Input() hideWhenCreateVacancy = true;
  @Input() ocultarEntrada!: string;
  @Input() status = 'NUEVO';

  @Output() propagarFiltroCliente = new EventEmitter<string>();
  @Output() propagarFiltroReclutador = new EventEmitter<string>();
  @Output() spreadVacantSearchInput = new EventEmitter<string>();
  @Output() spreadfilter1 = new EventEmitter<any>();
  @Output() spreadfilter2 = new EventEmitter<any>();
  @Output() spreadfilterVideo = new EventEmitter<any>();
  @Output() spreadfilterAge = new EventEmitter<any>();
  @Output() spreadfilterSalary = new EventEmitter<any>();
  @Output() spreadMorefilters = new EventEmitter<any>();
  @ViewChild('vacantSearchInput') vacantSearchInput!: ElementRef;
  countersVacant$!: Observable<any>;
  modal: any;

  ocultar: string;
  claseLista = 'lista';
  claseListaActivas = '';
  claseListaEnpausa = '';
  claseListaBorradores = '';
  classListClosed = '';
  classListTemplate = '';
  textoFiltro1 = 'Recientes';
  textoFiltro2 = 'Compatibilidad';
  textoFiltro3 = 'Edad';
  textoFiltro4 = 'Sueldo deseado';
  textosueldoMayor = 0;
  textoSueldoMenor = 0;
  textoVideo = 'Video';
  textoFiltro5 = 'Más filtros';
  columnasModal = 'row-cols-2';
  paddintop = '';
  vacio = '';

  selectdedCard: VacantCard;
  banderaVideo = false;
  baderaMasFiltros = false;
  banderaMensajes = false;
  select = { enpausa: '', plantillas: '', cerradas: '', borradores: '', activas: '' };
  procesoSeleccionado = { candidatos: { flecha: 'base', badge: 'badgeInitialProceso' }, pruebas: { flecha: 'base', badge: 'badgeInitialProceso' }, entrevistas: { flecha: 'base', badge: 'badgeInitialProceso' }, finalistas: { flecha: 'base', badge: 'badgeInitialProceso' }, contratados: { flecha: 'base', badge: 'badgeInitialProceso' } };

  imagenPerfil = '../../../../assets/img/dashboard/vacantes/sinFotoPerfilSmall.svg';
  arrayClientes: Array<any> = [];
  arrayReclutadores: Array<any> = [];
  arrayMasFiltros: Array<any> = [];
  arrayFilter1: Array<any> = [];
  arrayFilter2: Array<any> = [];
  shortList: Array<any> = [];

  mostrarPortafolios = false;

  value = 0;
  highValue = 0;
  valueSueldo = 0;
  highValueSueldo = 0;
  options: Options = {
    floor: 0,
    ceil: 100
  };
  options1: Options = {
    floor: 1000,
    ceil: 40000
  };

  @ViewChild('modalMasFiltros', { static: true }) modalMasFiltros!: TemplateRef<any>;

  constructor(private _menuSeleccionado: VacantesService, private router: Router,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private _sideNavService: SideNavService,
    private _processSelection: CandidatesService,
    private vacancyService: VacancyService,
    public _suscription: SuscriptionService,
    private cookieService: CookieService,
    private notification: PushNotificationsService,
    private sessionService: SessionService) {
    this.isCreatingANewVacancy = this.activatedRoute.firstChild?.outlet === 'vacancy';
    this.selectdedCard = _processSelection.getSelectedCard();
    this.contadorActivas = Number(this.authService.getVacantActive());
    this.contadorEnPausa = Number(this.authService.getVacantPause());
    this.contadorBorradores = Number(this.authService.getVacantDrafts());
    this.contadorCerradas = Number(this.authService.getVacantClosed());
    this.ocultar = '';
    this.photoProfile = (String(this.photoProfile) == 'null') ? '../../../../assets/img/dashboard/vacantes/sinFotoPerfil.svg' : this.photoProfile;
    this.inicializarFormularioFltros();
  }

  ngOnInit(): void {
    this.barraVacantesActiva$ = this._menuSeleccionado.barraActiva$;
    this.getClientes();
    this.menuSeleccionado();
    this.procesoSeleccionadoF();
    this.inicializaBarraCrearVacante();
    // this.getClientes();
    this.getReclutadores();
    this.updateConuntersVacant();
    this.mainNavConfig();
  }

  mainNavConfig = () => {
    if (!this.barraVacantesActiva$) {
      this.mainNavData = {
        title: 'Crear vacante',
        extra: true,
        search: false,
        image: 'assets/img/dashboard/vacantes/Grupo510.svg',
        imageClass: 'filter-green',
        iconBack: false,
        filters: true,
      };
    } else {
      this.mainNavData = {
        title: 'Vacantes',
        extra: true,
        search: false,
        image: 'assets/img/dashboard/vacantes/Grupo510.svg',
        imageClass: 'filter-green',
        iconBack: false,
      };
    }
  }

  ngAfterViewInit() {
    this.modal = document.querySelector('#moda');
    // this.inicializarBuscador();
    if (!this.barraVacantesActiva$._isScalar && this.selectdedCard != undefined) {
      this.selectionProcessCounter();
      this.firstFilterCatalog();
      this.secondFilterCatalog();
      this.filterRange();
      this.moreFilters();
    }
  }

  mouseOverLista(lista: string) {
    this.claseLista = 'badgeNoSelect';
    const menu = this._menuSeleccionado.getMenuSeleccionado();
    switch (lista) {
      case 'activas':
        if (menu != 'activas') {
          this.claseListaActivas = this.claseLista;
        }
        break;
      case 'enpausa':
        if (menu != 'enpausa') {
          this.claseListaEnpausa = this.claseLista;
        }
        break;
      case 'borradores':
        if (menu != 'borradores') {
          this.claseListaBorradores = this.claseLista;
        }
        break;
      case 'cerradas':
        if (menu != 'cerradas') {
          this.classListClosed = this.claseLista;
        }
        break;
      case 'plantillas':
        if (menu != 'plantillas') {
          this.classListTemplate = this.claseLista;
        }
        break;
      default:
        this.claseListaActivas = 'badgeInitial';
        this.claseListaEnpausa = 'badgeInitial';
        this.claseListaBorradores = 'badgeInitial';

    }
  }

  mouseLeaveLista(lista: string) {
    const menu = this._menuSeleccionado.getMenuSeleccionado();

    switch (lista) {
      case 'activas':
        if (menu != 'activas') {
          this.claseListaActivas = '';
        }
        break;
      case 'enpausa':
        if (menu != 'enpausa') {
          this.claseListaEnpausa = '';
        }
        break;
      case 'borradores':
        if (menu != 'borradores') {
          this.claseListaBorradores = '';
        }
        break;
      case 'cerradas':
        if (menu != 'cerradas') {
          this.classListClosed = '';
        }
        break;
      case 'plantillas':
        if (menu != 'plantillas') {
          this.classListTemplate = '';
        }
        break;
      default:
        this.claseListaActivas = 'badgeInitial';
        this.claseListaEnpausa = 'badgeInitial';
        this.claseListaBorradores = 'badgeInitial';
        this.classListClosed = 'badgeInitial';
        this.classListTemplate = 'badgeInitial';
        break;
    }
  }

  menuSeleccionado() {
    const menu = this._menuSeleccionado.getMenuSeleccionado();
    switch (menu) {
      case 'activas':
        this.select.activas = 'select';
        this.claseListaActivas = 'badgeSelect';
        break;
      case 'enpausa':
        this.select.enpausa = 'select';
        this.claseListaEnpausa = 'badgeSelect';
        break;
      case 'borradores':
        this.select.borradores = 'select';
        this.claseListaBorradores = 'badgeSelect';
        break;
      case 'cerradas':
        this.select.cerradas = 'select';
        this.classListClosed = 'badgeSelect';
        break;
      case 'plantillas':
        this.select.plantillas = 'select';
        this.classListTemplate = 'badgeSelect';
        break;
      default:
        this.claseListaActivas = 'badgeInitial';
        this.claseListaEnpausa = 'badgeInitial';
        this.claseListaBorradores = 'badgeInitial';
        this.classListClosed = 'badgeInitial';
        this.classListTemplate = 'badgeInitial';
        break;
    }
  }

  procesoSeleccionadoF() {
    const proceso = this._menuSeleccionado.getProcesoSeleccion();
    this.resetProcesoSeleccionado();
    switch (proceso) {
      case 'candidatos':
        this.banderaVideo = false;
        this.baderaMasFiltros = true;
        this.procesoSeleccionado.candidatos.flecha = 'active';
        this.procesoSeleccionado.candidatos.badge = 'badgeActiveProceso';
        break;
      case 'pruebas':
        this.banderaVideo = true;
        this.baderaMasFiltros = true;
        this.procesoSeleccionado.pruebas.flecha = 'active';
        this.procesoSeleccionado.pruebas.badge = 'badgeActiveProceso';
        break;
      case 'entrevistas':
        this.banderaVideo = true;
        this.baderaMasFiltros = true;
        this.procesoSeleccionado.entrevistas.flecha = 'active';
        this.procesoSeleccionado.entrevistas.badge = 'badgeActiveProceso';
        break;
      case 'finalistas':
        this.banderaVideo = false;
        this.baderaMasFiltros = false;
        this.procesoSeleccionado.finalistas.flecha = 'active';
        this.procesoSeleccionado.finalistas.badge = 'badgeActiveProceso';
        break;
      case 'contratados':
        this.banderaVideo = false;
        this.baderaMasFiltros = false;
        this.procesoSeleccionado.contratados.flecha = 'active';
        this.procesoSeleccionado.contratados.badge = 'badgeActiveProceso';
        break;
      default:
        this.resetProcesoSeleccionado;
        break;
    }
  }

  resetProcesoSeleccionado() {
    this.procesoSeleccionado = { candidatos: { flecha: 'base', badge: 'badgeInitialProceso' }, pruebas: { flecha: 'base', badge: 'badgeInitialProceso' }, entrevistas: { flecha: 'base', badge: 'badgeInitialProceso' }, finalistas: { flecha: 'base', badge: 'badgeInitialProceso' }, contratados: { flecha: 'base', badge: 'badgeInitialProceso' } };
  }

  mouseOverImagenPerfil() {
    this.imagenPerfil = '../../../../assets/img/dashboard/vacantes/Sinfoto.svg';
  }

  mouseLeaveImagenPerfil() {
    this.imagenPerfil = '../../../../assets/img/dashboard/vacantes/sinFotoPerfilSmall.svg';
  }

  redirectTocCrear() {
    this.vacancyService.resetCurrentVacancy();
    this.router.navigateByUrl('dashboard/vacantes/create/(vacancy:description)');
  }

  redirectToVacantes() {
    this._menuSeleccionado.setBarraActiva(true);
    this._menuSeleccionado.setBarraActiva(true);
    this.router.navigate(['../../../../vacantes/activas'], {
      relativeTo: this.activatedRoute,
    });
  }

  inicializaBarraCrearVacante(): void {
    this.ocultar = this.ocultarEntrada;
    if (this.ocultar === 'ocultar') {
      this.paddintop = 'paddintop';
    }
  }

  inicializaArrayClientes(data: any): void {

    for (let i = 0; i < data.length; i++) {
      this.arrayClientes.push({
        idCliente: data[i].clientId,
        nombre: data[i].name
      });
    }
  }

  inicializaArrayReclutadores(data: any): void {
    for (let i = 0; i < data.length; i++) {
      this.arrayReclutadores.push({
        idReclutador: data[i].recruiterId,
        nombre: data[i].user.name + ' ' + data[i].user.lastName + ' ' + data[i].user.secondLastName
      });
    }
  }

  inicializarFormularioFltros() {
    this.formularioFiltros = this.formBuilder.group({
      recientes: [{ value: true, disabled: false }, []],
      antiguos: [{ value: false, disabled: false }, []],
      favoritos: [{ value: false, disabled: false }, []],
      rechazados: [{ value: false, disabled: false }, []],
      terna1: [{ value: false, disabled: false }, []],
      terna2: [{ value: false, disabled: false }, []],
      terna3: [{ value: false, disabled: false }, []],
      terna4: [{ value: false, disabled: false }, []],
      cien: [{ value: false, disabled: false }, []],
      setenta: [{ value: false, disabled: false }, []],
      cincuenta: [{ value: false, disabled: false }, []],
      videopresentacion: [{ value: false, disabled: false }, []],
      videoentrevista: [{ value: false, disabled: false }, []],
      minimo: [{ value: false, disabled: false }, []],
      maximo: [{ value: false, disabled: false }, []],
    });

    // this.formularioFiltros.controls['recientes'].setValue(true);
  }
  validarChecksFiltros1(check: string) {
    const recientes = this.formularioFiltros.get('recientes')?.value;
    const antiguos = this.formularioFiltros.get('antiguos')?.value;
    const favoritos = this.formularioFiltros.get('favoritos')?.value;
    const rechazados = this.formularioFiltros.get('rechazados')?.value;
    const terna1 = this.formularioFiltros.get('terna1')?.value;
    const terna2 = this.formularioFiltros.get('terna2')?.value;
    const terna3 = this.formularioFiltros.get('terna3')?.value;
    const terna4 = this.formularioFiltros.get('terna4')?.value;
    console.log('rena1', terna1);
    this.resetChecksFiltros1();
    let sortDirection = { filter: '', flag: '' };
    switch (check) {
      case 'recientes':
        this.formularioFiltros.controls.recientes.setValue(recientes);
        this.textoFiltro1 = 'Recientes';
        sortDirection = { filter: '&sortDirection=' + 'DESC', flag: 'ordenamiento' };
        break;
      case 'antiguos':
        this.formularioFiltros.controls.antiguos.setValue(antiguos);
        this.textoFiltro1 = 'Más antiguos';
        sortDirection = { filter: '&sortDirection=' + 'ASC', flag: 'ordenamiento' };
        break;
      case 'favoritos':
        this.formularioFiltros.controls.favoritos.setValue(favoritos);
        this.textoFiltro1 = 'Favoritos';
        sortDirection = { filter: '&favourite=true', flag: 'favoritos' };
        break;
      case 'rechazados':
        this.formularioFiltros.controls.rechazados.setValue(rechazados);
        this.textoFiltro1 = 'Rechazados';
        sortDirection = { filter: '&rejected=true', flag: 'favoritos' };
        break;
      case 'terna1':
        this.formularioFiltros.controls.terna1.setValue(terna1);
        this.textoFiltro1 = 'Terna 1';
        this.shortListFileter();
        // sortDirection = { filter: "&rejected=true", flag: false }
        break;
      case 'terna2':
        this.formularioFiltros.controls.terna2.setValue(terna2);
        this.textoFiltro1 = 'Terna 2';
        this.shortListFileter();
        // sortDirection = { filter: "&rejected=true", flag: false }
        break;
      case 'terna3':
        this.formularioFiltros.controls.terna3.setValue(terna3);
        this.textoFiltro1 = 'Terna 3';
        this.shortListFileter();
        // sortDirection = { filter: "&rejected=true", flag: false }
        break;
      case 'terna4':
        this.formularioFiltros.controls.terna4.setValue(terna4);
        this.textoFiltro1 = 'Terna 4';
        this.shortListFileter();
        // sortDirection = { filter: "&rejected=true", flag: false }
        break;
    }
    if (sortDirection.filter != '') {
      this.spreadfilter1.emit(sortDirection);
    }
  }

  resetChecksFiltros1() {
    this.formularioFiltros.controls.recientes.setValue(false);
    this.formularioFiltros.controls.antiguos.setValue(false);
    this.formularioFiltros.controls.favoritos.setValue(false);
    this.formularioFiltros.controls.rechazados.setValue(false);
    this.formularioFiltros.controls.terna1.setValue(false);
    this.formularioFiltros.controls.terna2.setValue(false);
    this.formularioFiltros.controls.terna3.setValue(false);
    this.formularioFiltros.controls.terna4.setValue(false);
  }

  validarChecksFiltros2(check: string) {
    const cien = this.formularioFiltros.get('cien')?.value;
    const setenta = this.formularioFiltros.get('setenta')?.value;
    const cincuenta = this.formularioFiltros.get('cincuenta')?.value;
    let compatibility = '&compatibility=';
    this.resetChecksFiltros2(false);
    switch (check) {
      case 'cien':
        this.formularioFiltros.controls.cien.setValue(cien);
        this.textoFiltro2 = '100 - 70%';
        compatibility = '&compatibility=' + 'ALTA';
        break;
      case 'setenta':
        this.formularioFiltros.controls.setenta.setValue(setenta);
        this.textoFiltro2 = '70 - 50%';
        compatibility = '&compatibility=' + 'MEDIA';
        break;
      case 'cincuenta':
        this.formularioFiltros.controls.cincuenta.setValue(cincuenta);
        this.textoFiltro2 = 'Menos de 50%';
        compatibility = '&compatibility=' + 'BAJA';
        break;
    }
    this.spreadfilter2.emit(compatibility);

  }

  resetChecksVideo(reset: boolean) {
    if (reset) {
      this.spreadfilterVideo.emit('');
    }
    this.formularioFiltros.controls.videopresentacion.setValue(false);
    this.formularioFiltros.controls.videoentrevista.setValue(false);
    (reset) ? this.textoVideo = 'Video' : reset = false;
  }

  validarChecksVideo(check: string) {
    const videopresentacion = this.formularioFiltros.get('videopresentacion')?.value;
    const videoentrevista = this.formularioFiltros.get('videoentrevista')?.value;
    let filterVideo = '';
    this.resetChecksVideo(false);
    switch (check) {
      case 'videopresentacion':
        this.formularioFiltros.controls.videopresentacion.setValue(videopresentacion);
        this.textoVideo = 'Video presentación';
        filterVideo = '&presentation=true';
        break;
      case 'videoentrevista':
        this.formularioFiltros.controls.videoentrevista.setValue(videoentrevista);
        this.textoVideo = 'Video entrevista';
        filterVideo = '&interview=true';
        break;
    }

    this.spreadfilterVideo.emit(filterVideo);
  }

  resetChecksFiltros2(reset: boolean) {
    if (reset) {
      this.spreadfilter2.emit('');
    }
    this.formularioFiltros.controls.cien.setValue(false);
    this.formularioFiltros.controls.setenta.setValue(false);
    this.formularioFiltros.controls.cincuenta.setValue(false);
    (reset) ? this.textoFiltro2 = 'Compatibilidad' : reset = false;
  }



  openModalMasFiltros() {
    this.modalService.open(this.modalMasFiltros, { scrollable: true });
  }


  inicializarArregloMasFiltros(data: any) {
    let contador = 0;
    const arrayTem = [];
    for (const i of data) {
      contador++;
      const tem = String(i).split(',');
      arrayTem.push(
        {
          nombrePortafolio: (tem[1] == 'URL_FILE') ? 'Enlace a sitio o archivo online' : (tem[1] == 'URL_GIT') ? 'GitHub' : (tem[1] == 'URL_FOLDER') ? 'Archivo adjunto' :
            (tem[1] == 'URL_BEHANCED') ? 'Behance' : (tem[1] == 'URL_LINKED') ? 'LinkedIn' : (tem[1] == 'URL_CV') ? 'CV' : '',
          nameService: tem[1],
          numero: tem[0],
          id: 'portafolio' + contador,
          check: false
        }
      );
    }
    if (arrayTem.length > 0){
      this.arrayMasFiltros.push({
        nombreFiltro: 'Portafolio',  // Es el nombre del filtro
        columnasModal: 'row-cols-2', // para mostrar los checks en dos columnas
        mostrarTodo: false, // para habilitar el boton de mostrar mas
        portafolio: arrayTem
      });
    }
  }

  inicializarArregloMasFiltrosUbicacion(data: any) {
    let contador = 0;
    const arrayTem = [];
    for (const i of data) {
      contador++;
      const tem = String(i).split(',');
      arrayTem.push(
        {
          nombrePortafolio: tem[1],
          nameService: tem[1],
          numero: tem[0],
          id: 'ubicacion' + contador,
          check: false
        }
      );
    }if (arrayTem.length > 0){
      this.arrayMasFiltros.push({
        nombreFiltro: 'Ubicación',
        columnasModal: 'row-cols-2',
        mostrarTodo: false,
        portafolio: arrayTem
      });
    }
  }

  inicializarArregloMasFiltrosGenero(data: any) {
    let contador = 0;
    const arrayTem = [];
    for (const i of data) {
      contador++;
      const tem = String(i).split(',');
      const name = (tem[1] == 'WOMAN') ? 'Femenino' : (tem[1] == 'MAN') ? 'Masculino' : (tem[1] == 'NO_BINARY') ? 'No binario' : 'Indistinto';
      arrayTem.push(
        {
          nombrePortafolio: name,
          nameService: tem[1],
          numero: tem[0],
          id: 'genero' + contador,
          check: false
        }
      );
    }
    if (arrayTem.length > 0){
      this.arrayMasFiltros.push({
        nombreFiltro: 'Género',
        columnasModal: 'row-cols-2',
        mostrarTodo: false,
        portafolio: arrayTem
      });
    }
  }

  inicializarArregloMasFiltrosNivel(data: any) {
    let contador = 0;
    const arrayTem = [];
    for (const i of data) {
      contador++;
      const tem = String(i).split(',');
      arrayTem.push(
        {
          nombrePortafolio: tem[1],
          nameService: tem[1],
          numero: tem[0],
          id: 'nivel' + contador,
          check: false
        }
      );
    }
    if (arrayTem.length > 0){
      this.arrayMasFiltros.push({
        nombreFiltro: 'Nivel de estudios',
        columnasModal: 'row-cols-2',
        mostrarTodo: false,
        portafolio: arrayTem
      });
    }
  }

  inicializarArregloMasFiltrosNacionalidad(data: any) {
    let contador = 0;
    const arrayTem = [];
    for (const i of data) {
      contador++;
      const tem = String(i).split(',');
      arrayTem.push(
        {
          nombrePortafolio: tem[1],
          nameService: tem[1],
          numero: tem[0],
          id: 'nacionalidad' + contador,
          check: false
        }
      );
    }
    if (arrayTem.length > 0){
      this.arrayMasFiltros.push({
        nombreFiltro: 'Nacionalidad',
        columnasModal: 'row-cols-2',
        mostrarTodo: false,
        portafolio: arrayTem
      });
    }
  }

  inicializarArregloMasFiltrosInstituciones(data: any) {
    let contador = 0;
    const arrayTem = [];
    for (const i of data) {
      contador++;
      const tem = String(i).split(',');
      arrayTem.push(
        {
          nombrePortafolio: tem[1],
          nameService: tem[1],
          numero: tem[0],
          id: 'instituciones' + contador,
          check: false
        }
      );
    }
    if (arrayTem.length > 0){
      this.arrayMasFiltros.push({
        nombreFiltro: 'Instituciones',
        columnasModal: 'row-cols-1',
        mostrarTodo: false,
        portafolio: arrayTem
      });
    }
  }

  updateChecksPortafolio(idCheck: any, nombreFiltro: string) {
    for (let j = 0; j < this.arrayMasFiltros.length; j++) {
      const portafolio = this.arrayMasFiltros[j];
      for (let i = 0; i < portafolio.portafolio.length; i++) {
        if (portafolio.portafolio[i].id == idCheck && (portafolio.portafolio[i].nombrePortafolio != 'Indistinto')) {
          portafolio.portafolio[i].check = !portafolio.portafolio[i].check;
          (portafolio.portafolio[i].check) ? this.contadorMasFiltros++ : this.contadorMasFiltros--;
          break;
        } else {
          if (portafolio.nombreFiltro == 'Género' && portafolio.portafolio[i].nombrePortafolio == 'Indistinto' && nombreFiltro == 'Indistinto') {
            portafolio.portafolio[i].check = !portafolio.portafolio[i].check;
            (portafolio.portafolio[i].check) ? this.contadorMasFiltros++ : this.contadorMasFiltros--;
            if (portafolio.portafolio[i].check) {
              for (let i = 0; i < portafolio.portafolio.length; i++) {
                if (portafolio.portafolio[i].nombrePortafolio != 'Indistinto') {
                  if (portafolio.portafolio[i].check) {
                    this.contadorMasFiltros--;
                  }
                  portafolio.portafolio[i].check = false;
                }
              }
            }
          }

        }
      }
    }
  }

  mostrarTosdos(nombre: string) {
    for (let i = 0; i < this.arrayMasFiltros.length; i++) {
      if (this.arrayMasFiltros[i].nombreFiltro == nombre) {
        this.arrayMasFiltros[i].mostrarTodo = !this.arrayMasFiltros[i].mostrarTodo;
      }
    }
  }

  aplicarFiltros() {
    this.ContadorFiltrosActuales();
    const filtrosSeleccionados = this.arrayMasFiltros;
    console.log(filtrosSeleccionados); // aqui estan todos los filtros seleccionados
    const moreFilters = { filterURL: '', filterState: '', filterGender: '', filterLevelEducation: '', filterNationality: '', filterInstitution: '' };
    for (const filter of filtrosSeleccionados) {
      switch (filter.nombreFiltro) {
        case 'Portafolio':
          moreFilters.filterURL = (this.generateQueryMoreFilters(filter.portafolio) == '') ? '' : '&filterURL=' + this.generateQueryMoreFilters(filter.portafolio);
          break;
        case 'Ubicación':
          moreFilters.filterState = (this.generateQueryMoreFilters(filter.portafolio) == '') ? '' : '&filterState=' + this.generateQueryMoreFilters(filter.portafolio);
          break;
        case 'Género':
          moreFilters.filterGender = (this.generateQueryMoreFilters(filter.portafolio) == '') ? '' : '&filterGender=' + this.generateQueryMoreFilters(filter.portafolio);
          break;
        case 'Nivel de estudios':
          moreFilters.filterLevelEducation = (this.generateQueryMoreFilters(filter.portafolio) == '') ? '' : '&filterLevelEducation=' + this.generateQueryMoreFilters(filter.portafolio);
          break;
        case 'Nacionalidad':
          moreFilters.filterNationality = (this.generateQueryMoreFilters(filter.portafolio) == '') ? '' : '&filterNationality=' + this.generateQueryMoreFilters(filter.portafolio);
          break;
        case 'Instituciones':
          moreFilters.filterInstitution = (this.generateQueryMoreFilters(filter.portafolio) == '') ? '' : '&filterInstitution=' + this.generateQueryMoreFilters(filter.portafolio);
          break;
      }
    }
    this.spreadMorefilters.emit(moreFilters);
  }

  generateQueryMoreFilters(array: any) {
    let query = '';
    for (const filter of array) {
      if (filter.check) {
        if (query == '') {
          query = filter.nameService;
        } else {
          query = query + '%26' + filter.nameService;
        }
      }
    }
    return query;
  }
  quitarMasFiltros() {
    for (let j = 0; j < this.arrayMasFiltros.length; j++) {
      const portafolio = this.arrayMasFiltros[j];
      for (let i = 0; i < portafolio.portafolio.length; i++) {
        portafolio.portafolio[i].check = false;
      }
    }
    this.contadorMasFiltros = 0;
    const moreFilters = { filterURL: '', filterState: '', filterGender: '', filterLevelEducation: '', filterNationality: '', filterInstitution: '' };
    this.spreadMorefilters.emit(moreFilters);
  }

  quitarMasFiltrosGenero() {
    for (let j = 0; j < this.arrayMasFiltros.length; j++) {
      const portafolio = this.arrayMasFiltros[j];
      if (this.arrayMasFiltros[j].nombreFiltro == 'Género') {
        for (let i = 0; i < portafolio.portafolio.length; i++) {
          if (portafolio.portafolio.nombrePortafolio !== 'Indistinto') {
            portafolio.portafolio[i].check = false;
          }
        }
      }
    }
    this.contadorMasFiltros = 0;
  }

  ContadorFiltrosActuales() {
    let contadorTemp = 0;
    for (let j = 0; j < this.arrayMasFiltros.length; j++) {
      const portafolio = this.arrayMasFiltros[j];
      for (let i = 0; i < portafolio.portafolio.length; i++) {
        if (portafolio.portafolio[i].check) {
          contadorTemp++;
        }
      }
    }
    this.contadorMasFiltros = contadorTemp;
  }

  filtroEdad() {
    this.textoFiltro3 = '' + this.value + '-' + this.highValue + ' ' + 'años';
    this.spreadfilterAge.emit('&ageMaximum=' + this.highValue + '&ageMinimum=' + this.value);
  }

  resetFiltroEdad() {
    this.textoFiltro3 = 'Edad';
    this.spreadfilterAge.emit('');
  }

  filtroSueldo() {
    this.textoFiltro4 = '';
    this.textoSueldoMenor = this.valueSueldo;
    this.textosueldoMayor = this.highValueSueldo;
    this.spreadfilterSalary.emit('&salaryMaximum=' + this.highValueSueldo + '&salaryMinimum=' + this.valueSueldo);
  }

  resetFiltroSueldo() {
    this.textoFiltro4 = 'Sueldo deseado';
    this.textoSueldoMenor = 0;
    this.textosueldoMayor = 0;
    this.spreadfilterSalary.emit('');
  }

  logout(): void {
    this.authService.logOutRecuitre(this.notification.getTokenNotification()).subscribe(
      (response: any) => {
        localStorage.clear();
        this._suscription.setlastCompletedStep(0, true);
        document.cookie.split(';').map((el) => el.split('=')[0]).forEach((el) => {
          this.cookieService.delete(el.trim(), '/');
        });
        this.sessionService.setToken(null);
        this.sessionService.setTokenExpiration(null);
        this.sessionService.closeSessionChecker();
        this.router.navigateByUrl('/login');
      },
      (error) => {
        console.error(error);
      }
    );
  }

  getClientes() {
    this._sideNavService.getClientes().subscribe(
      (response) => {
        if (response != null && response.status == 200 && response.body != null && response.body.length > 0) {
          this.inicializaArrayClientes(response.body);
        }
      },
      (error) => {
        console.log('error en el servicio', error);
      }
    );
  }

  getReclutadores() {
    this._sideNavService.getReclutadores().subscribe(
      (response) => {
        if (response != null && response != '' && response.body?.length > 0) {
          this.inicializaArrayReclutadores(response.body);
        }
      },
      (error) => {
        console.log('error en el servicio', error);
      }
    );
  }

  getFiltroCliente(event: any): void {
    if (event == 'Cliente') {
      event = '';
    }
    this.propagarFiltroCliente.emit(event);
  }

  getFiltroReclutador(event: any) {
    if (event == 'Todos los reclutadores') {
      event = '';
    }
    this.propagarFiltroReclutador.emit(event);
  }


  // inicializarBuscador() {
  //   if ((this.vacantSearchInput !== null && this.vacantSearchInput !== undefined)) {
  //     fromEvent(this.vacantSearchInput.nativeElement, 'keyup').pipe(
  //       map((event: any) => {
  //         return event.target.value;
  //       })
  //       , debounceTime(600)
  //       , distinctUntilChanged()
  //     ).subscribe((text: string) => {
  //       <this.spreadVacantSearchInput.emit(text);>
  //     });
  //   }
  // }
  propagarBUsqueda = (text: any) => {
    this.spreadVacantSearchInput.emit(text);
  }
  updateConuntersVacant() {
    this.countersVacant$ = this._sideNavService.getCountersVacant$();
    this.countersVacant$.subscribe(clientes => {
      this.actualizarContadores();
    });
  }

  actualizarContadores() {
    this.contadorActivas = Number(this.authService.getVacantActive());
    this.contadorEnPausa = Number(this.authService.getVacantPause());
    this.contadorBorradores = Number(this.authService.getVacantDrafts());
    this.contadorCerradas = Number(this.authService.getVacantClosed());
  }

  selectionProcessCounter() {
    const idVacant = this.selectdedCard.id; // "2c9f9e9679eee9720179ef3087030001"
    this._processSelection.candidateCounter(idVacant).subscribe(
      (response: any) => {
        if (response != null) {
          this.contadorCandidatos = response[2][0][1];
          this.contadorPruebas = response[3][0][1];
          // this.contadorEntrevistas = response[1][0][1];
          this.contadorFinalistas = response[1][0][1];
          this.contadorContratados = response[0][0][1];
          this.averageAge = parseInt(response[4][0][1]);
          this.averageSalary = parseInt(response[5][0][1]);
        }
      },
      (error: any) => {

      }
    );
  }

  firstFilterCatalog() {
    this._processSelection.firstFilterCatalog().subscribe(
      (response: any) => {
        this.arrayFilter1 = [];
        if (response != null) {
          if (this.status == 'ENTREVISTA') {
            this.getShortList(false);
          }
          for (const nivel1 of response) {
            for (const nivel2 of nivel1) {
              const nameTem = (nivel2[1] != undefined || nivel2[1] != null) ? String(nivel2[1]).toLowerCase() : '';
              this.arrayFilter1.push({
                name: nivel2[1],
                nameForm: (nameTem != '' && String(nameTem).includes('antiguos')) ? 'antiguos' : nameTem
              });
            }
          }
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  secondFilterCatalog() {
    this._processSelection.secondFilterCatalog().subscribe(
      (response: any) => {
        this.arrayFilter2 = [];
        if (response != null) {
          for (const nivel1 of response) {
            for (const nivel2 of nivel1) {
              const temp = (nivel2[0] != undefined || nivel2[0] != null) ? String(nivel2[0]).toUpperCase() : '';
              let nameForm = '';
              nameForm = String(temp).includes('DE_100_A_70') ? 'cien' : String(temp).includes('DE_70_A_50') ? 'setenta' : 'cincuenta';
              this.arrayFilter2.push({
                name: nivel2[1],
                nameForm,
                value: nivel2[0]
              });
            }
          }
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  filterRange() {
    const params = {
      status: this.status,
      vacantId: this.selectdedCard.id
    };
    this._processSelection.filterRange(params).subscribe(
      (response: any) => {
        if (response != null) {
          this.highValueSueldo = (response.maximimSalary != null) ? response.maximimSalary : 0;
          this.valueSueldo = (response.minimumSalary != null) ? response.minimumSalary : 0;
          this.value = (response.minimumAge != null) ? response.minimumAge : 0;
          this.highValue = (response.maximumAge != null) ? response.maximumAge : 0;
          this.options = {
            floor: this.value,
            ceil: this.highValue
          };
          this.options1 = {
            floor: this.valueSueldo,
            ceil: this.highValueSueldo
          };
        }
      },
      (error) => {

      }
    );
  }

  moreFilters = () => {
    this._processSelection.moreFilters(this.selectdedCard.id).subscribe(
      (response: any) => {
        if (response != null) {
          // console.log("es el response de mas filtros", response);
          this.inicializarArregloMasFiltros(response[0]);
          this.inicializarArregloMasFiltrosUbicacion(response[1]);
          this.inicializarArregloMasFiltrosGenero(response[2]);
          this.inicializarArregloMasFiltrosNivel(response[3]);
          this.inicializarArregloMasFiltrosNacionalidad(response[4]);
          this.inicializarArregloMasFiltrosInstituciones(response[5]);

        }
      }
    );
  }

  getShortList = (aplyFilter: boolean) => {
    const params = {
      page: 0,
      size: 10,
      vacant: this.selectdedCard.id,
    };
    this._processSelection.getShortList(params).subscribe(
      (response: any) => {
        console.log('es l aresponse de get ternas', response);
        if (response != null) {
          const data = response.content;
          this.shortList = data;
          for (const shortList of data) {
            let exist = false;
            for (const filter of this.arrayFilter1) {
              if (filter.name == shortList.name) {
                exist = true;
              }
            }
            if (!exist) {
              this.arrayFilter1.push({
                name: shortList.name,
                nameForm: String(shortList.name).replace(/ /g, '').toLowerCase()
              });
            }
          }
          if (aplyFilter) {
            this.textoFiltro1 = this.arrayFilter1[this.arrayFilter1.length - 1].name;
            this.formularioFiltros.controls['terna' + data.length].setValue(true);
            this.formularioFiltros.controls.recientes.setValue(false);
            this.shortListFileter();
          }
          console.log('es el arreglo de ternas', this.arrayFilter1);
        }
      },
      (error) => {
        console.log('error', error);
      }
    );
  }

  shortListFileter = () => {
    const array = [];
    for (const list of this.shortList) {
      if (this.textoFiltro1 == list.name) {
        array.push(list.firstProcess);
        array.push(list.secondProcess);
        array.push(list.thirdProcess);
      }
    }
    const sortDirection = { filter: array, flag: 'terna' };
    this.spreadfilter1.emit(sortDirection);
  }

}
