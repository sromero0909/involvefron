import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../dashboard/dashboard.component';

const routes: Routes = [
    {
        path: 'seleccion',
        loadChildren: () => import('../selectionProcess/process/process-stages.module').then(
            m => m.processStagesModule
        )
    }

];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})

export class SelectionProcessRoutingModule { }