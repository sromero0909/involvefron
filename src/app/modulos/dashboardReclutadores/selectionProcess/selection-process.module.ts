import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectionProcessRoutingModule } from './selection-process-routing.moduel';
import { ModalReclutadoresComponent } from '../vacantes/modal-reclutadores/modal-reclutadores.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { InternalizacionModule } from 'src/app/commons/internalizacion/internalizacion.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { moduleSharedModule } from 'src/app/commons/commons.module';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { DashboardModule } from '../dashboard.module';




@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    SelectionProcessRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbTooltipModule,
    AngularEditorModule,
    InternalizacionModule,
    NgbModule,
    NgxSpinnerModule,
    NgxSliderModule,
    moduleSharedModule,
    DashboardModule
  ]
})
export class SelectionProcessModule { }
