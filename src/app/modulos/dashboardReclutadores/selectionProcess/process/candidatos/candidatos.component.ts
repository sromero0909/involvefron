import { Component, OnInit, ViewChild, TemplateRef, ElementRef, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { Observable, OperatorFunction } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap } from 'rxjs/operators';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { VacantesService } from 'src/app/services/reclutador/vacantes/vacantes.service';
import { CandidatesService } from '../../../../../services/reclutador/vacantes/selectionProcess/candidatos.service';
import { VacantCard } from 'src/app/services/models/cardVacants.model';
import { SidenavVacantesComponent } from '../../../sidenav-vacantes/sidenav-vacantes.component';
import { formatDate } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';

@Component({
  selector: 'app-candidatos',
  templateUrl: './candidatos.component.html',
  styleUrls: ['./candidatos.component.scss']
})
export class CandidatosComponent implements OnInit {

  mensajeToast: string = "";
  sortBy: string = "&sortBy=lastUpdate"
  sortDirection: string = "&sortDirection=DESC";
  compatibility: string = ""

  selectedCard: VacantCard
  rows: Array<any> = [];
  arrayRecruiters: Array<any> = []
  arraryWhachers: Array<any> = []
  statusTestPsychometric: boolean = false;
  banderaCerrarVacante: boolean = false;
  banderaAprobar: boolean = false;
  approveSelected: boolean = false;
  rejectSelected: boolean = false;
  searchFlag: boolean = false;
  rejectFlag: boolean = false;
  nextFlag: boolean = false;
  selecAllinvitation = false;
  shortListActive: boolean = false;
  flagMacth: boolean = false;
  flagPagination: boolean = false;
  flagService: boolean = false;
  public modelBuscarVacante: any = "";
  textFilter: string = "";
  filterURL: string = "";
  filterAge: string = "";
  filterSalary: string = "";
  otherFilter: string = "&rejected=false"; //&favourite=false
  filterState: string = "";
  filterGender: string = "";
  filterLevelEducation: string = "";
  filterNationality: string = "";
  filterInstitution: string = "";
  filterVideo: string = "";
  selectionProcessId: string = "";
  textSelect: string = "Seleccionar";
  optionAll: string = "A"
  formularioModalBuscar: any;
  informationCandidate = { phone: "", email: "", git: "", linkedIn: "", file: "", behance: "" }

  page = 1
  page_size: number = 9;
  page_number: number = 0;
  pageTemporal: number = 0;
  totalElements: number = 0;
  totalPages: number = 0;
  rowsMostrar: number = 0;
  pageActual: number = 0;
  restarRows: number = 0;
  rowsPorPagina = 0;
  contadorChecks = 0;

  @ViewChild('modalshowRecruiters', { static: true }) modalshowRecruiters!: TemplateRef<any>;
  @ViewChild('rechazar', { static: true }) modalRechazar!: TemplateRef<any>;
  @ViewChild('confirmacionRechazo', { static: true }) confirmacionRechazo!: TemplateRef<any>;
  @ViewChild('aprobar', { static: true }) aprobar!: TemplateRef<any>;
  @ViewChild('confirmacionAprobacion', { static: true }) confirmacionAprobacion!: TemplateRef<any>;
  @ViewChild('invitar', { static: true }) invitar!: TemplateRef<any>;
  @ViewChild('informacionContacto', { static: true }) informacionContacto!: TemplateRef<any>;
  @ViewChild("selector1") updateCounters!: SidenavVacantesComponent;
  @ViewChild('iconoChekBlanco', { static: true }) iconoChekBlanco!: TemplateRef<any>;
  @Input() initialText = "Por ahora no hay candidatos";
  @Input() status: string;
  @Input() setProcessSelection: string
  search: OperatorFunction<string, readonly { name: string }[]> = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term => term.length < 3 ? [] :
        this._candidates.searchVacancy(term).pipe(
          map((response: any) => {
            buscarVacante = [];
            if (response != undefined && response != null && response.content.length > 0) {
              const data = response.content
              for (let i of data) {
                buscarVacante.push({
                  name: i.vacant.position.position + '-' + i.vacant.client.name,
                  idVacant: i.vacant.vacantId,
                })
              }
              this.modelBuscarVacante = buscarVacante;
            }
            if (buscarVacante == "") {
              buscarVacante = [];
              buscarVacante.push({ name: "No se encontraron resultados" });
            }
            return buscarVacante
          }),

        )))
  formatter = (x: { name: string }) => x.name;
  constructor(private _menuSeleccionado: VacantesService,
    private toastService: ToastService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private _candidates: CandidatesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private profileService: ProfileService
  ) {
    this.selectedCard = this._candidates.getSelectedCard();
    this.setProcessSelection = "candidatos"
    this.status = "NUEVO"
  }

  ngOnInit(): void {
    this._menuSeleccionado.setProcesoSeleccion(this.setProcessSelection);
    if (this.selectedCard != undefined) {
      const flagRoute = this.activatedRoute.snapshot.paramMap.get("flag")
      const flag = (flagRoute == 'false') ? false : true
      if (flag) {
        this.rows = this._candidates.getRowsTemporal()
        this.setProcessSelection = this.profileService.getSelectedCard().setProcessSelection;
        this.status = this.profileService.getSelectedCard().status;
        this.totalElements = this.profileService.getSelectedCard().totalElemt;
        this.totalPages = this.profileService.getSelectedCard().totalPages;
        // this.updateRowsMOstradas(this.profileService.getSelectedCard().pageActual)
        this.page_number = this.profileService.getSelectedCard().pageActual - 1
        this.pageActual = this.profileService.getSelectedCard().pageActual
        this.rowsMostrar = this.profileService.getSelectedCard().rowsMostrar
        this.selectPage(String(this.profileService.getSelectedCard().pageActual));
      } else {
        this.inicializaCards();
      }

      this.inicializarFormularioBuscar();
      //this.onchangeBUscarVacante();
    }
  }
  ngAfterViewInit() {
    this._menuSeleccionado.setBarraActiva(false);
    this._menuSeleccionado.setBarraActiva(false);
  }

  ngOnDestroy() {
    this._menuSeleccionado.setBarraActiva(true);
    this._menuSeleccionado.setBarraActiva(true);
  }

  inicializaCards() {
    this.flagService = false;
    //let params = `page=${this.page_number}&pageSize=${this.page_size}&pageNumber=${this.page_number}${this.orderBy}${this.sortDirection}&vacantId=${this.selectedCard.id}${this.textFilter}`;
    let params = {
      pageNumber: this.page_number,
      pageSize: parseInt(String(this.page_size)) + 1,
      sortBy: this.sortBy,
      sortDirection: this.sortDirection,
      status: this.status,
      vacantId: this.selectedCard.id,
      wordSearch: this.textFilter,
      filterAge: this.filterAge,
      filterSalary: this.filterSalary,
      otherFilter: this.otherFilter,
      compatibility: this.compatibility,
      filterState: this.filterState,
      filterGender: this.filterGender,
      filterLevelEducation: this.filterLevelEducation,
      filterNationality: this.filterNationality,
      filterInstitution: this.filterInstitution,
      filterURL: this.filterURL,
      filterVideo: this.filterVideo
    }
    this._candidates.getVacantForStatus(params).subscribe(
      (response) => {
        if (response != null) {

          if (!this.flagPagination) {
            this.resetPaginadores();
            this.flagPagination = false
          }
          let candidatos = response.content
          this.fillTable(candidatos)
        } else {
          this.showMessageError("Ocurrio un error");
        }

        this.totalElements = response.totalElements;
        this.totalPages = response.totalPages;
        if (this.pageActual == 0) {
          this.updateRowsMOstradas(this.pageActual + 1)
        } else {
          this.updateRowsMOstradas(this.pageActual)
        }
        setTimeout(() => {
          this.flagService = true;
        }, 100)
      },
      (error) => {
        this.showMessageError("Ocurrio un error");
      }
    );
  }

  fillTable = (candidatos: any) => {
    for (const candidato in candidatos) {
      this.openModalInformacionContacto(candidatos[candidato].candidate, false)
      this.rows.push({
        id: candidatos[candidato].selectionProcessId,
        check: false,
        porcentaje: parseInt(candidatos[candidato].matchPorcentage) + "%",
        colorPorcentaje: this.coloresPorcentaje(parseInt(candidatos[candidato].matchPorcentage) + "%"),
        tiempo: this.formatDate(candidatos[candidato].lastUpdate),
        favorito: candidatos[candidato].isFavourite,
        nombre: (candidatos[candidato].candidate.user.name != null) ? candidatos[candidato].candidate.user.name + " " + candidatos[candidato].candidate.user.lastName + " " + candidatos[candidato].candidate.user.secondLastName : "-",
        licenciatura: (candidatos[candidato].candidate.positionSearch != null) ? candidatos[candidato].candidate.positionSearch : "-",
        sueldo: (candidatos[candidato].candidate.desiredSalary != null) ? candidatos[candidato].candidate.desiredSalary : '0',
        edad: (candidatos[candidato].candidate.age != null) ? candidatos[candidato].candidate.age + " años" : '-',
        ubicacion: (candidatos[candidato].candidate.statePosition != null) ? candidatos[candidato].candidate.statePosition.concat((candidatos[candidato].candidate.countryPosition != null) ? "," + candidatos[candidato].candidate.countryPosition : '') : "-",
        photo: (candidatos[candidato].candidate.user.photo != null) ? candidatos[candidato].candidate.user.photo : "../../../../assets/img/dashboard/vacantes/sinFotoPerfil.svg",
        presentationVideo: (candidatos[candidato].presentationVideo == null) ? false : candidatos[candidato].presentationVideo,
        interviewPresentation: (candidatos[candidato].interviewVideo == null) ? false : candidatos[candidato].interviewVideo,
        candidate: candidatos[candidato].candidate,
        statusTerna: candidatos[candidato].statusTerna,
        reject: candidatos[candidato].isRejected,
        flagMacth: (candidatos[candidato].isWarning == null) ? false : candidatos[candidato].isWarning,
        urls: this.informationCandidate,
        data: candidatos[candidato],
        whachers: this.showRecruiters(candidatos[candidato].watchers),
        canBeApproved: ((candidatos[candidato].psychometricTests && 
                        candidatos[candidato].testsPsychometricComplete && 
                        candidatos[candidato].pathInterviewVideo !== null && 
                        candidatos[candidato].candidate.pathPresentationVideo !== null &&
                        candidatos[candidato].status === 'PRUEBAS_Y_VIDEO') ||
                        candidatos[candidato].status !== 'PRUEBAS_Y_VIDEO')
      })
    }
  }

  updateFavorito(id: any) {
    this._candidates.updateFavorite(id).subscribe(
      (response) => {
        if (response != null) {
          for (let i of this.rows) {
            if (id == i.id) {
              i.favorito = response
            }
          }
        } else {
          this.showMessageError("Ocurrio un error");
        }
      },
      (error) => {
        this.showMessageError("Ocurrio un error");
      })
  }

  generateArrarSendReminder(template: any, id: string, flag: boolean, presentationVideo: boolean, interview: boolean) {
    let arrayProcessId = []
    let type = "";
    if (flag) {
      for (let card of this.rows) {
        if (card.check) {
          type = (!card.presentationVideo) ? "PRESENTATION" : (card.presentationVideo && !card.interviewPresentation) ? "INTERVIEW" : ""
          if (type != "") {
            arrayProcessId.push({
              "processId": card.id,
              "type": type
            })
          }
        }
      }
    } else {
      type = (!presentationVideo) ? "PRESENTATION" : (presentationVideo && !interview) ? "INTERVIEW" : ""
      arrayProcessId.push({
        "processId": id,
        "type": "PRESENTATION"
      })
    }

    if (arrayProcessId.length == 1) {
      flag = false
    }
    //this.sendReminder(template, id, flag)
    if (arrayProcessId.length > 0) {
      this.sendReminder(template, arrayProcessId, flag)
    }

  }
  sendReminder(template: any, id: any, flag: boolean) {
    this._candidates.sendReminder(id).subscribe(
      (response) => {
        if (response != null && response) {
          this.contadorChecks = 0;
          this.mensajeToast = (flag) ? '"Enviamos tu recordatorio a los candidatos"' : "Enviamos tu recordatorio al candidato"
          this.setMensajeRecoratorio(template,)
        } else {
          this.showMessageError("Ocurrio un error");
        }
      },
      (error) => {
        console.log(error);
        this.showMessageError("Ocurrio un error");
      }
    )
  }
  openModalRechazar(id: string, rejectSelected: boolean) {
    this.selectionProcessId = id;
    this.rejectSelected = rejectSelected;
    this.modalService.open(this.modalRechazar, { centered: true });
  }

  openModalconfirmacionRechazoCandidato(bandera: boolean, rejectFlag: boolean) {
    this.banderaCerrarVacante = bandera;
    this.rejectFlag = rejectFlag;
    this.modalService.open(this.confirmacionRechazo, { centered: true });
  }

  openModalAprobar(id: string, approveSelected: boolean) {
    this.selectionProcessId = id;
    this.approveSelected = approveSelected;
    this.modalService.open(this.aprobar, { centered: true });
  }

  openModalconfirmacionAprobacion(bandera: boolean, nextFlag: boolean) {
    this.nextFlag = nextFlag;
    this.banderaAprobar = bandera;
    this.modalService.open(this.confirmacionAprobacion, { centered: true });
  }

  openModalInvitar(id: string, flag: boolean) {
    this.selectionProcessId = id;
    this.selecAllinvitation = flag
    this.modalService.open(this.invitar, { size: "lg", centered: true });
  }

  openModalInformacionContacto(data: any, OpenModal: boolean) {
    this.informationCandidate.phone = data.user.phone = (data.user.phone != null) ? data.user.phone : ""
    this.informationCandidate.email = data.user.email
    this.informationCandidate.git = "";
    this.informationCandidate.linkedIn = "";
    this.informationCandidate.file = "";
    this.informationCandidate.behance = ""
    for (let user of data.urlFiles) {
      switch (user.type) {
        case "URL_GIT":
          this.informationCandidate.git = user.pathFile;
          break;
        case "URL_LINKED":
          this.informationCandidate.linkedIn = user.pathFile;
          break;
        case "URL_FOLDER":
          this.informationCandidate.file = user.pathFile;
          break;
      }
    }
    if (OpenModal) {
      this.modalService.open(this.informacionContacto, { size: "lg", centered: true });
    }
  }

  openModalShowRecltadores(whachers: any) {
    this.arraryWhachers = whachers
    this.modalService.open(this.modalshowRecruiters, { centered: true, windowClass: 'dark-modal' });
  }

  showSuccess(template: any) {
    this.toastService.remove(1)
    this.toastService.show(template, { classname: 'bg-success text-light ngb-toastsBottom', delay: 5000 });
  }

  setMensajeToast(template: any, flagPlural: boolean) {
    if (this.banderaCerrarVacante) {
      this.mensajeToast = (flagPlural) ? "Por ahora los candidatos no serán notificados de tu decisión" : "Por ahora el candidato no será notificado de tu decisión"
    } else {
      this.mensajeToast = (flagPlural) ? "Enviamos un mensaje  de agredecimiento a los candidatos" : "Enviamos un mensaje  de agredecimiento al candidato"
    }
    this.showSuccess(template);
  }

  setMensajeToastAprobar(template: any) {
    if (this.banderaAprobar) {
      this.mensajeToast = "Enviamos un mensaje de felicitación al candidato"
    } else {
      this.mensajeToast = "Por ahora el candidato no será notificado de tu decisión"
    }
    this.showSuccess(template);
  }

  setMensajeRecoratorio(template: any) {
    this.showSuccess(template);
  }

  setMensajeNotificacion(template: any, flagPlural: boolean) {
    this.mensajeToast = (flagPlural) ? "Por ahora los candidatos no serán notificados de tu decisión" : "Por ahora el candidato no será notificado de tu decisión"
    this.showSuccess(template);
  }

  setMensajeNotificacionFelicitacion(template: any, flagPlural: boolean) {
    this.mensajeToast = (flagPlural) ? "Enviamos un mensaje de felicitación a los candidatos" : "Enviamos un mensaje de felicitación al candidato"
    this.showSuccess(template);
  }
  inicializarFormularioBuscar() {
    this.formularioModalBuscar = this.formBuilder.group({
      modelBuscarVacante: ['', []],
    });
  }

  onchangeBUscarVacante() {
    this.formularioModalBuscar.get("modelBuscarVacante")?.valueChanges.subscribe((changes1: any) => {
      this.onPercentChange(changes1);
    });
  }
  onPercentChange(percent: any) {
    if (percent != '')
      this.modelBuscarVacante = percent;
    if (this.modelBuscarVacante != undefined && this.modelBuscarVacante.length > 2) {
      buscarVacante = ["Contador Publico - 102 panasonic", "Contador Publico - 111 walmart", "Contador Publico - 122 Canon"];
    }
  }



  buscarOtraVacante(): void {

    this.formularioModalBuscar.get("modelBuscarVacante")?.setValue("");
    this.formularioModalBuscar.get('modelBuscarVacante').setValidators([]);
    this.formularioModalBuscar.get('modelBuscarVacante').updateValueAndValidity()
  }

  generateArrayInvitation(flag: boolean, template: any) {
    let arrayProcessId = []
    if (flag || this.selecAllinvitation) {
      flag = this.selecAllinvitation
      for (let card of this.rows) {
        if (card.check) {
          arrayProcessId.push(card.id)
        }
      }

    } else {
      arrayProcessId.push(this.selectionProcessId)
    }
    if (arrayProcessId.length == 1) {
      flag = false;
    }
    this.sendInvitation(arrayProcessId, template, flag)
  }

  sendInvitation(array: any, template: any, flag: boolean) {
    this.modalService.dismissAll();
    const idVacante = this.formularioModalBuscar.get("modelBuscarVacante")?.value
    this._candidates.sendInvitation(idVacante.idVacant, array).subscribe(
      (response: any) => {
        if (response != null && response.status == 200) {
          this.contadorChecks = 0;
          if (flag) {
            this.mensajeToast = "Enviamos tu invitación a los candidatos";
          } else {
            this.mensajeToast = "Enviamos tu invitación al candidato";
          }
          this.showSuccess(template);
          this.buscarOtraVacante();
        } else {
          this.showMessageError("Ocurrio un error");
        }
      },
      (error) => {
        this.showMessageError("Ocurrio un error");
      }
    );
  }
  coloresPorcentaje(porcentaje: string) {
    let numero = porcentaje.replace('%', '');
    let color = '';
    if (parseInt(numero) > 70) color = "verde";
    if (parseInt(numero) >= 50 && parseInt(numero) <= 70) color = "naranja";
    if (parseInt(numero) < 50) color = "rojo";
    return color;
  }

  selectPage(page: string) {
    this.page = parseInt(page, 10) || 1;
  }

  formatInput(input: HTMLInputElement) {
    input.value = input.value.replace(FILTER_PAG_REGEX, '');
  }

  rowsMostradas(page: any) {
    this.page_number = page - 1
    if (this.pageTemporal < parseInt(page)) {
      this.flagPagination = true;
      this.pageActual = page;
      if (this.rows.length < this.totalElements) {
        this.inicializaCards();
      }
      this.pageTemporal = page - 1;
      if (this.rows.length >= this.totalElements) {
        this.updateRowsMOstradas(page)
      }
    } else {
      this.updateRowsMOstradas(page)
    }
  }

  updateRowsMOstradas(page: any) {
    setTimeout(() => {
      let a = this.rows.slice((this.page - 1) * this.page_size, page * this.page_size)
      this.rowsPorPagina = a.length;
      if (this.pageActual <= page) {
        this.rowsMostrar = this.rowsMostrar + a.length;
        this.restarRows = a.length;
      } else {
        this.rowsMostrar = this.rowsMostrar - this.restarRows;
      }
      this.pageActual = page;
    }, 100)
  }

  selecRow() {
    this.resetPaginadores();
    this.flagPagination = false;
    this.inicializaCards()
  }

  resetPaginadores() {
    this.rowsMostrar = 0;
    this.pageActual = 0;
    this.page_number = 0;
    this.restarRows = 0;
    this.pageTemporal = 0;
    this.rows = [];
    this.page = 1
  }

  searchVacant(text: string) {
    if (text.length > 2) {
      this.searchFlag = true;
      this.textFilter = "&wordSearch=" + text
    } else {
      this.textFilter = "";
      this.searchFlag = false;
    }
    //this.resetPaginadores();
    this.flagPagination = false;
    this.inicializaCards();
  }

  generateArrayRejectCandidate(flag: boolean, template: any) {
    let arrayProcessId = []
    if (flag || this.rejectSelected) {
      flag = this.rejectSelected
      for (let card of this.rows) {
        if (card.check) {
          arrayProcessId.push(card.id)
        }
      }
    } else {
      arrayProcessId.push(this.selectionProcessId)
    }
    if (arrayProcessId.length == 1) {
      flag = false
    }
    this.rejectCandidate(template, arrayProcessId, flag)
  }

  rejectCandidate = (template: any, arrayCandidates: any, flagNotification: boolean) => {
    this._candidates.rejectCandidate(this.rejectFlag, arrayCandidates).subscribe(
      (response: any) => {
        if (response != null && response.status == 200) {
          this.updateCounters.selectionProcessCounter();
          this.flagPagination = false;
          //this.resetPaginadores();
          if (this.shortListActive) {
            this.updateCounters.getShortList(true);
          } else {
            this.inicializaCards();
          }
          this.contadorChecks = 0
          this.setMensajeToast(template, flagNotification)
        } else {
          this.showMessageError("Ocurrio un error");
        }

      },
      (error) => {
        this.showMessageError("Ocurrio un error");
      }
    );
  }

  generateArrayNextStage(flag: boolean, template: any) {
    // TODO: Agregar validación para que sólo se pueda avanzar si tiene dos videos y una prueba
    let arrayProcessId = []
    if (this.approveSelected) {
      for (let card of this.rows) {
        if (card.check) {
          arrayProcessId.push(card.id)
        }
      }
    } else {
      arrayProcessId.push(this.selectionProcessId)
    }
    if (arrayProcessId.length == 1) {
      this.approveSelected = false
    }
    this.nextStage(template, arrayProcessId, flag, this.approveSelected)
  }

  nextStage = (template: any, arrayCandidates: any, flagNotification: boolean, flagPlural: boolean) => {
    this._candidates.nextStage(flagNotification, arrayCandidates).subscribe(
      (response: any) => {
        if (response != null && response.status == 200) {
          this.updateCounters.selectionProcessCounter();
          this.flagPagination = false;
          //this.resetPaginadores();
          if (this.shortListActive) {
            this.updateCounters.getShortList(true);
          } else {
            this.inicializaCards();
          }
          this.contadorChecks = 0
          if (flagNotification) {
            this.setMensajeNotificacionFelicitacion(template, flagPlural)
          } else {
            this.setMensajeNotificacion(template, flagPlural)
          }
        } else {
          this.showMessageError("Ocurrio un error");
        }

      },
      (error) => {
        this.showMessageError("Ocurrio un error");
      }
    );
  }
  showMessageError(dangerTpl: any) {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-light ngb-toastsBottom', delay: 8000 });
  }

  spreadfilter1 = (filter: any) => {
    switch (filter.flag) {
      case "ordenamiento":
        this.sortDirection = filter.filter;
        this.otherFilter = "&rejected=false";
        //this.resetPaginadores();
        this.flagPagination = false;
        this.inicializaCards();
        this.shortListActive = false;
        break;
      case "favoritos":
        this.otherFilter = filter.filter;
        //this.otherFilter = (String(this.otherFilter).includes("favourite")) ? "" : "&favourite=false"
        //this.resetPaginadores();
        this.flagPagination = false;
        this.inicializaCards();
        this.shortListActive = false;
        break;
      case "terna":
        this.resetPaginadores();
        this.fillTable(filter.filter);
        this.shortListActive = true
        this.rowsMostrar = this.rows.length;
        break;
    }

  }

  spreadfilter2 = (filter: any) => {
    this.compatibility = filter;
    //this.resetPaginadores();
    this.flagPagination = false;
    this.inicializaCards();
  }

  spreadfilterVideo = (filter: any) => {
    this.filterVideo = filter
    //this.resetPaginadores();
    this.flagPagination = false;
    this.inicializaCards();
  }

  spreadfilterAge = (filter: any) => {
    this.filterAge = filter;
    //this.resetPaginadores();
    this.flagPagination = false;
    this.inicializaCards();
  }

  spreadfilterSalary = (filter: any) => {
    this.filterSalary = filter;
    //this.resetPaginadores();
    this.flagPagination = false;
    this.inicializaCards();
  }

  spreadMorefilters = (filter: any) => {
    this.filterURL = filter.filterURL;
    this.filterState = filter.filterState;
    this.filterGender = filter.filterGender;
    this.filterLevelEducation = filter.filterLevelEducation;
    this.filterNationality = filter.filterNationality;
    this.filterInstitution = filter.filterInstitution;
    //this.resetPaginadores();
    this.flagPagination = false;
    this.inicializaCards();
  }

  onchangeCheck(id: number) {
    for (let i = 0; i < this.rows.length; i++) {
      if (this.rows[i].id == id) {
        (this.rows[i].check) ? this.contadorChecks-- : this.contadorChecks++
        this.rows[i].check = !this.rows[i].check
      }
    }
  }

  limpiarSeleccion() {
    for (let i = 0; i < this.rows.length; i++) {
      this.contadorChecks = 0
      this.rows[i].check = false
    }
  }

  selectedOption(option: string) {
    this.optionAll = option
  }

  applySelection() {

    switch (this.optionAll) {
      case "A":
        this.openModalAprobar('', true);
        break;
      case "R":
        this.openModalRechazar('', true);
        break;
      case "E":
        this.generateArrarSendReminder(this.iconoChekBlanco, "", true, false, false)
        break;
      case "I":
        this.openModalInvitar("", true)
        break;
    }
  }

  newShortList = () => {
    let arrayProcessId = []
    for (let card of this.rows) {
      if (card.check) {
        arrayProcessId.push(card)
      }
    }
    let fechaHoy = formatDate(new Date(), 'yyyy-MM-dd', 'en_US')
    const params = {
      "firstProcessId": arrayProcessId[0].id,
      "secondProcessId": arrayProcessId[1].id,
      "thirdProcessId": arrayProcessId[2].id,
    }

    this._candidates.newShortList(params).subscribe(
      (response: any) => {

        if (response !== null && response.status === 200) {
          this.updateCounters.getShortList(true);
          this.limpiarSeleccion();
          this.showSuccess(response.body);
        }
        if (response !== null && response.status !== 200) {
          this.showMessageError(response.body);
        }
      },
      (error) => {
        this.showMessageError(error.error);
      }
    );
  }

  redirectToProfile(id: any) {

    let contador = 0;
    for (let card of this.rows) {
      contador++
      if (card.id == id) {
        let data = {
          id: this.selectedCard.id,
          idShow: card.licenciatura,
          name: card.nombre,
          client: this.selectedCard.client,
          pageActual: this.pageActual,
          pageSize: this.page_size,
          cardActual: contador,
          setProcessSelection: this.setProcessSelection,
          status: this.status,
          totalElemt: this.totalElements,
          totalPages: this.totalPages,
          rowsMostrar: this.rowsMostrar,
          testsPsychometricComplete: this.statusTestPsychometric
        }
        this.profileService.setSelectedCard(data)
      }
    }
    this._candidates.setRowsTemporal(this.rows);
    this.router.navigateByUrl('dashboard/proceso/perfil/(profile:cv)');
  }

  updateShowProfile = (id: string) => {
    this._candidates.showProfile(id).subscribe(
      (response) => {
        this.getProcessById(id)
      },
      (error) => {

      }
    );
  }

  showRecruiters = (watchers: any) => {
    let recruiters = []
    for (let recruiter of watchers) {
      recruiters.push({
        iniciales: this.getInicialesNombre(recruiter.recruiter.user.name),
        nombre: recruiter.recruiter.user.name,
        url: (recruiter.recruiter.user.photo == null) ? "" : recruiter.recruiter.user.photo,
      })

    }
    return recruiters
  }

  getInicialesNombre(nombre: string) {
    let iniciales = ""
    let separador = " ",
      arregloDeSubCadenas = nombre.split(separador);

    for (let j = 0; j < arregloDeSubCadenas.length; j++) {
      const subCadena = arregloDeSubCadenas[j].substring(0, 1)
      iniciales += subCadena;
    }
    return iniciales.slice(0, 2).toUpperCase();
  }

  getProcessById = (id: string) => {
    this._candidates.getProcessById(id).subscribe(
      (response: any) => {
        if (response != null) {
          this.statusTestPsychometric = response.body.testsPsychometricComplete;
          for (let card in this.rows) {
            if (this.rows[card].id == response.body.selectionProcessId) {
              this.rows[card].whachers = this.showRecruiters(response.body.watchers);
              this.redirectToProfile(id)
              break;
            }
          }

        }
      },
      (error) => {

      }
    );
  }

  contactClicked(link: string, method: string = 'link'){
    window.open(`${method === 'email' ? 'mailto:' : ''}${method === 'phone' ? 'tel:' : ''}${link}`);
        // TOOD: Agregar ${method === 'phone' ? 'tel:' : ''} al método cuando la plataforma sea responsiva y se pueda abrir desde un celular
  }

  formatDate(sourceDate: string){
    const days: any = {
      monday: 'lunes',
      tuesday: 'martes',
      wednesday: 'miércoles',
      thursday: 'jueves',
      friday: 'viernes',
      saturday: 'sábado',
      sunday: 'domingo'
    };
    const months: any = {
      january: 'enero',
      february: 'febrero',
      march: 'marzo',
      april: 'abril',
      may: 'mayo',
      june: 'junio',
      july: 'julio',
      august: 'agosto',
      september: 'septiembre',
      october: 'octubre',
      november: 'noviembre',
      december: 'diciembre'
    };
    const tokens: any[] = sourceDate.trim().split(' ');
    return `${ days[tokens[0].toLowerCase()] } ${tokens[1]} de ${ months[tokens[3].toLowerCase()]} de ${tokens[4]}`;
  }

}
let buscarVacante: any = [""]
const FILTER_PAG_REGEX = /[^0-9]/g;
