import { Component, OnInit } from '@angular/core';
import { VacantesService } from 'src/app/services/reclutador/vacantes/vacantes.service';

@Component({
  selector: 'app-contratados',
  templateUrl: './contratados.component.html',
  styleUrls: ['./contratados.component.scss']
})
export class ContratadosComponent implements OnInit {

  status = "CONTRATADO";
  initialText = "Por ahora no hay candidatos contratados"
  setProcessSelection = "contratados"
  constructor() { }

  ngOnInit(): void {
  }

}
