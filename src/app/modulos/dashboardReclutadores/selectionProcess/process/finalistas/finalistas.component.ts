import { Component, OnInit } from '@angular/core';
import { VacantesService } from 'src/app/services/reclutador/vacantes/vacantes.service';

@Component({
  selector: 'app-finalistas',
  templateUrl: './finalistas.component.html',
  styleUrls: ['./finalistas.component.scss']
})
export class FinalistasComponent implements OnInit {

  status = "FINALISTA";
  initialText = "Por ahora no hay candidatos finalistas"
  setProcessSelection = "finalistas"
  constructor() { }

  ngOnInit(): void {
  }

}
