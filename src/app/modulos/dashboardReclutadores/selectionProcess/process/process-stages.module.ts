import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProcessStagesRoutingModule } from './process-stages-routing.module';
import { ModalReclutadoresComponent } from '../../vacantes/modal-reclutadores/modal-reclutadores.component';
import { CandidatosComponent } from './candidatos/candidatos.component';
import { EntrevistasComponent } from './entrevistas/entrevistas.component';
import { FinalistasComponent } from './finalistas/finalistas.component';
import { ContratadosComponent } from './contratados/contratados.component';
import { PruebasComponent } from './pruebas-video/pruebas-video.component';
import { moduleSharedModule } from 'src/app/commons/commons.module';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { NgxSpinnerModule } from 'ngx-spinner';
import { InternalizacionModule } from 'src/app/commons/internalizacion/internalizacion.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardModule } from '../../dashboard.module';



@NgModule({
  declarations: [
    CandidatosComponent,
    EntrevistasComponent,
    FinalistasComponent,
    ContratadosComponent,
    PruebasComponent
  ],
  imports: [
    CommonModule,
    ProcessStagesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbTooltipModule,
    AngularEditorModule,
    InternalizacionModule,
    NgbModule,
    NgxSpinnerModule,
    NgxSliderModule,
    moduleSharedModule,
    DashboardModule
  ]
})
export class processStagesModule { }
