import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { Observable, OperatorFunction } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap } from 'rxjs/operators';
import { ToastService } from 'src/app/services/commons/toast.setvice';
import { VacantesService } from 'src/app/services/reclutador/vacantes/vacantes.service';
import { CandidatesService } from '../../../../../services/reclutador/vacantes/selectionProcess/candidatos.service';
import { VacantCard } from 'src/app/services/models/cardVacants.model';

@Component({
  selector: 'app-pruebas-video',
  templateUrl: './pruebas-video.component.html',
  styleUrls: ['./pruebas-video.component.scss']
})
export class PruebasComponent implements OnInit {
  status = "PRUEBAS_Y_VIDEO";
  initialText = "Por ahora no hay pruebas y videos"
  setProcessSelection = "pruebas"
  constructor(private _menuSeleccionado: VacantesService,) {

    }

  ngOnInit(): void {
    this._menuSeleccionado.setProcesoSeleccion("pruebas");
    
  }
}