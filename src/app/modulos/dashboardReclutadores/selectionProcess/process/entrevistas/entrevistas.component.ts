import { Component, OnInit } from '@angular/core';
import { VacantesService } from 'src/app/services/reclutador/vacantes/vacantes.service';

@Component({
  selector: 'app-entrevistas',
  templateUrl: './entrevistas.component.html',
  styleUrls: ['./entrevistas.component.scss']
})
export class EntrevistasComponent implements OnInit {

  status = "ENTREVISTA";
  initialText = "Por ahora no hay entrevistas"
  setProcessSelection = "entrevistas"
  constructor() { }

  ngOnInit(): void {
  }

}
