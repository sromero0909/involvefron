import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CandidatosComponent } from './candidatos/candidatos.component';
import { ContratadosComponent } from './contratados/contratados.component';
import { EntrevistasComponent } from './entrevistas/entrevistas.component';
import { FinalistasComponent } from './finalistas/finalistas.component';
import { PruebasComponent } from './pruebas-video/pruebas-video.component';

const routes: Routes = [
    {
        path: 'candidatos/:flag',
        component: CandidatosComponent,
    },
    {
        path: 'pruebas-video/:flag',
        component: PruebasComponent,
    },
    {
        path: 'entrevistas/:flag',
        component: EntrevistasComponent,
    },
    {
        path: 'finalistas/:flag',
        component: FinalistasComponent,
    },
    {
        path: 'contratados/:flag',
        component: ContratadosComponent,
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})

export class ProcessStagesRoutingModule { }