import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';
import { PerfilRoutingModule } from './profile-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ProfileComponent } from './profile/profile.component';
import { CvComponent } from './cv/cv.component';
import { ProofComponent } from './proof/proof.component';
import { DashboardModule } from '../../dashboard.module';
import { InterviewComponent } from './interview/interview.component';
import { moduleSharedModule } from 'src/app/commons/commons.module';
import { CompatibilityComponent } from './cv/pages/compatibility/compatibility.component';
import { EducationComponent } from './cv/pages/education/education.component';
import { WorkExperienceComponent } from './cv/pages/work-experience/work-experience.component';
import { SkillsComponent } from './cv/pages/skills/skills.component';
import { CoursesComponent } from './cv/pages/courses/courses.component';
import { ChartRadarComponent } from './cv/components/chart-radar/chart-radar.component';
import { ChartBarsComponent } from './cv/components/chart-bars/chart-bars.component';
import { VideoComponent } from './video/video.component';
import { VideoPresentationComponent } from './video/pages/video-presentation/video-presentation.component';
import { VideoInterviewComponent } from './video/pages/video-interview/video-interview.component';
import { LanguagesComponent } from './cv/pages/languages/languages.component';
import { CertificationsComponent } from './cv/pages/certifications/certifications.component';
import { CvComponentTalent } from './cv/cv-talent-network/cv-talent-network.component';

@NgModule({
  declarations: [
    ProfileComponent,
    CvComponent,
    CvComponentTalent,
    ProofComponent,
    InterviewComponent,
    CompatibilityComponent,
    EducationComponent,
    WorkExperienceComponent,
    SkillsComponent,
    CoursesComponent,
    LanguagesComponent,
    CertificationsComponent,
    ChartRadarComponent,
    ChartBarsComponent,
    VideoComponent,
    VideoPresentationComponent,
    VideoInterviewComponent,
  ],
  imports: [
    CommonModule,
    PerfilRoutingModule,
    DashboardModule,
    NgbModule,
    moduleSharedModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule
  ]
})
export class PerfilModule { }
