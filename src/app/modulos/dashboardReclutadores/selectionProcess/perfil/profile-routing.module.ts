import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CvComponent } from './cv/cv.component';
import { InterviewComponent } from './interview/interview.component';
import { ProfileComponent } from './profile/profile.component';
import { ProofComponent } from './proof/proof.component';
import { CompatibilityComponent } from './cv/pages/compatibility/compatibility.component';
import { EducationComponent } from './cv/pages/education/education.component';
import { WorkExperienceComponent } from './cv/pages/work-experience/work-experience.component';
import { SkillsComponent } from './cv/pages/skills/skills.component';
import { CoursesComponent } from './cv/pages/courses/courses.component';
import { VideoComponent } from './video/video.component';
import { VideoPresentationComponent } from './video/pages/video-presentation/video-presentation.component';
import { VideoInterviewComponent } from './video/pages/video-interview/video-interview.component';
import { CleaverComponent } from '../../pruebas-psicometricas/components/cleaver/cleaver.component';
import { LuscherComponent } from '../../pruebas-psicometricas/components/luscher/luscher.component';
import { IpvComponent } from '../../pruebas-psicometricas/components/ipv/ipv.component';
import { MossComponent } from '../../pruebas-psicometricas/components/moss/moss.component';
import { CvComponentTalent } from './cv/cv-talent-network/cv-talent-network.component';


const routes: Routes = [
  {
    path: 'perfil',
    component: ProfileComponent,
    children: [
      {
        path: 'cv',
        component: CvComponent,
        outlet: 'profile',
        children: [
          {
            path: '',
            component: CompatibilityComponent,
            outlet: 'curriculum'
          },
          {
            path: 'compatibility',
            component: CompatibilityComponent,
            outlet: 'curriculum'
          },
          {
            path: 'education',
            component: EducationComponent,
            outlet: 'curriculum'
          },
          {
            path: 'work-experience',
            component: WorkExperienceComponent,
            outlet: 'curriculum'
          },
          {
            path: 'skills',
            component: SkillsComponent,
            outlet: 'curriculum'
          },
          {
            path: 'LanguaguesCourses',
            component: CoursesComponent,
            outlet: 'curriculum'
          }
        ]
      },
      {
        path: 'pruebas',
        component: ProofComponent,
        outlet: 'profile',
        children: [
          {
            path: 'cleaver',
            component: CleaverComponent,
            outlet: 'test'
          },
          {
            path: 'luscher',
            component: LuscherComponent,
            outlet: 'test'
          },
          {
            path: 'ipv',
            component: IpvComponent,
            outlet: 'test'
          },
          {
            path: 'moss',
            component: MossComponent,
            outlet: 'test'
          }
        ]
      },
      {
        path: 'video',
        component: VideoComponent,
        outlet: 'profile',
        children: [
          {
            path: '',
            component: VideoPresentationComponent,
            outlet: 'video'
          },
          {
            path: 'presentation',
            component: VideoPresentationComponent,
            outlet: 'video'
          },
          {
            path: 'interview',
            component: VideoInterviewComponent,
            outlet: 'video'
          }
        ]
      },
      {
        path: 'interview',
        component: InterviewComponent,
        outlet: 'profile'
      },
      {
        path: 'talent-CV',
        component: CvComponentTalent,
        outlet: 'profile',
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class PerfilRoutingModule { }
