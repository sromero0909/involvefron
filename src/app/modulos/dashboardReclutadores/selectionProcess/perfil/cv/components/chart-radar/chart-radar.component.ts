import { Component, Input, OnInit } from '@angular/core';
import { ChartDataSets, ChartTitleOptions, ChartType, RadialChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import html2canvas from 'html2canvas';
import { Options } from '@angular-slider/ngx-slider';

@Component({
  selector: 'app-chart-radar',
  templateUrl: './chart-radar.component.html',
  styleUrls: ['./chart-radar.component.scss']
})
export class ChartRadarComponent implements OnInit {
  @Input() dataChart : any;

  dataChartVector = [95, 95, 95, 95, 85];

  public radarChartOptions: RadialChartOptions = {
    responsive: true,
    legend: {
      display: false
    },
    scale: {
      ticks: {
        beginAtZero: true,
        fontColor: 'white', // labels such as 10, 20, etc
        showLabelBackdrop: false // hide square behind text
      },
    }
  };

  public titleChartOptions: ChartTitleOptions = {
    display: false,
    position: 'bottom'
  }

  public radarChartLabels: Label[] = ['Denominación del puesto', 'Nivel jérarquico del puesto', 'Condiciones laborales', 'Escolaridad', 'Experiencia laboral'];

  public radarChartData: ChartDataSets[] = [
    { data: [100, 100, 100, 100, 100], backgroundColor: "rgba(255,255,255,0.1)",
    borderColor: 'rgba(0,0,0,0.8)',
    pointBorderColor: 'rgba(0,0,0,0.8)',
    pointBackgroundColor: 'rgba(0,0,0,0.8)'},
    { data:[30,30,30,30,30],backgroundColor: "rgba(0,0,200,0.0)", borderColor: 'rgba(0,0,200,0.0)', pointBackgroundColor: 'rgba(82, 190, 128,0.1)' },
    { data: this.dataChartVector, backgroundColor:'rgba(168, 234, 194, 0.4)', borderColor: 'rgba(227,255,223,0.5)', pointBackgroundColor: 'rgba(82, 190, 128,0.1)'}
  ];

  //  backgroundColor: 'rgba(00,255,00, 0.1)',
    //  borderColor: '#00FF00'

  public radarChartType: ChartType = 'radar';

  // Set true to show legends
  lineChartLegend = false;

  // Define type of chart
  lineChartType = 'line';

  constructor() {
  }

  ngOnInit(): void {
    this.radarChartData[2].data = [this.dataChart.position, this.dataChart.positionLevel, this.dataChart.workExperience, this.dataChart.scholarship, this.dataChart.hardSkill];
  }

}