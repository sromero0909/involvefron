import { Component, Input, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-chart-bars',
  templateUrl: './chart-bars.component.html',
  styleUrls: ['./chart-bars.component.scss']
})
export class ChartBarsComponent implements OnInit {

  @Input() public barChartData: ChartDataSets[] = [];
  @Input() btnUpdate: boolean = true;
  @Input() public maxRotation: number = 0;
  @Input() public minRotation: number = 0;
  maxR!: number;
  minR!: number;
  @Input() public barChartOptionsInput!: ChartOptions
  public barChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      display: false
    },
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{
        gridLines: {
          color: "#ffff",
          lineWidth: 1,
          zeroLineColor: "#707070",
          zeroLineWidth: 2
        },
        ticks: {
          autoSkip: false,
          maxRotation: this.maxR,
          minRotation: this.minR
        }
      }],
      yAxes: [{
        ticks:{
          min : 0,
          max: 100,
          stepSize : 20,
          fontColor : "#ffff",
          fontSize : 1
        },
        gridLines: {
          color: "#ffff",
          lineWidth: 1,
          zeroLineColor: "#707070",
          zeroLineWidth: 2
        }
      }]
    },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  //public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  @Input() public barChartLabels: Label[] = []; //Etiquetas Horizontales
  @Input() public barChartType: ChartType = 'bar';
  @Input() public barChartLegend = true;
  @Input() min:number = 0;
  @Input() max:number = 100;
  @Input() stepSize: number = 20;

  // public barChartData: ChartDataSets[] = [
  //   { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
  //   { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  // ];

  constructor() {


  }

  ngOnInit(): void {
    this.maxR = this.maxRotation;
    this.minR = this.minRotation
    let char = this.barChartOptionsInput
    console.log("barChartOptionsInput",char)
    if(char != undefined){
      this.barChartOptions = char
    }
    //this.barChartOptions = 
  }

  public randomize(): void {
    // Only Change 3 values
    this.barChartData[0].data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
  }

}
