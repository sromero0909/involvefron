import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.scss']
})
export class CvComponent implements OnInit, OnDestroy {

  @Input() showCV: boolean;

  private subscriptions!: Subscription;
  candidate$!: Observable<any[]>;
  candidate: any;

  constructor(private profileService: ProfileService) { }

  ngOnInit(): void {
    this.changeTheCandidate();
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  changeTheCandidate() {
    this.candidate = this.profileService?.candiate?.data.candidate;
    this.candidate$ = this.profileService.getCandidate$();
    this.subscriptions = this.candidate$.subscribe((candidate: any) => {
    },
    (error) => {
      console.warn(error);
    });
  }

  get displaysLanguagesTab(): boolean{
    return this.candidate?.certifications.length > 0 ||
            this.candidate?.languages.length > 0 ||
            this.candidate?.courses.length > 0;
  }
}
