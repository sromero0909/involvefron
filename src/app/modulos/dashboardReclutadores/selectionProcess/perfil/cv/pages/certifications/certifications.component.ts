import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-certifications',
  templateUrl: './certifications.component.html',
  styleUrls: ['./certifications.component.scss']
})
export class CertificationsComponent implements OnInit {

  @Input() certificationsData: any;

  constructor() { }

  ngOnInit(): void {
  }

}
