import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';

@Component({
  selector: 'app-work-experience',
  templateUrl: './work-experience.component.html',
  styleUrls: ['./work-experience.component.scss']
})
export class WorkExperienceComponent implements OnInit {
  idProcess: string;
  workExp: Array<any> = [];
  areasExp: Array<any> = [];
  specialtiesExp: Array<any> = [];
  questionnaire: Array<any> = [];

  constructor(private profileService: ProfileService) { }

  ngOnInit(): void {
    this.getidProcess();
    this.getWorkExp(this.idProcess);
  }

  getWorkExp = (id: string) => {
    if (id !== null){
      this.profileService.getWorkExperience(id).subscribe((res: any) => {
        if (res.body.workExperiences.length > 0) {
          res.body.workExperiences.forEach((e: any) => {
            this.workExp.push(e);
          });
        }
        if (res.body.candidateAreas.length > 0) {
          res.body.candidateAreas.forEach((e: any) => {
            this.areasExp.push(e);
          });
        }
        if (res.body.candidateSpecialties.length > 0) {
          res.body.candidateSpecialties.forEach((e: any) => {
            this.specialtiesExp.push(e);
          });
        }
        if (res.body.questionnaire.length > 0) {
          res.body.questionnaire.forEach((e: any) => {
            this.questionnaire.push(e);
          });
        }
      }, (error: any) => {
        console.error("getWorkExp()", error);
      });
    }
  }

  getidProcess = () => {
    this.idProcess = this.profileService.getActiveCandidate()?.idProcess || null;
  }
}
