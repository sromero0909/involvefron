import { Component, Input, OnInit } from '@angular/core';
import { ProfileService } from '../../../../../../../services/reclutador/vacantes/selectionProcess/profile.service';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {

  @Input() educationData: any;
  private subscriptions!: Subscription;
  candidate$!: Observable<any[]>;
  educations: any[] = [];

  constructor(private profileService: ProfileService) { }

  ngOnInit(): void {
    this.changeTheCandidate();
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  changeTheCandidate() {
    this.candidate$ = this.profileService.getCandidate$();
    this.subscriptions = this.candidate$.subscribe(candidate => console.log("es el candidato activo", candidate));
    this.getEducation();
  }

  getEducation = () => {
    this.educations = this.profileService.getActiveCandidate()?.data.candidate.academicHistory;
  }

}
