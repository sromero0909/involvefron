import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';


@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {
  processId: string;
  workExperience: any;
  skills: any;

  constructor(private activatedRoute: ActivatedRoute,
              private profileService: ProfileService, ) { 
    if (this.activatedRoute.snapshot.paramMap.get('processId')){
      this.processId = this.activatedRoute.snapshot.paramMap.get('processId') || '';
      this.profileService.getWorkExperience(this.processId).subscribe((workExperienceResponse: any) => {
        this.workExperience = workExperienceResponse.body;
      },
      (error) => {
        console.error(error);
      });
      this.profileService.getSkills(this.processId).subscribe((skillsResponse: any) => {
        this.skills = skillsResponse.body;
      },
      (error) => {
        console.error(error);
      });
    }
  }

  ngOnInit(): void {
  }

  meetsLevel(columnLevel: string, candidateLevel: string): boolean{
    const levels = ['BASICO', 'INTERMEDIO', 'AVANZADO', 'EXPERTO'];
    return levels.indexOf(candidateLevel) >= levels.indexOf(columnLevel);
  }
}
