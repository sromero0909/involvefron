import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { ProfileService } from '../../../../../../../services/reclutador/vacantes/selectionProcess/profile.service';


@Component({
  selector: 'app-compatibility',
  templateUrl: './compatibility.component.html',
  styleUrls: ['./compatibility.component.scss']
})
export class CompatibilityComponent implements OnInit {

  @Input() setProcessSelection: any;
  private subscriptions!: Subscription;
  candidate$!: Observable<any[]>;
  /** */
  matchPorcentage = '';
  colorPorcentaje = '';
  dataChart = {};
  matchMessage = ""

  /** */

  constructor(private profileService: ProfileService) {
  }

  ngOnInit(): void {
    if (this.profileService.getActiveCandidate() !== undefined) {
      this.changeTheCandidate();
      this.getDataCandidate();
    }
  }

  getDataCandidate = () => {
    this.matchPorcentage = this.profileService.getActiveCandidate().porcentaje;
    this.colorPorcentaje = this.profileService.getActiveCandidate().color;
    const { hardSkillPoints, positionLevelPoints, positionPoints, scholarshipPoints, workExperiencePoints, matchMessage } = this.profileService.getActiveCandidate().data;

    this.dataChart = {
      'hardSkill': hardSkillPoints,
      'positionLevel': positionLevelPoints,
      'position': positionPoints,
      'scholarship': scholarshipPoints,
      'workExperience': workExperiencePoints,
    }

    this.matchMessage = matchMessage;
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }

  changeTheCandidate() {
    this.candidate$ = this.profileService.getCandidate$();
    this.subscriptions = this.candidate$.subscribe(candidate => console.log("es el candidato activo", candidate));
  }

}
