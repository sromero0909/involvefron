import { Component, Input, OnInit } from '@angular/core';
import { ProfileService } from '../../../../../../../services/reclutador/vacantes/selectionProcess/profile.service';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {

  @Input() coursesData: any[] = [];
  private subscriptions!: Subscription;
  candidate$!: Observable<any[]>;
  languages: any[] = [];
  courses: any[] = [];
  certifications: any[] = [];
  idProcess: string;

  constructor(private profileService: ProfileService) { 
    this.idProcess = this.profileService.getActiveCandidate()?.idProcess;
    if (this.idProcess){
      this.profileService.getSelectionProcess(this.idProcess).subscribe((successResponse: any) => {
        this.courses = successResponse.body.candidate.courses;
        this.certifications = successResponse.body.candidate.certifications;
        this.languages = successResponse.body.candidate.languages;
      });
    }
  }

  ngOnInit(): void {

  }


}
