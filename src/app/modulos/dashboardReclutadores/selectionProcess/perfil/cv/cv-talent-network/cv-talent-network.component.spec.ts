import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CvComponentTalent } from './cv-talent-network.component';

describe('CvComponent', () => {
  let component: CvComponentTalent;
  let fixture: ComponentFixture<CvComponentTalent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CvComponentTalent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CvComponentTalent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
