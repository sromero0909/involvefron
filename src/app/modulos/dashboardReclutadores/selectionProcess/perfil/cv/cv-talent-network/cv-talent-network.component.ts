import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { CandidatesService } from 'src/app/services/reclutador/vacantes/selectionProcess/candidatos.service';

@Component({
  selector: 'app-cv-talent',
  templateUrl: './cv-talent-network.component.html',
  styleUrls: ['./cv-talent-network.component.scss']
})
export class CvComponentTalent implements OnInit {


  private subscriptions!: Subscription;
  candidate$!: Observable<any[]>;
  candidate: any;
  idCandidate: any;
  constructor(private profileService: ProfileService,private authService: AuthService, private _candidates: CandidatesService,) { }

  ngOnInit(): void {
    this.authService;
    this._candidates;
    this.idCandidate=this.profileService?.card?.id
    this._candidates.rowsTemporal.forEach((canditato:any)=>{
      if(this.idCandidate == canditato.candidate.candidateId ){
        this.candidate=canditato.candidate;
      } 
    });
  }
  hardSkillsLevel(LevelTableView:number, hardSkillsLevel: string): Boolean{
    if((hardSkillsLevel=="INTERMEDIO"  && LevelTableView<= 1) || (hardSkillsLevel=="AVANZADO" && LevelTableView<= 2) || (hardSkillsLevel=="EXPERTO" && LevelTableView<= 3)){
        return true;
    }else{
      return false;
    }
    
  }
}
