import { Component, Input, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';
import { PsicometricService } from '../../../pruebas-psicometricas/components/psicometric.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-proof',
  templateUrl: './proof.component.html',
  styleUrls: ['./proof.component.scss']
})
export class ProofComponent implements OnInit {
  cleaver: boolean;
  moss: boolean;
  ipv: boolean;
  luscher: boolean;
  statusStartTests : boolean = false;
  processId: string;
  data: any[]=[];
  statusTest: string;


  constructor(private psicometricService:PsicometricService,private profileService: ProfileService, private _router : Router) { }

  ngOnInit(): void {
     this.profileService.getCandidate$();
      //2c9f906e7c61fe48017c636e51120001
     // 
     
     this.processId = this.profileService.candiate?.idProcess;
     if (this.processId){
       this.psicometricService.getTest(this.processId).subscribe((response: any)=>{
        if(response.code === 200){
          this.data= response.data.map((testsElement: any) => {
            return {
              psicometricosId: testsElement.psicometricosId,
              name: '',
            }
         });
        const hash: any = {};
        this.data = this.data.filter(o => hash[o.psicometricosId] ? false : hash[o.psicometricosId] = true);
        this.data.forEach((test: any) => {
          this.psicometricService.getNametest(test.psicometricosId).subscribe((response:any)=>{
            if(response.code === 200){
              this.updatedata(response.data.psicometrico.name);
            }
          });
        });
        }
       });
     }
  }
 
  updatedata(nameTest : string){
    if(this.statusStartTests == false){
      this.statusStartTests = true;
      if(nameTest== "IPV"){
        this._router.navigateByUrl('/dashboard/proceso/perfil/(profile:pruebas/(test:ipv)');
      }else if(nameTest== "Lüscher"){
        this._router.navigateByUrl('/dashboard/proceso/perfil/(profile:pruebas/(test:luscher)');
      }else if(nameTest== "Cleaver"){
        this._router.navigateByUrl('/dashboard/proceso/perfil/(profile:pruebas/(test:cleaver)');
      }else if(nameTest== "Moss"){
        this._router.navigateByUrl('/dashboard/proceso/perfil/(profile:pruebas/(test:moss)');
      }
      this.changetest(nameTest);
    }
    if( nameTest === "Cleaver"){
      this.cleaver =true;
    }else if (nameTest === "Moss"){
      this.moss = true ;
    }else if(nameTest === "IPV"){
      this.ipv=true;
    }else if(nameTest === "Lüscher"){
      this.luscher = true;
    }
  }
  changetest( test :string ){
    this.statusTest = test;
  }
}
