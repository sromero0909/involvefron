import { Component, Input, OnInit } from '@angular/core';

import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { Observable } from 'rxjs/Observable';
import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';
import { Subscription } from 'rxjs';
import { LoaderService } from 'src/app/services/commons/loader.service';

import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-video-presentation',
  templateUrl: './video-presentation.component.html',
  styleUrls: ['./video-presentation.component.scss']
})
export class VideoPresentationComponent implements OnInit {

  @Input() typeVideo: string
  candidate$!: Observable<any[]>;
  private subscriptions!: Subscription;
  public barChartData: any[] = []
  public barChartDataConclusion: any[] = []
  public barChartLabels: any[] = ['Enojo', 'Miedo', 'Tristeza', 'Sorpresa', 'Confusión', 'Disgustado', 'Calma', 'Felicidad'];
  public barChartLabelsConclucion: any[] = ['Negativo', 'Neutral', 'Positiva'];
  questions!: Array<any>

  btnUpdate: boolean = false;
  urlVideo: string = ""
  textConclusion = ""
  percentage = ""
  flagGraph = false
  maxRotation: number = 90;
  minRotation: number = 90;
  @Input() textVideo: string = "Vídeo de presentación"

  barChartOptionsAnalisis: ChartOptions = {
    responsive: true,
    legend: {
      display: false
    },
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{
        gridLines: {
          color: "#ffff",
          lineWidth: 0,
          zeroLineColor: "#707070",
          zeroLineWidth: 2
        },
        ticks: {
          autoSkip: false,
          maxRotation: 90,
          minRotation: 90
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#ffff",
          fontSize: 1
        },
        gridLines: {
          color: "#ffff",
          lineWidth: 0,
          zeroLineColor: "#707070",
          zeroLineWidth: 2
        }
      }]
    },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };

  barChartOptionsConclusion: ChartOptions = {
    responsive: true,
    legend: {
      display: false
    },
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{
        gridLines: {
          color: "#ffff",
          lineWidth: 0,
          zeroLineColor: "#707070",
          zeroLineWidth: 2
        },
        ticks: {
          autoSkip: false,
          maxRotation: 0,
          minRotation: 0
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: 100,
          stepSize: 20,
          fontColor: "#ffff",
          fontSize: 1
        },
        gridLines: {
          color: "#ffff",
          lineWidth: 0,
          zeroLineColor: "#707070",
          zeroLineWidth: 2
        }
      }]
    },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };

  constructor(private _sanitizer: DomSanitizer, private profileService: ProfileService, private loader: LoaderService) {
    this.typeVideo = "P"
  }

  ngOnInit(): void {
    this.getVideo();
  }

  ngOnDestroy() {
    if (this.subscriptions != undefined) {
      this.subscriptions.unsubscribe();
    }
  }

  getVideo = () => {
    if (this.profileService.getActiveCandidate() != undefined) {
      if (this.typeVideo == "P") {
        this.getUrlPresentation(this.profileService.getActiveCandidate())
        this.getQuestionsPresentation(this.profileService.getActiveCandidate().data.candidate)
      } else {
        this.getUrlInterview(this.profileService.getActiveCandidate());
        this.getQuestions(this.profileService.getActiveCandidate().data.vacant)
      }

      this.changeTheCandidate();
    }

  }

  getUrlPresentation = (data: any) => {
    this.urlVideo = ""
    const candidate = data.data.candidate.urlFiles
    // for (let obj of candidate) {
    //   if (obj.type == "URL_PRESENTATION_VIDEO") {
    //     this.urlVideo = obj.pathFile
    //   }
    // }
    this.urlVideo = (data.data.candidate.pathPresentationVideo == null) ? "" : data.data.candidate.pathPresentationVideo
    setTimeout(() => {
      this.readJsonPresentation(data.data.candidate.candidateId);
    }, 1500);

  }

  getUrlInterview = (data: any) => {
    this.urlVideo = ""
    const candidate = data.data.candidate.urlFiles
    // for (let obj of candidate) {
    //   if (obj.type == "URL_PRESENTATION_VIDEO") {
    //     this.urlVideo = obj.pathFile
    //   }
    // }
    this.urlVideo = (data.data.pathInterviewVideo == null) ? "" : data.data.pathInterviewVideo
    setTimeout(() => {
      this.readJsonInterview(data.idProcess)
    }, 1500);
  }

  changeTheCandidate() {
    this.candidate$ = this.profileService.getCandidate$();
    this.subscriptions = this.candidate$.subscribe(
      (candidate) => {
        (this.typeVideo == "P") ? this.getUrlPresentation(candidate) :
          this.getUrlInterview(candidate)
      }
    )

  }

  getVideoIframe(url: any) {
    var video, results;
    if (url === null) {
      return '';
    }
    results = url.match('[\\?&]v=([^&#]*)');
    video = (results === null) ? url : results[1];
    this.loader.hideLoader();
    return this._sanitizer.bypassSecurityTrustResourceUrl(video);
  }

  readJsonPresentation = async (id: string) => {
    const idCandidateTemp = "402880de79730a2c0179731b79c40001"
    this.flagGraph = false;
    //this.profileService.getFile(id + "_presentation.json")
    // this.profileService.getFile(id).subscribe(
    //   (data: any) => {
    //     console.log('LEER ARCHIVO...');
    //     console.log(data);
    //     let regex = /(\d+)/g;
    //     let jsonTexto = ((data.replace('PRIMER GRAFICA ', '')).replace('"', '')).replace('CONCLUSIONES', '');
    //     let x = jsonTexto.replace(/'/g, '"');
    //     let texto = x.substring(x.indexOf('TEXTO'), x.length);
    //     this.percentage = texto.match(regex)
    //     this.textConclusion = texto.slice(14, texto.length)
    //     console.log(this.textConclusion);
    //     let json = JSON.parse("{" + (x.substring(0, x.indexOf('TEXTO') - 3) + "}"));
    //     console.log("jsonparse", json);
    //     let { CALMA, CONFUSION, DISGUST0, ENOJO, FELICIDAD, MIEDO, NEGATIVO, NEUTRAL, Positivo, SORPRESA, TRISTEZA } = json
    //     let datos = [ENOJO, MIEDO, TRISTEZA, SORPRESA, CONFUSION, DISGUST0, CALMA, FELICIDAD]
    //     const datosConclucion = [NEGATIVO, NEUTRAL, Positivo]
    //     this.barChartData.push({
    //       data: datos,
    //       backgroundColor: [(ENOJO > 0) ? '#0BB9B9' : '#525252', (MIEDO > 0) ? '#0BB9B9' : '#525252', (TRISTEZA > 0) ? '#0BB9B9' : '#525252', (SORPRESA > 0) ? '#0BB9B9' : '#525252', (CONFUSION > 0) ? '#0BB9B9' : '#525252', (DISGUST0 > 0) ? '#0BB9B9' : '#525252', (CALMA > 0) ? '#0BB9B9' : '#525252', (FELICIDAD > 0) ? '#0BB9B9' : '#525252'],
    //       label: ' '
    //     });
    //     this.maxRotation = 90
    //     this.barChartDataConclusion.push({
    //       data: datosConclucion,
    //       backgroundColor: [(NEGATIVO > 0) ? '#E21D1D' : '#525252', (NEUTRAL > 0) ? '#979797' : '#525252', (Positivo > 0) ? '#0BB9B9' : '#525252'],
    //       label: ' '
    //     })
    //     this.flagGraph = true;
    //   }
    // );
  }

  readJsonInterview = async (id: string) => {
    this.maxRotation = 90
    this.flagGraph = false;
    const idInterviewTemp = "402880de79730a2c0179731b7aed0002"
    // this.profileService.getFile(id + "_interview.json").subscribe(
    //   (data: any) => {
    //     let regex = /(\d+)/g;
    //     let jsonTexto = ((data.replace('PRIMER GRAFICA ', '')).replace('"', '')).replace('CONCLUSIONES', '');
    //     let x = jsonTexto.replace(/'/g, '"');
    //     let texto = x.substring(x.indexOf('TEXTO'), x.length);
    //     this.percentage = texto.match(regex)
    //     this.textConclusion = texto.slice(14, texto.length)
    //     console.log(this.textConclusion);
    //     let json = JSON.parse("{" + (x.substring(0, x.indexOf('TEXTO') - 3) + "}"));
    //     console.log("jsonparse", json);
    //     let { CALMA, CONFUSION, DISGUST0, ENOJO, FELICIDAD, MIEDO, NEGATIVO, NEUTRAL, Positivo, SORPRESA, TRISTEZA } = json
    //     let datos = [ENOJO, MIEDO, TRISTEZA, SORPRESA, CONFUSION, DISGUST0, CALMA, FELICIDAD]
    //     const datosConclucion = [NEGATIVO, NEUTRAL, Positivo]
    //     this.barChartData.push({
    //       data: datos,
    //       backgroundColor: [(ENOJO > 0) ? '#0BB9B9' : '#525252', (MIEDO > 0) ? '#0BB9B9' : '#525252', (TRISTEZA > 0) ? '#0BB9B9' : '#525252', (SORPRESA > 0) ? '#0BB9B9' : '#525252', (CONFUSION > 0) ? '#0BB9B9' : '#525252', (DISGUST0 > 0) ? '#0BB9B9' : '#525252', (CALMA > 0) ? '#0BB9B9' : '#525252', (FELICIDAD > 0) ? '#0BB9B9' : '#525252'],
    //       label: ' ',
    //       hoverBackgroundColor: [(ENOJO > 0) ? '#0BB9B9' : '#525252', (MIEDO > 0) ? '#0BB9B9' : '#525252', (TRISTEZA > 0) ? '#0BB9B9' : '#525252', (SORPRESA > 0) ? '#0BB9B9' : '#525252', (CONFUSION > 0) ? '#0BB9B9' : '#525252', (DISGUST0 > 0) ? '#0BB9B9' : '#525252', (CALMA > 0) ? '#0BB9B9' : '#525252', (FELICIDAD > 0) ? '#0BB9B9' : '#525252'],
    //     });

    //     this.barChartDataConclusion.push({
    //       data: datosConclucion,
    //       backgroundColor: [(NEGATIVO > 0) ? '#E21D1D' : '#525252', (NEUTRAL > 0) ? '#979797' : '#525252', (Positivo > 0) ? '#0BB9B9' : '#525252'],
    //       label: ' ',
    //       hoverBackgroundColor: [(NEGATIVO > 0) ? '#E21D1D' : '#525252', (NEUTRAL > 0) ? '#979797' : '#525252', (Positivo > 0) ? '#0BB9B9' : '#525252'],
    //     })
    //     this.flagGraph = true;
    //   }
    // );
  }

  getQuestions = (data: any) => {
    const questions = data.questions
    this.questions = [];
    for (let question of questions) {
      if (question.type == "VIDEO") {
        this.questions.push({
          question: question.question
        })
      }
    }
  }

  getQuestionsPresentation = (data: any) => {
    this.questions = [];
    if (data.questionPresentation1 != null)
      this.questions.push({
        question: data.questionPresentation1.question
      })
    if (data.questionPresentation2 != null) {
      this.questions.push({
        question: data.questionPresentation2.question
      })
    }
  }
}
