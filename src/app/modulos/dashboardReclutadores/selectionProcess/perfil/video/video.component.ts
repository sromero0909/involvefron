import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';
@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {
  urlVideoEntrevista: string = "";
  videoEntrevista = false;
  maxRotation: number = 90;
  flagGraph = false;
  color = { presentation: true, interview: false }
  constructor(private router: Router, private activatedRoute: ActivatedRoute,private profileService: ProfileService) { }

  ngOnInit(): void {
    this.urlVideoEntrevista = this.profileService.candiate?.data.pathInterviewVideo;
    if(this.urlVideoEntrevista != null && this.urlVideoEntrevista != "" ){
      this.videoEntrevista= true;
    }else{
      this.videoEntrevista= false;
    }
    
  }
  changeColor = (menu: string) => {
    this.color.presentation = false;
    this.color.interview = false;
    (menu == "p") ? this.color.presentation = true : this.color.interview = true
  }

}
