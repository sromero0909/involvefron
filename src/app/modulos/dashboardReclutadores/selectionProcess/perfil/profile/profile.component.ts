import { Component, OnInit, ViewChild, TemplateRef, ElementRef, Input, HostListener } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { CandidatosComponent } from '../../process/candidatos/candidatos.component';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { debounceTime, distinctUntilChanged, map, tap, switchMap } from 'rxjs/operators';
import { Observable, OperatorFunction } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastService } from 'src/app/services/commons/toast.setvice';
import { CandidatesService } from '../../../../../services/reclutador/vacantes/selectionProcess/candidatos.service';
import { VacantCard } from 'src/app/services/models/cardVacants.model';
import { SidenavVacantesComponent } from '../../../sidenav-vacantes/sidenav-vacantes.component';
import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';
import { MainNavModel } from '../../../talent/models/main-nav.model';
import { BUTTONS_USER_CARD } from 'src/app/enums/keys-buttons.enum';
import { ApiService } from 'src/app/services/api.service';
import { MatDialog } from '@angular/material/dialog';
import { ModalFavoriteComponent } from '../../../talent/candidate-finder/favorites/modal-favorite/modal-favorite.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  @ViewChild(CandidatosComponent) candidateComponent!: CandidatosComponent;
  @ViewChild('addToFavorites', { static: true }) addToFavorites!: TemplateRef<any>;
  @ViewChild('emptyFavoritesList', { static: true }) emptyFavoritesList!: TemplateRef<any>;

  multipleCandidatesSelected: any[] = [];
  favoritesList: any[] = [];
  newFavoriteList: any;
  rows: Array<any> = [];
  candidates: any[] = [];
  closeResult: string;
  mainNavData: MainNavModel;
  isAnyCandidateSelected = false;
  page = 1
  page_size!: number;
  page_number!: number;
  pageTemporal: number = 0;
  pageCandidate: number = 0
  totalElements: number = 0;
  totalPages: number = 0;
  rowsMostrar: number = 0;
  pageActual: number = 0;
  restarRows: number = 0;
  rowsPorPagina = 0;
  contadorChecks = 0;
  mensajeToast: string = "";
  sortBy: string = "&sortBy=lastUpdate"
  sortDirection: string = "&sortDirection=DESC";
  compatibility: string = ""
  urlCV: string = "";
  urlFile: string = "";
  selectedCard: VacantCard;
  banderaCerrarVacante: boolean = false;
  banderaAprobar: boolean = false;
  approveSelected: boolean = false;
  rejectSelected: boolean = false;
  searchFlag: boolean = false;
  rejectFlag: boolean = false;
  nextFlag: boolean = false;
  selecAllinvitation = false;
  shortListActive = false;
  flagMacth: boolean = false;
  flagPagination: boolean = false;
  flagService: boolean = false;
  public modelBuscarVacante: any = "";
  textFilter: string = "";
  filterURL: string = "";
  filterAge: string = "";
  filterSalary: string = "";
  otherFilter: string = ""; //&favourite=false
  filterState: string = "";
  filterGender: string = "";
  filterLevelEducation: string = "";
  filterNationality: string = "";
  filterInstitution: string = "";
  filterVideo: string = "";
  selectionProcessId: string = "";
  textSelect: string = "Seleccionar";
  optionAll: string = "A"
  formularioModalBuscar: any;
  informationCandidate = { phone: "", email: "", git: "", linkedIn: "", file: "", behance: "" }
  statusActive = { cv: false, pruebas: false, videos: false, entrevista: false }
  menuActive = { cv: true, pruebas: false, videos: false, entrevista: false }


  @ViewChild('rechazar', { static: true }) modalRechazar!: TemplateRef<any>;
  @ViewChild('aprobar', { static: true }) modalAprobar!: TemplateRef<any>;
  @ViewChild('invitar', { static: true }) modalInvitar!: TemplateRef<any>;
  @ViewChild('confirmacionRechazo', { static: true }) confirmacionRechazo!: TemplateRef<any>;
  @ViewChild('confirmacionAprobacion', { static: true }) confirmacionAprobacion!: TemplateRef<any>;
  @ViewChild('informacionContacto', { static: true }) informacionContacto!: TemplateRef<any>;
  @ViewChild("selector1") updateCounters!: SidenavVacantesComponent;
  @ViewChild('iconoChekBlanco', { static: true }) iconoChekBlanco!: TemplateRef<any>;
  @Input() initialText = "Por ahora no hay candidatos";

  @Input() buttonsAcceptedRejected = false;
  @Input() showIcons = true;


  isSticky: boolean = false;

  @HostListener('window:scroll', ['$event'])
  checkScroll() {
    this.isSticky = window.pageYOffset >= 250;
  }
  status!: string;
  setProcessSelection!: string
  search: OperatorFunction<string, readonly { name: string }[]> = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(600),
      distinctUntilChanged(),
      switchMap(term => term.length < 3 ? [] :
        this._candidates.searchVacancy(term).pipe(
          map((response: any) => {
            buscarVacante = [];
            if (response != undefined && response != null && response.content.length > 0) {
              const data = response.content;
              for (let i of data) {
                buscarVacante.push({
                  name: i.vacant.position.position + ' - ' + i.vacant.client.name,
                  idVacant: i.vacant.vacantId,
                })
              }
              this.modelBuscarVacante = buscarVacante;
            }
            if (buscarVacante == "") {
              buscarVacante = [];
              buscarVacante.push({ name: "No se encontraron resultados" });
            }
            return buscarVacante
          }),

        )))
  formatter = (x: { name: string }) => x.name;

  constructor(
    private authService: AuthService,
    private toastService: ToastService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private _candidates: CandidatesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private profileService: ProfileService,
    private apiService: ApiService,
    private dialog: MatDialog,
  ) {
    this.selectedCard = this.profileService.getSelectedCard();
    if (this.selectedCard != undefined) {
      this.setProcessSelection = this.selectedCard.setProcessSelection
      this.validateStatus(this.selectedCard.status)
      this.page_number = this.selectedCard.pageActual - 1;
      this.page_size = 1//this.selectedCard.pageSize
      this.rowsMostrar = this.selectedCard.cardActual
      this.pageActual = this.selectedCard.pageActual
      this.pageCandidate = this.selectedCard.cardActual
      this.getFiles(this.pageCandidate - 1)
    }

  }

  ngOnInit(): void {
    if (this.selectedCard != undefined) {
      const rowsTem = this._candidates.getRowsTemporal();
      if (rowsTem != undefined) {
        this.rows = rowsTem
        this.totalElements = this.profileService.getSelectedCard().totalElemt;
        this.totalPages = this.profileService.getSelectedCard().totalPages;
        this.updateRowsMOstradas(this.profileService.getSelectedCard().cardActual)
        this.pageActual = this.profileService.getSelectedCard().cardActual
        this.selectPage(String(this.profileService.getSelectedCard().cardActual))
      } else {
        this.inicializaCards();
      }
      this.inicializarFormularioBuscar();
    }
  }

  scrollTo = (el: HTMLElement) => {
    el.scrollIntoView({ behavior: 'smooth' });
  }

  inicializaCards() {
    this.flagService = false;
    //let params = `page=${this.page_number}&pageSize=${this.page_size}&pageNumber=${this.page_number}${this.orderBy}${this.sortDirection}&vacantId=${this.selectedCard.id}${this.textFilter}`;
    let params = {
      pageNumber: this.page_number,
      pageSize: this.selectedCard.pageSize + 1,
      sortBy: this.sortBy,
      sortDirection: this.sortDirection,
      status: this.status,
      vacantId: this.selectedCard.id,
      wordSearch: this.textFilter,
      filterAge: this.filterAge,
      filterSalary: this.filterSalary,
      otherFilter: this.otherFilter,
      compatibility: this.compatibility,
      filterState: this.filterState,
      filterGender: this.filterGender,
      filterLevelEducation: this.filterLevelEducation,
      filterNationality: this.filterNationality,
      filterInstitution: this.filterInstitution,
      filterURL: this.filterURL,
      filterVideo: this.filterVideo
    }

    this._candidates.getVacantForStatus(params).subscribe(
      (response) => {
        if (response != null) {

          if (!this.flagPagination) {
            this.resetPaginadores();
            this.flagPagination = false
          }
          let candidatos = response.content;

          this.fillTable(candidatos)
        } else {
          this.showMessageError("Ocurrio un error");
        }

        this.totalElements = response.totalElements;
        this.totalPages = response.totalPages;
        if (this.pageActual == 0) {
          this.updateRowsMOstradas(this.selectedCard.cardActual)
        } else {
          this.updateRowsMOstradas(this.pageActual)
        }
        setTimeout(() => {
          this.flagService = true;
        }, 100)
      },
      (error) => {
        this.showMessageError("Ocurrio un error");
      }
    );
  }

  fillTable = (candidatos: any) => {
    for (let candidato in candidatos) {
      this.rows.push({
        id: candidatos[candidato].selectionProcessId,
        check: false,
        porcentaje: parseInt(candidatos[candidato].matchPorcentage) + "%",
        colorPorcentaje: this.coloresPorcentaje(parseInt(candidatos[candidato].matchPorcentage) + "%"),
        tiempo: candidatos[candidato].lastUpdate,
        favorito: candidatos[candidato].isFavourite,
        nombre: (candidatos[candidato].candidate.user.name != null) ? candidatos[candidato].candidate.user.name + " " + candidatos[candidato].candidate.user.lastName + " " + candidatos[candidato].candidate.user.secondLastName : "-",
        licenciatura: (candidatos[candidato].candidate.positionProfile != null) ? candidatos[candidato].candidate.positionProfile.position : "-",
        sueldo: (candidatos[candidato].candidate.desiredSalary != null) ? candidatos[candidato].candidate.desiredSalary : '0',
        edad: (candidatos[candidato].candidate.age != null) ? candidatos[candidato].candidate.age : '-',
        ubicacion: (candidatos[candidato].candidate.statePosition != null) ? candidatos[candidato].candidate.statePosition.concat((candidatos[candidato].candidate.countryPosition != null) ? "" : '') : "-",
        photo: (candidatos[candidato].candidate.user.photo != null) ? candidatos[candidato].candidate.user.photo : "../../../../assets/img/dashboard/vacantes/sinFotoPerfil.svg",
        presentationVideo: (candidatos[candidato].presentationVideo == null) ? false : candidatos[candidato].presentationVideo,
        interviewPresentation: (candidatos[candidato].interviewVideo == null) ? false : candidatos[candidato].interviewVideo, //interviewPresentation,
        candidate: candidatos[candidato].candidate,
        statusTerna: candidatos[candidato].statusTerna,
        reject: candidatos[candidato].isRejected,
        flagMacth: (candidatos[candidato].isWarning == null) ? false : candidatos[candidato].isWarning,
        urls: this.openModalInformacionContacto(candidatos[candidato].candidate),
        data: candidatos[candidato]
      })
    }
  }

  validateStatus = (status: string) => {
    this.status = status;
    this.statusActive.pruebas= this.profileService.card.testsPsychometricComplete;
    switch (status) {
      case "PRUEBAS_Y_VIDEO":
      case "ENTREVISTA":
      case "FINALISTA":
      case "CONTRATADO":
        this.statusActive.entrevista = false;
        break;

      default:
        this.statusActive.entrevista = false;
        break;

    }
  }

  generateArrarSendReminder(template: any, id: string, flag: boolean, presentationVideo: boolean, interview: boolean) {
    let arrayProcessId = []
    let type = "";
    if (flag) {
      for (let card of this.rows) {
        if (card.check) {
          type = (!card.presentationVideo) ? "PRESENTATION" : (card.presentationVideo && !card.interviewPresentation) ? "INTERVIEW" : ""
          if (type != "") {
            arrayProcessId.push({
              "processId": card.id,
              "type": type
            })
          }
        }
      }
    } else {
      type = (!presentationVideo) ? "PRESENTATION" : (presentationVideo && !interview) ? "INTERVIEW" : ""
      arrayProcessId.push({
        "processId": id,
        "type": "PRESENTATION"
      })
    }

    if (arrayProcessId.length == 1) {
      flag = false
    }
    //this.sendReminder(template, id, flag)
    if (arrayProcessId.length > 0) {
      this.sendReminder(template, arrayProcessId, flag)
    }

  }
  sendReminder(template: any, id: any, flag: boolean) {
    this._candidates.sendReminder(id).subscribe(
      (response) => {
        if (response != null && response) {
          this.contadorChecks = 0;
          this.mensajeToast = (flag) ? '"Enviamos tu recordatorio a los candidatos"' : "Enviamos tu recordatorio al candidato"
          this.setMensajeRecoratorio(template,)
        } else {
          this.showMessageError("Ocurrio un error");
        }
      },
      (error) => {
        console.log(error);
        this.showMessageError("Ocurrio un error");
      }
    )
  }
  openModalRechazar(id: string, rejectSelected: boolean) {
    this.selectionProcessId = id;
    this.rejectSelected = rejectSelected;
    this.modalService.open(this.modalRechazar, { centered: true });
  }

  openModalconfirmacionRechazoCandidato(bandera: boolean, rejectFlag: boolean) {
    this.banderaCerrarVacante = bandera;
    this.rejectFlag = rejectFlag;
    this.modalService.open(this.confirmacionRechazo, { centered: true });
  }

  openModalAprobar(id: string, approveSelected: boolean) {
    this.selectionProcessId = id;
    this.approveSelected = approveSelected;
    this.modalService.open(this.modalAprobar, { centered: true });
  }

  openModalconfirmacionAprobacion(bandera: boolean, nextFlag: boolean) {
    this.nextFlag = nextFlag;
    this.banderaAprobar = bandera;
    this.modalService.open(this.confirmacionAprobacion, { centered: true });
  }

  openModalInvitar(id: string, flag: boolean) {
    this.selectionProcessId = id;
    this.selecAllinvitation = flag
    this.modalService.open(this.modalInvitar, { size: "lg", centered: true });
  }

  openModalInformacionContacto(data: any) {
    this.informationCandidate.phone = data.user.phone = (data.user.phone != null) ? data.user.phone : ""
    this.informationCandidate.email = data.user.email
    this.informationCandidate.git = "";
    this.informationCandidate.linkedIn = "";
    this.informationCandidate.file = "";
    this.informationCandidate.behance = ""
    for (let user of data.urlFiles) {
      switch (user.type) {
        case "URL_GIT":
          this.informationCandidate.git = user.pathFile;
          break;
        case "URL_LINKED":
          this.informationCandidate.linkedIn = user.pathFile;
          break;
        case "URL_FOLDER":
          this.informationCandidate.file = user.pathFile;
          break;
        case "URL_BEHANCED":
          this.informationCandidate.behance = user.pathFile;
          break;
      }
    }
    return this.informationCandidate
  }



  openModal = (idModal: string) => {
    let content: any;
    switch (idModal) {
      case BUTTONS_USER_CARD.FAVORITE_TALENT:
        content = this.addUserToFavoritesToMultipleAction();
        break;
      case BUTTONS_USER_CARD.INVITATION:
        content = this.modalInvitar;
        break;
      case BUTTONS_USER_CARD.REJECTED:
        content = this.modalRechazar;
        break;
      case BUTTONS_USER_CARD.APPROVED:
        content = this.modalAprobar;
        break;
      case BUTTONS_USER_CARD.APPROVED_CONFIRMED:
        content = this.confirmacionAprobacion;
        break;
      case BUTTONS_USER_CARD.REJECTED_CONFIRMED:
        content = this.confirmacionRechazo;
        break;
      case BUTTONS_USER_CARD.INFORMATION:
        content = this.informacionContacto;
        break;
    }

    if (content) {
      this.modalService.open(content, { centered: true }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  updateFavorito(id: any) {
    this._candidates.updateFavorite(id).subscribe(
      (response) => {
        if (response != null) {
          for (let i of this.rows) {
            if (id == i.id) {
              i.favorito = response
            }
          }
        } else {
          this.showMessageError("Ocurrio un error");
        }
      },
      (error) => {
        this.showMessageError("Ocurrio un error");
      })
  }


  showSuccess(template: any) {
    this.toastService.remove(1)
    this.toastService.show(template, { classname: 'bg-success text-light ngb-toastsBottom', delay: 5000 });
  }

  setMensajeToast(template: any, flagPlural: boolean) {

    if (this.banderaCerrarVacante) {
      this.mensajeToast = (flagPlural) ? "Por ahora los candidatos no serán notificados de tu decisión" : "Por ahora el candidato no será notificado de tu decisión"
    } else {
      this.mensajeToast = (flagPlural) ? "Enviamos un mensaje  de agredecimiento a los candidatos" : "Enviamos un mensaje  de agredecimiento al candidato"
    }
    this.showSuccess(template);
  }

  setMensajeToastAprobar(template: any) {

    if (this.banderaAprobar) {
      this.mensajeToast = "Enviamos un mensaje de felicitación al candidato"
    } else {
      this.mensajeToast = "Por ahora el candidato no será notificado de tu decisión"
    }
    this.showSuccess(template);
  }

  setMensajeRecoratorio(template: any) {
    this.showSuccess(template);
  }

  setMensajeNotificacion(template: any, flagPlural: boolean) {
    this.mensajeToast = (flagPlural) ? "Por ahora los candidatos no serán notificados de tu decisión" : "Por ahora el candidato no será notificado de tu decisión"
    this.showSuccess(template);
  }

  setMensajeNotificacionFelicitacion(template: any, flagPlural: boolean) {
    this.mensajeToast = (flagPlural) ? "Enviamos un mensaje de felicitación a los candidatos" : "Enviamos un mensaje de felicitación al candidato"
    this.showSuccess(template);
  }
  inicializarFormularioBuscar() {
    this.formularioModalBuscar = this.formBuilder.group({
      modelBuscarVacante: ['', [Validators.required, Validators.minLength(3)]],
    });
  }

  onchangeBUscarVacante() {
    this.formularioModalBuscar.get("modelBuscarVacante")?.valueChanges.subscribe((changes1: any) => {
      this.onPercentChange(changes1);
    });
  }
  onPercentChange(percent: any) {
    if (percent != '')
      this.modelBuscarVacante = percent;
    if (this.modelBuscarVacante != undefined && this.modelBuscarVacante.length > 2) {
      buscarVacante = ["Contador Publico - 102 panasonic", "Contador Publico - 111 walmart", "Contador Publico - 122 Canon"];
    }
  }



  buscarOtraVacante(): void {

    this.formularioModalBuscar.get("modelBuscarVacante")?.setValue("");
    this.formularioModalBuscar.get('modelBuscarVacante').setValidators([]);
    this.formularioModalBuscar.get('modelBuscarVacante').updateValueAndValidity()
  }

  generateArrayInvitation(flag: boolean, template: any) {
    let arrayProcessId = []
    if (flag || this.selecAllinvitation) {
      flag = this.selecAllinvitation
      for (let card of this.rows) {
        if (card.check) {
          arrayProcessId.push(card.id)
        }
      }

    } else {
      arrayProcessId.push(this.selectionProcessId)
    }
    if (arrayProcessId.length == 1) {
      flag = false;
    }
    this.sendInvitation(arrayProcessId, template, flag)
  }

  sendInvitation(array: any, template: any, flag: boolean) {
    this.modalService.dismissAll();
    const idVacante = this.formularioModalBuscar.get("modelBuscarVacante")?.value
    this._candidates.sendInvitation(idVacante.idVacant, array).subscribe(
      (response: any) => {
        if (response != null && response.status == 200) {
          this.contadorChecks = 0;
          if (flag) {
            this.mensajeToast = "Enviamos tu invitación a los candidatos";
          } else {
            this.mensajeToast = "Enviamos tu invitación al candidato";
          }
          this.showSuccess(template);
          this.buscarOtraVacante();
        } else {
          this.showMessageError("Ocurrio un error");
        }
      },
      (error) => {
        this.showMessageError("Ocurrio un error");
      }
    );
  }
  coloresPorcentaje(porcentaje: string) {
    let numero = porcentaje.replace('%', '');
    let color = '';
    if (parseInt(numero) > 70) color = "verde";
    if (parseInt(numero) >= 50 && parseInt(numero) <= 70) color = "naranja";
    if (parseInt(numero) < 50) color = "rojo";
    return color;
  }

  selectPage(page: string) {
    this.page = parseInt(page, 10) || 1;
  }

  formatInput(input: HTMLInputElement) {
    input.value = input.value.replace(FILTER_PAG_REGEX, '');
  }

  rowsMostradas(page: any) {
    if (this.pageTemporal < parseInt(page)) {
      this.flagPagination = true;
      if (this.rows.length < this.totalElements) {
        this.page_number++;
        this.pageActual = page
        this.inicializaCards();
      } else {
        this.rowsMostrar = page
        this.page_number++;
        this.updateRowsMOstradas(page)
      }
    } else {
      if (this.rowsMostrar > 1) {
        //this.page_number --;
        this.rowsMostrar = this.rowsMostrar - 1;
      }
      this.updateRowsMOstradas(page)
    }
  }

  updateRowsMOstradas(page: any) {
    setTimeout(() => {
      this.rowsMostrar = page
      this.pageTemporal = page;
    }, 100)
    this.pageCandidate = page
    this.pageActual = page
    this.updateCandidate();
  }

  selecRow() {
    this.resetPaginadores();
    this.flagPagination = false;
    this.inicializaCards()
  }

  resetPaginadores() {
    this.rowsMostrar = 0;
    this.pageActual = 0;
    this.pageCandidate = 0
    this.page_number = 0;
    this.restarRows = 0;
    this.pageTemporal = 0;
    this.rows = [];
    this.page = 1
  }

  searchVacant(text: string) {
    if (text.length > 2) {
      this.searchFlag = true;
      this.textFilter = "&wordSearch=" + text
    } else {
      this.textFilter = "";
      this.searchFlag = false;
    }
    //this.resetPaginadores();
    this.flagPagination = false;
    this.inicializaCards();
  }

  generateArrayRejectCandidate(flag: boolean, template: any) {
    let arrayProcessId = []
    if (flag || this.rejectSelected) {
      flag = this.rejectSelected
      for (let card of this.rows) {
        if (card.check) {
          arrayProcessId.push(card.id)
        }
      }
    } else {
      arrayProcessId.push(this.selectionProcessId)
    }
    if (arrayProcessId.length == 1) {
      flag = false
    }
    this.rejectCandidate(template, arrayProcessId, flag)
  }

  rejectCandidate = (template: any, arrayCandidates: any, flagNotification: boolean) => {
    this._candidates.rejectCandidate(this.rejectFlag, arrayCandidates).subscribe(
      (response: any) => {
        if (response != null && response.status == 200) {
          this.updateCounters.selectionProcessCounter();
          this.flagPagination = false;
          //this.resetPaginadores();
          if (this.shortListActive) {
            this.updateCounters.getShortList(true);
          } else {
            this.inicializaCards();
          }
          this.contadorChecks = 0
          this.setMensajeToast(template, flagNotification)
        } else {
          this.showMessageError("Ocurrio un error");
        }

      },
      (error) => {
        this.showMessageError("Ocurrio un error");
      }
    );
  }

  generateArrayNextStage(flag: boolean, template: any) {
    let arrayProcessId = []
    if (this.approveSelected) {
      for (let card of this.rows) {
        if (card.check) {
          arrayProcessId.push(card.id)
        }
      }
    } else {
      arrayProcessId.push(this.selectionProcessId)
    }
    if (arrayProcessId.length == 1) {
      this.approveSelected = false
    }
    this.nextStage(template, arrayProcessId, flag, this.approveSelected)
  }

  nextStage = (template: any, arrayCandidates: any, flagNotification: boolean, flagPlural: boolean) => {
    this._candidates.nextStage(flagNotification, arrayCandidates).subscribe(
      (response: any) => {
        if (response != null && response.status == 200) {
          this.updateCounters.selectionProcessCounter();
          this.flagPagination = false;
          //this.resetPaginadores();
          if (this.shortListActive) {
            this.updateCounters.getShortList(true);
          } else {
            this.inicializaCards();
          }
          this.contadorChecks = 0
          if (flagNotification) {
            this.setMensajeNotificacionFelicitacion(template, flagPlural)
          } else {
            this.setMensajeNotificacion(template, flagPlural)
          }
          this.router.navigateByUrl('/dashboard/proceso/seleccion/entrevistas/false');
        } else {
          this.showMessageError("Ocurrio un error");
        }

      },
      (error) => {
        this.showMessageError("Ocurrio un error");
      }
    );
  }
  showMessageError(dangerTpl: any) {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-lightngb-toastsBottom', delay: 3000 });
  }

  spreadfilter1 = (filter: any) => {
    switch (filter.flag) {
      case "ordenamiento":
        this.sortDirection = filter.filter;
        this.otherFilter = "";
        //this.resetPaginadores();
        this.flagPagination = false;
        this.inicializaCards();
        this.shortListActive = false;
        break;
      case "favoritos":
        this.otherFilter = filter.filter;
        //this.resetPaginadores();
        this.flagPagination = false;
        this.inicializaCards();
        this.shortListActive = false;
        break;
      case "terna":
        this.resetPaginadores();
        this.fillTable(filter.filter);
        this.shortListActive = true
        this.rowsMostrar = this.rows.length;
        break;
    }

  }

  spreadfilter2 = (filter: any) => {
    this.compatibility = filter;
    //this.resetPaginadores();
    this.flagPagination = false;
    this.inicializaCards();
  }

  spreadfilterVideo = (filter: any) => {
    this.filterVideo = filter
    //this.resetPaginadores();
    this.flagPagination = false;
    this.inicializaCards();
  }

  spreadfilterAge = (filter: any) => {
    this.filterAge = filter;
    //this.resetPaginadores();
    this.flagPagination = false;
    this.inicializaCards();
  }

  spreadfilterSalary = (filter: any) => {
    this.filterSalary = filter;
    //this.resetPaginadores();
    this.flagPagination = false;
    this.inicializaCards();
  }

  spreadMorefilters = (filter: any) => {
    this.filterURL = filter.filterURL;
    this.filterState = filter.filterState;
    this.filterGender = filter.filterGender;
    this.filterLevelEducation = filter.filterLevelEducation;
    this.filterNationality = filter.filterNationality;
    this.filterInstitution = filter.filterInstitution;
    //this.resetPaginadores();
    this.flagPagination = false;
    this.inicializaCards();
  }

  onchangeCheck(id: number) {
    for (let i = 0; i < this.rows.length; i++) {
      if (this.rows[i].id == id) {
        (this.rows[i].check) ? this.contadorChecks-- : this.contadorChecks++
        this.rows[i].check = !this.rows[i].check
      }
    }
  }

  limpiarSeleccion() {
    for (let i = 0; i < this.rows.length; i++) {
      this.contadorChecks = 0
      this.rows[i].check = false
    }
  }

  selectedOption(option: string) {
    this.optionAll = option
  }

  applySelection() {
    switch (this.optionAll) {
      case "A":
        this.openModalAprobar('', true);
        break;
      case "R":
        this.openModalRechazar('', true);
        break;
      case "E":
        this.generateArrarSendReminder(this.iconoChekBlanco, "", true, false, false)
        break;
      case "I":
        this.openModalInvitar("", true)
        break;
    }
  }


  newShortList = () => {
    let arrayProcessId = []
    for (let card of this.rows) {
      if (card.check) {
        arrayProcessId.push(card)
      }
    }
    let fechaHoy = formatDate(new Date(), 'yyyy-MM-dd', 'en_US')
    const params = {
      "firstProcessId": arrayProcessId[0].id,
      "secondProcessId": arrayProcessId[1].id,
      "thirdProcessId": arrayProcessId[2].id,
    }

    this._candidates.newShortList(params).subscribe(
      (response: any) => {
        if (response != null && response.code == 200) {
          this.updateCounters.getShortList(true);
          this.limpiarSeleccion();
          this.showSuccess(response.message)
        }
        if (response != null && response.code != 200) {
          this.showMessageError(response.message)
        }
      },
      (error) => {
        this.showMessageError(error)
      }
    );
  }

  updateCandidate = () => {
    let candidate;
    this.pageCandidate
    this.getFiles(this.pageCandidate - 1)
    for (let i = 0; i < this.rows.length; i++) {
      if (i == (this.pageCandidate - 1)) {
        candidate = {
          idProcess: this.rows[i].id,
          color: this.rows[i].colorPorcentaje,
          porcentaje: this.rows[i].porcentaje,
          data: this.rows[i].data
        }
        if(this.rows[i].candidate?.workExperiencie[i]?.working){
          if(this.rows[i].candidate?.workExperiencie[i]?.company== undefined){
            this.mainNavData = {
              title: `${this.rows[i].candidate?.user?.name} ${this.rows[i].candidate?.user?.lastName}`,
              subtitle: `${this.rows[i].candidate?.positionSearch}`,
              search: false,
              iconBack: true,
            }
          }else{
            this.mainNavData = {
              title: `${this.rows[i].candidate?.user?.name} ${this.rows[i].candidate?.user?.lastName}`,
              subtitle: `${this.rows[i].candidate?.positionSearch} - ${this.rows[i].candidate?.workExperiencie[i]?.company}`,
              search: false,
              iconBack: true,
            }
          }

        }else{
          this.mainNavData = {
            title: `${this.rows[i].candidate?.user?.name} ${this.rows[i].candidate?.user?.lastName}`,
            subtitle: `${this.rows[i].candidate?.positionSearch}`,
            search: false,
            iconBack: true,
          }
        }
        
       
        this.profileService.updateCandidate(candidate)
      }
    }
  }

  redirectToProfile = () => {
    this.router.navigateByUrl('dashboard/proceso/perfil/(profile:cv)');
  }

  backToProcess = () => {
    let url = "./vacantes/activas"
    switch (this.profileService.getSelectedCard().setProcessSelection) {
      case "candidatos":
        url = '../../proceso/seleccion/candidatos/true';
        break;
      case "pruebas":
        url = '../../proceso/seleccion/pruebas-video/true';
        break;
      case "entrevistas":
        url = '../../proceso/seleccion/entrevistas/true';
        break;
      case "finalistas":
        url = '../../proceso/seleccion/finalistas/true';
        break;
      case "contratados":
        url = '../../proceso/seleccion/contratados/true';
        break;
      case "talent":
        url = '../../talent/candidatos';
        break;
    }
    this.router.navigate([url], {
      relativeTo: this.activatedRoute,
    });
  }

  goToLink = (url1: string) => {
    let url: string = '';
    if (!/^http[s]?:\/\//.test(url1)) {
      url += 'http://';
    }

    url = url + url1
    if (url1 !== undefined && url1 != null && url1 != "") {
      window.open(url, '_blank');
    }
  }

  getFiles = (numCandidate: number) => {
    this.urlFile = "";
    this.urlCV = "";
    for (let i = 0; i < this.rows.length; i++) {
      if (i == numCandidate) {
        const files = this.rows[i].candidate.urlFiles
        for (let candidate of files) {
          if (candidate.type == "URL_CV")
            this.urlCV = candidate.pathFile;
          if (candidate.type == "URL_FILE")
            this.urlFile = candidate.pathFile;
        }
      }
    }
  }

  changeColorMenu = (menu: string) => {
    this.resetMenu();
    switch (menu) {
      case 'cv':
        this.menuActive.cv = true;
        break;
      case 'pruebas':
        this.menuActive.pruebas = true;
        break;
      case 'videos':
        this.menuActive.videos = true;
        break;
      case 'entrevista':
        this.menuActive.entrevista = true;
        break;
    }
  }

  resetMenu = () => {
    this.menuActive.cv = false;
    this.menuActive.entrevista = false,
      this.menuActive.pruebas = false;
    this.menuActive.videos = false;
  }



  openCreateDialog = () => {
    this.modalService.dismissAll();
    this.dialog.open(ModalFavoriteComponent, { data: { create: true } }).afterClosed().subscribe((returnData) => {
      this.apiService.post({
        name: returnData.name,
        image: {
          imageId: returnData.imageId,
          pathImageBackground: returnData.imageBackground,
          pathImageMiniature: returnData.imageBackground
        }
      }, '/list-favorite').subscribe((resp: any) => {
        if (resp.body.listFavoriteId) {
          this.newFavoriteList = resp.body;
          console.log(this.newFavoriteList)
          this.multipleCandidatesSelected = this.rows[0].candidate.candidateId;
          this.onAddUsersToFavoriteListOrVacantProcess(this.newFavoriteList, () => {
            this.toastService.show('Se creó el grupo', { classname: 'bg-success text-light ngb-toastsBottom', delay: 3000 });
            this.router.navigate(['/dashboard', 'talent', 'candidatos', this.newFavoriteList.listFavoriteId, 'true']);
          });
        }
        else {
          this.toastService.showMessageError('Ocurrió un error al crear la lista de favoritos');
        }
      });
    });
  }

  addUserToFavoritesToMultipleAction(user?: any): void {
    if (user !== undefined) {
      this.multipleCandidatesSelected = [user];
    } else {
      this.multipleCandidatesSelected = [this.rows[0].candidate.candidateId];
    }

    this.apiService.get('/list-favorite/page').subscribe((resp: any) => {
      if (resp.content.length === 0) {
        this.modalService.open(this.emptyFavoritesList, { centered: true, size: 'md' });

      }
      else {
        this.favoritesList = resp.content;
        this.modalService.open(this.addToFavorites, { size: 'md', centered: true });
      }
    }, (error) => {
      console.log(error);
    });
  }



  onAddUsersToFavoriteListOrVacantProcess(favoriteList: any, callback?: any): void {
    const users = [this.rows[0].candidate.candidateId];
    if (favoriteList !== null) {
      const data = {
        candidates: users,
        listFavoriteId: favoriteList.listFavoriteId
      };
      this.apiService.post(data, '/list-favorite/candidate').subscribe((success) => {
        if (callback !== undefined) {
          callback();
        }
        else {
          this.toastService.show('Tus favoritos se actualizaron',
            { classname: 'bg-success text-light ngb-toastsBottom ngb-toastsBottom', delay: 3000 });
          this.modalService.dismissAll();
        }
      },
        (error) => {
          console.error(error);
        });
    }
  }

  closeModals() {
    this.modalService.dismissAll();
  }

}
let buscarVacante: any = [""]
const FILTER_PAG_REGEX = /[^0-9]/g;