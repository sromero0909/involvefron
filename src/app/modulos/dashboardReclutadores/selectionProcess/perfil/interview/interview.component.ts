import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { InterviewService } from 'src/app/services/reclutador/vacantes/selectionProcess/interview.service';
import { ToastService } from 'src/app/services/commons/toast.setvice';

import { Observable } from 'rxjs/Observable';
import { ProfileService } from 'src/app/services/reclutador/vacantes/selectionProcess/profile.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.scss']
})
export class InterviewComponent implements OnInit, OnDestroy {

  formInterview!: FormGroup;
  candidate$!: Observable<any[]>;
  idTexArea = { expLab:{id: "",text:""}, biggestChallenge: {id: "",text:""}, greatestAchievement: {id: "",text:""}, qualities: {id: "",text:""}, opportunityAreas: {id: "",text:""} }
  private subscriptions!: Subscription;

  constructor(private formBuilder: FormBuilder,
    private interviewService: InterviewService,
    public toastService: ToastService,
    private profileService: ProfileService
  ) {
    this.initializeFormInterview();
  }

  ngOnInit(): void {
    console.log("candidato activo entrevista primera carga", this.profileService.getActiveCandidate());
    if (this.profileService.getActiveCandidate() != undefined) {
      this.getExpLab();
    }
    this.changeTheCandidate()
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }


  changeTheCandidate() {
    this.candidate$ = this.profileService.getCandidate$();
    this.subscriptions = this.candidate$.subscribe(candidate => this.getExpLab());
  }

  initializeFormInterview = () => {
    this.formInterview = this.formBuilder.group({
      expLab: [{ value: '', disabled: false }, []],
      biggestChallenge: [{ value: '', disabled: false }, [Validators.required]],
      greatestAchievement: [{ value: '', disabled: false }, [Validators.required]],
      qualities: [{ value: '', disabled: false }, [Validators.required]],
      opportunityAreas: [{ value: '', disabled: false }, [Validators.required]],
    });
  }

  getExpLab = () => {
    this.resetTextArea();
    this.interviewService.getText(this.profileService.getActiveCandidate().idProcess).subscribe(
      (response: any) => {
        console.log("es el response de interview", response)
        if (response.code == 200) {
          if (response.data.length > 0) {
            this.setTextArea(response.data)
          }
        }
      },
      (error) => {

      }
    );
  }

  saveExpLb = (resultProfileId: string, type: string,textOld:string) => {
    let value = this.getTextArea(type)
    const primerBlanco = /^ /
    const ultimoBlanco = / $/
    const variosBlancos = /[ ]+/g

    value = value.replace(variosBlancos, " ");
    value = value.replace(primerBlanco, "");
    value = value.replace(ultimoBlanco, "");
    const valueTroceado = String(value).split(" ");
    const numeroPalabras = valueTroceado.length;
    console.log("es el value", value, numeroPalabras);
    let params = []
    if(textOld != value && numeroPalabras > 2){
      if (resultProfileId != "") {
        params.push({
          resultProfileId: resultProfileId,
          "selectionProcessId": this.profileService.getActiveCandidate().idProcess,
          "text": value,
          "type": type
  
        })
      } else {
        params.push({
          "selectionProcessId": this.profileService.getActiveCandidate().idProcess,
          "text": value,
          "type": type
  
        })
      }
      this.interviewService.saveText(params).subscribe(
        (response: any) => {
          console.log("es el response al aguardar texto", response)
          if (response != undefined && response.code != 200) {
            this.showMessageError(response.message)
          }
        }, (error) => {
          this.showMessageError(error)
        }
      );
    }
    
  }

  getTextArea(type: string) {
    let text = ""
    switch (type) {
      case "INTERVIEW_EXPERIENCE":
        text = this.formInterview.get("expLab")?.value
        break;
      case "INTERVIEW_CHALLENGE":
        text = this.formInterview.get("biggestChallenge")?.value
        break;
      case "INTERVIEW_ACHIEVEMENT":
        text = this.formInterview.get("greatestAchievement")?.value
        break;
      case "INTERVIEW_QUALITIES":
        text = this.formInterview.get("qualities")?.value
        break;
      case "INTERVIEW_CHANCE":
        text = this.formInterview.get("opportunityAreas")?.value
        break;
    }

    return text
  }

  setTextArea(data: any) {
    let text = ""
    for (let i of data) {
      switch (i.type) {
        case "INTERVIEW_EXPERIENCE":
          this.formInterview.controls['expLab'].setValue(i.text)
          this.idTexArea.expLab.id = i.resultProfileId;
          this.idTexArea.expLab.text = i.text
          break;
          case "INTERVIEW_CHALLENGE":
            this.formInterview.controls['biggestChallenge'].setValue(i.text)
            this.idTexArea.biggestChallenge.id = i.resultProfileId
            this.idTexArea.biggestChallenge.text = i.text
            break;
          case "INTERVIEW_ACHIEVEMENT":
            this.formInterview.controls['greatestAchievement'].setValue(i.text)
            this.idTexArea.greatestAchievement.id = i.resultProfileId;
            this.idTexArea.greatestAchievement.text = i.text;
            break;
          case "INTERVIEW_QUALITIES":
            this.formInterview.controls['qualities'].setValue(i.text)
            this.idTexArea.qualities.id = i.resultProfileId;
            this.idTexArea.qualities.text = i.text;
            break;
          case "INTERVIEW_CHANCE":
            this.formInterview.controls['opportunityAreas'].setValue(i.text)
            this.idTexArea.opportunityAreas.id = i.resultProfileId,
            this.idTexArea.opportunityAreas.text = i.text;
            break;   
      }
    }
  }

  resetTextArea = () =>{
    this.formInterview.controls['expLab'].setValue("")
    this.formInterview.controls['biggestChallenge'].setValue("")
    this.formInterview.controls['greatestAchievement'].setValue("")
    this.formInterview.controls['qualities'].setValue("")
    this.formInterview.controls['opportunityAreas'].setValue("")
  }

  showMessageError = (dangerTpl: any) => {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-lightngb-toastsBottom', delay: 3000 });
  }
}
