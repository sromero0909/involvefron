import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/commons/guards/auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ComparativaComponent } from './estadisticas-web/comparativa/comparativa.component';
import { DetalleComponent } from './estadisticas-web/detalle/detalle.component';
import { EstadisticasWebComponent } from './estadisticas-web/estadisticas-web.component';
import { GraficasComponent } from './estadisticas-web/graficas/graficas.component';
import { ResumenComponent } from './estadisticas-web/resumen/resumen.component';
import { BorradoresComponent } from './vacantes/borradores/borradores.component';
import { CerradasComponent } from './vacantes/cerradas/cerradas.component';
import { CrearVacanteComponent } from './vacantes/crear-vacante/crear-vacante.component';
import { EnpausaComponent } from './vacantes/enpausa/enpausa.component';
import { CondicionesComponent } from './vacantes/pasos-crearvacante/condiciones/condiciones.component';
import { DescripcionComponent } from './vacantes/pasos-crearvacante/descripcion/descripcion.component';
import { PositionComponent } from './vacantes/pasos-crearvacante/position/position.component';
import { PruebasVideoComponent } from './vacantes/pasos-crearvacante/pruebas-video/pruebas-video.component';
import { PublicarVacanteComponent } from './vacantes/pasos-crearvacante/publicar-vacante/publicar-vacante.component';
import { ReclutamientoComponent } from './vacantes/pasos-crearvacante/reclutamiento/reclutamiento.component';
import { Requeriments2Component } from './vacantes/pasos-crearvacante/requeriments2/requeriments2.component';
import { RequirementsComponent } from './vacantes/pasos-crearvacante/requirements/requirements.component';
import { RequisitosComponent } from './vacantes/pasos-crearvacante/requisitos/requisitos.component';
import { VistaPreviaComponent } from './vacantes/pasos-crearvacante/vista-previa/vista-previa.component';
import { PlantillasComponent } from './vacantes/plantillas/plantillas.component';
import { VacantesComponent } from './vacantes/vacantes.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        canActivate: [AuthGuard],
        redirectTo: '/dashboard',
        pathMatch: 'full'
      },
      {
        path: 'vacantes/activas',
        canActivate: [AuthGuard],
        component: VacantesComponent,
      },
      {
        path: 'vacantes/enpausa',
        canActivate: [AuthGuard],
        component: EnpausaComponent,
      },
      {
        path: 'vacantes/borradores',
        canActivate: [AuthGuard],
        component: BorradoresComponent,
      },
      {
        path: 'vacantes/cerradas',
        canActivate: [AuthGuard],
        component: CerradasComponent,
      },
      {
        path: 'vacantes/plantillas',
        component: PlantillasComponent,
      },
      {
        path: 'vacantes/create',
        component: CrearVacanteComponent,
        children: [
          {
            path: 'description',
            component: DescripcionComponent,
            outlet: 'vacancy'
          },
          {
            path: 'position',
            component: PositionComponent,
            outlet: 'vacancy'
          },
          {
            path: 'requeriments',
            component: RequirementsComponent,
            outlet: 'vacancy'
          },
          {
            path: 'questionnaire',
            component: Requeriments2Component,
            outlet: 'vacancy'
          },
          {
            path: 'tests',
            component: PruebasVideoComponent,
            outlet: 'vacancy'
          },
          {
            path: 'recruiters',
            component: ReclutamientoComponent,
            outlet: 'vacancy'
          },
          // {
          //   path: 'requisitos',
          //   component: RequisitosComponent,
          //   outlet: "requisitos"
          // },
          // {
          //   path: 'video',
          //   component: PruebasVideoComponent,
          //   outlet: "pruebas"
          // },
          // {
          //   path: 'reclutamiento',
          //   component: ReclutamientoComponent,
          //   outlet: "equipo"
          // },
          {
            path: 'preview',
            component: VistaPreviaComponent,
            outlet: 'vacancy'
          },
          {
             path: 'vacante',
             component: PublicarVacanteComponent,
             outlet: "vacancy"
          },
        ]
      },
      {
        path: 'estadisticas-web',
        component: EstadisticasWebComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: 'resumen',
            component: ResumenComponent,
          },
          {
            path: 'comparativa',
            component: ComparativaComponent,
          },
          {
            path: 'graficas',
            component: GraficasComponent,
          },
          {
            path: 'detalle',
            component: DetalleComponent,
          }
        ]
      },


      {
        path: 'proceso',
        canActivate: [AuthGuard],
        loadChildren: () => import('../dashboardReclutadores/selectionProcess/selection-process.module').then(
          m => m.SelectionProcessModule
        )
      },
      {
        path: 'proceso',
        canActivate: [AuthGuard],
        loadChildren: () => import('./selectionProcess/perfil/profile.module').then(
          m => m.PerfilModule
        )
      },
      {
        path: 'talent',
        canActivate: [AuthGuard],
        loadChildren: () => import('./talent/talent.module').then(
          m => m.TalentModule
        )
      },
      {
        path: 'clientes',
        canActivate: [AuthGuard],
        loadChildren: () => import('./clients/clients.module').then(
          m => m.ClientsModule
        )
      },
      {
        path: 'configuration',
        canActivate: [AuthGuard],
        loadChildren: () => import('./configuration/configuration.module').then(
          m => m.ConfigurationModule
        )
      },
      // {
      //   path: 'estadisticas',
      //   loadChildren: () => import('./clients/clients.module').then(
      //     m => m.ClientsModule
      //   )
      // }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class DashBoardRoutingModule { }
