import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardCandidatoComponent } from '../dashboardCandidato/dashboard-candidato/dashboard-candidato.component';
import { DashBoardCandidatoRoutingModule } from './dashboardCandidato-routing.module';
import { SidenavDashboardComponent } from './sidenav-dashboard/sidenav-dashboard.component';
import { OfertasComponent } from './ofertas/ofertas.component';
import { PostulacionesComponent } from './postulaciones/postulaciones.component';
import { MensajesComponent } from './mensajes/mensajes.component';
import { SidenavEditarcvComponent } from './sidenav-editarcv/sidenav-editarcv.component';
import { HeadereditarCvComponent } from './headereditar-cv/headereditar-cv.component';




@NgModule({
  declarations: [
    DashboardCandidatoComponent,
    SidenavDashboardComponent,
    OfertasComponent,
    PostulacionesComponent,
    MensajesComponent,
    SidenavEditarcvComponent,
    HeadereditarCvComponent,
   
  ],
  imports: [
    CommonModule,
    DashBoardCandidatoRoutingModule
  ]
})
export class DashboarCandidatodModule { }
