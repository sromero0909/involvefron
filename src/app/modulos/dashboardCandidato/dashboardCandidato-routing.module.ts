import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardCandidatoComponent } from './dashboard-candidato/dashboard-candidato.component';
import { OfertasComponent } from './ofertas/ofertas.component';
import { SidenavEditarcvComponent } from './sidenav-editarcv/sidenav-editarcv.component';


const routes: Routes = [
  {
    path: '',
    component: DashboardCandidatoComponent,
    children: [
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class DashBoardCandidatoRoutingModule { }