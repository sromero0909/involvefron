import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-candidato',
  templateUrl: './dashboard-candidato.component.html',
  styleUrls: ['./dashboard-candidato.component.scss']
})
export class DashboardCandidatoComponent implements OnInit {
  editarPerfil = false;
  constructor() { }

  ngOnInit(): void {
  }

  editaPerfil():void {
    this.editarPerfil = true; 
  }

  regresaDashBoard(): void {
    this.editarPerfil = false;
  }

}
