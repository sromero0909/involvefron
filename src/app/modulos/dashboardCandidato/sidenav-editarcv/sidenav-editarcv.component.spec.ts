import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidenavEditarcvComponent } from './sidenav-editarcv.component';

describe('SidenavEditarcvComponent', () => {
  let component: SidenavEditarcvComponent;
  let fixture: ComponentFixture<SidenavEditarcvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidenavEditarcvComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidenavEditarcvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
