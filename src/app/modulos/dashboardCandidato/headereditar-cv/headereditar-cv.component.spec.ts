import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadereditarCvComponent } from './headereditar-cv.component';

describe('HeadereditarCvComponent', () => {
  let component: HeadereditarCvComponent;
  let fixture: ComponentFixture<HeadereditarCvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeadereditarCvComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadereditarCvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
