import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-postulaciones',
  templateUrl: './postulaciones.component.html',
  styleUrls: ['./postulaciones.component.scss']
})
export class PostulacionesComponent implements OnInit {
  hayPlantillas:boolean = false;
  claseContenedor:string =''
  constructor() { 

  }

  ngOnInit(): void {
    this.claseContenedor = (this.hayPlantillas) ? 'contenedorVacantes' : 'contenedor'

  }

}
