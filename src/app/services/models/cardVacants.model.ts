
export interface VacantCard{
    id:string,
    idShow:string,
    name:string,
    client:string,
    pageSize:number,
    setProcessSelection:string,
    status:string;
    pageActual:number,
    cardActual:number,
    totalElemt:number,
    totalPages:number,
    rowsMostrar:number,
    testsPsychometricComplete:boolean
}