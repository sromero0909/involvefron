export interface User {
  company: {
    companyId: string,
    company: any,
    companySize: string,
    pathLogo: string,
    sector: any,
    industry: any,
    typeCompany: string,
    country: string
  }
  recruiterId: string
  user: {
    acceptNewsletter: boolean
    acceptPrivacy: boolean
    acceptTerms: boolean
    attempts: number
    checkCode: any
    createDate: string
    email: string
    gender: any
    lastName: string
    name: string
    password: string
    phone: string
    photo: string
    secondLastName: string
    userId: string
    userRol: string
  }

}