import { Injectable, TemplateRef } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ToastService {
  toasts: any[] = [];
  showToast: boolean = false;

  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    console.log("es textOrTpl ", typeof textOrTpl);
    if (typeof textOrTpl == "object") {
      this.toasts.push({ textOrTpl, ...options });
    } else {
      const array = String(textOrTpl).split(',')
      let flagIcono = "true"
      let urlIcono = "/assets/img/general/ckeckBlanco.svg"
      console.log("arar", array);
      if (array.length == 1) {
        textOrTpl = array[0]
      }

      if (array.length == 2 || array.length == 3) {
        flagIcono = (array[1] != undefined) ? "true" : "false"
      }
      if (array.length == 3) {
        urlIcono = (array[2] != undefined) ? array[2] : "/assets/img/general/ckeckBlanco.svg"
      }

      this.toasts.push({ flagIcono, textOrTpl: array[0], urlIcono, ...options });
    }
  }

  showNotification(notification: any, textOrTpl: string | TemplateRef<any>, options: any = {}) {
    console.log("es textOrTpl ", typeof textOrTpl);
    if (typeof textOrTpl == "object") {
      this.toasts.push({ textOrTpl, ...options });
    } else {
      const array = String(textOrTpl).split(',')
      let flagIcono = "false"
      let urlIcono = "/assets/img/general/ckeckBlanco.svg"
      console.log("arar", array);
      if (array.length == 1) {
        textOrTpl = array[0]
      }

      if (array.length == 2 || array.length == 3) {
        flagIcono = (array[1] != undefined) ? "true" : "false"
      }
      if (array.length == 3) {
        urlIcono = (array[2] != undefined) ? array[2] : "/assets/img/general/ckeckBlanco.svg"
      }
      console.log("es la noticacion", notification);
      this.toasts.push({
        flagIcono, textOrTpl: array[0],
        urlIcono,
        showHeader: notification.notification,
        textHeader: notification.textHeader,
        ...options
      });
      console.log("es la notdespiesicacion", this.toasts);
    }
  }
  remove(toast: any) {
    this.toasts = []
  }

  setMostrarToast(bandera: boolean) {
    this.showToast = bandera
  }

  getMostrarToast() {
    return this.showToast
  }

  showMessageError = (dangerTpl: any) => {
    this.show(dangerTpl, { classname: 'bg-danger text-light ngb-toastsBottom', delay: 3000 });
  }

  showSuccess = (mensaje: any) => {
    this.remove(1)
    this.show(mensaje, { classname: 'bg-success text-light ngb-toastsBottom', delay: 5000 });
  }

  showSuccessNotification = (notificacion: any, message: any) => {
    this.showNotification(notificacion, message, { classname: 'bg-success text-light ngb-toastsBottom', delay: 5000 });
  }
}
