import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  constructor(private loaderService:NgxSpinnerService) { }


  showLoader(){
    this.loaderService.show();
  }

  hideLoader(){
    this.loaderService.hide();
  }
}
