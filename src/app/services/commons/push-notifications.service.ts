import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { async } from 'q';
import { Observable } from 'rxjs-compat/Observable';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PushNotificationsService {

  tokenNotification: string = "";
  messagingFirebase: firebase.messaging.Messaging;
  numNotifications: any;
  constructor() {
    if (!firebase.apps.length) {
      firebase.initializeApp(environment.firebase);
    } else {
      firebase.app(); // if already initialized, use that one
    }
    this.messagingFirebase = firebase.messaging();
  }

  requestPermission = () => {
    return new Promise(async (resolve, reject) => {
      const permsis = await Notification.requestPermission()

      if (permsis === "granted") {
        const tokenFirebase = await this.messagingFirebase.getToken({ vapidKey: environment.VAPID_KEY });
        resolve(tokenFirebase);
      } else {
        reject("No se otorgaron los permisos")
      }
    })
  }

  private messaginObservable = new Observable<any>(observe => {
    this.messagingFirebase.onMessage(payload => {
      observe.next(payload)
    })
  })

  receiveMessage() {
    return this.messaginObservable;
  }

  setNumNotifications = (notification: any) => {
    this.numNotifications = notification;
  }

  getNumNotifications = () => {
    return this.numNotifications
  }

  setTokenNotification = (token: any) => {
    this.tokenNotification = token;
  }

  getTokenNotification = () => {
    return this.tokenNotification
  }
}