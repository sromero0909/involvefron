import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LoaderService } from './loader.service';

@Injectable()
export class InterceptorLoaderService implements HttpInterceptor {

  constructor(private loaderService: LoaderService) { }

  // intercept(req: HttpRequest<any>,next:HttpHandler):Observable<HttpEvent<any>>{
  //   this.loaderService.showLoader();

  //   return next.handle(req).pipe(
  //     finalize(() =>{this.loaderService.hideLoader()
  //     })
  //   )
  // }

  private requests: HttpRequest<any>[] = [];
  removeRequest(req: HttpRequest<any>) {
    const i = this.requests.indexOf(req);
    if (i >= 0) {
      this.requests.splice(i, 1);
    }
    //this.loaderService.isLoading.next(this.requests.length > 0);
    if(this.requests.length === 0){
      this.loaderService.hideLoader();//oculta tu loader
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.requests.push(req);
    //console.log("No of requests--->" + this.requests.length);
    //this.loaderService..next(true);
    this.loaderService.showLoader(); //llamas a tu spinner
    
    return new Observable(observer => {
      const subscription = next.handle(req)
        .subscribe(
          event => {
            if (event instanceof HttpResponse) {
              setTimeout(() => {
                this.removeRequest(req);
                observer.next(event);
              }, 100);
              
            }
          },
          err => {
            //alert('error' + err);
            this.removeRequest(req);
            observer.error(err);
          },
          () => {
            setTimeout(() => {
              this.removeRequest(req);
            observer.complete();
            }, 100);
            
          });
      // remove request from queue when cancelled
      return () => {
        this.removeRequest(req);
        subscription.unsubscribe();
      };
    });
  }
}
