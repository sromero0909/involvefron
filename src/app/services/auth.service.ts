import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { NgbPopoverWindow } from '@ng-bootstrap/ng-bootstrap/popover/popover';
import { CookieService } from "ngx-cookie-service";
import {formatDate} from '@angular/common';
import { Options } from '@angular-slider/ngx-slider';
import { environment } from 'src/environments/environment';
import { VacanteStepThree } from './reclutador/vacantes/borradores/interfaces/VacanteStepThree.interface';

import { User } from './models/user.model';
@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private headers = new HttpHeaders().set('Content-Type','application/json');
  private attempst:boolean = false;
  private pass:string = "";
  private user!:User
  
  tokeuser = this.cookies.get("token");
  arrToken =  this.tokeuser.split(" ");

  constructor(private http: HttpClient, private cookies: CookieService) { }

  //GET

  /**Metodo que trae todas las modalidades de empleo */
  consultEmploymentModality():Observable<any>{
    return this.http.get<any>(`${environment.restPrefix}/catalog/modality`, {observe: 'response'})
  }

  /**Metodo que trae todas las jornadas laborales */
  consultWorkDay():Observable<any>{
    return this.http.get<any>(`${environment.restPrefix}/catalog/work-day`, {observe: 'response'})
  }

  /**Metodo que trae todos los tipos de contratos */
  consultContract():Observable<any>{
    return this.http.get<Contracts>(`${environment.restPrefix}/catalog/type-contract?code=MX`)
  }

  /**Metodo que trae todos los sectores */
  consultSector():Observable<any>{
    return this.http.get<Sectores>(`${environment.restPrefix}/catalog/sector`, {observe: 'response'})
  }

  /**Metodo que trae todos los giros o industrias */
  consultGiro(idSector: number):Observable<any>{
    return this.http.get<Sectores>(`${environment.restPrefix}/catalog/industry?sectorId=${idSector}`)
  }

    /**Metodo que trae todos los cargos (tipos de puesto) */
    consultCargo():Observable<any>{
      return this.http.get<Cargos>(`${environment.restPrefix}/catalog/type-position`)
    }

    /**Metodo que trae todos los paises*/
    consultCountryu():Observable<any>{
      return this.http.get<Country>(`${environment.restPrefix}/catalog/country`)
    }

    /**Metodo que trae todos los estados del pais */
  consultState(idPais: string):Observable<any>{
    return this.http.get<Estados>(`${environment.restPrefix}/catalog/state?countryCode=${idPais}`)
  }

  /**Metodo que trae todos los municipios de los estados */
  consultMunicipio(idPais:string, idEstado: string):Observable<any>{
    return this.http.get<Municipios>(`${environment.restPrefix}/catalog/city?countryCode=${idPais}&stateCode=${idEstado}`)
  }

  /**Metodo que trae todos los niveles de estudio*/
  consultNivelEstudios():Observable<any>{
    return this.http.get<NivelEstudios>(`${environment.restPrefix}/catalog/education-level?code=mx`)
  }

  /**Metodo que trae todos los estatus acaemicos*/
  consultEstatusAcademicos():Observable<any>{
    return this.http.get<EstatusAcademico>(`${environment.restPrefix}/catalog/education-status`)
  }

  /**Metodo que trae todos las areas de especialidades*/
  consultAreasEspecialidades():Observable<any>{
    return this.http.get(`${environment.restPrefix}/catalog/area`)
  }

  /**Metodo que trae todas las especialidades por areas*/
  consultEspecialidadesPorArea(idArea: string):Observable<any>{
    return this.http.get(`${environment.restPrefix}/catalog/speciality?areaId=${idArea}`)
  }

  /**Metodo que trae todos los idiomas*/
  consultIdiomas():Observable<any>{
    return this.http.get(`${environment.restPrefix}/catalog/language`,{observe: 'response'})
  }

  /**Metodo que consulta el total de vacantes (activas, en pausa, borradores) */
  consultaVacantes():Observable<any>{
    return this.http.get<any>(`${environment.restPrefix}/administration-vacant/total-vacant`)
  }

  //**Metodo para obtener las vacantes por status */
  getVacantForStatus(status: string):Observable<any>{
    return this.http.get<any>(`${environment.restPrefix}/search-vacant/administration?sortDirection=ASC&status=${status}&pageNumber=0&pageSize=10`)
  }

  //**Metodo para obtener las vacantes por status */
  getNationality():Observable<any>{
    return this.http.get<EstatusAcademico>(`${environment.restPrefix}/catalog/nationality`)
  }

  //POST
  /*Metodo para validar que el correo exista*/
  validateMail(mail: string): Observable<any>{

    const objValEmail = {
      'email': mail
    }
    
    return this.http.post(`${environment.restPrefix}/login`, objValEmail,  {observe: 'response'})
  }

  /**Metodo para recuperar contraseña */
  recoverPassword(mail:string): Observable<any>{
    return this.http.post(`${environment.restPrefix}/pass/petition-restore-pass?email=${mail}`, null, {observe: 'response',responseType: 'text'})
  }

  /**Metodo para recuperar contraseña cuando el usuario esta bloqueado */
  recoverPasswordAttempsts(passWord:string): Observable<any>{
    return this.http.post(`${environment.restPrefix}/pass/attempts?password=${passWord}`, null, {observe: 'response'})
  }

  /**Metodo para reestablecer contraseña */
  resetPassword(password:string, keyToken:string): Observable<any>{
    return this.http.post(`${environment.restPrefix}/pass/restore?password=${password}`, null, {observe: 'response'})
  }

  //**Metodo que registra un reclutador */
  setInfoRecruiter(name:string,lastName:string,monLastName:string, contactPhone:string,email:string,nameBusiness:string,selectTamanio:String): Observable<any>{
    const objRecruiter = {
      "name" : name,
      "lastName" : lastName,
      "secondLastName" : monLastName,
      "phone" : contactPhone,
      "email" : email,
      "acceptTerms": true,
      "userRol": "RECRUITER"
    }
    let body = objRecruiter;
    return this.http.post<RegistryRecluitreResponse>(`${environment.restPrefix}/registry?nameCompany=${nameBusiness}&numCompany=${selectTamanio}`, body,{observe: 'response'})
  }

  registerCandidate(email: string) {
    const objRecruiter = {
      "email" : email,
      "userRol": "CANDIDATE"
    }

    let body = objRecruiter;
    return this.http.post<RegistryRecluitreResponse>(`${environment.restPrefix}/registry`, body,{observe: 'response'})
  }

  /**Metodo para activar la cuenta nueva de un reclutador */
  activateAccount(webinar:boolean,password:string,keyToken:string,): Observable<any>{
    //this.headers = this.headers.set('Authorization',`Bearer ${keyToken}`)
    const urlCreatPass = `${environment.restPrefix}/registry/create-pass?password=${password}`;
    return this.http.put<InfoRecuitre>(urlCreatPass, null)
  }

  /**Metodo para realizar el logn del reclutador */
  loginRecuitre(email: string, password: string): Observable<any>{
    const objLoginRecruiter = {
      "email" : email,
      "password" : password,
      "userRol": "RECRUITER"
    }
    return this.http.post(`${environment.restPrefix}/login`, objLoginRecruiter, {observe: 'response'})
  }

  loginCandidate(email: string, password: string): Observable<any> {
    const objLoginCandidate = {
      "email" : email,
      "password" : password,
      "userRol": "CANDIDATE"
    }
    return this.http.post(`${environment.restPrefix}/login`, objLoginCandidate, {observe: 'response'})
  }

  logOutRecuitre = (token:string) => {
    return this.http.get(`${environment.restPrefix}/login/logout?notificationToken=${token}`)
  } 
  /**Metodo para guardar el paso 1 de crear vacante */
creatVacantP1(paso1: any): Observable<any>{
  //**obtenemos la fecha del dia */
  let fechaHoy = formatDate(new Date(), 'yyyy-MM-dd', 'en_US')

  const objPaso1 = {
    "name": paso1.nombreVacante,
    "titlePosition": paso1.tituloPuesto,
    "numbersVacants": paso1.numeroVacantesDisponibles,
    "confidential": false,
    "typePositionId": paso1.tipoPuesto,
    "peopleCharge": paso1.numPersonasAcargo,
    "description": paso1.editorEnriquecido,
    "draftName": null,
    "status" : "ACTIVA",
    "steps":1,
    "client":{
      "companyId": null,
      "employees": paso1.numeroEmpleados,
      "industryId":paso1.giro,
      "name": paso1.nombreCliente,
      "sectorId":paso1.sector,
      "typeCompany":paso1.tipoEmpresa
    }
}

let body = objPaso1;
  return this.http.post(`${environment.restPrefix}/vacant`, body, {observe: 'response'})
}

createPosition = (position: string) => {
  let body = { "position": position };
  return this.http.post(`${environment.restPrefix}/catalog/position`, body, {observe: 'response'});
}

/**Metodo para guardar el paso 2 de crear vacante */
creatVacantP2(paso2: any, vacantId: string, isSalaryExactly: boolean): Observable<any>{
  console.log('Servicio:');
  console.log(vacantId);
  console.log(paso2);
  const objPaso2 = [
    // {
    //   "op":"replace",
    //   "path":"/salaryExactly",
    //   "value":paso2.sueldoMaximo
    // },
    {
      "op":"replace",
      "path":"/salaryShow",
      "value": true
    },
    {
      "op":"replace",
      "path":"/benefits",
      "value":paso2.editorEnriquecido
    },
    {
      "op":"replace",
      "path":"/country",
      "value":paso2.pais.iso2
    },
    {
      "op":"replace",
      "path":"/state",
      "value":paso2.estado.id
    },
    {
      "op":"replace",
      "path":"/town",
      "value":paso2.municipio.nombre
    },
    {
      "op":"replace",
      "path":"/modality",
      "value":paso2.modalidadEmpleo
    },
    {
      "op":"replace",
      "path":"/workingDay",
      "value":paso2.jornadaLaboral.id
    },
    {
      "op":"replace",
      "path":"/schedule",
      "value":paso2.horarioLaboral
    },
    {
      "op":"replace",
      "path":"/allNationality",
      "value":false
    },
    {
      "op":"replace",
      "path":"/nationalityId",
      "value":paso2.nacionalidad.id
    },
    {
      "op":"replace",
      "path":"/contractId",
      "value":paso2.tipoContrato.id
    },
    {
      "op":"replace",
      "path":"/steps",
      "value":2
    }
 ]

 if(isSalaryExactly) {
   objPaso2.push({
    "op":"replace",
    "path":"/salaryExactly",
    "value":paso2.sueldoMaximo
  })
 }

 else {
   objPaso2.push({
    "op":"replace",
    "path":"/salaryMaximum",
    "value":paso2.sueldoMaximo
 },
 {
    "op":"replace",
    "path":"/salaryMinimum",
    "value":paso2.sueldoMinimo
 },)
 }

 let body = objPaso2;

 console.log('PASO 2: ');
 console.log(body);
 return this.http.patch(`${environment.restPrefix}/vacant/${vacantId}`, body, {observe: 'response'})

}

/** Paso 3 para crear vacante  */
creatVacantP3(steps: any, vacantId: string): Observable<any>{
  let objPaso3 = {
    "idEducation": "402881cf79c889e50179c88fc5850003",
    "idStatusEducation": steps.paso3.estatusAcademico.estatus,
    "levelEducationExclud" : steps.paso3.nivelEstudios.excluyente,
    "statusEducationExclud" : steps.paso3.estatusAcademico.excluyente,
    "academicTitle": steps.paso3.academicTitle,
    "institution": [
      {
        "idInstitution": "402880de796fcc8f01796fe660e50005"
      }
    ],
    "area": steps.paso3.experienciaLaboral.area,
    "speciality": steps.paso3.experienciaLaboral.especialidades,
    "hardSkill": steps.paso3.habilidadesTecnicas.habilidad,
    "softSkill": steps.paso3.habilidadesSociales.habilidad,
    "language": steps.paso3.idiomas,
    "questions": steps.paso3.questions
    };
  console.log('Servicio paso 3');
  console.log(objPaso3);

  let body = objPaso3;
  return this.http.post(`${environment.restPrefix}/vacant/step3/${vacantId}`, body, {observe: 'response'});
}

  creatVacantP4(vacantId: string): Observable<any>{
    console.log('Genero el paso 4 dentro del servicio');
    const objPaso4 =
      [
        {
          "op":"replace",
          "path":"/steps",
          "value":4
        }
      ]
    console.log('PASO 4');
    let body = objPaso4;
    console.log(body);
    return this.http.patch(`${environment.restPrefix}/vacant/${vacantId}`, body, {observe: 'response'})
  }

  createVacantP5(vacantId: string): Observable<any> {
    const objPaso5 =
      [
        {
          "op":"replace",
          "path":"/steps",
          "value":5
        }
      ]
    console.log('PASO 5');
    let body = objPaso5;
    console.log(body);
    return this.http.patch(`${environment.restPrefix}/vacant/${vacantId}`, body, {observe: 'response'})
  }

  createVacantP6(vacantId: string): Observable<any> {
    const objPaso5 =
      [
        {
          "op":"replace",
          "path":"/status",
          "value":"ACTIVA"
        },
        {
          "op":"replace",
          "path":"/steps",
          "value":6
        }
      ]
    console.log('PASO 6');
    let body = objPaso5;
    console.log(body);
    return this.http.patch(`${environment.restPrefix}/vacant/${vacantId}`, body, {observe: 'response'})
  }



  //PUT

  //DELETE

  //Sesion cooke user
  setUser(user: string) {
    this.cookies.set("user", user, undefined, '/');
  }
  getUser() {
    return this.cookies.get("user").replace(/\\\"/g, '"').replace('"{', "{").replace('}"', "}");
  }

  setToken(token: string) {
    this.cookies.set("token", token, undefined, '/');
  }
  getToken() {
    return this.cookies.get("token");
  }

  setRecruiterId(recruiterId: string){
    this.cookies.set("recruiterId", recruiterId, undefined, '/')
  }

  getRecruiterId(){
    return this.cookies.get("recruiterId")
  }

  setEmail(email: string){
    this.cookies.set("email", email, undefined, '/')
  }

  getEmail(){
    return this.cookies.get("email")
  }

  setName(name: string){
    this.cookies.set("name", name, undefined, '/')
  }

  getName(){
    return this.cookies.get("name")
  }

  setLastName(lastName: string){
    this.cookies.set("lastName", lastName, undefined, '/')
  }

  getLastName(){
    return this.cookies.get("lastName")
  }

  setSecondLastName(secondLastName: string){
    this.cookies.set("secondLastName", secondLastName, undefined, '/')
  }

  getSecondLastName(){
    return this.cookies.get("secondLastName")
  }

  setPhotoProfile(photo: string){
    this.cookies.set("photo", photo, undefined, '/')
  }

  getPhotoProfile(){
    return this.cookies.get("photo")
  }

  setVacantId(vacantId: string){
    this.cookies.set("vacantId", vacantId, undefined, '/')
  }

  getVacantId(){
    return this.cookies.get("vacantId")
  }

  setVacantActive(vacantActive: string){
    this.cookies.set("vacantActive", vacantActive, undefined, '/')
  }

  getVacantActive(){
    return this.cookies.get("vacantActive")
  }

  setVacantPause(vacantPause: string){
    this.cookies.set("vacantPause", vacantPause, undefined, '/')
  }

  getVacantPause(){
    return this.cookies.get("vacantPause")
  }

  setVacantDrafts(vacantDrafts: string){
    this.cookies.set("vacantDrafts", vacantDrafts, undefined, '/')
  }
  
  getVacantDrafts(){
    return this.cookies.get("vacantDrafts")
  }
  
  setVacantClosed(vacantClosed: string){
    this.cookies.set('vacantClosed', vacantClosed, undefined, '/');
  }

  getVacantClosed(){
    return this.cookies.get('vacantClosed');
  }

  setAttempts(attempst: boolean){
    this.attempst = attempst
  }

  getAttempts(){
    return this.attempst;
  }

  setPass(pass: string){
    this.pass = pass
  }

  getPass(){
    return this.pass;
  }

  verifyEmailCandidate(token: string) {
    return this.http.get(`${environment.restPrefix}/registry/verify/email?token=${token}`);
  }

}



//INTERFACES PARA LAS RESPUESTAS DE CADA ENDPOINT
interface ValidaEmailPass {
  message: string;
  debugInfo: string;
  code: number;
  isCustomCode: boolean;
  data: {
    nombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    foto: string;
    rol: string;
    token: string;
  }
}

interface RecoverPassResponse {
  message: string;
  debugInfo: string;
  code: number;
  isCustomCode: boolean;
}

interface RegistryRecluitreResponse {
  recruiterId: string;
  isAdministrator: string;
  status: string;
  position: string;
  user:{
    userId: string;
    name: string;
    lastName: string;
    secondLastName: string;
    email: string;
    password: string;
    gender: string;
    photo: string;
    phone: string;
    attempts: number;
    userRol: string;
    acceptPrivacy: boolean;
    acceptTerms: boolean;
    acceptNewsletter: boolean;
    createDate: string;
  }
  company: {
    companyId: string;
    company: string;
    companySize: string;
    clients: string;
  }
}

interface SizeBusinessResponse {
  message: string;
  debugInfo: string;
  code: number;
  isCustomCode: boolean;
  data: {
    idTamano: number;
    tamano: string;
    }
}

interface InfoRecuitre {
  body: {
    code: number,
    data: {
      apellidoMaterno: string,
      apellidoPaterno: string,
      foto: string,
      nombre: string,
      rol: string,
      token: string
    },
    debugInfo: string,
    isCustomCode: boolean,
    message: string
  },
  statusCode: string,
  statusCodeValue: number
}

interface Modalitys {
  message: string;
  debugInfo: string;
  code: number;
  isCustomCode: boolean;
  data: {
    idModalidad: number;
    modalidad: string;
    }
}

interface WorkDays {
  message: string;
  debugInfo: string;
  code: number;
  isCustomCode: boolean;
  data: {
    idJornadaLaboral: number;
    jornadaLaboral: string;
    }
}

interface Contracts {
  message: string;
  debugInfo: string;
  code: number;
  isCustomCode: boolean;
  data: {
    contractId: string;
    name: string;
    codeCountry: string;
    }
}

interface Sectores {
  message: string;
  debugInfo: string;
  code: number;
  isCustomCode: boolean;
  data: {
    sectorId: number;
    englishName: string;
    spanishName: string;
    }
}

interface Cargos {
  message: string;
  debugInfo: string;
  code: number;
  isCustomCode: boolean;
  data: {
    positionTypeId: string;
    spanishName: string;
    englishName: string;
    }
}

interface Country {
  "id": number,
  "name": string,
  "iso3": string,
  "iso2": string,
  "phoneCode": string,
  "capital": string,
  "currency": string,
  "currencySymbol": string,
  "tld": string,
  "nameNative": string,
  "region": string,
  "subregion": string,
  "timezones": string,
  "translations": string,
  "latitude": number,
  "longitude": number
}

interface Estados {
  message: string;
  debugInfo: string;
  code: number;
  isCustomCode: boolean;
  data: {
    idEstado: number;
    nombre: string;
    idPais: string;
    }
}

interface Municipios {
  message: string;
  debugInfo: string;
  code: number;
  isCustomCode: boolean;
  data: {
    idMunicipio: number;
    nombre: string;
    }
}

interface NivelEstudios {
  message: string;
  debugInfo: string;
  code: number;
  isCustomCode: boolean;
  data: {
    idNivelEstudio: number;
    nombre: string;
    }
}

interface EstatusAcademico {
  message: string;
  debugInfo: string;
  code: number;
  isCustomCode: boolean;
  data: {
    idEstatusAcademico: number;
    nombre: string;
    }
}

interface Areas {
  message: string;
  debugInfo: string;
  code: number;
  isCustomCode: boolean;
  data: {
    idArea: number;
    nombre: string;
    }
}

interface Idiomas {
  message: string;
  debugInfo: string;
  code: number;
  isCustomCode: boolean;
  data: {
    idIdioma: number;
    nombre: string;
    }
}
