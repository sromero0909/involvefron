import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class NotificationsService {

    constructor(private http: HttpClient) { }

    getListNotifications = () => {
        return this.http.get(`${environment.restPrefix}/notification/recruiter`)
    }

    updateNotifications = (body:any) => {
        return this.http.put(`${environment.restPrefix}/notification/recruiter`,body)
    }
}
