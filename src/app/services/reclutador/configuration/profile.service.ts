import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from '../../models/user.model';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {
    private idRecruiterInvited: string = '';
    private urlPhoto: string = '';
    private nameCompany: string = '';

    constructor(private http: HttpClient) { }

    setIdRecruiterInvited = (id: string) => {
        this.idRecruiterInvited = id
    }

    getIdReruiterInvited = () => {
        return this.idRecruiterInvited
    }

    setUrlPhotoInvited = (id: string) => {
        this.urlPhoto = id
    }

    getUrlPhotoyInvited = () => {
        return this.urlPhoto
    }
    setNameCompayInvited = (id: string) => {
        this.nameCompany = id
    }

    getNameCompayInvited = () => {
        return this.nameCompany
    }

    getRecruiter = (idRecruiter: string) => {
        return this.http.get(`${environment.restPrefix}/recruiter/${idRecruiter}`)
    }

    updataRecruiter = (body: any) => {
        return this.http.put(`${environment.restPrefix}/recruiter`, body, { observe: 'response' })
    }

    deletePhoto = () => {
        return this.http.delete(`${environment.restPrefix}/client/delete-file?typeFile=URL_PHOTO`)
    }

    upLoadPhoto = (type: string, file: any) => {
        return this.http.post(`${environment.restPrefix}/client/uploadFile?typeFile=${type}`, file)
    }

    verifyPassWord = (pass: string) => {
        return this.http.post(`${environment.restPrefix}/login/verify-password?password=${pass}`, null)
    }

    changeEmail = (mail: string) => {
        return this.http.put(`${environment.restPrefix}/pass/change-email?newEmail=${mail}`, null, { responseType: 'text' as 'json' })
    }

    changePassWord = (pass: string) => {
        return this.http.put(`${environment.restPrefix}/login/change-password?newPassword=${pass}`, null)
    }

    updateRecruiterPatch = (body: any) => {
        return this.http.patch(`${environment.restPrefix}/recruiter`, body,{observe:'response'})
    }
}