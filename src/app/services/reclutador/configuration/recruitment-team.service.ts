import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from '../../models/user.model';

@Injectable({
    providedIn: 'root'
})
export class RecruiterTeamService {
    private user!: User

    constructor(private http: HttpClient) { }

    getRecruiters = (params: any) => {
        return this.http.get(`${environment.restPrefix}/recruitment-team?pageNumber=${params.pageNumber}&pageSize=${params.pageSize}${params.other}`)
    }
    updataCompany = (idCompany: string, body: any) => {
        return this.http.put(`${environment.restPrefix}/company/${idCompany}`, body)
    }

    changeRol = (recruiterId: string) => {
        return this.http.put(`${environment.restPrefix}/recruitment-team/rol?recruiterId=${recruiterId}`, null,{observe:'response', responseType:'text'})
    }

    deleteRecruiter = (oldRecruiterId: string, newRecruiter: string) => {
        return this.http.delete(`${environment.restPrefix}/recruitment-team?oldRecruiterId=${oldRecruiterId}${newRecruiter}`, { responseType: 'text' })
    }
    resendInvitation = (email:String) => {
        return this.http.post(`${environment.restPrefix}/recruitment-team/invitation?recruiterId=${email}`,null,{ observe: 'response', responseType: 'text' });
    }

    sentInvitation = (arrayEmail:any) => {
        return this.http.post(`${environment.restPrefix}/recruitment-team/invitation`,arrayEmail, 
        { observe: 'response', responseType: 'text' });
    }
}