import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../../auth.service';

@Injectable({
    providedIn: 'root'
})
export class SuscriptionService {

    private currentCard = '';
    private idSuscripcion = '';
    private editPlan = false;
    private reactivation = false;
    private executeModal = false;
    private target: any;
    private lastCompletedStep = 0;
    isSubcriptionActive = true;

    constructor(private http: HttpClient,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private authService: AuthService) { }

    /**
     * Compares the page step with the user saved step. If the user's is higher,
     * redirects the user to the corresponding screen
     * @param pageStep page step
     */
    validateCurrentStatus(pageStep: number): void {
        if (pageStep === null){
            this.redirectToCorrectScreen(null);
        }
        else if (pageStep <= this.lastCompletedStep){
            this.redirectToCorrectScreen(this.lastCompletedStep);
        }
    }

    /**
     * recover status
     * @returns last completed status
     */
    getLastCompletedStep(): number {
        return this.lastCompletedStep;
    }

    /**
     * Setter
     * @param lastCompletedStep new completed status
     * @returns last completed status
     */
    setlastCompletedStep(lastCompletedStep: number, isLogout: boolean = false): number{
        if (lastCompletedStep > this.lastCompletedStep ||
            isLogout){
            this.lastCompletedStep = lastCompletedStep;
        }
        return this.getLastCompletedStep();
    }


    redirectToCorrectScreen = (step: number | null, register = 'true') => {
        switch (step) {
            case 0:
                this.router.navigateByUrl('/terms-and-conditions/' + register);
                break;
            case 1:
                this.router.navigateByUrl('/register-guest-recruiter/' + register);
                break;
            case 2:
                this.router.navigateByUrl('/select-your-plan/' + register);
                break;
            case 3:
                this.router.navigateByUrl('/start-plan/' + register);
                break;
            case 4:
                this.router.navigateByUrl('/standby-screen/false');
                break;
            case 5:
                this.router.navigateByUrl('/company-data');
                break;
            case 6:
                this.router.navigateByUrl('/invite-your-team');
                break;
            case 7:
            case 8:
                this.router.navigateByUrl('/dashboard/vacantes/activas');
                break;
            default:
                this.router.navigateByUrl('/login');
                break;

        }
    }
    getCurrentCard = () => {
        return this.currentCard;
    }

    setCurrentCard = (currentCard: string) => {
        this.currentCard = currentCard;
    }

    getExecutemodal = () => {
        return this.executeModal;
    }

    setExecutemodal = (executeModal: boolean) => {
        this.executeModal = executeModal;
    }

    getIdSuscripcion = () => {
        return this.idSuscripcion;
    }

    setIdSuscripcion = (idSuscripcion: string) => {
        this.idSuscripcion = idSuscripcion;
    }

    getEditPlan = () => {
        return this.editPlan;
    }

    setEditPlan = (editPlan: boolean) => {
        this.editPlan = editPlan;
    }

    getTarget = () => {
        return this.target;
    }

    setTarget = (target: any) => {
        this.target = target;
    }



    getReactivation = () => {
        return this.reactivation;
    }

    setReactivation = (reactivation: boolean) => {
        this.reactivation = reactivation;
    }



    getSuscription = (params: any) => {
        return this.http.get(`${environment.restPrefixPay}/suscription?dateFin=${params.dateFin}&dateIni=${params.dateIni}&idClient=${params.idClient}&limit=100&offset=0`, { observe: 'response' });
    }

    getCards = (params: any) => {
        return this.http.get(`${environment.restPrefixPay}/card?clientId=${params.idClient}&dateFin=${params.dateFin}&dateIni=${params.dateIni}&limit=100&offset=0`, { observe: 'response' });
    }

    getPlanById = (idPlan: string) => {
        return this.http.get(`${environment.restPrefixPay}/plan/${idPlan}`, { observe: 'response' });
    }

    deleteCard = (cardId: string, clientId: string) => {
        return this.http.delete(`${environment.restPrefixPay}/card?cardId=${cardId}&clientId=${clientId}`, { observe: 'response', responseType: 'text' });
    }


    createSuscriptionByCard = (params: any) => {
        return this.http.post(`${environment.restPrefixPay}/pay/card?cardId=${params.cardId}&clientId=${params.clientId}&planId=${params.planId}&url=${ encodeURIComponent(params.url)}&deviceSessionId=${params.deviceSessionId}&cvv=${params.cvv}`, null, { responseType: 'text', observe: 'response'});
    }


    deleteSuscription = (params: any, reason: string) => {
        return this.http.delete(`${environment.restPrefixPay}/suscription?idClient=${params.idClient}&idSuscription=${params.idSuscription}&reasonDelete=${reason}`, { observe: 'response', responseType: 'text' });
    }


    getAmountByPlan = (query: string) => {
        return this.http.get(`${environment.restPrefixPay}/configuration/${query}`, { observe: 'response' });
    }

    getPlanSuscriptiondeleted = (idUser: string) => {
        return this.http.get(`${environment.restPrefixPay}/suscription/userdelete?idClient=${idUser}`, { observe: 'response' });
    }

    increaseFailedCardAttempts = (userId: string): Observable<any> => {
        return this.http.get(`${environment.restPrefixPay}/card/attempsts?userId=${userId}`, { responseType: 'text' });
    }
}
