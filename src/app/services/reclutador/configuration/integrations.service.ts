import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/services/auth.service';

@Injectable({
    providedIn: 'root'
})
export class IntegrationsService {
    //header:any;

    constructor(private http: HttpClient,
        private auth: AuthService) { }

    getAuth = (idRec:string,email:string,pass:string,bag:string) =>{
        return this.http.get(`${environment.restPrefix}/multipost/find?id=${idRec}&email=${email}&password=${pass}&bag=${bag}`);
    }

    getActiveBags = (idUser:any) => {
        return this.http.get(`${environment.restPrefix}/multipost/findBagActive?id=${idUser}`);
    }

    updatePassBag = (userBag:any) => {
        return this.http.put(`${environment.restPrefix}/multipost/updatepassbag`, userBag, {observe:'response'} || {});
    }

    saveVacancyToBag = (data:any, id:string) => {
      return this.http.post(`${environment.restPrefix}/multipost/save?id=${id}`, data, {observe:'response'} || {});
  }
}
