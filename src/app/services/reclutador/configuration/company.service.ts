import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from '../../models/user.model';

@Injectable({
    providedIn: 'root'
})
export class CompanyService {
    private user!: User

    constructor(private http: HttpClient) { }

    getCompany = (idCompany:string) =>{
        return this.http.get(`${environment.restPrefix}/company/${idCompany}`)
    }

    createCompany = (body:any) => {
        return this.http.post(`${environment.restPrefix}/company`, body,{observe:'response'})
    }

    updataCompany = (idCompany: string, body: any) => {
        return this.http.put(`${environment.restPrefix}/company/${idCompany}`, body)
    }

    upLoadPhotoCompany = (file: any,type:string) => {
       return this.http.request('post',`${environment.restPrefix}/client/bussines-logo?type=${type}`,{body: file})  
    }

    deleteCompany = (type:string) => {
        return this.http.delete(`${environment.restPrefix}/client/bussines-logo?type=${type}`)
    }
}