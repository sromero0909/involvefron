import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';3
import * as moment from 'moment';
import 'moment/locale/es';

import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';


@Injectable({
  providedIn: 'root'
})
export class SuscriptionCanceledService {

  private dateExpiration: string = '';
  private dateCurrency: string = '';
  constructor(private http: HttpClient, private cookies: CookieService) { }


  setDateExpiration(vacantDrafts: string) {
    if (vacantDrafts !== null){
      this.cookies.set("dateExpiration", vacantDrafts, undefined, '/')
    }
    else{
      this.cookies.set('dateExpiration', moment().format('YYYY-MM-DD')), undefined, '/';
    }
  }

  getDateExpiration() {
    return this.cookies.get("dateExpiration")
  }

  setCurrentDate(dateCurrency: string) {
    this.cookies.set("dateCurrency", dateCurrency, undefined, '/')
  }

  getCurrentDate() {
    return this.cookies.get("dateCurrency")
  }

  getCurrentDateService = () => {
    return this.http.get(`${environment.restPrefixPay}/suscription/date`, { observe: 'response', responseType: 'text' })
  }


  getSummary = () => {
    const summary = this.cookies.get("summary");
    return (summary !== '' ? JSON.parse(summary) : null);
  }

  setSummary = (summary: any) => {
    this.cookies.set("summary", summary, undefined, '/')
  }

  getIdPlan = () => {
    return this.cookies.get("idPlan");
}

setIdPlan = (id:string) => {
  this.cookies.set("idPlan", id, undefined, '/')
}
}