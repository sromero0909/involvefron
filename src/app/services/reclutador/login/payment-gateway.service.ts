import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { SuscriptionCanceledService } from './subscription-canceled.service';


@Injectable({
    providedIn: 'root'
})
export class PaymentGatewayService {

    private summary: any;
    private idPlan: string = '';
    private summary$ = new Subject<any[]>();

    constructor(private http: HttpClient,private _serviceGlobal:SuscriptionCanceledService) { }

    

     

    getSummary$(): Observable<any> {
        return this.summary$.asObservable();
    }

    setSummary$(candidate: any) {
        this.summary = candidate;
        this._serviceGlobal.setSummary(JSON.stringify(candidate));
        this.summary$.next(this.summary);
    }


    getPlan = (params: any) => {
        return this.http.get(`${environment.restPrefixPay}/plan?dateFin=${params.dateFin}&dateIni=${params.dateIni}&limit=100&offset=0`,{observe:'response'})
    }

    getDiscountCode = (code:string) => {
        return this.http.get(`${environment.restPrefixPay}/discounts/${encodeURIComponent(code)}`,{observe:'response'})
    }
    
    getKeysOpenPay = (code:string) => {
        return this.http.get(`${environment.restPrefixPay}/configuration/${code}`,{observe:'response'})
    }

}