import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root'
})
export class RegisterService {

    private summary: any;
    private idPlan: string = '';
    private statusRegistry$ = new Subject<any>();

    constructor(private http: HttpClient) { }

    setStatusRegistry$(recruiter: any) {
        this.statusRegistry$.next(recruiter);
    }

    getStatusRegistry$(): Observable<any> {
        return this.statusRegistry$.asObservable();
    }

    registryMail = (body: any) => {
        return this.http.post(`${environment.restPrefix}/registry`, body, { observe: 'response', responseType: 'text' })
    }

    createPassWord = (passoword: string) => {
        return this.http.put(`${environment.restPrefix}/registry/create-pass?password=${passoword}`, null, { observe: 'response' })
    }

    createUserOpenPay = (body: any) => {
        return this.http.post(`${environment.restPrefixPay}/client`, body, { observe: 'response' })
    }

    resendEmail = () => {
        return this.http.post(`${environment.restPrefix}/verify/email`, null, { observe: 'response' })
    }

}