import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {
  private clientTem: any

  constructor(private http: HttpClient) { }

  setClietTem = (client: any) => {
    this.clientTem = client
  }

  getClientTem = () => {
    return this.clientTem
  }

  getClients = (params: any) => {
    return this.http.get(`${environment.restPrefix}/client/page?pageNumber=${params.pageNumber}&pageSize=${params.pageSize}${params.other}`);
  }

  createNewClient = (params: any) => {
    return this.http.post(`${environment.restPrefix}/client`, params);
  }

  editClient = (params: any) => {
    return this.http.put(`${environment.restPrefix}/client`, params);
  }

  deleteClient = (body: any) => {
    return this.http.request('delete', `${environment.restPrefix}/client`, { body: body, observe: 'response' });
  }

}