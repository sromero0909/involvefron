import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class TalentNetoworkService {

    constructor(private http: HttpClient) { }

    getCandidates = (params: any, body: any) => {
        return this.http.post(`
        ${environment.restPrefix}/talent-network?page=${params.page}&query=${params.query}&size=${params.size}&stateId=${params.stateId}`, body);
    }

    getFavoriteListDetails(favoriteListId: string, params: any, filters: any){
        return this.http.post(`${environment.restPrefix}/list-favorite/content?listFavoriteId=${favoriteListId}&pageNumber=${params.pageNumber}&pageSize=${params.pageSize}&sortBy=candidate&sortDirection=ASC`,
        filters, {
            observe: 'response'
        });
    }

    removeCandidates(favoriteListId: string, candidates: string[]){
        return this.http.request('DELETE', `${environment.restPrefix}/list-favorite/candidate?listId=${favoriteListId}`, {
            body: candidates,
            observe: 'response'
        });
    }

    getStateList() {
        return this.http.get(`
        ${environment.restPrefix}/catalog/state?countryCode=mx`);
    }

}