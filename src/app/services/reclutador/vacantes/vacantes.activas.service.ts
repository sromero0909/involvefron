import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
    providedIn: 'root'
})
export class VacantesActivasService {

    private recruiter$ = new Subject<any[]>();

    constructor(private http: HttpClient,) {

    }

    setRecruiter$(recruiter: any) {
        this.recruiter$.next(recruiter);
    }

    getRecruiter$(): Observable<any[]> {
        return this.recruiter$.asObservable();
    }

    listarVacantesActivas(params:any):Observable<any> {
        return this.http.get(`${environment.restPrefix}${params}`)
    }
    pasusarVacante(vacantId:any,body:any,check:boolean): Observable<any> {
        return this.http.put(`${environment.restPrefix}/vacant/pause?showModal=${check}&vacantId=${vacantId}`,body)
    }

    cerrarVacante(vacantId:any,body:any): Observable<any> {
        return this.http.put(`${environment.restPrefix}/vacant/close?vacantId=${vacantId}`,body)
    }

    getMotivosCerrar():Observable<any>{
        return this.http.get(`${environment.restPrefix}/catalog/reason-close-vacant`)
    }

    getMotivosPausar():Observable<any>{
        return this.http.get(`${environment.restPrefix}/catalog/reason-pause-vacant`)
    }

    updateNotificaciones(idVacante:string):Observable<any> {
        return this.http.put(`${environment.restPrefix}/vacant/notifications?vacantId=${idVacante}`,null,{observe:'response'})
    }

    crearPlantilla (data:any):Observable<any> {
        return this.http.post(`${environment.restPrefix}/vacant/template`,data)

    }

    atuCompletadoReclutador(query:string):Observable<any>{
        return this.http.get(`${environment.restPrefix}/recruiter/autocomplete/${query}`,{observe:'response'})
    }

    agregarReclutador(idVacante:string,data:any):Observable<any>{
        return this.http.post(`${environment.restPrefix}/vacant/step6/${idVacante}`,data)
    }

    catalogoOrderBy():Observable<any>{
        return this.http.get(`${environment.restPrefix}/vacant/order-by`)
    }

    removeRecruiter(idVacant:string,idRecruiter:string):Observable<any>{
        return this.http.delete(`${environment.restPrefix}/vacant/recruiter/${idVacant}/${idRecruiter}`)
    }

    activateVacancy(vacantId:string,body:any): Observable<any>{
        return this.http.patch(`${environment.restPrefix}/vacant/${vacantId}`, body, {observe: 'response'})
    }

    deleteVacancy(vacantId: string, body: any): Observable<any>{
        return this.http.patch(`${environment.restPrefix}/vacant/${vacantId}`, body, {observe: 'response'})
    }

}