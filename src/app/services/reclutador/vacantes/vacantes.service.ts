import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VacantesService {
  private menuSeleccionado:string = "";
  private procesoSeleccion:string = ""
  private nuevaVacante:boolean = false;
  private barraActiva:boolean = false;
  private cabiarSideNav$ = new Subject<boolean>();
  private _barraAcitva$ = new BehaviorSubject<boolean>(true);
  constructor() {
    this.cabiarSideNav$.pipe(
      switchMap(() => this._search()),
    ).subscribe(result => {
      this._barraAcitva$.next(result);
    });
   }

  get barraActiva$() { return this._barraAcitva$.asObservable(); }

  setMenuSeleccionado(menu: string) {
    this.menuSeleccionado = menu;
  }

  getMenuSeleccionado() {
    return this.menuSeleccionado;
  }

  setNuevaVacante(vacante: boolean) {
    this.nuevaVacante = vacante;
  }

  getNuevaVacante() {
    return this.nuevaVacante;
  }

  setProcesoSeleccion(proceso: string) {
    this.procesoSeleccion = proceso;
  }

  getProcesoSeleccion() {
    return this.procesoSeleccion;
  }

  setBarraActiva(barraActiva: boolean) {
    this.cabiarSideNav$.next(barraActiva)
    this.barraActiva = barraActiva;
  }

  getBarraActiva() {
    return this.barraActiva;
  }

  private _search(): Observable<boolean> {

    return of(this.getBarraActiva());
  }
}
