import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SideNavService {
    private countersVacant$ = new Subject<any>();
    private countersVacant: any
    constructor(private http: HttpClient,) {

    }

    getClientes(): Observable<any> {
        return this.http.get<any>(`${environment.restPrefix}/client`, { observe: 'response'});
    }

    getReclutadores(): Observable<any> {
        return this.http.get<any>(`${environment.restPrefix}/recruiter`, { observe: 'response'})
    }

    totalVacancies(): Observable<any> {
        return this.http.get<any>(`${environment.restPrefix}/vacant/total-vacant`);
    }

    upDateCounters(counters: any) {
        this.countersVacant = counters;
        this.countersVacant$.next(this.countersVacant);
      }

      getCountersVacant$(): Observable<any> {
        return this.countersVacant$.asObservable();
      }
}
