import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
    providedIn: 'root'
})
export class VacantesCerradasService {

    constructor(private http: HttpClient,) {

    }

    getVacantForStatus(data:any): Observable<any> {
        return this.http.get(`${environment.restPrefix}/search-vacant?sortDirection=ASC&status=${data.status}&pageNumber=${data.pageNumber}&pageSize=${data.pageSize}`, { responseType: 'text' as 'json' })
    }

    eliminarVacante(body:any): Observable<any>{
        const options = {
            body: body,
          };
        return this.http.request('delete',`${environment.restPrefix}/vacant`,{body: body,observe: 'response'})  
    }
}