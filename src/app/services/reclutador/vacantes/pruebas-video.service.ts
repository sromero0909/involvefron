import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PruebasVideoService {

  private contadorPreguntasVideo = 0;
  constructor() { }


  setContadorPreuntas(contador: number) {
    this.contadorPreguntasVideo = contador;
  }

  getContadorPreuntas() {
    return this.contadorPreguntasVideo;
  }
}