import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class VacancyService {
  constructor(private httpclient: HttpClient) {}
  private vacancyInfo: any;

  /**
   *
   * A centralized method to call all the https requests.
   *
   * @param method Path to the specific method in the service
   * @param data The data to send to the method
   * @param type Type of the http petition
   * @returns An observable of the response to the requested resource
   */
   private callService(method: string, data?: any, type: string = 'POST', options?: any, isPsychometric?: boolean): Observable<any>|null {
    if (type === 'GET') {
      if (isPsychometric === true){
        return this.httpclient.get(`${environment.restPsychometric}${method}`, options || {});
      }
      else{
        return this.httpclient.get(`${environment.restPrefix}${method}`, options || {});
      }
    }
    if (type === 'POST') {
      return this.httpclient.post(`${environment.restPrefix}${method}`, data);
    }
    if (type === 'PATCH') {
      return this.httpclient.patch(`${environment.restPrefix}${method}`, data);
    }
    if (type === 'PUT'){
      return this.httpclient.put(`${environment.restPrefix}${method}`, data, options || {});
    }
    return null;
  }

  /**
   *
   * Used in the first step of the creation process
   *
   * @param vacancyInfo An object with the vacancy data from te first step
   * @returns If the vacancy was created, returns a vacancy object. It returns null if there was a problem.
   */
  createVacancy(vacancyInfo: any): Observable<any>|undefined{
    return this.callService('/vacant', vacancyInfo, 'POST')?.pipe(map((response: any) => {
      if (response.status && response.status >= 400){
        return null;
      }
      else{
        this.vacancyInfo = response;
        if (!environment.production){
          localStorage.setItem('vacancyInfo', JSON.stringify(this.vacancyInfo));
        }
        return response;
      }
    }));
  }

  getPresentationQuestions(): Observable<any> | null{
    return this.callService('/catalog/question-presentation', null, 'GET', { observe: 'response'});
  }

  getPsychometricTests(): Observable<any> | null {
    return this.callService('/tests/psicometrics/catTests', null, 'GET', { observe: 'response'}, true);
  }

  /**
   *
   * Get the server's version of a saved vacancy. If the vacancy exists,
   * it is saved on the service
   *
   * @param vacancyId The id of the interested vacancy
   * @returns The vacancy's info if the id is correct. Null if it is not
   */
  getVacancy(vacancyId: string): Observable<any>|undefined{
    return this.callService(`/vacant/${vacancyId}`, null, 'GET')?.pipe(map((response) => {
      if (response.status && response.status >= 400){
        return null;
      }
      else{
        this.vacancyInfo = response;
        if (!environment.production){
          localStorage.setItem('vacancyInfo', JSON.stringify(this.vacancyInfo));
        }
        return response;
      }
    }));
  }

  saveStep5(vacancyId: string, vacancyInfo: any): Observable<any>|undefined{
    return this.callService(`/vacant/step5/${vacancyId}`, vacancyInfo, 'POST', {observe: 'response'})?.pipe(map((response) => {
      if (response.status && response.status >= 400){
        return null;
      }
      else{
        this.vacancyInfo = response;
        if (!environment.production){
          localStorage.setItem('vacancyInfo', JSON.stringify(this.vacancyInfo));
        }
        return response;
      }
    }));
  }

  uploadVacancyInfo(vacancyInfo: any, completedStep: number): Observable<any>|undefined{
    const operationsToUpdate: any[] = [];
    /**
     * Create a set of operations based on the properties inside the form of the current step
     */
    Object.entries(vacancyInfo).forEach( (el) => {
      operationsToUpdate.push({
        op: 'replace',
        path: `/${el[0]}`,
        value: (typeof el[1] === 'object' ? el[1] : String(el[1]))
      });
    });
    operationsToUpdate.push({
      op: 'replace',
      path: `/steps`,
      value: String(completedStep)
    });

    return this.callService(`/vacant/${this.getCurrentVacancy().vacantId}`, operationsToUpdate, 'PATCH')?.pipe(map( (response) => {
      if (response.status && response.status >= 400){
        console.error(response);
        // TODO: validar errores que puede regresar el servicio
        return null;
      }
      else{
        Object.assign(this.vacancyInfo, response);
        if (!environment.production){
          localStorage.setItem('vacancyInfo', JSON.stringify(this.vacancyInfo));
        }
        return response;
      }
    }));
  }

  getCurrentVacancy(): any {
    if (!environment.production){
      if (localStorage.getItem('vacancyInfo')){
        // this.vacancyInfo = JSON.parse(localStorage.getItem('vacancyInfo') || '');
      }
    }
    return this.vacancyInfo;
  }

  resetCurrentVacancy(): any{
    this.vacancyInfo = null;
  }

  setVacancyInfo(vancy: any): any {
    this.vacancyInfo = vancy;
  }

  saveStep6(vacancyId: string, vacancyInfo: any): Observable<any>|undefined{
    return this.callService(`/vacant/step6/${vacancyId}`, vacancyInfo, 'POST')?.pipe(map((response) => {
      if (response.status && response.status >= 400){
        return null;
      }
      else{
        this.vacancyInfo = response;
        if (!environment.production){
          localStorage.setItem('vacancyInfo', JSON.stringify(this.vacancyInfo));
        }
        return response;
      }
    }));
  }

  publishVacancy(vacantId: string){
    return this.callService(`/vacant/actived?vacantId=${vacantId}`, null, 'PUT', { observe: 'response'});
  }
}
