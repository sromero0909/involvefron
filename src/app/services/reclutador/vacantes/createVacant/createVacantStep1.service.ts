import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CreateVacantStep1Service {


  constructor(private http: HttpClient) { }

  getBenefits = (): Observable<any> => {
    return this.http.get(`${environment.restPrefix}/catalog/benefit`,{observe: 'response'})
  }

  getClientsByQuery = (query:string): Observable<any> => {
    return this.http.get(`${environment.restPrefix}/client/query/${query}`,{observe: 'response'})
  }

  getCatalogModality = (): Observable<any> => {
    return this.http.get(`${environment.restPrefix}/catalog/modality`)
  }

  createVacancy = (params:any) => {
    return this.http.post(`${environment.restPrefix}/vacant`,params)
  }

  getCatalogPosition = (query:string): Observable<any> => {
    return this.http.get(`${environment.restPrefix}/catalog/position?query=${query}`)
  }
  

}
