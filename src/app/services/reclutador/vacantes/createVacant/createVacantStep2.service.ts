import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CreateVacantStep2Service {
  /**
   *
   * A centralized method to call all the https requests.
   *
   * @param method Path to the specific method in the service
   * @param data The data to send to the method
   * @param type Type of the http petition
   * @returns An observable of the response to the requested resource
   */
  private callService(method: string, data?: any, type: string = 'POST'): Observable<any>|null {
    if (type === 'GET') {
      return this.http.get(`${environment.restPrefix}${method}`);
    }
    if (type === 'POST') {
      return this.http.post(`${environment.restPrefix}${method}`, data);
    }
    if (type === 'PATCH') {
      return this.http.patch(`${environment.restPrefix}${method}`, data);
    }
    return null;
  }

  constructor(private http: HttpClient) { }

  /**
   *
   * @param code country code, in format iso2
   * @returns A list of available contract types based on the passed `code` country
   */
  getContractTypes(code: string): Observable<any> | null {
    return this.callService(`/catalog/type-contract?code=${code}`, null, 'GET');
  }

  /**
   *
   * @returns A list of nationalities
   */
  getNationalities(): Observable<any> | undefined {
    return this.callService('/catalog/nationality', null, 'GET')?.pipe(
      map((nationalities: any[]) => {
        return nationalities.sort((a, b) => (a.spanishName > b.spanishName ? 1 : -1));
      })
    );
  }

  /**
   *
   * @returns A list of available positions
   */
  getPositionTypes(): Observable<any> | undefined{
    return this.callService('/catalog/type-position', null, 'GET')?.pipe(
      map((positionTypes: any[]) => {
        return positionTypes.sort((a, b) => (a.spanishName > b.spanishName ? 1 : -1));
      })
    );
  }

  /**
   *
   * @returns A list of different working hours schemes (half-time, full-time, etc)
   */
  getPositionWorkingHours(): Observable<any> | undefined {
    return this.callService('/catalog/work-day', null, 'GET')?.pipe(
      map((positionWorkingHours: any[]) => {
        const options: any[] = [];
        positionWorkingHours.forEach( (el, i) => {
          options.push({
            id: el[0][0],
            spanishName: el[0][1]
          });
        });
        options.sort((a, b) => (a.spanishName > b.spanishName ? 1 : -1));
        return options;
      })
    );
  }
}
