import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CreateVacantStep3 {
  private infoStep3 = {hardSkill:null,idiom:null};

  constructor(private http: HttpClient) { }

  getHardSkill = (query: string): Observable<any> => {
    return this.http.get(`${environment.restPrefix}/catalog/hard-skill?query=${query}`)
  }

  getSoftSkill = (query: string): Observable<any> => {
    return this.http.get(`${environment.restPrefix}/catalog/soft-skill?query=${query}`)
  }

  getInstitution = (query: string): Observable<any> => {
    return this.http.get(`${environment.restPrefix}/catalog/institution?query=${query}`)
  }

  saveStep3 = (idVacant:string, body: any): Observable<any> => {
    return this.http.post(`${environment.restPrefix}/vacant/step3/${idVacant}`,body)
  }

  saveStep4 = (idVacant:string, body: any): Observable<any> => {
    return this.http.post(`${environment.restPrefix}/vacant/step4/${idVacant}`,body)
  }

  setInfoStep3 = (info:any) => {
    this.infoStep3 = info
  }

  getInfoStep3 = () => {
    return this.infoStep3
  }

}
