import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlantillasService {

  constructor( private http: HttpClient) { }

  getListPlantillas = (params: any) => {
    return this.http.get(`${environment.restPrefix}/vacant/admin?pageSize=${params.pageSize}&pageNumber=${params.pageNumber}&sortBy=${params.orderBy}&sortDirection=${params.sortDirection}&status=${params.status}${params.clientFilter}${params.recruiterFilter}${params.textFilter}`);
  }
}
