export interface VacanteStepThree {
    academicTitle:     string;
    area:              AreaElement[];
    education:         Education;
    hardSkill:         Skill[];
    idEducation:       string;
    idStatusEducation: string;
    institution:       Institution[];
    language:          LanguageElement[];
    questions:         Question[];
    softSkill:         Skill[];
    speciality:        Speciality[];
    statusEducation:   StatusEducation;
}

export interface AreaElement {
    area:           PurpleArea;
    areaId:         string;
    exclud:         boolean;
    vacantAreaId:   string;
    vacantId:       string;
    yearExperience: number;
}

export interface PurpleArea {
    areaId:       string;
    englishName:  string;
    spanishName:  string;
    specialties?: PurpleArea[];
    specialtyId?: string;
}

export interface Education {
    codeCountry: string;
    educationId: string;
    name:        string;
}

export interface Skill {
    level:              string;
    skill:              SkillClass;
    skillId:            string;
    vacantHardSkillId?: string;
    vacantId:           string;
    vacantSoftSkillId?: string;
}

export interface SkillClass {
    name:    string;
    skillId: string;
}

export interface Institution {
    idInstitution:       string;
    institute:           Institute;
    vacantId:            string;
    vacantInstitutionId: string;
}

export interface Institute {
    acronym:       string;
    codeCountry:   string;
    idInstitution: string;
    name:          string;
}

export interface LanguageElement {
    language:          LanguageLanguage;
    languageId:        string;
    level:             string;
    vacantId:          string;
    vacanteLanguageId: string;
}

export interface LanguageLanguage {
    english_name: string;
    languageId:   string;
    spanish_name: string;
}

export interface Question {
    exclud:      boolean;
    question:    string;
    questionId:  string;
    type:        string;
    typeAnsewer: boolean;
    vacantId:    string;
}

export interface Speciality {
    exclud:         boolean;
    speciality:     PurpleArea;
    specialtyId:    string;
    vacantId:       string;
    yearExperience: number;
}

export interface StatusEducation {
    idStatus: string;
    name:     string;
}
