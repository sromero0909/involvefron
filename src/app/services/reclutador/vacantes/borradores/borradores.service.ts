import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BorradoresService {

  constructor(private http: HttpClient) { }

  getListBorradores = (params: any) => {
    return this.http.get(`${environment.restPrefix}/vacant/admin?pageSize=${params.pageSize}&pageNumber=${params.pageNumber}${params.sortDirection}${params.orderBy}&status=${params.status}${params.clientFilter}${params.recruiterFilter}${params.textFilter}`);
  }

  eliminarVacante(body:any): Observable<any>{
    const options = {
        body: body,
      };
    return this.http.request('delete',`${environment.restPrefix}/vacant`,{body: body,observe: 'response'})  
}
}
