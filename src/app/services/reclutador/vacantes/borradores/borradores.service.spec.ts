import { TestBed } from '@angular/core/testing';

import { BorradoresService } from './borradores.service';

describe('BorradoresService', () => {
  let service: BorradoresService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BorradoresService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
