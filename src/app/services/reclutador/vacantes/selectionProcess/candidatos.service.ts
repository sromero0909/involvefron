import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { VacantCard } from 'src/app/services/models/cardVacants.model';
@Injectable({
    providedIn: 'root'
})
export class CandidatesService {
    card!: VacantCard
    rowsTemporal: Array<any> = []
    constructor(private http: HttpClient,) {

    }

    setSelectedCard(card: VacantCard) {
        this.card = card
    }

    getSelectedCard(): VacantCard {
        return this.card;
    }

    setRowsTemporal(rows: any) {
        this.rowsTemporal = rows
    }

    getRowsTemporal() {
        return this.rowsTemporal;
    }

    getVacantForStatus(params: any): Observable<any> {
        return this.http.get(`${environment.restPrefix}/selection-process/vacant?pageNumber=${params.pageNumber}&pageSize=${params.pageSize}${params.sortBy}${params.sortDirection}&status=${params.status}&vacantId=${params.vacantId}${params.wordSearch}${params.filterAge}${params.filterSalary}${params.otherFilter}${params.compatibility}${params.filterState}` +
            params.filterURL +
            params.filterGender +
            params.filterLevelEducation +
            params.filterNationality +
            params.filterVideo +
            params.filterInstitution)
    }

    candidateCounter(params: any) {
        return this.http.get(`${environment.restPrefix}/selection-process/countProcess?vacantId=${params}`)
    }

    updateFavorite = (id: any) => {
        return this.http.put(`${environment.restPrefix}/selection-process/favourite?processId=${id}`, null)
    }

    sendReminder = (params: any) => {
        return this.http.post(`${environment.restPrefix}/selection-process/reminder`, params)
    }

    sendInvitation = (id: string, body: any) => {
        return this.http.post(`${environment.restPrefix}/selection-process/invitation?vacantId=${id}`, body, 
        { observe: 'response', responseType: 'text'})
    }

    sendInvitationByCandidatesId = (vacantId: string, candidatesList: string[]) => {
        return this.http.post(`${environment.restPrefix}/list-favorite/invitation?vacantId=${vacantId}`, candidatesList, 
        { observe: 'response'});
    }

    searchVacancy = (text: string) => {
        return this.http.get(`${environment.restPrefix}/vacant/admin?textFilter=${text}`)
    }

    rejectCandidate = (flag: boolean, body: any) => {
        return this.http.post(`${environment.restPrefix}/selection-process/reject?notification=${flag}`, body, {observe: 'response', responseType: 'text'})
    }

    nextStage = (flag: boolean, body: any) => {
        return this.http.post(`${environment.restPrefix}/selection-process/next?notification=${flag}`, body, {observe: 'response', responseType: 'text'});
    }

    firstFilterCatalog = () => {
        return this.http.get(`${environment.restPrefix}/selection-process/filter-select`)
    }

    secondFilterCatalog = () => {
        return this.http.get(`${environment.restPrefix}/selection-process/filter-compatibility`)
    }

    filterRange = (params: any) => {
        return this.http.get(`${environment.restPrefix}/selection-process/statistics?status=${params.status}&vacant=${params.vacantId}`)
    }

    moreFilters = (vacantId: any) => {
        return this.http.get(`${environment.restPrefix}/selection-process/countFilters?vacantId=${vacantId}`)
    }

    getShortList = (params: any) => {
        return this.http.get(`${environment.restPrefix}/short-list?page=${params.page}&size=${params.size}&vacant=${params.vacant}`)
    }

    newShortList = (params: any) => {
        return this.http.post(`${environment.restPrefix}/short-list`, params, {observe: 'response', responseType: 'text'})

    }

    showProfile = (idProcess: any) => {
        return this.http.post(`${environment.restPrefix}/selection-process/profile?selectionProcessId=${idProcess}`, null, {observe: 'response'})

    }

    getProcessById = (id:string) => {
        return this.http.get(`${environment.restPrefix}/selection-process/${id}`,{observe:'response'})
    }
    
}