import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { VacantCard } from 'src/app/services/models/cardVacants.model';
@Injectable({
    providedIn: 'root'
})
export class InterviewService {
    
    constructor(private http: HttpClient,) {

    }

    getText = (id: string) => {
        return this.http.get(`${environment.restPrefix}/result-profile/selection-process?id=${id}`)
    }

    saveText = (params: any) => {
        return this.http.post(`${environment.restPrefix}/result-profile`, params)
    }
    
}