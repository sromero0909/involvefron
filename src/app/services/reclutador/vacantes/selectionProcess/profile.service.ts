import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { VacantCard } from 'src/app/services/models/cardVacants.model';
@Injectable({
    providedIn: 'root'
})
export class ProfileService {
    card!: VacantCard
    candiate:any
    private candidate$ = new Subject<any[]>();
    constructor(private http: HttpClient,) {

    }

    setSelectedCard(card: VacantCard) {
        this.card = card
    }

    getSelectedCard(): VacantCard {
        return this.card;
    }

    // setActiveCandidate = (candiate:any) => {
    //     this.candiate = candiate
    // }
     getActiveCandidate = () => {
         return this.candiate
    }
    
    updateCandidate(candidate: any) {
        this.candiate = candidate;
        this.candidate$.next(this.candiate);
    }

    getCandidate$(): Observable<any[]> {
        return this.candidate$.asObservable();
    }

    getFile = (url:string) => {
        return null;
        // https://izrld3wuvj.execute-api.us-east-1.amazonaws.com/dev/interview-json/2c9f98747bc722b8017bcd0f02930021/2c9f98747bc722b8017bcd0f02930021-entrevista.json
        // return this.http.get("https://izrld3wuvj.execute-api.us-east-1.amazonaws.com/dev/interview-json/2c9f98747bc722b8017bccd172d80014/2c9f98747bc722b8017bccd172d80014-interview.json");
        // return this.http.get(`${environment.restJson}/${url}/${url}-interview.json"`);
        //  2c9f98747bc722b8017bccd172d80014
    }

    getWorkExperience(selectionProcessId: string){
        return this.http.get(`${environment.restPrefix}/selection-process/work-experience?selectionProcessId=${selectionProcessId}`, {
            observe: 'response'
        });
    }

    getSkills(selectionProcessId: string){
        return this.http.get(`${environment.restPrefix}/selection-process/skill?selectionProcessId=${selectionProcessId}`, {
            observe: 'response'
        });
    }

    getSelectionProcess(selectionProcessId: string){
        return this.http.get(`${environment.restPrefix}/selection-process/${selectionProcessId}`, {
            observe: 'response'
        });
    }
}

//https://involveemotiondetection.s3.amazonaws.com/SELECTION_PROCESS/2c9f98747bc722b8017bcd0f02930021/2c9f98747bc722b8017bcd0f02930021-entrevista.json