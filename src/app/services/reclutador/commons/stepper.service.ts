import { Injectable } from '@angular/core';
import { Observable,Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class StepperService {
  private menuSeleccionado = '';
  private clientes$ = new Subject<{}>();
  private clientes = {}
  constructor() { }

  nextStepper(cliente: {}) {
    this.clientes=cliente;
    this.clientes$.next(this.clientes);
  }

  getPantalla$(): Observable<{}> {
    return this.clientes$.asObservable();
  }

}
