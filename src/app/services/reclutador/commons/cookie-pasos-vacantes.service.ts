import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class CookiePasosVacanteService {
  private pasosCrearVacante: any = {paso1: '', paso2: '', paso3: '', paso4: '', paso5: '', paso6: ''};
  private pasosCompletados: any = {paso1: false, paso2: false, paso3: false, paso4: false, paso5: false, paso6: false};
  private editarPlantilla = false;

  private createVacancyStep2Data: any = {
    formPosition: {},
    formWorkingConditions: {}
  };

  constructor() { }


  setPasosCrearVacante(pasosCrearVacante: any) {
    this.pasosCrearVacante = pasosCrearVacante;
  }

  getPasosCrearVacante() {
    return this.pasosCrearVacante;
  }

  setStatusPasosCrearVacante(pasosCompletados: any) {
    this.pasosCompletados = pasosCompletados;
  }

  getStatusPasosCrearVacante() {
    return this.pasosCompletados;
  }

  setEditarPlantilla(bandera: any) {
    this.editarPlantilla = bandera;
  }

  getEditarPlantilla() {
    return this.editarPlantilla;
  }

  /**
   * Saves, locally, the value of the form to be submitted in
   * next steps
   *
   * @param formName Updating form's name
   * @param data Updating form's value
   */
  setStep2Data(formName: string, data: any): void{
    this.createVacancyStep2Data[formName] = data;
    localStorage.setItem(formName, JSON.stringify(data));
  }

  /**
   * Returns the value of a form previously saved
   *
   * @param formName Interested form's name
   *
   * @returns The value of the form if it was already saved. Empty object if not
   */
  getStep2Data(formName: string): any {
    return this.createVacancyStep2Data[formName] || {};
  }
}
