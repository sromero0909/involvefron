import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Location } from '@angular/common';
import { StepperService } from '../../../services/reclutador/commons/stepper.service'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RutasCrearVacanteService {

  constructor(private _location: Location,
    private stepperService: StepperService, 
    private router: Router,
  ) { }


  pantalla(pantalla: string): void {
    let show = {
      action: 'previous',
      go: true,
      pantalla
    }
    this.redirecto(pantalla);
    this.stepperService.nextStepper(show);
  }
  redirecto(valor: string) {
   
    switch (valor) {
      case 'uno':
        this.router.navigateByUrl("dashboard/vacantes/crear/(vacante:descripcion)");
        break;
      case 'dos':
        this.router.navigateByUrl("dashboard/vacantes/crear/(vacante:descripcion//condiciones:laborales)");
        break;
      case 'tres':
        this.router.navigateByUrl("dashboard/vacantes/crear/(vacante:descripcion//condiciones:laborales//requisitos:requisitos)");
        break;
      case 'cuatro':
        this.router.navigateByUrl("dashboard/vacantes/crear/(vacante:descripcion//condiciones:laborales//requisitos:requisitos//pruebas:video)");
        break;
      case 'cinco':
        this.router.navigateByUrl("dashboard/vacantes/crear/(vacante:descripcion//condiciones:laborales//requisitos:requisitos//pruebas:video//equipo:reclutamiento)");
        break;
        case 'vistaprevia':
          this.router.navigateByUrl("dashboard/vacantes/crear/(vacante:descripcion//condiciones:laborales//requisitos:requisitos//pruebas:video//equipo:reclutamiento//vista:previa)");
        break;
        case 'publicar':
          this.router.navigateByUrl("dashboard/vacantes/crear/(vacante:descripcion//condiciones:laborales//requisitos:requisitos//pruebas:video//equipo:reclutamiento)");
        break;
      default:
        break;
    }

  }
}
