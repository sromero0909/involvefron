import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  constructor(private http:HttpClient) { }

  getClient = (idClient: string) => {
    return this.http.get(`${environment.restPrefix}/client/${idClient}`);
  }
}