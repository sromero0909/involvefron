import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  constructor( private http: HttpClient) { }

  getPositionOcurrences = (termino: string): Observable<any> => {
    return this.http.get(`${environment.restPrefix}/catalog/position?query=${termino}`);
  }

  getInstitutionOcurrences = (termino: string) : Observable<any> => {
    return this.http.get(`${environment.restPrefix}/catalog/institution?query=${termino}`);
  }

  getClientesOcurrences = (termino: string): Observable<any> => {
    return this.http.get(`${environment.restPrefix}/client/query/${termino}`);
  }

  getHardSkills = (termino: string): Observable<any> => {
    return this.http.get(`${environment.restPrefix}/catalog/hard-skill?query=${termino}`);
  }

  getSoftSkills = (termino: string): Observable<any> => {
    return this.http.get(`${environment.restPrefix}/catalog/soft-skill?query=${termino}`);
  }
}
