import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  constructor(private http: HttpClient) { }


  createVideoQuestion = (question: any) => {
    let body = question;
    //console.log('QUESTION:');
    //console.log(question);
    return this.http.post(`${environment.restPrefix}/questions`, body, {observe: 'response'})
  }
}
