import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  get = (url: string) => {
    return this.http.get(`${environment.restPrefix}${url}`);
  }

  post = (body: any, url: string) => {
    return this.http.post(`${environment.restPrefix}${url}`, body, { observe: 'response' });
  }

  postId = (url: string) => {
    return this.http.post(`${environment.restPrefix}${url}`, { observe: 'response' });
  }

  put = (url: string) => {
    return this.http.put(`${environment.restPrefix}${url}`, { observe: 'response' });
  }

  delete = (url: string) => {
    return this.http.delete(`${environment.restPrefix}${url}`, { observe: 'response' });
  }

  patch = (body: any, url: string) => {
    return this.http.patch(`${environment.restPrefix}${url}`, body, { observe: 'response' });
  }

}
