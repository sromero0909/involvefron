export enum BUTTONS_USER_CARD {
  FAVORITE = 'favorite',
  FAVORITE_TALENT = 'favoriteTalent',
  REMINDER = 'reminder',
  INVITATION = 'invitation',
  REJECTED = 'rejected',
  APPROVED = 'approved',
  APPROVED_CONFIRMED = 'confirmacionAprobacion',
  REJECTED_CONFIRMED = 'confirmacionRechazo',
  INFORMATION = 'informacionContacto',
}