import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from './auth/services/sessionService/session.service';
import { PushNotificationsService } from './services/commons/push-notifications.service';
import { SuscriptionCanceledService } from './services/reclutador/login/subscription-canceled.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [PushNotificationsService, SuscriptionCanceledService]
})
export class AppComponent {
  title = 'involve-reclutamiento-front';

  constructor(public sessionService: SessionService,
              private router: Router){
  }

  get isAtLogin(): boolean{
    return this.router.url === '/' ||
          this.router.url.includes('login') ||
          this.router.url.includes('register')  ||
          this.router.url.includes('forgot-password');
  }
}
