import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModalPrivacyComponent } from './auth/components/modal/modal-privacy/modal-privacy.component';
import { ModaltermsComponent } from './auth/components/modal/modalterms/modalterms.component';
import { CouponsTemplateComponent } from './auth/pages/coupons-template/coupons-template.component';
import { ChangelogComponent } from './commons/changelog/changelog.component';
import { AuthGuard } from './commons/guards/auth.guard';
// import { ResumenComponent } from './modulos/dashboardReclutadores/estadisticas-web/resumen/resumen.component';

const routes: Routes = [
  {path: '', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)},
  {path: 'dashboard', loadChildren: () => import('./modulos/dashboardReclutadores/dashboard.module').then(m => m.DashboardModule)},
  {path: 'error', loadChildren: () => import('./error-screens/error-screens.module').then(m => m.ErrorScreensModule)},
  {path: 'legal', loadChildren: () => import('./legal/legal.module').then(m => m.LegalModule)},

  {path: 'public/terms-and-conditions', component: ModaltermsComponent},
  {path: 'public/privacy-notice', component: ModalPrivacyComponent},
  { path: 'public/changelog', component: ChangelogComponent },
  { path: 'closed/coupons', component: CouponsTemplateComponent, canActivate: [AuthGuard]},

  // {path: 'app-resumen', component: ResumenComponent},
  {path: '**', pathMatch: 'full', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
