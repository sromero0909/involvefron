// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // restPrefix:"https://je2amljwxe.execute-api.us-east-1.amazonaws.com/dev/involve",
  restPrefix: 'https://2as657ryeb.execute-api.us-east-1.amazonaws.com/dev/involve',
  restPrefixPay: 'https://2as657ryeb.execute-api.us-east-1.amazonaws.com/dev/pay',
  restPsychometric: 'https://p08ea3ctx2.execute-api.us-east-1.amazonaws.com/dev',
  restStatistics: 'https://2as657ryeb.execute-api.us-east-1.amazonaws.com/dev',
  firebase: {
    apiKey: 'AIzaSyDts4tJyQtJA7PRmyUXYcLr12m6nvuc5Gw',
    authDomain: 'involvedemo.firebaseapp.com',
    projectId: 'involvedemo',
    storageBucket: 'involvedemo.appspot.com',
    messagingSenderId: '492023559670',
    appId: '1:492023559670:web:479c2fb00a90ebda4303d7',
  },
  VAPID_KEY: 'BH8REQWtD8Jg00JJNw5SXdUiJxNt6l90uKXFwA7V5_5fFlZVzXrBacblExgMl_b2YiYSWMWlpNTQMaecr0BqjfA',
  isDevelopment: true,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
